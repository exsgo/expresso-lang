using System;
using System.IO;
using Expresso.Ast.Analysis;
using Expresso.CodeGen;

namespace Expresso.Terminal
{
	class ExpressoMain
	{
		public static void Main(string[] args)
		{
            if(args.Length == 0 || args[0] == "--help"){
				Console.WriteLine(
@"Welcome to the Expresso Console!
I can read Expresso source files and compile them into assembly files that mono can execute.
Usage: exsc main_source_file_name {-o target_path | -e executable_name(without extension)}
To execute the resulting binary: mono <the name of the executable(with extension)> [program_arguments]"
                );
				return;
            }else if(args[0] == "--version"){
                Console.WriteLine($"Expresso compiler for command line v.{ExpressoConstants.VersionString}");
                return;
            }
			
			var file_name = args[0];
            var output_path = (args[1] == "-o") ? args[2] : args[4];
            var executable_name = (args[1] == "-o") ? args[4] : args[2];

            try{
                var parser = new Parser(new Scanner(file_name)){
                    DoPostParseProcessing = true
                };
                parser.Parse();

                var ast = parser.TopmostAst;

                var options = new ExpressoCompilerOptions{
                    OutputPath = output_path,
                    BuildType = BuildType.Debug | BuildType.Executable,
                    ExecutableName = executable_name
                };
                if(!Directory.Exists(output_path))
                    Directory.CreateDirectory(output_path);

                var generator = new CodeGenerator(parser, options);
                generator.VisitAst(ast, null);
            }
            catch(ParserException){
                // Ignore the exception because the parser already handled this case
                //Console.Error.WriteLine(e);
            }
            catch(Exception e){
                var prev_color = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine(e);
                Console.ForegroundColor = prev_color;
            }
		}
	}
}
