module main;


import System.{Math, Exception, SerializableAttribute} as {Math, Exception, SerializableAttribute};
import System.Text.RegularExpressions.Regex as Regex;
import test_module::{TestClass, TextClass2} from "./testmodule.exs" as {TestClass, TestClass2};
import test_module2::StaticClass from "./test_module2.exs" as {StaticClass};
import OtherLanguageWorld.{StaticClass, TestEnum} from "./InteroperabilityTest.dll" as {StaticClass, TestEnum};

let RAW_STRING = r#"<a href="http://www.expresso-lang.org"></a>
"#;

enum SomeEnum
{
    // some comment
    A(int),
    B(int, uint),
    C(SomeDerivedClass)
    
    public def print()
    {
        match self {
            SomeEnum::A{some_int} => println("${some_int}");,
            SomeEnum::B{10, 20u} => println("10, 20");,
            SomeEnum::B{some_int, some_uint} => {
                var some_int2 = some_int;
                while some_int2 > 0 {
                    some_int2 -= 1;
                }

                if some_int2 == 0 {
                    continue;
                }else if some_int == 10 {
                    break upto 2;
                }else{

                }

                let option = Option::Some<AST>{0: AST::Class{0: node, 1: "someClass"}};
                var some_uint2 = some_uint;
                do{
                    some_uint2 -= 1;
                }while some_uint2 > 0;

                for let x in 0..10 {
                    println("${x}");
                }
                println("${some_int} ${some_uint}");
            },
            SomeEnum::C{derived_class} => {
                let x = SomeDerivedClass3{some_x: 10};
                match derived_class {
                    SomeDerivedClass{x: x, y: y} => println("SomeDerivedClass{${x}, ${y}}");
                }

                derived_class = SomeDerivedClass{x: 10, y: 20};
            },
            SomeEnum::C{derived_class2} => {
                match derived_class2 {
                }
            }
        }
    }

    public def printUsingIf()
    {
        array.Copy(self.ch as char);
        if self == SomeEnum::A{some_int} {
            println("${some_int}");
        }
    }
}

enum SomeEnum2
{
    A = 1,
    B,
    C

    public def print()
    {
        match self {
            SomeEnum2::A => println("A");,
            SomeEnum2::B => println("B");,
            SomeEnum2::C => println("C");
        }
    }
}

enum SomeEnum3<T, U>
{
    A(int),
    B(uint, float)
}

export interface SomeInterface<T>
{
    def getX() -> T;
}

#[Serializable]
export class TestClass
{
    let x (- int;
    private let y (- SomeDerivedClass;
    private let z (- Option<int>;

    #[Author{name: "train12"}]
    public def getX(){
        return self.x;
    }

    public def getY() -> SomeDerivedClass
    {
        return self.y;
    }

    public def print<T>() -> void requires getX beforehand
    where T: Interface, Interface2, SomeClass
    {
        ;
    }

    public def print<T, U>() -> void
    requires getX beforehand
    where T: Interface, Interface2, SomeClass
    {
        ;
    }

    public def print2() -> Option<int>
    requires getX beforehand
    {
        ;
    }

    public def print3() -> Option<int>
    where T: Interface, Interface2, SomeClass
    {
        ;
    }
}

abstract class BaseClass
{
    let x (- int;

    public abstract mutating def addX(n (- int);

    public virtual def getX()
    {
        return self.x;
    }
}

/// # Summary
class SomeDerivedClass<T> : IInterface, IInterface2, TestClass
{
    let x (- T;   // some comment
    let y (- T;
}

class SomeDerivedClass2: IInterface, IInterface2{
}

sealed class SomeDerivedClass3 : BaseClass
{
    private let some_x (- int;

    // some comment
    public mutating def addX(n (- int)
    friends with getX
    {
        self.some_x += n;
    }

    public override def getX()
    {
        try{
            println("x: ${self.some_x}");
        }
        catch e (- Exception {

        }
        return self.x;
    }
}

def throwException()
{
    throw Exception{message: "Some error occurred"};
}

def getInt() -> int
{
    return 10;
}

def returnInt(n (- int, j (- SomeDerivedClass) -> int
{
    return n;
}

def returnNull(str = "abc") -> SomeDerivedClass
{
    return null;
}

def returnIntSeq()
{
    return 0..10;
}

def createOption()
{
    return Option::Some<int>{0: 10};
}

def callGenericMethod()
{
    doSomething<int, string>(10, "abc");
    doSomething2(10, 20);
}

def createResult()
{
    return Result::Ok<int, string>{0: 10};
}

def doMatch()
{
    match value {
        SomeEnum::A{value} => {
            println("A");
            return value;
        },
        SomeEnum::B{value} => {
            println("B");
            return value;
        },
        _ => {
            return -1;
        }
    }
}

enum SomeEnum
{
    A(int, uint),
    B(string, char)

    public def doMatch()
    {
        match self {
            SomeEnum::A{value, value2} => println("${value}");,
            SomeEnum::B{value, value2} => return value2;
        }
    }
}

enum SomeEnum2
{
    A = 1,
    B

    public def doMatch()
    {
        match self {
            SomeEnum2::A => println("A");,
            SomeEnum2::B => {
                println("B");
                return 10;
            },
            SomeEnum2::C => println("C");
        }
    }
}

/// # Summary
/// I'm the main function
def main()
{
    let a = 100;
    // I'm a line comment
    let sin = Math.Sin(0.0);
    /* I'm a multi-line comment
    blah blah
    */
    //let abc = 10;
    let sin2 (- double = Math.Sin(0.0);
    let str = "\\tsome stringあいうえお日本語";
    let chr = 'a';
    let linefeed = '\n';
    let backslash = '\\';
    let quote = '\'';
    let double_quote = "\"";
    let raw_string = r"some raw stringあいうえお日本語
    blah blah";
    let interpolation = "$$sin: ${sin}あいうえお日本語";
    let something = self.x;
    let b = 10, c = 20, d = 30;
    let e = getInt();
    let f = returnInt(10, 20);
    let g = e as double;
    let inst = SomeDerivedClass3{some_x: 10};
    inst.getX();

    var a2 = 100;
    var sin = Math.Sin(0.0);
    var str = "\\tsome stringあいうえお日本語";
    var chr = 'a';
    var raw_string = r"some raw string${sin}あいうえお日本語
    blah blah";
    var interpolation = "$$sin: ${sin}${getInt()}あいうえお日本語";
    var something = self.x;
    var b2 = 10, c2 = 20, d2 = 30;
    var e = getInt();
    var f = returnInt(10, 20);
    var g = e as double;

    for let i in returnIntSeq(){
        for let (i, j) in (returnIntSeq(), returnIntSeq()) {
            println(i);
            println(j);
            if i == j {
                continue upto 2;
            }
        }
    }

    let flag = true, another_flag = false;
    if flag {
        println("true");
    }else if another_flag {
        another_flag = false;
        let one_another_flag = false;
        option = Option::Some<uint>{0: 10u};
        println("another_flag");
    }else if let Option::Some{value} = option {
        println("false");
    }

    option = Option::Some<int>{0: 200};

    if let Option::Some{value} = option {
        println("Option::Some{{${value}}}");
    }
    
    println(a, sin);

    let str = "abc";
    match str {
        "abc" => {
            println("abc");
        },
        "def" => println("def");,
        "ghi" => throw Exception{message: "Unknown phrase"};
        _ => println("otherwise");
    }

    var i = 0;
    do{
        i += 1;
    }while i < 100;

    try{
        throwException();
    }
    catch e (- Exception {
        println("thrown");
    }
    finally{
        clean();
    }
}