.PHONY: Expresso.sln

DEST_BIN = bin
DEST_DIR = lib/exsc
PREFIX = /usr
DEST = $(PREFIX)/$(DEST_DIR)
EXE = bin/Release/exsc.exe
EXE_CONFIG = bin/Release/exsc.exe.config
EXPRESSO_DLL = bin/Release/Expresso.dll
EXPRESSO_RUNTIME_DLL = bin/Release/ExpressoRuntime.dll
NREFACTORY_DLL = Expresso/bin/Release/ICSharpCode.NRefactory.dll
COLLECTIONS_DLL = bin/Release/Microsoft.Experimental.Collections.dll
IMMUTABLE_DLL = bin/Release/System.Collections.Immutable.dll
METADATA_DLL = bin/Release/System.Reflection.Metadata.dll
STD_LIBRARY_FILE = Expresso/ExpressoRuntime/expresso_sources/std.exs
SOLUTION = ./Expresso.sln

all: exsc.exe

exsc.exe: $(SOLUTION)
	nuget restore $(SOLUTION)
	git clone https://github.com/icsharpcode/NRefactory.git Libraries/NRefactory
	msbuild $(SOLUTION) /p:Configuration=Release /p:Platform="x86"

install: $(EXE)
	install -d $(DEST)
	install $(EXE) $(DEST)
	install $(EXPRESSO_DLL) $(DEST)
	install $(EXPRESSO_RUNTIME_DLL) $(DEST)
	install $(NREFACTORY_DLL) $(DEST)
	install $(COLLECTIONS_DLL) $(DEST)
	install $(IMMUTABLE_DLL) $(DEST)
	install $(METADATA_DLL) $(DEST)
	install $(EXE_CONFIG) $(DEST)
	install $(STD_LIBRARY_FILE) $(DEST)
