#! /bin/sh

STD_EXS = "Expresso/ExpressoRuntime/expresso_sources/std.exs"

echo "mkdir ExpressoTest/test_executables"
mkdir ExpressoTest/test_executables
nuget restore Expresso.sln
msbuild Expresso.sln /p:Platform="x86"
cp -v InteroperabilityTest/bin/Debug/InteroperabilityTest.dll ExpressoTest/sources/for_unit_tests
cp -v AnotherCsharpDll/bin/Debug/AnotherCsharpDll.dll ExpressoTest/sources/for_unit_tests
cp -v ${STD_EXS} ./bin/Debug
cp -v ${STD_EXS} ./bin/Release
cp -v ${STD_EXS} ExpressoTest/bin/Debug
