﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

using Expresso.Ast;

namespace Expresso.CodeGen
{
    public partial class CodeGenerator : IAstWalker<CodeGeneratorContext, Type>
    {
        /// <summary>
        /// An InterfaceTypeDefiner is responsible for inspecting types and defining the outlines of types.
        /// </summary>
        public class InterfaceTypeDefiner : IAstWalker
        {
            CodeGenerator generator;
            CodeGeneratorContext context;
            string field_prefix;
            Type self_type;
            bool is_raw_value_enum;

            public InterfaceTypeDefiner(CodeGenerator generator, CodeGeneratorContext context)
            {
                this.generator = generator;
                this.context = context;
            }

            #region IAstWalker implementation

            public void VisitAst(ExpressoAst ast)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitBlock(BlockStatement block)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitBreakStatement(BreakStatement breakStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitContinueStatement(ContinueStatement continueStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitDoWhileStatement(DoWhileStatement doWhileStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitEmptyStatement(EmptyStatement emptyStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitExpressionStatement(ExpressionStatement exprStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitForStatement(ForStatement forStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStatement)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitIfStatement(IfStatement ifStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitReturnStatement(ReturnStatement returnStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitMatchStatement(MatchStatement matchStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitThrowStatement(ThrowStatement throwStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitTryStatement(TryStatement tryStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitWhileStatement(WhileStatement whileStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitYieldStatement(YieldStatement yieldStmt)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitArrayInitializer(ArrayInitializerExpression arrayInitializer)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitAssignment(AssignmentExpression assignment)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitBinaryExpression(BinaryExpression binaryExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitCallExpression(CallExpression call)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitCastExpression(CastExpression castExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitCatchClause(CatchClause catchClause)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitClosureLiteralExpression(ClosureLiteralExpression closure)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitComprehensionExpression(ComprehensionExpression comp)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitComprehensionForClause(ComprehensionForClause compFor)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitComprehensionIfClause(ComprehensionIfClause compIf)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitConditionalExpression(ConditionalExpression condExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitFinallyClause(FinallyClause finallyClause)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitLiteralExpression(LiteralExpression literal)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitIdentifier(Identifier ident)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitIndexerExpression(IndexerExpression indexExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitMemberReference(MemberReferenceExpression memRef)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitPathExpression(PathExpression pathExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitParenthesizedExpression(ParenthesizedExpression parensExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitObjectCreationExpression(ObjectCreationExpression creation)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitSequenceInitializer(SequenceInitializer seqInitializer)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitMatchClause(MatchPatternClause matchClause)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitSequenceExpression(SequenceExpression seqExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitUnaryExpression(UnaryExpression unaryExpr)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitSelfReferenceExpression(SelfReferenceExpression selfRef)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitSuperReferenceExpression(SuperReferenceExpression superRef)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitNullReferenceExpression(NullReferenceExpression nullRef)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitCommentNode(CommentNode comment)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitTextNode(TextNode textNode)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitTypeConstraint(TypeConstraint constraint)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitPostModifiers(PostModifiers postModifiers)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public void VisitSimpleType(SimpleType simpleType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitPrimitiveType(PrimitiveType primitiveType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitReferenceType(ReferenceType referenceType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitMemberType(MemberType memberType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitFunctionType(FunctionType funcType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitParameterType(ParameterType paramType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitKeyValueType(KeyValueType keyValueType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitPlaceholderType(PlaceholderType placeholderType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitFunctionParameterType(FunctionParameterType parameterType)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitAttributeSection(AttributeSection section)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitImportDeclaration(ImportDeclaration importDecl)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitFunctionDeclaration(FunctionDeclaration funcDecl)
            {
                Debug.Assert(generator.symbol_table.Name.StartsWith("type_", StringComparison.CurrentCulture), "We should be in a type.");

                int tmp_counter = generator.scope_counter;
                generator.DescendScope();
                generator.scope_counter = 0;

                Debug.Assert(generator.symbol_table.Name.StartsWith(
                    "func_", StringComparison.CurrentCulture) || generator.symbol_table.Name.StartsWith("method_signature_", StringComparison.CurrentCulture),
                    $"We're looking at a function '{funcDecl.Name}': {generator.symbol_table.Name}."
                );
                Debug.Assert(generator.symbol_table.Name.Contains(funcDecl.Name), $"We're looking at a function '{funcDecl.Name}': {generator.symbol_table.Name}.");

                var context_ast = context.ContextAst;
                context.ContextAst = funcDecl;

                var prev_generic_types = generator.generic_types;

                var param_types = funcDecl.TypeConstraints.Any() ? null : funcDecl.Parameters.Select((param, index) => {
                    context.ParameterIndex = funcDecl.Modifiers.HasFlag(Modifiers.Static) ? index : index + 1;
                    return generator.VisitParameterDeclaration(param, context);
                });
                context.ParameterIndex = -1;

                var return_type = funcDecl.TypeConstraints.Any() ? null :
                    (context.GenericTypeParameterReplacer != null) ? funcDecl.ReturnType.AcceptTypeWalker(context.GenericTypeParameterReplacer) :
                    CSharpCompilerHelpers.GetNativeTypeWithSelf(funcDecl.ReturnType, context.LazyTypeBuilder?.Name);

                var flags = MethodAttributes.Private | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Final;
                if(funcDecl.Modifiers.HasFlag(Modifiers.Public)){
                    flags |= MethodAttributes.Public;
                    flags ^= MethodAttributes.Private;
                }

                if(funcDecl.Modifiers.HasFlag(Modifiers.Static))
                    flags |= MethodAttributes.Static;

                if(funcDecl.Modifiers.HasFlag(Modifiers.Abstract))
                    flags |= MethodAttributes.Abstract;

                if(funcDecl.Modifiers.HasFlag(Modifiers.Virtual) || funcDecl.Modifiers.HasFlag(Modifiers.Override)){
                    flags |= MethodAttributes.Virtual;
                    flags ^= MethodAttributes.NewSlot;
                    flags ^= MethodAttributes.Final;
                }

                if(funcDecl.Parent is TypeDeclaration type_decl && type_decl.TypeKind == ClassType.Interface){
                    flags |= MethodAttributes.Abstract | MethodAttributes.Virtual;
                    flags ^= MethodAttributes.Final;
                }else if(funcDecl.Parent is TypeDeclaration type_decl2){
                    foreach(var base_type in type_decl2.BaseTypes){
                        var base_type_table = generator.symbol_table.GetTypeTable(base_type.Name);
                        // interfaces' methods must be virtual
                        if(base_type_table.HasSymbol(funcDecl.Name)){
                            flags |= MethodAttributes.Virtual;
                            flags ^= MethodAttributes.Final;
                        }
                    }
                }

                var generic_param_type_index = (generator.generic_types != null) ? generator.generic_types.Count : -1;
                MethodBuilder method_builder;
                if(funcDecl.Parent is TypeDeclaration type_decl3 && type_decl3.TypeKind == ClassType.Interface){
                    method_builder = context.InterfaceTypeBuilder.DefineMethod(funcDecl.Name, flags, return_type, param_types.ToArray());
                }else{
                    if(funcDecl.TypeConstraints.Any())
                        method_builder = context.LazyTypeBuilder.DefineGenericMethod(funcDecl.Name, flags, generator, context, funcDecl);
                    else
                        method_builder = context.LazyTypeBuilder.DefineMethod(funcDecl.Name, flags, return_type, param_types.ToArray(), generator, context, funcDecl);

                    var last_param = funcDecl.Parameters.LastOrDefault();
                    if(last_param != null && last_param.IsVariadic){
                        var param_index = funcDecl.Parameters.Count;
                        var param_builder = method_builder.DefineParameter(param_index, ParameterAttributes.None, last_param.Name);
                        var attribute_ctor = typeof(ParamArrayAttribute).GetConstructor(Type.EmptyTypes);
                        param_builder.SetCustomAttribute(new CustomAttributeBuilder(attribute_ctor, Type.EmptyTypes));
                    }
                }

                if(funcDecl.TypeConstraints.Any()){
                    var generic_types_in_func = generator.generic_types.Skip(generic_param_type_index).Select(gp => gp).ToList();
                    generator.generic_type_dict.Add(funcDecl.NameToken.IdentifierId, generic_types_in_func);
                    generator.generic_types.RemoveRange(generic_param_type_index, generator.generic_types.Count - generic_param_type_index);
                }

                context.CustomAttributeSetter = method_builder.SetCustomAttribute;
                context.AttributeTarget = AttributeTargets.Method;
                // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
                funcDecl.Attribute.AcceptWalker(generator, context);

                generator.AscendScope();
                generator.scope_counter = tmp_counter + 1;
            }

            public void VisitTypeDeclaration(TypeDeclaration typeDecl)
            {
                int tmp_counter = generator.scope_counter;
                generator.DescendScope();
                generator.scope_counter = 0;

                Debug.Assert(generator.symbol_table.Name.StartsWith("type_", StringComparison.CurrentCulture), $"We're looking at the type `{typeDecl.Name}`: {generator.symbol_table.Name}.");
                Debug.Assert(generator.symbol_table.Name.Contains(typeDecl.Name), $"We're looking at the type `{typeDecl.Name}`: {generator.symbol_table.Name}.");

                var parent_type = context.LazyTypeBuilder;

                is_raw_value_enum = generator.symbol_table.GetSymbol(Utilities.RawValueEnumValueFieldName) != null;
                var symbol = generator.GetRuntimeSymbol(typeDecl.NameToken);
                if(typeDecl.TypeKind == ClassType.Interface){
                    context.InterfaceTypeBuilder = (TypeBuilder)symbol.Type;

                    context.CustomAttributeSetter = context.InterfaceTypeBuilder.SetCustomAttribute;
                    // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
                    typeDecl.Attribute.AcceptWalker(generator, context);
                }else{
                    context.LazyTypeBuilder = symbol.LazyTypeBuilder;

                    context.CustomAttributeSetter = context.LazyTypeBuilder.TypeBuilder.SetCustomAttribute;
                    context.AttributeTarget = AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum;
                    // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
                    typeDecl.Attribute.AcceptWalker(generator, context);

                    if(typeDecl.TypeKind == ClassType.Enum && !is_raw_value_enum)
                        field_prefix = CSharpCompilerHelpers.HiddenMemberPrefix;
                    else
                        field_prefix = "";
                }

                if(typeDecl.TypeKind != ClassType.Enum || !is_raw_value_enum){
                    try{
                        // In order to define a generic field
                        if(generator.generic_type_dict.TryGetValue(typeDecl.NameToken.IdentifierId, out var generic_type_list)){
                            generator.generic_types = generic_type_list.Select(gt => gt).ToList();
                            context.GenericTypeParameterReplacer = new GenericTypeParameterReplacer(generator.generic_types, context);
                        }

                        foreach(var member in typeDecl.Members)
                            member.AcceptWalker(this);

                        var type = (typeDecl.TypeKind == ClassType.Interface) ? context.InterfaceTypeBuilder.CreateType() : context.LazyTypeBuilder.CreateInterfaceType();
                        var expresso_symbol = (typeDecl.TypeKind == ClassType.Interface) ? new ExpressoSymbol{Type = type} : new ExpressoSymbol{Type = type, LazyTypeBuilder = context.LazyTypeBuilder};
                        UpdateSymbol(typeDecl.NameToken, expresso_symbol);

                        if(typeDecl.TypeKind != ClassType.Interface){
                            //if(typeDecl.TypeKind == ClassType.Class)
                            //    AddParentFields(generator.symbol_table);

                            // Add fields as ExpressoSymbols so that we can refer to fields easily
                            RegisterFields(typeDecl, type);
                        }
                    }
                    finally{
                        context.LazyTypeBuilder = parent_type;
                        context.InterfaceTypeBuilder = null;
                        context.GenericTypeParameterReplacer = null;
                    }
                }else{
                    // Raw value enums are wrapped in class to have methods
                    try{
                        var interface_type_builder = context.LazyTypeBuilder;
                        var enum_builder = context.ModuleBuilder.DefineEnum(interface_type_builder.Name + "_RealEnum", TypeAttributes.Public, typeof(int));
                        self_type = enum_builder.AsType();

                        context.EnumBuilder = enum_builder;
                        foreach(var member in typeDecl.Members.OfType<FieldDeclaration>())
                            member.AcceptWalker(this);

                        context.EnumBuilder = null;
                        context.LazyTypeBuilder = interface_type_builder;
                        foreach(var method in typeDecl.Members.OfType<FunctionDeclaration>())
                            method.AcceptWalker(this);

                        var enum_type = enum_builder.CreateType();

                        var field_builder = interface_type_builder.DefineField(Utilities.RawValueEnumValueFieldName, enum_type, false);
                        var value_symbol = generator.symbol_table.GetSymbol(Utilities.RawValueEnumValueFieldName);
                        AddSymbol(value_symbol, new ExpressoSymbol{PropertyOrField = field_builder});

                        var interface_interface_type = interface_type_builder.CreateInterfaceType();
                        var expresso_symbol = new ExpressoSymbol{Type = interface_interface_type, LazyTypeBuilder = interface_type_builder};
                        UpdateSymbol(typeDecl.NameToken, expresso_symbol);

                        // Add fields as ExpressoSymbols so that we can refer to raw value style enum members
                        // This operation can't be moved to CodeGenerator because we have no reference to the enum type there
                        RegisterFields(typeDecl, enum_type);
                    }
                    finally{
                        context.LazyTypeBuilder = parent_type;
                        self_type = null;
                    }
                }

                generator.AscendScope();
                generator.scope_counter = tmp_counter + 1;
            }

            public void VisitAliasDeclaration(AliasDeclaration aliasDecl)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitFieldDeclaration(FieldDeclaration fieldDecl)
            {
                var flags = FieldAttributes.Private;
                if(fieldDecl.Modifiers.HasFlag(Modifiers.Static))
                    flags |= FieldAttributes.Static;

                // Don't set InitOnly flag or we'll fail to initialize the fields
                // because fields are initialized via impl methods.
                // Therefore they won't be closed in the consturctor.
                //if(fieldDecl.Modifiers.HasFlag(Modifiers.Immutable))
                //    attr |= FieldAttributes.InitOnly;

                if(fieldDecl.Modifiers.HasFlag(Modifiers.Private))
                    flags |= FieldAttributes.Private;
                else if(fieldDecl.Modifiers.HasFlag(Modifiers.Protected))
                    flags |= FieldAttributes.Family;
                else if(fieldDecl.Modifiers.HasFlag(Modifiers.Public))
                    flags |= FieldAttributes.Public;
                else
                    throw new GeneratorException("Unknown modifiers!");

                if(!fieldDecl.Modifiers.HasFlag(Modifiers.Private))
                    flags ^= FieldAttributes.Private;

                foreach(var init in fieldDecl.Initializers){
                    var type = DetermineType(init.NameToken.Type);
                    FieldBuilder field_builder;
                    if(context.EnumBuilder != null){
                        if(init.Initializer is LiteralExpression literal && literal.Value is int literal_value){
                            var raw_enum_value = literal_value;
                            field_builder = context.EnumBuilder.DefineLiteral(init.Name, raw_enum_value);
                        }else{
                            // Don't report the error because we've already done that in the parser
                            throw new Exception("Unreachable");
                            //throw new ParserException(
                            //    "Raw value enum initializers should be integer literal expressions on {0}.",
                            //    "ES4032",
                            //    fieldDecl,
                            //    init.ToString()
                            //);
                        }
                    }else{
                        field_builder = context.LazyTypeBuilder.DefineField(field_prefix + init.Name, type, !Expression.IsNullNode(init.Initializer), flags);
                    }

                    context.CustomAttributeSetter = field_builder.SetCustomAttribute;
                    context.AttributeTarget = AttributeTargets.Field;
                    // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
                    fieldDecl.Attribute.AcceptWalker(generator, context);

                    AddSymbol(init.NameToken, new ExpressoSymbol{FieldBuilder = field_builder});
                }
            }

            public void VisitParameterDeclaration(ParameterDeclaration parameterDecl)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitVariableInitializer(VariableInitializer initializer)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitWildcardPattern(WildcardPattern wildcardPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitIdentifierPattern(IdentifierPattern identifierPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitCollectionPattern(CollectionPattern collectionPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitDestructuringPattern(DestructuringPattern destructuringPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitTuplePattern(TuplePattern tuplePattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitExpressionPattern(ExpressionPattern exprPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitKeyValuePattern(KeyValuePattern keyValuePattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitPatternWithType(PatternWithType pattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitTypePathPattern(TypePathPattern pathPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitNullNode(AstNode nullNode)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitNewLine(NewLineNode newlineNode)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitWhitespace(WhitespaceNode whitespaceNode)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitExpressoTokenNode(ExpressoTokenNode tokenNode)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitPatternPlaceholder(AstNode placeholder, ICSharpCode.NRefactory.PatternMatching.Pattern child)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            #endregion

            void RegisterFields(TypeDeclaration typeDecl, Type type)
            {
                // Add fields as ExpressoSymbols so that we can reference raw value style enum members
                foreach(var field_decl in typeDecl.Members.OfType<FieldDeclaration>()){
                    foreach(var initializer in field_decl.Initializers){
                        var field = type.GetField(field_prefix + initializer.Name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
                        if(field == null)
                            throw new InvalidOperationException($"{initializer.Name} is null! Something wrong has occurred!");

                        UpdateSymbol(initializer.NameToken, new ExpressoSymbol{PropertyOrField = field});
                    }
                }
            }

            //TODO: implement protected on fields
            /*void AddParentFields(SymbolTable typeTable)
            {
                var parent_type_table = typeTable.ParentType;
                while(parent_type_table != null){
                    foreach(var symbol in parent_type_table.Symbols.Where(s => !(s.Type is FunctionType))){
                        var flags = FieldAttributes.Private;
                        if(symbol.Modifiers.HasFlag(Modifiers.Static))
                            flags |= FieldAttributes.Static;

                        if(symbol.Modifiers.HasFlag(Modifiers.Private))
                            flags |= FieldAttributes.Private;
                        else if(symbol.Modifiers.HasFlag(Modifiers.Protected))
                            flags |= FieldAttributes.Family;
                        else if(symbol.Modifiers.HasFlag(Modifiers.Public))
                            flags |= FieldAttributes.Public;
                        else
                            throw new GeneratorException("Unknown modifiers!");

                        if(!symbol.Modifiers.HasFlag(Modifiers.Private))
                            flags ^= FieldAttributes.Private;

                        var type = DetermineType(symbol.Type);
                        var field_builder = context.LazyTypeBuilder.DefineField(symbol.Name, type, false, flags);

                        AddSymbol(symbol, new ExpressoSymbol{FieldBuilder = field_builder});
                    }

                    parent_type_table = parent_type_table.Parent;
                }
            }*/

            Type DetermineType(AstType astType)
            {
                // self_type is needed for raw value enums' fields
                if(self_type != null)
                    return self_type;

                if(context.GenericTypeParameterReplacer != null)
                    return astType.AcceptTypeWalker(context.GenericTypeParameterReplacer);
                else
                    return CSharpCompilerHelpers.GetNativeTypeWithSelfTypeBuilder(astType, context.LazyTypeBuilder.TypeBuilder);
            }

            void DefineGenericTypeParameterReplacer()
            {
                if(generator.generic_types.Any())
                    context.GenericTypeParameterReplacer = new GenericTypeParameterReplacer(generator.generic_types, context);
            }
        }
    }
}

