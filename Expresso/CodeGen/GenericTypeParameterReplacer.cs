﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using Expresso.Ast;

namespace Expresso.CodeGen
{
    using CSharpExpr = System.Linq.Expressions.Expression;

    /// <summary>
    /// This class is responsible for replacing <see cref="SimpleType"/> nodes with <see cref="ParameterType"/> nodes.
    /// </summary>
    public class GenericTypeParameterReplacer : IAstTypeWalker<Type>
    {
        public IEnumerable<GenericTypeParameterBuilder> GenericTypeParameters{get; private set;}
        private CodeGeneratorContext context;

        public GenericTypeParameterReplacer(IEnumerable<GenericTypeParameterBuilder> genericTypeParameters, CodeGeneratorContext context)
        {
            GenericTypeParameters = genericTypeParameters;
            this.context = context;
        }

        public Type VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            return parameterType.Type.AcceptTypeWalker(this);
        }

        public Type VisitFunctionType(FunctionType funcType)
        {
            var replaced_return_type = funcType.ReturnType.AcceptTypeWalker(this);
            var replaced_param_types = funcType.Parameters.Select(p => p.AcceptTypeWalker(this)).ToArray();
            if(replaced_return_type == typeof(void)){
                if(replaced_param_types.Any(p => p == null || p.ContainsGenericParameters)){
                    var generic_action_type = CSharpCompilerHelpers.GetActionType(replaced_param_types.Length);
                    return generic_action_type.MakeGenericType(replaced_param_types);
                }else{
                    return CSharpExpr.GetActionType(replaced_param_types);
                }
            }else{
                var type_args = replaced_param_types.Concat(new []{replaced_return_type}).ToArray();
                if(type_args.Any(ta => ta == null || ta.ContainsGenericParameters)){
                    var generic_func_type = CSharpCompilerHelpers.GetFuncType(replaced_param_types.Length);
                    return generic_func_type.MakeGenericType(replaced_param_types.Concat(new []{replaced_return_type}).ToArray());
                }else{
                    return CSharpExpr.GetFuncType(type_args);
                }
            }
        }

        public Type VisitKeyValueType(KeyValueType keyValue)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public Type VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptTypeWalker(this);
            return VisitSimpleType(memberType.ChildType);
        }

        public Type VisitParameterType(ParameterType paramType)
        {
            return GenericTypeParameters.First(t => t.Name == paramType.Name);
        }

        public Type VisitPlaceholderType(PlaceholderType placeholder)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public Type VisitPrimitiveType(PrimitiveType primitiveType)
        {
            return CSharpCompilerHelpers.GetPrimitiveType(primitiveType);
        }

        public Type VisitReferenceType(ReferenceType referenceType)
        {
            return CSharpCompilerHelpers.GetNativeType(referenceType);
        }

        public Type VisitSimpleType(SimpleType simpleType)
        {
            var type = CSharpCompilerHelpers.GetNativeTypeWithSelf(simpleType, context.LazyTypeBuilder.Name, false);
            if(type == null)
                throw new InvalidOperationException($"type is null: {simpleType}, self = {context.LazyTypeBuilder.Name}");

            // The following code can not be removed for some reason
            // Without it, the Option type will cause a native error
            if(!type.IsGenericTypeDefinition)
                return type;

            // !simpleType.TypeArguments.Any(): fix for types like vector<T> (- vector
            if(!simpleType.IdentifierNode.Type.IsNull && !simpleType.TypeArguments.Any() && simpleType.IdentifierNode.Type is SimpleType alias_type2){
                var type_args = alias_type2.TypeArguments.Select(ta => ta.AcceptTypeWalker(this));
                if(type_args.Any(ta => ta == null))
                    return null;

                type = type.MakeGenericType(type_args.ToArray());
            }else{
                var type_args = simpleType.TypeArguments.Select(ta => ta.AcceptTypeWalker(this));
                if(type_args.Any(ta => ta == null))
                    return null;

                type = type.MakeGenericType(type_args.ToArray());
            }

            return type;
        }

        public Type VisitNullNode(AstNode nullNode)
        {
            throw new InvalidOperationException("Can not work on that node");
        }
    }
}
