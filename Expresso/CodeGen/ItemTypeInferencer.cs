﻿using System;
using System.Linq;
using Expresso.Ast;

namespace Expresso.CodeGen
{
    public partial class CodeGenerator : IAstWalker<CodeGeneratorContext, Type>
    {
        /// <summary>
        /// This class is responsible for inferring the type of a pattern.
        /// </summary>
        public class ItemTypeInferencer : IAstPatternWalker<AstType>
	    {
            public AstType VisitCollectionPattern(CollectionPattern collectionPattern)
            {
                return collectionPattern.CollectionType;
            }

            public AstType VisitDestructuringPattern(DestructuringPattern destructuringPattern)
            {
                return destructuringPattern.TypePath;
            }

            public AstType VisitExpressionPattern(ExpressionPattern expressionPattern)
            {
                // Ignore expression patterns because they don't show any importance in defining variables
                return null;
            }

            public AstType VisitIdentifierPattern(IdentifierPattern identifierPattern)
            {
                return identifierPattern.Identifier.Type;
            }

            public AstType VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
            {
                return null;
            }

            public AstType VisitKeyValuePattern(KeyValuePattern keyValuePattern)
            {
                return keyValuePattern.KeyIdentifier.Type;
            }

            public AstType VisitMatchClause(MatchPatternClause matchClause)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public AstType VisitNullNode(AstNode nullNode)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public AstType VisitPatternWithType(PatternWithType pattern)
            {
                return pattern.Type;
            }

            public AstType VisitTuplePattern(TuplePattern tuplePattern)
            {
                var item_types = tuplePattern.Patterns.Select(p => p.AcceptPatternWalker(this));
                return AstType.MakeSimpleType("tuple", item_types);
            }

            public AstType VisitTypePathPattern(TypePathPattern typePathPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public AstType VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
            {
                return VisitPatternWithType(valueBindingPattern.Initializer.Pattern);
            }

            public AstType VisitWildcardPattern(WildcardPattern wildcardPattern)
            {
                return null;
            }
        }
    }
}
