﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Reflection.Emit;
using System.Reflection.Metadata;
using System.Text;

using Expresso.Ast;
using Expresso.Ast.Analysis;
using Expresso.Runtime.Builtins;

using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.CodeGen
{
    /// <summary>
    /// An <see cref="IComparer&lt;T&gt;"/> implementation that sorts import paths.
    /// With it, paths to the standard library come first and others come second.
    /// </summary>
    class ImportPathComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            if(x.StartsWith("System.", StringComparison.CurrentCulture) && !y.StartsWith("System.", StringComparison.CurrentCulture))
                return -1;
            else if(x.StartsWith("System.", StringComparison.CurrentCulture) && y.StartsWith("System.", StringComparison.CurrentCulture))
                return 0;
            else if(!x.StartsWith("System.", StringComparison.CurrentCulture) && y.StartsWith("System.", StringComparison.CurrentCulture))
                return -1;
            else
                return 0;
        }
    }

    /// <summary>
    /// Expressoの構文木を解釈してReflection.Emitでコード生成をするクラス。
    /// It emits IL code from the AST of Expresso.
    /// Assumes we are all OK with syntax and semantics since this class is considered
    /// to belong to the backend part in the compiler theory.
    /// </summary>
    /// <remarks>
    /// While emitting, we don't check the semantics and validity because the type check and
    /// other semantics analisys phases do that.
    /// We just care if an Expresso's expression is convertible and has been converted to IL code or not.
    /// </remarks>
    public partial class CodeGenerator : IAstWalker<CodeGeneratorContext, Type>
	{
        //###################################################
        //# Symbols defined in the whole program.
        //# It is represented as a hash table rather than a symbol table
        //# because we have defined a symbol id on all the identifier nodes
        //# that identifies the symbol uniqueness within the whole program.
        //###################################################
        // Actually I want it only visible to ExpressoTest but it doesn't allow a specific type to be associated with the InternalsVisibleTo attribute
        public static Dictionary<uint, ExpressoSymbol> Symbols = new Dictionary<uint, ExpressoSymbol>();
        internal static Dictionary<string, Type[]> InterfaceConstraints = new Dictionary<string, Type[]>();
        static int ClosureId = 0;
		
        const string ClosureMethodName = "__Apply";
        const string ClosureDelegateName = "__ApplyMethod";

        Parser parser;
        SymbolTable symbol_table;
        ExpressoCompilerOptions options;
        MatchClauseConditionDefiner condition_definer;
        ItemTypeInferencer item_type_inferencer;
        ILGenerator il_generator;
        List<GenericTypeParameterBuilder> generic_types;
        Dictionary<uint, List<GenericTypeParameterBuilder>> generic_type_dict;
        MethodDefinitionHandle main_method_def_handle;

        List<Label> break_targets = new List<Label>();
        List<Label> continue_targets = new List<Label>();

        int scope_counter = 0;

        ISymbolDocumentWriter document;

        /// <summary>
        /// Gets the generated assembly.
        /// </summary>
        public AssemblyBuilder AssemblyBuilder{
            get; private set;
        }

        public CodeGenerator(Parser parser, ExpressoCompilerOptions options)
        {
            this.parser = parser;
            symbol_table = parser.Symbols;
            this.options = options;
            item_type_inferencer = new ItemTypeInferencer();
            generic_types = new List<GenericTypeParameterBuilder>();
            generic_type_dict = new Dictionary<uint, List<GenericTypeParameterBuilder>>();

            CSharpCompilerHelpers.AddPrimitiveNativeSymbols();
            Debug.Assert(symbol_table.Name == "programRoot", "Expected the symbol_table points to 'programRoot'");
        }

        LocalBuilder MakeEnumeratorCreations(Type iteratorType)
        {
            LocalBuilder iterator_builder;

            if(iteratorType.Name.Contains("Dictionary")){
                var type = typeof(DictionaryEnumerator<,>);
                var substituted_type = type.MakeGenericType(iteratorType.GetGenericArguments());
                var ctor = substituted_type.GetConstructors().First();

                iterator_builder = il_generator.DeclareLocal(substituted_type);    //__iter
                il_generator.Emit(OpCodes.Newobj, ctor);
                EmitSet(null, iterator_builder, -1, null);
            }else{
                Type enumerator_type;
                if(iteratorType.IsArray)
                    enumerator_type = typeof(IEnumerable<>).MakeGenericType(iteratorType.GetElementType());
                else if(iteratorType.IsClass)
                    enumerator_type = iteratorType.GetInterface("IEnumerable`1");
                else
                    enumerator_type = iteratorType;

                var get_enumerator_method = enumerator_type.GetMethod("GetEnumerator");
                iterator_builder = il_generator.DeclareLocal(get_enumerator_method.ReturnType);

                EmitCall(get_enumerator_method);
                EmitSet(null, iterator_builder, -1, null);
            }

            return iterator_builder;
        }

        void EmitIterableAssignments(IEnumerable<LocalBuilder> variables, LocalBuilder enumerator, Label breakTarget)
        {
            var iterator_type = enumerator.LocalType;
            var move_false_label = il_generator.DefineLabel();
            var move_method = iterator_type.GetInterface("IEnumerator").GetMethod("MoveNext");
            EmitLoadLocal(enumerator, false);
            il_generator.Emit(OpCodes.Callvirt, move_method);

            EmitUnaryOp(OperatorType.Not);
            il_generator.Emit(OpCodes.Brfalse, move_false_label);
            il_generator.Emit(OpCodes.Br, breakTarget);
            il_generator.MarkLabel(move_false_label);

            var current_property = iterator_type.GetProperty("Current");
            if(variables.Count() == 1){
                EmitLoadLocal(enumerator, false);
                EmitCall(current_property.GetMethod);
                EmitSet(null, variables.First(), -1, null);
            }else{
                EmitLoadLocal(enumerator, false);
                EmitCall(current_property.GetMethod);
                var tuple_type = current_property.PropertyType;
                if(!tuple_type.Name.Contains("Tuple"))
                    throw new InvalidOperationException("iterators must return a tuple type when assigned to a tuple pattern.");
                
                foreach(var pair in Enumerable.Range(1, variables.Count() + 1).Zip(variables, (l, r) => new {Index = l, LocalBuilder = r})){
                    il_generator.Emit(OpCodes.Dup);
                    var item_property = tuple_type.GetProperty($"Item{pair.Index}");
                    EmitCall(item_property.GetMethod);
                    EmitSet(null, pair.LocalBuilder, -1, null);
                }
                il_generator.Emit(OpCodes.Pop);
            }
        }

        internal static void AddSymbol(Identifier ident, ExpressoSymbol symbol)
        {
            if(ident.IdentifierId == 0)
                throw new InvalidOperationException("An invalid identifier is invalid because it can't be used for any purpose.");
            
            try{
                Symbols.Add(ident.IdentifierId, symbol);
            }
            catch(ArgumentException){
                throw new InvalidOperationException($"The native symbol for '{ident.Name}' @ <id: {ident.IdentifierId}> is already added.");
            }
        }

        internal static void UpdateSymbol(Identifier ident, ExpressoSymbol symbol)
        {
            if(ident.IdentifierId == 0)
                throw new InvalidOperationException("An invalid identifier is invalid because it can't be used for any purpose.");

            if(!Symbols.TryGetValue(ident.IdentifierId, out var target_symbol))
                throw new InvalidOperationException($"The native symbol for '{ident.Name}' @ <id: {ident.IdentifierId}> isn't found.");

            //Symbols.Remove(ident.IdentifierId);
            //Symbols.Add(ident.IdentifierId, symbol);
            // We don't need to update other fields
            //target_symbol.FieldBuilder = symbol.FieldBuilder;
            //target_symbol.Lambda = symbol.Lambda;
            //target_symbol.Member = symbol.Member;
            //target_symbol.Method = symbol.Method;
            //target_symbol.Parameter = symbol.Parameter;
            //target_symbol.Parameters = symbol.Parameters;
            target_symbol.PropertyOrField = symbol.PropertyOrField;
            target_symbol.Type = symbol.Type;
            target_symbol.LazyTypeBuilder = symbol.LazyTypeBuilder;
        }

        void DescendScope()
        {
            symbol_table = symbol_table.Children[scope_counter];
        }

        void AscendScope()
        {
            symbol_table = symbol_table.Parent;
        }

        static bool CanReturn(ReturnStatement returnStmt)
        {
            var surrounding_func = returnStmt.Ancestors
                                             .OfType<BlockStatement>()
                                             .First()
                                             .Parent as FunctionDeclaration;
            return surrounding_func == null || surrounding_func != null && surrounding_func.Name != "main";
        }

        static BuildType PersistBuildType(BuildType previous, BuildType newConfig)
        {
            var new_build_config = newConfig;
            new_build_config |= previous.HasFlag(BuildType.Debug) ? BuildType.Debug : BuildType.Release;
            return new_build_config;
        }

        static bool ShouldDefineModuleType(IEnumerable<EntityDeclaration> entities)
        {
            return entities.Any(e => e is FunctionDeclaration || e is FieldDeclaration);
        }

        ExpressoSymbol GetRuntimeSymbol(Identifier ident)
        {
            if(Symbols.TryGetValue(ident.IdentifierId, out var symbol))
                return symbol;
            else
                return null;
        }

        void VisitChildAst(ExpressoAst ast, CodeGeneratorContext context, Parser currentParser)
        {
            var original_parser = parser;

            parser = currentParser;
            symbol_table = parser.Symbols;
            VisitAst(ast, context);

            // We can't move this code out of this method because then this.parser would point at other instance when we call this method in VisitAst
            parser = original_parser;
            symbol_table = original_parser.Symbols;
        }

        #region IAstWalker implementation

        public Type VisitAst(ExpressoAst ast, CodeGeneratorContext context)
        {
            if(context == null)
                context = new CodeGeneratorContext{OperationTypeOnIdentifier = OperationType.Load, ParameterIndex = -1};

            condition_definer = new MatchClauseConditionDefiner(this, context);

            var assembly_name = options.BuildType.HasFlag(BuildType.Assembly) ? ast.Name : options.ExecutableName;
            var name = new AssemblyName(assembly_name);

            var asm_builder = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.RunAndSave, options.OutputPath);
            var file_name = options.BuildType.HasFlag(BuildType.Assembly) ? assembly_name + ".dll" : assembly_name + ".exe";

            if(options.BuildType.HasFlag(BuildType.Debug)){
                var attrs = DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations;
                var attr_arg_types = new []{typeof(DebuggableAttribute.DebuggingModes)};
                var attr_arg_values = new object[]{attrs};
                asm_builder.SetCustomAttribute(new CustomAttributeBuilder(
                    typeof(DebuggableAttribute).GetConstructor(attr_arg_types), attr_arg_values
                ));
            }

#if WINDOWS
            var mod_builder = asm_builder.DefineDynamicModule(assembly_name, file_name, true);
#else
            var mod_builder = asm_builder.DefineDynamicModule(assembly_name, file_name);
#endif
            document = mod_builder.DefineDocument(parser.scanner.FilePath, ExpressoCompilerHelpers.LanguageGuid, Guid.Empty, SymDocumentType.Text);

            var local_pdb_generator = PortablePDBGenerator.CreatePortablePDBGenerator();
            local_pdb_generator.AddDocument(parser.scanner.FilePath, ExpressoCompilerHelpers.LanguageGuid);

            // Leave `ast.Name` as is because otherwise we can't refer to it later when visiting ImportDeclarations
            var type_builder = ShouldDefineModuleType(ast.Declarations) ? new LazyTypeBuilder(mod_builder, CSharpCompilerHelpers.ConvertHeadToUpperCase(ast.Name),
                                                                                              TypeAttributes.Class | TypeAttributes.Public, typeof(object), true, false)
                                                                                             : null;
            context.ModuleLazyTypeBuilder = type_builder;

            context.AssemblyBuilder = asm_builder;
            context.ModuleBuilder = mod_builder;
            context.LazyTypeBuilder = type_builder;

            if(ast.Name == "main")
                options.BuildType = PersistBuildType(options.BuildType, BuildType.Assembly);

            context.ExternalModuleType = null;
            var ordered = ast.Imports.OrderBy(i => i.ImportPaths.First().Name, new ImportPathComparer());
            foreach(var pair in ast.ExternalModules.Zip(ordered.SkipWhile(i => i.ImportPaths.First().Name.StartsWith("System.", StringComparison.CurrentCulture)),
                                                        (l, r) => new {Ast = l, Import = r})){
                Debug.Assert(pair.Import.ImportPaths.First().Name.StartsWith(pair.Ast.Name, StringComparison.CurrentCulture), "The module name must be matched to the import path");

                var tmp_counter = scope_counter;
                VisitChildAst(pair.Ast, context, parser.InnerParsers.First(ip => ip.TopmostAst == pair.Ast));
                scope_counter = tmp_counter;

                var first_import = pair.Import.ImportPaths.First();
                if(!first_import.Name.Contains("::") && !first_import.Name.Contains(".")){
                    var first_alias = pair.Import.AliasTokens.First();
                    Symbols.Add(first_alias.IdentifierId, new ExpressoSymbol{
                        Type = context.ExternalModuleType
                    });
                    context.ExternalModuleType = null;
                }
            }
            if(ast.Name == "main")
                options.BuildType = PersistBuildType(options.BuildType, BuildType.Executable);

            // These 4 lines are required because otherwise context.LazyTypeBuilder would be set to other type than the type representing the `Main` class
            context.AssemblyBuilder = asm_builder;
            context.ModuleBuilder = mod_builder;
            context.LazyTypeBuilder = type_builder;
            context.PDBGenerator = local_pdb_generator;

            context.CustomAttributeSetter = asm_builder.SetCustomAttribute;
            context.AttributeTarget = AttributeTargets.Assembly;
            foreach(var attribute in ast.Attributes)
                VisitAttributeSection(attribute, context);

            context.CustomAttributeSetter = mod_builder.SetCustomAttribute;
            context.AttributeTarget = AttributeTargets.Module;
            foreach(var attribute in ast.Attributes)
                VisitAttributeSection(attribute, context);

            Utilities.CompilerOutput.WriteLine($"Emitting code in {ast.ModuleName}...");

            foreach(var import in ast.Imports)
                import.AcceptWalker(this, context);

            // Define only the function signatures so that the functions can call themselves
            // and the interface type is defined before we inspect the functions and fields
            DefineFunctionSignaturesAndFields(ast.Declarations, context);
            // Register interface functions to CodeGenerator.Symbols so that we can call methods that are defined after callees
            RegisterInterfaceFunctions(ast.Declarations, context.LazyTypeBuilder, context.AssemblyBuilder);
            foreach(var decl in ast.Declarations)
                decl.AcceptWalker(this, context);

            // If the module ends with a type we can't call CreateInterfaceType
            if(type_builder != null && !type_builder.IsDefined)
                type_builder.CreateInterfaceType();

            context.ExternalModuleType = type_builder?.CreateType();
            asm_builder.Save(file_name);

            var pdb_id = new BlobContentId(Guid.NewGuid(), 0xffeeddcc);
            EmitDebugDirectoryAndPdb(Path.Combine(options.OutputPath, file_name), assembly_name + ".pdb", pdb_id, context.PDBGenerator);

            AssemblyBuilder = asm_builder;

            return null;
        }

        public Type VisitBlock(BlockStatement block, CodeGeneratorContext context)
        {
            if(block.IsNull)
                return null;

            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;


            var parent_block = context.ContextAst;
            context.ContextAst = block;

            var start_offset = il_generator.ILOffset;
            context.PDBGenerator.AddLocalScope(start_offset);
            il_generator.BeginScope();

            foreach(var stmt in block.Statements)
                stmt.AcceptWalker(this, context);

            il_generator.EndScope();

            context.PDBGenerator.SetLengthOnLocalScope(il_generator.ILOffset - start_offset);

            context.ContextAst = parent_block;

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public Type VisitBreakStatement(BreakStatement breakStmt, CodeGeneratorContext context)
        {
            int count = (int)breakStmt.CountExpression.Value;
            if(count > break_targets.Count)
                throw new GeneratorException("Can not break out of loops that many times!");

            //break upto Count; => goto label;
            il_generator.Emit(OpCodes.Br, break_targets[break_targets.Count - count]);
            return null;
        }

        public Type VisitContinueStatement(ContinueStatement continueStmt, CodeGeneratorContext context)
        {
            int count = (int)continueStmt.CountExpression.Value;
            if(count > continue_targets.Count)
                throw new GeneratorException("Can not break out of loops that many times!");

            //continue upto Count; => goto label;
            il_generator.Emit(OpCodes.Br, continue_targets[continue_targets.Count - count]);
            return null;
        }

        public Type VisitDoWhileStatement(DoWhileStatement doWhileStmt, CodeGeneratorContext context)
        {
            VisitWhileStatement(doWhileStmt.Delegator, context);
                                                            //{ body;
            return null;                                    //  while_stmt}
        }

        public Type VisitEmptyStatement(EmptyStatement emptyStmt, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitExpressionStatement(ExpressionStatement exprStmt, CodeGeneratorContext context)
        {
            exprStmt.Expression.AcceptWalker(this, context);
            return null;
        }

        public Type VisitForStatement(ForStatement forStmt, CodeGeneratorContext context)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var prev_parameters = context.Parameters;
            context.Parameters = new List<LocalBuilder>();
            forStmt.Left.AcceptWalker(this, context);
            var variables = context.Parameters;
            context.Parameters = prev_parameters;

            var iterator_type = forStmt.Target.AcceptWalker(this, context);

            var break_target = il_generator.DefineLabel();
            var continue_target = il_generator.DefineLabel();
            break_targets.Add(break_target);
            continue_targets.Add(continue_target);

            il_generator.BeginScope();
            var enumerator_builder = MakeEnumeratorCreations(iterator_type);

            il_generator.BeginScope();      //the beginning of the loop
            il_generator.MarkLabel(continue_target);

            EmitIterableAssignments(variables, enumerator_builder, break_target);

            // Here, `Body` represents just the body block itself
            // In a for statement, we must move the iterator a step forward
            // and assign the result to inner-scope variables
            VisitBlock(forStmt.Body, context);
            il_generator.Emit(OpCodes.Br, continue_target);
            il_generator.EndScope();
            il_generator.EndScope();

            il_generator.MarkLabel(break_target);
            break_targets.RemoveAt(break_targets.Count - 1);
            continue_targets.RemoveAt(continue_targets.Count - 1);

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public Type VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStatement, CodeGeneratorContext context)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var parent_params = context.Parameters;
            context.Parameters = new List<LocalBuilder>();
            valueBindingForStatement.Initializer.Pattern.AcceptWalker(this, context);
            var bound_variables = context.Parameters;
            context.Parameters = parent_params;

            var initializer_type = valueBindingForStatement.Initializer.Initializer.AcceptWalker(this, context);

            var break_target = il_generator.DefineLabel();
            var continue_target = il_generator.DefineLabel();
            break_targets.Add(break_target);
            continue_targets.Add(continue_target);

            il_generator.BeginScope();
            var enumerator_builder = MakeEnumeratorCreations(initializer_type);

            il_generator.BeginScope();      // the beginning of the loop
            il_generator.MarkLabel(continue_target);

            EmitIterableAssignments(bound_variables, enumerator_builder, break_target);

            // Here, `Body` represents just the body block itself
            // In a for statement, we must move the iterator a step forward
            // and assign the result to inner-scope variables
            VisitBlock(valueBindingForStatement.Body, context);
            il_generator.Emit(OpCodes.Br, continue_target);
            il_generator.EndScope();
            il_generator.EndScope();

            il_generator.MarkLabel(break_target);
            break_targets.RemoveAt(break_targets.Count - 1);
            continue_targets.RemoveAt(continue_targets.Count - 1);

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
            // for let x in some_sequence {} => (enumerator){ creation (bound_variables, block_variables){ loop_variable_initializer { real_body } } }
        }

        public Type VisitIfStatement(IfStatement ifStmt, CodeGeneratorContext context)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var jump_label = il_generator.DefineLabel();
            var false_label = il_generator.DefineLabel();
            var true_label = il_generator.DefineLabel();

            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;
            ifStmt.Condition.AcceptWalker(this, context);

            context.OperationTypeOnIdentifier = prev_op_type;
            il_generator.Emit(OpCodes.Brfalse, false_label);

            il_generator.MarkLabel(true_label);

            // Emit destructuring code
            if(ifStmt.Condition is ValueBindingPattern value_binding)
                VisitVariableInitializer(value_binding.Initializer, context);

            VisitBlock(ifStmt.TrueBlock, context);

            if(ifStmt.FalseStatement.IsNull){
                il_generator.MarkLabel(false_label);
            }else{
                il_generator.Emit(OpCodes.Br, jump_label);

                il_generator.MarkLabel(false_label);

                ifStmt.FalseStatement.AcceptWalker(this, context);

                il_generator.MarkLabel(jump_label);

                // in case this if statement is the last one in the function
                if(ifStmt.Parent is BlockStatement block && block.Parent is FunctionDeclaration && ifStmt.NextSibling == null)
                    il_generator.Emit(OpCodes.Ret);
            }

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public Type VisitReturnStatement(ReturnStatement returnStmt, CodeGeneratorContext context)
        {
            // If we are in the main function, then make return statements do nothing
            if(!CanReturn(returnStmt))
                return null;

            returnStmt.Expression.AcceptWalker(this, context);
            il_generator.Emit(OpCodes.Ret);
            return null;
        }

        public Type VisitMatchStatement(MatchStatement matchStmt, CodeGeneratorContext context)
        {
            // Match statement semantics: First we evaluate the target expression
            // and assign the result to a temporary variable that's alive within the whole statement.
            // All the pattern clauses must meet the same condition.
            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;
            var target_type = matchStmt.Target.AcceptWalker(this, context);
            context.OperationTypeOnIdentifier = prev_op_type;
            var tmp_var = il_generator.DeclareLocal(target_type);
            EmitSet(null, tmp_var, -1, null);

            //var parameters = ExpandTuple(target_type, tmp_var);
            var parent_tmp_var = context.TemporaryVariable;
            context.TemporaryVariable = tmp_var;
            var context_ast = context.ContextAst;
            context.ContextAst = matchStmt;

            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var prev_jump_label = context.CurrentJumpLabel;
            context.CurrentJumpLabel = il_generator.DefineLabel();

            //ExprTree.ConditionalExpression res = null, tmp_res = res;
            foreach(var clause in matchStmt.Clauses){
                var or_label = il_generator.DefineLabel();
                var and_label = il_generator.DefineLabel();
                context.CurrentOrTargetLabel = or_label;
                context.CurrentAndTargetLabel = and_label;
                clause.AcceptWalker(this, context);
                il_generator.MarkLabel(and_label);
            }

            il_generator.MarkLabel(context.CurrentJumpLabel);
            context.CurrentJumpLabel = prev_jump_label;

            // in case of this statement being the last one in the function
            if(matchStmt.Parent is BlockStatement block && block.Parent is FunctionDeclaration func && matchStmt.NextSibling == null){
                EmitDefaultValue(func.ReturnType);
                il_generator.Emit(OpCodes.Ret);
            }

            AscendScope();
            scope_counter = tmp_counter + 1;

            context.TemporaryVariable = parent_tmp_var;
            context.ContextAst = context_ast;

            return null;
        }

        public Type VisitThrowStatement(ThrowStatement throwStmt, CodeGeneratorContext context)
        {
            VisitObjectCreationExpression(throwStmt.CreationExpression, context);
            il_generator.Emit(OpCodes.Throw);
            return null;
        }

        public Type VisitTryStatement(TryStatement tryStmt, CodeGeneratorContext context)
        {
            // Start the try block
            il_generator.BeginExceptionBlock();

            VisitBlock(tryStmt.EnclosingBlock, context);

            foreach(var @catch in tryStmt.CatchClauses)
                VisitCatchClause(@catch, context);

            tryStmt.FinallyClause.AcceptWalker(this, context);
            il_generator.EndExceptionBlock();

            return null;
        }

        public Type VisitWhileStatement(WhileStatement whileStmt, CodeGeneratorContext context)
        {
            var break_target = il_generator.DefineLabel();
            var loop_start = il_generator.DefineLabel();
            var continue_target = il_generator.DefineLabel();
            break_targets.Add(break_target);
            continue_targets.Add(continue_target);

            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            if(!(whileStmt.Parent is DoWhileStatement))
                il_generator.Emit(OpCodes.Br, continue_target);

            var loop_start_offset = il_generator.ILOffset;
            il_generator.MarkLabel(loop_start);
            
            VisitBlock(whileStmt.Body, context);

            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;
            il_generator.MarkLabel(continue_target);
            whileStmt.Condition.AcceptWalker(this, context);
            context.OperationTypeOnIdentifier = prev_op_type;
            var loop_offset = il_generator.ILOffset - loop_start_offset;
            EmitBrTrue(loop_offset, loop_start);
            
            break_targets.RemoveAt(break_targets.Count - 1);
            continue_targets.RemoveAt(continue_targets.Count - 1);

            il_generator.MarkLabel(break_target);

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public Type VisitYieldStatement(YieldStatement yieldStmt, CodeGeneratorContext context)
        {
            throw new NotImplementedException();
        }

        public Type VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl, CodeGeneratorContext context)
        {
            context.ContextAst = varDecl;
            foreach(var variable in varDecl.Variables){
                // We have to care for only context.Additionals
                variable.AcceptWalker(this, context);
            }
            context.ContextAst = null;
            // A variable declaration statement may contain multiple declarations
            return null;
        }

        public Type VisitArrayInitializer(ArrayInitializerExpression arrayInitializer, CodeGeneratorContext context)
        {
            var native_type = CSharpCompilerHelpers.GetNativeType(arrayInitializer.TypePath);
            arrayInitializer.SizeExpression.AcceptWalker(this, context);
            il_generator.Emit(OpCodes.Newarr, native_type);
            return native_type.MakeArrayType();
        }

        public Type VisitAssignment(AssignmentExpression assignment, CodeGeneratorContext context)
        {
            Type result = null;
            if(assignment.Left is SequenceExpression lhs){    // see if `assignment` is a simultaneous assignment
                // expected form: a, b, ... = 1, 2, ...
                // evaluation order: we will see the left hand side and right hand side as a pair and evaluate them from left to right
                // that is, they will be evaluated as a = 1, b = 2, ...
                var rhs = (SequenceExpression)assignment.Right;
                if(rhs == null)
                    throw new GeneratorException("Expected a sequence expression!");

                context.RequestPropertyOrField = true;
                var prev_op_type = context.OperationTypeOnIdentifier;

                if(rhs.Items.Count == 1){
                    // If there is just 1 item on each side
                    // skip making temporary variables
                    MemberInfo prev_member = null;
                    if(assignment.Operator == OperatorType.Power){
                        var pow_method = typeof(Math).GetMethod("Pow");
                        if(result == null)
                            result = pow_method.ReturnType;

                        context.OperationTypeOnIdentifier = OperationType.Load;
                        EmitCallExpression(pow_method, new []{lhs.Items.First(), rhs.Items.First()}, null, context);
                    }else{
                        // We need to emit another array reference before we emit rhs
                        if(assignment.Operator != OperatorType.Assign){
                            context.OperationTypeOnIdentifier = OperationType.Set;
                            lhs.Items.First().AcceptWalker(this, context);
                            prev_member = context.PropertyOrField;
                        }

                        if(assignment.Operator == OperatorType.Assign)
                            context.OperationTypeOnIdentifier = OperationType.Set;
                        else
                            context.OperationTypeOnIdentifier = OperationType.Load;

                        var tmp = lhs.Items.First().AcceptWalker(this, context);
                        if(result == null)
                            result = tmp;

                        context.OperationTypeOnIdentifier = OperationType.Load;
                        var prev_local_builder = context.TargetLocalBuilder;
                        var prev_param_index = context.ParameterIndex;
                        if(prev_member == null)
                            prev_member = context.PropertyOrField;

                        rhs.Items.First().AcceptWalker(this, context);
                        context.TargetLocalBuilder = prev_local_builder;
                        context.ParameterIndex = prev_param_index;
                        context.PropertyOrField = prev_member;

                        switch(assignment.Operator){
                        case OperatorType.Plus:
                            il_generator.Emit(OpCodes.Add);
                            break;

                        case OperatorType.Minus:
                            il_generator.Emit(OpCodes.Sub);
                            break;

                        case OperatorType.Times:
                            il_generator.Emit(OpCodes.Mul);
                            break;

                        case OperatorType.Divide:
                            il_generator.Emit(OpCodes.Div);
                            break;

                        case OperatorType.Modulus:
                            il_generator.Emit(OpCodes.Rem);
                            break;

                        case OperatorType.Assign:
                            break;

                        default:
                            throw new InvalidOperationException($"Unknown operation: {assignment.Operator}!");
                        }
                    }

                    var is_method = assignment.Ancestors.Any(a => a is TypeDeclaration);
                    EmitSet(context.PropertyOrField, context.TargetLocalBuilder, context.ParameterIndex, result, is_method);
                }else{
                    var tmp_variables = new List<LocalBuilder>();
                    context.OperationTypeOnIdentifier = OperationType.Load;
                    foreach(var item in rhs.Items){
                        var type = item.AcceptWalker(this, context);
                        if(result == null)
                            result = type;

                        var tmp_variable = il_generator.DeclareLocal(type);
                        tmp_variables.Add(tmp_variable);

                        EmitSet(null, tmp_variable, -1, null);
                    }

                    context.OperationTypeOnIdentifier = OperationType.Set;
                    foreach(var pair in lhs.Items.Zip(tmp_variables, (l, t) => new {Lhs = l, TemporaryVariable = t})){
                        var type = pair.Lhs.AcceptWalker(this, context);
                        EmitLoadLocal(pair.TemporaryVariable, false);
                        EmitSet(context.PropertyOrField, context.TargetLocalBuilder, context.ParameterIndex, type);
                    }
                }

                context.OperationTypeOnIdentifier = prev_op_type;
            }else{
                // falls into composition branch
                // expected form: a = b = ...
                context.RequestPropertyOrField = true;
                context.PropertyOrField = null;
                assignment.Right.AcceptWalker(this, context);

                var prev_op_type = context.OperationTypeOnIdentifier;
                context.OperationTypeOnIdentifier = OperationType.Set;
                result = assignment.Left.AcceptWalker(this, context);
                context.OperationTypeOnIdentifier = prev_op_type;

                //EmitSet(context.PropertyOrField, context.TargetLocalBuilder, result);
            }

            context.RequestPropertyOrField = false;
            context.PropertyOrField = null;
            context.TargetLocalBuilder = null;
            context.ParameterIndex = -1;

            return result;
        }

        public Type VisitBinaryExpression(BinaryExpression binaryExpr, CodeGeneratorContext context)
        {
            Type result = null;

            var parent_complex_condition_expr = IsRootConditionExpression(binaryExpr) && context.InspectingComplexConditionExpression;
            var parent_seen_condition_and = IsRootConditionExpression(binaryExpr) && context.HasSeenConditionAnd;
            var parent_seen_condition_or = IsRootConditionExpression(binaryExpr) && context.HasSeenConditionOr;
            //var parent_or_target = IsRootConditionExpression(binaryExpr) ? context.CurrentOrTargetLabel : new Label();

            context.RequestPropertyOrField = true;
            if(IsRootConditionExpression(binaryExpr)){
                context.InspectingComplexConditionExpression = false;
                context.HasSeenConditionAnd = false;
                context.HasSeenConditionOr = false;
            }

            if(binaryExpr.Operator == OperatorType.Power){
                var pow_method = typeof(Math).GetMethod("Pow");
                EmitCallExpression(pow_method, new []{binaryExpr.Left, binaryExpr.Right}, null, context);
                context.RequestPropertyOrField = false;
                context.PropertyOrField = null;

                result = pow_method.ReturnType;
            }else{
                var preferable_type = DetermineType(binaryExpr.PreferredType, context);
                var lhs_type = binaryExpr.Left.AcceptWalker(this, context);
                if(CSharpCompilerHelpers.IsNumericalType(preferable_type) && lhs_type != preferable_type)
                    EmitCast(lhs_type, preferable_type);

                Label? label = null;
                if(!context.HasSeenConditionOr && binaryExpr.Operator == OperatorType.ConditionalOr){
                    label = il_generator.DefineLabel();
                    context.CurrentOrTargetLabel = label.Value;
                }
                if(!context.HasSeenConditionAnd && binaryExpr.Operator == OperatorType.ConditionalAnd){
                    label = il_generator.DefineLabel();
                    context.CurrentAndTargetLabel = label.Value;
                }

                if((binaryExpr.Operator == OperatorType.ConditionalOr || binaryExpr.Operator == OperatorType.ConditionalAnd) && IsRootConditionExpression(binaryExpr))
                    context.InspectingComplexConditionExpression = IsInspectingComplexConditionExpression(binaryExpr);

                if(label.HasValue && context.InspectingComplexConditionExpression)
                    EmitBinaryOpInMiddle(binaryExpr.Operator, label);

                if(binaryExpr.Operator == OperatorType.ConditionalOr)
                    context.HasSeenConditionOr = true;
                else if(binaryExpr.Operator == OperatorType.ConditionalAnd)
                    context.HasSeenConditionAnd = true;
                
                var rhs_type = binaryExpr.Right.AcceptWalker(this, context);
                if(CSharpCompilerHelpers.IsNumericalType(preferable_type) && rhs_type != preferable_type)
                    EmitCast(rhs_type, preferable_type);

                context.RequestPropertyOrField = false;
                context.PropertyOrField = null;
                preferable_type = (preferable_type.IsPrimitive || binaryExpr.Operator == OperatorType.Equality || binaryExpr.Operator == OperatorType.InEquality) ? null : preferable_type;

                EmitBinaryOp(binaryExpr.Operator, preferable_type, context.InspectingComplexConditionExpression, label);
                if(context.HasSeenConditionAnd && context.HasSeenConditionOr && IsRootConditionExpression(binaryExpr)){
                    var bool_variable = il_generator.DeclareLocal(typeof(bool));
                    il_generator.MarkLabel(context.CurrentOrTargetLabel);
                    il_generator.Emit(OpCodes.Stloc, bool_variable);
                    il_generator.Emit(OpCodes.Ldloc, bool_variable);
                }

                result = lhs_type;
            }

            if(IsRootConditionExpression(binaryExpr)){
                context.InspectingComplexConditionExpression = parent_complex_condition_expr;
                context.HasSeenConditionAnd = parent_seen_condition_and;
                context.HasSeenConditionOr = parent_seen_condition_or;
            }

            return result;
        }

        public Type VisitCallExpression(CallExpression call, CodeGeneratorContext context)
        {
            var parent_args = context.ArgumentTypes;
            var prev_method = context.Method;
            var prev_inspecting_call = context.IsInspectingCallExpression;
            context.IsInspectingCallExpression = true;
            context.ArgumentTypes = call.OverloadSignature.IsNull ? null : ConvertToNativeTypes(call.OverloadSignature.Parameters, context);

            if(call.Target is PathExpression){
                // This means we are calling a module function
                context.TargetType = (context.ModuleLazyTypeBuilder != null) ? context.ModuleLazyTypeBuilder.InterfaceType : context.TargetType;
            }

            context.RequestMethod = true;
            call.Target.AcceptWalker(this, context);
            context.RequestMethod = false;
            context.ArgumentTypes = parent_args;

            // context.Method could be changed
            var method = context.Method;
            context.Method = prev_method;
            context.IsInspectingCallExpression = prev_inspecting_call;

            var result = EmitCallExpression(method, call.Arguments, call.TypeArguments, context, call.CallAsExtension, call.Target);
            // This code is needed because otherwise self types in generic types will cause trouble
            result = TreatTypeBuilderType(context.LazyTypeBuilder.Name, result);
            context.Method = null;

            context.TargetType = result;

            // Ignore the return value
            if(method.ReturnType != typeof(void) && call.Ancestors.TakeWhile(a => !(a is BlockStatement) && !(a is CallExpression))
                                                        .Any(a => a is ExpressionStatement) && call.Ancestors.TakeWhile(a => !(a is ExpressionStatement))
                                                        .All(a => !(a is AssignmentExpression))){
                il_generator.Emit(OpCodes.Pop);
            }

            return result;
        }

        public Type VisitCastExpression(CastExpression castExpr, CodeGeneratorContext context)
        {
            var original_type = castExpr.Target.AcceptWalker(this, context);
            var to_type = CSharpCompilerHelpers.GetNativeTypeWithSelf(castExpr.ToType, context.LazyTypeBuilder.Name);
            EmitCast(original_type, to_type);
            context.TargetType = to_type;

            return to_type;
        }

        public Type VisitCatchClause(CatchClause catchClause, CodeGeneratorContext context)
        {
            var ident = catchClause.Identifier;
            var exception_type = CSharpCompilerHelpers.GetNativeType((ident.Type.IdentifierNode.Type != null) ? ident.Type.IdentifierNode.Type : ident.Type);
            var exception_builder = il_generator.DeclareLocal(exception_type);
            AddSymbol(ident, new ExpressoSymbol{LocalBuilder = exception_builder});

            il_generator.BeginCatchBlock(exception_type);
            EmitSet(null, exception_builder, -1, null);
            VisitBlock(catchClause.Body, context);
            return null;
        }

        public Type VisitClosureLiteralExpression(ClosureLiteralExpression closure, CodeGeneratorContext context)
        {
            // This AST creates the following class
            // class <>__Closure<id>
            // {
            //     Func|Action __ApplyMethod;
            //     <the Closure method>
            // }
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var closure_type_builder = new LazyTypeBuilder(context.ModuleBuilder, CSharpCompilerHelpers.HiddenMemberPrefix + "Closure`" + ClosureId++,
                                                           TypeAttributes.Class, typeof(object), false, false);

            var param_types = closure.Parameters
                                     .Select((p, index) => {
                context.ParameterIndex = index + 1;
                return p.AcceptWalker(this, context);
            }).ToArray();
            context.ParameterIndex = -1;

            var prev_context_closure_expr = context.ContextClosureLiteral;
            context.ContextClosureLiteral = closure;

            // retrievingTypeArgs and shouldSubstituteTypeArguments should be true because it can treat generic types
            var return_type = CSharpCompilerHelpers.GetNativeType(closure.ReturnType, true, true);

            closure_type_builder.DefineMethod(ClosureMethodName, MethodAttributes.Public, return_type, param_types);

            var field_idents = closure.LiftedIdentifiers
                                      .Select(ident => new {ident.Name, Type = CSharpCompilerHelpers.GetNativeType(ident.Type)});
            foreach(var ctor_param in field_idents)
                closure_type_builder.DefineField(ctor_param.Name, ctor_param.Type, false);

            var param_ast_types = closure.Parameters.Select(p => p.ReturnType.Clone());
            var closure_func_type = AstType.MakeFunctionType("closure", closure.ReturnType.Clone(), param_ast_types);
            // retrievingTypeArgs should be true because it can treat generic types
            var closure_native_type = CSharpCompilerHelpers.GetNativeType(closure_func_type, true);
            var delegate_ctor = closure_native_type.GetConstructors().First();

            var closure_delegate_field = closure_type_builder.DefineField(ClosureDelegateName, closure_native_type, true);
            var interface_type = closure_type_builder.CreateInterfaceType();

            var prev_context_closure = context.ContextClosureType;
            context.ContextClosureType = interface_type;

            var closure_method_builder = closure_type_builder.GetMethodBuilder(ClosureMethodName);
            var prev_il_generator = il_generator;
            il_generator = closure_method_builder.GetILGenerator();
            var parent_seq_points = context.PDBGenerator.StartClosureDefinition();
            VisitBlock(closure.Body, context);
            context.PDBGenerator.EndClosureDefinition(parent_seq_points);
            if(!(closure.Body.Statements.Last() is ReturnStatement))
                il_generator.Emit(OpCodes.Ret);

            il_generator = prev_il_generator;

            var ctor_il = closure_type_builder.GetILGeneratorForFieldInit(closure_delegate_field);
            var closure_call_method = interface_type.GetMethod(ClosureMethodName);

            ctor_il.Emit(OpCodes.Nop);
            ctor_il.Emit(OpCodes.Ldarg_0);
            ctor_il.Emit(OpCodes.Ldarg_0);
            ctor_il.Emit(OpCodes.Ldftn, closure_call_method);
            ctor_il.Emit(OpCodes.Newobj, delegate_ctor);
            ctor_il.Emit(OpCodes.Stfld, closure_delegate_field);
            var closure_type = closure_type_builder.CreateType();

            context.ContextClosureType = prev_context_closure;
            context.ContextClosureLiteral = prev_context_closure_expr;

            foreach(var lifted_ident in closure.LiftedIdentifiers)
                VisitIdentifier(lifted_ident, context);
            
            var ctor = closure_type.GetConstructors().First();
            il_generator.Emit(OpCodes.Newobj, ctor);

            var closure_call_target = closure_type.GetField(ClosureDelegateName);
            il_generator.Emit(OpCodes.Ldfld, closure_call_target);

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public Type VisitComprehensionExpression(ComprehensionExpression comp, CodeGeneratorContext context)
        {
            //TODO: implement it
            var generator = comp.Item.AcceptWalker(this, context);
            //context.ContextExpression = generator;
            var type = comp.Body.AcceptWalker(this, context);
            return type;
        }

        public Type VisitComprehensionForClause(ComprehensionForClause compFor, CodeGeneratorContext context)
        {
            compFor.Left.AcceptWalker(this, context);
            compFor.Target.AcceptWalker(this, context);
            compFor.Body.AcceptWalker(this, context);
            return null;
        }

        public Type VisitComprehensionIfClause(ComprehensionIfClause compIf, CodeGeneratorContext context)
        {
            // TODO: implement it
            /*if(compIf.Body.IsNull)      //[generator...if Condition] -> ...if(Condition) seq.Add(generator);
                return CSharpExpr.IfThen(compIf.Condition.AcceptWalker(this, context), context.ContextExpression);
            else                        //[...if Condition...] -> ...if(Condition){...}
                return CSharpExpr.IfThen(compIf.Condition.AcceptWalker(this, context), compIf.Body.AcceptWalker(this, context));
                */
            return null;
        }

        public Type VisitConditionalExpression(ConditionalExpression condExpr, CodeGeneratorContext context)
        {
            var true_label = il_generator.DefineLabel();
            var false_label = il_generator.DefineLabel();
            var end_label = il_generator.DefineLabel();

            condExpr.Condition.AcceptWalker(this, context);
            il_generator.Emit(OpCodes.Brfalse, false_label);

            il_generator.MarkLabel(true_label);
            var result = condExpr.TrueExpression.AcceptWalker(this, context);
            il_generator.Emit(OpCodes.Br_S, end_label);

            il_generator.MarkLabel(false_label);

            condExpr.FalseExpression.AcceptWalker(this, context);
            il_generator.MarkLabel(end_label);

            return result;
        }

        public Type VisitFinallyClause(FinallyClause finallyClause, CodeGeneratorContext context)
        {
            il_generator.BeginFinallyBlock();
            VisitBlock(finallyClause.Body, context);
            return null;
        }

        public Type VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue, CodeGeneratorContext context)
        {
            var ident = keyValue.KeyExpression as PathExpression;
            if(ident != null){
                if(context.TargetType == null)
                    throw new GeneratorException("Can not create an object of UNKNOWN!");

                /*var field = context.TargetType.GetField(ident.AsIdentifier.Name);
                if(field == null){
                    throw new EmitterException(
                        "Type `{0}` does not have the field `{1}`.",
                        context.TargetType, ident.AsIdentifier.Name
                    );
                }
                context.Field = field;*/

                var type = keyValue.ValueExpression.AcceptWalker(this, context);
                return type;
            }else{
                // In a dictionary literal, the key can be any expression that is evaluated
                // to a hashable object.
                keyValue.KeyExpression.AcceptWalker(this, context);
                keyValue.ValueExpression.AcceptWalker(this, context);
                return null;
            }
        }

        public Type VisitLiteralExpression(LiteralExpression literal, CodeGeneratorContext context)
        {
            EmitObject(literal.Value);
            return literal.Value.GetType();
        }

        public Type VisitIdentifier(Identifier ident, CodeGeneratorContext context)
        {
            if(context.ContextClosureType != null && context.ContextClosureLiteral.LiftedIdentifiers.Any(i => i.Name == ident.Name)){
                var lifted_field = context.ContextClosureType.GetField(ident.Name);
                LoadArg(0);
                EmitLoadField(lifted_field);
                return lifted_field.FieldType;
            }

            var symbol = GetRuntimeSymbol(ident);
            if(symbol != null){
                if(symbol.LocalBuilder != null){
                    var local_type = symbol.LocalBuilder.LocalType;
                    if(context.RequestType){
                        var type_table = symbol_table.GetTypeTable(ident.Type.Name);
                        // With enum variants, we want the enum type as TargetType to access its members, not the variant type
                        if(local_type.Name != ident.Type.Name && type_table != null && type_table.TypeKind == ClassType.Enum && ident.Type is SimpleType simple){
                            // ident.Type.TypeArguments contains `KeyValueType`s
                            var type_args = simple.TypeArguments.OfType<KeyValueType>().Select(ta => ta.ValueType.Clone());
                            var enum_type = AstType.MakeSimpleType(simple.Name, type_args);
                            context.TargetType = CSharpCompilerHelpers.GetNativeTypeWithSelf(enum_type, context.LazyTypeBuilder.Name);
                        }else{
                            context.TargetType = local_type;
                        }

                        context.ConstructorType = local_type;
                    }

                    if(context.OperationTypeOnIdentifier == OperationType.Load){
                        if(!context.ExpectsReferenceIsPrimitive)
                            context.ExpectsReference = context.ExpectsReference && local_type.IsValueType && !local_type.IsPrimitive;

                        if(local_type.IsValueType && !local_type.IsPrimitive && ident.Ancestors.Any(a => a is MemberReferenceExpression))
                            context.ExpectsReference = true;

                        EmitLoadLocal(symbol.LocalBuilder, context.ExpectsReference);

                        // when local_type is a primitive type and it's a member reference expression, we should first box it
                        if(local_type.IsPrimitive && ident.Ancestors.Any(a => a is MemberReferenceExpression))
                            il_generator.Emit(OpCodes.Box, local_type);
                    }

                    if(symbol.LocalBuilder.LocalType.Name.StartsWith("Func", StringComparison.CurrentCulture)
                    || symbol.LocalBuilder.LocalType.Name.StartsWith("Action", StringComparison.CurrentCulture)){
                        context.Method = symbol.LocalBuilder.LocalType.GetMethod("Invoke");
                    }

                    context.TargetLocalBuilder = symbol.LocalBuilder;
                    return symbol.LocalBuilder.LocalType;
                }else if(symbol.Parameter != null){
                    var parameter = symbol.Parameter;
                    if(context.RequestType)
                        context.TargetType = parameter.Type;

                    if(context.OperationTypeOnIdentifier == OperationType.Load && symbol.ParameterIndex != -1 || parameter.IsByRef)
                        LoadArg(symbol.ParameterIndex);

                    if(parameter.Type.Name.StartsWith("Func", StringComparison.CurrentCulture)
                        || parameter.Type.Name.StartsWith("Action", StringComparison.CurrentCulture)){
                        var param_type = parameter.Type;
                        if(param_type.ContainsGenericParameters && !param_type.IsGenericTypeDefinition){
                            // Expects Func<U> or Action<U> where U is a type parameter
                            var generic_param_type = param_type.GetGenericTypeDefinition();
                            var generic_invoke_method = generic_param_type.GetMethod("Invoke");
                            context.Method = TypeBuilder.GetMethod(param_type, generic_invoke_method);
                        }else{
                            context.Method = param_type.GetMethod("Invoke");
                        }
                    }

                    context.ParameterIndex = symbol.ParameterIndex;
                    return parameter.Type;
                }else if(context.RequestPropertyOrField && symbol.PropertyOrField != null){
                    context.PropertyOrField = symbol.PropertyOrField;
                    if(context.RequestType){
                        // In order to retrieve module variable's members
                        context.TargetType = (symbol.PropertyOrField is PropertyInfo prop) ? prop.PropertyType :
                                             (symbol.PropertyOrField is FieldInfo field) ? field.FieldType : null;
                    }

                    return (symbol.PropertyOrField is PropertyInfo property) ? property.PropertyType : ((FieldInfo)symbol.PropertyOrField).FieldType;
                }else if(context.RequestType && symbol.Type != null){
                    context.TargetType = symbol.Type;
                    if(context.TargetType.GetConstructors().Any()){
                        // context.TargetType could be a static type
                        context.Constructor = context.TargetType.GetConstructors().Last();
                    }

                    return symbol.Type;
                }else if(context.RequestMethod){
                    if(symbol.Method == null)
                        throw new GeneratorException($"The native symbol '{ident.Name}' isn't defined.");

                    context.Method = symbol.Method;
                    return null;
                }else{
                    throw new GeneratorException("I can't guess what you want.");
                }
            }else{
                if(context.TargetType != null && !context.TargetType.IsGenericParameter && context.TargetType.IsEnum){
                    var enum_field = context.TargetType.GetField(ident.Name);
                    context.PropertyOrField = enum_field ?? throw new GeneratorException($"It is found that the native symbol '{ident.Name}' doesn't represent an enum field on `{context.TargetType}`.");
                    AddSymbol(ident, new ExpressoSymbol{PropertyOrField = enum_field});

                    return null;
                }else if(context.TargetType != null && context.RequestMethod){
                    // For methods or functions in external modules
                    // We regard types containing namespaces as types from other assemblies
                    var method = CSharpCompilerHelpers.GetMethod(context.TargetType, ident, context.ArgumentTypes);
                    if(method == null){
                        if(!context.RequestPropertyOrField)
                            throw new GeneratorException($"It is found that the native symbol '{ident.Name}' doesn't represent a method on `{context.TargetType}`.");

                        var field = context.TargetType.GetField(ident.Name);
                        if(field == null){
                            var property = context.TargetType.GetProperty(ident.Name);
                            if(property == null)
                                throw new GeneratorException($"It is found that the native symbol '{ident.Name}' doesn't resolve to either a field, a property or a method on `{context.TargetType}` with args({ExpressoCompilerHelpers.StringifyList(context.ArgumentTypes)}).");

                            context.PropertyOrField = property;
                            AddSymbol(ident, new ExpressoSymbol{PropertyOrField = property});
                            return null;
                        }

                        context.PropertyOrField = field;
                        AddSymbol(ident, new ExpressoSymbol{PropertyOrField = field});
                    }
                    
                    context.Method = method;
                    // Don't add a method symbol so that we can find other overloads of it
                    //AddSymbol(ident, new ExpressoSymbol{Method = method});
                    return null;
                }else{
                    throw new GeneratorException($"It is found that the native symbol '{ident.Name}' isn't defined: {ident.StartLocation}");
                }
            }
        }

        public Type VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq, CodeGeneratorContext context)
        {
            var intseq_type = typeof(ExpressoIntegerSequence);
            var intseq_ctor = intseq_type.GetConstructor(new []{typeof(int), typeof(int), typeof(int), typeof(bool)});
       
            intSeq.Start.AcceptWalker(this, context);
            intSeq.End.AcceptWalker(this, context);
            intSeq.Step.AcceptWalker(this, context);
            EmitObject(intSeq.UpperInclusive);

            if(context.RequestMethod && context.Method == null)
                context.Method = intseq_type.GetMethod("Includes");

            il_generator.Emit(OpCodes.Newobj, intseq_ctor);
            return intseq_type;      //new ExpressoIntegerSequence(Start, End, Step, UpperInclusive)
        }

        public Type VisitIndexerExpression(IndexerExpression indexExpr, CodeGeneratorContext context)
        {
            var prev_op = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;
            var target_type = indexExpr.Target.AcceptWalker(this, context);

            // We need to call ToArray here so that the enumerable will be evaluated just once
            var arg_types = indexExpr.Arguments.Select(a => a.AcceptWalker(this, context)).ToArray();
            context.OperationTypeOnIdentifier = prev_op;
            context.TargetLocalBuilder = null;

            if(arg_types.Count() == 1 && arg_types.First().Name == "ExpressoIntegerSequence"){
                var seq_type = target_type;
                var elem_type = seq_type.IsArray ? seq_type.GetElementType() : seq_type.GenericTypeArguments[0];
                var slice_type = typeof(Slice<,>).MakeGenericType(new []{seq_type, elem_type});
                var ctor = slice_type.GetConstructor(new []{seq_type, typeof(ExpressoIntegerSequence)});

                il_generator.Emit(OpCodes.Newobj, ctor);
                return slice_type;   // a[ExpressoIntegerSequence]
            }

            // We don't need to set here because Assignment calls this only when it should read values
            if(target_type.IsArray){
                var elem_type = target_type.GetElementType();
                if(context.OperationTypeOnIdentifier == OperationType.Load)
                    EmitLoadElem(elem_type);

                return elem_type;
            }else{
                PropertyInfo property_info;
                try{
                    property_info = target_type.GetProperty("Item");
                }
                catch(AmbiguousMatchException){
                    property_info = target_type.GetProperty("Item", arg_types);
                }
                if(context.OperationTypeOnIdentifier == OperationType.Load)
                    il_generator.Emit(OpCodes.Callvirt, property_info.GetMethod);
      
                context.PropertyOrField = property_info;
                // For chaining
                context.TargetType = property_info.GetMethod.ReturnType;

                return property_info.GetMethod.ReturnType;
            }
        }

        public Type VisitMemberReference(MemberReferenceExpression memRef, CodeGeneratorContext context)
        {
            // In Expresso, a member access can be resolved either to a field reference or an (instance and static) method call
            var prev_op_type = context.OperationTypeOnIdentifier;
            if(context.OperationTypeOnIdentifier != OperationType.None)
                context.OperationTypeOnIdentifier = OperationType.Load;

            context.ExpectsReference = true;
            context.ExpectsReferenceIsPrimitive = false;
            memRef.Target.AcceptWalker(this, context);
            context.ExpectsReference = false;
            context.OperationTypeOnIdentifier = prev_op_type;
            context.RequestPropertyOrField = true;
            context.RequestMethod = true;
            var prev_member = context.PropertyOrField;

            VisitIdentifier(memRef.Member, context);
            context.RequestPropertyOrField = false;
            context.RequestMethod = false;

            if(context.Method != null){
                return context.Method.ReturnType;    // Parent should be a CallExpression
            }else if(context.PropertyOrField is PropertyInfo property){
                if(context.OperationTypeOnIdentifier == OperationType.Load){
                    context.PropertyOrField = prev_member;
                    EmitCall(property.GetMethod);
                }
                context.TargetType = property.PropertyType;
                return property.GetMethod.ReturnType;
            }else{
                var field = (FieldInfo)context.PropertyOrField;
                context.TargetType = field.FieldType;
                if(context.OperationTypeOnIdentifier != OperationType.None && field.DeclaringType.IsEnum){
                    context.PropertyOrField = prev_member;

                    // Enum value could be byte, uint or other types
                    EmitObject(field.GetRawConstantValue());
                }else if(context.OperationTypeOnIdentifier == OperationType.Load){
                    context.PropertyOrField = prev_member;
                    if(field.Attributes.HasFlag(FieldAttributes.Literal)){
                        var value = field.GetValue(null);
                        EmitObject(value);
                    }else{
                        EmitLoadField(field, context.IsInspectingCallExpression);
                    }
                }
                // This code is needed because otherwise self types in generic types will cause trouble
                return TreatTypeBuilderType(context.LazyTypeBuilder.Name, field.FieldType);
            }
        }

        public Type VisitPathExpression(PathExpression pathExpr, CodeGeneratorContext context)
        {
            if(pathExpr.Items.Count == 1){
                context.RequestType = true;
                context.RequestMethod = true;
                context.RequestPropertyOrField = true;
                context.PropertyOrField = null;
                context.Method = null;
                context.TargetType = null;

                var type = VisitIdentifier(pathExpr.AsIdentifier, context);
                context.RequestType = false;
                context.RequestMethod = false;
                context.RequestPropertyOrField = false;

                // Assume m is a module variable and it's for let b = m;
                if(context.PropertyOrField != null && context.PropertyOrField is FieldInfo field && field.IsStatic && context.OperationTypeOnIdentifier == OperationType.Load){
                    il_generator.Emit(OpCodes.Ldsfld, field);
                    context.PropertyOrField = null;
                    return field.FieldType;
                }else{
                    if(pathExpr.TypeArguments.Any()){
                        var type_args = pathExpr.TypeArguments.Select(ta => CSharpCompilerHelpers.GetNativeType(ta, true));
                        type = type.MakeGenericType(type_args.ToArray());
                        context.TargetType = type;
                    }

                    return type;
                }
            }

            context.TargetType = null;
            // We do this because the items in a path should already be resolved
            // and a path with more than 1 item can only refer to an external module's function
            // or an enum member
            var last_item = pathExpr.Items.Last();
            var native_symbol = GetRuntimeSymbol(last_item);
            if(native_symbol != null){
                context.TargetType = native_symbol.Type;
                context.Method = native_symbol.Method;
                context.PropertyOrField = native_symbol.PropertyOrField;
            }else{
                throw new GeneratorException($"It is found that the runtime symbol '{last_item.Name}' doesn't represent anything.");
            }

            if(native_symbol.PropertyOrField != null && native_symbol.PropertyOrField is FieldInfo field2){
                context.PropertyOrField = null;

                var first_item = pathExpr.Items.First();
                var type_table = symbol_table.GetTypeTable(first_item.Name);
                // Guard let flag = some_enum == SomeEnum::A;
                if(type_table != null && type_table.TypeKind == ClassType.Enum && !pathExpr.Ancestors.Any(a => a is BinaryExpression) &&
                    (pathExpr.Ancestors.Any(a => a is VariableInitializer) || pathExpr.Parent is CallExpression)){
                    // This code is needed for assgining raw value enums
                    var field_type = field2.FieldType;
                    var wrapper_symbol = symbol_table.GetTypeSymbolInAnyScope(first_item.Name);
                    var wrapper_type = GetRuntimeSymbol(wrapper_symbol).Type;
                    var ctor = wrapper_type.GetConstructors().First();
                    var raw_value = field2.GetRawConstantValue();
                    EmitObject(raw_value);
                    il_generator.Emit(OpCodes.Newobj, ctor);
                    return wrapper_type;
                }else{
                    if(field2.Attributes.HasFlag(FieldAttributes.Literal))
                        EmitObject(field2.GetRawConstantValue());
                    else
                        il_generator.Emit(OpCodes.Ldsfld, field2);

                    return field2.FieldType;
                }
            }
            // On .NET environment, a path item is mapped to
            // Assembly::[Module]::{Class}
            // In reverse, an Expresso item can be mapped to the .NET type system as
            // Module.{Class}
            // Usually modules are converted to assemblies on themselves
            /*foreach(var ident in pathExpr.Items){
                if(context.TargetType == null){
                    var native_symbol = GetNativeSymbol(ident);
                    if(native_symbol != null)
                        context.TargetType = native_symbol.Type;
                    else
                        throw new EmitterException("Type `{0}` isn't defined!", ident.Name);
                }else if(context.TargetType != null){
                    // For methods or functions in external modules
                    var method_name = /*context.TargetType.FullName.StartsWith("System", StringComparison.CurrentCulture) ? CSharpCompilerHelper.ConvertToPascalCase(ident.Name);
                                             //: ident.Name;
                    var method = context.TargetType.GetMethod(method_name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
                    if(method == null)
                        throw new EmitterException("It is found that the native symbol '{0}' doesn't represent a method", ident.Name);

                    context.Method = method;
                    Symbols.Add(ident.IdentifierId, new ExpressoSymbol{Method = method});
                    return null;
                }else{
                    var type = context.TargetType.GetNestedType(ident.Name);
                    if(type != null){
                        context.TargetType = type;
                        if(context.Constructor == null)
                            context.Constructor = type.GetConstructors().Last();
                    }else{
                        throw new EmitterException("A nested type `{0}` isn't defined in Type `{1}`", ident.Name, context.TargetType.FullName);
                    }
                }
            }*/

            return null;
        }

        public Type VisitParenthesizedExpression(ParenthesizedExpression parensExpr, CodeGeneratorContext context)
        {
            return parensExpr.Expression.AcceptWalker(this, context);
        }

        public Type VisitObjectCreationExpression(ObjectCreationExpression creation, CodeGeneratorContext context)
        {
            context.TargetType = null;
            creation.TypePath.AcceptWalker(this, context);

            if(creation.CtorType.IsNull && creation.TypePath is MemberType member){
                // Construct an enum variant
                var enum_table = symbol_table.GetTypeTable(member.Target.Name);
                var variant_name = member.ChildType.Name;
                var variant_symbol = enum_table.GetSymbol(variant_name);
                var field_info = context.TargetType.GetField(CSharpCompilerHelpers.HiddenMemberPrefix + variant_name);
                var field_num = context.TargetType.GetFields()
                                       .Select((fld, index) => new {Index = index, Field = fld})
                                       .Where(arg => field_info == arg.Field)
                                       .Select(arg => arg.Index)
                                       .First();

                var field_type = field_info.FieldType;
                var type = field_type.GetGenericArguments().Any() ? typeof(Tuple) : typeof(Unit);
                var create_method = type.GetMethods()
                                        .First(m => m.Name == "Create" && m.GetParameters().Length == field_type.GetGenericArguments().Length);
                Type[] generic_args;
                if(field_type.ContainsGenericParameters && variant_symbol.Type is SimpleType variant_type && variant_type.IdentifierNode.Type is SimpleType real_variant_type){
                    // The variant has generic type parameters
                    generic_args = creation.TypeArguments
                                           .Where(keyvalue => real_variant_type.TypeArguments.Any(ta => keyvalue.KeyType.Name == ta.Name))
                                           .Select(keyvalue => DetermineType(keyvalue.ValueType, context))
                                           .ToArray();
                }else{
                    generic_args = field_type.GenericTypeArguments;
                }
                create_method = create_method.IsGenericMethod && generic_args.Any() && generic_args.All(ga => ga != null) ? create_method.MakeGenericMethod(generic_args)
                    : create_method;

                if(creation.TypeArguments.Any()){
                    var concrete_type_args = creation.TypeArguments.Select(ta => DetermineType(ta.ValueType, context)).ToArray();
                    context.ConstructorType = context.ConstructorType.MakeGenericType(concrete_type_args);
                }

                var ctor = CSharpCompilerHelpers.GetConstructor(context.ConstructorType);
                var prev_op_type = context.OperationTypeOnIdentifier;
                context.OperationTypeOnIdentifier = OperationType.Load;
                var parameters = ctor.GetParameters();
                foreach(var pair in Enumerable.Range(0, parameters.Length).Zip(parameters, (lhs, rhs) => new {Index = lhs, Parameter = rhs})){
                    if(pair.Index != field_num){
                        il_generator.Emit(OpCodes.Ldnull);
                        EmitCast(typeof(object), pair.Parameter.ParameterType);
                    }else{
                        context.RequestPropertyOrField = true;
                        foreach(var item in creation.Items)
                            item.AcceptWalker(this, context);
                        
                        il_generator.Emit(OpCodes.Call, create_method);
                    }
                }
                context.OperationTypeOnIdentifier = prev_op_type;
                il_generator.Emit(OpCodes.Newobj, ctor);
                
                return context.ConstructorType;
            }

            //if(creation.TypeArguments.Any())
            //    context.TargetType = context.TargetType.MakeGenericType(creation.TypeArguments.Select(ta => CSharpCompilerHelpers.GetNativeType(ta.ValueType)).ToArray());

            // Don't report TargetType missing error because TypeChecker has already reported it
            //if(context.TargetType == null)
            //    throw new EmitterException("")
            var arg_types =
                from p in creation.CtorType.Parameters
                select DetermineType(p, context);
            var ctor_type = context.ConstructorType;
            var target_ctor = ctor_type.GetConstructor(arg_types.ToArray());
            if(target_ctor == null){
                throw new GeneratorException(
                    $"No constructors found for the path `{creation.TypePath}` with arguments {CSharpCompilerHelpers.ExpandContainer(arg_types.ToArray())} on {ctor_type}",
                    creation
                );
            }
            context.Constructor = target_ctor;

            if(context.OperationTypeOnIdentifier == OperationType.None)
                return target_ctor.DeclaringType;
            
            var formal_params = target_ctor.GetParameters();
            var prev_op_type2 = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;
            context.RequestPropertyOrField = true;
            foreach(var item in creation.Items)
                item.AcceptWalker(this, context);

            context.OperationTypeOnIdentifier = prev_op_type2;
            context.RequestPropertyOrField = false;

            il_generator.Emit(OpCodes.Newobj, target_ctor);
            return ctor_type;
        }

        public Type VisitSequenceInitializer(SequenceInitializer seqInitializer, CodeGeneratorContext context)
        {
            var obj_type = seqInitializer.ObjectType;
            var seq_type = CSharpCompilerHelpers.GetContainerType(obj_type);
            // If this node represents a dictionary literal
            // context.Constructor will get set the appropriate constructor method.
            context.Constructor = null;

            if(seq_type == typeof(Array)){
                var first_elem = seqInitializer.Items.FirstOrDefault();
                if(first_elem is IntegerSequenceExpression){
                    foreach(var item in seqInitializer.Items)
                        item.AcceptWalker(this, context);
                    
                    var array_create_method = typeof(ExpressoIntegerSequence).GetMethod("CreateArrayFromIntSeq");
                    il_generator.Emit(OpCodes.Call, array_create_method);
                    return typeof(int[]);
                }else{
                    var elem_type = DetermineType(obj_type.TypeArguments.First(), context);
                    EmitNewArray(elem_type, seqInitializer.Items, item => {
                        item.AcceptWalker(this, context);
                    });
                    return elem_type.MakeArrayType();
                }
            }else if(seq_type == typeof(List<>)){
                var first_elem = seqInitializer.Items.FirstOrDefault();
                if(first_elem is IntegerSequenceExpression){
                    foreach(var item in seqInitializer.Items)
                        item.AcceptWalker(this, context);
                    
                    var list_create_method = typeof(ExpressoIntegerSequence).GetMethod("CreateListFromIntSeq");
                    il_generator.Emit(OpCodes.Call, list_create_method);
                    return typeof(List<int>);
                }else{
                    var elem_type = DetermineType(obj_type.TypeArguments.First(), context);
                    var generic_type = EmitListInitForList(elem_type, seqInitializer.Items, context);
                    return generic_type;
                }
            }else if(seq_type == typeof(Dictionary<,>)){
                var key_type = DetermineType(obj_type.TypeArguments.FirstOrNullObject(), context);
                var value_type = DetermineType(obj_type.TypeArguments.LastOrNullObject(), context);

                var generic_type = EmitListInitForDictionary(key_type, value_type, seqInitializer.Items.OfType<KeyValueLikeExpression>(), context);
                return generic_type;
            }else if(seq_type == typeof(Tuple)){
                var child_types = new List<Type>();
                foreach(var item in seqInitializer.Items){
                    var tmp = item.AcceptWalker(this, context);
                    child_types.Add(tmp);
                }
                var ctor_method = typeof(Tuple).GetMethod("Create", child_types.ToArray());
                il_generator.Emit(OpCodes.Call, ctor_method);
                return CSharpCompilerHelpers.GuessTupleType(child_types);
            }else{
                throw new GeneratorException("Can not emit code.");
            }
        }

        public Type VisitMatchClause(MatchPatternClause matchClause, CodeGeneratorContext context)
        {
            // Since we can't translate the pattern matching directly to IL
            // we have to translate it into equivalent expressions
            // e.g. match a {
            //          1 => print("1");
            //          2 | 3 => print("2 or 3");
            //          _ => print("otherwise");
            //      }
            // =>   if(a == 1){
            //          Console.Write("1");
            //      }else if(a == 2 || a == 3){
            //          Console.Write("2 or 3");
            //      }else{
            //          Console.Write("otherwise");
            //      }
            //
            // e.g.2 class Test
            //       {
            //           private let x (- int, y (- int;
            //       }
            //
            //       match t {
            //           Test{1, _} => print("1, x");,
            //           Test{2, 2} => print("2, 2");,
            //           Test{3, _} => print("3, x");,
            //           Test{x, y} if y == 2 * x => print("y == 2 * x");,
            //           Test{x, _} => print("{0}, y", x);,
            //           _          => ;
            //       }
            // =>    var __0 = t.x, __1 = t.y;
            //       if(__0 == 1){
            //           Console.Write("1, x");
            //       }else if(__0 == 2 && __1 == 2){
            //           Console.Write("2, 2");
            //       }else if(__0 == 3){
            //           Console.Write("3, x");
            //       }else{
            //           int x = __0, y = __1;    //destructuring becomes inner scope variable declarations
            //           if(y == 2 * x){          //a guard becomes an inner-scope if statement
            //               Console.Write("y == 2 * x");
            //           }
            //       }else{                       // a wildcard pattern becomes the else clause
            //       }
            // e.g.3 let x = [1, 2, 3];
            //       match x {
            //           [1, 2, x] => println("x is {0}", x);,
            //           [_, 2, x] => println("x is {0}", x);,
            //           [x, ..]   => println("x is {0}", x);
            //       }
            // =>    var __0 = x[0], __1 = x[1], __2 = x[2];  // in practice, the prefix is omitted
            //       if(__0 == 1 && __1 == 2){
            //           var x = __2;
            //           Console.WriteLine("x is {0}", x);
            //       }else if(__1 == 2){
            //           var x = __2;
            //           Console.WriteLine("x is {0}", x);
            //       }else if(x.Length > 1){
            //           var x = __0;
            //           Console.WriteLine("x is {0}", x);
            //       }
            // e.g.4 let t = (1, 'a', true);
            //       match t {
            //           (1, x, y) => println("x is {0} and y is {1}", x, y);,
            //           (1, 'a', _) => println("t is (1, 'a', _)");
            //       }
            // =>    var __0 = t.Item0, __1 = t.Item1, __2 = t.Item2; // in practice, the prefix is omitted
            //       if(__0 == 1){
            //           var x = __1, y = __2;
            //           Console.WriteLine("x is {0} and y is {1}", x, y);
            //       }else if(__0 == 1 && __1 == 'a'){
            //           Console.WriteLine("t is (1, 'a', _)");
            //       }
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            // Emit conditions
            var prev_op_type = context.OperationTypeOnIdentifier;
            var has_emitted_condition = false;
            var guard_label = il_generator.DefineLabel();
            var prev_or_label = context.CurrentOrTargetLabel;
            if(!matchClause.Guard.IsNull)
                context.CurrentOrTargetLabel = guard_label;

            if(!MatchIsLastStatement(matchClause) || !IsEnumLastClause(matchClause)){
                context.OperationTypeOnIdentifier = OperationType.Load;
                matchClause.Patterns.First().AcceptPatternWalker(condition_definer);
                foreach(var pattern in matchClause.Patterns.Skip(1)){
                    il_generator.Emit(OpCodes.Brtrue, context.CurrentOrTargetLabel);
                    pattern.AcceptPatternWalker(condition_definer);
                }

                has_emitted_condition = true;
            }

            context.CurrentOrTargetLabel = prev_or_label;

            //if(destructuring_exprs.Count() != context.Additionals.Count()){
                // The number of destructured variables must match in every pattern
            //    throw new EmitterException(
            //       "Expected the pattern contains {0} variables, but it only contains {1}!",
            //        destructuring_exprs.Count(), context.Additionals.Count()
            //    );
            //}

            if(!matchClause.Guard.IsNull){
                il_generator.Emit(OpCodes.Brfalse, context.CurrentAndTargetLabel);
                il_generator.MarkLabel(guard_label);
            }

            matchClause.Guard.AcceptWalker(this, context);

            if(has_emitted_condition && !(matchClause.Patterns.First() is WildcardPattern))
                il_generator.Emit(OpCodes.Brfalse, context.CurrentAndTargetLabel);

            il_generator.MarkLabel(context.CurrentOrTargetLabel);
            context.OperationTypeOnIdentifier = prev_op_type;

            // Emit destructuring code
            matchClause.Patterns.First().AcceptWalker(this, context);

            matchClause.Body.AcceptWalker(this, context);

            // If the match statement is the last statement in the block, we don't emit br command
            if(matchClause.NextSibling != null)
                il_generator.Emit(OpCodes.Br, context.CurrentJumpLabel);

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public Type VisitSequenceExpression(SequenceExpression seqExpr, CodeGeneratorContext context)
        {
            // A sequence expression is always translated to a tuple
            // Evaluate it here becasue otherwise, AcceptWalker will be called more than once
            var types = seqExpr.Items
                               .Select(item => item.AcceptWalker(this, context))
                               .ToArray();

            if(types.Length == 1){
                return types.First();
            }else{
                var ctor_method = typeof(Tuple).GetGenericMethod("Create", BindingFlags.Public | BindingFlags.Static, types.ToArray());

                il_generator.Emit(OpCodes.Call, ctor_method);
                return CSharpCompilerHelpers.GuessTupleType(types);
            }
        }

        public Type VisitUnaryExpression(UnaryExpression unaryExpr, CodeGeneratorContext context)
        {
            var type = unaryExpr.Operand.AcceptWalker(this, context);
            EmitUnaryOp(unaryExpr.Operator);
            return type;
        }

        public Type VisitSelfReferenceExpression(SelfReferenceExpression selfRef, CodeGeneratorContext context)
        {
            context.ParameterIndex = -1;
            if(context.OperationTypeOnIdentifier == OperationType.Set)
                context.ParameterIndex = 0;
            else
                LoadArg(0);

            // Don't need to change it to AcceptWalker(context.GenericTypeParameterReplacer) because selfRef.SelfIdentifier.Type never contains
            // `ParameterType`s
            var self_type = CSharpCompilerHelpers.GetNativeTypeWithSelf(selfRef.SelfIdentifier.Type, context.LazyTypeBuilder.Name);
            context.TargetType = self_type;
            return self_type;
        }

        public Type VisitSuperReferenceExpression(SuperReferenceExpression superRef, CodeGeneratorContext context)
        {
            // TODO: implement it
            //var super_type = context.LazyTypeBuilder.BaseType;
            //return CSharpExpr.Parameter(super_type, "super");
            return null;
        }

        public Type VisitNullReferenceExpression(NullReferenceExpression nullRef, CodeGeneratorContext context)
        {
            il_generator.Emit(OpCodes.Ldnull);
            return typeof(object);
        }

        public Type VisitCommentNode(CommentNode comment, CodeGeneratorContext context)
        {
            // Just ignore comment nodes...
            return null;
        }

        public Type VisitTextNode(TextNode textNode, CodeGeneratorContext context)
        {
            // Just ignore text nodes, too...
            return null;
        }

        public Type VisitTypeConstraint(TypeConstraint constraint, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitPostModifiers(PostModifiers postModifiers, CodeGeneratorContext context)
        {
            throw new InvalidOperationException("Can not work on that node!");
        }

        // AstType nodes should be treated with special care
        public Type VisitSimpleType(SimpleType simpleType, CodeGeneratorContext context)
        {
            // Use case: Called from VisitObjectCreationExpression
            // TypeArguments can differ so we can't use GetRuntimeSymbol
            //var symbol = GetRuntimeSymbol(simpleType.IdentifierToken);
            var type = CSharpCompilerHelpers.GetNativeOtherType(simpleType, true);
            // We need to set both TargetType and ConstructorType because VisitObjectCreation will use them
            context.TargetType = type;
            context.ConstructorType = type;
            return type;
        }

        public Type VisitPrimitiveType(PrimitiveType primitiveType, CodeGeneratorContext context)
        {
            // Use case: Called from VisitObjectCreationExpression
            var type = CSharpCompilerHelpers.GetNativeType(primitiveType);
            // We need to set both TargetType and ConstructorType because VisitObjectCreation will use them
            context.TargetType = type;
            context.ConstructorType = type;
            return type;
        }

        public Type VisitReferenceType(ReferenceType referenceType, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitMemberType(MemberType memberType, CodeGeneratorContext context)
        {
            context.RequestType = true;
            // When this MemerType refers to an enum variant we should only look into Target
            var type_table = symbol_table.GetTypeTable(memberType.Target.Name);
            memberType.Target.AcceptWalker(this, context);
            if(type_table != null && type_table.TypeKind == ClassType.Enum && context.ConstructorType != null){
                context.RequestType = false;
                return context.ConstructorType;
            }

            VisitSimpleType(memberType.ChildType, context);
            context.RequestType = false;
            return context.ConstructorType;
        }

        public Type VisitFunctionType(FunctionType funcType, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitParameterType(ParameterType paramType, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitPlaceholderType(PlaceholderType placeholderType, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitKeyValueType(KeyValueType keyValueType, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitFunctionParameterType(FunctionParameterType parameterType, CodeGeneratorContext context)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public Type VisitAttributeSection(AttributeSection section, CodeGeneratorContext context)
        {
            var target_context = context.AttributeTarget;

            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.None;
            // We need to force execution at this point by calling ToArray() here
            // Or otherwise context.OperationTypeOnIdentifier = OperationType.None has no effect
            var attributes = section.Attributes.Select(attribute => {
                VisitObjectCreationExpression(attribute, context);
                var ctor = context.Constructor;
                context.Constructor = null;

                var usage_attribute = context.TargetType.GetCustomAttribute<AttributeUsageAttribute>();

                var args = attribute.Items
                                    .Select(pair => {
                    if(pair.ValueExpression is LiteralExpression literal){
                        return literal.Value;
                    }else if(pair.ValueExpression is MemberReferenceExpression member){
                        member.AcceptWalker(this, context);
                        var field = (FieldInfo)context.PropertyOrField;
                        context.PropertyOrField = null;
                        return field.GetValue(null);
                    }else{
                        throw new InvalidOperationException("Unknown argument expression!");
                    }
                }).ToArray();

                return new {Ctor = ctor, Arguments = args, UsageAttribute = usage_attribute, TypeName = context.TargetType.Name};
            }).ToArray();
            context.OperationTypeOnIdentifier = prev_op_type;
               
            IEnumerable<AttributeTargets> allowed_targets;
            AttributeTargets preferable_target;
            string help_message;
            switch(section.Parent){
            case ExpressoAst ast:
                allowed_targets = new []{AttributeTargets.Assembly, AttributeTargets.Module};
                preferable_target = AttributeTargets.Module;
                help_message = "'assembly' or 'module'";
                break;

            case TypeDeclaration type_decl:
                allowed_targets = new []{AttributeTargets.Class, AttributeTargets.Enum};
                preferable_target = (type_decl.TypeKind == ClassType.Class) ? AttributeTargets.Class :
                (type_decl.TypeKind == ClassType.Enum) ? AttributeTargets.Enum : AttributeTargets.Property;
                help_message = "'type'";
                break;

            case FieldDeclaration field_decl:
                allowed_targets = new []{AttributeTargets.Field};
                preferable_target = AttributeTargets.Field;
                help_message = "'field'";
                break;

            case FunctionDeclaration func_decl:
                allowed_targets = new []{AttributeTargets.Method, AttributeTargets.ReturnValue};
                preferable_target = AttributeTargets.Method;
                help_message = "'method'";
                break;

            case ParameterDeclaration param_decl:
                allowed_targets = new []{AttributeTargets.Parameter};
                preferable_target = AttributeTargets.Parameter;
                help_message = "'param'";
                break;

            default:
                throw new InvalidOperationException("Unreachable");
            }

            if(!section.AttributeTargetToken.IsNull){
                var specified_context = AttributeSection.GetAttributeTargets(section.AttributeTarget);
                if(!allowed_targets.Any(t => t == specified_context)){
                    throw new ParserException(
                        $"The attribute target '{section.AttributeTarget}' is not expected in this context.",
                        "ES4021",
                        section
                    ){
                        HelpObject = help_message
                    };
                }

                foreach(var attribute in attributes){
                    var expected_target = attribute.UsageAttribute.ValidOn;
                    if(!expected_target.HasFlag(specified_context)){
                        throw new ParserException(
                            $"The specified attribute target '{specified_context}' is not allowed for the attribute `{attribute.TypeName}`.",
                            "ES4023",
                            section
                        ){
                            HelpObject = expected_target
                        };
                    }

                    if(target_context.HasFlag(specified_context))
                        context.CustomAttributeSetter(new CustomAttributeBuilder(attribute.Ctor, attribute.Arguments));
                }
            }else{
                foreach(var attribute in attributes){
                    var expected_target = attribute.UsageAttribute.ValidOn;
                    if(!expected_target.HasFlag(preferable_target)){
                        throw new ParserException(
                            $"The attribute target '{preferable_target}' is not allowed for the attribute `{attribute.TypeName}`.",
                            "ES4023",
                            section
                        ){
                            HelpObject = expected_target
                        };
                    }

                    if(target_context.HasFlag(preferable_target))
                        context.CustomAttributeSetter(new CustomAttributeBuilder(attribute.Ctor, attribute.Arguments));
                }
            }

            return null;
        }

        public Type VisitAliasDeclaration(AliasDeclaration aliasDecl, CodeGeneratorContext context)
        {
            context.TargetType = null;
            context.Method = null;
            aliasDecl.Path.AcceptWalker(this, context);
            if(context.Method == null && context.TargetType == null)
                throw new GeneratorException($"`{aliasDecl.Path}` could not be resolved to an entity name!");

            AddSymbol(aliasDecl.AliasToken, new ExpressoSymbol{
                Type = context.TargetType,
                Method = context.Method
            });
            return null;
        }

        public Type VisitImportDeclaration(ImportDeclaration importDecl, CodeGeneratorContext context)
        {
            foreach(var pair in importDecl.ImportPaths.Zip(importDecl.AliasTokens, (l, r) => new {ImportPath = l, Alias = r})){
                if(!pair.ImportPath.Name.Contains("::") && !pair.ImportPath.Name.Contains("."))
                    break;
                
                var import_path = pair.ImportPath;
                var alias = pair.Alias;

                var last_index = import_path.Name.LastIndexOf("::", StringComparison.CurrentCulture);
                var type_name = import_path.Name.Substring(last_index == -1 ? 0 : last_index + "::".Length);
                var type_args = import_path.TypeArguments.Select(arg => arg.Clone());
                var type_table = symbol_table.GetTypeTable(type_name);
                var simple_type = AstType.MakeSimpleType(CSharpCompilerHelpers.ConvertHeadToUpperCase(type_name), type_args);
                var type = (type_table == null) ? null :
                           (importDecl.TargetFilePath != null && importDecl.TargetFilePath.EndsWith(".dll", StringComparison.CurrentCulture))
                                                               ? CSharpCompilerHelpers.GetNativeTypeFromAssembly(simple_type, importDecl.TargetFilePath)
                                                               : CSharpCompilerHelpers.GetNativeType(simple_type);

                if(type != null){
                    var expresso_symbol = new ExpressoSymbol{Type = type};
                    //AddSymbol(import_path, expresso_symbol);
                    // This if statement is needed because otherwise types in the standard library
                    // won't get cached
                    if(!Symbols.ContainsKey(alias.IdentifierId))
                        AddSymbol(alias, expresso_symbol);
                }else{
                    var symbol = GetRuntimeSymbol(alias);
                    if(symbol != null){
                        //AddSymbol(alias, symbol);
                    }else{
                        var module_type_name = import_path.Name.Substring(0, last_index);
                        var module_type = CSharpCompilerHelpers.GetNativeType(AstType.MakeSimpleType(module_type_name));
                        var member_name = type_name;

                        if(module_type != null){
                            var flags = BindingFlags.Static | BindingFlags.NonPublic;
                            var method = module_type.GetMethod(member_name, flags);

                            if(method != null){
                                AddSymbol(alias, new ExpressoSymbol{Method = method});
                            }else{
                                var module_field = module_type.GetField(member_name, flags);

                                if(module_field != null){
                                    AddSymbol(alias, new ExpressoSymbol{PropertyOrField = module_field});
                                }else{
                                    // ES0103 thrown
                                    throw new ParserException(
                                        $"It is found that the import name '{import_path.Name}' isn't defined.",
                                        "ES1903",
                                        importDecl
                                    );
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        public Type VisitFunctionDeclaration(FunctionDeclaration funcDecl, CodeGeneratorContext context)
        {
            // Ignore abstract methods
            if(funcDecl.Body.IsNull)
                return null;
            
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var prev_context_ast = context.ContextAst;
            context.ContextAst = funcDecl;

            var type_generic_param_index = (generic_types != null) ? generic_types.Count : -1;
            if(generic_type_dict.TryGetValue(funcDecl.NameToken.IdentifierId, out var generic_type_list)){
                generic_types.AddRange(generic_type_list);
                context.GenericTypeParameterReplacer = new GenericTypeParameterReplacer(generic_types, context);
            }

            var method_builder = context.LazyTypeBuilder.GetMethodBuilder(funcDecl.Name);

            var prev_il_generator = il_generator;
            il_generator = method_builder.GetILGenerator();

            context.PDBGenerator.AddSequencePoints(funcDecl.Name);

            funcDecl.Body.AcceptWalker(this, context);
            context.ContextAst = prev_context_ast;

            if(!EndsWithReturnOrThrow(funcDecl.Body))
                il_generator.Emit(OpCodes.Ret);

            //if(funcDecl.Name == "main")
            //    context.AssemblyBuilder.SetEntryPoint(interface_func);

            if(type_generic_param_index != -1 && generic_types.Count > type_generic_param_index)
                generic_types.RemoveRange(type_generic_param_index, generic_types.Count - type_generic_param_index);
            
            il_generator = prev_il_generator;

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public Type VisitTypeDeclaration(TypeDeclaration typeDecl, CodeGeneratorContext context)
        {
            var parent_type = context.LazyTypeBuilder;
            context.LazyTypeBuilder = Symbols[typeDecl.NameToken.IdentifierId].LazyTypeBuilder;

            if(generic_type_dict.TryGetValue(typeDecl.NameToken.IdentifierId, out var generic_type_list)){
                generic_types = generic_type_list;
                context.GenericTypeParameterReplacer = new GenericTypeParameterReplacer(generic_types, context);

                foreach(var tc in typeDecl.TypeConstraints){
                    // We don't resolve type parameters in interface constraints
                    // because doing so makes it impossible to resolve methods
                    var interface_types =
                        from c in tc.TypeConstraints
                        let type = DetermineType(c, context)
                        where type.IsInterface
                        select type;

                    InterfaceConstraints.Add(tc.TypeParameter.Name, interface_types.ToArray());
                }
            }else{
                generic_types = new List<GenericTypeParameterBuilder>();
            }

            var tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            try{
                foreach(var member in typeDecl.Members)
                    member.AcceptWalker(this, context);

                if(typeDecl.TypeKind != ClassType.Interface)
                    context.LazyTypeBuilder.CreateType();
            }
            finally{
                context.LazyTypeBuilder = parent_type;
                generic_types = null;
                context.GenericTypeParameterReplacer = null;
                InterfaceConstraints.Clear();
            }

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public Type VisitFieldDeclaration(FieldDeclaration fieldDecl, CodeGeneratorContext context)
        {
            foreach(var init in fieldDecl.Initializers){
                var field_builder = (init.NameToken != null) ? Symbols[init.NameToken.IdentifierId].FieldBuilder : throw new GeneratorException($"Invalid field: {init.Pattern}");
                var prev_il_generator = il_generator;
                il_generator = context.LazyTypeBuilder.GetILGeneratorForFieldInit(field_builder);
                if(il_generator == null){
                    il_generator = prev_il_generator;
                    continue;
                }

                var type = init.Initializer.AcceptWalker(this, context);
                if(il_generator != null && type != null){
                    if(field_builder.IsStatic){
                        il_generator.Emit(OpCodes.Stsfld, field_builder);
                    }else{
                        LoadArg(0);
                        il_generator.Emit(OpCodes.Stfld, field_builder);
                    }
                }

                il_generator = prev_il_generator;
            }

            return null;
        }

        public Type VisitParameterDeclaration(ParameterDeclaration parameterDecl, CodeGeneratorContext context)
        {
            if(!Symbols.ContainsKey(parameterDecl.NameToken.IdentifierId)){
                var native_type = DetermineType(parameterDecl.ReturnType, context);
                var param = CSharpCompilerHelpers.CreateParameterInformation(native_type, parameterDecl.Name);
                AddSymbol(parameterDecl.NameToken, new ExpressoSymbol{Parameter = param, ParameterIndex = context.ParameterIndex});
                return native_type;
            }else{
                throw new InvalidOperationException($"The Symbols field already contains the parameter {parameterDecl.Name}");
            }
        }

        public Type VisitVariableInitializer(VariableInitializer initializer, CodeGeneratorContext context)
        {
            if(initializer.Initializer.IsNull){
                var prev_op_type = context.OperationTypeOnIdentifier;
                context.OperationTypeOnIdentifier = OperationType.None;
                initializer.Pattern.AcceptWalker(this, context);
                context.OperationTypeOnIdentifier = prev_op_type;
                return null;
            }else{
                if(initializer.Pattern.Pattern is TuplePattern tuple_pattern){
                    if(initializer.Initializer is SequenceExpression seq_expr){
                        // let (t1, t2) = Tuple.Create(...);
                        var prev_op_type = context.OperationTypeOnIdentifier;
                        var types = new List<Type>();
                        foreach(var pair in tuple_pattern.Patterns.Zip(seq_expr.Items, (l, r) => new {Pattern = l, Expression = r})){
                            context.OperationTypeOnIdentifier = OperationType.Load;
                            var tmp = pair.Expression.AcceptWalker(this, context);
                            types.Add(tmp);

                            context.OperationTypeOnIdentifier = OperationType.Set;
                            pair.Pattern.AcceptWalker(this, context);
                        }

                        return CSharpCompilerHelpers.GuessTupleType(types);
                    }else if(initializer.Initializer is PathExpression path || initializer.Initializer is CallExpression){
                        var type = initializer.Initializer.AcceptWalker(this, context);
                        if(initializer.Initializer is CallExpression call) {
                            var tmp_var = il_generator.DeclareLocal(type);
                            EmitSet(null, tmp_var, -1, type);
                            EmitLoadLocal(tmp_var, false);
                        }

                        // let (t1, t2) = t where t is Tuple
                        var prev_op_type = context.OperationTypeOnIdentifier;
                        context.OperationTypeOnIdentifier = OperationType.Set;

                        foreach(var pair in Enumerable.Range(1, tuple_pattern.Patterns.Count).Zip(tuple_pattern.Patterns, (l, r) => new {Index = l, Pattern = r})){
                            il_generator.Emit(OpCodes.Dup);
                            var property = type.GetProperty($"Item{pair.Index}");
                            il_generator.Emit(OpCodes.Callvirt, property.GetMethod);
                            pair.Pattern.AcceptWalker(this, context);
                        }
                        il_generator.Emit(OpCodes.Pop);

                        //var debug_info_list = debug_infos.ToList();
                        //debug_info_list.AddRange(tmps);
                        //result = CSharpExpr.Block(debug_info_list);//options.BuildType.HasFlag(BuildType.Debug) ? CSharpExpr.Block(debug_info_list) : CSharpExpr.Block(tmps);
                        return type;
                    }else{
                        throw new InvalidOperationException($"Invalid expression found: {initializer.Initializer.GetType().Name}");
                    }
                }else{
                    var prev_op_type = context.OperationTypeOnIdentifier;
                    if(!(initializer.Parent is ValueBindingPattern))
                        context.OperationTypeOnIdentifier = OperationType.Load;
                    else
                        context.OperationTypeOnIdentifier = OperationType.None;

                    var type = initializer.Initializer.AcceptWalker(this, context);

                    if(initializer.Parent is ValueBindingPattern)
                        context.TemporaryVariable = context.TargetLocalBuilder;

                    context.OperationTypeOnIdentifier = OperationType.Set;
                    VisitPatternWithType(initializer.Pattern, context);
                    context.OperationTypeOnIdentifier = prev_op_type;
                    if(options.BuildType.HasFlag(BuildType.Debug))
                        context.PDBGenerator.MarkSequencePoint(il_generator.ILOffset, initializer.StartLocation, initializer.EndLocation);

                    return type;
                }
            }
        }

        //#################################################
        //# Patterns can either be lvalues or rvalues
        //# Guideline for pattern visit methods:
        //# All pattern visit methods should meet the following preconditions and requirements
        //# Preconditions:
        //#     context.ContextExpression refers to the current state of the body block
        //# Requirements:
        //#     context.ContextExpression represents the expressions the corresponding body block should care for
        //#     and the return value indicates the branch condition(in other words, it indicates the condition
        //#     that the pattern should branch on)
        //#################################################
        public Type VisitWildcardPattern(WildcardPattern wildcardPattern, CodeGeneratorContext context)
        {
            // A wildcard pattern is translated to the else clause
            // so just return null to indicate that.
            return null;
        }

        public Type VisitIdentifierPattern(IdentifierPattern identifierPattern, CodeGeneratorContext context)
        {
            // An identifier pattern can arise by itself or as a child
            var symbol = GetRuntimeSymbol(identifierPattern.Identifier);
            if(symbol == null){
                // There are times when we haven't emitted the condition yet
                var type = DetermineType(identifierPattern.Identifier.Type, context);
                #if DEBUG
                if(type == null)
                    throw new InvalidOperationException($"type is null: {identifierPattern.Identifier.Type}");
                #endif

                if(type.IsGenericParameter && identifierPattern.Parent is DestructuringPattern destructuring2 && destructuring2.TypePath is MemberType member_type){
                    var type_table = symbol_table.GetTypeTable(member_type.Target.Name);
                    var tmp_var_type = context.TemporaryVariable.LocalType;
                    if(type_table != null && !tmp_var_type.ContainsGenericParameters){
                        int index = 0;
                        type_table.TypeParameters.Any(tp => {
                            if(tp.Name != type.Name){
                                ++index;
                                return false;
                            }

                            return true;
                        });
                        if(tmp_var_type.GenericTypeArguments.Length <= index)
                            throw new InvalidOperationException("Unknown number of type arguments.");

                        type = tmp_var_type.GenericTypeArguments.ElementAt(index);
                    }
                }
                var local_builder = il_generator.DeclareLocal(type);
                symbol = new ExpressoSymbol{LocalBuilder = local_builder};
                AddSymbol(identifierPattern.Identifier, symbol);
            }

            if(context.Parameters != null)
                context.Parameters.Add(symbol.LocalBuilder);

            if(context.OperationTypeOnIdentifier == OperationType.Set)
                EmitSet(null, symbol.LocalBuilder, -1, null);
            
            if(context.ContextAst is MatchStatement || identifierPattern.Ancestors.Any(a => a is ValueBindingPattern))
                context.CurrentTargetVariable = symbol.LocalBuilder;

            if(options.BuildType.HasFlag(BuildType.Debug))
                context.PDBGenerator.AddLocalVariable(default, symbol.LocalBuilder.LocalIndex, identifierPattern.Identifier.Name);

            //var start_loc = identifierPattern.Identifier.StartLocation;
            //var end_loc = identifierPattern.Identifier.EndLocation;
            //il_generator.MarkSequencePoint(document, start_loc.Line, start_loc.Column - 1, end_loc.Line, end_loc.Column - 1);

            if(identifierPattern.Parent is MatchPatternClause){
                EmitLoadLocal(context.TemporaryVariable, false);
                EmitSet(null, symbol.LocalBuilder, -1, null);
                context.CurrentTargetVariable = symbol.LocalBuilder;
            }

            if(context.PropertyOrField == null && context.TargetType != null && context.ContextAst is MatchStatement 
               && identifierPattern.Parent is DestructuringPattern destructuring && !destructuring.IsEnum){
                // context.TargetType is supposed to be set in CodeGenerator.VisitIdentifier
                var field = context.TargetType.GetField(identifierPattern.Identifier.Name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
                if(field == null){
                    throw new GeneratorException(
                        $"The type `{context.TargetType}` doesn't have the field `{identifierPattern.Identifier.Name}`."
                    );
                }
                context.PropertyOrField = field;
            }

            if(!identifierPattern.InnerPattern.IsNull)
                return identifierPattern.InnerPattern.AcceptWalker(this, context);
            else
                return symbol.LocalBuilder.LocalType;
        }

        public Type VisitCollectionPattern(CollectionPattern collectionPattern, CodeGeneratorContext context)
        {
            // First, make type validation expression
            // TODO: implement it
            /*var collection_type = CSharpCompilerHelpers.GetContainerType(collectionPattern.CollectionType);
            var item_type = CSharpCompilerHelpers.GetNativeType(collectionPattern.CollectionType.TypeArguments.First());
            var prev_additionals = context.Additionals;
            context.Additionals = new List<object>();
            var prev_additional_params = context.AdditionalParameters;
            context.AdditionalParameters = new List<ExprTree.ParameterExpression>();

            CSharpExpr res = null;
            int i = 0;
            var block = new List<CSharpExpr>();
            var block_params = new List<ExprTree.ParameterExpression>();
            foreach(var pattern in collectionPattern.Items){
                var index = CSharpExpr.Constant(i++);
                var elem_access = CSharpExpr.ArrayIndex(context.TemporaryExpression, index);
                //var tmp_param = CSharpExpr.Parameter(item_type, "__" + VariableCount++);
                //var assignment = CSharpExpr.Assign(tmp_param, elem_access);
                //context.Additionals.Add(assignment);
                //context.AdditionalParameters.Add(tmp_param);
                //block.Add(assignment);
                //block_params.Add(tmp_param);

                var prev_tmp_expr = context.TemporaryExpression;
                context.TemporaryExpression = elem_access;
                var expr = pattern.AcceptWalker(this, context);
                context.TemporaryExpression = prev_tmp_expr;

                var param = expr as ExprTree.ParameterExpression;
                if(param != null){
                    var assignment2 = CSharpExpr.Assign(param, elem_access);
                    block.Add(assignment2);
                    block_params.Add(param);
                }else{
                    if(context.Additionals.Any()){
                        var block_contents = context.Additionals.OfType<CSharpExpr>().ToList();
                        if(expr != null){
                            var if_content = CSharpExpr.IfThen(expr, context.ContextExpression);
                            block_contents.Add(if_content);
                        }
                        context.ContextExpression = CSharpExpr.Block(context.AdditionalParameters, block_contents);
                    }if(res == null){
	                    res = expr;
                    }else{
	                    res = CSharpExpr.AndAlso(res, expr);
                    }
                }
            }

            if(res == null){
                var native_type = CSharpCompilerHelpers.GetNativeType(collectionPattern.CollectionType);
                res = CSharpExpr.TypeIs(context.TemporaryVariable, native_type);
            }

            if(res != null)
                block.Add(CSharpExpr.IfThen(res, context.ContextExpression));
            else
                block.Add(context.ContextExpression);

            context.Additionals = prev_additionals;
            context.AdditionalParameters = prev_additional_params;

            if(block.Any())
                context.ContextExpression = CSharpExpr.Block(block_params, block);

            return res;//CSharpExpr.TypeIs(context.TemporaryVariable, collection_type);*/
            return null;
        }

        public Type VisitDestructuringPattern(DestructuringPattern destructuringPattern, CodeGeneratorContext context)
        {
            context.TargetType = null;
            destructuringPattern.TypePath.AcceptWalker(this, context);

            var type = context.TargetType;
            var prev_tmp_var = context.TemporaryVariable;
            if(destructuringPattern.IsEnum){
                var member_type = (MemberType)destructuringPattern.TypePath;
                var variant_name = member_type.ChildType.Name;
                var local_type = prev_tmp_var.LocalType;
                var variant_field = TreatField(local_type.Name.StartsWith("Tuple", StringComparison.CurrentCulture) ? context.TargetType : local_type,
                                               context.LazyTypeBuilder, member_type.ChildType.IdentifierNode, CSharpCompilerHelpers.HiddenMemberPrefix + variant_name);
                var variant_variable = il_generator.DeclareLocal(variant_field.FieldType);

                EmitLoadLocal(context.TemporaryVariable, false);
                EmitLoadField(variant_field);
                EmitSet(null, variant_variable, -1, null);
                context.TemporaryVariable = variant_variable;
            }

            context.RequestPropertyOrField = true;

            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.None;
            int i = 1;
            foreach(var pattern in destructuringPattern.Items){
                var item_ast_type = pattern.AcceptPatternWalker(item_type_inferencer);
                if(item_ast_type == null){
                    ++i;
                    continue;
                }
                var item_type = CSharpCompilerHelpers.GetNativeType(item_ast_type);

                context.PropertyOrField = null;
                var pattern_type = pattern.AcceptWalker(this, context);
                //context.TemporaryVariable = prev_tmp_variable;

                if(destructuringPattern.IsEnum){
                    var property_name = $"Item{i++}";
                    var variant_type = context.TemporaryVariable.LocalType;
                    var property = variant_type.GetProperty(property_name);
                    var param = context.CurrentTargetVariable;

                    EmitLoadLocal(context.TemporaryVariable, false);
                    EmitCall(property.GetMethod);
                    EmitSet(null, param, -1, null);
                }else{
                    var field = (FieldInfo)context.PropertyOrField;
                    context.PropertyOrField = null;
                    EmitLoadLocal(context.TemporaryVariable, false);
                    EmitLoadField(field);

                    if(context.CurrentTargetVariable != null){
                        EmitSet(null, context.CurrentTargetVariable, -1, null);
                        context.CurrentTargetVariable = null;
                    }else{
                        /*if(context.Additionals.Any()){
                            var block_contents = context.Additionals.OfType<CSharpExpr>().ToList();
                            if(expr != null){
                                var if_content = CSharpExpr.IfThen(expr, context.ContextExpression);
                                block_contents.Add(if_content);
                            }
                            context.ContextExpression = CSharpExpr.Block(context.AdditionalParameters, block_contents);
                        }else{*/
                            EmitBinaryOpInMiddle(OperatorType.ConditionalAnd, context.CurrentAndTargetLabel);
                        //}
                    }
                }
            }
            context.OperationTypeOnIdentifier = prev_op_type;

            /*if(res != null)
                block.Add(CSharpExpr.IfThen(res, context.ContextExpression));
            else
                block.Add(context.ContextExpression);*/

            context.TemporaryVariable = prev_tmp_var;
            context.RequestPropertyOrField = false;

            return type;
        }

        public Type VisitTuplePattern(TuplePattern tuplePattern, CodeGeneratorContext context)
        {
            // Tuple patterns should always be combined with value binding patterns
            if(tuplePattern.Ancestors.Any(a => a is MatchStatement)){
                var native_tuple_type = context.TemporaryVariable.LocalType;
                int i = 1;
                var prev_op_type = context.OperationTypeOnIdentifier;
                context.OperationTypeOnIdentifier = OperationType.None;
                foreach(var pattern in tuplePattern.Patterns){
                    var item_ast_type = pattern.AcceptPatternWalker(item_type_inferencer);
                    if(item_ast_type == null){
                        ++i;
                        continue;
                    }
                    
                    var item_type = CSharpCompilerHelpers.GetNativeType(item_ast_type);
                    //var tmp_param = CSharpExpr.Parameter(item_type, "__" + VariableCount++);
                    var prop_name = "Item" + i++;
                    var property = native_tuple_type.GetProperty(prop_name);
                    EmitLoadLocal(context.TemporaryVariable, false);
                    EmitCall(property.GetMethod);
                    //var assignment = CSharpExpr.Assign(tmp_param, property_access);
                    //context.Additionals.Add(assignment);
                    //context.AdditionalParameters.Add(tmp_param);
                    //block.Add(assignment);
                    //block_params.Add(tmp_param);

                    //var prev_tmp_expr = context.TemporaryExpression;
                    //context.TemporaryExpression = property_access;
                    var expr = pattern.AcceptWalker(this, context);
                    //context.TemporaryExpression = prev_tmp_expr;

                    //var param = expr as ExprTree.ParameterExpression;
                    if(context.CurrentTargetVariable != null){
                        EmitSet(null, context.CurrentTargetVariable, -1, null);
                        context.CurrentTargetVariable = null;
                    }else{
                        /*if(context.Additionals.Any()){
                            var block_contents = context.Additionals.OfType<CSharpExpr>().ToList();
                            if(expr != null){
                                var if_content = CSharpExpr.IfThen(expr, context.ContextExpression);
                                block_contents.Add(if_content);
                            }
                            context.ContextExpression = CSharpExpr.Block(context.AdditionalParameters, block_contents);
                        }else if(res == null){
                            res = expr;
                        }else{
                            res = CSharpExpr.AndAlso(res, expr);
                        }*/
                    }
                }
                context.OperationTypeOnIdentifier = prev_op_type;

                /*if(res != null)
                    block.Add(CSharpExpr.IfThen(res, context.ContextExpression));
                else
                    block.Add(context.ContextExpression);*/

                return native_tuple_type;
            }else{
                foreach(var pattern in tuplePattern.Patterns)
                    pattern.AcceptWalker(this, context);

                return null;
            }
        }

        public Type VisitExpressionPattern(ExpressionPattern exprPattern, CodeGeneratorContext context)
        {
            if(context.ContextAst is MatchStatement)
                return null;
            
            context.RequestMethod = true;
            context.Method = null;
            var type = exprPattern.Expression.AcceptWalker(this, context);
            context.RequestMethod = false;

            if(context.Method != null && context.Method.DeclaringType.Name == "ExpressoIntegerSequence"){
                var method = context.Method;
                context.Method = null;
                il_generator.Emit(OpCodes.Callvirt, method);
                return null;
            }else if(context.ContextAst is MatchStatement){
                EmitLoadLocal(context.TemporaryVariable, false);
                il_generator.Emit(OpCodes.Ceq);
                return type;
            }else{
                return type;
            }
        }

        public Type VisitIgnoringRestPattern(IgnoringRestPattern restPattern, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitKeyValuePattern(KeyValuePattern keyValuePattern, CodeGeneratorContext context)
        {
            context.RequestPropertyOrField = true;
            VisitIdentifier(keyValuePattern.KeyIdentifier, context);
            var type = keyValuePattern.Value.AcceptWalker(this, context);

            if(context.CurrentTargetVariable != null){
                EmitLoadLocal(context.TemporaryVariable, false);
                var field = (FieldInfo)context.PropertyOrField;
                context.PropertyOrField = null;
                EmitLoadField(field);
                EmitSet(null, context.CurrentTargetVariable, -1, null);
                return null;
            }

            return type;
        }

        public Type VisitPatternWithType(PatternWithType pattern, CodeGeneratorContext context)
        {
            return pattern.Pattern.AcceptWalker(this, context);
        }

        public Type VisitTypePathPattern(TypePathPattern pathPattern, CodeGeneratorContext context)
        {
            return null;
        }

        public Type VisitValueBindingPattern(ValueBindingPattern valueBindingPattern, CodeGeneratorContext context)
        {
            if(valueBindingPattern.Parent is IfStatement){
                var prev_op_type = context.OperationTypeOnIdentifier;
                context.OperationTypeOnIdentifier = OperationType.None;

                valueBindingPattern.Initializer.Initializer.AcceptWalker(this, context);
                context.TemporaryVariable = context.TargetLocalBuilder;
                valueBindingPattern.Initializer.Pattern.AcceptPatternWalker(condition_definer);

                context.OperationTypeOnIdentifier = prev_op_type;
                return null;
            }
            return VisitVariableInitializer(valueBindingPattern.Initializer, context);
        }

        public Type VisitNullNode(AstNode nullNode, CodeGeneratorContext context)
        {
            // Just ignore null nodes...
            return null;
        }

        public Type VisitNewLine(NewLineNode newlineNode, CodeGeneratorContext context)
        {
            // Just ignore new lines...
            return null;
        }

        public Type VisitWhitespace(WhitespaceNode whitespaceNode, CodeGeneratorContext context)
        {
            // Just ignore whitespaces...
            return null;
        }

        public Type VisitExpressoTokenNode(ExpressoTokenNode tokenNode, CodeGeneratorContext context)
        {
            // It doesn't matter what tokens Expresso uses
            return null;
        }

        public Type VisitPatternPlaceholder(AstNode placeholder, Pattern child, CodeGeneratorContext context)
        {
            // Ignore placeholder nodes because they are just placeholders...
            return null;
        }

        #endregion

		#region methods
        void EmitBinaryOp(OperatorType opType, Type targetType = null, bool inspectingComplexCondition = false, Label? label = null)
		{
			switch(opType){
			case OperatorType.BitwiseAnd:
                if(targetType != null){
                    var and_method = targetType.GetMethod("op_BitwiseAnd", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(and_method);
                }else{
                    il_generator.Emit(OpCodes.And);
                }
                break;

			case OperatorType.BitwiseShiftLeft:
                if(targetType != null){
                    var left_shift_method = targetType.GetMethod("op_LeftShift", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(left_shift_method);
                }else{
                    il_generator.Emit(OpCodes.Shl);
                }
                break;

			case OperatorType.BitwiseOr:
                if(targetType != null){
                    var or_method = targetType.GetMethod("op_BitwiseOr", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(or_method);
                }else{
                    il_generator.Emit(OpCodes.Or);
                }
                break;

			case OperatorType.BitwiseShiftRight:
                if(targetType != null){
                    var right_shift_method = targetType.GetMethod("op_RightShift", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(right_shift_method);
                }else{
                    il_generator.Emit(OpCodes.Shr);
                }
                break;

            case OperatorType.ConditionalAnd:
                if(inspectingComplexCondition && label.HasValue){
                    il_generator.MarkLabel(label.Value);
                    EmitObject(false);
                }else if(inspectingComplexCondition && !label.HasValue){
                    il_generator.Emit(OpCodes.And);
                }else{
                    il_generator.Emit(OpCodes.And);
                }
                break;

            case OperatorType.ConditionalOr:
                il_generator.Emit(OpCodes.Or);
                if(label.HasValue && inspectingComplexCondition)
                    il_generator.Emit(OpCodes.Br_S, label.Value);

                break;

			case OperatorType.ExclusiveOr:
                if(targetType != null){
                    var xor_method = targetType.GetMethod("op_ExclusiveOr", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(xor_method);
                }else{
                    il_generator.Emit(OpCodes.Xor);
                }
                break;

			case OperatorType.Divide:
                if(targetType != null){
                    var div_method = targetType.GetMethod("op_Division", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(div_method);
                }else{
                    il_generator.Emit(OpCodes.Div);
                }
                break;

			case OperatorType.Equality:
                if(targetType != null){
                    var equality_method = targetType.GetMethod("op_Equality", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(equality_method);
                }else{
                    il_generator.Emit(OpCodes.Ceq);
                }
                break;

			case OperatorType.GreaterThan:
                if(targetType != null){
                    var greater_than_method = targetType.GetMethod("op_GreaterThan", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(greater_than_method);
                }else{
                    il_generator.Emit(OpCodes.Cgt);
                }
                break;

            case OperatorType.GreaterThanOrEqual:
                if(targetType != null){
                    var greater_than_or_equal_method = targetType.GetMethod("op_GreaterThanOrEqual", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(greater_than_or_equal_method);
                }else{
                    il_generator.Emit(OpCodes.Clt);
                    il_generator.Emit(OpCodes.Ldc_I4_0);
                    il_generator.Emit(OpCodes.Ceq);
                }
                break;

            case OperatorType.LessThan:
                if(targetType != null){
                    var less_than_method = targetType.GetMethod("op_LessThan", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(less_than_method);
                }else{
                    il_generator.Emit(OpCodes.Clt);
                }
                break;

            case OperatorType.LessThanOrEqual:
                if(targetType != null){
                    var less_than_or_equal_method = targetType.GetMethod("op_LessThanOrEqual", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(less_than_or_equal_method);
                }else{
                    il_generator.Emit(OpCodes.Cgt);
                    il_generator.Emit(OpCodes.Ldc_I4_0);
                    il_generator.Emit(OpCodes.Ceq);
                }
                break;

			case OperatorType.Minus:
                if(targetType != null){
                    var subtraction_method = targetType.GetMethod("op_Subtraction", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(subtraction_method);
                }else{
                    il_generator.Emit(OpCodes.Sub);
                }
                break;

			case OperatorType.Modulus:
                if(targetType != null){
                    var modulus_method = targetType.GetMethod("op_Modulus", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(modulus_method);
                }else{
                    il_generator.Emit(OpCodes.Rem);
                }
                break;

			case OperatorType.InEquality:
                if(targetType != null){
                    var ineuality_method = targetType.GetMethod("op_Inequality", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(ineuality_method);
                }else{
                    il_generator.Emit(OpCodes.Ceq);
                    il_generator.Emit(OpCodes.Ldc_I4_0);
                    il_generator.Emit(OpCodes.Ceq);
                }
                break;

			case OperatorType.Plus:
                if(targetType != null){
                    var add_method = targetType.GetMethod("op_Addition", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(add_method);
                }else{
                    il_generator.Emit(OpCodes.Add);
                }
                break;

			case OperatorType.Times:
                if(targetType != null){
                    var multiply_method = targetType.GetMethod("op_Multiply", new []{typeof(BigInteger), typeof(BigInteger)});
                    EmitCall(multiply_method);
                }else{
                    il_generator.Emit(OpCodes.Mul);
                }
                break;

			default:
				throw new GeneratorException("Unknown binary operator!");
			}
		}

        void EmitBinaryOpInMiddle(OperatorType operatorType, Label? andTargetLabel)
        {
            switch(operatorType){
            case OperatorType.ConditionalAnd:
                il_generator.Emit(OpCodes.Brfalse, andTargetLabel.Value);
                break;
            }
        }

        void EmitUnaryOp(OperatorType opType)
		{
			switch(opType){
            case OperatorType.Reference:
                // The parameter modifier "ref" is the primary candidate for this operator and it is expressed as a type
                // So we don't need to do anything here :-)
                break;

			case OperatorType.Plus:
                break;

			case OperatorType.Minus:
                il_generator.Emit(OpCodes.Neg);
                break;

			case OperatorType.Not:
                il_generator.Emit(OpCodes.Ldc_I4_0);
                il_generator.Emit(OpCodes.Ceq);
                break;

			default:
				throw new GeneratorException("Unknown unary operator!");
			}
		}

        Type EmitCallExpression(MethodInfo method, IEnumerable<Expression> args, IEnumerable<KeyValueType> typeArgs, CodeGeneratorContext context,
            bool callAsExtension = false, Expression callTarget = null)
        {
            if(method == null)
                throw new ArgumentNullException(nameof(method));

            if(method.DeclaringType.Name == "String" && method.Name == "Format"){
                // We can't remove this specialization because we need to call ExpandContainer on each argument
                var first = args.First();
                var expand_method = typeof(CSharpCompilerHelpers).GetMethod("ExpandContainer");
                if(first is LiteralExpression first_string && first_string.Value is string && ((string)first_string.Value).Contains("{0}")){
                    var parameters = method.GetParameters();
                    first_string.AcceptWalker(this, context);

                    switch(parameters.Length){
                    case 2:
                        if(parameters[1].ParameterType.IsArray){
                            EmitNewArray(parameters[1].ParameterType.GetElementType(), args.Skip(1), arg => {
                                var original_type = arg.AcceptWalker(this, context);
                                EmitCast(original_type, typeof(object));
                                il_generator.Emit(OpCodes.Call, expand_method);
                            });
                            il_generator.Emit(OpCodes.Call, method);
                        }else{
                            var original_type = args.ElementAt(1).AcceptWalker(this, context);
                            EmitCast(original_type, typeof(object));
                            il_generator.Emit(OpCodes.Call, expand_method);
                            il_generator.Emit(OpCodes.Call, method);
                        }
                        break;

                    case 3:
                    case 4:
                        foreach(var arg in args.Skip(1)){
                            var original_type = arg.AcceptWalker(this, context);
                            EmitCast(original_type, typeof(object));
                            il_generator.Emit(OpCodes.Call, expand_method);
                        }

                        il_generator.Emit(OpCodes.Call, method);
                        break;

                    default:
                        throw new InvalidOperationException("Unreachable");
                    }
                }else{
                    // This block is needed because otherwise we can't print anything
                    var builder = new StringBuilder("{0}");
                    for(int i = 1; i < args.Count(); ++i){
                        builder.Append(", ");
                        builder.Append($"{{{i}}}");
                    }

                    il_generator.Emit(OpCodes.Ldstr, builder.ToString());

                    var prev_op_type = context.OperationTypeOnIdentifier;
                    context.OperationTypeOnIdentifier = OperationType.Load;
                    EmitNewArray(typeof(string), args, arg => {
                        var original_type = arg.AcceptWalker(this, context);
                        EmitCast(original_type, typeof(object));
                        il_generator.Emit(OpCodes.Call, expand_method);
                    });
                    context.OperationTypeOnIdentifier = prev_op_type;
                    il_generator.Emit(OpCodes.Call, method);
                }

                return method.ReturnType;
            }else if(method.DeclaringType.Name == "Math" && method.Name == "Pow"){
                // Specialize the Pow method so that it can return int
                var arg_count = args.Count();
                Type expected_type = null;
                foreach(var pair in Enumerable.Range(0, arg_count).Zip(method.GetParameters(), (l, r) => new {Index = l, Parameter = r})){
                    var arg = args.ElementAt(pair.Index);
                    var type = arg.AcceptWalker(this, context);
                    if(expected_type == null)
                        expected_type = type;

                    if(pair.Parameter.ParameterType != type && !pair.Parameter.ParameterType.IsByRef)
                        EmitCast(type, pair.Parameter.ParameterType);
                }

                EmitCall(method);
                if(expected_type != typeof(double))
                    EmitCast(typeof(double), expected_type);

                return null;
            }else{
                Type return_type = method.ReturnType;

                if(method.ContainsGenericParameters && typeArgs.Any()){
                    var native_type_args = typeArgs.Select(ta => DetermineType(ta.ValueType, context));
                    if(method.IsGenericMethodDefinition){
                        method = method.MakeGenericMethod(native_type_args.ToArray());
                    }else{
                        if(native_type_args.Any(ta => ta.IsGenericParameter)){
                            var declaring_type = method.DeclaringType;
                            var generic_params = declaring_type.GetGenericArguments();
                            var real_type_args = generic_params.Select(gp => {
                                if(gp.IsGenericParameter)
                                    return native_type_args.First(ta => ta.Name == gp.Name);
                                else
                                    return gp;
                            });
                            var generic_type = method.DeclaringType.GetGenericTypeDefinition();
                            var type = generic_type.MakeGenericType(real_type_args.ToArray());
                            var generic_method = generic_type.GetMethod(method.Name);
                            method = TypeBuilder.GetMethod(type, generic_method);
                        }else{
                            var type = method.DeclaringType;
                            type = type.MakeGenericType(native_type_args.ToArray());
                            method = type.GetMethod(method.Name);
                        }
                    }

                    return_type = method.ReturnType;
                }

                var field_type = context.TargetType;
                if(method.ContainsGenericParameters && !method.IsGenericMethodDefinition){
                    // Work around for declaring types like IEnumerable<U> where U is a type parameter
                    // Those methods don't support the GetParameters method
                    EmitArgumentsForMethodWithGenericParameters(args, context);
                }else{
                    var method_params = method.GetParameters();
                    var arg_count = args.Count();
                    if(method_params.Length < arg_count){
                        // For params methods
                        int base_index = method_params.Length;
                        var array_param = method_params.Last();
                        if(!array_param.ParameterType.IsArray)
                            throw new GeneratorException($"Expected the last parameter is an array(params): function name: {method.Name} parameter name: {array_param.Name}.");
                    
                        var element_type = array_param.ParameterType.GetElementType();
                        EmitArguments(method_params.Take(method_params.Length - 1).ToArray(), args, context, false);

                        EmitNewArray(element_type, args.Skip(base_index - 1), arg => {
                            arg.AcceptWalker(this, context);
                        });
                        /*foreach(var arg in args.Skip(base_index - 1)){
                            var original_type = arg.AcceptWalker(this, context);
                            EmitCast(original_type, element_type);
                        }*/
                    }else if(method_params.Length > arg_count){
                        if(ExpressoCompilerHelpers.IsParamsParameter(method_params.Last())){
                            // For params methods with 0 parameters on the params parameter
                            EmitArguments(method_params.Take(method_params.Length - 1).ToArray(), args, context, false);

                            var element_type = method_params.Last().ParameterType.GetElementType();
                            var generic_empty_method = typeof(Array).GetMethod("Empty");
                            var empty_method = generic_empty_method.MakeGenericMethod(element_type);
                            EmitCall(empty_method);
                        }else{
                            // For optional parameters
                            EmitArguments(method_params, args, context, false);
                        
                            foreach(var pair in Enumerable.Range(arg_count, method_params.Length - arg_count)
                                                          .Zip(method_params.Skip(arg_count), (l, r) => new {Index = l, Parameter = r})){
                                if(!pair.Parameter.HasDefaultValue)
                                    throw new InvalidOperationException($"Expected #{pair.Index} of the parameters of {method.Name} has some default value.");

                                EmitObject(pair.Parameter.DefaultValue);
                            }
                        }
                    }else{
                        EmitArguments(method_params, args, context, callAsExtension);
                    }
                }

                EmitConstrainedOpCode(callTarget, field_type);
                if((method.DeclaringType == typeof(int) || method.DeclaringType == typeof(bool)) && !method.IsStatic)
                    il_generator.Emit(OpCodes.Box, method.DeclaringType);

                EmitCall(method);

                return return_type;
            }
        }

        void EmitArgumentsForMethodWithGenericParameters(IEnumerable<Expression> args, CodeGeneratorContext context)
        {
            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;

            foreach(var arg in args)
                arg.AcceptWalker(this, context);

            context.OperationTypeOnIdentifier = prev_op_type;
        }

        void EmitArguments(ParameterInfo[] parameters, IEnumerable<Expression> args, CodeGeneratorContext context, bool callAsExtension)
        {
            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;

            var arg_count = args.Count();
            foreach(var pair in Enumerable.Range(0, arg_count).Zip(parameters, (l, r) => new {Index = l, Parameter = r})){
                if(callAsExtension && pair.Index == 0)
                    continue;

                var arg = args.ElementAt(pair.Index);
                if(pair.Parameter.ParameterType.IsByRef){
                    context.ExpectsReference = true;
                    context.ExpectsReferenceIsPrimitive = true;
                }else{
                    context.ExpectsReference = false;
                    context.ExpectsReferenceIsPrimitive = false;
                }

                var type = arg.AcceptWalker(this, context);

                // can't write it as pair.Param.ParametrType != type because otherwise it will fail due to pair.Param.ParameterType not being type
                // since there are still classes of the same name in the other tests
                // We can't change the order of these conditions because then !type.IsEnum would throw for generic type parameters
                if(type != null && !type.IsGenericParameter && pair.Parameter.ParameterType.Name != type.Name && !type.IsEnum && !pair.Parameter.ParameterType.IsByRef)
                    EmitCast(type, pair.Parameter.ParameterType);
            }

            context.ExpectsReference = false;
            context.ExpectsReferenceIsPrimitive = false;
            context.OperationTypeOnIdentifier = prev_op_type;
        }

        void EmitConstrainedOpCode(Expression callTarget, Type fieldType)
        {
            if(callTarget is MemberReferenceExpression mem_ref && mem_ref.Target is MemberReferenceExpression mem_ref2){
                var field_type = mem_ref2.Member.Type;
                if(field_type is ParameterType param_type && InterfaceConstraints.ContainsKey(param_type.Name))
                    il_generator.Emit(OpCodes.Constrained, fieldType);
            }
        }

        void RegisterInterfaceFunctions(IEnumerable<EntityDeclaration> entities, LazyTypeBuilder currentTypeBuilder, AssemblyBuilder assemblyBuilder)
        {
            foreach(var entity in entities){
                switch(entity){
                case TypeDeclaration type_decl:
                    if(type_decl.TypeKind == ClassType.Interface)
                        continue;

                    var symbol = GetRuntimeSymbol(type_decl.NameToken);
                    if(symbol == null)
                        throw new InvalidOperationException($"`{type_decl.Name}` is not registered in Symbols!");

                    var lazy_type_builder = symbol.LazyTypeBuilder;
                    RegisterInterfaceFunctions(type_decl.Members, lazy_type_builder, assemblyBuilder);
                    break;

                case FunctionDeclaration func_decl:
                    RegisterInterfaceFunction(func_decl, currentTypeBuilder, assemblyBuilder);
                    break;
                }
            }
        }

        void RegisterInterfaceFunction(FunctionDeclaration funcDecl, LazyTypeBuilder lazyTypeBuilder, AssemblyBuilder assemblyBuilder)
        {
            var is_global_function = !funcDecl.Modifiers.HasFlag(Modifiers.Public) && !funcDecl.Modifiers.HasFlag(Modifiers.Protected) && !funcDecl.Modifiers.HasFlag(Modifiers.Private);
            var flags = is_global_function || funcDecl.Modifiers.HasFlag(Modifiers.Static) ? BindingFlags.Static : BindingFlags.Instance;
            if(funcDecl.Modifiers.HasFlag(Modifiers.Export) || funcDecl.Modifiers.HasFlag(Modifiers.Public))
                flags |= BindingFlags.Public;
            else
                flags |= BindingFlags.NonPublic;

            var interface_func = lazyTypeBuilder.GetInterfaceMethod(funcDecl.Name, flags);
            if(!interface_func.DeclaringType.IsGenericTypeDefinition)
                AddSymbol(funcDecl.NameToken, new ExpressoSymbol{Method = interface_func});

            if(funcDecl.Name == "main")
                assemblyBuilder.SetEntryPoint(interface_func);
        }

        void DefineFunctionSignaturesAndFields(IEnumerable<EntityDeclaration> entities, CodeGeneratorContext context)
        {
            int tmp_counter = scope_counter;
            PreDefineTypes(entities, context);
            scope_counter = tmp_counter;

            // This code is needed because otherwise we can't define types that point at each other
            CallInterfaceTypeDefiner(entities, context);
            scope_counter = tmp_counter;

            // We can't make this method an iterator because then we can't look at all the entities
            // We need to store scope_counter here because the DefineFunctionSignature method will make it 1 step forward every time it will be called
            foreach(var entity in entities){
                if(entity is FunctionDeclaration func_decl
                   && context.LazyTypeBuilder.GetMethodBuilder(func_decl.Name) == null){
                    DefineFunctionSignature(func_decl, context);
                }else if(entity is FieldDeclaration field_decl
                        && context.LazyTypeBuilder.GetFieldBuilder(field_decl.Initializers.First().Name) == null){
                    DefineField(field_decl, context);
                }
            }

            if(context.LazyTypeBuilder != null)
                context.LazyTypeBuilder.CreateInterfaceType();

            scope_counter = tmp_counter;
        }

        void PreDefineTypes(IEnumerable<EntityDeclaration> entities, CodeGeneratorContext context)
        {
            foreach(var entity in entities){
                if(entity is TypeDeclaration type_decl)
                    PreDefineType(type_decl, context);
            }
        }

        void PreDefineType(TypeDeclaration typeDecl, CodeGeneratorContext context)
        {
            var flags = typeDecl.Modifiers.HasFlag(Modifiers.Export) ? TypeAttributes.Public : TypeAttributes.NotPublic;
            flags |= (typeDecl.TypeKind == ClassType.Interface) ? TypeAttributes.Interface | TypeAttributes.Abstract
                                                                : TypeAttributes.Class | TypeAttributes.BeforeFieldInit | TypeAttributes.AnsiClass;
            if(typeDecl.Modifiers.HasFlag(Modifiers.Sealed))
                flags |= TypeAttributes.Sealed;

            if(typeDecl.Modifiers.HasFlag(Modifiers.Abstract))
                flags |= TypeAttributes.Abstract;

            var name = typeDecl.Name;
            var base_types = typeDecl.BaseTypes
                                     .Select(bt => {
                                         if(bt is SimpleType simple && simple.TypeArguments.Any() && simple.TypeArguments.Any(ta => !(ta is ParameterType))){
                                             var type_args = simple.TypeArguments.Select(ta => CSharpCompilerHelpers.GetNativeType(ta, true)).ToArray();
                                             var generic_type = Symbols[bt.IdentifierNode.IdentifierId].Type;
                                             if(!generic_type.IsGenericTypeDefinition)
                                                 throw new InvalidOperationException($"Expected {generic_type} is a generic type!");

                                             var constructed = generic_type.MakeGenericType(type_args);
                                             return constructed;
                                         }else{
                                             return Symbols[bt.IdentifierNode.IdentifierId].Type;
                                         }
                                     })
                                     .ToList();

            // derive from object if we haven't already derived from that
            var last_type = base_types.LastOrDefault();
            if(last_type != null && !last_type.IsClass || last_type == null)
                base_types.Add(typeof(object));

            var is_raw_value_enum = symbol_table.GetSymbol(Utilities.RawValueEnumValueFieldName) != null;
            if(typeDecl.TypeKind == ClassType.Interface){
                var interface_type_builder = context.ModuleBuilder.DefineType(name, flags);
                var expresso_symbol = new ExpressoSymbol{Type = interface_type_builder};
                AddSymbol(typeDecl.NameToken, expresso_symbol);

                if(typeDecl.TypeConstraints.Any()){
                    generic_types = interface_type_builder
                        .DefineGenericParameters(typeDecl.TypeConstraints.Select(c => c.TypeParameter.Name).ToArray())
                        .ToList();

                    generic_type_dict.Add(typeDecl.NameToken.IdentifierId, generic_types.Select(gt => gt).ToList());
                }

                // Call InterfaceTypeDefiner here on interfaces in order for us to inherit generic interfaces with concrete types
                var interface_definer = new InterfaceTypeDefiner(this, context);
                interface_definer.VisitTypeDeclaration(typeDecl);
            }else{
                var lazy_type_builder = new LazyTypeBuilder(context.ModuleBuilder, name, flags, base_types.Last(), false, is_raw_value_enum);
                var expresso_symbol = new ExpressoSymbol{Type = lazy_type_builder.TypeBuilder, LazyTypeBuilder = lazy_type_builder};
                // We need to add TypeBuilder here because otherwise we can't define method signatures
                AddSymbol(typeDecl.NameToken, expresso_symbol);

                foreach(var base_type in base_types){
                    if(base_type.IsInterface)
                        lazy_type_builder.TypeBuilder.AddInterfaceImplementation(base_type);
                }

                if(typeDecl.TypeConstraints.Any()){
                    generic_types = lazy_type_builder.DefineGenericParameters(typeDecl.TypeConstraints.Select(c => c.TypeParameter.Name).ToArray()).ToList();

                    // We need to split this operation because otherwise we can't use type parameters in generic parameter constraints
                    context.GenericTypeParameterReplacer = new GenericTypeParameterReplacer(generic_types, context);
                    generic_types = generic_types.Select(gp => SetConstraints(typeDecl.TypeConstraints, context, gp)).ToList();

                    generic_type_dict.Add(typeDecl.NameToken.IdentifierId, generic_types.Select(gt => gt).ToList());
                    context.GenericTypeParameterReplacer = null;
                }else{
                    // We can't remove this code because subsequent operations will use it
                    generic_types = new List<GenericTypeParameterBuilder>();
                }
            }
        }

        GenericTypeParameterBuilder SetConstraints(IEnumerable<TypeConstraint> typeConstraints, CodeGeneratorContext context, GenericTypeParameterBuilder gp)
        {
            var type_constraint = typeConstraints.Where(c => {
                if(c.TypeParameter.Name != gp.Name)
                    return false;

                return c.TypeConstraints.Any(inner_c => {
                    var constraint_type_name = inner_c is SimpleType simple && !simple.IdentifierNode.Type.IsNull ? simple.IdentifierNode.Type.Name : inner_c.Name;
                    var type_table = symbol_table.GetTypeTable(constraint_type_name);
                    if(type_table == null)
                        return false;

                    return type_table.TypeKind == ClassType.Class || type_table.TypeKind == ClassType.Interface;
                });
            }).FirstOrDefault(c => c.TypeParameter.Name == gp.Name);

            if(type_constraint != null){
                var constraints = type_constraint.TypeConstraints
                                                 .Select(c => c)
                                                 .ToList();
                var constraint_base_type = constraints.FirstOrDefault(c => {
                    var constraint_type_name = c is SimpleType simple && !simple.IdentifierNode.Type.IsNull ? simple.IdentifierNode.Type.Name : c.Name;
                    var type_table = symbol_table.GetTypeTable(constraint_type_name);
                    return type_table.TypeKind == ClassType.Class;
                });
                if(constraint_base_type != null){
                    gp.SetBaseTypeConstraint(CSharpCompilerHelpers.GetNativeType(constraint_base_type));
                    constraints.Remove(constraint_base_type);
                }

                gp.SetInterfaceConstraints(constraints.Select(c => DetermineType(c, context)).ToArray());
            }
            return gp;
        }

        void CallInterfaceTypeDefiner(IEnumerable<EntityDeclaration> entities, CodeGeneratorContext context)
        {
            var interface_type_definer = new InterfaceTypeDefiner(this, context);
            foreach(var entity in entities){
                // module variables can also be `EntityDeclaration`s
                if(entity is TypeDeclaration type_decl && type_decl.TypeKind != ClassType.Interface)
                    interface_type_definer.VisitTypeDeclaration(type_decl);
                else if(entity is TypeDeclaration type_decl2 && type_decl2.TypeKind == ClassType.Interface || entity is FunctionDeclaration)
                    ++scope_counter;
            }
        }

        void DefineFunctionSignature(FunctionDeclaration funcDecl, CodeGeneratorContext context)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var param_types = funcDecl.Parameters.Select((param, index) => {
                context.ParameterIndex = index;
                return VisitParameterDeclaration(param, context);
            });
            context.ParameterIndex = -1;

            var return_type = CSharpCompilerHelpers.GetNativeType(ResolveReturnType(funcDecl.ReturnType), true, true);

            var is_global_function = !funcDecl.Modifiers.HasFlag(Modifiers.Public) && !funcDecl.Modifiers.HasFlag(Modifiers.Protected) && !funcDecl.Modifiers.HasFlag(Modifiers.Private);
            var flags = is_global_function ? MethodAttributes.Static :
                        funcDecl.Modifiers.HasFlag(Modifiers.Protected) ? MethodAttributes.Family :
                        funcDecl.Modifiers.HasFlag(Modifiers.Public) ? MethodAttributes.Public : MethodAttributes.Private;
            if(funcDecl.Modifiers.HasFlag(Modifiers.Export))
                flags |= MethodAttributes.Public;
            else if(is_global_function)
                flags |= MethodAttributes.Assembly;

            var method_builder = context.LazyTypeBuilder.DefineMethod(funcDecl.Name, flags, return_type, param_types.ToArray(), this, context, funcDecl);

            var last_param = funcDecl.Parameters.LastOrDefault();
            if(last_param != null && last_param.IsVariadic){
                var index = funcDecl.Parameters.Count;
                var param_builder = method_builder.DefineParameter(index, ParameterAttributes.None, last_param.Name);
                var attribute_ctor = typeof(ParamArrayAttribute).GetConstructor(Type.EmptyTypes);
                param_builder.SetCustomAttribute(new CustomAttributeBuilder(attribute_ctor, Type.EmptyTypes));
            }

            context.CustomAttributeSetter = method_builder.SetCustomAttribute;
            context.AttributeTarget = AttributeTargets.Method;
            // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
            funcDecl.Attribute.AcceptWalker(this, context);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        void DefineField(FieldDeclaration fieldDecl, CodeGeneratorContext context)
        {
            FieldAttributes attr = FieldAttributes.Assembly | FieldAttributes.Static;

            // Don't set InitOnly flag or we'll fail to initialize the fields
            //if(fieldDecl.Modifiers.HasFlag(Modifiers.Immutable))
            //    attr |= FieldAttributes.InitOnly;

            if(fieldDecl.Modifiers.HasFlag(Modifiers.Export)){
                // FieldAttributes.Public contains FamANDAssem and Family and FieldAttributes.Assembly contains Private and FamANDAssem
                attr |= FieldAttributes.Public;
                attr ^= FieldAttributes.Private;
            }

            foreach(var init in fieldDecl.Initializers){
                if(init.Pattern is PatternWithType inner && inner.Pattern is IdentifierPattern ident_pat){
                    var type = CSharpCompilerHelpers.GetNativeType(ident_pat.Identifier.Type, true);
                    var field_builder = context.LazyTypeBuilder.DefineField(init.Name, type, !Expression.IsNullNode(init.Initializer), attr);

                    context.CustomAttributeSetter = field_builder.SetCustomAttribute;
                    context.AttributeTarget = AttributeTargets.Field;
                    // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
                    fieldDecl.Attribute.AcceptWalker(this, context);

                    // PropertyOrField is needed so that we can refer to it easily later on
                    // We can do this because we wouldn't need to be able to return module classes
                    AddSymbol(ident_pat.Identifier, new ExpressoSymbol{FieldBuilder = field_builder, PropertyOrField = field_builder});
                }else{
                    throw new GeneratorException("Invalid module field!");
                }
            }
        }

        Type DetermineType(AstType astType, CodeGeneratorContext context)
        {
            if(context.GenericTypeParameterReplacer != null)
                return astType.AcceptTypeWalker(context.GenericTypeParameterReplacer);
            else
                return CSharpCompilerHelpers.GetNativeTypeWithSelf(astType, context.LazyTypeBuilder.Name);
        }

        Type[] ConvertToNativeTypes(AstNodeCollection<AstType> types, CodeGeneratorContext context)
        {
            var native_types = types.Select(t => DetermineType(t, context)).ToArray();
            if(native_types.Any(t => t == null))
                return null;
            else
                return native_types;
        }

        /// <summary>
        /// Determines whether every last statement is a return statement.
        /// </summary>
        /// <returns><c>true</c>, if return was contained, <c>false</c> otherwise.</returns>
        /// <param name="stmt">Statement.</param>
        bool EndsWithReturnOrThrow(Statement stmt)
        {
            if(stmt is BlockStatement block){
                return EndsWithReturnOrThrow(block.Statements.Last());
            }else if(stmt is ReturnStatement || stmt is ThrowStatement){
                return true;
            }else if(stmt is MatchStatement){
                // Always returns true because VisitMatchStatement has already inspected this
                return true;
            }else if(stmt is IfStatement if_stmt){
                return EndsWithReturnOrThrow(if_stmt.TrueBlock) && if_stmt.FalseStatement.IsNull || EndsWithReturnOrThrow(if_stmt.FalseStatement);
            }else{
                return false;
            }
        }

        bool IsEnumLastClause(MatchPatternClause matchClause)
        {
            return matchClause.Patterns.First() is DestructuringPattern destructuring && destructuring.IsEnum && matchClause.NextSibling == null;
        }

        bool MatchIsLastStatement(MatchPatternClause matchClause)
        {
            return matchClause.Parent.NextSibling == null;
        }

        FieldInfo TreatField(Type type, LazyTypeBuilder selfType, Identifier identifier, string fieldName)
        {
            var symbol = GetRuntimeSymbol(identifier);
            if(type.Name == selfType.Name && symbol != null)
                return (FieldInfo)symbol.PropertyOrField;
            else
                return type.GetField(fieldName);
        }

        Type TreatTypeBuilderType(string selfTypeName, Type originalType)
        {
            if(!originalType.IsGenericType || originalType.IsConstructedGenericType)
                return originalType;

            foreach(var type_arg in originalType.GenericTypeArguments){
                if(IsSelfType(selfTypeName, type_arg)){
                    var ast_type = ExpressoCompilerHelpers.GetExpressoType(originalType);
                    return CSharpCompilerHelpers.GetNativeTypeWithSelf(ast_type, selfTypeName);
                }
            }

            return originalType;
        }

        bool IsSelfType(string selfTypeName, Type type)
        {
            return type.Name == selfTypeName;
        }

        bool IsInspectingComplexConditionExpression(BinaryExpression binaryExpression, bool hasSeenConditionAnd = false, bool hasSeenConditionOr = false)
        {
            if(binaryExpression == null)
                return hasSeenConditionAnd && hasSeenConditionOr;

            if(binaryExpression.Operator == OperatorType.ConditionalAnd)
                hasSeenConditionAnd = true;
            else if(binaryExpression.Operator == OperatorType.ConditionalOr)
                hasSeenConditionOr = true;

            var paren = binaryExpression.Right as ParenthesizedExpression;
            var prev = binaryExpression.Right as ParenthesizedExpression;
            while(paren != null){
                prev = paren;
                paren = paren.Expression as ParenthesizedExpression;
            }

            return IsInspectingComplexConditionExpression(((prev != null) ? prev.Expression : binaryExpression.Right) as BinaryExpression, hasSeenConditionAnd, hasSeenConditionOr);
        }

        bool IsRootConditionExpression(BinaryExpression binaryExpression)
        {
            var parent_binary = binaryExpression.Ancestors.OfType<BinaryExpression>().FirstOrDefault();
            return (parent_binary == null) || parent_binary.Operator != OperatorType.ConditionalOr && parent_binary.Operator != OperatorType.ConditionalAnd;
        }

        #region Emit methods

        void EmitDefaultValue(AstType astType)
        {
            if(astType is PrimitiveType primitive){
                switch(primitive.Name){
                case "int":
                    EmitInt(0);
                    break;

                case "uint":
                    EmitUInt(0u);
                    break;

                case "bool":
                    EmitObject(false);
                    break;

                case "float":
                    EmitObject(0.0f);
                    break;

                case "double":
                    EmitObject(0.0);
                    break;

                case "char":
                    EmitObject('\0');
                    break;

                case "string":
                    EmitObject("");
                    break;

                case "intseq":
                    il_generator.Emit(OpCodes.Ldnull);
                    break;
                }
            }else{
                // Exclude `ParameterType`s because a generic type can be a value type
                if(!ExpressoCompilerHelpers.IsVoidType(astType) && !(astType is ParameterType))
                    il_generator.Emit(OpCodes.Ldnull);
            }
        }

        void EmitObject(object obj)
        {
            if(obj is string str){
                il_generator.Emit(OpCodes.Ldstr, str);
            }else if(obj is byte by){
                il_generator.Emit(OpCodes.Ldc_I4, by);
                il_generator.Emit(OpCodes.Conv_I1);
            }else if(obj is char ch){
                EmitInt(ch);
            }else if(obj is int integer){
                EmitInt(integer);
            }else if(obj is uint unsigned){
                EmitUInt(unsigned);
            }else if(obj is float f){
                il_generator.Emit(OpCodes.Ldc_R4, f);
            }else if(obj is double d){
                il_generator.Emit(OpCodes.Ldc_R8, d);
            }else if(obj is bool b){
                il_generator.Emit(b ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
            }else if(obj is BigInteger big_int){
                var parse_method = typeof(BigInteger).GetMethod("Parse", new []{typeof(string)});
                il_generator.Emit(OpCodes.Ldstr, big_int.ToString());
                il_generator.Emit(OpCodes.Call, parse_method);
            }else{
                throw new ArgumentException($"Unknown object type!: {obj.GetType().Name}");
            }
        }

        void EmitCast(Type originalType, Type type)
        {
            if(type == typeof(object) && (!originalType.IsClass || originalType.IsGenericParameter))
                il_generator.Emit(OpCodes.Box, originalType);
            else if(type == typeof(byte))
                il_generator.Emit(OpCodes.Conv_U1);
            else if(type == typeof(char))
                il_generator.Emit(OpCodes.Conv_I2);
            else if(type == typeof(int))
                il_generator.Emit(OpCodes.Conv_I4);
            else if(type == typeof(uint))
                il_generator.Emit(OpCodes.Conv_U4);
            else if(type == typeof(long))
                il_generator.Emit(OpCodes.Conv_I8);
            else if(type == typeof(ulong))
                il_generator.Emit(OpCodes.Conv_U8);
            else if(type == typeof(float))
                il_generator.Emit(OpCodes.Conv_R4);
            else if(type == typeof(double))
                il_generator.Emit(OpCodes.Conv_R8);
            else
                il_generator.Emit(OpCodes.Castclass, type);
        }

        void EmitCall(MethodInfo method)
        {
            var base_type = method.DeclaringType.BaseType;
            var parent_method = (base_type != typeof(object) && method.IsVirtual && !method.Attributes.HasFlag(MethodAttributes.NewSlot)) ?
                base_type.GetMethod(method.Name, method.GetParameters().Select(p => p.ParameterType).ToArray()) : null;
            // it is static or the current type, in the first place, declares the method(virtual but not override)
            if(method.IsStatic || method.DeclaringType.IsClass && method.IsVirtual &&
                !(parent_method != null && parent_method.GetParameters().SequenceEqual(method.GetParameters()))){
                il_generator.Emit(OpCodes.Call, method);
            }else{
                il_generator.Emit(OpCodes.Callvirt, method);
            }
        }

        void EmitLoadLocal(LocalBuilder localBuilder, bool expectsReference)
        {
            if(expectsReference)
                il_generator.Emit(OpCodes.Ldloca_S, localBuilder.LocalIndex);
            else
                il_generator.Emit(OpCodes.Ldloc, localBuilder);
        }

        void EmitLoadField(FieldInfo field, bool isCallOnField = false)
        {
            var field_type = field.FieldType;
            if(field.IsStatic)
                il_generator.Emit(OpCodes.Ldsfld, field);
            else if(isCallOnField && field_type.IsGenericParameter) // Expects self.field.someMethod() where field is of some parameter type like `T
                il_generator.Emit(OpCodes.Ldflda, field);
            else
                il_generator.Emit(OpCodes.Ldfld, field);
        }

        void EmitSet(MemberInfo member, LocalBuilder localBuilder, int parameterIndex, Type targetType, bool isMethod = false)
        {
            if(member is PropertyInfo property)
                EmitCall(property.SetMethod);
            else if(member is FieldInfo field)
                EmitSetField(field);
            else if(isMethod && parameterIndex == 0)
                il_generator.Emit(OpCodes.Starg, 0);    // for assignment to self
            else if(localBuilder != null)
                il_generator.Emit(OpCodes.Stloc, localBuilder);
            else if(parameterIndex > 0 || !isMethod && parameterIndex == 0)
                EmitSetInd(targetType);
            else
                EmitSetElem(targetType);
        }

        void EmitSetField(FieldInfo field)
        {
            if(field.IsStatic)
                il_generator.Emit(OpCodes.Stsfld, field);
            else
                il_generator.Emit(OpCodes.Stfld, field);
        }

        void EmitSetInd(Type targetType)
        {
            if(!targetType.IsByRef)
                throw new InvalidOperationException("Expected a ref type");

            var elem_type = targetType.GetElementType();
            if(elem_type == typeof(byte))
                il_generator.Emit(OpCodes.Stind_I1);
            else if(elem_type == typeof(int) || elem_type == typeof(uint))
                il_generator.Emit(OpCodes.Stind_I4);
            else if(elem_type == typeof(float))
                il_generator.Emit(OpCodes.Stind_R4);
            else if(elem_type == typeof(double))
                il_generator.Emit(OpCodes.Stind_R8);
            else
                il_generator.Emit(OpCodes.Stind_Ref);
        }

        void EmitNewArray(Type elementType, IEnumerable<Expression> initializers, Action<Expression> action)
        {
            var count = initializers.Count();
            EmitInt(count);
            il_generator.Emit(OpCodes.Newarr, elementType);
            foreach(var pair in Enumerable.Range(0, count).Zip(initializers, (l, r) => new {Index = l, Initializer = r})){
                il_generator.Emit(OpCodes.Dup);
                EmitInt(pair.Index);
                action(pair.Initializer);
                EmitSetElem(elementType);
            }
        }

        void EmitInt(int i)
        {
            switch(i){
            case -1:
                il_generator.Emit(OpCodes.Ldc_I4_M1);
                break;

            case 0:
                il_generator.Emit(OpCodes.Ldc_I4_0);
                break;

            case 1:
                il_generator.Emit(OpCodes.Ldc_I4_1);
                break;

            case 2:
                il_generator.Emit(OpCodes.Ldc_I4_2);
                break;

            case 3:
                il_generator.Emit(OpCodes.Ldc_I4_3);
                break;

            case 4:
                il_generator.Emit(OpCodes.Ldc_I4_4);
                break;

            case 5:
                il_generator.Emit(OpCodes.Ldc_I4_5);
                break;

            case 6:
                il_generator.Emit(OpCodes.Ldc_I4_6);
                break;

            case 7:
                il_generator.Emit(OpCodes.Ldc_I4_7);
                break;

            case 8:
                il_generator.Emit(OpCodes.Ldc_I4_8);
                break;

            default:
                if(i <= sbyte.MaxValue)
                    il_generator.Emit(OpCodes.Ldc_I4_S, (sbyte)i);
                else
                    il_generator.Emit(OpCodes.Ldc_I4, i);
                
                break;
            }
        }

        void EmitUInt(uint n)
        {
            if(n <= sbyte.MaxValue)
                il_generator.Emit(OpCodes.Ldc_I4, n);
            else
                EmitInt((int)n);

            il_generator.Emit(OpCodes.Conv_I4);
        }

        void EmitBrTrue(int loopOffset, Label loopStart)
        {
            il_generator.Emit((Math.Abs(loopOffset) < 128) ? OpCodes.Brtrue_S : OpCodes.Brtrue, loopStart);
        }

        void LoadArg(int i)
        {
            switch(i){
            case 0:
                il_generator.Emit(OpCodes.Ldarg_0);
                break;

            case 1:
                il_generator.Emit(OpCodes.Ldarg_1);
                break;

            case 2:
                il_generator.Emit(OpCodes.Ldarg_2);
                break;

            case 3:
                il_generator.Emit(OpCodes.Ldarg_3);
                break;

            default:
                if(i <= short.MaxValue)
                    il_generator.Emit(OpCodes.Ldarg_S, (short)i);
                else
                    il_generator.Emit(OpCodes.Ldarg, i);

                break;
            }
        }

        Type EmitListInitForList(Type elementType, IEnumerable<Expression> initializers, CodeGeneratorContext context)
        {
            var list_type = typeof(List<>);
            var generic_type = list_type.MakeGenericType(elementType);
            var add_method = generic_type.GetMethod("Add");
            var ctor = generic_type.GetConstructor(new Type[]{});

            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;
            il_generator.Emit(OpCodes.Newobj, ctor);
            foreach(var initializer in initializers){
                il_generator.Emit(OpCodes.Dup);
                initializer.AcceptWalker(this, context);
                il_generator.Emit(OpCodes.Callvirt, add_method);
            }
            context.OperationTypeOnIdentifier = prev_op_type;

            return generic_type;
        }

        Type EmitListInitForDictionary(Type keyType, Type valueType, IEnumerable<KeyValueLikeExpression> initializers, CodeGeneratorContext context)
        {
            var dict_type = typeof(Dictionary<,>);
            var generic_type = dict_type.MakeGenericType(keyType, valueType);
            var add_method = generic_type.GetMethod("Add");
            var ctor = generic_type.GetConstructor(new Type[]{});

            var prev_op_type = context.OperationTypeOnIdentifier;
            context.OperationTypeOnIdentifier = OperationType.Load;
            il_generator.Emit(OpCodes.Newobj, ctor);
            foreach(var initializer in initializers){
                il_generator.Emit(OpCodes.Dup);
                initializer.AcceptWalker(this, context);
                il_generator.Emit(OpCodes.Callvirt, add_method);
            }
            context.OperationTypeOnIdentifier = prev_op_type;

            return generic_type;
        }

        void EmitSetElem(Type targetElementType)
        {
            if(!targetElementType.IsPrimitive)
                il_generator.Emit(OpCodes.Stelem_Ref);
            else if(targetElementType == typeof(byte) || targetElementType == typeof(sbyte))
                il_generator.Emit(OpCodes.Stelem_I1);
            else if(targetElementType == typeof(short) || targetElementType == typeof(ushort) || targetElementType == typeof(char))
                il_generator.Emit(OpCodes.Stelem_I2);
            else if(targetElementType == typeof(int) || targetElementType == typeof(uint))
                il_generator.Emit(OpCodes.Stelem_I4);
            else if(targetElementType == typeof(long) || targetElementType == typeof(ulong))
                il_generator.Emit(OpCodes.Stelem_I8);
            else if(targetElementType == typeof(float))
                il_generator.Emit(OpCodes.Stelem_R4);
            else if(targetElementType == typeof(double))
                il_generator.Emit(OpCodes.Stelem_R8);
            else
                il_generator.Emit(OpCodes.Stelem, targetElementType);
        }

        void EmitLoadElem(Type targetElementType)
        {
            if(!targetElementType.IsPrimitive)
                il_generator.Emit(OpCodes.Ldelem_Ref);
            else if(targetElementType == typeof(byte))
                il_generator.Emit(OpCodes.Ldelem_U1);
            else if(targetElementType == typeof(sbyte))
                il_generator.Emit(OpCodes.Ldelem_I1);
            else if(targetElementType == typeof(ushort))
                il_generator.Emit(OpCodes.Ldelem_U2);
            else if(targetElementType == typeof(short))
                il_generator.Emit(OpCodes.Ldelem_I2);
            else if(targetElementType == typeof(uint))
                il_generator.Emit(OpCodes.Ldelem_U4);
            else if(targetElementType == typeof(int))
                il_generator.Emit(OpCodes.Ldelem_I4);
            else if(targetElementType == typeof(long) || targetElementType == typeof(ulong))
                il_generator.Emit(OpCodes.Ldelem_I8);
            else if(targetElementType == typeof(float))
                il_generator.Emit(OpCodes.Ldelem_R4);
            else if(targetElementType == typeof(double))
                il_generator.Emit(OpCodes.Ldelem_R8);
            else
                il_generator.Emit(OpCodes.Ldelem, targetElementType);
        }
        #endregion

        AstType ResolveReturnType(AstType type)
        {
            // Functions that only throw will return SimpleType.Null
            if(type.IsNull)
                return AstType.MakeSimpleType("tuple");
            else
                return type;
        }
        #endregion
    }
}

