﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

using Expresso.Ast;
using Expresso.Runtime.Builtins;
using Expresso.TypeSystem;

namespace Expresso.CodeGen
{
    using CSharpExpr = System.Linq.Expressions.Expression;

    class GenericMethodComparer : IEqualityComparer<Type>
    {
        public bool Equals(Type x, Type y)
        {
            return x.IsGenericType && y.IsGenericType || x.Assembly == y.Assembly && x.Namespace == y.Namespace && x.Name == y.Name;
        }

        public int GetHashCode(Type obj)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Contains helper methods for Expresso compilation.
    /// </summary>
    public static class CSharpCompilerHelpers
    {
        static Dictionary<string, (string, uint)> SpecialNamesMap = new Dictionary<string, (string, uint)>{
            {"intseq", ("Expresso.Runtime.Builtins.ExpressoIntegerSequence", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 0u)},
            {"slice", ("Expresso.Runtime.Builtins.Slice", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 1u)},
            {"bool", ("System.Boolean", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 2u)},
            {"int", ("System.Int32", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 3u)},
            {"uint", ("System.UInt32", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 4u)},
            {"float", ("System.Single", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 5u)},
            {"double", ("System.Double", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 6u)},
            {"char", ("System.Char", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 7u)},
            {"byte", ("System.Byte", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 8u)},
            {"string", ("System.String", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 9u)},
            {"array", ("System.Array", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 10u)},
            {"vector", ("System.Collections.Generic.List", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 11u)},
            {"tuple", ("System.Tuple", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 12u)},
            {"dictionary", ("System.Collections.Generic.Dictionary", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 13u)},
            {"bigint", ("System.Numerics.BigInteger", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 14u)},
            {"object", ("System.Object", ExpressoCompilerHelpers.StartOfPrimitiveTypeIdentifierId + 15u)}
        };

        public const string HiddenMemberPrefix = "<>__";

        /// <summary>
        /// Helper method to convert a PrimitiveType to a C#'s type.
        /// </summary>
        /// <returns>A <see cref="System.Type"/> object.</returns>
        /// <param name="type">Type.</param>
        public static Type GetPrimitiveType(PrimitiveType type)
        {
            if(type == null)
                throw new ArgumentNullException(nameof(type));

            switch(type.KnownTypeCode){
            case KnownTypeCode.Bool:
                return typeof(bool);

            case KnownTypeCode.Int:
                return typeof(int);

            case KnownTypeCode.UInt:
                return typeof(uint);

            case KnownTypeCode.Byte:
                return typeof(byte);

            case KnownTypeCode.Char:
                return typeof(char);

            case KnownTypeCode.Float:
                return typeof(float);

            case KnownTypeCode.Double:
                return typeof(double);

            case KnownTypeCode.BigInteger:
                return typeof(BigInteger);

            case KnownTypeCode.String:
                return typeof(string);

            case KnownTypeCode.Array:
                return typeof(Array);

            case KnownTypeCode.Vector:
                return typeof(List<>);

            case KnownTypeCode.Tuple:
                return typeof(Tuple);

            case KnownTypeCode.Dictionary:
                return typeof(Dictionary<,>);

            case KnownTypeCode.IntSeq:
                return typeof(ExpressoIntegerSequence);

            case KnownTypeCode.Slice:
                return typeof(Slice<,>);

            default:
                return typeof(object);
            }
        }

        /// <summary>
        /// Gets `astType` as a native type.
        /// </summary>
        /// <returns>The native type with all type parameters substituted if it's a <see cref="SimpleType"/> with type arguments.</returns>
        /// <param name="astType">Ast type.</param>
        public static Type GetNativeType(AstType astType, bool retrievingTypeArgs = false, bool shouldSubstituteTypeArguments = true, bool isRecursiveCall = false)
        {
            // retrievingTypeArgs is needed because it's needed in GetNativeTypeWithSelf

            // Ignore these types because they are different types depending on the type arguments
            if(astType.Name != "tuple" && astType.Name != "array" && astType.IdentifierNode.IdentifierId != 0u
               && CodeGenerator.Symbols.TryGetValue(astType.IdentifierNode.IdentifierId, out var symbol)){
                if(retrievingTypeArgs && astType is SimpleType simple && symbol.Type.IsGenericTypeDefinition && simple.TypeArguments.Any() 
                   && !simple.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                    var type = SubstituteTypeArguments(symbol.Type, simple.TypeArguments);
                    return type;
                }
                return symbol.Type;
            }

            if(astType is PrimitiveType primitive)
                return GetPrimitiveType(primitive);

            if(astType is SimpleType simple2){
                var name = ConvertToDotNetTypeName(simple2.Identifier);
                if(simple2.Identifier == "tuple" && !simple2.TypeArguments.Any()){
                    if(!(simple2.Parent is Identifier))
                        return typeof(void);
                    else
                        return typeof(Unit);
                }

                if(simple2.Identifier == "array"){
                    if(!simple2.TypeArguments.Any())
                        return typeof(Array);

                    var type_arg = simple2.TypeArguments
                                          .Select(ta => GetNativeType(ta, true, true, true))
                                          .First();

                    var array = Array.CreateInstance(type_arg, 1);
                    return array.GetType();
                }

                /*if(!simple2.IdentifierNode.Type.IsNull && (simple2.IdentifierNode.Type is SimpleType || simple2.IdentifierNode.Type is MemberType)){
                    var tmp_type = GetNativeType(simple2.IdentifierNode.Type, true, true, true);
                    if(tmp_type != null)
                        return tmp_type;
                }*/

                // ignore type arguments when simple2 represents a tuple-like enum
                if(retrievingTypeArgs || simple2.Name == "tuple")
                    name += simple2.TypeArguments.Any() ? "`" + simple2.TypeArguments.Count : "";

                Type type = null;
                var asms = AppDomain.CurrentDomain.GetAssemblies();
                foreach(var asm in asms){
                    type = asm.GetType(name);
                    if(type != null)
                        break;
                }

                if(shouldSubstituteTypeArguments && type != null && type.IsGenericTypeDefinition && simple2.TypeArguments.Any()
                    && !simple2.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                    return SubstituteTypeArguments(type, simple2.TypeArguments);
                }

                if(type == null && (!retrievingTypeArgs || !shouldSubstituteTypeArguments))
                    return GetNativeType(astType, true, true, true);
                else if(type == null && isRecursiveCall)
                    throw new GeneratorException($"The type `{simple2}`(`{name}`) is not found!", simple2);

                return type;
            }

            if(astType is MemberType member){
                Type type = null;
                foreach(var asm in AppDomain.CurrentDomain.GetAssemblies()){
                    var types = GetTypes(asm);
                    type = types.FirstOrDefault(t => t.Name.StartsWith(member.MemberName, StringComparison.CurrentCulture));
                    if(type != null)
                        break;
                }

                if(type == null)
                    throw new GeneratorException($"The type `{member}` is not found!", member);

                return type;
            }

            if(astType is FunctionType func){
                var param_types = func.Parameters
                                      .Select(p => GetNativeType(p, true, true, true))
                                      .ToArray();

                var return_type = GetNativeType(func.ReturnType, true, true, true);
                if(return_type == typeof(void)){
                    if(param_types.Any(t => t == null))
                        return GetActionType(param_types.Length);
                    else
                        return CSharpExpr.GetActionType(param_types);
                }else{
                    var type_args = param_types.Concat(new []{return_type})
                                               .ToArray();

                    if(type_args.Any(t => t == null))
                        return GetFuncType(param_types.Length);
                    else
                        return CSharpExpr.GetFuncType(type_args);
                }
            }

            if(astType is ReferenceType reference){
                var native_type = GetNativeType(reference.BaseType, false, true, true);
                return native_type.MakeByRefType();
            }

            if(astType is ParameterType param_type){
                // We can't create a type parameter on its own
                // so return a null to indicate that
                return null;
            }

            if(astType is KeyValueType keyvalue)
                return GetNativeType(keyvalue.ValueType, false, true, true);

            if(astType is FunctionParameterType func_param_type)
                return GetNativeType(func_param_type.Type, retrievingTypeArgs, shouldSubstituteTypeArguments, true);

            if(astType is PlaceholderType placeholder)
                throw new GeneratorException("Unresolved type found when retrieving a native type!");

            throw new GeneratorException($"Unknown AstType: {astType}({astType.GetType().Name})");
        }

        /// <summary>
        /// Retrieves a native type, giving priority to the other type if `astType` is a <see cref="SimpleType"/>.
        /// </summary>
        /// <param name="astType"></param>
        /// <param name="retrievingTypeArgs"></param>
        /// <returns></returns>
        public static Type GetNativeOtherType(AstType astType, bool retrievingTypeArgs = false)
        {
            // This part of code is needed because there are references that we should first look at the IdentifierId
            // Ignore these types because they are different types depending on the type arguments
            if(astType.Name != "tuple" && astType.Name != "array" && astType.IdentifierNode.IdentifierId != 0u
               && CodeGenerator.Symbols.TryGetValue(astType.IdentifierNode.IdentifierId, out var symbol)){
                if(retrievingTypeArgs && astType is SimpleType simple && symbol.Type.IsGenericTypeDefinition && simple.TypeArguments.Any() 
                   && !simple.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                    var type = SubstituteTypeArguments(symbol.Type, simple.TypeArguments);
                    return type;
                }
                return symbol.Type;
            }

            if(astType is SimpleType simple2 && !simple2.IdentifierNode.Type.IsNull)
                return GetNativeType(simple2.IdentifierNode.Type, retrievingTypeArgs);
            else
                return GetNativeType(astType, retrievingTypeArgs);
        }

        /// <summary>
        /// Works the same as <see cref="GetNativeType(AstType, bool, bool, bool)"/> except that it searches for a <see cref="Type"/> which represents the self type.
        /// </summary>
        /// <returns>The native type with self.</returns>
        /// <param name="astType">Ast type.</param>
        /// <param name="selfTypeName">Self type name.</param>
        /// <param name="retrievingTypeArgs">If set to <c>true</c> retrieving type arguments.</param>
        public static Type GetNativeTypeWithSelf(AstType astType, string selfTypeName, bool retrievingTypeArgs = false)
        {
            if(selfTypeName != null && astType.IdentifierNode.Type.IsNull && astType.Name == selfTypeName)
                return GetNativeType(astType);

            if(astType is SimpleType simple && ExpressoCompilerHelpers.IsExpressoEnumType(simple))
                return GetNativeTypeOfExpressoEnum(astType, retrievingTypeArgs);

            // FIXME: Can be deleted: 2019/9/13(including the comment)
            // !simple.TypeArguments.Any(): fix for types like vector<T> (- vector
            if(astType is SimpleType simple3 && ExpressoCompilerHelpers.OtherTypeIsForeignType(simple3) && simple3.IdentifierNode.IdentifierId == 0u/*!simple.IdentifierNode.Type.IsNull && !simple3.TypeArguments.Any()*/
                && (simple3.IdentifierNode.Type is SimpleType || simple3.IdentifierNode.Type is MemberType)){
                // retrievingTypeArgs should be true for this call
                // because it only retrieves other types such as tuples(as with other types of enums)
                var tmp_type = GetNativeTypeWithSelf(simple3.IdentifierNode.Type, selfTypeName, true);
                if(tmp_type != null && tmp_type.IsGenericTypeDefinition && simple3.TypeArguments.Any() &&
                   !simple3.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                    return SubstituteTypeArgumentsWithType(tmp_type, simple3.TypeArguments);
                }else if(tmp_type != null){
                    return tmp_type;
                }
            }

            var type = GetNativeType(astType, retrievingTypeArgs, false, false);
            if(type != null && type.IsGenericTypeDefinition && astType is SimpleType simple2
               && simple2.TypeArguments.Any() && !simple2.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                //var self_type = (selfTypeName == null) ? null : GetNativeType(AstType.MakeSimpleType(selfTypeName));
                return SubstituteTypeArgumentsWithType(type, simple2.TypeArguments, selfTypeName);
            }

            return type;
        }

        // TODO: implement it properly(I want to make it properly return a necessary Type)
        static Type GetNativeTypeOfExpressoEnum(AstType astType, bool retrievingTypeArgs = false)
        {
            // FIXME: Can be deleted: 2019/9/13(including the comment)
            // !simple.TypeArguments.Any(): fix for types like vector<T> (- vector
            if(astType is SimpleType simple && !simple.IdentifierNode.Type.IsNull && !simple.TypeArguments.Any()
                && (simple.IdentifierNode.Type is SimpleType || simple.IdentifierNode.Type is MemberType)){
                // retrievingTypeArgs should be true for this call
                // because it only retrieves other types such as tuples(as with other types of enums)
                var tmp_type = GetNativeTypeOfExpressoEnum(simple.IdentifierNode.Type, true);
                if(tmp_type != null && tmp_type.IsGenericTypeDefinition && simple.TypeArguments.Any() &&
                   !simple.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                    return SubstituteTypeArgumentsWithType(tmp_type, simple.TypeArguments);
                }else if(tmp_type != null){
                    return tmp_type;
                }
            }

            var type = GetNativeType(astType, retrievingTypeArgs, false, false);
            if(type != null && type.IsGenericTypeDefinition && astType is SimpleType simple2
               && simple2.TypeArguments.Any() && !simple2.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                //var self_type = (selfTypeName == null) ? null : GetNativeType(AstType.MakeSimpleType(selfTypeName));
                return SubstituteTypeArgumentsWithType(type, simple2.TypeArguments/*, selfTypeName*/);
            }

            return type;
        }

        /// <summary>
        /// Works the same as <see cref="GetNativeType(AstType, bool, bool, bool)"/> except that it searches for a <see cref="TypeBuilder"/> which represents the self type.
        /// </summary>
        /// <returns>The native type with self.</returns>
        /// <param name="astType">Ast type.</param>
        /// <param name="selfTypeBuilder">Self type name.</param>
        /// <param name="retrievingTypeArgs">If set to <c>true</c> retrieving type arguments.</param>
        public static Type GetNativeTypeWithSelfTypeBuilder(AstType astType, TypeBuilder selfTypeBuilder, bool retrievingTypeArgs = false)
        {
            if(selfTypeBuilder != null && selfTypeBuilder.Name == astType.Name && astType.IdentifierNode.Type.IsNull)
                return selfTypeBuilder;

            if(astType is SimpleType simple && !simple.IdentifierNode.Type.IsNull && (simple.IdentifierNode.Type is SimpleType || simple.IdentifierNode.Type is MemberType)) {
                var tmp_type = GetNativeTypeWithSelfTypeBuilder(simple.IdentifierNode.Type, selfTypeBuilder, true);
                if(tmp_type != null && tmp_type.IsGenericTypeDefinition && simple.TypeArguments.Any()
                   && !simple.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                    return SubstituteTypeArguments(tmp_type, simple.TypeArguments);
                }else if(tmp_type != null){
                    return tmp_type;
                }
            }

            var type = GetNativeType(astType, retrievingTypeArgs, false, false);
            if(type != null && type.IsGenericTypeDefinition && astType is SimpleType simple2
               && simple2.TypeArguments.Any() && !simple2.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                return SubstituteTypeArguments(type, simple2.TypeArguments, selfTypeBuilder);
            }

            return type;
        }

        /// <summary>
        /// Gets `astType` as a native type from the specific assembly.
        /// </summary>
        /// <returns>The native type with all type parameters substituted if it's a <see cref="SimpleType"/> with type arguments.</returns>
        /// <param name="astType">Ast type.</param>
        public static Type GetNativeTypeFromAssembly(AstType astType, string assemblyPath)
        {
            if(astType is SimpleType simple){
                var name = ConvertToDotNetTypeName(simple.Identifier);
                // FIXME: Can be deleted: 2019/9/13
                /*if(simple2.Identifier == "tuple" && !simple2.TypeArguments.Any()){
                    if(!(simple2.Parent is Identifier))
                        return typeof(void);
                    else
                        return typeof(Unit);
                }

                if(simple2.Identifier == "array"){
                    if(!simple2.TypeArguments.Any())
                        return typeof(Array);

                    var type_arg = simple2.TypeArguments
                                          .Select(ta => GetNativeType(ta, true, true, true))
                                          .First();

                    var array = Array.CreateInstance(type_arg, 1);
                    return array.GetType();
                }*/

                name += simple.TypeArguments.Any() ? "`" + simple.TypeArguments.Count : "";

                Type type = null;
                var asms = AppDomain.CurrentDomain.GetAssemblies();
                foreach(var asm in asms){
                    if(!CheckAssemblyName(asm.FullName, assemblyPath))
                        continue;

                    type = asm.GetType(name);
                    if(type != null)
                        break;
                }

                if(type != null && type.IsGenericTypeDefinition && simple.TypeArguments.Any()
                    && !simple.TypeArguments.Any(ta => ta is PlaceholderType || ta is ParameterType)){
                    return SubstituteTypeArguments(type, simple.TypeArguments);
                }

                if(type == null)
                    throw new GeneratorException($"The type `{simple}`(`{name}`) is not found!", simple);

                return type;
            }

            throw new GeneratorException($"Unknown AstType: {astType}({astType.GetType().Name})");
        }

        static bool CheckAssemblyName(string assemblyFullName, string assemblymPath)
        {
            var actual_asm_name = assemblyFullName.Substring(0, assemblyFullName.IndexOf(",", StringComparison.CurrentCulture));

            var slash_index = assemblymPath.LastIndexOf("/", StringComparison.CurrentCulture);
            if(slash_index == -1)
                throw new GeneratorException("An assembly path must contain at least one '/'!");

            var path_substr = assemblymPath.Substring(slash_index + "/".Length);
            var asm_name = path_substr.Substring(0, path_substr.IndexOf(".", StringComparison.CurrentCulture));

            return asm_name == actual_asm_name;
        }

        static Type SubstituteTypeArguments(Type genericType, IEnumerable<AstType> typeArgs, TypeBuilder selfTypeBuilder = null)
        {
            if(!typeArgs.Any())
                throw new InvalidOperationException("SubstituteTypeArguments requires that typeArgs contains, at least, 1 argument");

            var type_args = typeArgs.Select(ta => GetNativeTypeWithSelfTypeBuilder(ta, selfTypeBuilder)).ToArray();
            if(type_args.All(et => et != null))
                return genericType.MakeGenericType(type_args);
            else
                throw new InvalidOperationException($"Can not substitute some of the type arguments: {genericType.Name}<{ExpressoCompilerHelpers.StringifyList(typeArgs)}>");
        }

        static Type SubstituteTypeArgumentsWithType(Type genericType, IEnumerable<AstType> typeArgs, string selfTypeName = null)
        {
            if(!typeArgs.Any())
                throw new InvalidOperationException("SubstituteTypeArgumentsWithType requires that typeArgs contains, at least, 1 argument");

            var type_args = typeArgs.Select(ta => GetNativeTypeWithSelf(ta, (selfTypeName == null) ? null : selfTypeName)).ToArray();
            if(type_args.All(et => et != null))
                return genericType.MakeGenericType(type_args);
            else
                throw new InvalidOperationException($"Can not substitute some of the type arguments: {genericType.Name}<{ExpressoCompilerHelpers.StringifyList(typeArgs)}>");
        }

        /// <summary>
        /// Returns the container type that `type` represents.
        /// </summary>
        /// <returns>The container type.</returns>
        /// <param name="type">Type.</param>
        public static Type GetContainerType(SimpleType type)
        {
            switch(type.Identifier){
            case "vector":
                return typeof(List<>);

            case "array":
                return typeof(Array);

            case "dictionary":
                return typeof(Dictionary<,>);

            case "tuple":
                return typeof(Tuple);

            default:
                throw new GeneratorException("Unknown container type");
            }
        }

        public static Type GuessTupleType(IEnumerable<Type> elementTypes)
        {
            var types = elementTypes.ToArray();
            switch(types.Length){
            case 1:
                return typeof(Tuple<>).MakeGenericType(types);

            case 2:
                return typeof(Tuple<,>).MakeGenericType(types);

            case 3:
                return typeof(Tuple<,,>).MakeGenericType(types);

            case 4:
                return typeof(Tuple<,,,>).MakeGenericType(types);

            case 5:
                return typeof(Tuple<,,,,>).MakeGenericType(types);

            case 6:
                return typeof(Tuple<,,,,,>).MakeGenericType(types);

            case 7:
                return typeof(Tuple<,,,,,,>).MakeGenericType(types);

            case 8:
                return typeof(Tuple<,,,,,,,>).MakeGenericType(types);
            
            default:
                throw new GeneratorException("Expresso on .NET doesn't support that many tuple elements");
            }
        }

        public static Assembly GetAssembly(AssemblyName name)
        {
            foreach(var loaded in AppDomain.CurrentDomain.GetAssemblies()){
                if(loaded.GetName() == name)
                    return loaded;
            }

            return AppDomain.CurrentDomain.Load(name);
        }

        public static Module GetModule(string moduleName)
        {
            var loaded_asms = AppDomain.CurrentDomain.GetAssemblies();
            Module module = null;
            foreach(var asm in loaded_asms){
                module = asm.GetModule(moduleName);
                if(module != null)
                    break;
            }

            return module;
        }

        /// <summary>
        /// Finds the generic method matching `methodName` and `parameterTypes`. `parameterTypes` can contain type parameters.
        /// </summary>
        /// <returns>The matched generic method.</returns>
        /// <param name="type">Type.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameterTypes">Parameter types.</param>
        public static MethodInfo FindMatchedGenericMethod(this Type type, string methodName, params Type[] parameterTypes)
        {
            return type.FindMatchedGenericMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic, parameterTypes);
        }

        /// <summary>
        /// Finds the generic method matching `methodName`, `flags` and `parameterTypes`. `parameterTypes` can contain type parameters.
        /// </summary>
        /// <returns>The matched generic method.</returns>
        /// <param name="type">Type.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="flags">Flags.</param>
        /// <param name="parameterTypes">Parameter types.</param>
        public static MethodInfo FindMatchedGenericMethod(this Type type, string methodName, BindingFlags flags, params Type[] parameterTypes)
        {
            var results = 
                from m in type.GetMember(methodName, MemberTypes.Method, flags).OfType<MethodInfo>()
                let paramTypes = m.GetParameters().Select(p => p.ParameterType)
                where paramTypes.SequenceEqual(parameterTypes, new GenericMethodComparer())
                select m;

            if(results.Count() > 1)
                throw new AmbiguousMatchException("Ambiguous methods! Multiple candidates found!");
            else if(!results.Any())
                throw new Exception($"There is no candidate methods for '{methodName}'");

            return results.First();
        }

        /// <summary>
        /// Gets the generic method matching `methodName`, `flags` and `parameterTypes`.
        /// </summary>
        /// <returns>The generic method.</returns>
        /// <param name="type">Type.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="flags">Flags.</param>
        /// <param name="parameterTypes">Parameter types.</param>
        public static MethodInfo GetGenericMethod(this Type type, string methodName, BindingFlags flags, params Type[] parameterTypes)
        {
            var results =
                from m in type.GetMember(methodName, MemberTypes.Method, flags).OfType<MethodInfo>()
                where m.ContainsGenericParameters && m.GetParameters().Length == parameterTypes.Length
                select m;

            if(results.Count() > 1)
                throw new AmbiguousMatchException("Ambiguous methods! Multiple candidates found!");
            else if(!results.Any())
                throw new Exception($"There is no candidate methods for '{methodName}'");

            return results.First().MakeGenericMethod(parameterTypes);
        }

        public static string ConvertToDotNetTypeName(string originalName)
        {
            if(SpecialNamesMap.ContainsKey(originalName))
                return SpecialNamesMap[originalName].Item1;
            else
                return originalName;
        }

        public static string ConvertHeadToUpperCase(string name)
        {
            return name.Substring(0, 1).ToUpper() + name.Substring(1);
        }

        public static string ExpandContainer(object obj)
        {
            if(obj == null)
                return "null";
            
            var type = obj.GetType();
            if(type.IsGenericType){
                var type_def = type.GetGenericTypeDefinition();
                if(type_def == typeof(List<>)){
                    if(obj is IEnumerable<int> enumerable)
                        return ExpandSequence(enumerable);
                    else if(obj is IEnumerable<uint> eumerable2)
                        return ExpandSequence(eumerable2);
                    else
                        return ExpandSequence((IEnumerable<object>)obj);
                }

                if(type_def == typeof(Dictionary<,>)){
                    if(obj is Dictionary<int, int> dict)
                        return ExpandDictionary(dict);
                    else if(obj is Dictionary<uint, int> dict2)
                        return ExpandDictionary(dict2);
                    else if(obj is Dictionary<string, int> dict3)
                        return ExpandDictionary(dict3);
                    else if(obj is Dictionary<int, uint> dict4)
                        return ExpandDictionary(dict4);
                    else if(obj is Dictionary<int, string> dict5)
                        return ExpandDictionary(dict5);
                    else if(obj is Dictionary<uint, uint> dict6)
                        return ExpandDictionary(dict6);
                    else if(obj is Dictionary<uint, string> dict7)
                        return ExpandDictionary(dict7);
                    else if(obj is Dictionary<string, uint> dict8)
                        return ExpandDictionary(dict8);
                    else if(obj is Dictionary<string, string> dict9)
                        return ExpandDictionary(dict9);
                }
            }else if(type.IsArray){
                if(obj is IEnumerable<int> enumerable)
                    return ExpandSequence(enumerable);
                else if(obj is IEnumerable<uint> enumerable2)
                    return ExpandSequence(enumerable2);
                else if(obj is IEnumerable<char> enumerable3)
                    return ExpandSequence(enumerable3);
                else
                    return ExpandSequence((IEnumerable<object>)obj);
            }else if(type == typeof(string)){
                return $"\"{obj}\"";
            }else if(type == typeof(char)){
                return $"'{obj}'";
            }

            return obj.ToString();
        }

        public static void AddPrimitiveNativeSymbols()
        {
            CodeGenerator.Symbols.Add(1u, new ExpressoSymbol{
                Method = typeof(Console).GetMethod("Write", new []{
                    typeof(string)
                })
            });
            CodeGenerator.Symbols.Add(2u, new ExpressoSymbol{
                Method = typeof(Console).GetMethod("WriteLine", new []{
                    typeof(string)
                })
            });

            foreach(var builtin_pair in SpecialNamesMap){
                var primitive = AstType.MakePrimitiveType(builtin_pair.Key);
                var type = GetPrimitiveType(primitive);

                CodeGenerator.Symbols.Add(builtin_pair.Value.Item2, new ExpressoSymbol{
                    Type = type
                });
            }
        }

        /// <summary>
        /// Retrieves a method from a type that may be constructed.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ident"></param>
        /// <param name="paramTypes"></param>
        /// <returns></returns>
        public static MethodInfo GetMethod(Type type, Identifier ident, params Type[] paramTypes)
        {
            if(ident.Type.IdentifierNode.Modifiers.HasFlag(Modifiers.Extension)){
                var extension_type = GetNativeType(ident.Type.IdentifierNode.Type);
                if(extension_type != null){
                    paramTypes = new []{type}.Concat(paramTypes).ToArray();
                    type = extension_type;
                }
            }else if(paramTypes == null){
                if(type.IsGenericParameter && CodeGenerator.InterfaceConstraints.TryGetValue(type.Name, out var constraints)){
                    foreach(var constraint_type in constraints){
                        if(constraint_type.ContainsGenericParameters && !constraint_type.IsGenericTypeDefinition){
                            var generic_interface = constraint_type.GetGenericTypeDefinition();
                            var generic_method = generic_interface.GetMethod(ident.Name);
                            var method = TypeBuilder.GetMethod(constraint_type, generic_method);
                            if(method != null)
                                return method;
                        }else{
                            var method = constraint_type.GetMethod(ident.Name);
                            if(method != null)
                                return method;
                        }
                    }

                    return null;
                }else{
                    return type.GetMethod(ident.Name);
                }
            }else if(paramTypes.Any(pt => pt.ContainsGenericParameters)){
                // If paramType contains types that have generic parameters,
                // type.GetMethod(string, Type[]) will throw complaining Type[] must be types that the runtime provide
                return type.GetMethod(ident.Name);
            }else if(type.ContainsGenericParameters){
                var generic_interface = type.GetGenericTypeDefinition();
                var generic_method = generic_interface.GetMethod(ident.Name);
                var method = TypeBuilder.GetMethod(type, generic_method);
                return method;
            }

            if(paramTypes.Any(t => t.IsGenericType))
                return FindMatchedGenericMethod(type, ident.Name, paramTypes);
            else
                return type.GetMethod(ident.Name, paramTypes);
        }

        /// <summary>
        /// Retrieves a constructor that may be a constructed type.
        /// </summary>
        /// <param name="constructedType"></param>
        /// <returns></returns>
        public static ConstructorInfo GetConstructor(Type constructedType)
        {
            if(constructedType.IsConstructedGenericType && constructedType.ContainsGenericParameters){
                var generic_type_def = constructedType.GetGenericTypeDefinition();
                var generic_ctor = generic_type_def.GetConstructors().First();
                return TypeBuilder.GetConstructor(constructedType, generic_ctor);
            }else{
                return constructedType.GetConstructors().First();
            }
        }

        /// <summary>
        /// Determines whether `type` represents a numerical type or not as a C# type.
        /// </summary>
        /// <returns><c>true</c>, if numerical type was ised, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        public static bool IsNumericalType(Type type)
        {
            return type == typeof(int) || type == typeof(uint) || type == typeof(byte) || type == typeof(float) || type == typeof(double) || type == typeof(BigInteger);
        }

        public static ParameterInformation CreateParameterInformation(Type type, string name)
        {
            if(type == null)
                throw new ArgumentNullException(nameof(type));

            return new ParameterInformation{Type = type, IsByRef = type.IsByRef, Name = name};
        }

        static string ExpandSequence<T>(IEnumerable<T> enumerable)
        {
            var builder = new StringBuilder();
            var type = enumerable.GetType();
            if(enumerable.Any()){
                builder.AppendFormat("[{0}", ExpandContainer(enumerable.First()));
                foreach(var elem in enumerable.Skip(1))
                    builder.AppendFormat(", {0}", ExpandContainer(elem));

                if(type.IsArray)
                    builder.Append("]");
                else
                    builder.Append(", ...]");
            }else{
                if(type.IsArray)
                    builder.Append("[]");
                else
                    builder.Append("[...]");
            }
            return builder.ToString();
        }

        static string ExpandDictionary<T, S>(Dictionary<T, S> dict)
        {
            var builder = new StringBuilder();
            if(dict.Any()){
                builder.AppendFormat("{{{0}: {1}", ExpandContainer(dict.First().Key), ExpandContainer(dict.First().Value));
                foreach(var pair in dict.Skip(1))
                    builder.AppendFormat(", {0}: {1}", ExpandContainer(pair.Key), ExpandContainer(pair.Value));

                builder.Append("}");
            }else{
                builder.Append("{}");
            }
            return builder.ToString();
        }

        static IEnumerable<Type> GetTypes(Assembly asm)
        {
            try{
                return asm.GetTypes();
            }
            catch(ReflectionTypeLoadException ex){
                foreach(var e in ex.LoaderExceptions)
                    Console.WriteLine(e.Message);

                return Enumerable.Empty<Type>();
            }
            catch(Exception){
                return Enumerable.Empty<Type>();
            }
        }

        internal static Type GetActionType(int parametersCount)
        {
            switch(parametersCount){
            case 0:
                return typeof(Action);

            case 1:
                return typeof(Action<>);

            case 2:
                return typeof(Action<,>);

            case 3:
                return typeof(Action<,,>);

            case 4:
                return typeof(Action<,,,>);

            case 5:
                return typeof(Action<,,,,>);

            case 6:
                return typeof(Action<,,,,,>);

            case 7:
                return typeof(Action<,,,,,,>);

            case 8:
                return typeof(Action<,,,,,,,>);

            case 9:
                return typeof(Action<,,,,,,,,>);

            case 10:
                return typeof(Action<,,,,,,,,,>);

            case 11:
                return typeof(Action<,,,,,,,,,,>);

            case 12:
                return typeof(Action<,,,,,,,,,,,>);

            case 13:
                return typeof(Action<,,,,,,,,,,,,>);

            case 14:
                return typeof(Action<,,,,,,,,,,,,,>);

            case 15:
                return typeof(Action<,,,,,,,,,,,,,,>);

            case 16:
                return typeof(Action<,,,,,,,,,,,,,,,>);

            default:
                throw new InvalidOperationException("Too many type parameters for an Action.");
            }
        }

        internal static Type GetFuncType(int parametersCount)
        {
            switch(parametersCount){
            case 0:
                return typeof(Func<>);

            case 1:
                return typeof(Func<,>);

            case 2:
                return typeof(Func<,,>);

            case 3:
                return typeof(Func<,,,>);

            case 4:
                return typeof(Func<,,,,>);

            case 5:
                return typeof(Func<,,,,,>);

            case 6:
                return typeof(Func<,,,,,,>);

            case 7:
                return typeof(Func<,,,,,,,>);

            case 8:
                return typeof(Func<,,,,,,,,>);

            case 9:
                return typeof(Func<,,,,,,,,,>);

            case 10:
                return typeof(Func<,,,,,,,,,,>);

            case 11:
                return typeof(Func<,,,,,,,,,,,>);

            case 12:
                return typeof(Func<,,,,,,,,,,,,>);

            case 13:
                return typeof(Func<,,,,,,,,,,,,,>);

            case 14:
                return typeof(Func<,,,,,,,,,,,,,,>);

            case 15:
                return typeof(Func<,,,,,,,,,,,,,,,>);

            case 16:
                return typeof(Func<,,,,,,,,,,,,,,,,>);

            default:
                throw new InvalidOperationException("Too many type parameters for a Func.");
            }
        }
    }
}

