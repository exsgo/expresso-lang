using System;
using Expresso.Ast;

namespace Expresso.CodeGen
{
	/// <summary>
    /// CodeGenerator内で発生した例外をあらわす。
	/// Represents an exception occurred while emitting native code.
	/// </summary>
	public class GeneratorException : Exception
	{
        public AstNode Node{
            get; set;
        }

        public GeneratorException(string msg) : base($"CodeGenerator Error: {msg}")
		{
		}

        public GeneratorException(string format, AstNode node)
            : this(format)
        {
            Node = node;
        }
	}
}

