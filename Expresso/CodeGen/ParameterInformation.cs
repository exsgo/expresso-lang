﻿using System;
namespace Expresso.CodeGen
{
    /// <summary>
    /// Contains informations about a variable or a parameter.
    /// </summary>
    public class ParameterInformation
    {
        public Type Type{
            get; set;
        }

        public bool IsByRef{
            get; set;
        }

        public string Name{
            get; set;
        }
    }
}
