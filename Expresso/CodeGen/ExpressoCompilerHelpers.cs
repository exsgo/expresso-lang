﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using Expresso.Ast;
using Expresso.Ast.Analysis;

using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.CodeGen
{
    /// <summary>
    /// Contains helper methods for native symbols.
    /// </summary>
    public static class ExpressoCompilerHelpers
    {
        static List<string> _AssemblyNames =
            new List<string>{"System.Numerics, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", "ExpressoRuntime"};

        // I actually want to keep it private, but I can't because the test runner will need to clean it up each time it runs a test
        public static Dictionary<string, SymbolTable> SymbolTableCache = new Dictionary<string, SymbolTable>();

        internal static uint StartOfIdentifierId = 1u;
        internal static uint StartOfPrimitiveTypeIdentifierId = StartOfIdentifierId + 2u;
        internal static Guid LanguageGuid = Guid.Parse("408e5e88-0566-4b8a-9c69-4d2f7c74baf9");
        internal static uint EndOfPrimitiveTypeIdentifierIds = StartOfPrimitiveTypeIdentifierId + 16u;
        internal static uint EndOfImportedIdentifierIds;
        //static uint IdentifierId = EndOfPrimitiveTypeIdentifierId;
        internal const string TypePrefix = "type_";

        static Dictionary<string, (string, uint)> SpecialNamesMapInverse = new Dictionary<string, (string, uint)>{
            {"Expresso.Runtime.Builtins.ExpressoIntegerSequence", ("intseq", StartOfPrimitiveTypeIdentifierId + 0)},
            {"Expresso.Runtime.Builtins.Slice", ("slice", StartOfPrimitiveTypeIdentifierId + 1u)},
            {"System.Boolean", ("bool", StartOfPrimitiveTypeIdentifierId + 2u)},
            {"System.Int32", ("int", StartOfPrimitiveTypeIdentifierId + 3u)},
            {"System.UInt32", ("uint", StartOfPrimitiveTypeIdentifierId + 4u)},
            {"System.Single", ("float", StartOfPrimitiveTypeIdentifierId + 5u)},
            {"System.Double", ("double", StartOfPrimitiveTypeIdentifierId + 6u)},
            {"System.Char", ("char", StartOfPrimitiveTypeIdentifierId + 7u)},
            {"System.Byte", ("byte", StartOfPrimitiveTypeIdentifierId + 8u)},
            {"System.String", ("string", StartOfPrimitiveTypeIdentifierId + 9u)},
            {"System.Array", ("array", StartOfPrimitiveTypeIdentifierId + 10u)},
            {"System.Collections.Generic.List", ("vector", StartOfPrimitiveTypeIdentifierId + 11u)},
            {"System.Tuple", ("tuple", StartOfPrimitiveTypeIdentifierId + 12u)},
            {"System.Collections.Generic.Dictionary", ("dictionary", StartOfPrimitiveTypeIdentifierId + 13u)},
            {"System.Numerics.BigInteger", ("bigint", StartOfPrimitiveTypeIdentifierId + 14u)},
            // System.Object is only there for automatically loading because it is needed for all object references
            {"System.Object", ("object", StartOfPrimitiveTypeIdentifierId + 15u)}
        };

        public static string GetExpressoTypeName(string csharpFullName)
        {
            var backquote_index = csharpFullName.IndexOf('`');
            // csharp_name contains names without `'s, which we need
            var csharp_name = csharpFullName.Substring(0, (backquote_index == -1) ? csharpFullName.Length : backquote_index);
            if(SpecialNamesMapInverse.ContainsKey(csharp_name))
                return SpecialNamesMapInverse[csharp_name].Item1;
            else
                return csharp_name;
        }

        public static PrimitiveType GetPrimitiveAstType(string type)
        {
            var type_code = PrimitiveType.GetActualKnownTypeCodeForPrimitiveType(type);
            if(type_code == TypeSystem.KnownTypeCode.None)
                return null;
            
            return AstType.MakePrimitiveType(type);
        }

        public static void RegisterPrimitiveTypesAndTheirBaseTypeTables(SymbolTable target)
        {
            foreach(var primitive in SpecialNamesMapInverse){
                var type_name = primitive.Value.Item1;
                var type = (type_name == "array" || type_name == "vector" || type_name == "dictionary" || type_name == "object")
                            ? (AstType)AstType.MakeSimpleType(type_name)
                            : AstType.MakePrimitiveType(type_name);
                target.AddTypeSymbol(type_name, type);
                target.GetTypeSymbol(type_name).IdentifierId = primitive.Value.Item2;
            }

            foreach(var child_table in SymbolTable.PrimitiveTypeSymbolTables.Children)
                target.AddTypeTable(child_table.Clone());
        }

        public static void InitializePrimitiveTypeSymbolTables(SymbolTable uniqueTable)
        {
            foreach(var primitive in SpecialNamesMapInverse)
                AddNativeSymbolTable(AstNode.MakeIdentifier(primitive.Key), uniqueTable, true);
        }

        public static void AddNativeSymbolTable(Identifier identifier, SymbolTable table, bool isPrimitive = false)
        {
            Type type = null;
            var asms = AppDomain.CurrentDomain.GetAssemblies();
            var regex = new Regex($"{identifier.Name}(?![a-zA-Z0-9])");
            foreach(var asm in asms){
                var types = GetTypes(asm);
                type = types.FirstOrDefault(t => regex.IsMatch($"{t.Namespace}.{t.Name}"));
                if(type != null)
                    break;
            }
            if(type == null){
                throw new ParserException(
                    $"The type `{identifier.Name}` is not a native type.",
                    "ES5000",
                    identifier
                ){
                    HelpObject = identifier.Name.StartsWith("System", StringComparison.CurrentCulture)
                };
            }
                
            PopulateSymbolTable(table, type, isPrimitive);
        }

        /// <summary>
        /// Stringifies a list of items.
        /// </summary>
        /// <returns>The list.</returns>
        /// <param name="enumerable">Enumerable.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static string StringifyList<T>(IEnumerable<T> enumerable)
        {
            if(enumerable == null)
                return "<null>";

            if(!enumerable.Any())
                return string.Empty;
            
            var first_item = enumerable.First();
            var builder = new StringBuilder(first_item.ToString());
            foreach(var item in enumerable.Skip(1)){
                builder.Append(", ");
                builder.Append(item.ToString());
            }

            return builder.ToString();
        }

        /// <summary>
        /// Loads appropriate assemblies to prepare for code analyzing.
        /// </summary>
        public static void Prepare()
        {
            foreach(var name in _AssemblyNames){
                var an = new AssemblyName(name);
                Assembly.Load(an);
            }
        }

        internal static void DisplayHelp(ParserException e)
        {
            var error_number = e.ErrorCode.Substring(2);
            // They have no additional information so we ignore them
            if(error_number == "1000")
                return;
            
            var prev_color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Utilities.CompilerOutput.Write("help: ");

            switch(error_number){
            // 0000~0999: Literal and syntax errors
            case "0001":
                Utilities.CompilerOutput.WriteLine("Otherwise you can't receive all the parameters.");
                break;

            case "0002":
                Utilities.CompilerOutput.WriteLine("Because we don't have named parameters, it isn't allowed.");
                break;

            case "0003":
                Utilities.CompilerOutput.WriteLine("I can't do anything if there is no values from which I'll infer the type of the expression.");
                break;

            case "0004":
                Utilities.CompilerOutput.WriteLine("At least one of them has to be placed so that the compiler can determine the type of the parameter.");
                break;

            case "0005":
                Utilities.CompilerOutput.WriteLine("Otherwise I can't determine where the string ends.");
                break;

            case "0006":
                Utilities.CompilerOutput.WriteLine("Please make sure that the file exists.");
                break;

            case "0007":
                Utilities.CompilerOutput.WriteLine("Otherwise it won't do anything.");
                break;

            case "0008":
                Utilities.CompilerOutput.WriteLine("Otherwise some values won't be assigned.");
                break;

            /*case "0009":
                Utilities.CompilerOutput.WriteLine("For a keyword list, see the online documentation.");
                break;*/

            case "0010":
                Utilities.CompilerOutput.WriteLine("This isn't due to a technical issue.");
                break;

            case "0011":
                Utilities.CompilerOutput.WriteLine("This is obviously a restriction but supporting it will definitely make the implementation more complex.");
                break;

            case "0020":
                Utilities.CompilerOutput.WriteLine("Other patterns don't make sense, do they?");
                break;

            case "0021":
                Utilities.CompilerOutput.WriteLine("Other patterns don't make sense, do they?");
                break;

            case "0030":
                Utilities.CompilerOutput.WriteLine("You have to add the 'abstract' modifier to leave the body empty.");
                break;

            /*case "0040":
                Utilities.CompilerOutput.WriteLine("See the C# documentation for the format of uint literals.");
                break;

            case "0050":
                Utilities.CompilerOutput.WriteLine("See the C# documentation for the format of float literals.");
                break;

            case "0051":
                Utilities.CompilerOutput.WriteLine("See the C# documentation for the format of number literals.");
                break;*/

            case "0100":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the identifier name.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "0101":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the type symbol.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "0102":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the symbol.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "0103":
                {
                    var is_importing_system = (bool)e.HelpObject;
                    if(is_importing_system){
                        Utilities.CompilerOutput.WriteLine("To import names from the System namespace(that is, from .NET), you don't have to write the from clause.");
                    }else{
                        Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the symbol name.");
                        Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                    }
                }
                break;

            case "0110":
                Utilities.CompilerOutput.WriteLine("All fields have to be initialized when instances created.");
                Utilities.CompilerOutput.WriteLine("If you need to assign default values to fields, you can define factory functions that fill in fields with default values.");
                break;

            case "0120":
                Utilities.CompilerOutput.WriteLine("Unlike Python, Ruby and JavaScript etc., Expresso doesn't allow you to use variables or functions before they are declared.");
                Utilities.CompilerOutput.WriteLine("Make sure that the item is declared before use.");
                break;

            case "0121":
                Utilities.CompilerOutput.WriteLine("All symbols except for type symbols must be used after they are defined.");
                break;

            case "0122":
                Utilities.CompilerOutput.WriteLine("We expect that types that point at each other should be next to each other.");
                break;

            case "0123":
                Utilities.CompilerOutput.WriteLine("We expect that types that point at each other should be next to each other.");
                break;

            case "0130":
                Utilities.CompilerOutput.WriteLine("`{0}` is a class.", e.HelpObject);
                break;

            case "0140":
                Utilities.CompilerOutput.WriteLine("Raw value enums can only accept literal integers.");
                break;

            case "0150":
                Utilities.CompilerOutput.WriteLine("Only abstract classes can have abstract methods as in C#.");
                Utilities.CompilerOutput.WriteLine("In addition, abstract classes can contain normal fields and methods with bodies.");
                break;

            case "0160":
                Utilities.CompilerOutput.WriteLine("Just as you can't define another variable of the same name in the same scope,");
                Utilities.CompilerOutput.WriteLine("you can't define another variable in the inner scope than the one in which the other variable is defined.");
                break;

            case "0200":
                Utilities.CompilerOutput.WriteLine("Please make sure that the variable is initialized.");
                break;

            case "0201":
                Utilities.CompilerOutput.WriteLine("The `self` keyword always point at the current instance in the current scope.");
                Utilities.CompilerOutput.WriteLine("So it doesn't make sense to use it in functions.");
                break;

            case "0210":
                Utilities.CompilerOutput.WriteLine("An instance method call requires an instance reference as the first implicit parameter.");
                Utilities.CompilerOutput.WriteLine("That's why we need an object reference.");
                break;

            case "0211":
                Utilities.CompilerOutput.WriteLine("A static method call doesn't require an instance reference as the first implicit parameter.");
                Utilities.CompilerOutput.WriteLine("That's why we need a type reference.");
                break;

            case "0300":
                Utilities.CompilerOutput.WriteLine("Generally, you will miss some values without the _ pattern or variable name patterns.");
                Utilities.CompilerOutput.WriteLine("I assume that at least you have one _ pattern or one variable pattern for each field or variable.");
                break;

            case "0301":
                Utilities.CompilerOutput.WriteLine("Value binding patterns can only appear in an if-let construct.");
                break;

            case "0400":
                Utilities.CompilerOutput.WriteLine("Just use it somewhere or prefix the name with an underscore in order to silence the warning.");
                break;

            case "0500":
                Utilities.CompilerOutput.WriteLine("A glob('*') has to be balanced with another glob in the alias name.");
                break;

            // 1000~1999: Type errors
            // not being used
            case "1000":
                break;

            case "1001":
                Utilities.CompilerOutput.WriteLine("The variable or field should be compatible with the value on the right hand side when assigned.");
                Utilities.CompilerOutput.WriteLine("In this context, 'compatible' means that the types of the both values match");
                Utilities.CompilerOutput.WriteLine("or the right hand side can be implicitly casted to the left hand side, which is discouraged in Expresso.");
                break;

            case "1002":
                Utilities.CompilerOutput.WriteLine("For combinations of the operators and the types that can be used with them, see the online documentation.");
                break;

            case "1003":
                Utilities.CompilerOutput.WriteLine("Aren't you trying to cast an object to the type the class isn't derived from?");
                Utilities.CompilerOutput.WriteLine("For combinations in which primitive types can be casted, see the online documentation.");
                break;

            case "1004":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the type name.");
                Utilities.CompilerOutput.WriteLine("Check for spelling, taking casing into account.");
                Utilities.CompilerOutput.WriteLine("It's second most likely that you forgot to import the type.");
                break;

            case "1005":
                Utilities.CompilerOutput.WriteLine("Every expression has one type. So is the conditional expression.");
                Utilities.CompilerOutput.WriteLine("That's why we should determine the one type that the conditional expression will be.");
                break;

            case "1006":
                Utilities.CompilerOutput.WriteLine("This means that you can't run this code without any explicit casts.");
                Utilities.CompilerOutput.WriteLine("If you need to change the default behavior, you must add manual casts.");
                break;

            case "1007":
                Utilities.CompilerOutput.WriteLine("Generally, you can't know beforehand whether a certain operator can be applied to a given generic type.");
                Utilities.CompilerOutput.WriteLine("That's why you can't apply operators on generic types.");
                break;

            case "1010":
                Utilities.CompilerOutput.WriteLine("Interfaces can only be derived from interfaces.");
                break;

            case "1011":
                Utilities.CompilerOutput.WriteLine("I assume that the parent class is placed in the last position.");
                Utilities.CompilerOutput.WriteLine("Otherwise, the parent class would not be set properly.");
                Utilities.CompilerOutput.WriteLine("It could be that you specify more than 1 parent class.");
                break;

            case "1020":
                Utilities.CompilerOutput.WriteLine("In Expresso, 'null' is a special keyword which is allowed in contexts with foreign codes.");
                Utilities.CompilerOutput.WriteLine("If you need 'null' in pure Expresso code, consider using the `Option<T>` type that is provided as the standard library.");
                break;

            case "1021":
                Utilities.CompilerOutput.WriteLine("Strings in foreign code can be null, but those in Expresso wouldn't.");
                break;

            case "1022":
                Utilities.CompilerOutput.WriteLine("The value against which the value is tested must be exactly one type.");
                Utilities.CompilerOutput.WriteLine("Otherwise the compiler will issue a compiler error at that point.");
                break;

            case "1030":
                Utilities.CompilerOutput.WriteLine("It's because methods that implement interfaces should have exactly the same signature.");
                break;

            case "1031":
                Utilities.CompilerOutput.WriteLine("In addition, an overridable method requires its parent type's method to be virtual.");
                break;

            case "1032":
                Utilities.CompilerOutput.WriteLine("In Expresso, methods without the virtual modifier will be automatically final.");
                break;

            case "1100":
                Utilities.CompilerOutput.WriteLine("The variable or field should be compatible with the value on the right hand side when assigned.");
                Utilities.CompilerOutput.WriteLine("In this context, 'compatible' means that the types of the both values match");
                Utilities.CompilerOutput.WriteLine("or the right hand side can be implicitly casted to the left hand side, which is discouraged in Expresso.");
                break;

            case "1101":
                Utilities.CompilerOutput.WriteLine("Types in patterns must match.");
                break;

            case "1102":
                Utilities.CompilerOutput.WriteLine("The key types have to be one type.");
                Utilities.CompilerOutput.WriteLine("Otherwise, we can't create an instance.");
                break;

            case "1103":
                Utilities.CompilerOutput.WriteLine("The value types have to be one type.");
                Utilities.CompilerOutput.WriteLine("Otherwise, we can't create an instance.");
                break;

            case "1104":
                Utilities.CompilerOutput.WriteLine("The item types have to be one type.");
                Utilities.CompilerOutput.WriteLine("Otherwise, we can't create an instance out of them.");
                break;

            case "1110":
                Utilities.CompilerOutput.WriteLine($"Please specify a value with type `{e.HelpObject}`.");
                break;

                // not being used
            /*case "1111":
                Utilities.CompilerOutput.WriteLine("Missing some type parameters or there are too many type parameters.");
                break;*/

            case "1111":
                Utilities.CompilerOutput.WriteLine("This is an error because we can't take those functions into account.");
                break;

            case "1200":
                Utilities.CompilerOutput.WriteLine("The operand must be of type `bool`.");
                break;

            case "1201":
                Utilities.CompilerOutput.WriteLine("For combinations of the operators and types that can be used with the operators, see the online documentation.");
                break;

            case "1202":
                Utilities.CompilerOutput.WriteLine("This is a warning because this fact by itself doesn't cause any problems.");
                break;

            case "1210":
                Utilities.CompilerOutput.WriteLine("Classes must implement certain interfaces or inherit certain classes in order to allow specific actions.");
                break;

            case "1300":
                Utilities.CompilerOutput.WriteLine("This generally means that you have no ways to make it runnable as it is.");
                break;

            case "1301":
                Utilities.CompilerOutput.WriteLine("For statements can only be used for iterating over a sequence.");
                break;

                // not being used
            /*case "1302":
                Utilities.CompilerOutput.WriteLine("A comprehension expects a sequence object.");
                break;*/

            case "1303":
                Utilities.CompilerOutput.WriteLine("Expresso doesn't implicitly cast objects to other types.");
                Utilities.CompilerOutput.WriteLine("So consider adding an explicit casting.");
                Utilities.CompilerOutput.WriteLine("When the method has any type parameters, it could be that you are giving an argument which mismatch in type.");
                Utilities.CompilerOutput.WriteLine("Check the types of each argument.");
                break;

            case "1304":
                Utilities.CompilerOutput.WriteLine("Have you changed the expression? Because that's most likely to cause the problem.");
                Utilities.CompilerOutput.WriteLine("Of course, you can use type inference!");
                break;

            case "1310":
                Utilities.CompilerOutput.WriteLine("Consider specifying the initial value or adding the type annotation.");
                break;

            case "1400":
                Utilities.CompilerOutput.WriteLine("Interfaces don't implement methods and not have fields.");
                Utilities.CompilerOutput.WriteLine("That's why interfaces can't be instantiated.");
                break;

            case "1401":
                Utilities.CompilerOutput.WriteLine("Abstract classes can have abstract methods.");
                Utilities.CompilerOutput.WriteLine("That's why abstract classes can't be instantiated.");
                break;

            case "1410":
                Utilities.CompilerOutput.WriteLine("Enums can contain type parameters in each variant");
                Utilities.CompilerOutput.WriteLine("and when you create one of them, type parameters on only one variant can be inferred from that expression.");
                Utilities.CompilerOutput.WriteLine("That's why we don't support type inference on enum object creations.");
                break;

            case "1500":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled it.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "1501":
                Utilities.CompilerOutput.WriteLine("Did you forget to import the module that the type was defined?");
                Utilities.CompilerOutput.WriteLine("Also you can check for spelling. Don't forget to take casing into account.");
                break;

            case "1502":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the field name. So check for spelling, taking casing into account.");
                break;

            case "1503":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the name. Check for spelling, taking casing into account.");
                Utilities.CompilerOutput.WriteLine($"The following variants are available: {e.HelpObject}");
                break;

            case "1504":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the name. Check for spelling, taking casing into account.");
                break;

            case "1510":
                Utilities.CompilerOutput.WriteLine("When importing a generic type, you must add a type parameter list `<>` after the type name that you are importing.");
                break;

            case "1511":
                Utilities.CompilerOutput.WriteLine("If there is any additional postfix, Expresso can't get what you want.");
                break;

            case "1512":
                Utilities.CompilerOutput.WriteLine("The number of type parameters in an import path must match to that of the actual type.");
                break;

            case "1600":
                {
                    Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the method name. Check for spelling, taking casing into account.");
                    var call_method_in_system = (bool)e.HelpObject;
                    if(call_method_in_system){
                        Utilities.CompilerOutput.WriteLine("Are you trying to call a method on the `System` namespace?");
                        Utilities.CompilerOutput.WriteLine("Then, it should start with a capital letter because they are named in pascal case.");
                    }
                }
                break;

            case "1601":
                Utilities.CompilerOutput.WriteLine("It could be that you forgot to cast some of the arguments.");
                Utilities.CompilerOutput.WriteLine("Or it could be that you are calling the method that has some overloads, ignoring parameters which have default values.");
                Utilities.CompilerOutput.WriteLine("In this case, Expresso can't recognize a correct overload due to some technical difficulty.");
                break;

            case "1602":
                Utilities.CompilerOutput.WriteLine("Unlike methods and functions, you may not omit the return types of method signatures in interfaces.");
                Utilities.CompilerOutput.WriteLine("This is due to the trouble of implementing it and because interfaces must be explicit.");
                break;

            case "1610":
                Utilities.CompilerOutput.WriteLine("You can use type inference to infer the return type.");
                break;

            case "1700":
                Utilities.CompilerOutput.WriteLine("It's most likely that you forgot to import some module.");
                Utilities.CompilerOutput.WriteLine("Or maybe you misspelled the type or symbol name. Check for spelling, taking casing into account.");
                break;

            case "1710":
                Utilities.CompilerOutput.WriteLine("It's because array sizes can't be other types than `int`.");
                break;

            case "1800":
                Utilities.CompilerOutput.WriteLine("Otherwise the return type of the function becomes `void`.");
                break;

            case "1801":
                Utilities.CompilerOutput.WriteLine("Functions and methods used in string interpolation should return something to commit a change in the string.");
                Utilities.CompilerOutput.WriteLine("In that sense, those functions and methods also should have no side effects.");
                break;

            case "1802":
                Utilities.CompilerOutput.WriteLine("In Expresso, callables include functions and closures.");
                break;

            case "1810":
                Utilities.CompilerOutput.WriteLine("The number of items must match.");
                break;

            case "1811":
                Utilities.CompilerOutput.WriteLine("The types must all be matched in order for it to be matched.");
                Utilities.CompilerOutput.WriteLine($"The variant type is: {e.HelpObject}.");
                break;

            //not being used
            case "1812":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the type name.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "1820":
                Utilities.CompilerOutput.WriteLine("The types must all be matched in order for the whole pattern to be matched.");
                break;

            case "1821":
                Utilities.CompilerOutput.WriteLine("You have to mention all the fields to match to the type.");
                break;

            case "1900":
                Utilities.CompilerOutput.WriteLine("Immutable variables are immutable becuase it's how they are.");
                Utilities.CompilerOutput.WriteLine("If you absolutely need to change the value, then consider changing the `let` keyword to `var` in the variable declaration.");
                Utilities.CompilerOutput.WriteLine("But think twice before doing so because mutability could cause evil things.");
                break;

            // never happens
            case "1901":
                Utilities.CompilerOutput.WriteLine("You can't omit the return type when there is no statements in the body!");
                break;

            case "1902":
                Utilities.CompilerOutput.WriteLine("Immutable fields can't be assigned values more than once because it is how immutable fields are.");
                break;

            case "1903":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the item.");
                break;

            case "1904":
                Utilities.CompilerOutput.WriteLine("It's because references' values can't be changed.");
                break;

            case "1905":
                Utilities.CompilerOutput.WriteLine("In order to change parameters, you need to add & before the type and make it a reference type.");
                Utilities.CompilerOutput.WriteLine("With reference types, values will be changed via references thus the changes can be seen outside the functions.");
                break;

            case "1910":
                Utilities.CompilerOutput.WriteLine("A derived class must implement all the methods that an interface declares.");
                break;

            case "1911":
                Utilities.CompilerOutput.WriteLine("In Expresso, you can't derive classes from primitive types like `int`, `uint` and `intseq`.");
                Utilities.CompilerOutput.WriteLine("If you are to derive types defined in an external module,");
                Utilities.CompilerOutput.WriteLine("try adding an alias to give the compiler chance to resolve the name, using the `import ... as ...` construct.");
                break;

            case "1912":
                Utilities.CompilerOutput.WriteLine("It's most likely that the type you are deriving is not exported.");
                Utilities.CompilerOutput.WriteLine("So consider adding the `export` modifier to the type.");
                break;

            case "1913":
                Utilities.CompilerOutput.WriteLine("Primitive types can't be derived.");
                break;

            case "1914":
                Utilities.CompilerOutput.WriteLine("Sealed classes close over themselves and aren't derivable.");
                break;

            case "1920":
                Utilities.CompilerOutput.WriteLine();
                break;

            // 2000~2999: Missing-name and type-argument errors
            case "2000":
                Utilities.CompilerOutput.WriteLine("It could be that you forgot to import the type of the expression.");
                break;

            case "2001":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the field or the property name.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "2002":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the method name. So check for spelling, taking casing into account.");
                Utilities.CompilerOutput.WriteLine("The second most likely is you specifying wrong signatures. So check for the arguments' types.");
                break;

            case "2003":
                Utilities.CompilerOutput.WriteLine("It's most likely that you forgot to cast the object to the field type.");
                Utilities.CompilerOutput.WriteLine("Consider adding one.");
                break;

            case "2004":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the method name.");
                break;

            // never thrown?
            case "2005":
                Utilities.CompilerOutput.WriteLine("Please be explicit for type arguments with the `<...>` syntax.");
                break;

            case "2010":
                Utilities.CompilerOutput.WriteLine("The types are mismatched. Did you forget to cast arguments?");
                Utilities.CompilerOutput.WriteLine($"The following overloads exist: {e.HelpObject}.");
                break;

            case "2011":
                Utilities.CompilerOutput.WriteLine("You are giving the constructor too few arguments.");
                Utilities.CompilerOutput.WriteLine("Consider providing more arguments.");
                break;

            case "2012":
                Utilities.CompilerOutput.WriteLine("You are giving the constructor too many arguments.");
                Utilities.CompilerOutput.WriteLine("Consider removing some of the arguments.");
                break;

            case "2013":
                Utilities.CompilerOutput.WriteLine("The types are mismatched. Did you forget to cast arguments?");
                Utilities.CompilerOutput.WriteLine($"The variant type is ({e.HelpObject}).");
                break;

            case "2020":
                Utilities.CompilerOutput.WriteLine("Too few parameters are provided in the type arguments.");
                Utilities.CompilerOutput.WriteLine("Consider adding more type arguments.");
                break;

            case "2021":
                Utilities.CompilerOutput.WriteLine("Too many parameters are provided in the type arguments.");
                Utilities.CompilerOutput.WriteLine("Consider removing some of the type arguments.");
                break;

            case "2022":
                Utilities.CompilerOutput.WriteLine("Types are different in the type arguments and that of an argument.");
                Utilities.CompilerOutput.WriteLine("It's possible that you are adding an extra type casting.");
                break;

            case "2023":
                Utilities.CompilerOutput.WriteLine("You have to provide that argument!");
                break;

            case "2030":
                Utilities.CompilerOutput.WriteLine("You are giving the call too few arguments!");
                Utilities.CompilerOutput.WriteLine("Consider adding arguments.");
                break;

            case "2031":
                Utilities.CompilerOutput.WriteLine("You are giving the call too many arguments!");
                Utilities.CompilerOutput.WriteLine("Consider removing some of the arguments.");
                break;

            case "2100":
                Utilities.CompilerOutput.WriteLine("You can't call a mutating method on an immutable variable because it modifies the state of the object.");
                Utilities.CompilerOutput.WriteLine("If you absolutely need to do so, consider changing the `let` keyword to `var` in the variable declaration.");
                break;

            case "2101":
                Utilities.CompilerOutput.WriteLine("Augmented assignments can't be simultaneous assignments.");
                break;

            case "2200":
                Utilities.CompilerOutput.WriteLine("Type constraints mean that you can use only the fields and methods implemented on them with that type parameter.");
                break;

            case "2201":
                Utilities.CompilerOutput.WriteLine("You use type constraints to make contracts with the compiler that the type parameter should have certain fields or methods.");
                break;

            case "2202":
                Utilities.CompilerOutput.WriteLine("If you need to use type parameters only in methods, you can define method-wide ones.");
                break;

            case "2300":
                Utilities.CompilerOutput.WriteLine("In Expresso, the `self` keyword is required.");
                break;

            case "2400":
                Utilities.CompilerOutput.WriteLine("When constructing an enum variant, you should specify which variant to construct after '::'.");
                Utilities.CompilerOutput.WriteLine($"The following variants are available: {e.HelpObject}.");
                break;

            // not being used
            case "2401":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the variant name.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "2410":
                Utilities.CompilerOutput.WriteLine("It's most likely that you misspelled the field name.");
                Utilities.CompilerOutput.WriteLine("So check for spelling, taking casing into account.");
                break;

            case "2500":
                Utilities.CompilerOutput.WriteLine($"The generic type has {e.HelpObject} type parameter(s).");
                Utilities.CompilerOutput.WriteLine("So you must add them in a generic type argument list(<...>).");
                break;

            // 3000~3999: operator-related errors
            case "3000":
                Utilities.CompilerOutput.WriteLine("For more info, see the previous error.");
                break;

            case "3010":
                Utilities.CompilerOutput.WriteLine("In Expresso, you can only omit the parameter and return types in closures if and only if they are directly passed to functions");
            	Utilities.CompilerOutput.WriteLine("or methods that take closures.");
                break;

            case "3011":
                Utilities.CompilerOutput.WriteLine("In Expresso, you can only apply the indexer operator on `vector`s, `array`s, `dictionar`ies");
                Utilities.CompilerOutput.WriteLine("and classes that implement the `System.Collections.Generic.ICollection<T>` interface.");
                break;

            case "3012":
                Utilities.CompilerOutput.WriteLine("You can only use an `intseq` with the indexer operator on `array`s or `vector`s.");
                break;

            case "3013":
                Utilities.CompilerOutput.WriteLine("In Expresso, you can't use the indexer operator on an arbitrary object.");
                break;

            case "3014":
                Utilities.CompilerOutput.WriteLine("You can't declare the same name more than once as a type.");
                break;

            case "3015":
                Utilities.CompilerOutput.WriteLine("Currently, Expresso doesn't allow you to declare more than 1 variable with the same name in the same scope.");
                Utilities.CompilerOutput.WriteLine("In the future you may be able to use shadowing, though.");
                break;

            case "3100":
                Utilities.CompilerOutput.WriteLine("You aren't allowed to shadow module variables. You have to rename either the module variable or the local variable.");
                break;

            case "3200":
                Utilities.CompilerOutput.WriteLine("For combinations of the unary operators and the types, see the online documentation.");
                break;

            case "3300":
                Utilities.CompilerOutput.WriteLine("That is what it is.");
                break;

            case "3301":
                Utilities.CompilerOutput.WriteLine("That is what it is.");
                break;

            case "3302":
                Utilities.CompilerOutput.WriteLine("Consider adding the `export` flag to the item.");
                break;

            // 4000~: Other errors
            case "4000":
                Utilities.CompilerOutput.WriteLine("Unfortunately for you, it's illegal in Expresso to write an expression that's evaluated to other types than `bool`");
                Utilities.CompilerOutput.WriteLine("in a conditional expression of an if statement.");
                break;

            case "4001":
                Utilities.CompilerOutput.WriteLine("An integer sequence expression expects an `int`.");
                break;

            case "4002":
                Utilities.CompilerOutput.WriteLine($"You are trying to pass {e.HelpObject} to the intseq constructor.");
                break;

            case "4003":
                Utilities.CompilerOutput.WriteLine("If you mean that the integer overflows within the negative and then reaches `end`, you can safely ignore this warning.");
                Utilities.CompilerOutput.WriteLine("I'm displaying this warning because it entirely consists of literal expressions.");
                Utilities.CompilerOutput.WriteLine("Otherwise I can't show this warning due to technical difficulty.");
                break;

            case "4004":
                Utilities.CompilerOutput.WriteLine("If you mean that the integer overflows within the positive and then reaches `end`, you can safely ignore this warning.");
                Utilities.CompilerOutput.WriteLine("I'm displaying this warning because it entirely consists of literal expressions.");
                Utilities.CompilerOutput.WriteLine("Otherwise I can't show this warning due to technical difficulty.");
                break;

            case "4005":
                Utilities.CompilerOutput.WriteLine("If you are to make an inifinite series of integers, you can safely ignore this warning.");
                Utilities.CompilerOutput.WriteLine("I'm displaying this warning because it entirely consists of literal expressions.");
                Utilities.CompilerOutput.WriteLine("Otherwise I can't show this warning due to technical difficulty.");
                break;

            case "4006":
                Utilities.CompilerOutput.WriteLine("Usually this is not an intended behavior. So consider changing either `start` or `end`.");
                Utilities.CompilerOutput.WriteLine("I'm displaying this warning because it entirely consists of literal expressions.");
                Utilities.CompilerOutput.WriteLine("Otherwise I can't show this warning due to technical difficulty.");
                break;

            case "4010":
                Utilities.CompilerOutput.WriteLine($"You can break out of loops {e.HelpObject} times at this point.");
                break;

            case "4011":
                Utilities.CompilerOutput.WriteLine($"You can continue out of loops {e.HelpObject} times at this point.");
                break;

            case "4020":
                Utilities.CompilerOutput.WriteLine("The module that has the main function is usually called 'main' because it is the entry point to the program.");
                break;

            case "4021":
                Utilities.CompilerOutput.WriteLine($"In this context, the following targets are allowed: {e.HelpObject}.");
                break;

            case "4022":
                Utilities.CompilerOutput.WriteLine("Arguments for attributes must be compile-time constants.");
                Utilities.CompilerOutput.WriteLine("Compile-time constants mean that there should be neither object constructions nor function calls.");
                break;

            case "4023":
                Utilities.CompilerOutput.WriteLine($"For this attribute, the following targets are expected: {e.HelpObject}.");
                break;

            case "4030":
                Utilities.CompilerOutput.WriteLine("You can't define particular values more than once.");
                break;

            case "4031":
                Utilities.CompilerOutput.WriteLine("You have to only use either tuple-like members or raw value members in an enum.");
                break;

            case "4040":
                Utilities.CompilerOutput.WriteLine("Currently, the pattern in a for statement can only be either a variable or a tuple.");
                break;

            case "4100":
                Utilities.CompilerOutput.WriteLine("Recursive functions and methods can't use type inference on their signatures.");
                break;

            case "4101":
                Utilities.CompilerOutput.WriteLine("If the function or method is defined after the callee, they must be complete in their signatures.");
                Utilities.CompilerOutput.WriteLine("Otherwise, you can't call them.");
                break;

            case "4110":
                Utilities.CompilerOutput.WriteLine("Mutating methods are supposed to mutate themselves.");
                Utilities.CompilerOutput.WriteLine("You can safely get rid of the `mutating` keyword.");
                break;

            case "4120":
                Utilities.CompilerOutput.WriteLine("In Expresso, you should add the `mutating` keyword in order to mutate `self`.");
                Utilities.CompilerOutput.WriteLine("In this way, you can avoid accidentally changing the values of self fields.");
                break;

                // not being used
            case "4121":
                Utilities.CompilerOutput.WriteLine("In Expresso, you should add the `mutating` keyword in order to access to a mutable field.");
                Utilities.CompilerOutput.WriteLine("In this way, you can avoid accidentally changing the value of the field.");
                break;

            case "4200":
                Utilities.CompilerOutput.WriteLine("It is recommended in Expresso that type names start with a capital letter");
                Utilities.CompilerOutput.WriteLine("and be in pascal case.");
                Utilities.CompilerOutput.WriteLine("Pascal case is names like `SomeType`.");
                break;

            case "4201":
                Utilities.CompilerOutput.WriteLine("It is recommended in Expresso that function and method names start with a lower case letter");
                Utilities.CompilerOutput.WriteLine("and be in camel case.");
                Utilities.CompilerOutput.WriteLine("Camel case is names like `someFunction`.");
                break;

            case "4202":
                Utilities.CompilerOutput.WriteLine("It is recommended in Expresso that public field names start with a capital letter");
                Utilities.CompilerOutput.WriteLine("and be in pascal case.");
                Utilities.CompilerOutput.WriteLine("Pascal case is names like 'SomeField'.");
                break;

            case "4203":
                Utilities.CompilerOutput.WriteLine("It is recommended in Expresso that non-public field names start with a lower case letter.");
                break;

            case "4300":
                Utilities.CompilerOutput.WriteLine("Use guide guides you through when to call the method.");
                Utilities.CompilerOutput.WriteLine("Even though the reasons why you must call the method beforehand vary, with this feature you will be sure");
                Utilities.CompilerOutput.WriteLine("that you have certainly called the methods Use guide suggests.");
                break;

            case "4301":
                Utilities.CompilerOutput.WriteLine("Use guide guides you through when to call the method.");
                Utilities.CompilerOutput.WriteLine("Even though the reasons why you must call the method inside vary, with this feature you will be sure");
                Utilities.CompilerOutput.WriteLine("that you have certainly called the methods Use guide suggests.");
                break;

            case "4302":
                Utilities.CompilerOutput.WriteLine("Use guide has to be only used for methods.");
                break;

            case "4310":
                Utilities.CompilerOutput.WriteLine("Only friend methods can access to the method with the modifier 'friends'.");
                Utilities.CompilerOutput.WriteLine("Note that friend methods themselves can access to those methods that are accessible from the current method.");
                break;

            case "4400":
                Utilities.CompilerOutput.WriteLine("There are more than 9 substrings that need interpolation and that also contains curly braces");
                Utilities.CompilerOutput.WriteLine("and that may slow your program to execute.");
                break;

            case "4500":
                Utilities.CompilerOutput.WriteLine("Default parameters on object types usually means null and null is prohibited in Expresso.");
                Utilities.CompilerOutput.WriteLine("That's why we can't use default parameters on object types.");
                Utilities.CompilerOutput.WriteLine("For structs, it's due to a technical restriction.");
                break;

            case "5000":
                {
                    var is_importing_system = (bool)e.HelpObject;
                    if(is_importing_system){
                        Utilities.CompilerOutput.WriteLine("It seems that you are just misspelling it. Check for spelling, taking casing into account.");
                    }else{
                        Utilities.CompilerOutput.WriteLine("Did you forget to add the from clause? To import names from external files, you must add the from clause after the import clause.");
                        Utilities.CompilerOutput.WriteLine("The from clause tells the compiler about a file that it could find names.");
                        Utilities.CompilerOutput.WriteLine("It should be a relative path to this source file.");
                    }
                }
                break;

            default:
                throw new InvalidOperationException("Unreachable");
            }

            Console.ForegroundColor = prev_color;
        }

        /// <summary>
        /// Generates a <see cref="SymbolTable"/> instance that represents an assembly.
        /// </summary>
        /// <returns>The symbol table for assembly.</returns>
        /// <param name="assemblyPath">Assembly path.</param>
        public static SymbolTable GetSymbolTableForAssembly(string assemblyPath, SymbolTable parserSymbolTable)
        {
            var asm = Assembly.LoadFrom(assemblyPath);
            if(SymbolTableCache.TryGetValue(asm.FullName, out var cached))
                return cached;

            var root_table = new SymbolTable();

            foreach(var type in asm.GetTypes()){
                PopulateSymbolTable(root_table, type, false, parserSymbolTable);
                // FIXME: Can be deleted: 2019/8/21
                //var expresso_type_name = GetExpressoTypeName($"{type.Namespace}.{type.Name}");
                //root_table.GetTypeSymbol(expresso_type_name).Modifiers = Modifiers.Export;
            }

            SymbolTableCache.Add(asm.FullName, root_table);
            return root_table;
        }

        /// <summary>
        /// Substitutes all the type parameters appearing in an <see cref="AstType"/> with real type arguments.
        /// </summary>
        /// <returns>The generic type.</returns>
        /// <param name="target">Target.</param>
        /// <param name="genericTypes">Generic types.</param>
        public static AstType SubstituteGenericTypes(AstType target, IEnumerable<KeyValueType> genericTypes)
        {
            if(target == null || target.IsNull)
                throw new ArgumentNullException(nameof(target));

            if(genericTypes.Any(keyvalue => keyvalue.ValueType.IsNull || IsPlaceholderType(keyvalue.ValueType)))
                return target;

            var replacer = new InPlaceTypeArgumentReplacer(genericTypes);
            var replaced = target.AcceptTypeWalker(replacer);

            return replaced;
        }

        /// <summary>
        /// Extract type arguments from arguments and make them pairs with type parameters.
        /// </summary>
        /// <param name="parametersAndReturnType">Parameters.</param>
        /// <param name="realArgsAndReturnType">Real arguments.</param>
        /// <param name="typeArgs">Type arguments.</param>
        public static void ExtractTypeArguments(SymbolTable symbolTable, IEnumerable<AstType> parametersAndReturnType, IEnumerable<AstType> realArgsAndReturnType,
                                                IEnumerable<KeyValueType> typeArgs)
        {
            if(parametersAndReturnType.Count() != realArgsAndReturnType.Count())
                throw new InvalidOperationException("The number of type parameters doesn't match that of the type arguments");

            var substituter = new TypeParameterSubstituter(symbolTable, typeArgs);
            foreach(var pair in parametersAndReturnType.Zip(realArgsAndReturnType, (l, r) => new {ParameterType = l, Argument = r}))
                pair.ParameterType.AcceptTypeWalker(substituter, pair.Argument);
        }

        /// <summary>
        /// Replaces all type parameters of `target` with type arguments of `allTypeArgs`.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="allTypeArgs"></param>
        public static void SubstituteTypeParameters(IEnumerable<KeyValueType> target, IEnumerable<KeyValueType> allTypeArgs)
        {
            foreach(var pair in target.Zip(allTypeArgs, (l, r) => new {Target = l, TypeArgument = r}))
                pair.Target.ValueType.ReplaceWith(pair.TypeArgument.ValueType.Clone());
        }

        /// <summary>
        /// Creates a <see cref="SimpleType"/> instance representing the IEnumerable type that DictionaryEnumerator implements.
        /// </summary>
        /// <returns>The dictionary adaptor enumerable expresso type.</returns>
        /// <param name="typeArgs">Type arguments.</param>
        public static SimpleType CreateDictionaryAdaptorEnumerableExpressoType(IEnumerable<AstType> typeArgs)
        {
            return AstType.MakeSimpleType(
                "System.Collections.Generic.IEnumerable",
                TextLocation.Empty,
                TextLocation.Empty,
                AstType.MakeSimpleType(
                    "tuple",
                    TextLocation.Empty,
                    TextLocation.Empty,
                    typeArgs.First().Clone(),
                    typeArgs.Last().Clone()
                )
            );
        }

        /// <summary>
        /// Creates a <see cref="SimpleType"/> instance representing the IEnumerable type that Array implements.
        /// </summary>
        /// <returns>The array enumerable expresso type.</returns>
        /// <param name="typeArgs">Type arguments.</param>
        public static SimpleType CreateArrayEnumerableExpressoType(IEnumerable<AstType> typeArgs)
        {
            return AstType.MakeSimpleType("System.Collections.Generic.IEnumerable", typeArgs.Select(ta => ta.Clone()));
        }

        /// <summary>
        /// Creates a <see cref="SimpleType"/> instance representing the IEnumerable type that Slice implements.
        /// </summary>
        /// <returns>The slice enumerable expresso type.</returns>
        /// <param name="typeArgs">Type arguments.</param>
        public static SimpleType CreateSliceEnumerableExpressoType(IEnumerable<AstType> typeArgs)
        {
            return AstType.MakeSimpleType("System.Collections.Generic.IEnumerable", TextLocation.Empty, TextLocation.Empty, typeArgs.Last().Clone());
        }

        public static void ResolveTypeAlias(SimpleType type, Identifier ident)
        {
            if(ident != null && !ident.Type.IsNull && !(ident.Type is PrimitiveType))
                type.IdentifierNode.Type = ident.Type.Clone();
        }

        public static FunctionType ReplaceKeyValueTypes(FunctionType type)
        {
            var cloned = (FunctionType)type.Clone();
            foreach(var param in cloned.Parameters){
                switch(param){
                case SimpleType simple:
                    foreach(var type_arg in simple.TypeArguments){
                        switch(type_arg){
                        case KeyValueType keyvalue:
                            ReplaceKeyValueTypeImpl(type_arg, keyvalue);
                            break;
                        }
                    }
                    break;
                }
            }

            switch(cloned.ReturnType){
            case SimpleType simple2:
                foreach(var type_arg in simple2.TypeArguments){
                    switch(type_arg){
                    case KeyValueType keyvalue:
                        ReplaceKeyValueTypeImpl(type_arg, keyvalue);
                        break;
                    }
                }
                break;
            }

            return cloned;
        }

        public static void BindTypeName(SymbolTable table, AstType type)
        {
            var type_symbol = table.GetTypeSymbolInAnyScope(type.Name);
            switch(type){
            case PrimitiveType _:
                return;

            case SimpleType simple:
                if(type_symbol != null)
                    simple.IdentifierNode.IdentifierId = type_symbol.IdentifierId;

                foreach(var type_arg in simple.TypeArguments)
                    BindTypeName(table, type_arg);

                break;

            case ParameterType _:
                return;

            default:
                throw new InvalidOperationException("Unknown AstType to bind");
            }

            if(type.IdentifierNode.IdentifierId == 0){
                if(type_symbol != null){
                    throw new ParserException(
                        $"You cannot use the type symbol '{type.Name}' before defined!",
                        "ES0121",
                        type
                    );
                }else{
                    throw new ParserException(
                        $"The type symbol '{type.Name}' turns out not to be declared or accessible in the current scope {table.Name}!",
                        "ES0101",
                        type
                    );
                }
            }
        }

        public static AstType GetExpressoType(Type type)
        {
            var full_name = type.FullName ?? type.Name;
            var index = full_name.IndexOf("`", StringComparison.CurrentCulture);
            var actual_type_name = full_name.Substring(0, (index == -1) ? full_name.Length : index);
            var partial_index = type.Name.IndexOf("`", StringComparison.CurrentCulture);
            var type_name = SpecialNamesMapInverse.ContainsKey(actual_type_name) ? SpecialNamesMapInverse[actual_type_name].Item1
                                                  : type.Name.Substring(0, (partial_index == -1) ? type.Name.Length : partial_index);

            if(type_name == "Void")
                return AstType.MakeSimpleType("tuple");

            if(type_name == "Func" || type_name == "Action"){
                // This code is needed because otherwise we can't use methods in .NET that take closures
                var type_args = type.GetGenericArguments();
                var is_func = type_name.StartsWith("Func", StringComparison.CurrentCulture);
                var param_types = is_func ? type_args.Take(type_args.Length - 1).Select(arg => GetExpressoType(arg)) : type_args.Select(arg => GetExpressoType(arg));
                var return_type = is_func ? GetExpressoType(type_args.Last()) : AstType.MakeSimpleType("tuple");

                return AstType.MakeFunctionType("closure", return_type, param_types);
            }

            /*if(type_name == "Predicate"){
                var return_type = type.GenericTypeArguments;
            }*/

            // This code is needed because in C# arrays are represented as *[] where * is any type
            // That is, arrays in C# aren't array type
            if(type.IsArray)
                return AstType.MakeSimpleType("array", TextLocation.Empty, TextLocation.Empty, GetExpressoType(type.GetElementType()));

            var primitive = GetPrimitiveAstType(type_name);
            if(primitive != null)
                return primitive;

            if(type.IsGenericParameter)
                return AstType.MakeParameterType(AstNode.MakeIdentifier(type.Name));

            var fully_qualified_name = SpecialNamesMapInverse.ContainsKey(actual_type_name) ? null : type.FullName;
            var type_args2 =
                from arg in type.GetGenericArguments()
                select GetExpressoType(arg);

            return (fully_qualified_name != null) ? AstType.MakeSimpleType(AstNode.MakeIdentifier(type_name, AstType.MakeSimpleType(fully_qualified_name)), type_args2)
                                                  : AstType.MakeSimpleType(type_name, type_args2);
        }

        public static bool IsParamsParameter(ParameterInfo parameterInfo)
        {
            return parameterInfo.CustomAttributes.Any(ca => ca.AttributeType.Name == "ParamArrayAttribute");
        }

        public static bool IsExtension(MethodInfo methodInfo)
        {
            return methodInfo.CustomAttributes.Any(ca => ca.AttributeType.Name == "ExtensionAttribute");
        }

        /// <summary>
        /// Determines whether `type` is the void type.
        /// </summary>
        /// <returns><c>true</c>, if `type` is the void type, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        public static bool IsVoidType(AstType type)
        {
            if(type is SimpleType simple)
                return simple.Name == "tuple" && !simple.TypeArguments.Any();
            else
                return false;
        }

        /// <summary>
        /// Determines whether `type` is a placeholder type.
        /// </summary>
        /// <returns><c>true</c>, if `type` is a placeholder type, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        public static bool IsPlaceholderType(AstType type)
        {
            return type is PlaceholderType;
        }

        /// <summary>
        /// Traverses the type tree and determines whether it contains <see cref="ParameterType"/> nodes that match to the argument given.
        /// </summary>
        /// <returns><c>true</c>, if parameter type was contained, <c>false</c> otherwise.</returns>
        /// <param name="paramType">Parameter type.</param>
        /// <param name="type">Type.</param>
        public static bool MatchesParameterType(this ParameterType paramType, AstType type)
        {
            switch(type){
            case PrimitiveType _:
                return false;

            case SimpleType simple:
                foreach(var type_arg in simple.TypeArguments){
                    if(MatchesParameterType(paramType, type_arg))
                        return true;
                }

                return false;

            case ParameterType param_type:
                return paramType.IsMatch(param_type);

            case FunctionParameterType func_param_type:
                return paramType.MatchesParameterType(func_param_type.Type);

            default:
                throw new InvalidOperationException($"Unexpected AstType node!: {type.GetType().Name}");
            }
        }

        /// <summary>
        /// Determines whether the type contains any <see cref="ParameterType"/>.
        /// </summary>
        /// <param name="astType"></param>
        /// <returns></returns>
        public static bool ContainsAnyParameterType(AstType astType)
        {
            switch(astType){
            case PrimitiveType _:
                return false;

            case SimpleType simple:
                return simple.TypeArguments.Any(ContainsAnyParameterType);

            case ParameterType _:
                return true;

            case FunctionParameterType func_param_type:
                return ContainsAnyParameterType(func_param_type.Type);

            case MemberType member_type:
                return ContainsAnyParameterType(member_type.Target) || ContainsAnyParameterType(member_type.ChildType);

            case FunctionType func_type:
                return ContainsAnyParameterType(func_type.ReturnType) || func_type.Parameters.Any(ContainsAnyParameterType);

            case ReferenceType reference:
                return ContainsAnyParameterType(reference.BaseType);

            case KeyValueType key_value_type:
                return ContainsAnyParameterType(key_value_type.ValueType);

            case PlaceholderType _:
                return false;

            default:
                throw new InvalidOperationException($"Unexpected AstType node: {astType.GetType().Name}");
            }
        }

        public static FunctionType ConvertToFunctionType(FunctionDeclaration funcDecl)
        {
            var param_types =
                from param in funcDecl.Parameters
                select AstType.MakeFunctionParameterType(param.ReturnType.Clone(), !param.Option.IsNull, param.IsVariadic);

            var return_type = funcDecl.ReturnType.Clone();
            return AstType.MakeFunctionType(funcDecl.Name, return_type, param_types, funcDecl.TypeConstraints.Select(c => c.TypeParameter.Clone()));
        }

        /// <summary>
        /// Determines whether this <see cref="SimpleType"/> represents an Expresso enum type.
        /// </summary>
        /// <param name="simple"></param>
        /// <returns></returns>
        public static bool IsExpressoEnumType(SimpleType simple)
        {
            var other_type = simple.IdentifierNode.Type;
            return !other_type.IsNull && (other_type.Name == "tuple" || other_type.Name == "int");
        }

        /// <summary>
        /// Determines whether this <see cref="SimpleType"/> represents a foreign type in the other type section.
        /// </summary>
        /// <param name="simple"></param>
        /// <returns></returns>
        public static bool OtherTypeIsForeignType(SimpleType simple)
        {
            var other_type = simple.IdentifierNode.Type;
            return !other_type.IsNull && other_type.Name.Contains(".");
        }

        static void ReplaceKeyValueTypeImpl(AstType type, KeyValueType keyValueType)
        {
            type.ReplaceWith(keyValueType.ValueType.Clone());
        }

        static SymbolTable PopulateSymbolTable(SymbolTable table, Type type, bool isPrimitive = false, SymbolTable parserSymbolTable = null)
        {
            var type_name = type.Name;
            var expresso_type_name = GetExpressoTypeName($"{type.Namespace}.{ExtractRealTypeName(type_name)}");
            if(table.GetTypeTable(expresso_type_name) != null)
                return table.GetTypeTable(expresso_type_name);

            if(type_name.StartsWith("<>", StringComparison.CurrentCulture)){
                // ignore compiler-generated classes
                return null;
            }

            var expresso_name_builder = new StringBuilder(expresso_type_name);
            var converted_name = new StringBuilder(TypePrefix + expresso_type_name);

            var type_params = new List<ParameterType>();
            if(type.IsGenericType){
                converted_name.Append("`");
                expresso_name_builder.Append("`");
                bool first = true;
                foreach(var type_arg in type.GetGenericArguments()){
                    if(first){
                        first = false;
                    }else{
                        converted_name.Append(", ");
                        expresso_name_builder.Append(", ");
                    }

                    converted_name.Append(type_arg.Name);
                    expresso_name_builder.Append(type_arg.Name);
                    type_params.Add(AstType.MakeParameterType(type_arg.Name));
                }
            }

            var converted_name_full = converted_name.ToString();
            var expresso_type_name_full = expresso_name_builder.ToString();
            var new_table = table.Children.FirstOrDefault(t => t.Name == converted_name_full);
            bool table_was_created = new_table == null;
            new_table = new_table ?? new SymbolTable(type.IsClass ? ClassType.Class : 
                                                     type.IsEnum ? ClassType.Enum : ClassType.Interface, true);
            new_table.Name = TypePrefix + expresso_type_name;

            if(type.IsEnum){
                var full_name = type.FullName;
                foreach(var enum_name in type.GetEnumNames()){
                    var ident = AstNode.MakeIdentifier(enum_name, AstType.MakeSimpleType(AstNode.MakeIdentifier(type_name, AstType.MakeSimpleType(full_name))));
                    UniqueIdGenerator.DefineNewId(ident);
                    new_table.AddSymbol(enum_name, ident);
                }
            }else{
                new_table.AddTypeParameters(type_params);

                foreach(var public_method in type.GetMethods()){
                    var method_name = public_method.Name;
                    // We don't ignore Equals and GetHashCode methods because then we won't have any means to compare strings
                    if(/*IgnoreList.Contains(method_name) || */method_name.StartsWith("op_", StringComparison.CurrentCulture))
                        continue;
                    
                    var return_type = GetExpressoType(public_method.ReturnType);
                    var param_types =
                        from param in public_method.GetParameters()
                        let is_variadic = IsParamsParameter(param)
                        select AstType.MakeFunctionParameterType(GetExpressoType(param.ParameterType), param.IsOptional, is_variadic, param.DefaultValue);
                    var method_type_params = public_method.GetGenericArguments()
                                                          .Select(ga => AstType.MakeParameterType(ga.Name));

                    var method_type = AstType.MakeFunctionType(method_name, return_type, param_types, method_type_params);
                    new_table.AddSymbol(method_name, method_type);
                    var symbol = new_table.GetSymbol(method_name, method_type);

                    symbol.Modifiers = public_method.Attributes.HasFlag(MethodAttributes.Static) ? Modifiers.Static | Modifiers.Public : Modifiers.Public;

                    if(IsExtension(public_method)){
                        var first_param = public_method.GetParameters().First();
                        var param_type = first_param.ParameterType;
                        var param_type_name = $"{param_type.Namespace}.{ExtractRealTypeName(param_type.Name)}";

                        var this_type_table = (parserSymbolTable ?? table).GetTypeTable(GetExpressoTypeName(param_type_name));
                        if(this_type_table == null)
                            this_type_table = PopulateSymbolTable(table, param_type);

                        var transformed_param_types = param_types.Skip(1).Select(pt => pt.Clone());
                        var first_param_type = param_types.First();
                        var transformed_method_type = AstType.MakeFunctionType(method_name, return_type.Clone(), transformed_param_types, method_type_params);
                        transformed_method_type.Identifier.Modifiers = Modifiers.Extension;
                        transformed_method_type.Identifier.Type = AstType.MakeSimpleType(expresso_type_name);
                        this_type_table.AddSymbol(method_name, transformed_method_type);

                        var symbol2 = this_type_table.GetSymbol(method_name, transformed_method_type);
                        UniqueIdGenerator.DefineNewId(symbol2);
                    }
                    
                    UniqueIdGenerator.DefineNewId(symbol);
                }

                foreach(var ctor in type.GetConstructors()){
                    var name = "constructor";

                    var return_type = AstType.MakeSimpleType("tuple");
                    var param_types =
                        from p in ctor.GetParameters()
                        select GetExpressoType(p.ParameterType);
                    var ctor_type_params = ctor.IsGenericMethod ? ctor.GetGenericArguments().Select(ga => AstType.MakeParameterType(ga.Name)).ToList()
                                                                : Enumerable.Empty<ParameterType>().ToList();
                    if(type.IsGenericType){
                        ctor_type_params.AddRange(
                            type.GetGenericArguments()
                                .Select(ga => AstType.MakeParameterType(ga.Name))
                        );
                    }

                    var ctor_type = AstType.MakeFunctionType(name, return_type, param_types, ctor_type_params);
                    new_table.AddSymbol(name, ctor_type);
                    var symbol = new_table.GetSymbol(name, ctor_type);
                    UniqueIdGenerator.DefineNewId(symbol);
                }

                foreach(var field in type.GetFields()){
                    var name = field.Name;
                    var field_type = GetExpressoType(field.FieldType);

                    new_table.AddSymbol(name, field_type);
                    var symbol = new_table.GetSymbol(name, field_type);
                    UniqueIdGenerator.DefineNewId(symbol);
                }

                foreach(var property in type.GetProperties()){
                    var name = property.Name;
                    var property_type = GetExpressoType(property.PropertyType);
                    new_table.AddSymbol(name, property_type);
                    var symbol = new_table.GetSymbol(name, property_type);
                    UniqueIdGenerator.DefineNewId(symbol);
                }

                var base_types = type.GetInterfaces()
                                     .Select(i => {
                    PopulateSymbolTable(table, i);
                    // We need to resolve them with full name because we need them when we search for symbol tables
                    return (AstType)AstType.MakeSimpleType(
                        $"{i.Namespace}.{ExtractRealTypeName(i.Name)}",
                        i.GetGenericArguments()
                         .Select(ga => ga.IsGenericParameter ? AstType.MakeParameterType(ga.Name) : GetExpressoType(ga))
                    );
                }).ToList();

                if(type.BaseType != null){
                    new_table.ParentClass = PopulateSymbolTable(table, type.BaseType);
                    var base_type_name = GetExpressoTypeName($"{type.BaseType.Namespace}.{ExtractRealTypeName(type.BaseType.Name)}");
                    // FIXME: Can be deleted: 2019/9/12
                    /*new_table.ParentClass = table.GetTypeTable(base_type_name);
                    if(new_table.ParentClass == null){
                        new_table.ParentClass = PopulateSymbolTable(table, type.BaseType);
                    
                        if(new_table.ParentClass == null)
                            throw new InvalidOperationException($"{type.BaseType.Name} is already added.");
                    }*/

                    base_types.Add(AstType.MakeSimpleType(base_type_name));
                }

                new_table.ParentTypes = base_types;
            }

            if(table_was_created){
                table.AddTypeTable(new_table);

                if(!isPrimitive){
                    // Squash the `public` modifier to the `export` modifier
                    table.AddTypeSymbol(expresso_type_name, AstType.MakeSimpleType(
                        expresso_type_name, 
                        type_params.Select(p => AstType.MakeKeyValueType(p.Clone(), AstType.MakePlaceholderType()))
                    ), type.IsPublic ? Modifiers.Export : Modifiers.None);
                    var type_symbol = table.GetTypeSymbol(expresso_type_name);
                    UniqueIdGenerator.DefineNewId(type_symbol);
                }
            }

            return new_table;
        }

        static IEnumerable<Type> GetTypes(Assembly asm)
        {
            try{
                return asm.GetTypes();
            }
            catch(Exception){
                return Enumerable.Empty<Type>();
            }
        }

        static string ExtractRealTypeName(string typeName)
        {
            var backtick_index = typeName.IndexOf('`');
            return (backtick_index == -1) ? typeName : typeName.Substring(0, backtick_index);
        }
    }
}
