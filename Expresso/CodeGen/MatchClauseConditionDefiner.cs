﻿using System;
using System.Reflection;
using System.Reflection.Emit;

using Expresso.Ast;

namespace Expresso.CodeGen
{
    public partial class CodeGenerator : IAstWalker<CodeGeneratorContext, Type>
    {
        /// <summary>
        /// This class is responsible for defining the conditions of match clauses before inspecting the body statements of a match clause.
        /// </summary>
        public class MatchClauseConditionDefiner : IAstPatternWalker
	    {
            CodeGenerator generator;
            CodeGeneratorContext context;

            public MatchClauseConditionDefiner(CodeGenerator generator, CodeGeneratorContext context)
            {
                this.generator = generator;
                this.context = context;
            }

            public void VisitCollectionPattern(CollectionPattern collectionPattern)
            {
                foreach(var item in collectionPattern.Items)
                    item.AcceptPatternWalker(this);
            }

            public void VisitDestructuringPattern(DestructuringPattern destructuringPattern)
            {
                destructuringPattern.TypePath.AcceptWalker(generator, context);

                if(destructuringPattern.IsEnum){
                    var member_type = (MemberType)destructuringPattern.TypePath;
                    var variant_name = member_type.ChildType.Name;
                    var local_type = context.TemporaryVariable.LocalType;

                    // The first condition expression is needed because otherwise the test for Enum1 won't run
                    var field = generator.TreatField(local_type.Name.StartsWith("Tuple", StringComparison.CurrentCulture) ? context.TargetType : local_type,
                                                     context.LazyTypeBuilder, member_type.ChildType.IdentifierNode,
                                                     CSharpCompilerHelpers.HiddenMemberPrefix + variant_name);

                    generator.EmitLoadLocal(context.TemporaryVariable, false);
                    generator.EmitLoadField(field);
                    generator.il_generator.Emit(OpCodes.Ldnull);
                    generator.EmitBinaryOp(OperatorType.InEquality);
                }else{
                    var native_type = CSharpCompilerHelpers.GetNativeType(destructuringPattern.TypePath);

                    generator.EmitLoadLocal(context.TemporaryVariable, false);
                    generator.il_generator.Emit(OpCodes.Isinst, native_type);
                    generator.il_generator.Emit(OpCodes.Ldnull);
                    generator.EmitBinaryOp(OperatorType.InEquality);
                }
            }

            public void VisitExpressionPattern(ExpressionPattern exprPattern)
            {
                // Common scinario in an expression pattern:
                // An integer sequence expression or a literal expression.
                // In the former case we should test an integer against an IntSeq type object using an IntSeq's method
                // while in the latter case we should just test the value against the literal
                context.RequestMethod = true;
                context.Method = null;
                var type = exprPattern.Expression.AcceptWalker(generator, context);
                context.RequestMethod = false;

                if(context.Method != null && context.Method.DeclaringType.Name == "ExpressoIntegerSequence"){
                    var method = context.Method;
                    context.Method = null;

                    generator.EmitLoadLocal(context.TemporaryVariable, false);
                    generator.il_generator.Emit(OpCodes.Callvirt, method);
                }else if(context.ContextAst is MatchStatement){
                    generator.EmitLoadLocal(context.TemporaryVariable, false);
                    generator.il_generator.Emit(OpCodes.Ceq);
                }
            }

            public void VisitIdentifierPattern(IdentifierPattern identifierPattern)
            {
                var type = generator.DetermineType(identifierPattern.Identifier.Type, context);
                var local_builder = generator.il_generator.DeclareLocal(type);
                AddSymbol(identifierPattern.Identifier, new ExpressoSymbol{LocalBuilder = local_builder});

                context.CurrentTargetVariable = local_builder;
                identifierPattern.InnerPattern.AcceptPatternWalker(this);
            }

            public void VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
            {
            }

            public void VisitKeyValuePattern(KeyValuePattern keyValuePattern)
            {
                keyValuePattern.Value.AcceptPatternWalker(this);
            }

            public void VisitMatchClause(MatchPatternClause matchClause)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitTuplePattern(TuplePattern tuplePattern)
            {
                // TODO: implement it properly(IgnoreRest pattern and Wildcard pattern can't be translated to actual code)
                //var native_types = tuplePattern.Patterns.Select(p => CSharpCompilerHelpers.GetNativeType(p.AcceptWalker(generator.item_type_inferencer)));
                var tuple_type = context.TemporaryVariable.LocalType;
                generator.EmitLoadLocal(context.TemporaryVariable, false);
                generator.il_generator.Emit(OpCodes.Isinst, tuple_type);
            }

            public void VisitTypePathPattern(TypePathPattern pathPattern)
            {
                // We can't just delegate it to VisitMemberType because we can't distinguish between tuple style enums and raw value style enums
                var member = (MemberType)pathPattern.TypePath;
                context.RequestType = true;
                member.Target.AcceptWalker(generator, context);
                context.RequestType = false;

                // We can't just delegate to VisitSimpleType because it doesn't take properties or fields into account
                var prev_op_type = context.OperationTypeOnIdentifier;
                context.OperationTypeOnIdentifier = OperationType.None;
                context.RequestMethod = true;
                context.RequestPropertyOrField = true;
                generator.VisitIdentifier(member.ChildType.IdentifierNode, context);
                context.RequestMethod = false;
                context.RequestPropertyOrField = false;
                context.OperationTypeOnIdentifier = prev_op_type;

                var field = (FieldInfo)context.PropertyOrField;
                context.PropertyOrField = null;

                if (field.Attributes.HasFlag(FieldAttributes.Literal))
                    generator.EmitObject(field.GetRawConstantValue());
                else
                    generator.EmitLoadField(field);

                generator.EmitLoadLocal(context.TemporaryVariable, false);
                generator.EmitBinaryOp(OperatorType.Equality);
            }

            public void VisitWildcardPattern(WildcardPattern wildcardPattern)
            {
            }

            public void VisitPatternWithType(PatternWithType pattern)
            {
                pattern.Pattern.AcceptPatternWalker(this);
            }

            public void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitNullNode(AstNode nullNode) => throw new InvalidOperationException("Can not work on that node");
        }
    }
}
