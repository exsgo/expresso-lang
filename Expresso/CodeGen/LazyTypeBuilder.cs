﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

using Expresso.Ast;
#if WINDOWS
using System.Runtime.CompilerServices;
#endif

namespace Expresso.CodeGen
{
    public partial class CodeGenerator : IAstWalker<CodeGeneratorContext, Type>
    {
        /// <summary>
        /// A specialized type builder that allows users to delay the implementation of types.
        /// Although it now doesn't use the expression tree to emit IL,
        /// most of the implementation was taken from the following web site "http://takeshik.org/blog/2011/12/14/expression-trees-with-il-emit/".
        /// </summary>
        /// <remarks><see cref="LazyTypeBuilder"/> is needed because otherwise references to `FieldBuilder`s won't work.</remarks>
        public class LazyTypeBuilder
        {
            readonly Type[] types;
            readonly TypeBuilder impl_type_builder;
            readonly Dictionary<string, MemberInfo> members;
            readonly List<string> has_initializer_list = new List<string>();
            readonly Dictionary<string, LazyTypeBuilder> nested_types = new Dictionary<string, LazyTypeBuilder>();
            // We can't remove prologue and static_prologue because of when we define them
            MethodBuilder prologue, static_prologue;
            Type type_cache;
            bool is_created, is_raw_value_enum;

            /// <summary>
            /// Represents whether the interface type is already created.
            /// </summary>
            /// <value><c>true</c> if the interface type is defined; otherwise, <c>false</c>.</value>
            public bool IsDefined => type_cache != null;

            /// <summary>
            /// Gets the base type.
            /// </summary>
            /// <value>The base type.</value>
            public Type BaseType => TypeBuilder.BaseType;

            /// <summary>
            /// Gets the interface type as <see cref="InterfaceType"/>.
            /// This method should be called after <see cref="CreateInterfaceType()"/> is called.
            /// </summary>
            /// <value>The interface type as <see cref="InterfaceType"/>.</value>
            public Type InterfaceType{
                get{
                    if(type_cache == null)
                        throw new InvalidOperationException("The interface type is yet to be defined");
                    
                    return type_cache;
                }
            }

            /// <summary>
            /// Gets the <see cref="System.Reflection.Emit.TypeBuilder"/> that represents the interface type.
            /// </summary>
            /// <value>The interface type builder.</value>
            public TypeBuilder TypeBuilder{
                get; private set;
            }

            /// <summary>
            /// Gets the name of this type.
            /// </summary>
            /// <value>The name.</value>
            public string Name => TypeBuilder.Name;

            public bool HasStaticFields{
                get{
                    return members.Values
                                  .OfType<FieldBuilder>()
                                  .Any(fb => fb.IsStatic);
                }
            }

            public LazyTypeBuilder(ModuleBuilder module, string name, TypeAttributes attr, Type baseType, bool isGlobalFunctions, bool isRawValueEnum)
                : this(module.DefineType(name, attr, baseType), isGlobalFunctions, isRawValueEnum)
            {
            }

            LazyTypeBuilder(TypeBuilder builder, bool isGlobalFunctions, bool isTupleStyleEnum)
            {
                TypeBuilder = builder;
                types = isGlobalFunctions ? Type.EmptyTypes : new []{builder};
                // the impl type is needed because otherwise we can't use fields
                impl_type_builder = TypeBuilder.DefineNestedType("<Impl>", TypeAttributes.NestedPrivate);
    #if WINDOWS
                members = new Dictionary<string, MemberInfo>();
    #else
                members = builder.GetMethods()
                                 .OfType<MethodBuilder>()
                                 .Cast<MemberInfo>()
                                 .ToDictionary(mi => mi.Name);
    #endif
                is_created = false;
                is_raw_value_enum = isTupleStyleEnum;
            }

            /// <summary>
            /// Defines a new field on this type.
            /// </summary>
            /// <returns>The field.</returns>
            /// <param name="name">Name.</param>
            /// <param name="type">Type.</param>
            /// <param name="attr">Attr.</param>
            public FieldBuilder DefineField(string name, Type type, bool hasInitializer, FieldAttributes attr = FieldAttributes.Public)
            {
                var field = TypeBuilder.DefineField(name, type, attr);
                if(hasInitializer)
                    has_initializer_list.Add(name);
                
                members.Add(name, field);
                return field;
            }

            /// <summary>
            /// Defines a new method on this type.
            /// Returns a <see cref="MethodBuilder"/> that represents the interface method.
            /// </summary>
            /// <returns>The method.</returns>
            /// <param name="name">Name.</param>
            /// <param name="returnType">Return type.</param>
            /// <param name="parameterTypes">Parameter types.</param>
            public MethodBuilder DefineMethod(string name, MethodAttributes attr, Type returnType, Type[] parameterTypes, CodeGenerator generator = null,
                                              CodeGeneratorContext context = null, Ast.FunctionDeclaration funcDecl = null)
            {
                var method = TypeBuilder.DefineMethod(name, attr, returnType, parameterTypes);

                if(funcDecl != null){
                    var return_value_builder = method.DefineParameter(0, ParameterAttributes.Retval, null);
                    context.CustomAttributeSetter = return_value_builder.SetCustomAttribute;
                    context.AttributeTarget = AttributeTargets.ReturnValue;
                    funcDecl.Attribute.AcceptWalker(generator, context);

                    foreach(var pair in Enumerable.Range(1, parameterTypes.Length + 1).Zip(funcDecl.Parameters, (p, arg) => new {Index = p, ParameterDeclaration = arg})){
                        var param_attr = pair.ParameterDeclaration.Option.IsNull ? ParameterAttributes.None : ParameterAttributes.HasDefault | ParameterAttributes.Optional;
                        var param_builder = method.DefineParameter(pair.Index, param_attr, pair.ParameterDeclaration.Name);
                        if(!pair.ParameterDeclaration.Option.IsNull){
                            var option = pair.ParameterDeclaration.Option;
                            var default_value = (option is Ast.LiteralExpression literal) ? literal.Value : throw new InvalidOperationException($"Invalid default value: {option}!");
                            param_builder.SetConstant(default_value);
                        }

                        context.CustomAttributeSetter = param_builder.SetCustomAttribute;
                        context.AttributeTarget = AttributeTargets.Parameter;
                        pair.ParameterDeclaration.Attribute.AcceptWalker(generator, context);
                    }
                }

                foreach(var interface_type in TypeBuilder.GetInterfaces()){
                    var overriding_method = interface_type.GetMethod(name);
                    if(overriding_method != null)
                        TypeBuilder.DefineMethodOverride(method, overriding_method);
                }

                if(attr.HasFlag(MethodAttributes.Abstract))
                    return method;

                var il = method.GetILGenerator();
                // Emit call to the implementation method no matter whether we actually need it.
                var real_params = attr.HasFlag(MethodAttributes.Static) ? parameterTypes : types.Concat(parameterTypes).ToArray();
                var impl_method = impl_type_builder.DefineMethod(name + "_Impl", MethodAttributes.Assembly | MethodAttributes.Static, returnType, real_params);
                LoadArgs(il, (real_params.Length == 0) ? new int[]{} : Enumerable.Range(0, real_params.Length));
                il.Emit(OpCodes.Call, impl_method);
                il.Emit(OpCodes.Ret);

                members.Add(name, impl_method);
                return method;
            }

            /// <summary>
            /// Defines a new generic method on this type.
            /// Returns a <see cref="MethodBuilder"/> that represents the interface method.
            /// </summary>
            /// <returns>The method.</returns>
            /// <param name="name">Name.</param>
            public MethodBuilder DefineGenericMethod(string name, MethodAttributes attr, CodeGenerator generator = null, CodeGeneratorContext context = null,
                                                     Ast.FunctionDeclaration funcDecl = null)
            {
                if(funcDecl == null)
                    throw new ArgumentNullException(nameof(funcDecl));

                var generic_method = TypeBuilder.DefineMethod(name, attr);

                var generic_params = generic_method.DefineGenericParameters(funcDecl.TypeConstraints.Select(tc => tc.TypeParameter.Name).ToArray())
                    .Select(gp => generator.SetConstraints(funcDecl.TypeConstraints, context, gp))
                    .ToList();
                var generic_params_for_interface_method = generator.generic_types.Concat(generic_params);

                context.GenericTypeParameterReplacer = new GenericTypeParameterReplacer(generic_params_for_interface_method, context);

                var param_types = funcDecl.Parameters.Select((param, index) => {
                    context.ParameterIndex = funcDecl.Modifiers.HasFlag(Modifiers.Static) ? index : index + 1;
                    return generator.VisitParameterDeclaration(param, context);
                }).ToArray();
                context.ParameterIndex = -1;
                generic_method.SetParameters(param_types);

                var return_type = funcDecl.ReturnType.AcceptTypeWalker(context.GenericTypeParameterReplacer);
                generic_method.SetReturnType(return_type);

                var return_value_builder = generic_method.DefineParameter(0, ParameterAttributes.Retval, null);
                context.CustomAttributeSetter = return_value_builder.SetCustomAttribute;
                context.AttributeTarget = AttributeTargets.ReturnValue;
                // We can't rewrite this as generator.VisitAttributeSection because then we would visit null nodes
                funcDecl.Attribute.AcceptWalker(generator, context);

                foreach(var pair in Enumerable.Range(1, funcDecl.Parameters.Count + 1).Zip(funcDecl.Parameters, (p, arg) => new {Index = p, ParameterDeclaration = arg})){
                    var param_attr = pair.ParameterDeclaration.Option.IsNull ? ParameterAttributes.None : ParameterAttributes.HasDefault | ParameterAttributes.Optional;
                    var param_builder = generic_method.DefineParameter(pair.Index, param_attr, pair.ParameterDeclaration.Name);
                    if(!pair.ParameterDeclaration.Option.IsNull){
                        var option = pair.ParameterDeclaration.Option;
                        var default_value = (option is LiteralExpression literal) ? literal.Value : throw new InvalidOperationException($"Invalid default value: {option}!");
                        param_builder.SetConstant(default_value);
                    }

                    context.CustomAttributeSetter = param_builder.SetCustomAttribute;
                    context.AttributeTarget = AttributeTargets.Parameter;
                    pair.ParameterDeclaration.Attribute.AcceptWalker(generator, context);
                }

                foreach(var interface_type in TypeBuilder.GetInterfaces()){
                    var overriding_method = interface_type.GetMethod(name);
                    if(overriding_method != null)
                        TypeBuilder.DefineMethodOverride(generic_method, overriding_method);
                }

                if(attr.HasFlag(MethodAttributes.Abstract))
                    return generic_method;

                var il = generic_method.GetILGenerator();
                // Emit call to the implementation method no matter whether we actually need it.
                var generic_impl_method = impl_type_builder.DefineMethod(name + "_Impl", MethodAttributes.Assembly | MethodAttributes.Static);

                var generic_params2 = generic_impl_method.DefineGenericParameters(funcDecl.TypeConstraints.Select(tc => tc.TypeParameter.Name).ToArray())
                    .Select(gp => generator.SetConstraints(funcDecl.TypeConstraints, context, gp))
                    .ToList();
                generator.generic_types.AddRange(generic_params2);

                context.GenericTypeParameterReplacer = new GenericTypeParameterReplacer(generic_params_for_interface_method, context);

                var impl_param_types = funcDecl.Parameters.Select(p => p.ReturnType.AcceptTypeWalker(context.GenericTypeParameterReplacer)).ToArray();
                var impl_params = attr.HasFlag(MethodAttributes.Static) ? impl_param_types : types.Concat(impl_param_types).ToArray();
                generic_impl_method.SetParameters(impl_params);

                var impl_return_type = funcDecl.ReturnType.AcceptTypeWalker(context.GenericTypeParameterReplacer);
                generic_impl_method.SetReturnType(impl_return_type);

                LoadArgs(il, (impl_params.Length == 0) ? new int[]{} : Enumerable.Range(0, impl_params.Length));

                var impl_method = generic_impl_method.MakeGenericMethod(generic_params.ToArray());
                il.Emit(OpCodes.Call, impl_method);
                il.Emit(OpCodes.Ret);

                members.Add(name, generic_impl_method);
                return generic_method;
            }

            /// <summary>
            /// Defines a new constructor on this type.
            /// </summary>
            /// <returns>The constructor.</returns>
            /// <param name="parameterTypes">Parameter types.</param>
            /// <param name="body">Body.</param>
            ConstructorBuilder DefineConstructor(Type[] parameterTypes, Ast.Expression body = null)
            {
                var ctor = TypeBuilder.DefineConstructor(
                    MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName,
                    CallingConventions.Standard, parameterTypes
                );
                var il = ctor.GetILGenerator();
                if(!TypeBuilder.BaseType.Attributes.HasFlag(TypeAttributes.Interface)){
                    LoadArgs(il, 0);
                    il.Emit(OpCodes.Call, TypeBuilder.BaseType.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, Type.EmptyTypes, null));
                }

                if(prologue != null){
                    LoadArgs(il, 0);
                    il.Emit(OpCodes.Call, prologue);
                }

                members.Add("constructor", ctor);
                return ctor;
            }

            /// <summary>
            /// Defines and creates a new LazyTypeBuilder instance as a nested type.
            /// </summary>
            /// <returns>The nested type.</returns>
            /// <param name="name">Name.</param>
            /// <param name="baseTypes">Base types.</param>
            public LazyTypeBuilder DefineNestedType(string name, TypeAttributes attr, IEnumerable<Type> baseTypes)
            {
                var real_attr = attr.HasFlag(TypeAttributes.Public) ? TypeAttributes.NestedPublic : TypeAttributes.NestedPrivate;
                var tmp = new LazyTypeBuilder(TypeBuilder.DefineNestedType(name, real_attr, baseTypes.Any() ? baseTypes.First() : typeof(object), baseTypes.Skip(1).ToArray()), false, is_raw_value_enum);
                nested_types.Add(name, tmp);
                return tmp;
            }

            /// <summary>
            /// Defines generic type parameters.
            /// </summary>
            /// <returns>The generic type parameters.</returns>
            /// <param name="names">Names.</param>
            public GenericTypeParameterBuilder[] DefineGenericParameters(params string[] names)
            {
                impl_type_builder.DefineGenericParameters(names);
                return TypeBuilder.DefineGenericParameters(names);
            }

            /// <summary>
            /// Completes defining the interface type(the outer type that the user actually accesses).
            /// </summary>
            /// <returns>The interface type.</returns>
            public Type CreateInterfaceType()
            {
                if(is_raw_value_enum || types.Any()){
                    var fields = members.Values
                                        .OfType<FieldBuilder>()
                                        .Where(fb => !has_initializer_list.Contains(fb.Name));
                    var param_types = fields.Select(fb => fb.FieldType)
                                            .ToArray();

                    //var generic_names = genericTypes?.Select(builder => builder.Name).ToArray();
                    if(has_initializer_list.Any())
                        prologue = impl_type_builder.DefineMethod("Prologue", MethodAttributes.Assembly | MethodAttributes.Static, typeof(void), types);

                    var ctor = DefineConstructor(param_types);

                    var il_generator = ctor.GetILGenerator();
                    foreach(var pair in Enumerable.Range(1, fields.Count()).Zip(fields, (i, r) => new {Counter = i, Field = r})){
                        LoadArgs(il_generator, new []{0});
                        LoadArgs(il_generator, new []{pair.Counter});

                        il_generator.Emit(OpCodes.Stfld, pair.Field);
                    }
                    il_generator.Emit(OpCodes.Ret);
                }

                if(HasStaticFields){
                    if(static_prologue == null)
                        static_prologue = impl_type_builder.DefineMethod("StaticPrologue", MethodAttributes.Assembly | MethodAttributes.Static, typeof(void), null);

                    DefineStaticConstructor();
                }

                if(type_cache == null)
                    type_cache = TypeBuilder.CreateType();

                return type_cache;
            }

            /// <summary>
            /// Completes defining the implementation type(the inner type that contains real implementations).
            /// </summary>
            /// <returns>The interface type.</returns>
            public Type CreateType()
            {
                if(TypeBuilder.Attributes.HasFlag(TypeAttributes.Interface))
                    return null;

                if(type_cache == null)
                    throw new InvalidOperationException("Call CreateInterfaceType before completing the type.");

                if(is_created)
                    return type_cache;

    /*#if WINDOWS
                var debug_info_generator = DebugInfoGenerator.CreatePdbGenerator();
                foreach(var implementer in implementers){
                    var expr = implementer.Item1 as LambdaExpression;
                    var lambda = expr ?? Expression.Lambda(implementer.Item1);
                    lambda.CompileToMethod(implementer.Item2, debug_info_generator);
                }
    #else
                var debug_info_generator = PortablePDBGenerator.CreatePortablePDBGenerator();
                foreach(var implementer in implementers){
                    var expr = implementer.Item1 as LambdaExpression;
                    var lambda = expr ?? Expression.Lambda(implementer.Item1);
                    lambda.CompileToMethod(implementer.Item2, debug_info_generator);
                }*/

                /*var debug_info_generator = PortablePDBGenerator.CreatePortablePDBGenerator();
                Utilities.CompilerOutput.WriteLine("Emitting a PDB on type {0}...", interface_type_builder.Name);
                debug_info_generator.WriteToFile(pdb_file_path);
#endif*/

                if(prologue != null)
                    prologue.GetILGenerator().Emit(OpCodes.Ret);
                if(static_prologue != null)
                    static_prologue.GetILGenerator().Emit(OpCodes.Ret);

                impl_type_builder.CreateType();
                is_created = true;
                return type_cache;
            }

            /// <summary>
            /// Gets a method on the interface type using name and flags.
            /// </summary>
            /// <returns>The interface method.</returns>
            /// <param name="name">Name.</param>
            /// <param name="flags">Flags.</param>
            public MethodInfo GetInterfaceMethod(string name, BindingFlags flags)
            {
                if(type_cache == null)
                    throw new InvalidOperationException($"The interface type is yet to be defined: you are trying to get the interface method of '{name}'.");

                return type_cache.GetMethod(name, flags);
            }

            /// <summary>
            /// Gets a method defined on this type. Note that it searches for non-public methods.
            /// </summary>
            /// <returns>The method.</returns>
            /// <param name="name">Name.</param>
            public MethodBuilder GetMethodBuilder(string name)
            {
                if(members.TryGetValue(name, out var method))
                    return (MethodBuilder)method;
                else
                    return null;
            }

            /// <summary>
            /// Gets a field defined on this type. Note that it searches for non-public fields.
            /// </summary>
            /// <returns>The field.</returns>
            /// <param name="name">Name.</param>
            public FieldBuilder GetFieldBuilder(string name)
            {
                if(members.TryGetValue(name, out var field))
                    return (FieldBuilder)field;
                else
                    return null;
            }

            public FieldInfo GetField(string name, BindingFlags flags)
            {
                if(type_cache == null)
                    throw new InvalidOperationException("The interface type is yet to be defined");

                return type_cache.GetField(name, flags);
            }

            public ConstructorBuilder GetConstructor(Type[] paramTypes)
            {
                if(members.TryGetValue("constructor", out var ctor))
                    return (ConstructorBuilder)ctor;
                else
                    return null;
            }

            /// <summary>
            /// Gets a nested type.
            /// </summary>
            /// <returns>The nested type.</returns>
            /// <param name="name">The name to search.</param>
            public LazyTypeBuilder GetNestedType(string name)
            {
                return nested_types[name];
            }

            /// <summary>
            /// Gets the <see cref="ILGenerator"/> for initializing the field.
            /// </summary>
            /// <param name="field">Field.</param>
            public ILGenerator GetILGeneratorForFieldInit(FieldBuilder field)
            {
                if(field.IsStatic)
                    return static_prologue?.GetILGenerator();
                else
                    return prologue?.GetILGenerator();
            }

            public override string ToString()
            {
                return $"[LazyTypeBuilder: Name={TypeBuilder.Name}, BaseType={BaseType}]";
            }

            ConstructorBuilder DefineStaticConstructor()
            {
                var cctor = TypeBuilder.DefineConstructor(
                    MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName,
                    CallingConventions.Standard, null
                );
                var il = cctor.GetILGenerator();

                il.Emit(OpCodes.Call, static_prologue);

                il.Emit(OpCodes.Ret);
                members.Add("cctor", cctor);
                return cctor;
            }

            static void LoadArgs(ILGenerator ilGenerator, IEnumerable<int> indices)
            {
                foreach(var i in indices){
                    switch(i){
                    case 0:
                        ilGenerator.Emit(OpCodes.Ldarg_0);
                        break;

                    case 1:
                        ilGenerator.Emit(OpCodes.Ldarg_1);
                        break;

                    case 2:
                        ilGenerator.Emit(OpCodes.Ldarg_2);
                        break;

                    case 3:
                        ilGenerator.Emit(OpCodes.Ldarg_3);
                        break;

                    default:
                        if(i <= short.MaxValue)
                            ilGenerator.Emit(OpCodes.Ldarg_S, (short)i);
                        else
                            ilGenerator.Emit(OpCodes.Ldarg, i);

                        break;
                    }
                }
            }

            static void LoadArgs(ILGenerator ilGenerator, params int[] indices)
            {
                LoadArgs(ilGenerator, (IEnumerable<int>)indices);
            }
        }
    }
}

