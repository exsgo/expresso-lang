using System.IO;
using System.Collections.Generic;
using System.Linq;
using ICSharpCode.NRefactory.PatternMatching;


namespace Expresso.Ast
{
    /// <summary>
    /// An ast walker that outputs each node as a sequence of strings for debugging purpose.
    /// </summary>
    public class DebugOutputWalker : IAstWalker
    {
        static Dictionary<string, string[]> Enclosures = new Dictionary<string, string[]>{
            {"vector", new []{"[", "]"}},
            {"array", new []{"[", "]"}},
            {"dictionary", new []{"{", "}"}},
            {"tuple", new []{"(", ")"}}
        };

        readonly TextWriter writer;

        public DebugOutputWalker(TextWriter writer)
        {
            this.writer = writer;
        }

        void PrintLongList<TObject>(IEnumerable<TObject> list, string terminator = "...", int numberItems = 5)
            where TObject : AstNode
        {
            int i = 0, max_items = numberItems + 1;
            foreach(var elem in list){
                if(i > max_items){
                    writer.Write(", {0}", terminator);
                    break;
                }else if(i != 0){
                    writer.Write(", ");
                }

                elem.AcceptWalker(this);
                ++i;
            }
        }

        void PrintList<T>(IEnumerable<T> list)
            where T : AstNode
        {
            if(!list.Any())
                return;
            
            var first = list.First();
            first.AcceptWalker(this);
            foreach(var elem in list.Skip(1)){
                writer.Write(", ");
                elem.AcceptWalker(this);
            }
        }

        #region IAstWalker implementation

        public void VisitAst(ExpressoAst ast)
        {
            if(!ast.Attributes.IsEmpty)
                PrintList(ast.Attributes);

            #if LIBRARY
            writer.WriteLine($"<module {ast.Name}>");
            writer.WriteLine("{");
            PrintList(ast.Declarations);
            writer.WriteLine("}");
            #else
            writer.Write($"<module {ast.Name} : {ast.Declarations.Count}>");
            #endif
        }

        public void VisitBlock(BlockStatement block)
        {
            #if LIBRARY
            writer.WriteLine("{");
            PrintList(block.Statements);
            writer.WriteLine("}");
            #else
            writer.Write("{count: ");
            writer.Write(block.Statements.Count);
            writer.Write("...}");
            #endif
        }

        public void VisitBreakStatement(BreakStatement breakStmt)
        {
            writer.Write("break upto ");
            breakStmt.CountExpression.AcceptWalker(this);
        }

        public void VisitContinueStatement(ContinueStatement continueStmt)
        {
            writer.Write("continue upto ");
            continueStmt.CountExpression.AcceptWalker(this);
        }

        public void VisitDoWhileStatement(DoWhileStatement doWhileStmt)
        {
            #if LIBRARY
            writer.Write("do");
            VisitBlock(doWhileStmt.Delegator.Body);
            writer.Write("while ");
            #else
            writer.Write("do{");
            writer.Write($"...{doWhileStmt.Delegator.Body.Statements.Count}");
            writer.Write("}while ");
            #endif
            doWhileStmt.Delegator.Condition.AcceptWalker(this);
        }

        public void VisitEmptyStatement(EmptyStatement emptyStmt)
        {
            writer.Write("<empty>");
        }

        public void VisitExpressionStatement(ExpressionStatement exprStmt)
        {
            exprStmt.Expression.AcceptWalker(this);
        }

        public void VisitForStatement(ForStatement forStmt)
        {
            writer.Write("for ");
            forStmt.Left.AcceptWalker(this);
            writer.Write(" in ");
            forStmt.Target.AcceptWalker(this);
            #if LIBRARY
            VisitBlock(forStmt.Body);
            #else
            writer.Write("{...}");
            #endif
        }

        public void VisitIfStatement(IfStatement ifStmt)
        {
            writer.Write("if ");
            ifStmt.Condition.AcceptWalker(this);
            #if LIBRARY
            VisitBlock(ifStmt.TrueBlock);
            #else
            writer.Write("{...}");
            #endif
            if(!ifStmt.FalseStatement.IsNull){
            #if LIBRARY
                ifStmt.FalseStatement.AcceptWalker(this);
            #else
                writer.Write("else{...}");
            #endif
            }
        }

        public void VisitMatchStatement(MatchStatement matchStmt)
        {
            writer.Write("match ");
            matchStmt.Target.AcceptWalker(this);
            writer.Write(" {");
            #if LIBRARY
            PrintList(matchStmt.Clauses);
            writer.Write("}");
            #else
            writer.Write(matchStmt.Clauses.Count);
            writer.Write("...}");
            #endif
        }

        public void VisitReturnStatement(ReturnStatement returnStmt)
        {
            writer.Write("return ");
            if(!returnStmt.Expression.IsNull)
                returnStmt.Expression.AcceptWalker(this);

            writer.Write(";");
        }

        public void VisitThrowStatement(ThrowStatement throwStmt)
        {
            writer.Write("throw ");
            throwStmt.CreationExpression.AcceptWalker(this);
        }

        public void VisitTryStatement(TryStatement tryStmt)
        {
            writer.Write("try");
            VisitBlock(tryStmt.EnclosingBlock);
            tryStmt.CatchClauses.AcceptWalker(this);
            tryStmt.FinallyClause.AcceptWalker(this);
        }

        public void VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStmt)
        {
            writer.Write("for ");
            writer.Write(valueBindingForStmt.Modifiers.HasFlag(Modifiers.Immutable) ? "let" : "var");
            writer.Write(' ');
            valueBindingForStmt.Initializer.Pattern.AcceptWalker(this);
            writer.Write(" in ");
            valueBindingForStmt.Initializer.Initializer.AcceptWalker(this);
            #if LIBRARY
            VisitBlock(valueBindingForStmt.Body);
            #else
            writer.Write("{...}");
            #endif
        }

        public void VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl)
        {
            if(varDecl.Modifiers.HasFlag(Modifiers.Immutable))
                writer.Write("let ");
            else
                writer.Write("var ");

            PrintLongList(varDecl.Variables, "...", 1);
        }

        public void VisitWhileStatement(WhileStatement whileStmt)
        {
            writer.Write("while ");
            whileStmt.Condition.AcceptWalker(this);
            writer.Write("{...}");
        }

        public void VisitYieldStatement(YieldStatement yieldStmt)
        {
            writer.Write("yield ");
            yieldStmt.Expression.AcceptWalker(this);
            writer.Write(";");
        }

        public void VisitAssignment(AssignmentExpression assignment)
        {
            assignment.Left.AcceptWalker(this);
            writer.Write(" ");
            switch(assignment.Operator){
                case OperatorType.Assign:
                writer.Write("= ");
                break;

                case OperatorType.Plus:
                writer.Write("+= ");
                break;

                case OperatorType.Minus:
                writer.Write("-= ");
                break;

                case OperatorType.Times:
                writer.Write("*= ");
                break;

                case OperatorType.Divide:
                writer.Write("/= ");
                break;

                case OperatorType.Modulus:
                writer.Write("%= ");
                break;

                case OperatorType.Power:
                writer.Write("**= ");
                break;
            }
            assignment.Right.AcceptWalker(this);
        }

        public void VisitBinaryExpression(BinaryExpression binaryExpr)
        {
            binaryExpr.Left.AcceptWalker(this);
            writer.Write(' ');
            binaryExpr.OperatorToken.AcceptWalker(this);
            writer.Write(' ');
            binaryExpr.Right.AcceptWalker(this);
        }

        public void VisitCallExpression(CallExpression callExpr)
        {
            callExpr.Target.AcceptWalker(this);
            if(callExpr.TypeArguments.Any()){
                writer.Write("<");
                PrintList(callExpr.TypeArguments);
                writer.Write(">");
            }
            writer.Write("(");
            PrintList(callExpr.Arguments);
            writer.Write(")");
        }

        public void VisitCastExpression(CastExpression castExpr)
        {
            castExpr.Target.AcceptWalker(this);
            writer.Write(" as ");
            castExpr.ToType.AcceptWalker(this);
        }

        public void VisitCatchClause(CatchClause catchClause)
        {
            writer.Write("catch ");
            VisitIdentifier(catchClause.Identifier);
            #if LIBRARY
            VisitBlock(catchClause.Body);
            #else
            writer.Write("{...}");
            #endif
        }

        public void VisitIdentifier(Identifier ident)
        {
            if(ident.Modifiers.HasFlag(Modifiers.Immutable))
                writer.Write("Immutable ");
            
            writer.Write(ident.Name);
            writer.Write (" @ ");
            if (ident.IdentifierId == 0)
                writer.Write ("<invalid>");
            else
                writer.Write ("<id: {0}>", ident.IdentifierId);

            if (!ident.Type.IsNull){
                writer.Write(" (- ");
                ident.Type.AcceptWalker(this);
            }
        }

        public void VisitMemberReference(MemberReferenceExpression memRef)
        {
            memRef.Target.AcceptWalker(this);
            writer.Write(".");
            VisitIdentifier(memRef.Member);
        }

        public void VisitSequenceInitializer(SequenceInitializer seqInitializer)
        {
            var enclosures = Enclosures[seqInitializer.ObjectType.Name];
            writer.Write(enclosures[0]);

            PrintLongList(seqInitializer.Items, "~~");

            if(seqInitializer.ObjectType.Identifier == "vector")
                writer.Write("...");

            writer.Write(enclosures[1]);
        }

        // FIXME: Can be deleted: 2019/8/25
        /*public void VisitMatchPatternClause(MatchPatternClause clause)
        {
            PrintList(clause.Patterns);
            /*
            bool first = true;
            foreach(var pattern in clause.Patterns){
                if(!first)
                    writer.Write(" | ");
                else
                    first = false;

                pattern.AcceptWalker(this);
            }

            writer.Write(" => ");
            clause.Body.AcceptWalker(this);
        }*/

        public void VisitSequenceExpression(SequenceExpression seqExpr)
        {
            PrintList(seqExpr.Items);
        }

        public void VisitUnaryExpression(UnaryExpression unaryExpr)
        {
            unaryExpr.OperatorToken.AcceptWalker(this);
            unaryExpr.Operand.AcceptWalker(this);
        }

        public void VisitNullNode(AstNode nullNode)
        {
            writer.Write("<null>");
        }

        public void VisitPatternPlaceholder(AstNode placeholder, Pattern child)
        {
            writer.Write("<placeholder for patterns>");
        }

        public void VisitArrayInitializer(ArrayInitializerExpression arrayInitializer)
        {
            VisitPrimitiveType(arrayInitializer.TypePath);
            writer.Write("[");
            writer.Write(arrayInitializer.SizeExpression);
            writer.Write("]");
        }

        public void VisitClosureLiteralExpression(ClosureLiteralExpression closure)
        {
            writer.Write("|");
            PrintList(closure.Parameters);
            writer.Write("| -> ");
            closure.ReturnType.AcceptWalker(this);
            VisitBlock(closure.Body);
        }

        public void VisitComprehensionExpression(ComprehensionExpression comp)
        {
            writer.Write("[");
            comp.Item.AcceptWalker(this);
            VisitComprehensionForClause(comp.Body);
            writer.Write("]");
        }

        public void VisitComprehensionForClause(ComprehensionForClause compFor)
        {
            writer.Write("... for ");
            compFor.Left.AcceptWalker(this);
            writer.Write(" in ");
            compFor.Target.AcceptWalker(this);
            if(!compFor.Body.IsNull)
                writer.Write(" ...");
        }

        public void VisitComprehensionIfClause(ComprehensionIfClause compIf)
        {
            writer.Write("... if ");
            compIf.Condition.AcceptWalker(this);
            if(!compIf.Body.IsNull)
                writer.Write(" ...");
        }

        public void VisitConditionalExpression(ConditionalExpression condExpr)
        {
            condExpr.Condition.AcceptWalker(this);
            writer.Write(" ? ");
            condExpr.TrueExpression.AcceptWalker(this);
            writer.Write(" : ");
            condExpr.FalseExpression.AcceptWalker(this);
        }

        public void VisitFinallyClause(FinallyClause finallyClause)
        {
            writer.Write("finally");
            VisitBlock(finallyClause.Body);
        }

        public void VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue)
        {
            keyValue.KeyExpression.AcceptWalker(this);
            writer.Write(" : ");
            keyValue.ValueExpression.AcceptWalker(this);
        }

        public void VisitLiteralExpression(LiteralExpression literal)
        {
            var value_type = literal.Value.GetType();
            string enclosure = null;
            switch(value_type.Name.ToLower()){
            case "string":
                enclosure = "\"";
                break;

            case "char":
                enclosure = "'";
                break;

            default:
                enclosure = "";
                break;
            }

            writer.Write("{0}{1}{2}", enclosure, literal.Value, enclosure);
        }

        public void VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq)
        {
            intSeq.Start.AcceptWalker(this);
            writer.Write(intSeq.UpperInclusive ? "..." : "..");
            intSeq.End.AcceptWalker(this);
            writer.Write(":");
            intSeq.Step.AcceptWalker(this);
        }

        public void VisitIndexerExpression(IndexerExpression indexExpr)
        {
            indexExpr.Target.AcceptWalker(this);
            writer.Write("[");
            PrintList(indexExpr.Arguments);
            writer.Write("]");
        }

        public void VisitPathExpression(PathExpression pathExpr)
        {
            bool first = true;
            foreach(var item in pathExpr.Items){
                if(!first)
                    writer.Write("::");
                else
                    first = false;

                item.AcceptWalker(this);
            }

            if(pathExpr.TypeArguments.Any()){
                writer.Write("<");
                PrintList(pathExpr.TypeArguments);
                writer.Write(">");
            }
        }

        public void VisitParenthesizedExpression(ParenthesizedExpression parensExpr)
        {
            writer.Write("(");
            parensExpr.Expression.AcceptWalker(this);
            writer.Write(")");
        }

        public void VisitObjectCreationExpression(ObjectCreationExpression creation)
        {
            creation.TypePath.AcceptWalker(this);
            if(creation.TypeArguments.Any()){
                writer.Write("<");
                PrintList(creation.TypeArguments);
                writer.Write(">");
            }
            writer.Write("{");
            PrintLongList(creation.Items);
            writer.Write("}");
        }

        public void VisitMatchClause(MatchPatternClause matchClause)
        {
            PrintLongList(matchClause.Patterns, "...", 3);

            writer.Write(" => {...}");
        }

        public void VisitSelfReferenceExpression(SelfReferenceExpression selfRef)
        {
            writer.Write("self");
        }

        public void VisitSuperReferenceExpression(SuperReferenceExpression superRef)
        {
            writer.Write("super");
        }

        public void VisitNullReferenceExpression(NullReferenceExpression nullRef)
        {
            writer.Write("null");
        }

        public void VisitCommentNode(CommentNode comment)
        {
            writer.Write("//" + comment.Text);
        }

        public void VisitTextNode(TextNode textNode)
        {
            writer.Write("<?");
            writer.Write(textNode.Text);
            writer.Write("?>");
        }

        public void VisitTypeConstraint(TypeConstraint constraint)
        {
            writer.Write(constraint.TypeParameter.Name);
            if(constraint.TypeConstraints.Any()){
                writer.Write(" : ");
                PrintLongList(constraint.TypeConstraints);
            }
        }

        public void VisitPostModifiers(PostModifiers postModifiers)
        {
            writer.Write("requires ");

            if(postModifiers.RequiresToBeCalledBeforehand.Any()){
                PrintList(postModifiers.RequiresToBeCalledBeforehand);
                writer.Write(" beforehand");
            }
        }

        public void VisitSimpleType(SimpleType simpleType)
        {
            writer.Write(simpleType.Identifier);
            if(simpleType.TypeArguments.Count > 0){
                writer.Write("<");
                PrintList(simpleType.TypeArguments);
                writer.Write(">");
            }

            writer.Write(" (- ");
            simpleType.IdentifierNode.Type.AcceptWalker(this);
        }

        public void VisitPrimitiveType(PrimitiveType primitiveType)
        {
            writer.Write(primitiveType.KnownTypeCode);
        }

        public void VisitReferenceType(ReferenceType referenceType)
        {
            writer.Write("&");
            referenceType.BaseType.AcceptWalker(this);
        }

        public void VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptWalker(this);
            writer.Write("::");
            VisitSimpleType(memberType.ChildType);
        }

        public void VisitFunctionType(FunctionType funcType)
        {
            writer.Write("|");
            PrintList(funcType.Parameters);
            writer.Write("| -> ");
            funcType.ReturnType.AcceptWalker(this);
        }

        public void VisitParameterType(ParameterType paramType)
        {
            writer.Write("`");
            writer.Write(paramType.Name);
        }

        public void VisitPlaceholderType(PlaceholderType placeholderType)
        {
            writer.Write("<placeholder type>");
        }

        public void VisitKeyValueType(KeyValueType keyValueType)
        {
            VisitParameterType(keyValueType.KeyType);
            writer.Write(" = ");
            keyValueType.ValueType.AcceptWalker(this);
        }

        public void VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            writer.Write(parameterType.Name);
            writer.Write(" (- ");
            parameterType.Type.AcceptWalker(this);
        }

        public void VisitAttributeSection(AttributeSection section)
        {
            writer.Write("[");
            if(!section.AttributeTargetToken.IsNull){
                writer.Write(section.AttributeTarget);
                writer.Write(": ");
            }
                
            PrintList(section.Attributes);
            writer.Write("]");
        }

        public void VisitAliasDeclaration(AliasDeclaration aliasDecl)
        {
            writer.Write("alias ");
            aliasDecl.Path.AcceptWalker(this);
            writer.Write(" as ");
            writer.Write(aliasDecl.AliasName);
        }

        public void VisitImportDeclaration(ImportDeclaration importDecl)
        {
            writer.Write("import ");
            if(importDecl.ImportPaths.Count > 1)
                writer.Write('{');

            PrintList(importDecl.ImportPaths);

            if(importDecl.ImportPaths.Count > 1)
                writer.Write('}');

            if(!importDecl.TargetFile.IsNull){
                writer.Write(" from ");
                writer.Write(importDecl.TargetFilePath);
            }

            writer.Write(" as ");
            if(importDecl.AliasTokens.Count > 1)
                writer.Write('{');
            
            PrintList(importDecl.AliasTokens);
            if(importDecl.AliasTokens.Count > 1)
                writer.Write('}');
        }

        public void VisitFunctionDeclaration(FunctionDeclaration funcDecl)
        {
            if(!funcDecl.Attribute.IsNull)
                VisitAttributeSection(funcDecl.Attribute);

            if(!funcDecl.Modifiers.HasFlag(Modifiers.None)){
                writer.Write(funcDecl.Modifiers);
                writer.Write(" ");
            }
            writer.Write("def ");
            writer.Write(funcDecl.Name);
            if(funcDecl.TypeConstraints.Any()){
                writer.Write("<");
                PrintList(funcDecl.TypeConstraints);
                writer.Write(">");
            }
                
            writer.Write("(");
            PrintList(funcDecl.Parameters);
            writer.Write(") -> ");
            funcDecl.ReturnType.AcceptWalker(this);
            writer.Write("{...}");
        }

        public void VisitTypeDeclaration(TypeDeclaration typeDecl)
        {
            if(!typeDecl.Attribute.IsNull)
                VisitAttributeSection(typeDecl.Attribute);
            
            writer.Write("{0} ", typeDecl.TypeKind.ToString());
            writer.Write(typeDecl.Name);
            if(typeDecl.TypeConstraints.Any()){
                writer.Write("<");
                PrintList(typeDecl.TypeConstraints);
                writer.Write(">");
            }

            if(typeDecl.BaseTypes.Any()){
                writer.Write(" : ");
                PrintList(typeDecl.BaseTypes);
            }
            writer.Write("{...}");
        }

        public void VisitFieldDeclaration(FieldDeclaration fieldDecl)
        {
            if(!fieldDecl.Attribute.IsNull)
                VisitAttributeSection(fieldDecl.Attribute);
            
            if(fieldDecl.HasModifier(Modifiers.Public))
                writer.Write("public ");
            else if(fieldDecl.HasModifier(Modifiers.Protected))
                writer.Write("protected ");
            else if(fieldDecl.HasModifier(Modifiers.Private))
                writer.Write("private ");
            
            if(fieldDecl.HasModifier(Modifiers.Immutable))
                writer.Write("let ");
            else
                writer.Write("var ");

            PrintLongList(fieldDecl.Initializers, "...", 2);
        }

        public void VisitParameterDeclaration(ParameterDeclaration parameterDecl)
        {
            if(!parameterDecl.Attribute.IsNull)
                VisitAttributeSection(parameterDecl.Attribute);
            
            VisitIdentifier(parameterDecl.NameToken);
            if(parameterDecl.IsVariadic)
                writer.Write("...");
            
            if(!parameterDecl.Option.IsNull){
                writer.Write(" = ");
                parameterDecl.Option.AcceptWalker(this);
            }
        }

        public void VisitVariableInitializer(VariableInitializer initializer)
        {
            initializer.Pattern.AcceptWalker(this);
            writer.Write(" = ");
            initializer.Initializer.AcceptWalker(this);
        }

        public void VisitWildcardPattern(WildcardPattern wildcardPattern)
        {
            writer.Write("_");
        }

        public void VisitIdentifierPattern(IdentifierPattern identifierPattern)
        {
            identifierPattern.Identifier.AcceptWalker(this);
            if(!identifierPattern.InnerPattern.IsNull){
                writer.Write(" @ ");
                identifierPattern.InnerPattern.AcceptWalker(this);
            }
        }

        public void VisitCollectionPattern(CollectionPattern collectionPattern)
        {
            var type = collectionPattern.CollectionType;
            if(type.Identifier == "dictionary"){
                writer.Write("{");
                PrintLongList(collectionPattern.Items);
                writer.Write("}");
            }else if(type.Identifier == "vector" || type.Identifier == "array"){
                writer.Write("[");
                PrintLongList(collectionPattern.Items);
                writer.Write("]");
            }
        }

        public void VisitDestructuringPattern(DestructuringPattern destructuringPattern)
        {
            destructuringPattern.TypePath.AcceptWalker(this);
            writer.Write("{");
            PrintLongList(destructuringPattern.Items);
            writer.Write("}");
        }

        public void VisitTuplePattern(TuplePattern tuplePattern)
        {
            writer.Write("(");
            PrintLongList(tuplePattern.Patterns);
            writer.Write(")");
        }

        public void VisitExpressionPattern(ExpressionPattern exprPattern)
        {
            exprPattern.Expression.AcceptWalker(this);
        }

        public void VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
        {
            writer.Write("..");
        }

        public void VisitKeyValuePattern(KeyValuePattern keyValuePattern)
        {
            VisitIdentifier(keyValuePattern.KeyIdentifier);
            writer.Write(": ");
            keyValuePattern.Value.AcceptWalker(this);
        }

        public void VisitPatternWithType(PatternWithType pattern)
        {
            pattern.Pattern.AcceptWalker(this);
            writer.Write(" (- ");
            pattern.Type.AcceptWalker(this);
        }

        public void VisitTypePathPattern(TypePathPattern pathPattern)
        {
            pathPattern.TypePath.AcceptWalker(this);
        }

        public void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
        {
            if(valueBindingPattern.Modifiers.HasFlag(Modifiers.Immutable))
                writer.Write("let ");
            else
                writer.Write("var ");

            VisitVariableInitializer(valueBindingPattern.Initializer);
        }

        public void VisitNewLine(NewLineNode newlineNode)
        {
            writer.Write("\n");
        }

        public void VisitWhitespace(WhitespaceNode whitespaceNode)
        {
            writer.Write(whitespaceNode.WhitespaceText);
        }

        public void VisitExpressoTokenNode(ExpressoTokenNode tokenNode)
        {
            writer.Write(tokenNode.Token);
        }

        #endregion
    }
}

