﻿

namespace Expresso
{
    /// <summary>
    /// Contains constants about exsc.
    /// </summary>
    public static class ExpressoConstants
    {
        public static string VersionString = "1.1.12";
    }
}
