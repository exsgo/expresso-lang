﻿using System;
using Expresso.Ast;
using System.Reflection;
using Expresso.CodeGen;
using Expresso.Ast.Analysis;

namespace Expresso
{
    /// <summary>
    /// Provides means of interacting with Expresso's compiler services.
    /// </summary>
    public static class ExpressoCompilerService
    {
        /// <summary>
        /// Parses the file as Expresso source code and returns the abstract syntax tree.
        /// </summary>
        /// <param name="filePath">File path.</param>
        public static ExpressoAst Parse(string filePath, bool doPostParseProcessing)
        {
            var parser = new Parser(new Scanner(filePath)){
                DoPostParseProcessing = true
            };
            parser.Parse();

            return parser.TopmostAst;
        }

        /// <summary>
        /// Compiles and assemble into an assembly object.
        /// </summary>
        /// <returns>An assembly object.</returns>
        /// <param name="filePath">Path to the file which will be compiled.</param>
        /// <param name="options">Compiler options to use for the compilation.</param>
        public static Assembly CompileToAssembly(string filePath, ExpressoCompilerOptions options)
        {
            var parser = new Parser(new Scanner(filePath)){
                DoPostParseProcessing = true
            };
            parser.Parse();

            var ast = parser.TopmostAst;
            try{
                var generator = new CodeGenerator(parser, options);
                generator.VisitAst(ast, null);
                return generator.AssemblyBuilder;
            }
            catch(ParserException e){
                parser.errors.SemErr(e.ToString());
                ExpressoCompilerHelpers.DisplayHelp(e);
                Utilities.CompilerOutput.WriteLine(e.StackTrace);
                throw e;
            }
        }
    }
}

