﻿## Use Guide

Let me introduce another unique feature for Expresso. Let's think about methods. Methods and functions have parameters, the return type and several modifiers can be
attached to them. Futhermore, they can have generic parameters and use them inside. Additionally, the generic parameters can have constraints on how to use them.
But generally, in most programming languages, there are no means to control when to call the methods. In Expresso, we provide it and it's called *Use guide*.

### What's It Like?

In method signatures, Use guide declarations start with the keyword `requires` between the optional return type and generic parameter constraints. In class and enum
method signatures, you can declare that the method should be called before another method, and it's declared as `a() requires b beforehand` when we have `a` and `b`
methods. In this context, *before* means that you should call `b` before `a` in the same method on source code, which could then be interpreted as having a direct or
indirect call to `b` in the same method that a call to `a` resides in.

Meanwhile, in interface method signatures, you can declare that the method should have a call to another method inside, and it's declared as `c() requires d in` when
we have `c` and `d` methods.

### Use Cases

Because it would be completely new to most programmers, here we will list some use cases.

#### Use Case 1: For Initialization

What I will come up with first when saying about the use case of Use guide, is for initialization. Consider situations like we have `initialize` and `a` methods.
Then all we have to do is to add a Use guide declaration on the `a` method. After that, Expresso will verify that the `initialize` method was directly or indirectly
called in the same method before we call the `a` method and we'll be happy ;-)

```expresso
class SomeClass
{
    public def initialize() -> void
    {
        // body omitted
    }
    
    public def a() -> void
    requires initialize beforehand
    {
        // body omitted
    }
}
```

<span class="caption">Listing 17-1: Use guide fo classes and enums</span>

The preferred style of Use guides for classes and enums is to have them on their own lines. Note the position. You must declare a Use guide after the return type and
before the where clause.

If you had `initialize`, `preInitialize` and `postInitialize` methods, the method signature would look like the following:

```
public def a() -> void
requires initialize beforehand, preInitialize beforehand, postInitialize beforehand
```

#### Use Case 2: For Calling Implementation Methods

The next use case that I've come up with is for calling implementation methods. This time, we will use the interface Use guide and consider having `a` and `aImpl`
methods. And then all we have to do is to add a Use guide declaration on the `a` method again. After that, in classes that implement the interface, Expresso will
verify that the `aImpl` method is called inside the `a` method AND we'll be happy once again.

Note that in this use case, Expresso should be able to confirm all cases because the implementation needed to do that is so straightforward.

```expresso
interface SomeInterface
{
    def a() -> int requires aImpl in;
    def aImpl() -> int;
}
```

<span class="caption">Listing 17-2: Use guide for interfaces</span>

This time around, the preferred style is to have them on the same line as the method signature.

#### Use Case 3: An Indirect Call to the Required Method

Because it's too complicated to indirectly call a required method, here I will show an example of it.

```expresso
class PostModifierTest
{
    public def a() -> void
    {
        println("called a");
    }

    public def b() -> void
    {
        println("called b");
        self.a();
    }

    public def c() -> void 
    requires a beforehand
    {
        println("called c");
    }

    public def d() -> void
    {
        println("called d");
        self.b();
    }
}

def main()
{
    let inst = PostModifierTest{};
    inst.d();
    inst.c();
}
```

<span class="caption">Listing 17-3: An indirect call to a required method</span>

In Listing 17-3, the method 'c' requires the method 'a' to be called beforehand and that is achieved via a call to the method 'd', which is defined after the method
'c'. It now can verify that this code is also valid.

Note that the following code would throw a compiler error.

```expresso
class SomeClass
{
    public def a() -> void
    {
        println("a called");
        self.c();
    }

    public def b() -> void
    {
        println("b called");
    }

    public def c() -> void
    requires b beforehand
    {
        println("c called");
    }

    public def d() -> void
    {
        println("d called");
        self.b();
    }
}

def main()
{
    let inst = SomeClass{};
    inst.d();
    inst.a();
}
```

<span class="caption">Listing 17-4: An indirect calls to methods 'a' and 'c'</span>

In Listing 17-4, both calls to the method 'a' and method 'c' are indirect calls and therefore it won't be proved to be true.

### Summary

Well done, man or lady! We've reached the end of the tutorial. Now you should have the basic knowledges on Expresso and programming itself. I wouldn't say that you
can tackle with any challenges that you will encounter on your Expresso journey only with the knowledges you've gained in this tutorial, but you should now have the
fundamental knowledges to tackle with those kinds of problems. From now on, it's up to you whether you are going to decide what kind of program you will write. I just
hope this tutorial will be any help with doing that.