﻿## References(the `out` and `ref` Modifier in Expresso)

Have you ever heard of reference types? On CLR, all types are classified into either reference types or value types. In short, reference types hold references in variables.
This means that you can't easily copy them, while with value types you can easily copy them by just assigning them to new variables. That said, let's consider
the following situation.

```expresso
def changeInt(n ( - int)
{
    n = 10;
}

def main()
{
    var x = 5;
    changeInt(x);
    println("${x}");
}
```

<span class="caption">Listing 15-1: Trying to change the value of an `int`</span>

How do you make of it? What would the `println` statement output? 10? No. It's actually 5 even though it looks like the function changes the value of `x`. As a human,
you would have expected that behavior, but it doesn't behave like that. To get that behavior, you would need to change the declaration of `changeInt` as follows.

```expresso
def changeInt(n (- &int)
```

<span class="caption">Listing 15-2: The declaration of `changeInt` that actually changes the value of its parameter</span>

This states that the type of `n` is a reference to an `int`. Taking a reference means that if you change the value of the reference, that change will be visible outside
the function. But that's not enough. To change the value of `x`, you also would need to change the call to `changeInt` to the following.

```expresso
changeInt(&x);
```

<span class="caption">Listing 15-3: Use of the reference operator</span>

This call passes `x` as a reference to it. To pass a reference to a function, you would always need to add the reference operator before it. This is because you should
know that the value of `x` will be changed by `changeInt`. In other words, this call is needed to indicate that you opt to change its value.

In a sense, references in Expresso are like the `out` modifier in C# and in fact the implementation uses the `out` modifier on CLR. Although the reference operator is
inspired by the one in C++, it's meaningless to use it on classes. More specifically, the reference operator should be used on value types, which are primitive types and 
`struct`s imported from other .NET languages because there is no way in Expresso to create `struct`s.

In C# 7.0 and onward, you can use the `ref` modifier on variables while in Expresso it's meaningless to add the reference operator on a variable. It will simply result
in compiler error. References as normal variables are important in that they don't create new duplicate values when they are assigned. So it might be supported
in Expresso as well.

> ### Advanced Topic: User-Defined Operators
>
> In C#, you can define user-defined operators on classes and structs. In Expresso, you can't neither define nor use them as of now. So for example, you have to write
> `str.Equals("string")` when you compare strings in Expresso while in C#, you can just write `str == "string"`. I know it's inconvinient but the functionality isn't
> in the roadmap, so maybe we forever won't support it.
