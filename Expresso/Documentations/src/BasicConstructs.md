﻿## Basic Constructs

### The `while` Loop

```expresso
var result (- int = 1;
var i (- int = 0;
while i < 10 {
    result *= 2;
    i += 1;
}
println("${result}");
```

<span class="caption">Listing 3-1: A use of the `while` loop</span>

Look at the above code. You may be familliar with it. This is the `while` loop in Expresso and it is the same as that of other programming languages.
What the above code does is to first declare two variables and use one of them as a counter and the other for calculating the result. Then it prints the result
to the console. We use the string interpolation where we print it, but for now, don't worry about it. Just assume that it's a handy way to print some variables.
When you run this code, it should print 1024.
Let's look at another example.

```expresso
let ary (- int[] = [1, 2, 3, 4, 5];
var i (- int = 0;
while i < ary.Length {
    println("${ary[i]}");
    i += 1;
}
```

<span class="caption">Listing 3-2: Another use of the `while` loop</span>

This time, we index into an array. In Listing 3-2, we have an array to print and then actually print the elements of it.
Although `ary.Length` returns the length of the array and it will work even if the length of it changes, the intent isn't clear, is it?
For Listing 3-2 also, it would be enough for the purpose, but the `i += 1;` part is slightly annoying. Would it be cleaner if Expresso counts up for us?

### The `for` Loop

```expresso
var result (- int = 1;
for let i in 0..10 {
    result *= 2;
}
println("${result}");
```

<span class="caption">Listing 3-3: A use of the `for` loop</span>

So we'll introduce the `for` loop. The `for` loop, in contrast to the `while` loop, is a consturct that is used to loop over some sequence or count up or down 
with `intseq`s. Here, we count up from 0 to 10 while calculating the power of two raised to 10.

```expresso
let ary (- int[] = [1, 2, 3, 4, 5];
for let elem in ary {
    println("${elem}");
}
```

<span class="caption">Listing 3-4: Another use of the `for` loop</span>

Look at this variation of the previous example. Now the intent is clearer and the code is more concise. Therefore it's the most commonly used loop construct in Expresso.

> #### `dictionary` in a `for` Loop
>
> Look at the following code.
>
> ```expresso
> let dict = {"a" : 10, "b" : 20};
> for let (key, value) in dict {
>     println("${key} : ${value}");
> }
> ``` 
>
> <span class="caption">Listing 3-5: Dictionary in a `for` loop</span>
>
> As you can see, you can iterate through a `dictionary`. This is accomplished by returning a tuple at an iteration behind the scenes. A `for` loop requires
> its right operand to be an `IEnumerable` so the `dictionary` returns an `IEnumerable<(string, int)>`. In other words, you can iterate through any objects as long as
> they implement `IEnumerable<>`, which <del>may currently not possible using only Expresso but you can with other .NET languages like C#</del> can also be achieved
> using only Expresso, and return a tuple at an iteration.
>
> Note the `let` keyword in the `for` statement. Even though I found that both `for (key, value) in ...` and `for let (key, value) in ...` syntaxes pass compiling
> and can be run and I leave them as its specifications, we usually prefer and I recommend you to use the latter syntax because it's explicit about making variables.
> If you have some Rust background, you may feel more fluent without the `let` keyword than with one, but hey, we are speaking Expresso so you should get adapted to
> it.

### The `if` Statement

Imagine that we want to do different things depending on the value of some variable. That's the time when we'll look at the `if` statement.

```expresso
let flag (- bool = true;
if flag {
    println("The condition is true");
}else{
    println("The condition is false");
}
```

<span class="caption">Listing 3-6: A use of the `if` statement</span>

The above code prints `The condition is true`. The `if` statement is also common in programming languages, so you may be familliar with it. But note that
we mentioned in the "The `bool` Type" section that the condition of an `if` statement must be the `bool` type unlike C. Otherwise, the compiler will issue an error.

There are also the `do while` statement and the `match` statement that uses patterns for branching. The functionality of the `do while` statement is almost the same as
that of the `while` statement so we'll ignore it and we'll dig into the `match` later on.

We've seen the data types and the basic constructs so far, now it's good time to talk about the `//` syntax.

### Comments

In Listing 2-7 to 2-9, we've written the comments as side notes. The sections after `//` and inside `/*` and `*/` are called comments and they will be ignored
by the compiler. They are purely for us. Although they are completely meaningless to the compiler, it will take them into account when calculating token offsets.
Thank goodness.

You can write anything inside comments. People say that you will become someone else if two weeks passed since you have written some code. So it's good to leave notes
about what you've done, especially when you chose the implementation due to some technical difficulties, or for other reasons.

```expresso
// Are you fu*king idiot? What the heck is this code?!
let two (- int = 1 + 1;
```

<span class="caption">Listing 3-7: An offensive comment</span>

But don't try to write comments like above. Be kind to others, and of course, to yourself ;)

> ### The Philosophies
>
> In Expresso, there are 2 philosophies, *What the compiler doesn't allow to do is what just you can't* and *Just do as the compiler says*. The former should be
> self-explanatory because otherwise it has some bugs.
> The latter is also said hilariously as *Just lean and bend on the compiler until a perpendicular line becomes a pararell one*, meaning that if you just follow
> what the compiler says, then you can do right things naturally.
