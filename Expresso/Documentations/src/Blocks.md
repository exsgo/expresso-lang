﻿## Understanding Blocks

In the previous section, we've seen the functionalities of the `while`, `for` and `if` statements. By the way, each of the statements defines new blocks. Blocks are
a general programming concept in which a series of operations resides. The important role of the blocks is that they have scopes in which variables can only be defined
and accessed. In other words, you define scopes to separate variables.

```expresso
def main()
{
    let x (- int = 1;
    {
        let y (- int = 2;
        println("${x}");
        println("${y}");
    }

    println("${x}")
    println("${y}");    // This results in a compile-time error
}
```

<span class="caption">Listing 3-8: Invalid access to an inner variable</span>

In Listing 3-8, we've defined 2 variables named `x` and `y`. We defined `x` in the outer block, and `y` in the inner block, and tried to use `x` and `y` in each block.
Actually, the access to `y` in the outer block will fail, but do you know why? This is because `y` is no longer defined at where we tried to use it in the outer block.
That's why we separate variable scopes.

In short, we separate scopes to limit unneccessary variable accesses, which might cause bugs. It is said that scopes should be as small as possible. Otherwise, it will
be more likely for you to introduce bugs.
