﻿## Modifiers on Fields and Methods

Above all, look at this code again.

```expresso
class Circle
{
    // fields omitted

    public def touchesCirlce(other (- Circle)
    {
        return (other.x - self.x) ** 2 + (other.y - self.y) ** 2 <= (other.radius + self.radius) ** 2;
    }
}
```

<span class="caption">Listing 4-3: A method that determines whether the circle touches another circle</span>

This code shows the definition of the `touchesCircle` method. Have you noticed the `public` modifier? Like other object-oriented programming languages, Expresso has 
several modifiers on fields and methods. `public` modifier makes the item public, in other words, you can access from anywhere to the item with the `public` modifier.
`protected` modifier allows the current and derived types to access to the item. `private` items can only be accessed from within that type only.

Modifiers relating to virtuality include `virtual`, `abstract` and `override`. `virtual` modifier makes the method virtual, meaning that derived classes can override
the method implementation. `abstract` modifier makes the method empty and forces derived classes to implement the method. Finally, `override` declares that the method
will be overridden. In Expresso, methods without the `virtual` modifier will automatically be final. So if there is any chance that you will override the method,
you have to declare it as `virtual`.

Related to virtuality, Expresso also has class modifiers: `abstract` and `sealed`. `abstract` makes the whole class empty, meaning that the class can't be instantiated
(fields and methods with implementations can be declared unlike the `interface`) and `sealed` makes the whole class final, meaning that the class won't be derived anymore.

In Expresso, there is one last modifier that you can attach to methods: `mutating`. It's used, combined with `var` declarations. That is, `mutating` modifier declares that
the method will mutate the `self` variable, thus needs the `var` modifier. Expresso will verify that mutating methods actually mutate their states and that non-mutating
methods don't, issue a warning when mutating methods don't actually mutate their states and issue an error when non-mutating methods accidentally mutate their states.

Also in Expresso, there are post modifiers that can be attached to methods after the method signatures. One of them is called *Use guide* and we'll dig into detail
in a later chapter, here we'll look at the `friends` modifier.

The `friends` modifier declares a friend method and a friend method can only be accessed from the methods that are stated in the declaration. Consider the following
code:

```expresso
class SomeClass
{
    def a() -> void
    friends with b
    {
        ;
    }

    public def b() -> void
    {
        self.a();
    }

    public def c() -> void
    {
        ;
    }
}
```

In this code, the method 'a' can only be accessed from the method 'b' despite of no access modifiers being defined.

> #### Static Methods
>
> If you are familiar with other programming languages, you may have heard of *static methods*. Static methods can't be inherited and technically, they are associated
> with the type rather than an instance.
> As other programming languages, static methods can be defined by adding the `static` modifier before the `def` keyword.
> But currently, you can't define static fields.
>
> In Expresso, calling a static method would be performed with `Type.staticMethod()` rather than `Type::staticMethod()` despite of its resemblance to C++.
