﻿## Hello, Expresso World!

OK, enough with pre-words. By far, we are all armed enough with concepts, goals and something like that, so let's get started.
First of all, we start from the traditional *hello world* program.
You may expect it to be a one-liner program considering the fact that I've mentioned Expresso looks rather like a scripting language,
but unfortunately it's two liners like the following:

```expresso
module main;
def main()
{
    println("Hello, world!");
}
```

<span class="caption">Listing 1-1: A traditional *hello, world!* program in Expresso</span>

This reads: let's write an Expresso program where we're defining a module named __main__ and a function also called __main__.
The program will start executing from the __main__ function, and inside which we're outputing...

Every Expresso program consists of at least one module and every module must be explicitly named.
Here, we name the module `main` because we usually call the top-level module that is the entry point for a program `main`. As
you progress reading the tutorial, you will see different names used for the module names.
In Expresso the naming convention for modules is the snake case. In other words, you name a module `test_module` rather than
`testModule` or `TestModule`. This is not a strict rule that the compiler enforces but it's considered a good style to keep that rule.
In `main` function, which is the entry point for the program, we `println`'ed a string. `println` is a function that outputs
some string to the console. There is also the single line version, `print`, that doesn't output a line break at the end.
In addition, although we'll come back later to this topic, Expresso has string interpolation as well.

> ### About the `exsc` Command
>
> Above all, I have to say it's immature because the order in which arguments are specified is fixed. It will be improved someday.

## Getting into the Expresso World

Let's dive into the Expresso world. Because I mentioned at the beginning of this guide that Expresso looks a type-free language rather than a type-strict one,
we'll use Expresso as a simple calculator first. 
Note that from now on, we'll not be including the `module main; def main(){` part as long as we don't need to show the complete source code because with it,
the examples will be too complex. Since it's boring to repeat writing the same thing over and over and it's considered hard to see if we include
the magic header every time, we won't do that.
So if you follow this tutorial, don't forget to include the magic header at the beginning of source files.
As the saying goes *A journey of a thousand miles begins with a single step*, we'll be starting with a simple calculation.

```expresso
println("${1 + 1}");
```

This will print `2` as you will be answering. Note that we are using a feature called *string interpolation*, to which we'll come back later and explain the details.

Next, a bit more complex.

```expresso
println("${4 * 5}");
```

This will print `20` as you may expect.
A piece of cake? Indeed. Then add some complexity.

```expresso
println("${4 * 5 + 1}");
```

<span class="caption">Listing 1-2: An expression with multiplication and addition</span>

This will print `21`.
It's still a piece of cake. As you might see, the evaluation will respect the mathematical operator precedences. That is, multiplication first and then addition.
Likewise, it will follow the same rules for other operators such as exponentiation.

> ### About exponentiation
> In Expresso, you can calculate the exponentiation with `**`. The result of executing `let a = 2 ** 10` will be 1024.

OK, then we'll be doing the above calculation in a slightly different way: by using a variable.

```expresso
let a (- int = 4 * 5;
println("${a + 1}");
```

<span class="caption">Listing 1-3: Expressing Listing 1-2 in another way</span>

### Let Bindings and Variable Declarations

In Expresso, there are 2 forms of variable bindings. 
Variables are useful considering the ability to keep track of values they hold. But sometimes we just want to give values descriptive names
because we need the same values several times or because it is tedious to change all the literal values over and over when you try
to guess the proper values for some programs. That's where immutable variables come into play.
As a general term, an immutable variable is a constant value, meaning that values that are bound to variables will never be changed during program execution.
Let bindings introduce tags, and tags are names which you use in order to refer to the values later on and immutable variables are considered to be the tags.
By contrast, variable declarations introduce boxes that have certain shapes, and those boxes can be filled in with anything at any time as long as
the shapes match.

In Listing 1-3, we introduced a new let binding stating `let a (- int = 4 * 5;`. This reads: we'll introduce a new let binding named `a` whose type is `int` and
the value of it will be `4 * 5`. Note that we explicitly annotate the type here. Because Expresso has a strong type inference system, you wouldn't usually need
to do so. But we'll generally do that here in the tutorial for clarity.

Note that we use `(-` for separating the variable name and the type. It represents `∈` in ASCII-compatible characters and means exactly the mathematical `∈`.
In other words, the right hand side includes the left hand side as an element in a mathematical sense.

By introducing let bindings, we can keep the results of some operations aside. And then we can perform other operations based on those values. I would suggest 
you to prefer let bindings over variable declarations because let bindings tend to make the code clearer and more concise by making the code easier to read and
to follow the logic.

OK, enough with simple calculations. Next, let's look at the data types that variables are in.

Like other programming languages, Expresso has some types built into it. Namely the `int`, `uint` types for signed and unsigned integers, the `bool` type for
the booleans, the `float` and `double` types for single floating-point numbers and double floating-point numbers and the `char` for characters, etc.
Let's explore each type to see what it does.

> #### --- Give Things Descriptive Names ---
>
> For coders, source code is another yourself. Once any part of source code is available to the public, you will be valued at it.
> Having that said, names are important for humans in the real world and in the computer world. Humans make errors. But unfortunately
> the computers are smarter than us. They don't care if the names are long or short. But we do care. The more discriptive names we give things,
> the less likely we are to make errors. Names describe their properties. So give things descriptive names as much as possible.
> Names in source code are much like letters you write on paper. The more descriptive they get, the more beautifuly you write letters.
> Lazy and old-fashioned C programmers would prefer shorter names and abbreviations. Think of source codes as publications of coders for coders, and by coders.
> They will be read by other coders and when that happens what do they think if the publications look ugly? It deteriorates not only readability but also their
> interests in your source codes, which makes it harder to be maintained and therefore they will be replaced with other ones at some point in the future. Then
> your self is disappeared from that world. That's a sad story. Surely. Certainly. And I don't want to read those sad stories anymore.
> 
> Another reason why you should use longer names is because names are fighters. Imagine if you wrote a very large program of, say, 10,000 LOC.
> Several weeks had passed and you were told that your program had some bugs in it and you should read it all back again and
> it would cost you precious time. And too simple and obscure names make it difficult for you to understand the flow of the program. I know programmers are lazy.
> Too lazy to get up at early in the morning. But don't be lazy at giving longer, descriptive and hopefully distinct names. They will certainly revenge on you,
> I can tell.

> #### Naming Rules
>
> As mentioned above, names are important. But you may be wondering what kind of names are good or considered acceptable. Here's my rules.
>
> First of all, fields are rather like nouns, and functions and methods verbs. So name them in that way. That is, naming fields only with nouns or phrases
> containing nouns, and naming functions and methods using verbs because fields represent properties of types, and methods and functions represent actions. Take the
> exsdoc's Lexer class as an example.
>
> ```expresso
> class Lexer
> {
>     private var reader (- StreamReader;
>     private var cur_token (- Option<Token>;
>     private var pos (- Int64;
>     // -- snip --
>     private var is_start_of_source_code (- bool;
>
>     private def nextChar()
>     {
>         // -- snip --
>     }
>     // -- snip --
>     public def peek() -> Token
>     {
>         // -- snip --
>     }
>
>     public def scan() -> Token
>     {
>         // -- snip --
>     }
>
>     public def readModuleName() -> string
>     {
>         // -- snip --
>     }
> }
> ```
>
> <span class="caption">Listing 1-4: Code we extract from the Lexer class's definition</span>
>
> This is code we extract from the exsdoc's Lexer class definition. As you can see, almost all the fields and methods follow the aforementioned rules. As is easily
> noticed, there are 2 exceptions in this Listing.
>
> The first is the boolean field: `is_start_of_source_code`. On this field, the name starts with a verb. This is acceptable because its purpose is to
> indicate whether or not we are at the beginning of a source code part.
>
> The second is the first method: `nextChar`. This is also acceptable because it is private. Private methods can't be accessed from outside the class, so not anyone
> else would be using it and therefore it would be thought as no problem.
