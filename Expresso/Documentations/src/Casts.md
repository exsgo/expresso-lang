﻿## Classes and Casts

Sometimes, in object-oriented programming, you want to treat instances as another class that the instance is derived from or convert `double`s or `float`s to `int`s or
`uint`s or vice versa. That's what castings introduce. In this chapter, we'll look at those examples.

### Primitive Casts

As described in the *Data Types* section, there are 7 number primitive types in Expresso. Unfortunately, there are currently no convertible casts from and to `bigint`s.
If you want to create a `bigint` from other types, you need to call its appropriate constructor like `bigint{value: variable}`. For other types, see the following table.
Because there is no `byte` literal, it omits the `byte` type in the *from Types* column.

<span class="caption">Table 4-11: Whether each type can be casted to another type</span>

| from Types | to `byte` | to `int` | to `uint` | to `float` | to `double` |
|------------|-----------|----------|-----------|------------|-------------|
|  `int`     |    able   |     -    |    able   |    able    |     able    |
|  `uint`    |    able   |   able   |     -     |    able    |     able    |
|  `float`   |    able   |   able   |    able   |     -      |     able    |
|  `double`  |    able   |   able   |    able   |    able    |      -      |

As you can see, all combinations are possible, which means that you can expect that all combinations are convertible except for `bigint`s.

### Upcasts

Let's get back to shapes. Both `Circle` and `Rectangle` classes implement the `HasArea` interface. Remember that in Listing 4-7 and 4-8, we use `as HasArea` syntax?
That's a cast we're talking about.

Unlike C#, we use the `as` operator to cast something. It's not like the `as` operator in C#, though. That is, it throws an exception when casting failed. There is no
alternative to the C#'s `as` operator. So if you need to cast something and the casting could fail, you couldn't avoid an expcetion being thrown. That's clearly
a disadvantage, and so it may be changed in the future.

### Downcasts

The cast we've seen is called *upcast*, meaning that it's a cast that tracks up the class hierarchy. There is an opposite cast called *downcast*, which tracks down the
class hierarchy. It's always safe to use upcasts, but that is not the case with downcasts. You could do wrong things with downcasts, so be careful.

```expresso
let circle = has_area as Circle;
```

<span class="caption">Listing 4-12: Using a downcast</span>

The syntax is the same as upcasts. Just the class name has been changed, using the `as` operator.

Downcasts would not always be successful. When failed, it, as other casts would do, throws an exception.

Generally speaking, you usually wouldn't need downcasts. So if you actually needed downcasts, maybe you should first check whether you would have written your program
properly.
