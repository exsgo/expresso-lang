﻿# The Expresso Tutorial

## Preface

Expresso primarily aims to be a programming language intended for use in education, or in other words, another Pascal. And therefore Expresso
has two concepts. Expresso aims to be an *easy-to-write* and *expressive* programming language.
The language name __Expresso__ is coined by combining __Espresso__ with __expressive__, which means that it's easy for programmers to write codes in it
as you can say *I can write a code while I'm having a cup of __Espresso__* and that it's highly __expressive__.
Even though we heavily put focus on syntax and the grammar, its policy reads *Easy for beginners, elegant for enthusisasts*,
meaning that it's easy for beginners to read and write, and still it has lots of rich features that advanced programmers can take advantage of.
Even though it's a type-strict language, it looks rather in type-free way thanks to the very powerful type inference system.

## Introduction

Welcome to *The Expresso Tutorial*, a tutorial about Expresso hopefully for those who program for the first time.

### Who Expresso Is For

As I mentioned above, Expresso aims to be an educational programming language so I hope it would be for students and professors.

I was experiencing Pascal in class at the university, which is said to be the educational programming language, and for which Expresso aims. Honestly, it was completely
different, I thought, and far away from the real programming, at least as of 2018. It's just too different from C, which in turn means that it's just for education,
not for real uses. That led me to the thought that I would develop a new programming language intended for use in education.

### Who This Tutorial Is For

Because this tutorial explains the features of Expresso and briefly introduces the general concepts of programming, beginners of programming would also have fun time
reading this tutorial. This tutorial assumes that you will read it front to back. So there are some chapters that reference previous chapters.

This tutorial continues to be updated as a new version comes out or as needed. 

In this tutorial, we'll talk about concepts and features of the Expresso language. We won't be showing and creating any example projects. Please, keep that in mind.

In the following chapters, this tutorial assumes that you have basic familiarity with the command line and you can use the `exsc` command to compile your Expresso
programs. If you don't, then you can't even run any Expresso programs.

Just for beginners, compiling is a general programming concept where the compiler reads your source code and translate it into something that computers can interpret.
So if you have source code and you want to run the program, you have to first compile the source code and generate an executable file.
