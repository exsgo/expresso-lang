﻿## Generic Types

So far, we've learned that functions abstract over a sequence of operations, and that interfaces abstract over behavior. What if we want to abstract away types?
For that situation, other languages have generics and Expresso is no exception.

### What Is Generics?

Simply speaking, generics create abstractions over types. Consider the following situation.

```expresso
class Stack
{
    private var items (- vector<?>;

    // methods omitted
}
```

<span class="caption">Listing 13-1: A hypothetical `Stack` class</span>

We want a `Stack` class, which piles up values of any type. What type should we declare the element of the vector as? We don't know yet how it is represented.

```expresso
class Stack<T>
{
    private var items (- vector<T>;

    // methods omitted
}
```

<span class="caption">Listing 13-2: Introducing generics</span>

Let's say we have a `T` type which can substitute for any type. Then we can implement the features without problems. This `T` is a generic type.

We mentioned in Chapter 2 that the syntax `vector<T>` is called generics. Now you should know why we write the type of vectors as such. In short, `vector<T>` 
abstracts over the element type and the element type can substitute for any type.

```expresso
def main()
{
    var stack = Stack{items: [1, 2, 3, ...]}; // you can write this as `Stack<int>{items: [1, 2, 3, ...]};`
    stack.push(4);
    stack.push(5);
    
    let top = stack.pop();
    println("${top}");  //prints 5
    
    var stack2 = Stack{items: [1.0, 2.0, 3.0, ...]};
    stack.push(4.0);
    stack.push(5.0);
    
    let top2 = stack2.pop();
    println("${top2}"); //prints 5.0
}
```

<span class="caption">Listing 13-3: Using the `Stack<T>` class</span>

To instantiate the class, you would create an object of the class as usual. As for type generics, you can omit type arguments when instantiating them because Expresso
automatically infers the type arugments from the object creation expression.

This way, you can abstract away types and apply the same operations on different types. Then you wouldn't need to repeat writing the same code more than once.

### Type Constraints

Once declared, you can see it as any type. But there's restriction as to how you use it. For example, consider the following situation.

```expresso
class GenericAdder<T>
{
    private let lhs (- T, rhs (- T;

    public def calculate() -> T
    {
        return self.lhs + self.rhs;
    }
}
```

<span class="caption">Listing 13-4: Using the `+` operator on the type `T`</span>

Here defines a `GenericAdder` class which has two fields, adds them, and returns the result. Because `T` can be any type, we don't know whether we can add values of
the type `T`. That's a problem. How can we know that we can add values of the type `T`?

```expresso
interface Addable<T>
{
    def add(rhs (- T) -> T;
}

class IntAdder : Addable<int>
{
    private let lhs (- int;
    
    public def add(rhs (- int) -> int
    {
        return self.lhs + rhs;
    }
}

class DoubleAdder : Addable<double>
{
    private let lhs (- double;
    
    public def add(rhs (- double) -> double
    {
        return self.lhs + rhs;
    }
}

class GenericAdder<T, U>
where T: Addable<U>
{
    private let lhs (- T, rhs (- U;
    
    public def calculate() -> U
    {
        return self.lhs.add(self.rhs);
    }
}
```

<span class="caption">Listing 13-5: The `GenericAdder` class</span>

Here, we define the `Addable` interface which has one type parameter and describes the add operation. To use this interface, we also define `IntAdder` and
`DoubleAdder` classes. These classes will actually do the add operation on `int` or `double` types, respectively.

As you can see from this example, we constraint on the type parameters with the `where` clause and the type constraints describe what operations you can perform
on the type parameters.
