﻿## Little about LINQ

If you are familiar with any of the .NET languages, you may have heard of LINQ. LINQ stands for *Language Integrated Query* and is a collection of operations that
you can perform on collections, which is expressed in code by the `IEnumerable` interface. Thanks to LINQ, you can generalize a wide variety of operations that you will
want to perform on collections and you don't have to implement them all every time you need them. This chapter is a brief introduction to LINQ and explains how it is used
in Expresso.

### What Is LINQ Like?

Take a look at the following code.

```expresso
let ary (- int[] = [1..20];
var result (- int = 0;
for let elem in ary {
    result += elem;
}
println("Result: ${result}");
```

<span class="caption">Listing 10-1: Example code before using LINQ</span>

`ary` contains integers less than 20. Then the above code calculates the sum of it and prints the result. That's just that but how do you think of it?
In the above code, we re-invented a sum operation and therefore it didn't convey our intent clearly. At least, you can't understand that it is trying to calculate the
sum by just seeing the first line. That's why Expressors(that's what we call ourselves, or Expressoists. Honestly, because I'm not a native English speaker, I don't
know which is more suitable. In my opinion, I prefer the latter because the Italian-like name + a French suffix = sounds cool) would think that it isn't good or nice.
Fortunately for us, LINQ has a suitable method that would fit our needs: `Enumerable.Sum`.

```expresso
import System.Linq.Enumerable as Enumerable;
import System.Collections.Generic.IEnumerable<> as IEnumerable;

def main()
{
    let ary (- int[] = [1..20];
    let result (- IEnumerable<int> = Enumerable.Sum(ary);
    println("Result: ${result}");
}
```

<span class="caption">Listing 10-2: Code with `Enumerable.Sum`</span>

Now the intent is clearer and the code is more concise, isn't it?

## How It's Used in Expresso

If you are familiar with C#, you would use LINQ as extension methods. <del>As of now, Expresso doesn't have extension methods in its toolbox. So you will need to write
the full name of those methods.</del>

> ### Extension Methods
> In C#, you can write the code in Listing 10-2 as follows:
>
> ```csharp
> static void Main()
> {
>     var ary = Enumerable.Range(1, 20);
>     var result = ary.Sum();
>     Console.WriteLine($"Result: {result}");
> }
> ```
>
> <span class="caption">Listing 10-3: Listing 10-2 in C#</span>
> 
> That is, an extension method can be seen as an instance method of the first parameter. Compare this to Listing 10-2. In general, extension methods convey the intent
> more clearly because the class name, which is unnecessary in this context, is omitted and the relationship between the subject and the verb is now obvious.
>
> <del>We are planning to implement it in Expresso, but maybe we won't any time soon.</del>
> Finally, extension methods have arrived on Expresso!

### Summary

Overall, you can use all the operations existing in LINQ, and futhermore you can do that just as you would in C#!<del>but you can't use them as you would in C#.</del>
Yes, there is no query expressions in Expresso and thus you will always have to use them in method form. So you would feel unusual, but please remember there is LINQ
in Expresso as well.
