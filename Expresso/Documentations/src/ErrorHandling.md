﻿## Error Handling

An error is a sign of life. Errors should happen when they should and the key is whether you would handle them, not whether they happen.
Like most other programming languages, Expresso has exceptions. It's just that *you throw, we throw*. But that's not the end of the story. Here, we'll look at error
handling using exceptions and the `Result<T, E>` type.

### Popped up Errors with Exceptions

Exceptions are a common concept in programming languages, so you may be familiar with it already. The essence in exceptions is that you handle errors by delegating it
to the functions that call the current function.

```expresso
import System.Exception as Exception;

def throwException() -> void
{
    throw Exception{message: "thrown"};
}

def main()
{
    try{
        throwException()
    }
    catch e (- Exception {
        println(e.Message);
    }
}
```

<span class="caption">Listing 8-1: Throwing and catching an exception</span>

In this little example, we throw and catch an exception. Like C++ and C#, throwing exceptions causes the call stack to unwind and has the calling functions handle it.
To throw exceptions, you will just put the `throw` keyword before an object creation expression. An exception is a simple instance of classes that derive from
the `Exception` class.

To catch exceptions, you will first surround `throw` expressions with the `try` block and put a `catch` block after it. By the `catch` keyword, you put a variable name
and the target class that you will handle there. The target class obeys the catch-all rule, meaning that the target class itself and any derived classes will be catched.
Therefore, the above code will catch any exceptions that happen in the `try` block.

For exceptions, you can choose not to handle them on purpose. In that case, it will lead your program to immediately halting and exiting.

#### The Return Type of Throwing Functions

Before moving on to the next topic, there is a point to note. Look at the following code.

```expresso
import System.Exception as Exception;

def parseZero(value (- string) -> ?
{
    if value.Equals("0") {
        return 0;
    }else{
        throw Exception{message: "We can't parse strings other than '0'!"};
    }
}
```

<span class="caption">Listing 8-2: What type should the function return?</span>

This is a small parse function that can only parse the string `"0"`. To compile and run the code, what type should we define the return type of the function as?

OK? The answer is `int`. Easy, huh?

Then, let's return to the Listing 8-1. Try changing the return type of the `throwException` function to, say, `int`, like so.

```expresso
// import omitted

def throwException() -> int
{
    throw Exception{message: "thrown"};
}
```

<span class="caption">Listing 8-3: Actually, what type does the function have to return?</span>

Get ready? Go!

See? It runs! But why does it run? Don't throwing functions return `void`?

Actually, the answer is no. They return neither `void` nor `int`. But then, how do they compile or even run?

To put it simply, in type theory, there is a special type called the *bottom type* and it can be substituted for any type. Thus the above code will compile and run.

### Unpopped up Errors with `Result<T, E>`

Sometimes, you may just need to know the result of some operation and the operation may be successful or in failure. Exceptions aren't the best choice in this scenario
because it unwinds the call stack and therefore implies some performance overhead. For such situations we have the `Result<T, E>` type. Technically, it's a generic enum
that takes 2 type parameters. One is called `T` standing for *Type* and the other is `E` standing for *Error* and the former represents the type of the result value,
while the latter represents the type of the error.

Let's use it together. Think about the code in Listing 8-2 again. Actually, this code should be written with the `Result<T, E>` type.

```expresso
import std_expresso::Result<,> from "std.exs" as Result;

def parseZero(value (- string) -> Result<int, string>
{
    if value.Equals("0") {
        return Result::Ok<int, string>{0: 0};
    }else{
        return Result::Error<int, string>{0: "We can't parse strings other than '0'!"};
    }
}
```

<span class="caption">Listing 8-4: The `parseZero` function using the `Result<T, E>` type</span>

This way, you can give the caller a chance to also delegate the error to callers or to handle it by itself.

Note that we import the `Result<T, E>` type from *std.exs*. This is rather a keyword, although it is in a string. Expresso will treat this keyword specially and it
will import the *std.exs* file located in the directory that the compiler executable resides in.