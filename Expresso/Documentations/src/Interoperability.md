﻿## Interoperability with Other .NET Languages

Let's imagine a world without any borders. You could go anywhere, you could do anything and you could live anywhere. Such a world would be woderful, don't you think?
In a sense, we can view Expresso as such. Because it runs on the CLR(Common Language Runtime), you can not only call functions or methods that come from other lanauages,
but also construct objects that come from those languages and even call instance methods on them. It's exactly as if we would be in a border-less world! Doing so is
as simple as follows:

```expresso
module main;


import OtherLanguageWorld.{TestClass, StaticTest} from "./CSharpDll.dll" as {TestClass, StaticTest};

def main()
{
    let t (- TestClass = TestClass{maximum: 1000};
    let i (- int = t.GetSomeInt();
    let seq (- intseq = t.GetSomeIntSeq(0);

    StaticTest.Greet();
    let flag (- bool = StaticTest.GetSomeFlag();
    let vec (- vector<int> = StaticTest.GetSomeIntList();

    println("${i}, ${seq}, ${flag}, ${vec}");
}
```

<span class="caption">Listing 12-1: Interoperating with C#</span>

First, we import types defined in the other language. Note that the file path in the `from` clause is relative to the source file that the import declaration
resides in. Note also that we specify the namespace when we import a C#'s type.
Once the types are imported, you can do anything with them as if they were defined in Expresso, as with the types in the standard library. Oh, I forgot showing
the C# code. Say we have the following C# code:

```csharp
// In TestInterface.cs
using System;
using Expresso.Runtime.Builtins;

namespace OtherLanguageWorld
{
    public interface TestInterface
    {
        int GetSomeInt();
        ExpressoIntegerSequence GetSomeIntSeq();
    }
}

// In TestClass.cs
using System;
using Expresso.Runtime.Builtins;

namespace OtherLanguageWorld
{
    public class TestClass : TestInterface
    {
        int maximum;

        public TestClass(int maximum)
        {
            this.maximum = maximum;
        }

        public List<int> GetSomeInt()
        {
            Console.WriteLine("GetSomeInt called");
            return 100;
        }

        public int GetSomeIntSeq(int lower)
        {
            Console.WriteLine("GetSomeIntSeq called");
            return new ExpressoIntegerSequence(lower, maximum, 1, false);
        }
    }
}

// In StaticTest.cs
using System;
using System.Collections.Generic;

namespace OtherLanguageWorld
{
    public class StaticTest
    {
        public static void Greet()
        {
            Console.WriteLine("Hello from StaticTest.Greet");
        }

        public static bool GetSomeFlag()
        {
            Console.WriteLine("GetSomeFlag called");
            return true;
        }

        public static List<int> GetSomeIntList()
        {
            Console.WriteLine("GetSomeIntList called");
            return new List<int>{1, 2, 3, 4, 5};
        }
    }
}
```

<span class="caption">Listing 12-2: C# code to import</span>

When we run the code in Listing 12-1, we'll see outputs from `Console.WriteLine` statements, `100`, `[0..1000:1]`, `true` and a list of 
`[1, 2, 3, 4, 5, ...]` on the console. Note that because we're currently defining the `ExpressoIntegerSequence` type in C#, we also can return it from C#.
As you can see from this fact, you can interoperate with Expresso from C# as well.

### Summary

To summarize, importing a DLL file enables you to import any types even defined in other .NET languages, and use them as if they were defined in Expresso. That is really
easy, isn't it?
