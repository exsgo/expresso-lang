# The Expresso Programming Language

[Preface and Introduction](./PrefaceAndIntroduction.md)

## Body

- [Getting Started](./GettingStarted.md)
- [Data Types](./DataTypes.md)
- [Basic Constructs](./BasicConstructs.md)
    - [Understanding Blocks](./Blocks.md)
- [Interfaces and Classes](./InterfacesAndClasses.md)
    - [Classes and Inheritance](./Inheritance.md)
    - [Classes and Casts](./Casts.md)
    - [Classes and Modifiers](./ClassModifiers.md)
- [Functions](./Functions.md)
- [Enums](./Enums.md)
    - [Concise Control Flow with `if let`](./IfLet.md)
- [Modules and Imports](./Modules.md)
- [Error Handling](./ErrorHandling.md)
- [String Interpolation and Raw Strings](./StringInterpolation.md)
- [Little about LINQ](./AboutLinq.md)
- [.NET as Standard Library](./DotNetAsStandardLibrary.md)
- [Interoperability with Other .NET Languages](./Interoperability.md)
- [Generic Types](./Generics.md)
- [Patterns Match the Structure of Values](./Match.md)
- [References](./References.md)
- [Closures](./Closures.md)
- [Use Guide](./UseGuide.md)

[Appendix](./AllKeywordsAndOperatorPrecedence.md)
