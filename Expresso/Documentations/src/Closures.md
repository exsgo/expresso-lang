﻿## Closures

Functions are useful in that they abstract away a series of operations and make them reusable. Functions are created because some piece of code will be used more than once.
But there are times when you want to abstract away a series of operations that will be used only once. That's where you should be equipped with *closures*.

### What Is Closures?

Though there are some minor differences, closures are like functions without names. You use closures in place of functions, and they work just like functions. However,
one difference between functions and closures is that, as I mentioned above, closures don't have names. Therefore, closures can't recurse into themselves. All recursions
must be unfolded as loops in closures. The other difference is that closures can capture surrounded values. This is what the name comes from. That is, the values will be
closed over. This behavior can be so useful that you might want to use them for this reason.

### Let's Use Them

As you might notice, we've already used closures twice; in Listing 2-8 and 2-9. Let's break down the syntax of closures bit by bit.

```expresso
|l, r| (l, r)
```

<span class="caption">Listing 16-1: Closure syntax</span>

This is a syntax for one-liner closures. To define a closure, we start with a pair of vertical pipes(`|`), inside which we specify the parameters for the closure;
this syntax is inspired by Rust. As you can see, and as with function parameters, you separate the parameters with commas.

After the parameters goes an expression, which represents the closure body in 1 line. This is the same as writing:

```expresso
|l, r| {
    return (l, r);
};
```

<span class="caption">Listing 16-2: Alternative syntax</span>

This means that you can omit the curly brackets and the `return` keyword if it contains just one return statement. Furthermore, you could write this as:

```expresso
|l (- int, r (- int| {
    return (l, r);
};
```

<span class="caption">Listing 16-3: Another syntax</span>

As you can see, the types of the parameters can all be omitted if the closure is passed directly to a function or a method. This is another deliberate choice in language
design. So remember that if you need to assign a closure to a variable, you have to write out all the parameter types of the closure. Of course, you can write
the return type.

```expresso
|l (- int, r ( - int| -> (int, int) {
    return (l, r);
};
```

<span class="caption">Listing 16-4: A closure with all the types written explicitly</span>

But generally, you wouldn't need to do so. You can just leave the compiler to infer the return type.

With all the type annotations, the syntax of the closure looks more similar to that of functions or methods. Let's line up them then.

```
def createTuple1   (l (- int, r (- int) -> (int, int) {return (l, r);}
let createTuple2 = |l (- int, r (- int) -> (int, int) {return (l, r);};
let createTuple3 = |l,        r|                      {return (l, r);};
let createTuple4 = |l,        r|                              (l, r)  ;
```

This way, you can easily see how closure syntax is similar to function syntax except for the use of pipes and the amount of syntax that is optional. In this list, 
the first line shows a function definition, and the second line shows a fully annotated closure definition. The third line removes the type annotations from the closure
definition, and the fourth line removes the curly brackets and the `return` keyword, which are optional because the body has only one statement. These are all valid
definitions that will produce the same behavior when they're called.

Next, let's look at the closure's capability that functions don't have.

### Capturing Environment

```expresso
let n = 2;
let c = |x (- int| x + n;
let a = c(5);
println("${a}");
```

<span class="caption">Listing 16-5: A closure that captures a variable</span>

This time, we've captured `n` in the closure. To capture it, just use it in the closure and Expresso will take care of everything else. It's easy, huh? Note that
the captured variables will be copied, meaning that if you change their values in the closure, it isn't visible outside the closure. But if the captured variables are
reference types, then the changes will be visible outside the closure.

> ### Tip about Closures
>
> Technically, closures are class instances. They capture their environment using fields and this could incur performance overhead. So you should define closures
> when absolutely necessary.

Though I haven't tried it, you should be able to capture fields as well. Just give it a shot.

> ### You Can Give Shorter Names to Short-Lived Variables
>
> Remember that I said to give things longer names? That rule generally applies to anything, but that depends on how long the variables are alive. Imagine the following
> situation.
> 
> ```expresso
> def main()
> {
>     let c = |n (- int| n + 1;
>     let six = c(5);
> }
> ```
>
> <span class="caption">Listing 16-6: A closure that is only alive for a short period of time</span>
>
> In Listing 16-6, the closure itself is only alive for 2 lines and the parameter of the closure is only alive for 1 line. Do you think we need to give the paremeter
> a longer name? I would say no because closures are usually local to a function or a method and so is indeed the closure.
>
> Let's say we have the following code.
>
> ```expresso
> def main()
> {
>     let closure = |willBeAddedOne (- int| willBeAddedOne + 1;
>     let constant_six = closure(5);
> }
> ```
>
> <span class="caption">Listing 16-7: The code that we gave longer names</span>
>
> Too much information, do you think? Too much information certainly deteriorates readability and throws you into a darkness(hahaha, do you know what I mean?). So use
> your brain ;-)
