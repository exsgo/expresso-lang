﻿The main policy for Expresso is that "Programming languages must allow programmers to write what it does, not how it does something".
In traditional C, we often end up writing something like the following:

```c
// construct some array of ints
// and name it array
for(int i = 0; i < sizeof(array) / sizeof(int); ++i){
    // do something on each element to transform them
}
```

Even though the for loop has long long history, I think that it doesn't express your intension very clearly especially
when you want to process an array. Instead, I recommend you to use functional style. In functional programming languages,
we define the work flow as a chain of functions. So in Expresso, you can rewrite the above example like this:

```expresso
// construct some array
// and assume the array is named "a"
let mapped = Enumerable.Select(a, |elem| => /* do something on each element */);
```

This reads: We're assigining the value that will be computed by iterating through the array and performing some calculations on each element of
the array to a variable. This sounds more straightforward and is easier to read, isn't it?

## Statements

Like most curly-brace-and-semicolon-delimited languages, Expresso employs statements for its first language constructs.

## Expressions

## Tuples, vectors and dictionaries

Expresso supports three basic data structures as builtin types. The biggest advantage of supporting vectors or dictionaries as builtin types
is the ability to support literal forms that help programmers make those objects easily and thoroughly.
And also, it is worth noting that the compiler can tell more specific and understandable error messages if vectors and dictionaries are builtin types.
Of course, it can even suggest the propper usages when it encounters some errors.
In contrast, it can be considered as a disadvantage that supporting those types as builtin types makes it hard to maintain the source code and
it can lead to compiler size inflation.