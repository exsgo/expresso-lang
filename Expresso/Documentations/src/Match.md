﻿## Patterns Match the Structure of Values

If you are from imperative languages, you are most likely not to know what patterns are. They consist of some combinations of the following:

* Literals
* Destructured classes, enums, or tuples
* Variables
* Wildcards
* Placeholders

These components describe the shape of the data we're working with, which we then match against values to determine whether our program has the correct data to continue
running a particular piece of code.

Even though patterns can only be used in certain places and the current implementation is limited, they have a huge potential inside them.

### All the Places Patterns Can Be Used

#### `match` Arms

Have you seen the `match` statement? You may say no. `match` statements can be considered as a replacement for `switch` statements, considering the purposes of them.
As `switch` statements take different paths depending on the value, `match` statements do so depending on the structure of the value. In other words, we compare a value
to patterns of branches and take the most matched path from them.

A `match` statement takes the following form:

```
match VALUE {
    PATTERN => STATEMENTS;,
    PATTERN => STATEMENTS;,
    PATTERN => STATEMENTS;
}
```

The trailing semicolons are mandatory now. I know we should remove them, but that will be very difficult. One requirement for `match` statements is that they need to
be *exhaustive* in the sense that all possibilities for the value in the `match` statement must be accounted for, although literal values will only be tested for 
whether they have the `_` pattern or a variable pattern. One way to ensure that you've covered every possibility is to have a catchall pattern for the last arm:
for example, a variable name matching any value can never fail and thus covers every remaining case.

A particular pattern `_` will match anything, but it never binds to a variable, so it's often used in the last match arm. The `_` pattern can be useful when you want to
ignore any value not specified, for example.

#### The `for` Statement

No matter whether you know patterns in this chapter or you've known them for a long time, we've already used it in Chapter 2 and 3. The `for` statement takes a pattern,
but it's fairly limited. Currently only a variable or a tuple containing multiple variables can be used in that place. If you want, for example, an array of
class instances, you would need to take it as a variable first and then pattern match it with the `match` statement. This restriction might be eased off in the future.

Because you are most likely not to be familiar with patterns, next we'll list all the things that you can do with patterns. Expresso is a developing language so
it has less features of patterns than other languages. Use this as a reference.

#### The Variable Declaration Statement

As in `for` statements, you can use simple patterns in variable declaration or let binding statements. They can only be either an identifier pattern or a tuple pattern
and prohibit nesting. That way, you can decouple tuples into individual variables.

#### Conditional `if let` or `if var` Statements

In Chapter 6, we've seen that `if let` or `if var` statements would be a replacement for a `match` that would only match one pattern. Additionally, you can include
an `else` in an `if let` or `if var` statement.

Futhermore, you can mix an `if let` or `if var` statement with ordinary `if`, `else if` and `else` statements. Let's imagine that we want to output a message based on
the values that we would get from the user in a real program.

```expresso
import std_expresso::{Option<>, Result<,>} from "std.exs" as {Option, Result};

def main()
{
    let option = Option::None<string>{};
    let is_tuesday = false;
    let age = Result::Ok<int, string>{0: 34};

    if let Option::Some{favorite_color} = option {
        println("Using your favorite color ${favorite_color} as the background!");
    }else if is_tuesday {
        println("Tuesday is green day!");
    }else if let Result::Ok{real_age} = age {
        if real_age > 30 {
            println("Using purple as the background color.");
        }else{
            println("Using orange as the background color.");
        }
    }else{
        println("Using blue as the background color.");
    }
}
```

In this code, we hardcoded values, which we would be likely to get from the user. From top to bottom, we first check that the user selected a favorite color, and if so,
we will use that color as the background color. If today is Tuesday, the background color is green. If the user specifies their age as an `int` and we can get it as
a `Result`, then the color is either purple or orange depending on the value of the number. Otherwise, we will use blue as the background color.

As you can see from this example, we can not only mix an `if let` or `if var` statement with ordinary `if`, `else if` and `else` statements, but also make them not
related to each other. This flexibility gives us extra power to express complex conditions.

However, there is a downside to use `if let` or `if var` statements. That's because we will lose exaustiveness checking, whereas with `match` statements it does.
If we omitted the last `else` and therefore missed handling some cases, the compiler would not alert us to the possible logic bug.

### Pattern Syntax

#### Matching Literals

We completely haven't yet seen patterns. Here's an example.

```expresso
let x = 1;
match x {
    1 => println("one");,
    2 => println("two");,
    3 => println("three");,
    _ => println("other");
}
```

This code prints `one` because the value in `x` is 1. It looks like a `switch` statement if you only match against literal values like that. You can match against string
literals as well. We'll talk about the `_` syntax later.

#### Matching Named Variables

Named variables match any value of one type. Therefore you can use it like this.

```expresso
let x = 10;
match x {
    1 => println("one");,
    2 => println("two");,
    3 => println("three");,
    y => println("${y}");
}
```

<span class="caption">Listing 14-1: A `match` statement that cares about all the remaining cases</span>

This code prints 10. Note that you can't write the `y` as `x` currently because then the `x` in the match statement *shadows* the outer `x`. Shadowing is prohibited
in Expresso. But it is a developing language, so it may be changed in the future.

By the way, you could have written it like this.

```expresso
let x = 10;
if x == 1 {
    println("one");
}else if x == 2 {
    println("two");
}else if x == 3 {
    println("three");
}else{
    println("${x}");
}
```

<span class="caption">Listing 14-2: A more verbose way to express Listing 14-1</span>

It's not that bad, do you think? Ah, yes. Some people might rather prefer this way because it is simple. But imagine that you have to list all the way to, say, 10.
That would take over 20 lines on screen. It's still good. But what about 20? Then that would take over 40 lines. It's now bad, at least not good.
Of course, it's up to you whether you choose `match` statements or `if`, `else if` and `else` statements. It's all your preference.

#### Multiple Patterns

In `match` statements, you can match multiple patterns using the `|` syntax, which means *or*. For example, the following code matches the value of `x` against match arms,
the first of which has an *or* option, meaning if the value of `x` matches either of the values in that arm, that arm's code will run:

```expresso
let x = 1;
match x {
    1 | 2 => println("one or two");,
    3 => println("three");,
    _ => println("other");
}
```

This code prints `one or two`.

#### Matching Ranges of Values with `intseq`s

As you might imagine, the literal values fitting into patterns include instances of `intseq`. So, for example:

```expresso
let x = 3;
match x {
    1..5 => println("one through five");,
    _ => println("something else");
}
```

This code prints `one through five`, as you would expect. Because it's a plain `intseq`, if you wrote it as `1...5`, then 5 would be matched.

#### Destructuring Patterns

We can also use patterns to destructure classes, enums and tuples to use different parts of these values. Let's walk through each value.

##### Destructuring Classes

Currently in Expresso, we can't destructure values using `let` statements. Let's break apart the `Circle` class that was created in Listing 4-1.

```expresso
let circle = Circle{x: 0, y: 0, radius: 10};
match circle {
    Circle{x: x, y: y, radius: radius} => println("It's a circle at (${x}, ${y}) with radius: ${radius}");
    _ => println("dummy");
}
```

<span class="caption">Listing 14-3: Breaking apart an instance of the `Circle` class using a `match` statement</span>

This code creates the variables `x`, `y` and `radius` that match the values of `x`, `y` and `radius` fields of `circle`. The names of the variables don't have to match
those of the fields, but it will be preferable to have those matched.

##### Destructuring Enums

This time, let's break apart the `TrafficLight` enum that was completed in Listing 6-4.

```expresso
let light = TrafficLight::Green{0: 10};

match light {
    TrafficLight::Green{time} => println("Green continues for ${time} seconds.");,
    TrafficLight::Yellow{time2} => println("Yellow continues for ${time2} seconds.");,
    TrafficLight::Red{time3} => println("Red continues for ${time3} seconds.");
}
```

<span class="caption">Listing 14-4: Breaking apart an instance of the `TrafficLight` enum using a `match` statement</span>

This code creates a `time` variable in the first arm, and prints `Green continues for 10 seconds`. Try changing the let binding and see how the output also changes.

Then, imagine we have the following enum.

```expresso
enum Message
{
    Quit(),
    Go(int)
}
```

<span class="caption">Listing 14-5: Definition of the `Message` enum</span>

You must output some message when you get `Message::Quit` value. What would you do?

```expresso
let quit = Message::Quit{};

match quit {
    Message::Quit => println("Quit");,
    Message::Go{address} => println("Go to ${address}");
}
```

<span class="caption">Listing 14-6: A wrong way to handle the `Message` enum</span>

It's wrong. Unfortunately, you can't omit the curly brackets just after the variant name. So you have to instead write `Message::Quit{} => println("Quit");,`.
This is due to a grammatical restriction.

Currently, patterns can not be nested. If you need to handle nested values, you will have to nest `match` statements.

##### Destructuring Tuples

Like enum variants, tuples can also be destructured.

```expresso
let tuple = (1, 2);

match tuple {
    (x, y) => println("(${x}, ${y})");
}
```

As I mentioned above, `let` statements can't be used to destructure values. Destructuring tuples is straightforward so you don't have to worry about anything with it.

#### Ignoring Values in Patterns

You've seen that it's sometimes useful to ignore values in a pattern, such as in the last arm of a `match`, to get a catchall that doesn't actually do anything but
does account for all remaining possible values. There are a few ways to ignore entire values or parts of values in a pattern: using the `_` pattern(which you've seen),
using the `_` pattern within another pattern, or using `..` to ignore remaining parts of a value. Let's explore how and why to use each of these patterns.

##### Ignoring an Entire Value with `_`

We've used the underscore(`_`) as a wildcard pattern that will match any value but not bind to the value. So that's all for it.

##### Ignoring Parts of a Value with a Nested `_`

We can also use `_` inside another pattern to ignore some part of the structure. Let's say we have a function that returns a tuple like so.

```expresso
def returnTuple(x (- int, y (- int)
{
    return (x, y);
}
```

<span class="caption">Listing 14-7: Some example function that returns a tuple</span>

Then, we can ignore part of it as follows.

```expresso
def main()
{
    let tuple = returnTuple(1, 2);
    match tuple {
        (_, y) => println("y is ${y}");
        _ => println("other")
    }
}
```

<span class="caption">Listing 14-8: A use of the `returnTuple` function</span>

We can also use underscores in multiple places within one pattern to ignore particular values. Listing 14-9 shows an example of ignoring the second and fourth values
in a tuple of five items.

```expresso
let fibonacci = (1, 1, 2, 3, 5);

match fibonacci {
    (first, _, third, _, fifth) => {
        println("Some fibonacci numbers: ${first}, ${third}, ${fifth}");
    }
}
```

<span class="caption">Listing 14-9: Ignoring multiple parts of a tuple</span>

This code will print `Some fibonacci numbers: 1, 2, 5`, and the values 1 and 3 will be ignored.

##### Ignoring Remaining Parts of a Value with `..`

With values that have many parts, we can use the `..` syntax to use only a few parts and ignore the rest, avoiding the need to list underscores for each ignored value.
In Listing 14-10, we have a `Point` class that holds a coordinate in three-dimentional space. In the `match` statement, we want to operate only on the `x` coordinate
and ignore the values in the `y` and `z` fields.

```expresso
class Point
{
    let x (- int, y (- int, z (- int;
}

def main()
{
    let point = Point{x: 0, y: 1, z: 2};
    match point {
        Point{x: x, ..} => println("x is ${x}");
    }
}
```

<span class="caption">Listing 14-10: Ignoring all values of a `Point` except for `x` by using `..`</span>

However, the functionality of `..` is incomplete so you may experience a glich.

#### Extra Conditionals with Match Guards

A *match guard* is an additional `if` condition specified after the pattern in a `match` arm that must also match, along with the pattern matching, for that arm to be
chosen. Match guards are useful for expressing more complex ideas than a pattern alone allows.

The condition can use variables created in the pattern.

```expresso
let some_tuple = (1, 2);

match some_tuple {
    (x, _) if x < 5 => println("x is less than five: ${x}");,
    (x, _) => println("x is ${x}"),
    _ => println("otherwise");
}
```

<span class="caption">Listing 14-11: A match arm with a *match guard*</span>

This code will print `x is less than five: 1`.

If `some_tuple` had been `(10, 2)` instead, the match guard in the first match arm would have been false because 10 is not less than 5. Expresso would then go to the
second arm, which would match because the second arm doesn't have a match guard and therefore matches any 2-element tuple.

There is no way to express the `if x < 5` condition within a pattern, so the match guard gives us the ability to express this logic.

Also, you can use the *or* operator along with a match guard. The match guard condition will apply to all the patterns. Listing 14-12 shows the precedence of combining
a match guard with a pattern that uses `|`. The important part of this example is that the `if y` match guard applies to `4`, `5`, *and* `6`, even though it might look
like `if y` only applies to `6`.

```expresso
let x = 4;
let y = false;

match x {
    4 | 5 | 6 if y => println("yes");,
    _ => println("no");
}
```

<span class="caption">Listing 14-12: Combining multiple patterns with a match guard</span>

Let's walk through each step that the `match` does when you run this program. The pattern of the first arm is matched because `x` is 4, but it won't be chosen
because `y` is `false` and therefore the `if y` match guard is `false`. The code moves on to the second arm, which does match, and the program will print `no`. 
The reason is that the `if` condition applies to the whole pattern `4 | 5 | 6`, not only to the last pattern `6`. In other words, the precedence of a match guard
in relation to a pattern behaves like this:

```
(4 | 5 | 6) if y => ...
```

rather than this:

```
4 | 5 | (6 if y) => ...
```

After running this code, the precedence behavior is evident: if the match guard were applied only to the final value in the list of values specified with the `|`
operators, the arm would have matched and thus the program would have printed `yes`.

#### `@` Bindings

The *at* operator(`@`) lets us create a variable that holds a value at the same time we're testing that value to see whether it matches a pattern. Listing 14-13 shows
an example where we want to test that `x` is within the range `3...7`. But we also want to bind the value to a variable `x2` so that we can use it in the code
associated with the arm.

```expresso
let x = 5;
match x {
    x2 @ 3...7 => {
        println("Found that x is in range: ${x2}");
    },
    10...12 => {
        println("Found that x is in another range");
    },
    x2 => println("otherwise: ${x2}");
}
```

<span class="caption">Listing 14-13: Using `@` to bind to a value in a pattern while also testing it</span>

In the second arm, where we only have a range that doesn't overlap with the range within the first arm, the associated code doesn't know the value of `x` because we don't
specify any variables in the pattern. We just know `x` was 10, 11 or 12 but not that we got which of these. In this case, however, you can use `x` directly
to inspect its value.

In the last arm, where we just have a variable that holds the value of `x`, though the associated code knows and can access the value of `x`, it's not guaranteed that
`x` was in the ranges, which means any value would match.

### Summary

It was a rather long chapter, wasn't it? But it means that `match` statements have as much power as the expression is true and you've seen the power of `match` statements,
haven't you? Now that you have the fundamental knowledge, let's proceed to more advanced topics next.
