﻿## Appendix

### All the Keywords

Here, we list up all the keywords valid in Expresso programs.

`abstract`, `as`, `asm`, `beforehand`, `bigint`, `bool`, `break`, `byte`, `catch`, `char`, `class`, `continue`, `def`, `dictionary`, `do`, `double`, `else`, `enum`,
`export`, `false`, `fld`, `finally`, `float`, `for`, `friends`, `from`, `if`, `import`, `in`, `int`, `interface`, `intseq`, `let`, `match`, `meth`, `module`,
`mutating`, `null`, `override`, `param`, `private`, `protected`, `public`, `requires`, `return`, `sealed`, `self`, `slice`, `static`, `string`, `throw`, `true`, `try`,
`uint`, `upto`, `var`, `vector`, `virtual`, `void`, `where`, `while`, `with`

Below, we list up all the keywords that are reserved for future use.

`yield`, `internal`, `super`

### Operator precedence

Here, we'll explain the operator precedence in Expresso. Operator precedence is the order that operators are bound with. From up to bottom, it will become less precedent.

|Precedence|Operator|Explanation|
|:--------:|:-------|:----------|
|1|(-|Type binding|
|2|(...)|Grouping and tuple creation|
|3|func()|Function call|
|3|item[]|Indexing|
|3|.elem|Member access|
|4|!|Logical NOT|
|4|&|Reference|
|4|+|Unary plus|
|4|-|Arithmetic negation|
|5|as|Casting|
|6|**|Exponentiation|
|7|*|Multiplication|
|7|/|Division|
|7|%|Modulo|
|8|+|Addition|
|8|-|Subtraction|
|9|<<|Bitwise left shift|
|9|>>|Bitwise right shift|
|10|&|Bitwise AND|
|11|^|Bitwise XOR|
|12|\||Bitwise OR|
|13|a(..\|...)b:c|Integer sequence|
|14|<|Comparison|
|14|>|Comparison|
|14|==|Comparison|
|14|!=|Comparison|
|14|<=|Comparison|
|14|>=|Comparison|
|15|&&|Conditional AND|
|16|\|\||Conditional OR|
|17|?:|Conditional operator|
|18|=|Assignment|
|18|+=|Assignment by sum|
|18|-=|Assignment by difference|
|18|*=|Assignment by product|
|18|/=|Assignment by quotient|
|18|%=|Assignment by remainder|
|18|<<=|Assignment by bitwise left shift|
|18|>>=|Assignment by bitwise right shift|
|18|&=|Assignment by bitwise AND|
|18|\|=|Assignment by bitwise OR|
|18|^=|Assignment by bitwise XOR|
