﻿## Modules

Have you heard of modules? Even if you say no, we've defined them hundreds of times already. Look at the beginning of the source codes we've written so far, do you see
`module main;`? Yes, this defines a module named `main`. The `main` module provides the program's entry point and it is called `main`.
Here, let's define an external module that can be used in the `main` module.

```expresso
module test_module;

export class TestClass
{
    let x (- int, y (- int;

    public def getX()
    {
        return self.x;
    }

    public def getY()
    {
        return self.y;
    }
}

export let pair = (100, 200);

export def createInstance(x (- int, y (- int)
{
    return TestClass{x: x, y: y};
}
```

<span class="caption">Listing 7-1: An external module definition</span>

You can define module functions, module variables and classes in modules and specify whether each of them will be exported with the `export` modifier. Only exported items
can be accessed from external modules.

### The Import Statement

To use module items, you need to put import statements before using.

```expresso
module main;

import test_module from "./test_module.exs" as TestModule;
```

<span class="caption">Listing 7-2: An import statement that imports a whole module</span>

To import a whole module, you write like the above, and access to the module object using `TestModule` in this Listing. So for example, to call
the `createInstance` function, you need to write `TestModule::createInstance()`.

On the other hand, if you don't need everything in a module, you can import some of the items.

```expresso
module main;

import test_module::{TestClass, pair} from "./test_module.exs" as {TestClass, pair};
```

<span class="caption">Listing 7-3: Another import statement that imports only some of the items</span>

This statement states that we will import `TestClass` and `pair`. Then you can construct a `TestClass` object like writing `let a = TestClass{x: 1, y: 2}`.
Even though the names are matched, you can't omit the `as` clause. It might be changed in the future, though.

This import statement occupies the global namespace. Thus it becomes more likely to cause a name conflict, so be careful.

Let's talk about the missing 2 lines at the beginning in Chapter 2. To import from namespaces, you will use `.` instead of `::`. As you saw in Listing 2-8 and 2-9,
if you will import generic types, you should indicate that by adding `<>` postfix. This notation can be seen in C#, and as you might expect, you will add as many
commas as there are type arguments in the generic type. So you would write `import System.Collections.Generic.Dictionary<,> as Dictionary;` rather than writing
`import System.Collections.Generic.Dictionary<K, V> as Dictionary;`.

This is a semantic restriction. When actually importing types, we need to know whether the types have type arguments or not beforehand. Without them, we would be
searching for types that have no type arguments, which would result in an error.

#### Globs

Starting from exsc 1.1.9, we can now use the glob operator(`*`) to import every type in a namespace and you don't need to name them each by each. So for example, 
until 1.1.9, we had to write like the following.

```expresso
import InteroperabilityTest.{TestInterface, InteroperabilityTest, StaticTest, PropertyTest, TestEnum, EnumTest, SomeGenericClass, MyIntseq, ArgumentTest, ExtensionClass} from "./InteroperabilityTest.dll" as {TestInterface, InteroperabilityTest, StaticTest, PropertyTest, TestEnum, EnumTest, SomeGenericClass, MyIntseq, ArgumentTest, ExtensionClass};
```

<span class="caption">Listing 7-4: A too long import list</span>

From exsc 1.1.9, we can write like this.

```expresso
import InteroperabilityTest.* from "./InteroperabilityTest.dll" as *;
```

And it means exactly the same as Listing 7-4. This would save a lot of time. Whew.
