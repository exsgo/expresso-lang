﻿## Enums

Enums or **enumerations** are useful when you have options. Enums also are common concepts in programming languages, you may have heard of them.
But their features differ in each language. In Expresso, like Rust, they are most alike to **algebraic data types** in Functional programming languages such as F#, Ocaml
and Haskell. In this chapter, we'll implement a simple traffic light by using an enum and see its advantages over classes. Get ready?

### Specifications and Implementations

Above all, let's think about what a traffic light will do. It has 3 states. Go, be cautious and stop. Enums should go where there are options. Here's the code.

```expresso
enum TrafficLight
{
    Green,
    Yellow,
    Red
}
```

<span class="caption">Listing 6-1: Definition of the traffic light</span>

OK, states defined. Traffic lights aren't traffic lights unless they signal whether you can go or stop. Let's implement that next.

```expresso
enum TrafficLight
{
    // States omitted

    public def canGo()
    {
        match self {
            TrafficLight::Green{} => return true;,
            _ => return false;
        }
    }

    public def shouldStop()
    {
        match self {
            TrafficLight::Red{} => return true;,
            _ => return false;
        }
    }
}
```

<span class="caption">Listing 6-2: Definition of signaling</span>

There should be something new to you: the `match` statement. We'll describe it thoroughly in Chapter 14, so for now, consider it like the `switch` statement.

Then, let's implement state change. But wait, you may have the question, how long does the green state continue? 5 seconds? 10 seconds? How do we know that?
So each state should have seconds that the state should continue to be in. Having learned that classes can have fields, you may think that the code like this
will fulfill that.

```expresso
class TrafficLight
{
    private let green_duration (- int;
    private let yellow_duration (- int;
    private let red_duration (- int;

    private var current_state (- TrafficLight;
}
```

<span class="caption">Listing 6-3: Definition of the `TrafficLight` class</span>

Yes, that should do what we want, but it's rather verbose. So in Expresso, like Rust, each variant of enums can have fields like this.

```expresso
enum TrafficLight
{
    Green(int),
    Yellow(int),
    Red(int)

    // methods omitted
}
```

<span class="caption">Listing 6-4: Revised definition of the `TrafficLight` enum</span>

Then, we can implement state changes.

```expresso
import System.{Console, ConsoleColor} as {Console, ConsoleColor};
import System.Threading.Thread as Thread;

let greenDuration = 20_000;
let yellowDuration = 3_000;
let redDuration = 10_000;

def changeState(trafficLight (- TrafficLight)
{
    let prev_color = Console.ForegroundColor;
    match trafficLight {
        TrafficSignal::Green{milliseconds} => {
            Console.ForegroundColor = CosoleColor.Green;
            println("Green");
            Thread.Sleep(milliseconds);
                
            Console.ForegroundColor = ConsoleColor.Yellow;
            println("Next yellow");
            Console.ForegroundColor = prev_color;
            return TrafficLight::Yellow{0: yellowDuration};
        },
        TrafficSignal::Yellow{milliseconds} => {
            Console.ForegroundColor = ConsoleColor.Yellow;
            println("Yellow");
            Thread.Sleep(milliseconds);

            Console.ForegroundColor = ConsoleColor.Red;
            println("Next red");
            Console.ForegroundColor = prev_color;
            return TrafficLight::Red{0: redDuration};
        },
        TrafficSignal::Red{milliseconds} => {
            Console.ForegroundColor = ConsoleColor.Red;
            println("Red");
            Thread.Sleep(milliseconds);

            Console.ForegroundColor = ConsoleColor.Green;
            println("Next green");
            Console.ForegroundColor = prev_color;
            return TrafficLight::Green{0: greenDuration};
        }
    }
}
```

<span class="caption">Listing 6-5: Definition of a state change function</span>

With this code, you should be able to easily imagine how the traffic light works. Note that when creating an instance of the enum, we are using
`TrafficLight::Green{0: greenDuration}` syntax. This is due to a grammatical restriction. The number before the colon corresponds to the position of the values.

```expresso
enum Message
{
    Color(int, int, int)
}
```

So let's say we have this enum. Then, you will have to create an instance of it with `let color = Message::Color{0: 0, 1: 160, 2: 255};`.

In case, here's the code that uses the `TrafficLight` enum.

```expresso
var traffic_light = TrafficLight::Green{0: greenDuration};

traffic_light = changeState(traffic_light);
traffic_light = changeState(traffic_light);
traffic_light = changeState(traffic_light);
```

<span class="caption">Listing 6-6: A code that uses the `TrafficLight` enum</span>

As you can see, enums are suitable for things that have some options. Like C Enums, they can be associated with integers. Unlike C, though, you aren't allowed to convert
them between integers and variants. In Expresso, enum variants are enum variants. They are different from integers, after all. But maybe variants can be casted into
integers in the future.
