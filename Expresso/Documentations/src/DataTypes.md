﻿## Data Types

### The `int`, `uint` and `byte` Types

The first types that we'll look at are the `int`, `uint` and `byte` types. Expresso provides just these three types for integers because it's considered to be used as
a scripting language rather than a type-strict language, which would need more control over the size of integers.

As internal representations, `int`, `uint` and `byte` use `int`, `uint` and `byte` types on C#, respectively, so see API documentations for more information on those
types.

### The `float` and `double` Types

Next up is the `float` and `double` types. Like the above types, they are defined on C# as well.
That's all for floating-point numbers.

### The `bool` Type

Next, let's look at a simple type: the `bool` type. The `bool` represents the boolean and has two possible values: `true` and `false`.
In Expresso, the `if` statement doesn't allow the conditional to have any types other than `bool`. If you are trying to do that,
you'll see the following error:

```
Error ES4000: The conditional expression has to be of type `bool`.
```

The error message should tell the story.

### The `char` Type

The `char` type represents a character. As usual, it is the same as the C#'s char type, so see the API documentation for more information on the type.
But it is worth noting that the C#'s char type (and thus the `char` type) is encoded in UTF-16. To put it simply, a `char` can represent any character in UTF-16.
It's wonderful considering the fact that in the traditional C, most of the characters aren't represented in one char.

### The `bigint` Type

In the computer world, numbers are approximated using two's complement representation or are limited in size. For example, the `int` type can only represent integers
from -(2<sup>31</sup>) to 2<sup>31</sup> - 1 inclusive.

But in some fields, you'll need to represent numbers more properly, for example, when dealing with money. For those situations Expresso provides the `bigint` type.
A `bigint` can store any arbitrary integer. This makes the `bigint` type suitable for dealing with money.

> ### Column: Digital vs. Analog
>
> Fundamentally, numbers in the analog world is infinite. Let's write a number together. We start from a 1, and then we can write any number of 0's. Even though
> you can't say the number properly, it's still a valid number(integer) in digit. To put it simply, the analog world is linear and infinite.
>
> Meanwhile, in the digital world, we can't represent numbers in any size because we only have limited memory and time. Futhermore, the digital world is not linear
> because we have limited memory and time again. This is a very interesting topic, but digging into the details is out of the scope of this tutorial. Feel free to
> google about it or read books.

### The `string` Type

I realize that `char` and `string` types are the fundamental types because sometimes you would even need more string manipulations than you would on integers.
As with the `char` type, the `string` type also is in UTF-16. See the C#'s documentation for other details on the `string` type.

### The `tuple` Type

Sometimes, you may want to return more than 1 variable from functions. In other times, you may want to combine two values into some grouped construct.
For those situations, we have the `tuple` type as a builtin type. The `tuple` type conceptually groups more than 1 value into one construct and allows you to
move it around and pass it around as if it is one value.

### The `vector` Type

In programs, you would often want to store multiple values of one type in one place. That's the time when the `vector` type comes into play. The `vector` type 
allows you to put multiple values of one type in an object like the following.

```expresso
var natural_numbers (- vector<int> = [0, 1, 2, 3, 4, 5, ...];
```

<span class="caption">Listing 2-1: Initializing a vector of natural numbers</span>

Note the trailing periods. If you forget them, the object will be an array, which doesn't allow you to grow or shrink its size. As you can see,
Expresso allows you to construct a vector object in literal form, which most other type-strict programming languages don't.

You can add or remove an item from the vector.

```expresso
natural_numbers.Add(6);
println("${natural_numbers}");
natural_numbers.Remove(6);
println("${natural_numbers}");
```

<span class="caption">Listing 2-2: Several uses of the vector's methods</span>

For other methods available, see the API documentation for the .NET's `List<T>` class. <del>Note that we still can't call extension methods.</del>

Note that the statement `println("${natural_numbers}")` outputs like `[0, 1, 2, 3, 4, 5, ...]`. These formats are specialized for the builtin types namely `array`,
`vector` and `dictionary`.

Note that the type of the vector is written as `vector<int>`. In other words, the type of vectors is written as `vector<Element type>`. In general, this syntax is
called generics. We'll talk more in depth about generics in Chapter 13, but for now know that `vector<int>` states that we want a `vector` of `int`s.

### The `array` Type

The `array` type is another sequence type, which, as we mentioned above, doesn't allow you to grow or shrink its size. Thus the compiler already knows the size of
an array object when compiling. However, it currently doesn't take advantage of it.

```expresso
let natural_numbers2 (- int[] = [0..5];
```

<span class="caption">Listing 2-3: Initializing an array using an intseq exression</span>

Note that we write it this time without trailing periods. As mentioned above, it will make an array, which has a different API from the vector.
Note also that it uses an expression that is much like a range in other programming languages. We call it an intseq expression, but the functionality is
almost the same as that of ranges. We'll come back later to this topic again.

Look at the following code.

```expresso
import System.Type as Type;

def main()
{
    let ints (- int[] = int[5];
    println("${ints}");
    
    let types (- Type[] = Type[5];   // This would throw a compiler error
    println("${types}");
}
```

<span class="caption">Listing 2-4: Initializing arrays with default values</span>

You may be familiar with the syntax. These let bindings create arrays with default values, and the first `println` statement would print `[0, 0, 0, 0, 0]`.
However, the latter let binding would result in a compiler error. In Expresso, this isn't allowed because we have no `null` values in regular Expresso code. 

By the way, this is a deliberate decision in language design. In this way, you have to always be explicit with the default values of user-defined types when creating
an array. In other words, the following code will compile and run.

```expresso
let some_int (- int = 0;
let types (- Type[] = [some_int.GetType(), some_int.GetType(), some_int.GetType()];
println("${types}");
```

<span class="caption">Listing 2-5: A working code</span>

This code isn't useful on its own, but it should make sense, or even if it doesn't to you currently, you would understand the implication someday. Oh, and there is
a possibility that we will change the implementation like using `Default` interface.

### The `dictionary` Type

The `dictionary` type, which is sometimes called a `HashMap` or simply an `object`, is a collection of several keys and values.

```expresso
let some_dictionary (- dictionary<string, int> = {"a": 10, "b": 20};
```

<span class="caption">Listing 2-6: Initializing a dictionary using a literal</span>

Like Python, you can create a dictionary in literal form in Expresso.

### The `intseq` Type

One unique characteristic for Expresso is the built-in `intseq` type. As the name suggests, it produces a series of integers.
The `intseq` type has 3 fields, `start`, `end` and `step`. `start` represents the start value of the sequence,
`end` the end value and `step` the step by which an iteration proceeds at a time.
The `intseq` type has the corresponding literal form and it is notated as `start(..|...)end[:step]`.
This can be read in English as *from start to end by step*.

```expresso
let series (- int[] = [1..10];   // `step` can be omitted and 1 is assumed if ommited and the double dots mean that the start value is
                                 // inclusive but the end value is exclusive
for let elem in series {
    print("${elem}");        // print "123456789"
}

println("${series}"); // print "[1, 2, 3, 4, 5, 6, 7, 8, 9]"
```

<span class="caption">Listing 2-7: An `intseq` turns into an array</span>

An integer sequence expression does not create a vector of integers by itself. Instead, it creates a new object that is ready
to produce integers that are in the range specified in the expression. Note in Listing 2-5 that the intseq expression initializes
an array. This is a unique feature in Expresso. As far as I know, all the other programming languages don't support it (except for Haskell).
Of course, you can initialize a vector if you write it as `[1..10, ...];` instead.
Unlike the LINQ operations(the methods defined on the `System.Linq.Enumerable` class), it is __eager__, meaning that objects will be created
at where the expression is written.

```expresso
import System.Linq.Enumerable as Enumerable;
import System.Collections.Generic.IEnumerable<> as IEnumerable;

def main()
{
    let negative_seq (- int[] = [-10..0];
    let to_negative (- int[] = [0..-10:-1];         // The compiler checks whether it is correct. That means that if you wrote it as `-10..0:-1`,
    println("Legend: (f(x) = x - 10, g(x) = -x)");  // it would issue a warning that tells you it doesn't seem to be correct.
                                                    // Note that if you put a variable in any of the operands, the compiler won't check its validity.
    for let (x, to_n) in Enumerable.Zip(negative_seq as IEnumerable<int>, to_negative as IEnumerable<int>, |l, r| (l, r)) {
        print("(f(x), g(x)) = (${x}, ${to_n}),");   // print "(f(x), g(x)) = (-10, 0),(f(x), g(x)) = (-9, -1)" and so on
        if x == to_n {                              // and when it reaches (-5, -5), it also prints "Crossed over!"
            println("Crossed over!");
        }
    }
}
```

<span class="caption">Listing 2-8: Two graphs crossed over</span>

We call such objects iterators because they iterate through a sequence-like object and yields an element at a time.
It's very useful and it's one of the reasons that gives Expresso the power of expressiveness.
An integer sequence expression can take negative values in any of its operands as long as they fit in the range of
the built-in `int` type(which corresponds to [-2<sup>31</sup>, 2<sup>31</sup> - 1]).
You may notice that the integer sequence expression looks like something, say, the conditional operator. And yes, that's right! 
In Expresso, we have 2 types of ternary operators. One is the conditional operator(often just called *the ternary operator*
because most programming languages does have only one ternary operator) and the other is the integer sequence operator we have just introduced.

In Listing 2-6, just ignore the 2 lines at the beginning. We'll come back later on this topic. Do you see that we explicitly cast the `array`s to `IEnumerable<int>`.
This is mandatory for now. Expresso doesn't allow any implicit casts right now. But it doesn't mean that we won't support it forever. It's just that Expresso isn't
currently smart enough to support it.

> ### The Compiler Validity Check
> For reference, try writing the above code as `0..-10`. The compiler should warn you like this.
>
> ```
> Error ES4004: Although `step` is positive, `start` is larger than `end`.
> Did you mean -10..0:1?
> ```
>
> This check will run only if you put literal values on all of the operands, otherwise, it doesn't check its validity due to technical difficulty.

### The `slice` Type

So far, you may be sick of the tiring and boring explanations. But when combined with sequence types such as `array`s or `vector`s,
the `intseq` type reveals its funny yet powerful potential out to the public.

```expresso
import System.Linq.Enumerable as Enumerable;
import System.Collections.Generic.IEnumerable<> as IEnumerable;

def main()
{
    let some_array (- int[] = [0..10];
    let first_half (- slice<int[], int> = some_array[0..5];
    let second_half (- slice<int[], int> = some_array[5..10];
    for let (a, b) in Enumerable.Zip(first_half as IEnumerable<int>, second_half as IEnumerable<int>, |l, r| (l, r)) {
        print("(${a}, ${b}),");   // print "(0, 5),(1, 6),(2, 7)" and so on
    }
}
```

<span class="caption">Listing 2-9: Spliting into 2 slices</span>

Once again ignoring the first 2 lines, in the above example, it seems that the latter 2 `intseq` objects extract elements from the same `array` object.
You may be wondering that it is inefficient because it seems that we have 3 arrays in the end. Having 3 arrays means that
Expresso first allocates 3 chunks of memory and fills the first chunk of memory with integers from 0 to 10, and then it copies
the first half of elements of the previous array to the second chunk of memory and the second half of elements to the last chunk of memory.

But Expresso is smart enough to solve this problem. Instead of returning a new array, it returns iterators that go through
the first half of the elements and the second half of the elements respectively.
Note that the chunks of memory(`first_half` and `second_half` let bindings in this snippet) are called `slice` in Expresso and that `slice` is another iterator
(in .NET term it is also called an enumerator).

Sometimes, it's useful to view into some sequence. That's the time when the `slice` type comes into play. The `slice` type, as the name implies,
slices some sequence and allows you to view a portion of that sequence. Combining some sequence with an `intseq` using the indexer syntax
creates a new `slice` object. The slice object, then, can be used to iterate through some portion of the sequence.
Note that the `slice` is just an iterator of the sequence. Thus the `slice` object doesn't create a new copy of the original sequence.

If you are familiar with C#, you might notice that `slice` is the same as the `Span<T>` struct. And that's right. The job that the `slice` type is intended to do is the
same as that of the `Span<T>` struct. The differences are that the `slice` type is a class and that the `slice` type might be slower than the `Span<T>` struct.

OK, so far so good. We've explained the very basics of builtin types. Next up is exponentiation. But we'll be doing it in a slightly different way.
Even though Expresso has the operator for it, here we'll be doing it on our own, using `while` loop.