﻿## Using .NET as the Standard Library

Expresso runs on the CLR. That means that you can import .NET types and use them as if they were defined in Expresso. As an example, let's create a program that
writes some text into a file.

```expresso
module main;


import System.IO.{File, FileStream} as {File, FileStream};
import System.Text.UTF8Encoding as UTF8Encoding;


def main()
{
    var writer (- FileStream;
    try{
        writer = File.OpenWrite("./some_text.txt");
        let bytes (- byte[] = UTF8Encoding{encoderShouldEmitUTF8Identifier: true}.GetBytes("This is to test writing a file");
        writer.Write(bytes, 0, bytes.Length);
    }
    finally{
        if writer != null {
            writer.Dispose();
        }
    }
}
```

<span class="caption">Listing 11-1: Using the `FileStream` class defined in .NET to write some text into a file.</span>

Let's break down the code in Listing 11-1. First we need to import types from .NET that have a file handle and convert strings into another encoding.
So the import statements.
In `main` function, we declare a new variable that will hold a stream of a file and create and open a new file. And then we create a `UTF8Encoding` that converts strings
into UTF-8 because strings in Expresso are encoded in UTF-16. We get a string as a byte sequence using the `UTF8Encoding.GetBytes` method.
Then we write the byte sequence to the file that we created just a short while ago. Finally, we check that the file is successfully created, and if so we dispose of
the file stream.

### The `null` Literal

First, look at this code. 

```expresso
var some_instance (- SomeExpressoType = null;
```

<span class="caption">Listing 11-2: The use of `null`, which won't compile</span>

What do you think of the above code? Ugh, the null literal! It's the null literal! Agh! You respond?
I know, I know that you hate it. But fortunately, or maybe unfortunately, you usually don't see `null` in Expresso in source codes.
This is because Expresso doesn't allow `null`, but there are places where `null` is allowed, which is unfortunate to most people.
That's in contexts that involve foreign codes, including .NET.
In Listing 11-1, we use `null` in `if writer != null` to check whether `writer` is successfully created and then we dispose of `writer` if it is created.
Because `writer` is an instance of `FileStream` and `FileStream` is defined in .NET, we can use `null`. Other use cases include call expressions to foreign functions.
In any other contexts, you can't use `null`. 

Returning to Listing 11-2, it is intended to initialize an instance variable, but it won't compile because `SomeExpressoType` is a type defined in Expresso and
therefore the use of `null` is prohibited in that context. In such a case, use the `Option<T>` type defined in the Expresso's standard library.

### About the `Option<T>` Type

Let's introduce the `Option<T>` type. It represents whether there is some value or not. So, typically you use it instead of the `null` literal. The advantage of using
the `Option<T>` type is that with the `match` construct, you will be always sure that you are handling both the `Some` case and the `None` case. By constrast, with
`null`, you may misuse it.

Let's see how to use it!

```expresso
import System.Console as Console;
import std_expresso::Option<> from "std.exs" as Option;

def readAbc(str (- string) -> Option<string>
{
    if str.Equals("abc") {
        return Option::Some<string>{0: "abc"};
    }else{
        return Option::None<string>{};
    }
}

def main()
{
    var option (- Option<string> = Option::None<string>{};
    var line (- string = Console.ReadLine();
    while !line.Equals("quit") {
        option = readAbc(line);
        match option {
            Option::Some{_} => break;,
            Option::None{} => line = Console.ReadLine();
        }
    }

    if option.isSome() {
        println("${option.unwrap()}");
    }
}
```

<span class="caption">Listing 11-3: Using the `Option<T>` type</span>

This program will prompt you to enter a string at a time, exit the loop when the string is either *abc* or *quit*, and print the string if the string is *abc*. 

