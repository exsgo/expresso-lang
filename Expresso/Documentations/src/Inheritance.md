﻿## Classes and Inheritance

Classes are not so useful on their own. Yes, it's true that having names in fields is useful by itself, but that's that. Names are not important to computers and it could 
be the same to few people. In this section, we'll look at class inheritance, which, I think, is the major power source of object-oriented languages. Sometimes, inheritance
could make programs too complicated, but generally it is useful and there are even some programs you can only achieve by using inheritance. Inheritance would now be
regarded as unnecessary and out of favor, but the most of the mainstream languages support it. So we support it, too.

### Syntax

We've already seen the syntax. We will reproduce the relevant codes here.

```expresso
interface HasArea
{
    def calculateArea() -> double;
}

class Circle : HasArea
{
    // content omitted
}

class Rectangle : HasArea
{
    // content omitted
}

def main()
{
    var has_area = Circle{x: 0, y: 0: radius: 10} as HasArea;
    println("Circle: ${has_area.calculateArea()}");
    has_area = Rectangle{x: 0, y: 0, width: 5, height: 5};
    println("Rectangle: ${has_area.calculateArea()}");
}
```

<span class="caption">Listing 4-9: Full code of inheritance</span>

The syntax is the same as that of most of the other programming languages. In other words, you list the names of the types that you are defining and that it will be
derived from.

> ### Note about the Inheritance Tree List
>
> Though this is a current implementation restriction, you have to write the parent class in the last position of the inheritance tree, otherwise it won't be regarded as 
> the parent type. Unlike some languages, Expresso doesn't allow multiple inheritance, meaning that you can't specify more than 1 parent class.
>
> So if we had a `Shape` class, the `Circle` class would look like this.
>
> ```expresso
> class Circle : HasArea, Shape
> ```
>
> <span class="caption">Listing 4-10: A hypothetical `Circle` class definition</span>
>
> This couldn't be `class Circle : Shape, HasArea`. If that happened, Expresso wouldn't recognize that `Shape` is a parent class, which would result in an error.

### Behavior

The code in Listing 4-9 would behave like the following.

1. Declares a new variable called `has_area` and initializes it with a new `Circle` instance casted to `HasArea`.
2. `println`s the area of the `Circle` instance.
3. Re-initializes `has_area` with a new `Rectangle` instance casted to `HasArea`.
4. Does the same as 2. for the `Rectangle` instance.

If you are familiar with object-oriented programming, you should easily expect this behavior. That's just that.

### How to Use?

First of all and most importantly, you can use this behavior for abstracting away object use: using another instance of other classes for other behavior with
a variable of the same name. With this behavior, you can easily add a new class with the same interface and a different behavior. This is useful when the interface for
a class is concrete, but the functionality isn't known or will be changed. If the functionality is determined to be changed, you will leave the existing implementation
as is and add a new class that implements the same interface.
