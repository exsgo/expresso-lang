﻿## Functions

Speaking of methods, we haven't yet talked about what functions are. Functions are common in Expresso and useful considering the power of them. It can create abstractions
over a sequence of operations.

```expresso
def isZero(n (- int) -> bool
{
    return n == 0;
}
```

<span class="caption">Listing 5-1: An example of a function</span>

Look at the above code. This function determines whether the input is equal to 0. From now on, we can call this function to determine whether some integer is 0.
Abstractions are useful when there are multiple places that call the function. Imagine that we have the following code.

```expresso
let n = 1;
if n == 0 {
    println("${n == 0}");
}else{
    println("otherwise");
}
```

<span class="caption">Listing 5-2: Several uses of the same expression</span>

In this little example, we have only two places that has the same expression `n == 0`, but you will see the advantage in a moment. Say you need to change
the implementation.

```expresso
let n = 1;
if n == 1 {
    println("${n == 1}");
}else{
    println("otherwise");
}
```

<span class="caption">Listing 5-3: A modified implementation</span>

In Listing 5-2, you need to change two places. But if you created the function, you would need to change one place in the function(and possibly the function name). 

> #### On `main` Function in More Detail
>
> So far, you've seen that the `main` function is the entry point for a program. Like C#, you can receive parameters in it.
>
> ```expresso
> def main(args (- string[]) -> ()
> ```
>
> <span class="caption">Listing 5-4: The `main` function definition with a parameter</span>
>
> The type of the parameter is `string[]`. That is, an `array` of `string`s. Note that the mono runtime cuts out the first two parameters, which means that you can't
> get the program name in an Expresso program. So for example, if you pass `mono main.exe parameter1 parameter2`, you will only get `parameter1 parameter2` in your
> program.
>
> Unlike C++ or others, the `main` function can't return an `int`. If you do that, the runtime will issue an error.
>
> Note that it returns `()`, which represents an empty tuple. In Expresso, this means the `void` type, and `void` is simply an alias of `()`.

### Parameters and the Return Type

As shown in the above code, functions (and methods) can have parameters and the return type. Parameters are listed in the parentheses with comma separated, 
and the return type follows after `->`. The return type can be omitted and will be inferred from the body. Parameters can also be omitted if they have optional values.
This is a deliberate choice in language design.

We could make it so that we could always omit parameter types. But that way it would take longer to compile because we would need to infer the parameter types from
the function body.
We could make it so that we have to always explicitly write the parameter types. But then it would be less convenient to programmers.
Everything is in trade-off and we need to make decisions at somewhere. You will see other instances of choice in language design in this tutorial. Keep in mind that we,
especially I, are focusing on taking balance between ergonomics and performance where suits me. I hope it would suit you, too.

> #### Vs. Mathematical Functions
>
> Speaking of functions, you might have imagined the ones in mathematics. Yes, those two have common characteristics but also have a differenece: states.
> Functions in programming can have states, meaning that even if you give a function the same input, it doesn't always return the same output while mathematical ones
> and ones in functional programming always do.
