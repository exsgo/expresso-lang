﻿## String Interpolation and Raw Strings

> ### What Is Strings?
> Strings would often be an obstacle for beginners or even for intermediate programmers that have Dynamically Typed Language background where you wouldn't need to put
> so much thought into using them because of the differences between how computers treat letters and how humans interpret them. Although Expresso is rather one of
> such programming languages, we'll briefly talk about them here.
>
> Fundamentally, computers can only treat 0's and 1's. They see any data that they treat as sequence of 0's and 1's, including numbers, letters, even movies, and audio.
> In short, strings are represented as a sequence of code points. For example, you would determine that code point 0 represents 'a', code point 1 'b' and so forth.
> As such, every letter is given a code point, and then computers can treat textual data. Therefore, what they see is only code points. But wait. They can find
> substrings and replace them. How do they achieve that?
>
> Simply speaking, they search for substrings by comparing code points and replace strings by searching and replacing old code points with new ones. During that
> process, they will never recognize the meanings of the strings.
>
> Let's see together how they achieve it. 
>
> ```expresso
> let str (- string = "Hello";
> let replaced (- string = str.Replace("Hello", "Good afternoon");
> println("${replaced}");
> ```
>
> The above code will print `Good afternoon`.
>
> What about "F\*cking machine, give me a greeting"? 
> 
> ```expresso
> let str (- string = "F*cking machine, give me a greeting";
> let replaced (- string = str.Replace("F*cking machine", "Good boy");
> println("${replaced}");
> ```
>
> Even then, they will just do their job and print `Good boy, give me a greeting`, meaning they don't care what it means. Good boy/girl.
>
> But if I tell it to you, then you may be angry. Oh, don't punch me in the face...

While programming, you often want to output some values to the console. Imagine the following situation.

```expresso
module main;

import System.Console as Console;

def main()
{
    let number (- string = Console.ReadLine();
    println();  // you want to output `number` here
}
```

<span class="caption">Listing 9-1: A situation where you want to output some value</span>

How can we do that? One way to do that is as follows.

```expresso
Console.WriteLine("You entered {0}", number);
```

<span class="caption">Listing 9-2: A solution with `Console.WriteLine`</span>

By using the `Console.WriteLine` method instead of `println`. Yes, that's enough but it is somewhat inconvinient because you need to first import the `Console` class.
It is also obscure because the string is separated from the variable name. With that in mind, we have a clear and concise tool in the toolbox: the string interpolation.

```expresso
println("You entered ${number}");
```

<span class="caption">Listing 9-3: Using string interpolation</span>

With string interpolation, you don't need to import additional classes and the variable name is now part of the string. Would it be clearer and more concise than 
the previous solution?
In addition, you can truly use any expressions in placeholders.

```expresso
let ary (- int[] = [1, 1, 2, 3, 5, 8];
println("The 6th fibonacci number is ${ary[5]}");
```

<span class="caption">Listing 9-4: An advanced use of string interpolation</span>

As you can see in Listing 4-7 and 9-4, you can put any arbitrary expressions in placeholders.

```expresso
enum SomeEnum
{
    A(int)
}

def main()
{
    let some_enum = SomeEnum::A{0: 10};
    match some_enum {
        SomeEnum::A{value} => println("some_enum = SomeEnum::A{{${value}}}");
    }
}
```

<span class="caption">Listing9-5: Printing curly braces</span>

In order to print the dollar sign(`$`) and curly braces(`{` and `}`), you have to escape them by doubling them.

### Raw Strings

Raw strings are useful when you need a multi-line string.

```expresso
let str (- string = r"This is a string.
This line is also a part of the string.";

println("${str}");
```

<span class="caption">Listing 9-6: A simple raw string</span>

This code prints the string as is. That's why they are called *raw* strings.
