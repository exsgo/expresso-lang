﻿## Interfaces and Classes

### Classes

Like the `tuple` type, which was discussed previously, classes are another way to structure up multiple values into one type. The difference is that with classes you will name
each field so you will recognize them easily.

Tuples are commonly used to return multiple values from functions. Because those values are immediately destructured into separate variables, we don't need names on fields.
It would be rather verbose.
Classes, on the other hand, are used for longer-lived objects. Here, we will consider a `Circle` class, which represents a circle.

```expresso
class Circle
{
    let x (- int, y (- int, radius (- int;
}
```

<span class="caption">Listing 4-1: A `Circle` class definition</span>

As you may imagine, this class has `x`, `y` coordinates and the `radius`. You can represent a circle using this class like so.

```expresso
let circle_a = Circle{x: 0, y: 0, radius: 10};
```

<span class="caption">Listing 4-2: An object of the `Circle` class</span>

As you can see, instances are created without the `new` keyword. In Expresso, `new` isn't a keyword. Note that we are using curly brackets to specify the initial values.

> ### Constructor and Its Parameters
>
> In Expresso, you can't define constructors explicitly. Instead, Expresso will automatically generate a constructor using all the field names and their types. Take
> a look at the object creation again. It has field names in it. Now it does matter to the compiler. When Expresso inspects an object creation expression, it will
> look at the types and the names, too. And the order in which fields are listed is not significant to the compiler so you can name them in arbitrary orders. Therefore,
> writing `Circle{y: 0, x: 5, radius: 10}` is equivalent to writing `Circle{x: 5, y: 0, radius: 10}`. In other words, field names will be inspected AND respected, too.
>
> Note that this is not true for constructors of foreign types. It will still only look at the order in which arguments are specified and type mismatches will be
> reported.

Let's add a method that determines whether another circle touches this one.

```expresso
class Circle
{
    // fields omitted

    public def touchesCirlce(other (- Circle)
    {
        return (other.x - self.x) ** 2 + (other.y - self.y) ** 2 <= (other.radius + self.radius) ** 2;
    }
}
```

<span class="caption">Listing 4-3: A method that determines whether the circle touches another circle</span>

This method calculates the distance between the center points of the circles and compares it to the sum of the radii of the circles. Note that we add the `self` keyword
to each of the fields. This is mandatory. Although there is no real uses here, this is true for methods. So if you needed to call this method in another method,
you would have to write `self.touchesCircle(other);` rather than `touchesCircle(other);`.

> ### The `self` Keyword
>
> In C++ or other C-like languages, `this` is a keyword and can be omitted. In Rust, which is said to be and seems like a descendant of C++, `self` is also a keyword
> but treated rather like a plain parameter.
>
> In Expresso, `self` is a keyword, implicit and can never be omitted. If you forget to put the `self` keyword before any members that belong to the `self` object,
> Expresso will issue the following error.
>
> ```text
> Error ES2300: Missing `self`!
> ```
>

Let's add another shape, a rectangle.

```expresso
class Rectangle
{
    let x (- int, y (- int, width (- int, height (- int;
}
```

<span class="caption">Listing 4-4: A `Rectangle` class definition</span>

This class should also be self-explanatory. It has the `x`, `y` coordinates and the `width` and the `height`.
Then let's say we need to calculate the area of the two shapes. Of course, we could just add another method that does that, but there's something that prevents you
from doing so.
You can calculate the area for both the shapes. Would it be nice if we can abstract it away and make it a common interface? So we'll introduce the `interface`.

### Interfaces

First, look at the definitions.

```expresso
interface HasArea
{
    def calculateArea() -> double;
}
```

<span class="caption">Listing 4-5: A `HasArea` interface definition</span>

This interface declares what a class that implements the interface should have and what is declared here is a method named `calculateArea` and that has the signature
that has no parameters and returns a `double`.
Then you can implement it on the concrete classes like this.

```expresso
import System.Math as Math;


class Circle : HasArea
{
    // fields omitted...
    
    public def calculateArea() -> double
    {
        return 2.0 * Math.PI * ((self.radius * self.radius) as double);
    }
}

class Rectangle : HasArea
{
    // fields omitted...
    
    public def calculateArea() -> double
    {
        return (self.width * self.height) as double;
    }
}
```

<span class="caption">Listing 4-6: Classes that implement the interface</span>

This way, we can treat an object of the `Circle` class as the `HasArea` interface.

```expresso
def main()
{
    let has_area (- HasArea = Circle{x: 0, y: 0, radius: 10} as HasArea;
    println("Circle: ${has_area.calculateArea()}");
}
```

<span class="caption">Listing 4-7: Treating an object of the `Circle` class as the `HasArea` interface</span>

In a similar way, we can also treat the `Rectangle` class as `HasArea` interface like so.

```expresso
has_area = Rectangle{x: 0, y: 0, width: 5, height: 5} as HasArea;
println("Rectangle: ${has_area.calculateArea()}");
```

<span class="caption">Listing 4-8: Using the same variable for a `Rectangle`</span>

What is important in this example is that you can use the same variable for two distinct instances, meaning that we've abstracted away the use of the instances.
Although this example doesn't fully illustrate the advantage of it, you will be happy someday that you did this.

Interfaces are useful when you want to create abstractions over methods.
Oh, we've forgotten to mention that we've called a method in Listing 4-7 and 4-8. Methods can be called if you write an object, a dot, the method name and a pair of
parentheses as in the Listings.

> ### Functions and Methods
> Do you notice the difference between functions and methods? In Expresso, subroutines that live in classes are called methods and those in modules are called
> functions. In other words, methods take a `self` object(which is implicit) as its first parameter, and functions don't.