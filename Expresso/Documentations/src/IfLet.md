﻿### Concise Control Flow with `if let`

Let's look at the following code.

```expresso
let option = Option::Some<int>{0: 10};
match option {
    Option::Some{10} => println("10");,
    _ => ;
}
```

<span class="caption">Listing 6-7: A `match` statement that only cares about `Option::Some(10)` value</span>

In this code, we are only interested in one variant, which is `Option::Some`, and the value is 10. In this situation, although we haven't yet talked about the `match`
statement, it's rather redundant because `match` statements must be complete about variants. In other words, to satisfy the `match` statement, we have to add `_ => ;`
after processing just one variant, which is a lot of boilerplate code to add.

Instead, we could write this in a shorter way using `if let`. The following code functions equally to the code in Listing 6-7.

```expresso
if let Option::Some{10} = option {
    println("10");
}
```

The `if let` construct takes a pattern and an expression separated by an equal sign. It works the same way as a `match`, where the expression is given to the `match`
and the pattern is its first arm.

Using `if let` means less typing, less indentation and less boilerplate code. However, you lose the exaustive checking that `match` enforces. Choosing between `match`
and `if let` depends on what you're doing in your particular situation and whether gaining conciseness is an appropriate trade-off for losing exaustive checking.

In other words, you can think of `if let` as syntax sugar for a `match` that runs code when the value matches one pattern and then ignores all other values.

Futhermore, you can include an `else` with an `if let`. The block of code that goes with the `else` is the same as the block of code that would go with the `_` case
in the `match` statement that is equivalent to the `if let` and `else`.

So if you wanted to print `otherwise` in any other cases in Listing 6-7, you could do that with a `match` statement like this:

```expresso
let option = Option::Some<int>{0: 10};
match option {
    Option::Some{10} => println("10");,
    _ => println("otherwise");
}
```

Or you could do that with an `if let` and `else` statement like this:

```expresso
let option = Option::Some<int>{0: 10};
if let Option::Some{10} = option {
    println("10");
}else{
    println("otherwise");
}
```

If you have a situation in which your program is too verbose to express using a `match`, remember that `if let` is in your Expresso toolbox as well.
