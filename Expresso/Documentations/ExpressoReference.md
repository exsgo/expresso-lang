# The reference for Expresso

## Basic concepts and terms

First of all, I will define basic concepts and terms used throughout the reference.

### LValues, RValues

Roughly speaking, a lvalue is a variable that can be assigned and referenced, while a rvalue is a value, meaning results of expressions.

### Equality and equivalence

Assume you would compare some objects to some other objects. How would you determine to say they are the same things?
In the real world, we sometimes focus only on names or the kinds of things. 

## Pattern matching

Pattern matching, which maybe sounds unfamiliar to those who are from imperative languages, is a powerful yet
comprehensive tool considering its expressive-ness.

## References

Like the C or C++ language, Expresso has the pointer type. It is called a reference in Expresso, and its functionality
and semantics are very close to that of references in C++. More precisely, a reference always refers to a valid memory
location so Expresso has no null literal.

## Built-in types

Like most other programming languages, Expresso has a lot of useful built-in types. Among them are included primitive
types like `int`, `bool`, etc. and complex types like `tuple`, `vector`, `dictionary` and types that need special care
like `unit`, `string` etc. Most of them also exist in other major programming languages, so it's a good bet that
you are already familiar with the concepts.

### Primitive types

Expresso has several primitive types. Namely `bool` 

### Arrays

### Tuples, vectors and dictionaries

Expresso supports 3 built-in data structures; tuples, vectors and dictionaries.
Tuples are alike to arrays except that they can contain different types of objects in it.

```Expresso
    let some_tuple = (1, true, "We can conbine different types of things in one tuple");
    println(some_tuple);    // print "(1,true,"We can conbine different types of things in one tuple")"
```

Vectors are also alike to arrays, but they can contain any number of elements in it if you desire and if your buddy allows,
of course.(I mean, if you have enough memory. Don't take me wrong...)

```Expresso
    let some_vector = [1, 2, 3, 4, ...];
    println(some_vector);   // print "[1,2,3,4...]"
```

Unlike the previous 2 data structures, dictionaries introduce a bit different story because they should have 2 values
for each of their elements.

One is called "key" and the other "value". A key, like indeices in arrays or vectors, refers to a specific element.
And the element referred to by the key is the value. In other words, we can say that arrays and vectors are the kind of
dictionaries that use indecies as keys. Most of the times, however, we distinguish them because they are based on
different concepts and are used in different situations.

```Expresso
    let some_dict = {
        "akari" : "kawakawa",
        "chinatsu" : "2424",
        "kyoko" : "...",
        "yui" : "..."
    };
    println(some_dict);     // print "{"akari" : "kawakawa", "chinatsu" : "2424", "kyoko" : "...", "yui" : "..."}"
```

Often tuples, vectors and dictionaries are called "containers" because they contain or store data in them and keep them
inside for later computation. In Expresso term, we also call those containers "sequence" in similarity to `intseq` type
standing for integer sequence.

#### Tips

Let's talk about those data structures more deeply. Imagine if you should create a new vector class of your own,
what would you do? How would you design it? 

In general, those data structures often consist of 2 layers. One is the "header" layer and the other is the "data" layer.
Each layer corresponds to one class so a container type usually has two classes as its component. The header class stores
metadata on the data it holds like the number of elements and where the real data reside(as a pointer). The data layer
literally holds data(usually as a node). 

### Strings

Strings are always a big concern. Maybe most of the programmers coming from the scripting language background don't
really care about them but you should keep several points in mind when you are manipulating strings in Expresso.
First, a string is essentialy an array of characters. So if you want to compare bare strings then the time taken to do it
is linear to the number of elements (O(N) in mathematical term). If you are concerned about only the equivalence, not the equality,
you should consider using reference comparison, that is, apply the equal operator on the `&string` type.

### Iterators

As I promised just a short period of time ago, let's take a closer look at iterators.
As I've mentioned, `intseq` type itself is an iterator so take this as an example.

### .NET specific Runtime environment

Our primary goal of teaching beginners the basics of programming leads us to the .NET environment.

