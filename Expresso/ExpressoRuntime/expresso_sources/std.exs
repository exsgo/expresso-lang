module std_expresso;


import System.Exception as Exception;

export enum Option<T>
{
    Some(T),
    None()

    public def isSome() -> bool
    {
        match self {
            Option::Some{_} => return true;,
            Option::None{} => return false;
        }
    }

    public def isNone() -> bool
    {
        match self {
            Option::Some{_} => return false;,
            Option::None{} => return true;
        }
    }

    public def expect(msg (- string) -> T
    {
        match self {
            Option::Some{value} => return value;,
            Option::None{} => throw Exception{message: msg};
        }
    }

    public def unwrap() -> T
    {
        match self {
            Option::Some{value} => return value;,
            Option::None{} => throw Exception{message: "unwrap() throws an exception!"};
        }
    }

    public def unwrapOr(default (- T) -> T
    {
        match self {
            Option::Some{value} => return value;,
            Option::None{} => return default;
        }
    }

    public def unwrapOrElse(closure (- || -> T) -> T
    {
        match self {
            Option::Some{value} => return value;,
            Option::None{} => return closure();
        }
    }

    public def map<U>(f (- |T| -> U) -> Option<U>
    {
        match self {
            Option::Some{value} => return Option::Some<U>{0: f(value)};,
            Option::None{} => return Option::None<U>{};
        }
    }

    public def mapOr<U>(default (- U, f (- |T| -> U) -> U
    {
        match self {
            Option::Some{value} => return f(value);,
            Option::None{} => return default;
        }
    }

    public def mapOrElse<U>(closure (- || -> U, f (- |T| -> U) -> U
    {
        match self {
            Option::Some{value} => return f(value);,
            Option::None{} => return closure();
        }
    }

    public def okOr<E>(error (- E) -> Result<T, E>
    {
        match self {
            Option::Some{value} => return Result::Ok<T, E>{0: value};,
            Option::None{} => return Result::Error<T, E>{0: error};
        }
    }

    public def okOrElse<E>(errorClosure (- || -> E) -> Result<T, E>
    {
        match self {
            Option::Some{value} => return Result::Ok<T, E>{0: value};,
            Option::None{} => return Result::Error<T, E>{0: errorClosure()};
        }
    }

    public def and<U>(b (- Option<U>) -> Option<U>
    {
        match self {
            Option::Some{_} => return b;,
            Option::None{} => return Option::None<U>{};
        }
    }

    public def andThen<U>(f (- |T| -> U) -> Option<U>
    {
        match self {
            Option::Some{value} => return Option::Some<U>{0: f(value)};,
            Option::None{} => return Option::None<U>{};
        }
    }

    public def filter(predicate (- |T| -> bool) -> Option<T>
    {
        match self {
            Option::Some{value} => {
                if predicate(value) {
                    return Option::Some<T>{0: value};
                }
            },
            _ => ;
        }

        return Option::None<T>{};
    }

    public def or(b (- Option<T>) -> Option<T>
    {
        match self {
            Option::Some{_} => return self;,
            Option::None{} => return b;
        }
    }

    public def orElse(f (- || -> T) -> Option<T>
    {
        match self {
            Option::Some{_} => return self;,
            Option::None{} => return Option::Some<T>{0: f()};
        }
    }

    public def xor(b (- Option<T>) -> Option<T>
    {
        match self {
            Option::Some{a} => {
                match b {
                    Option::Some{_} => return Option::None<T>{};,
                    Option::None{} => return Option::Some<T>{0: a};
                }
            },
            Option::None{} => {
                match b {
                    Option::Some{b} => return Option::Some<T>{0: b};,
                    Option::None{} => return Option::None<T>{};
                }
            }
        }
    }

    // We can't currently make these methods work as expected
    /*public mutating def take() -> Option<T>
    {
        match self {
            Option::Some{value} => {
                self = Option::None<T>{};
                return self;
            },
            _ => return Option::None<T>{};
        }
    }

    public mutating def replace(value (- T) -> Option<T>
    {
        match self {
            _ => {
                self = Option::Some<T>{0: value};
                return self;
            }
        }
    }*/
}

export enum Result<T, E>
{
    Ok(T),
    Error(E)

    public def isOk() -> bool
    {
        match self {
            Result::Ok{_} => return true;,
            Result::Error{_} => return false;
        }
    }

    public def isError() -> bool
    {
        match self {
            Result::Ok{_} => return false;,
            Result::Error{_} => return true;
        }
    }

    public def ok() -> Option<T>
    {
        match self {
            Result::Ok{value} => return Option::Some<T>{0: value};,
            Result::Error{_} => return Option::None<T>{};
        }
    }

    public def error() -> Option<E>
    {
        match self {
            Result::Error{error} => return Option::Some<E>{0: error};,
            Result::Ok{_} => return Option::None<E>{};
        }
    }

    public def map<U>(f (- |T| -> U) -> Result<U, E>
    {
        match self {
            Result::Ok{value} => return Result::Ok<U, E>{0: f(value)};,
            Result::Error{error} => return Result::Error<U, E>{0: error};
        }
    }

    public def mapOrElse<U>(fallback (- |E| -> U, mapper (- |T| -> U) -> U
    {
        return self.map(mapper).unwrapOr(fallback);
    }

    public def mapError<F>(op (- |E| -> F) -> Result<T, F>
    {
        match self {
            Result::Ok{value} => return Result::Ok<T, F>{0: value};,
            Result::Error{error} => return Result::Error<T, F>{0: op(error)};
        }
    }

    public def and<U>(result (- Result<U, E>) -> Result<U, E>
    {
        match self {
            Result::Ok{_} => return result;,
            Result::Error{error} => return Result::Error<U, E>{0: error};
        }
    }

    public def andThen<U>(op (- |T| -> U) -> Result<U, E>
    {
        match self {
            Result::Ok{value} => return Result::Ok<U, E>{0: op(value)};,
            Result::Error{error} => return Result::Error<U, E>{0: error};
        }
    }

    public def or<F>(result (- Result<T, F>) -> Result<T, F>
    {
        match self {
            Result::Ok{value} => return Result::Ok<T, F>{0: value};,
            Result::Error{_} => return result;
        }
    }

    public def orElse<F>(op (- |E| -> F) -> Result<T, F>
    {
        match self {
            Result::Ok{value} => return Result::Ok<T, F>{0: value};,
            Result::Error{error} => return Result::Error<T, F>{0: op(error)};
        }
    }

    public def unwrapOr(b (- T) -> T
    {
        match self {
            Result::Ok{value} => return value;,
            Result::Error{_} => return b;
        }
    }

    public def unwrapOrElse(op (- |E| -> T) -> T
    {
        match self {
            Result::Ok{value} => return value;,
            Result::Error{error} => return op(error);
        }
    }
}