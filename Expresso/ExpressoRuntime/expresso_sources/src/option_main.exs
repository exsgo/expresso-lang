module main;


import std_expresso::{Option<>, Result<,>} from "../std.exs" as {Option, Result};

def main()
{
    let option_a = Option::Some<int>{0: 10};
    let content = option_a.unwrap();
    println("${content}");

    let or = option_a.or(Option::None<int>{});
    println("${or}");

    let taken = option_a.take();
    println("${option_a.isSome()}");
    println("${option_a.isNone()}");
    println("${taken}");

    let option_b = Option::Some<int>{0: 10};
    let replaced = option_b.replace(20);
    println("${option_b.unwrap()}");
    println("${replaced.unwrap()}");
}