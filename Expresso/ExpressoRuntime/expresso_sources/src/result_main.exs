module main;


import std_expresso::{Option<>, Result<,>} from "../std.exs" as {Option, Result};

def main()
{
    let result = Result::Ok<int, string>{0: 10};
    println("${result.isOk()}");
    println("${result.isError()}");

    let option = result.ok();
    println("${option.unwrap()}");

    let unwrap = result.unwrapOr(20);
    println("${unwrap}");
}