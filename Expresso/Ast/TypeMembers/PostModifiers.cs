﻿using System.Collections.Generic;
using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.Ast
{
    /// <summary>
    /// Represents the modifiers that should be put between the method signature and the body.
    /// </summary>
    public class PostModifiers : AstNode
    {
        public static readonly Role<Identifier> BeforehandIdentifierRole = new Role<Identifier>("BeforehandIdentifier", Identifier.Null);
        //public static readonly Role<Identifier> AfterwardsIdentifierRole = new Role<Identifier>("AfterwardsIdentifier", Identifier.Null);
        public static readonly Role<Identifier> FriendIdentifierRole = new Role<Identifier>("FriendIdentifier", Identifier.Null);
        public static readonly Role<Identifier> InIdentifierRole = new Role<Identifier>("InIdentifier", Identifier.Null);

        #region Null
        public new static readonly PostModifiers Null = new NullPostModifiers();

        sealed class NullPostModifiers : PostModifiers
        {
            public override bool IsNull => true;

            public override void AcceptWalker(IAstWalker walker) => walker.VisitNullNode(this);

            public override TResult AcceptWalker<TResult>(IAstWalker<TResult> walker) => walker.VisitNullNode(this);

            public override TResult AcceptWalker<TResult, TData>(IAstWalker<TData, TResult> walker, TData data) => walker.VisitNullNode(this, data);

            internal protected override bool DoMatch(AstNode other, Match match)
            {
                return other == null || other.IsNull;
            }
        }
        #endregion

        /// <summary>
        /// Method names that should be called before this one.
        /// </summary>
        /// <value>The requires to be called beforehand.</value>
        public AstNodeCollection<Identifier> RequiresToBeCalledBeforehand{
            get => GetChildrenByRole(BeforehandIdentifierRole);
        }

        // FIXME: Can be deleted: 2019/8/8
        // afterwards can't be comfirmed when an error happens
        /// <summary>
        /// Method names that should be called after this one.
        /// After name binding, it should be empty because "A requires B to be called afterwards" means that B requires A to be called beforehand.
        /// </summary>
        /// <value>The requires to be called afterwards.</value>
        /*public AstNodeCollection<Identifier> RequiresToBeCalledAfterwards{
            get => GetChildrenByRole(AfterwardsIdentifierRole);
        }*/

        /// <summary>
        /// Method names that should be called inside this one.
        /// It will be empty if this method belongs to a class or enum.
        /// </summary>
        /// <value>The requirement to be called inside.</value>
        public AstNodeCollection<Identifier> RequiresToBeCalledInside{
            get => GetChildrenByRole(InIdentifierRole);
        }

        /// <summary>
        /// Friend methods names.
        /// Only friend methods can access to this method.
        /// </summary>
        public AstNodeCollection<Identifier> FriendMethods{
            get => GetChildrenByRole(FriendIdentifierRole);
        }

        protected PostModifiers()
        {
        }

        public PostModifiers(IEnumerable<Identifier> beforehand, IEnumerable<Identifier> ins, IEnumerable<Identifier> friends, TextLocation startLoc
                             , TextLocation endLoc)
            : base(startLoc, endLoc)
        {
            if(friends != null)
                FriendMethods.AddRange(friends);

            if(beforehand != null)
                RequiresToBeCalledBeforehand.AddRange(beforehand);

            if(ins != null)
                RequiresToBeCalledInside.AddRange(ins);
        }

        public override NodeType NodeType => NodeType.Member;

        public override void AcceptWalker(IAstWalker walker)
        {
            walker.VisitPostModifiers(this);
        }

        public override TResult AcceptWalker<TResult>(IAstWalker<TResult> walker)
        {
            return walker.VisitPostModifiers(this);
        }

        public override TResult AcceptWalker<TResult, TData>(IAstWalker<TData, TResult> walker, TData data)
        {
            return walker.VisitPostModifiers(this, data);
        }

        protected internal override bool DoMatch(AstNode other, Match match)
        {
            var o = other as PostModifiers;
            return o != null && FriendMethods.DoMatch(o.FriendMethods, match)
                   && RequiresToBeCalledBeforehand.DoMatch(o.RequiresToBeCalledBeforehand, match)
                   && RequiresToBeCalledInside.DoMatch(o.RequiresToBeCalledInside, match);
        }
    }
}
