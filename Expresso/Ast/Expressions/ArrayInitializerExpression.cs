﻿using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.Ast
{
    /// <summary>
    /// Represents the array initializer, which allocates some memory and initializes it with default values.
    /// </summary>
    public class ArrayInitializerExpression : Expression
    {
        public static readonly Role<PrimitiveType> PrimitiveTypeRole = new Role<PrimitiveType>("PrimitiveType", PrimitiveType.Null);

        /// <summary>
        /// Represents the type path of which we will create an array.
        /// </summary>
        /// <value>The type path.</value>
        public PrimitiveType TypePath{
            get => GetChildByRole(PrimitiveTypeRole);
            set => SetChildByRole(PrimitiveTypeRole, value);
        }

        /// <summary>
        /// The expression that is evaluated to an array size.
        /// </summary>
        /// <value>The size.</value>
        public Expression SizeExpression{
            get => GetChildByRole(Roles.Expression);
            set => SetChildByRole(Roles.Expression, value);
        }

        public ArrayInitializerExpression(PrimitiveType typePath, Expression sizeExpression, TextLocation endLocation)
            : base(typePath.StartLocation, endLocation)
        {
            TypePath = typePath;
            SizeExpression = sizeExpression;
        }

        #region implemented abstract members of AstNode

        public override void AcceptWalker(IAstWalker walker)
        {
            walker.VisitArrayInitializer(this);
        }

        public override TResult AcceptWalker<TResult>(IAstWalker<TResult> walker)
        {
            return walker.VisitArrayInitializer(this);
        }

        public override TResult AcceptWalker<TResult, TData>(IAstWalker<TData, TResult> walker, TData data)
        {
            return walker.VisitArrayInitializer(this, data);
        }

        protected internal override bool DoMatch(AstNode other, Match match)
        {
            var o = other as ArrayInitializerExpression;
            return o != null && TypePath.DoMatch(o.TypePath, match) && SizeExpression.DoMatch(o.SizeExpression, match);
        }

        #endregion
    }
}
