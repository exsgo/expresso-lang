﻿using System;

namespace Expresso.Ast
{
    /// <summary>
    /// Indicates the property of a function or method parameter.
    /// </summary>
    [Flags]
    public enum FunctionParameterModifiers
    {
        None = 0x00,
        Optional = 0x01,
        Variadic = 0x01 << 1
    }
}
