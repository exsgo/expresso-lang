using System;
using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.TypeSystem;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.Ast
{
    using ExpressoTypeCode = Expresso.TypeSystem.KnownTypeCode;
    /// <summary>
    /// Represents a primitive type reference node.
    /// In Expresso, primitive types are listed as follows:
    /// <list type="primitiveTypes">
    /// <item>int</item>
    /// <item>uint</item>
    /// <item>bool</item>
    /// <item>byte</item>
    /// <item>float</item>
    /// <item>double</item>
    /// <item>bigint</item>
    /// <item>char</item>
    /// <item>tuple</item>
    /// <item>vector</item>
    /// <item>dictionary</item>
    /// <item>intseq</item>
    /// <item>slice</item>
    /// <item>array of the above types</item>
    /// </list>
    /// Note that the array, vector and dictionary types are builtin types and primitive types but we treat them as SimpleType nodes in AST.
    /// </summary>
    public class PrimitiveType : AstType
    {
        #region Null
        public static new readonly PrimitiveType Null = new NullPrimitiveType();

        sealed class NullPrimitiveType : PrimitiveType
        {
            public override bool IsNull => true;

            public override void AcceptWalker(IAstWalker walker) => walker.VisitNullNode(this);

            public override TResult AcceptWalker<TResult>(IAstWalker<TResult> walker) => walker.VisitNullNode(this);

            public override TResult AcceptWalker<TResult, TData>(IAstWalker<TData, TResult> walker, TData data) => walker.VisitNullNode(this, data);

            public override void AcceptTypeWalker(IAstTypeWalker walker) => walker.VisitNullNode(this);

            public override TResult AcceptTypeWalker<TResult>(IAstTypeWalker<TResult> walker) => walker.VisitNullNode(this);

            public override TResult AcceptTypeWalker<TData, TResult>(IAstTypeWalker<TData, TResult> walker, TData data) => walker.VisitNullNode(this, data);

            internal protected override bool DoMatch(AstNode other, Match match) => other == null || other.IsNull;
        }
        #endregion

        public ExpressoTypeCode KnownTypeCode => GetKnownTypeCodeForPrimitiveType(Keyword);

        public override string Name => Keyword;

        /// <summary>
        /// Gets the keyword.
        /// </summary>
        /// <value>The keyword.</value>
        public string Keyword { get; }

        public override Identifier IdentifierNode => Identifier.Null;

        protected PrimitiveType()
        {
        }

        public PrimitiveType(string keyword, TextLocation location)
            : base(location, new TextLocation(location.Line, location.Column + keyword.Length))
        {
            this.Keyword = keyword;
        }

        public override void AcceptWalker(IAstWalker walker)
        {
            walker.VisitPrimitiveType(this);
        }

        public override TResult AcceptWalker<TResult>(IAstWalker<TResult> walker)
        {
            return walker.VisitPrimitiveType(this);
        }

        public override TResult AcceptWalker<TResult, TData>(IAstWalker<TData, TResult> walker, TData data)
        {
            return walker.VisitPrimitiveType(this, data);
        }

        public override void AcceptTypeWalker(IAstTypeWalker walker)
        {
            walker.VisitPrimitiveType(this);
        }

        public override TResult AcceptTypeWalker<TResult>(IAstTypeWalker<TResult> walker)
        {
            return walker.VisitPrimitiveType(this);
        }

        public override TResult AcceptTypeWalker<TData, TResult>(IAstTypeWalker<TData, TResult> walker, TData data)
        {
            return walker.VisitPrimitiveType(this, data);
        }

        internal protected override bool DoMatch(AstNode other, Match match)
        {
            if(other is PlaceholderType)
                return true;
            
            var o = other as PrimitiveType;
            return o != null && Keyword == o.Keyword;
        }

        #region implemented abstract members of AstType
#if NETCOREAPP2_0
        public override ITypeReference ToTypeReference(NameLookupMode lookupMode, InterningProvider interningProvider = null)
        {
            var type_code = GetKnownTypeCodeForPrimitiveType(Keyword, null);
            if(type_code == ExpressoTypeCode.None)
                return new UnknownType(null, Keyword);
            else
                return Expresso.TypeSystem.KnownTypeReference.Get(type_code);
        }
#else
        public override ITypeReference ToTypeReference(NameLookupMode lookupMode, InterningProvider interningProvider = null)
        {
            throw new NotImplementedException();
        }
#endif

        #endregion

        /// <summary>
        /// Gets an <see cref="ExpressoTypeCode"/> instance corresponds to `keyword`.
        /// </summary>
        /// <returns>The known type code for primitive type.</returns>
        /// <param name="keyword">Keyword.</param>
        public static ExpressoTypeCode GetKnownTypeCodeForPrimitiveType(string keyword)
        {
            switch(keyword){
            case "bool":
                return ExpressoTypeCode.Bool;

            case "char":
                return ExpressoTypeCode.Char;

            case "byte":
                return ExpressoTypeCode.Byte;

            case "int":
                return ExpressoTypeCode.Int;

            case "uint":
                return ExpressoTypeCode.UInt;

            case "float":
                return ExpressoTypeCode.Float;
            
            case "double":
                return ExpressoTypeCode.Double;

            case "bigint":
                return ExpressoTypeCode.BigInteger;

            case "array":
                return ExpressoTypeCode.Array;

            case "vector":
                return ExpressoTypeCode.Vector;

            case "dictionary":
                return ExpressoTypeCode.Dictionary;

            case "tuple":
                return ExpressoTypeCode.Tuple;

            case "intseq":
                return ExpressoTypeCode.IntSeq;

            /*case "void":
                return ExpressoTypeCode.Void;*/

            case "string":
                return ExpressoTypeCode.String;

            case "slice":
                return ExpressoTypeCode.Slice;

            default:
                return ExpressoTypeCode.None;
            }
        }

        /// <summary>
        /// Gets a <see cref="ExpressoTypeCode"/> instance corresponds to the `keyword`.
        /// Note that it ignores the generic types.
        /// </summary>
        /// <returns>The actual known type code.</returns>
        /// <param name="keyword">Keyword.</param>
        public static ExpressoTypeCode GetActualKnownTypeCodeForPrimitiveType(string keyword)
        {
            switch(keyword){
            case "bool":
                return ExpressoTypeCode.Bool;

            case "char":
                return ExpressoTypeCode.Char;

            case "byte":
                return ExpressoTypeCode.Byte;

            case "int":
                return ExpressoTypeCode.Int;

            case "uint":
                return ExpressoTypeCode.UInt;

            case "float":
                return ExpressoTypeCode.Float;

            case "double":
                return ExpressoTypeCode.Double;

            case "bigint":
                return ExpressoTypeCode.BigInteger;

            case "intseq":
                return ExpressoTypeCode.IntSeq;

                /*case "void":
                return ExpressoTypeCode.Void;*/

            case "string":
                return ExpressoTypeCode.String;

            default:
                return ExpressoTypeCode.None;
            }
        }
    }
}

