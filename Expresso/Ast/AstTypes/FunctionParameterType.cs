﻿using System;
using ICSharpCode.NRefactory.PatternMatching;
using ICSharpCode.NRefactory.TypeSystem;

namespace Expresso.Ast
{
    /// <summary>
    /// Represents the type of a function or method parameter.
    /// </summary>
    public class FunctionParameterType : AstType
    {
        /// <summary>
        /// The function parameter's type.
        /// </summary>
        /// <value>The type.</value>
        public AstType Type{
            get => GetChildByRole(Roles.Type);
            set => SetChildByRole(Roles.Type, value);
        }

        /// <summary>
        /// Modifiers for the function parameter.
        /// </summary>
        /// <value>The modifiers.</value>
        public FunctionParameterModifiers Modifiers{
            get; set;
        }

        /// <summary>
        /// The default value for this parameter.
        /// </summary>
        public object DefaultValue{
            get; set;
        }

        public FunctionParameterType(AstType type, bool isOptional, bool isVariadic, object defaultValue)
            : base(type.StartLocation, type.EndLocation)
        {
            Type = type;
            Modifiers = GetParameterModifiers(isOptional, isVariadic);
            DefaultValue = defaultValue;
        }

        public override string Name => Type.Name;

        public override Identifier IdentifierNode => Type.IdentifierNode;

        public override void AcceptWalker(IAstWalker walker)
        {
            walker.VisitFunctionParameterType(this);
        }

        public override TResult AcceptWalker<TResult>(IAstWalker<TResult> walker)
        {
            return walker.VisitFunctionParameterType(this);
        }

        public override TResult AcceptWalker<TResult, TData>(IAstWalker<TData, TResult> walker, TData data)
        {
            return walker.VisitFunctionParameterType(this, data);
        }

        public override void AcceptTypeWalker(IAstTypeWalker walker)
        {
            walker.VisitFunctionParameterType(this);
        }

        public override TResult AcceptTypeWalker<TResult>(IAstTypeWalker<TResult> walker)
        {
            return walker.VisitFunctionParameterType(this);
        }

        public override TResult AcceptTypeWalker<TData, TResult>(IAstTypeWalker<TData, TResult> walker, TData data)
        {
            return walker.VisitFunctionParameterType(this, data);
        }

        public override ITypeReference ToTypeReference(NameLookupMode lookupMode, InterningProvider interningProvider = null)
        {
            throw new NotImplementedException ();
        }

        protected internal override bool DoMatch(AstNode other, Match match)
        {
            var o = other as FunctionParameterType;
            if(o != null)
                return Type.DoMatch(o.Type, match) && Modifiers.Equals(o.Modifiers);
            else if(other is AstType ast_type)
                return Type.DoMatch(ast_type, match);
            else
                return false;
        }

        private static FunctionParameterModifiers GetParameterModifiers(bool isOptional, bool isVariadic)
        {
            var value = FunctionParameterModifiers.None;
            if(isOptional){
                value ^= FunctionParameterModifiers.None;
                value |= FunctionParameterModifiers.Optional;
            }

            if(isVariadic){
                value ^= FunctionParameterModifiers.None;
                value |= FunctionParameterModifiers.Variadic;
            }

            return value;
        }
    }
}
