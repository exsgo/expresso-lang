﻿using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.Ast
{
    /// <summary>
    /// Represents a value binding pattern.
    /// A ValueBindingPattern introduces a new variable into the current scope and captures a value into that variable.
    /// ( "let" | "var" ) PatternWithType '=' Pattern ;
    /// </summary>
    public class ValueBindingPattern : PatternConstruct
    {
        /// <summary>
        /// Represents the variable initializer.
        /// </summary>
        public VariableInitializer Initializer{
            get => GetChildByRole(Roles.Variable);
            set => SetChildByRole(Roles.Variable, value);
        }

        /// <summary>
        /// Represents the modifiers that describe the properties of inner patterns.
        /// </summary>
        public Modifiers Modifiers{
            get => EntityDeclaration.GetModifiers(this);
            set => EntityDeclaration.SetModifiers(this, value);
        }

        public ValueBindingPattern(PatternWithType pattern, Expression expression, Modifiers modifiers, TextLocation loc)
            : base(loc, expression.EndLocation)
        {
            Modifiers = modifiers;
            Initializer = MakeVariableInitializer(pattern, expression);
        }

        #region implemented abstract members of AstNode

        public override void AcceptWalker(IAstWalker walker)
        {
            walker.VisitValueBindingPattern(this);
        }

        public override TResult AcceptWalker<TResult>(IAstWalker<TResult> walker)
        {
            return walker.VisitValueBindingPattern(this);
        }

        public override TResult AcceptWalker<TResult, TData>(IAstWalker<TData, TResult> walker, TData data)
        {
            return walker.VisitValueBindingPattern(this, data);
        }

        public override void AcceptPatternWalker(IAstPatternWalker walker)
        {
            walker.VisitValueBindingPattern(this);
        }

        public override TResult AcceptPatternWalker<TResult>(IAstPatternWalker<TResult> walker)
        {
            return walker.VisitValueBindingPattern(this);
        }

        protected internal override bool DoMatch(AstNode other, Match match)
        {
            var o = other as ValueBindingPattern;
            return o != null && Initializer.DoMatch(o.Initializer, match) && Modifiers == o.Modifiers;
        }

        #endregion
    }
}

