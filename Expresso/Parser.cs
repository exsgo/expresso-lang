using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using Expresso.Ast;
using Expresso.Ast.Analysis;
using Expresso.CodeGen;
using Expresso.TypeSystem;
using ICSharpCode.NRefactory;


using ExpressoModifiers = Expresso.Ast.Modifiers;




using System;

namespace Expresso {



public class Parser {
	public const int _EOF = 0;
	public const int _double_dots = 1;
	public const int _triple_dots = 2;
	public const int _colon = 3;
	public const int _double_colon = 4;
	public const int _semicolon = 5;
	public const int _lcurly = 6;
	public const int _lparen = 7;
	public const int _langle_bracket = 8;
	public const int _lbracket = 9;
	public const int _rparen = 10;
	public const int _rcurly = 11;
	public const int _rangle_bracket = 12;
	public const int _rbracket = 13;
	public const int _comma = 14;
	public const int _dot = 15;
	public const int _thick_arrow = 16;
	public const int _equal = 17;
	public const int _vertical_pipe = 18;
	public const int _ident = 19;
	public const int _integer = 20;
	public const int _float = 21;
	public const int _hex_digit = 22;
	public const int _unicode_escape = 23;
	public const int _character_literal = 24;
	public const int _string_literal = 25;
	public const int _raw_string_literal = 26;
	public const int _keyword_for = 27;
	public const int maxT = 123;

	const bool _T = true;
	const bool _x = false;
	const int minErrDist = 2;
	
	public Scanner scanner;
	public Errors  errors;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;

public static Dictionary<string, SymbolTable> CachedSymbolTables = new Dictionary<string, SymbolTable>();
    static uint ScopeId = 1;
    string cur_class_name;
    static Regex UnicodeEscapeFinder = new Regex(@"\\[uU]([\dA-Fa-f]{4}|[\dA-Fa-f]{6})", RegexOptions.Compiled);
    ExpressoModifiers cur_modifiers, cur_class_modifiers;
    bool is_first_comprehension_for_clause = true, defining_closure_parameters = false, is_defining_variable = false;
    TextLocation parent_location;
    List<ParameterType> class_type_params = new List<ParameterType>();
    // We need to initialize this because otherwise Type can't determine whether it contains type parameters
    List<ParameterType> class_and_func_type_params = new List<ParameterType>();
    internal SymbolTable Symbols{get; set;}
    /// <summary>
    /// This flag determines whether we are doing post-parse processing including name binding,
    /// type validity check, type inference and flow analisys.
    /// </summary>
    public bool DoPostParseProcessing{get; set;}
	public ExpressoAst TopmostAst{get; private set;}	//the top-level AST the parser is parsing
    public TextLocation CurrentLocation{
        get{
            if(parent_location == default)
                return new TextLocation(parent_location.Line + t.line, parent_location.Column + t.col);
            else
                return new TextLocation(parent_location.Line + t.line - 1, parent_location.Column + t.col - 1);
        }
    }
    public TextLocation CurrentEndLocation{
        get{
            if(parent_location == default)
                return new TextLocation(parent_location.Line + t.line, parent_location.Column + t.col + t.val.Length);
            else
                return new TextLocation(parent_location.Line + t.line - 1, parent_location.Column + t.col + t.val.Length - 1);
        }
    }
    public TextLocation NextLocation{
        get{
            if(parent_location == default)
                return new TextLocation(parent_location.Line + la.line, parent_location.Column + la.col);
            else
                return new TextLocation(parent_location.Line + la.line - 1, parent_location.Column + la.col - 1);
        }
    }
    internal List<Parser> InnerParsers{get;} = new List<Parser>();

	///<summary>
	/// Parser Implementation details:
	/// 	During parsing we'll construct the symbol table.
	/// 	And in post-parse process, do type validity check and flow analysis, including local name bindings
    ///     and type inference.
	///		Note that the identifiers are just placeholders until after doing name binding. 
	/// 	(Thus referencing them causes runtime exceptions)
	///</summary>
    ///<remarks>
    /// The Parser class itself is responsible for constructing the AST AND the symbol table.
    ///</remarks>
	Parser(TextLocation parentLocation)
	{
        DoPostParseProcessing = false;
        ExpressoCompilerHelpers.Prepare();
        Symbols = SymbolTable.Create();
        parent_location = parentLocation;
	}
	
	LiteralExpression CreateDefaultValue(KnownTypeCode type)
	{
		LiteralExpression result = null;
		var loc = CurrentLocation;
        var type_name = type.ToString().ToLower();

		switch(type){
		case KnownTypeCode.Int:
        case KnownTypeCode.UInt:
        case KnownTypeCode.Byte:
			result = Expression.MakeConstant(type_name, 0, loc);
			break;
			
		case KnownTypeCode.Bool:
			result = Expression.MakeConstant(type_name, false, loc);
			break;
			
		case KnownTypeCode.Float:
			result = Expression.MakeConstant(type_name, 0.0, loc);
			break;
			
		case KnownTypeCode.String:
			result = Expression.MakeConstant(type_name, "", loc);
			break;

        case KnownTypeCode.Char:
            result = Expression.MakeConstant(type_name, '\0', loc);
			break;

		default:
			throw new InvalidOperationException("Unknown object type");
		}
		
		return result;
	}

    AstType CreatePrimitiveType(string keyword, TextLocation loc, bool isReference)
    {
        AstType type = new PrimitiveType(keyword, loc);
        if(isReference)
            type = new ReferenceType(type, loc);

        return type;
    }

    LiteralExpression CreateLiteral(string value, TextLocation loc)
    {
        string type_name = "int";
        object obj = null;
        string suffix = value.Substring(value.Length - 1);
        string numerics = value.Replace("_", "");

        switch(suffix){
        case "u":
        case "U":
        {
            type_name = "uint";
            if(uint.TryParse(numerics.Substring(0, numerics.Length - 1), out var u))
                obj = u;
            else
                throw new InvalidOperationException("Unreachable!");
            break;
        }

        case "l":
        case "L":
            type_name = "bigint";
            obj = BigInteger.Parse(numerics.Substring(0, numerics.Length - 1));
            break;

        case "f":
        case "F":
        {
            type_name = "float";
            if(float.TryParse(numerics.Substring(0, numerics.Length - 1), out var f))
                obj = f;
            else
                throw new InvalidOperationException("Unreachable!");
            break; 
        }

        default:
        {
            if(int.TryParse(numerics, out var i)){
                obj = i;
                type_name = "int";
            }else if(double.TryParse(numerics, out var d)){
                obj = d;
                type_name = "double";
            }else{
                throw new InvalidOperationException("Unreachable!");
            }
            break;
        }
        }

        return Expression.MakeConstant(type_name, obj, loc);
    }

    LiteralExpression HandleEscapes(string typeName, string literal, TextLocation loc)
    {
        string tmp = literal.Replace("\\n", "\n").Replace("\\r", "\r").Replace("\\t", "\t")
                            .Replace("\\v", "\v").Replace("\\f", "\f").Replace("\\0", "\0")
                            .Replace("\\b", "\b").Replace(@"\\", @"\").Replace("\\'", "'").Replace("\\\"", "\"");
        tmp = UnicodeEscapeFinder.Replace(tmp, m => {
            return ((char)int.Parse(m.Value.Substring(2), NumberStyles.HexNumber)).ToString();
        });

        if(typeName == "char")
            return Expression.MakeConstant(typeName, Convert.ToChar(tmp), loc);
        else if(typeName == "string")
            return Expression.MakeConstant(typeName, tmp, loc);
        else
            throw new InvalidOperationException("Escape sequence found in an unknown type");
    }
    
    AstType ConvertPathToType(PathExpression path)
    {
        AstType type = null;
        foreach(var item in path.Items){
            item.Remove();
            if(type == null)
                type = AstType.MakeSimpleType(item.Name, item.StartLocation);
            else
                type = AstType.MakeMemberType(type, AstType.MakeSimpleType(item.Name, item.StartLocation), item.EndLocation);
        }

        return type;
    }

    SimpleType CreateTypeWithArgs(string typeName, params AstType[] typeArgs)
    {
        return new SimpleType(typeName, typeArgs, TextLocation.Empty, TextLocation.Empty);
    }

    bool IsPrimitiveGenericType(string name)
    {
        return name != null && (name == "dictionary" || name == "vector");
    }

    /// <summary>
    /// Creates a new <see ref="ICSharpCode.NRefactory.TextLocation" />
    /// that points to the location n characters before the current.
    /// </summary>
    /// <remarks>
    /// It doesn't take line breaks into account.
    /// </remarks>
    TextLocation CreateLocationBefore(int n)
    {
        return new TextLocation(t.line, t.col - n);
    }

    void GoDownScope()
    {
        Symbols = Symbols.Children[Symbols.Children.Count - 1];
    }

    void GoUpScope()
    {
        Symbols = Symbols.Parent;
    }
	
	bool IsSequenceInitializer()
	{
		var x = la;
		if(x.kind != _comma)
            return true;
		
		while(x.kind != 0 && x.kind != _comma && x.kind != _keyword_for)
            x = scanner.Peek();

        scanner.ResetPeek();
        return x.kind != _keyword_for;
	}

    bool IsIdentifierPattern()
    {
        var t = la;
        var x = scanner.Peek();
        scanner.ResetPeek();
        return t.kind == _ident && x.kind != _double_colon && x.kind != _lcurly;
    }

    bool IsDestructuringPattern()
    {
        var tt = la;
        var x = scanner.Peek();
        scanner.ResetPeek();
        return tt.kind == _ident && (x.kind == _double_colon || x.kind == _lcurly);
    }

    bool NotFinalComma()
    {
        var t = la;
        var tt = scanner.Peek();
        scanner.ResetPeek();
        return t.kind == _comma && tt.kind != _rparen && tt.kind != _rbracket && tt.kind != _triple_dots;
    }

    bool IsObjectCreation()
    {
        var t = la;
        if(t.kind != _lcurly)
            return false;

        var key = scanner.Peek();
        var tt = scanner.Peek();
        scanner.ResetPeek();
        return key.kind == _rcurly || (key.kind == _integer || key.kind == _ident) && tt.kind == _colon;
    }

    bool IsStartOfAnotherType()
    {
        var t = la;
        var tt = scanner.Peek();
        scanner.ResetPeek();
        return t.val != "|" || tt.val != "->";
    }

    bool IsStartOfBlockScope()
    {
        return la.val == "{";
    }

    bool IsStartOfImportPath()
    {
        var token = scanner.Peek();
        scanner.ResetPeek();
        return token.val != "::" && token.val != ".";
    }

    bool IsStartOfTypeNameOfImportPath()
    {
        var token = scanner.Peek();
        scanner.ResetPeek();
        return token.val != ".";
    }

    bool IsIntSeqColon()
    {
        var tt = la;
        var x = scanner.Peek();
        bool another_colon_found = false;
        while(x.kind != _semicolon){
            if(x.kind == _colon)
                another_colon_found = true;

            x = scanner.Peek();
        }
        scanner.ResetPeek();
        return tt.kind == _colon && !another_colon_found;
    }

    bool IsGenericTypeSignature()
    {
        var t = la;
        if(t.kind != _langle_bracket)
            return false;

        var x = scanner.Peek();
        bool closing_bracket_found = false;
        while(x.kind != _semicolon){
            if(x.kind == _rangle_bracket)
                closing_bracket_found = true;

            x = scanner.Peek();
        }
        scanner.ResetPeek();
        return la.kind == _langle_bracket && closing_bracket_found;
    }

    bool IsArrayTypeSignature()
    {
        var tt = la;
        var x = scanner.Peek();
        scanner.ResetPeek();
        return tt.kind == _lbracket && x.kind == _rbracket;
    }

    bool IsTupleStyleEnumMember()
    {
        var x = scanner.Peek();
        scanner.ResetPeek();
        return x.kind == _lparen;
    }

    bool IsKeyIdentifier()
    {
        var x = scanner.Peek();
        scanner.ResetPeek();
        return x.kind == _colon;
    }

    bool DistinguishBetweenDestructuringPatternAndEnumMember()
    {
        var tt = la;
        var x = scanner.Peek();
        while(x.kind == _ident || x.kind == _double_colon)
            x = scanner.Peek();

        scanner.ResetPeek();
        return tt.kind == _ident && x.kind == _lcurly;
    }

    bool IsTypeParameter()
    {
        var tt = la;
        var x = scanner.Peek();
        scanner.ResetPeek();
        return tt.kind == _ident && x.kind == _equal; 
    }

    bool ShouldContainGenerics()
    {
        var tt = la;
        if(tt.kind != _langle_bracket)
            return false;

        var x = scanner.Peek();
        while(x.kind != _rangle_bracket && x.kind != _dot && x.kind != _lcurly && x.kind != _semicolon)
            x = scanner.Peek();

        scanner.ResetPeek();
        return x.kind == _rangle_bracket;
    }

    bool IsTrailer()
    {
        var tt = la;
        if(tt.kind == _dot || tt.kind == _lbracket || tt.kind == _lparen)
            return true;

        if(tt.kind != _langle_bracket)
            return false;

        var x = scanner.Peek();
        while(x.kind != _rangle_bracket && x.kind != _semicolon && x.kind != _lcurly)
            x = scanner.Peek();

        scanner.ResetPeek();
        return x.kind == _rangle_bracket;
    }

    /*bool IsLValueList()
    {
        var tt = la;
        if(tt.kind == _semicolon || tt.kind == _equal)
            return false;

        var x = scanner.Peek();
        while(!x.val.Contains("=") || x.kind != _thick_arrow || x.kind != _semicolon)
            x = scanner.Peek();

        return la.kind == _comma && x.val.Contains("=");
    }

    bool IsRValueList()
    {
        var tt = la;
        return tt.kind == _comma;
    }*/
    
    bool IsObjectCreationWithBigInt()
    {
        if(la.val != "bigint")
            return true;
            
        var tt = la;
        var t = scanner.Peek();
        scanner.ResetPeek();
        return la.val == "bigint" && t.kind == _lcurly;
    }
    
    bool IsBitOr()
    {
        if(la.kind != _vertical_pipe)
            return false;
        
        var t = scanner.Peek();
        scanner.ResetPeek();
        return t.kind != _vertical_pipe;
    }

    bool ReturnsTrue()
    {
        return true;
    }

    bool CheckKeyword(string name)
    {
        // invalid syntax error thrown
        /*if(KnownTypeReference.Keywords.Contains(name)){
            SemanticError("ES0009", $"{name} is reserverd for a keyword.");
            return true;
        }*/

        return false;
    }

    /// <summary>
    /// Reports a semantic error message.
    /// It is intended to be used inside the Parser class.
    /// </summary>
    /// <param name="errorCode">The error code. It must contain the string "ES" at the head.</param>
    /// <param name="message">The message string.</param>
	void SemanticError(string errorCode, string message)
	{
		//Convenient method for printing a semantic error with a format string
        var node = AstType.MakeSimpleType("", null, CurrentLocation);
        throw new ParserException(message, errorCode, node);
	}

    /// <summary>
    /// Reports a semantic error message.
    /// It is intended to be used inside the Parser class.
    /// </summary>
    /// <param name="loc">The location at which the error occurred.</param>
    /// <param name="errorCode">The error code. It must contain the string "ES" at the head.</param>
    /// <param name="message">The message string.</param>
    void SemanticError(TextLocation loc, string errorCode, string message)
    {
        var node = AstType.MakeSimpleType("", null, loc);
        throw new ParserException(message, errorCode, node);
    }

    /// <summary>
    /// Reports a semantic error message with a range.
    /// It is intended to be used inside the Parser class.
    /// </summary>
    /// <param name="start">The location which is the start of the error range.</param>
    /// <param name="end">The location which is the end of the error range.</param>
    /// <param name="errorCode">The error code. It must contain the string "ES" at the head.</param>
    /// <param name="message">The message string.</param>
    void SemanticError(TextLocation start, TextLocation end, string errorCode, string message)
    {
        var start_node = AstType.MakeSimpleType("", null, start);
        var end_node = AstType.MakeSimpleType("", null, end);
        throw new ParserException(message, errorCode, start_node, end_node);
    }

    /// <summary>
    /// Reports a warning message.
    /// It is intended to be used from outside the Parser class.
    /// </summary>
    /// <param name="message">The message string.</param>
    /// <param name="errorCode">The error code. It must contain the string "ES" at the head.</param>
    /// <param name="node">The node on which the warning occurred.</param>
    public void ReportWarning(string message, string errorCode, AstNode node)
    {
#if DEBUG
        throw new ParserException(message, errorCode, node);
#else
        var msg = $"{node.StartLocation} -- Warning {errorCode}: {message}";
        errors.Warning(msg);
        ExpressoCompilerHelpers.DisplayHelp(new ParserException(null, errorCode, node));
#endif
    }

    /// <summary>
    /// Reports a semantic error message.
    /// It is intended to be used from outside the Parser class.
    /// </summary>
    /// <param name="message">The message string.</param>
    /// <param name="errorCode">The error code. It must contain the string "ES" at the head.</param>
    /// <param name="node">The node on which the error occurred.</param>
    public void ReportSemanticError(string message, string errorCode, AstNode node)
    {
#if DEBUG
        throw new ParserException(message, errorCode, node);
#else
        var msg = $"{node.StartLocation} -- Error {errorCode}: {message}";
        errors.SemErr(msg);
        ExpressoCompilerHelpers.DisplayHelp(new ParserException(null, errorCode, node));
#endif
    }

    /// <summary>
    /// Reports a semantic error message with a range information.
    /// It is intended to be used from outside the Parser class.
    /// </summary>
    /// <param name="message">The message string.</param>
    /// <param name="errorCode">The error code. It must contain the string "ES" at the head.</param>
    /// <param name="start">The node which is the start of the error range.</param>
    /// <param name="end">The node which is the end of the error range.</param>
    public void ReportSemanticErrorRegional(string message, string errorCode, AstNode start, AstNode end)
    {
#if DEBUG
        throw new ParserException(message, errorCode, start, end);
#else
        var msg = $"{start.StartLocation} ~ {end.EndLocation} -- Error {errorCode}: {message}";
        errors.SemErr(msg);
        ExpressoCompilerHelpers.DisplayHelp(new ParserException(null, errorCode, start, end));
#endif
    }
	
/*--------------------------------------------------------------------------*/


	public Parser(Scanner scanner, TextLocation parentLocation = default) : this(parentLocation) {
		this.scanner = scanner;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(NextLocation.Line, NextLocation.Column, n);
		errDist = 0;
	}

	public void SemErr (string msg) {
		if (errDist >= minErrDist) errors.SemErr(CurrentLocation.Line, CurrentLocation.Column, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) { ++errDist; break; }

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	bool StartOf (int s) {
		return set[s, la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}


	bool WeakSeparator(int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) {Get(); return true;}
		else if (StartOf(repFol)) {return false;}
		else {
			SynErr(n);
			while (!(set[syFol, kind] || set[repFol, kind] || set[0, kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}

	
	void Expresso() {
		ExpressoAst module_decl = null;
		     try{
		  
		ModuleBody(out module_decl);
		Debug.Assert(Symbols.Parent.Name == "root", "The symbol table should point at \"programRoot\" before name binding.");
		#if !DEBUG
		if(errors.count > 0)
		   throw new FatalError("Invalid syntax found!");
		#endif
		
		#if !LIBRARY
		if(DoPostParseProcessing){
		   PreProcessor.PerformPreProcess(module_decl, this);
		   ExpressoNameBinder.BindAst(module_decl, this); // Here's the start of post-parse processing
		}
		#endif
		}
		catch(ParserException e){
		errors.SemErr(e.ToString());
		ExpressoCompilerHelpers.DisplayHelp(e);
		Utilities.CompilerOutput.WriteLine(e.StackTrace);
		
		throw e;
		}
		catch(FatalError ex){
		// Do nothing with a FatalError
		// It only signals that the program should immediately exit
		throw ex;
		}
		
		TopmostAst = module_decl;	//Currently there is not so much code out there, though...
		
	}

	void ModuleBody(out ExpressoAst ast) {
		var decls = new List<EntityDeclaration>();
		string module_name; List<AttributeSection> attributes; AttributeSection item_attribute = null;
		List<ImportDeclaration> prog_defs = null; EntityDeclaration decl = null;
		
		ModuleNameDefinition(out module_name, out attributes);
		Utilities.CompilerOutput.WriteLine("Parsing {0}...", module_name); 
		if (la.kind == 40) {
			ProgramDefinition(out prog_defs);
		}
		if (la.kind == 39) {
			AttributeSection(out item_attribute);
		}
		while (la.kind == 28 || la.kind == 29 || la.kind == 30) {
			ModuleModifiers();
		}
		if (la.kind == 47) {
			FuncDecl(out decl, cur_modifiers, item_attribute);
		} else if (la.kind == 60 || la.kind == 61) {
			ModuleVariableDecl(out decl, item_attribute);
		} else if (la.kind == 33) {
			ClassDecl(out decl, cur_modifiers, item_attribute);
		} else if (la.kind == 44) {
			InterfaceDecl(out decl, cur_modifiers, item_attribute);
		} else if (la.kind == 34) {
			EnumDecl(out decl, cur_modifiers, item_attribute);
		} else SynErr(124);
		decls.Add(decl);
		cur_modifiers = ExpressoModifiers.None;
		
		while (StartOf(1)) {
			item_attribute = null; 
			if (la.kind == 39) {
				AttributeSection(out item_attribute);
			}
			while (la.kind == 28 || la.kind == 29 || la.kind == 30) {
				ModuleModifiers();
			}
			if (la.kind == 47) {
				FuncDecl(out decl, cur_modifiers, item_attribute);
			} else if (la.kind == 60 || la.kind == 61) {
				ModuleVariableDecl(out decl, item_attribute);
			} else if (la.kind == 33) {
				ClassDecl(out decl, cur_modifiers, item_attribute);
			} else if (la.kind == 44) {
				InterfaceDecl(out decl, cur_modifiers, item_attribute);
			} else if (la.kind == 34) {
				EnumDecl(out decl, cur_modifiers, item_attribute);
			} else SynErr(125);
			decls.Add(decl);
			cur_modifiers = ExpressoModifiers.None;
			
		}
		ast = AstNode.MakeModuleDef(module_name, decls, prog_defs, attributes); 
	}

	void ModuleModifiers() {
		if (la.kind == 28) {
			Get();
			cur_modifiers |= ExpressoModifiers.Export; 
		} else if (la.kind == 29) {
			Get();
			cur_modifiers |= ExpressoModifiers.Abstract; 
		} else if (la.kind == 30) {
			Get();
			cur_modifiers |= ExpressoModifiers.Sealed; 
		} else SynErr(126);
	}

	void ModuleNameDefinition(out string moduleName, out List<AttributeSection> attributes ) {
		attributes = new List<AttributeSection>(); AttributeSection attribute = null; 
		while (la.kind == 39) {
			AttributeSection(out attribute);
			attributes.Add(attribute); 
		}
		Expect(32);
		Expect(19);
		moduleName = t.val; 
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(127); Get();}
		Expect(5);
	}

	void ProgramDefinition(out List<ImportDeclaration> imports ) {
		imports = new List<ImportDeclaration>();
		ImportDeclaration tmp;
		
		ImportDecl(out tmp);
		imports.Add(tmp); 
		while (la.kind == 40) {
			ImportDecl(out tmp);
			imports.Add(tmp); 
		}
	}

	void AttributeSection(out AttributeSection section ) {
		var attributes = new List<ObjectCreationExpression>(); ObjectCreationExpression attribute = null;
		string target = null; var start_loc = NextLocation;
		
		Expect(39);
		if (StartOf(2)) {
			AttributeTarget(out target);
			Expect(3);
		}
		Attribute(out attribute);
		attributes.Add(attribute); 
		while (la.kind == 14) {
			Get();
			Attribute(out attribute);
			attributes.Add(attribute); 
		}
		Expect(13);
		section = AstNode.MakeAttributeSection(target, attributes, start_loc, CurrentEndLocation); 
	}

	void FuncDecl(out EntityDeclaration decl, ExpressoModifiers modifiers, AttributeSection attribute) {
		Identifier ident = null; PostModifiers post_modifiers = null;
		string name; AstType type = null; BlockStatement block;
		var type_params = new List<ParameterType>(class_type_params.Select(tp => tp).ToList());
		var @params = new List<ParameterDeclaration>();
		var start_loc = NextLocation;
		var type_constraints = new List<TypeConstraint>();
		var replacer = new ParameterTypeReplacer(type_params);
		decl = null;
		
		while (!(la.kind == 0 || la.kind == 47)) {SynErr(128); Get();}
		Expect(47);
		if(cur_modifiers.HasFlag(ExpressoModifiers.Abstract) && !cur_class_modifiers.HasFlag(ExpressoModifiers.Abstract))
		   SemanticError(start_loc, "ES0150", "Abstract methods must reside in abstract classes.");
		
		Symbols.AddScope();
		var ident_start_loc = NextLocation;
		
		Expect(19);
		name = t.val;
		if(!CheckKeyword(name)){
		   ident = AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(CurrentLocation), modifiers, null, ident_start_loc); 
		   Symbols.AddSymbol(name, ident);
		}else{
		   // The name is unsuitable for a method or a function name.
		   // Leave the parser to recover its state.
		}
		
		if (la.kind == 8) {
			GenericTypeParameters(type_params);
			class_and_func_type_params = type_params.Select(tp => tp).ToList(); 
		}
		Expect(7);
		GoDownScope();
		Symbols.Name = $"func_{name}`{ScopeId++}";
		
		if (la.kind == 19 || la.kind == 39) {
			ParamList(type_params, @params);
		}
		Expect(10);
		if (la.kind == 48) {
			Get();
			Type(out type);
		}
		if(type == null)
		   type = new PlaceholderType(CurrentLocation);
		
		if (la.kind == 45 || la.kind == 57) {
			PostModifiers(out post_modifiers);
		}
		var cloned = (PostModifiers)post_modifiers?.Clone();
		ident.PostModifiers = cloned;
		
		if (la.kind == 62) {
			TypeConstraints(type_constraints);
		}
		type_constraints.AddRange(
		   type_params.Where(p => !class_type_params.Any(tp => tp.Name == p.Name) && !type_constraints.Any(c => c.TypeParameter.Name == p.Name))
		              .Select(p => AstNode.MakeTypeConstraint((ParameterType)p.Clone(), null))
		);
		
		if (la.kind == 5) {
			Get();
			if(!modifiers.HasFlag(ExpressoModifiers.Abstract))
			   SemanticError("ES0030", "Empty function body requires the method to be abstract.");
			
			decl = EntityDeclaration.MakeFunc(ident, @params, null, type, modifiers, post_modifiers, type_constraints, attribute, start_loc);
			
		} else if (la.kind == 6) {
			Block(out block);
			decl = EntityDeclaration.MakeFunc(ident, @params, block, type, modifiers, post_modifiers, type_constraints, attribute, start_loc); 
		} else SynErr(129);
		decl.ReturnType.AcceptTypeWalker(replacer);
		if(type_constraints.Any()){
		   int type_param_index = class_type_params.Count;
		   class_and_func_type_params.RemoveRange(type_param_index, class_and_func_type_params.Count - type_param_index);
		}
		
		GoUpScope();
		
	}

	void ModuleVariableDecl(out EntityDeclaration field, AttributeSection attribute) {
		Expression rhs; PatternWithType typed_pattern; var start_loc = NextLocation;
		var patterns = new List<PatternWithType>(); var exprs = new List<Expression>();
		
		while (!(la.kind == 0 || la.kind == 60 || la.kind == 61)) {SynErr(130); Get();}
		if (la.kind == 60) {
			Get();
			cur_modifiers |= ExpressoModifiers.Immutable; 
		} else if (la.kind == 61) {
			Get();
		} else SynErr(131);
		VarDef(out typed_pattern, out rhs);
		if(!(typed_pattern.Pattern is IdentifierPattern))
		  SemanticError("ES0020", $"A module variable can only contain identifier patterns; actual: `{typed_pattern.Pattern}`.");
		
		patterns.Add(typed_pattern);
		exprs.Add(rhs);
		
		while (la.kind == 14) {
			Get();
			VarDef(out typed_pattern, out rhs);
			if(!(typed_pattern.Pattern is IdentifierPattern))
			  SemanticError("ES0020", $"A module variable can only contain an indentifier patterns; actual: `{typed_pattern.Pattern}`.");
			
			patterns.Add(typed_pattern);
			exprs.Add(rhs);
			
		}
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(132); Get();}
		Expect(5);
		field = EntityDeclaration.MakeField(patterns, exprs, cur_modifiers, attribute, start_loc, CurrentEndLocation);
		cur_modifiers = ExpressoModifiers.None; 
		
	}

	void ClassDecl(out EntityDeclaration decl, ExpressoModifiers modifiers, AttributeSection attribute) {
		EntityDeclaration entity = null; var decls = new List<EntityDeclaration>(); AstType type_path, parent_type = null;
		string name; var bases = new List<AstType>(); var start_loc = NextLocation;
		Identifier ident = null; AttributeSection item_attribute = null; var type_constraints = new List<TypeConstraint>();
		
		while (!(la.kind == 0 || la.kind == 33)) {SynErr(133); Get();}
		Expect(33);
		Symbols.AddScope(ClassType.Class);
		var ident_start_loc = NextLocation;
		
		Expect(19);
		name = t.val;
		if(!CheckKeyword(name)){
		   ident = AstNode.MakeIdentifier(name, modifiers, ident_start_loc);
		   Symbols.AddTypeSymbol(name, ident);
		   cur_class_name = name;
		}else{
		   // Failed to parse an identifier.
		   // Leave the parser to recover its state.
		}
		
		if (la.kind == 8) {
			GenericTypeParameters(class_type_params);
		}
		if (la.kind == 3) {
			Get();
			Type(out type_path);
			bases.Add(type_path);
			parent_type = type_path;
			
			while (la.kind == 14) {
				Get();
				Type(out type_path);
				bases.Add(type_path);
				parent_type = type_path;
				
			}
		}
		if (la.kind == 62) {
			TypeConstraints(type_constraints);
		}
		type_constraints.AddRange(
		   class_type_params.Where(tp => !type_constraints.Any(c => c.TypeParameter.Name == tp.Name))
		                    .Select(p => AstNode.MakeTypeConstraint(p.Clone(), null))
		);
		// This code is needed in order to define fields with type parameters
		class_and_func_type_params = class_type_params.Select(tp => tp.Clone()).ToList();
		
		Expect(6);
		GoDownScope();
		Symbols.Name = $"type_{name}`{ScopeId++}";
		Symbols.AddTypeParameters(class_type_params);
		Symbols.ParentTypes = bases;
		Symbols.TypeConstraints = type_constraints.Select(tc => tc).ToList();
		
		while (StartOf(3)) {
			cur_class_modifiers = cur_modifiers;
			cur_modifiers = ExpressoModifiers.Private; 
			item_attribute = null;
			
			if (la.kind == 39) {
				AttributeSection(out item_attribute);
			}
			while (StartOf(4)) {
				Modifiers();
			}
			if (la.kind == 47) {
				FuncDecl(out entity, cur_modifiers, item_attribute);
				decls.Add(entity); 
			} else if (la.kind == 60 || la.kind == 61) {
				FieldDecl(out entity, item_attribute);
				decls.Add(entity); 
			} else if (la.kind == 33) {
				ClassDecl(out entity, cur_modifiers, item_attribute);
				decls.Add(entity); 
			} else if (la.kind == 44) {
				InterfaceDecl(out entity, cur_modifiers, item_attribute);
				decls.Add(entity); 
			} else SynErr(134);
		}
		while (!(la.kind == 0 || la.kind == 11)) {SynErr(135); Get();}
		Expect(11);
		decl = EntityDeclaration.MakeClassDecl(ident, bases, decls, modifiers, type_constraints, attribute, start_loc, CurrentEndLocation);
		GoUpScope();
		cur_modifiers = ExpressoModifiers.None;
		class_type_params.Clear();
		
	}

	void InterfaceDecl(out EntityDeclaration decl, ExpressoModifiers modifiers, AttributeSection attribute) {
		string name = null; Identifier ident = null; AttributeSection item_attribute = null;
		AstType type_path = null;
		EntityDeclaration method = null;
		var bases = new List<AstType>();
		var decls = new List<EntityDeclaration>();
		var start_loc = NextLocation;
		var type_constraints = new List<TypeConstraint>();
		
		while (!(la.kind == 0 || la.kind == 44)) {SynErr(136); Get();}
		Expect(44);
		Symbols.AddScope(ClassType.Interface);
		var ident_start_loc = NextLocation;
		
		Expect(19);
		name = t.val;
		if(!CheckKeyword(name)){
		   ident = AstNode.MakeIdentifier(name, modifiers, ident_start_loc);
		   Symbols.AddTypeSymbol(name, ident);
		}
		
		if (la.kind == 8) {
			GenericTypeParameters(class_type_params);
		}
		if (la.kind == 3) {
			Get();
			Type(out type_path);
			bases.Add(type_path); 
			while (la.kind == 14) {
				Get();
				Type(out type_path);
				bases.Add(type_path); 
			}
		}
		if (la.kind == 62) {
			TypeConstraints(type_constraints);
		}
		if(!type_constraints.Any())
		   type_constraints = class_type_params.Select(p => AstNode.MakeTypeConstraint(p.Clone(), null)).ToList();
		
		Expect(6);
		GoDownScope();
		Symbols.Name = $"type_{name}`{ScopeId++}";
		Symbols.AddTypeParameters(class_type_params);
		Symbols.ParentTypes = bases;
		Symbols.TypeConstraints = type_constraints.Select(tc => tc).ToList();
		
		while (la.kind == 39 || la.kind == 47) {
			if (la.kind == 39) {
				AttributeSection(out item_attribute);
			}
			MethodSignature(out method, item_attribute);
			decls.Add(method); 
		}
		Expect(11);
		decl = EntityDeclaration.MakeInterfaceDecl(ident, bases, decls, modifiers, type_constraints, attribute, start_loc, CurrentEndLocation);
		GoUpScope();
		class_type_params.Clear();
		
	}

	void EnumDecl(out EntityDeclaration decl, ExpressoModifiers modifiers, AttributeSection attribute) {
		EntityDeclaration entity = null; var decls = new List<EntityDeclaration>(); string name;
		var start_loc = NextLocation; Identifier ident = null; AttributeSection item_attribute = null;
		int raw_value = 0; bool has_value_identifier_added = false; var type_constraints = new List<TypeConstraint>();
		
		while (!(la.kind == 0 || la.kind == 34)) {SynErr(137); Get();}
		Expect(34);
		Symbols.AddScope(ClassType.Enum);
		var ident_start_loc = NextLocation;
		
		Expect(19);
		name = t.val;
		if(!CheckKeyword(name)){
		   ident = AstNode.MakeIdentifier(name, modifiers, ident_start_loc);
		   Symbols.AddTypeSymbol(name, ident);
		   cur_class_name = name;
		}
		
		if (la.kind == 8) {
			GenericTypeParameters(class_type_params);
		}
		if (la.kind == 62) {
			TypeConstraints(type_constraints);
		}
		type_constraints.AddRange(
		   class_type_params.Where(tp => !type_constraints.Any(c => c.TypeParameter.Name == tp.Name))
		                    .Select(p => AstNode.MakeTypeConstraint(p.Clone(), null))
		);
		// This code is needed in order to define variants with type parameters
		class_and_func_type_params = class_type_params.Select(tp => tp.Clone()).ToList();
		
		Expect(6);
		GoDownScope();
		Symbols.Name = $"type_{name}`{ScopeId++}";
		Symbols.AddTypeParameters(class_type_params);
		Symbols.TypeConstraints = type_constraints.Select(tc => tc).ToList();
		
		while (StartOf(5)) {
			cur_modifiers = ExpressoModifiers.Private;
			item_attribute = null;
			
			if (la.kind == 39) {
				AttributeSection(out item_attribute);
			}
			while (StartOf(4)) {
				Modifiers();
			}
			if (la.kind == 47) {
				FuncDecl(out entity, cur_modifiers, item_attribute);
				decls.Add(entity); 
			} else if (IsTupleStyleEnumMember()) {
				TupleStyleEnumMember(out entity, item_attribute);
				decls.Add(entity); 
			} else if (la.kind == 19) {
				RawValueStyleEnumMember(out entity, item_attribute, ref raw_value);
				decls.Add(entity);
				if(!has_value_identifier_added){
				   var ident2 = AstNode.MakeIdentifier(Utilities.RawValueEnumValueFieldName, ExpressoModifiers.Public);
				   Symbols.AddSymbol(Utilities.RawValueEnumValueFieldName, ident2);
				   has_value_identifier_added = true;
				}
				
			} else SynErr(138);
			if (la.kind == 14) {
				Get();
			}
		}
		while (!(la.kind == 0 || la.kind == 11)) {SynErr(139); Get();}
		Expect(11);
		decl = EntityDeclaration.MakeEnumDecl(ident, decls, modifiers, type_constraints, attribute, start_loc, CurrentEndLocation);
		GoUpScope();
		cur_modifiers = ExpressoModifiers.None;
		class_type_params.Clear();
		
	}

	void AttributeTarget(out string target) {
		switch (la.kind) {
		case 31: {
			Get();
			break;
		}
		case 32: {
			Get();
			break;
		}
		case 33: {
			Get();
			break;
		}
		case 34: {
			Get();
			break;
		}
		case 35: {
			Get();
			break;
		}
		case 36: {
			Get();
			break;
		}
		case 37: {
			Get();
			break;
		}
		case 38: {
			Get();
			break;
		}
		default: SynErr(140); break;
		}
		target = t.val; 
	}

	void Attribute(out ObjectCreationExpression creation) {
		creation = null; Expression expr = null; string name = null; AstType type_path = null; var start_loc = NextLocation; 
		Expect(19);
		name = !t.val.EndsWith("Attribute", StringComparison.CurrentCulture) ? t.val + "Attribute" : t.val;
		type_path = AstType.MakeSimpleType(name, start_loc);
		
		if (la.kind == 6) {
			ObjectCreation(type_path, null, out expr);
			creation = (ObjectCreationExpression)expr; 
		}
		if(creation == null)
		   creation = Expression.MakeObjectCreation(type_path, CurrentEndLocation);
		
	}

	void ObjectCreation(AstType typePath, List<KeyValueType> typeArgs, out Expression expr ) {
		var fields = new List<Identifier>(); var values = new List<Expression>(); 
		Expect(6);
		if (la.kind == 19 || la.kind == 20) {
			if (la.kind == 20) {
				Get();
			} else {
				Get();
			}
			fields.Add(AstNode.MakeIdentifier(t.val, ExpressoModifiers.None, CurrentLocation)); 
			Expect(3);
			CondExpr(out expr);
			values.Add(expr); 
			while (la.kind == 14) {
				Get();
				if (la.kind == 20) {
					Get();
				} else if (la.kind == 19) {
					Get();
				} else SynErr(141);
				fields.Add(AstNode.MakeIdentifier(t.val, ExpressoModifiers.None, CurrentLocation)); 
				Expect(3);
				CondExpr(out expr);
				values.Add(expr); 
			}
		}
		while (!(la.kind == 0 || la.kind == 11)) {SynErr(142); Get();}
		Expect(11);
		expr = Expression.MakeObjectCreation(typePath, fields, values, typeArgs, CurrentEndLocation); 
	}

	void ImportDecl(out ImportDeclaration decl) {
		decl = null; bool seen_from = false;
		Identifier alias = null; Identifier target_file = null; var start_loc = NextLocation;
		var paths = new List<SimpleType>(); var aliases = new List<Identifier>();
		
		while (!(la.kind == 0 || la.kind == 40)) {SynErr(143); Get();}
		Expect(40);
		ImportPaths(ref paths, start_loc);
		if (la.kind == 41) {
			Get();
			seen_from = true;
			var file_start_loc = NextLocation;
			
			Expect(25);
			var name = t.val.Substring(1, t.val.Length - 2);
			if(name != "std.exs" && !scanner.ChildFileExists(name)){
			   SemanticError(file_start_loc, "ES0006", $"The external file '{name}' doesn't exist.");
			}
			target_file = AstNode.MakeIdentifier(name, ExpressoModifiers.None, file_start_loc);
			
		}
		foreach(var path in paths){
		   if(!seen_from)
		       Symbols.AddNativeSymbolTable(path.IdentifierToken);
		}
		
		Expect(42);
		var alias_start_loc = NextLocation; 
		if (la.kind == 43) {
			Get();
			aliases.Add(AstNode.MakeIdentifier("*", ExpressoModifiers.None, alias_start_loc)); 
		} else if (la.kind == 19) {
			ImportAlias(out alias, alias_start_loc);
			aliases.Add(alias);
			// Add module type symbol
			if(!paths.First().Name.Contains("::") && !paths.First().Name.Contains("."))
			   Symbols.AddTypeSymbol(alias.Name, alias);
			
			if(paths.First().Name.Contains("*") && !alias.Name.Contains("*"))
			   SemanticError(alias_start_loc, "ES0500", "The import path contains a glob but the alias doesn't.");
			
		} else if (la.kind == 6) {
			Get();
			ImportAlias(out alias, alias_start_loc);
			aliases.Add(alias); 
			while (la.kind == 14) {
				Get();
				alias_start_loc = NextLocation; 
				ImportAlias(out alias, alias_start_loc);
				aliases.Add(alias); 
			}
			Expect(11);
		} else SynErr(144);
		if(!seen_from){
		   // Add TypeSymbols for .NET's types
		   foreach(var alias2 in aliases)
		       Symbols.AddTypeSymbol(alias2.Name, alias2);
		}
		
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(145); Get();}
		Expect(5);
		decl = AstNode.MakeImportDecl(paths, aliases, target_file, start_loc, CurrentEndLocation); 
	}

	void ImportPaths(ref List<SimpleType> paths, TextLocation startLoc) {
		var builder = new StringBuilder(); SimpleType simple = null; 
		if (IsStartOfImportPath()) {
			ImportPath(out simple, "", startLoc);
			paths.Add(simple); 
		} else if (la.kind == 19) {
			Get();
			builder.Append(t.val); 
		} else SynErr(146);
		if (la.kind == 4) {
			Get();
			builder.Append("::");
			var parent_namespace = builder.ToString();
			
			if (la.kind == 19) {
				if (IsStartOfTypeNameOfImportPath()) {
					ImportPath(out simple, parent_namespace, startLoc);
					paths.Add(simple); 
				} else {
					Get();
					builder.Append(t.val); 
				}
			} else if (la.kind == 6) {
				Get();
				var parent_namespace2 = builder.ToString(); 
				ImportPath(out simple, parent_namespace2, startLoc);
				paths.Add(simple); 
				while (la.kind == 14) {
					Get();
					ImportPath(out simple, parent_namespace2, startLoc);
					paths.Add(simple); 
				}
				Expect(11);
			} else SynErr(147);
		}
		while (la.kind == 15) {
			Get();
			builder.Append('.');
			var parent_namespace3 = builder.ToString();
			
			if (la.kind == 19 || la.kind == 43) {
				if (la.kind == 43) {
					Get();
					paths.Add(AstType.MakeSimpleType(parent_namespace3 + "*")); 
				} else if (IsStartOfTypeNameOfImportPath()) {
					ImportPath(out simple, parent_namespace3, startLoc);
					paths.Add(simple); 
				} else {
					Get();
					builder.Append(t.val); 
				}
			} else if (la.kind == 6) {
				Get();
				var parent_namespace4 = builder.ToString(); 
				ImportPath(out simple, parent_namespace4, startLoc);
				paths.Add(simple); 
				while (la.kind == 14) {
					Get();
					ImportPath(out simple, parent_namespace4, startLoc);
					paths.Add(simple); 
				}
				Expect(11);
			} else SynErr(148);
		}
	}

	void ImportAlias(out Identifier ident, TextLocation startLoc) {
		ident = null; 
		Expect(19);
		if(!CheckKeyword(t.val)){
		   ident = AstNode.MakeIdentifier(t.val, ExpressoModifiers.None, startLoc);
		   //Symbols.AddTypeSymbol(ident.Name, ident);
		}else{
		   // Failed to parse an alias name
		   // Leave the parser to recover its state.
		}
		
	}

	void ImportPath(out SimpleType simple, string parentNamespace, TextLocation startLoc) {
		simple = null; var type_params = new List<AstType>(); string name = null; 
		Expect(19);
		name = t.val; 
		if (la.kind == 8) {
			Get();
			type_params.Add(AstType.MakePlaceholderType()); 
			while (la.kind == 14) {
				Get();
				type_params.Add(AstType.MakePlaceholderType()); 
			}
			Expect(12);
		}
		simple = AstType.MakeSimpleType(parentNamespace + name, type_params, startLoc, CurrentLocation);
		
	}

	void GenericTypeParameters(List<ParameterType> types ) {
		Expect(8);
		Expect(19);
		types.Add(AstType.MakeParameterType(t.val)); 
		while (la.kind == 14) {
			Get();
			Expect(19);
			types.Add(AstType.MakeParameterType(t.val)); 
		}
		Expect(12);
	}

	void Type(out AstType type) {
		var start_loc = NextLocation; type = new PlaceholderType(NextLocation);
		var is_reference = false;
		
		if (StartOf(6)) {
			if (la.kind == 63) {
				Get();
				is_reference = true;
				start_loc = NextLocation;
				
			}
			switch (la.kind) {
			case 64: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 65: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 66: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 67: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 68: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 69: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 70: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 71: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 72: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 7: {
				TupleTypeSignature(out type);
				break;
			}
			case 73: {
				Get();
				type = AstType.MakeSimpleType(t.val, Enumerable.Empty<AstType>(), start_loc, CurrentEndLocation); 
				break;
			}
			case 74: {
				Get();
				type = AstType.MakeSimpleType(t.val, Enumerable.Empty<AstType>(), start_loc, CurrentEndLocation); 
				break;
			}
			case 75: {
				Get();
				type = AstType.MakeSimpleType(t.val, Enumerable.Empty<AstType>(), start_loc, CurrentEndLocation); 
				break;
			}
			case 76: {
				Get();
				type = CreatePrimitiveType(t.val, start_loc, is_reference); 
				break;
			}
			case 77: {
				Get();
				type = AstType.MakeSimpleType("tuple", Enumerable.Empty<AstType>(), start_loc, CurrentEndLocation); 
				break;
			}
			case 19: {
				TypePathExpression(out type);
				if(is_reference)
				   type = AstType.MakeReferenceType(type, start_loc);
				
				var name = type.Name;
				if(class_and_func_type_params.Any(param => param.Name == name)){
				   var param_type = class_and_func_type_params.First(arg => arg.Name == name);
				   type = param_type.Clone();
				}
				
				break;
			}
			default: SynErr(149); break;
			}
			start_loc = NextLocation; 
			if (IsGenericTypeSignature()) {
				GenericTypeSignature(is_reference, ref type);
				
			}
			while (IsArrayTypeSignature()) {
				Expect(9);
				Expect(13);
				type = AstType.MakeSimpleType("array", start_loc, CurrentEndLocation, type);
				
			}
			var replacer = new ParameterTypeReplacer(class_and_func_type_params);
			var dummy = AstNode.MakeIdentifier("dummy", type);
			type.AcceptTypeWalker(replacer);
			type.Remove();
			
		} else if (la.kind == 18) {
			FunctionTypeSignature(out type);
		} else SynErr(150);
	}

	void TypeConstraints(List<TypeConstraint> constraints ) {
		TypeConstraint constraint = null; 
		TypeConstraint(out constraint);
		constraints.Add(constraint); 
		while (la.kind == 62) {
			TypeConstraint(out constraint);
			constraints.Add(constraint); 
		}
	}

	void MethodSignature(out EntityDeclaration method, AttributeSection attribute) {
		Identifier ident = null; PostModifiers post_modifiers = null;
		string name = null; AstType type = null;
		var type_params = new List<ParameterType>(class_type_params);
		var @params = new List<ParameterDeclaration>();
		var start_loc = NextLocation;
		var type_constraints = new List<TypeConstraint>();
		var replacer = new ParameterTypeReplacer(type_params);
		
		while (!(la.kind == 0 || la.kind == 47)) {SynErr(151); Get();}
		Expect(47);
		Symbols.AddScope();
		var ident_start_loc = NextLocation;
		
		Expect(19);
		name = t.val;
		if(!CheckKeyword(name)){
		   ident = AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(CurrentLocation), ExpressoModifiers.Public, null, ident_start_loc); 
		   Symbols.AddSymbol(name, ident);
		}else{
		   // The name is unsuitable for a method or a function name.
		   // Leave the parser to recover its state.
		}
		
		if (la.kind == 8) {
			GenericTypeParameters(type_params);
		}
		Expect(7);
		GoDownScope();
		Symbols.Name = $"method_signature_{name}`{ScopeId++}";
		
		if (la.kind == 19 || la.kind == 39) {
			ParamList(type_params, @params);
		}
		Expect(10);
		Expect(48);
		Type(out type);
		if (la.kind == 45) {
			SignaturePostModifiers(out post_modifiers);
		}
		var cloned = (PostModifiers)post_modifiers?.Clone();
		ident.PostModifiers = cloned;
		
		if (la.kind == 62) {
			TypeConstraints(type_constraints);
		}
		if(!type_constraints.Any()){
		   type_constraints = type_params.Where(p => !class_type_params.Any(tp => tp.Name == p.Name))
		                                 .Select(p => AstNode.MakeTypeConstraint(p.Clone(), null)).ToList();
		}
		
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(152); Get();}
		Expect(5);
		method = EntityDeclaration.MakeFunc(ident, @params, null, type, ExpressoModifiers.Public, post_modifiers, type_constraints, attribute, start_loc);
		method.ReturnType.AcceptTypeWalker(replacer);
		GoUpScope();
		
	}

	void SignaturePostModifiers(out PostModifiers postModifiers) {
		string name; Identifier ident; var ins = new List<Identifier>(); var start_loc = NextLocation; 
		Expect(45);
		Expect(19);
		name = t.val; 
		Expect(46);
		ident = AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(CurrentLocation), ExpressoModifiers.None, null, start_loc);
		ins.Add(ident);
		
		while (WeakSeparator(14,7,8) ) {
			start_loc = NextLocation; 
			Expect(19);
			name = t.val; 
			Expect(46);
			ident = AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(CurrentLocation), ExpressoModifiers.None, null, start_loc);
			ins.Add(ident);
			
		}
		postModifiers = AstNode.MakePostModifiers(null, null, ins, start_loc, CurrentEndLocation); 
	}

	void ParamList(List<ParameterType> typeParams, List<ParameterDeclaration> @params ) {
		ParameterDeclaration param; bool seen_option = false; bool seen_variadic = false; var replacer = new ParameterTypeReplacer(typeParams); 
		Parameter(out param);
		if(!param.Option.IsNull)
		   seen_option = true;
		else if(param.IsVariadic)
		   seen_variadic = true;
		
		param.ReturnType.AcceptTypeWalker(replacer);
		@params.Add(param);
		
		while (WeakSeparator(14,9,10) ) {
			Parameter(out param);
			if(seen_option && param.Option.IsNull)
			   SemanticError("ES0002", "You cannot put optional parameters before non-optional parameters.");
			else if(seen_variadic)
			   SemanticError("ES0010", "The variadic parameter has to be placed in the last position of a parameter list.");
			else if(param.IsVariadic)
			   seen_variadic = true;
			else if(!seen_option && !param.Option.IsNull)
			   seen_option = true;
			
			param.ReturnType.AcceptTypeWalker(replacer);
			@params.Add(param);
			
		}
	}

	void Modifiers() {
		switch (la.kind) {
		case 49: {
			Get();
			cur_modifiers &= ~ExpressoModifiers.Private;
			cur_modifiers |= ExpressoModifiers.Public;
			
			break;
		}
		case 50: {
			Get();
			cur_modifiers &= ~ExpressoModifiers.Private;
			cur_modifiers |= ExpressoModifiers.Protected;
			
			break;
		}
		case 51: {
			Get();
			cur_modifiers |= ExpressoModifiers.Private; 
			break;
		}
		case 52: {
			Get();
			cur_modifiers |= ExpressoModifiers.Static; 
			break;
		}
		case 53: {
			Get();
			cur_modifiers |= ExpressoModifiers.Mutating; 
			break;
		}
		case 54: {
			Get();
			cur_modifiers |= ExpressoModifiers.Override; 
			break;
		}
		case 55: {
			Get();
			cur_modifiers |= ExpressoModifiers.Virtual; 
			break;
		}
		case 29: {
			Get();
			cur_modifiers |= ExpressoModifiers.Abstract; 
			break;
		}
		default: SynErr(153); break;
		}
	}

	void FieldDecl(out EntityDeclaration field, AttributeSection attribute) {
		PatternWithType typed_pattern; var start_loc = NextLocation; Expression expr;
		var patterns = new List<PatternWithType>(); var exprs = new List<Expression>();
		
		while (!(la.kind == 0 || la.kind == 60 || la.kind == 61)) {SynErr(154); Get();}
		if (la.kind == 60) {
			Get();
			cur_modifiers |= ExpressoModifiers.Immutable; 
		} else if (la.kind == 61) {
			Get();
		} else SynErr(155);
		VarDef(out typed_pattern, out expr);
		patterns.Add(typed_pattern);
		exprs.Add(expr ?? Expression.Null);
		expr = null;
		
		while (la.kind == 14) {
			Get();
			VarDef(out typed_pattern, out expr);
			patterns.Add(typed_pattern);
			exprs.Add(expr ?? Expression.Null);
			expr = null;
			
		}
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(156); Get();}
		Expect(5);
		field = EntityDeclaration.MakeField(patterns, exprs, cur_modifiers, attribute, start_loc, CurrentEndLocation);
		cur_modifiers = ExpressoModifiers.None; 
		
	}

	void TupleStyleEnumMember(out EntityDeclaration entity, AttributeSection attribute) {
		var start_loc = NextLocation; Identifier ident = null; AstType type = null; var types = new List<AstType>(); 
		Expect(19);
		var name = t.val; 
		Expect(7);
		if (StartOf(11)) {
			Type(out type);
			types.Add(type); 
		}
		while (la.kind == 14) {
			Get();
			Type(out type);
			types.Add(type); 
		}
		Expect(10);
		var ident_type = AstType.MakeSimpleType(cur_class_name);
		var pattern_type = AstType.MakeSimpleType("tuple", types);
		ident_type.IdentifierNode.Type = pattern_type;
		ident = AstNode.MakeIdentifier(name, ident_type, ExpressoModifiers.Public, null, start_loc);
		Symbols.AddSymbol(name, ident);
		
		var ident_pat = PatternConstruct.MakeIdentifierPattern(ident);
		var pattern = PatternConstruct.MakePatternWithType(ident_pat, pattern_type.Clone());
		entity = EntityDeclaration.MakeField(pattern, null, ExpressoModifiers.Public, attribute, start_loc, CurrentEndLocation);
		
	}

	void RawValueStyleEnumMember(out EntityDeclaration entity, AttributeSection attribute, ref int rawValue) {
		var start_loc = NextLocation; string name; Identifier ident = null; Expression expr = null; 
		Expect(19);
		name = t.val;
		var ident_type = AstType.MakeSimpleType(cur_class_name);
		var type = AstType.MakePrimitiveType("int", NextLocation);
		var modifiers = ExpressoModifiers.Public | ExpressoModifiers.Static | ExpressoModifiers.Immutable;
		ident_type.IdentifierNode.Type = type;
		ident = AstNode.MakeIdentifier(name, ident_type, modifiers, null, start_loc);
		Symbols.AddSymbol(name, ident);
		
		var ident_pat = PatternConstruct.MakeIdentifierPattern(ident);
		var pattern = PatternConstruct.MakePatternWithType(ident_pat, type.Clone());
		
		if (la.kind == 17) {
			Get();
			Literal(out expr);
			if(expr is LiteralExpression raw_value_literal && raw_value_literal.Value is int raw_value)
			   rawValue = raw_value;
			else
			   SemanticError(start_loc, "ES0140", $"The raw value enum initializer should be an integer literal expression on {ident}.");
			
		}
		if(expr == null)
		   expr = Expression.MakeConstant("int", rawValue);
		
		++rawValue;
		entity = EntityDeclaration.MakeField(pattern, expr, modifiers, attribute, start_loc, CurrentEndLocation);
		
	}

	void PostModifiers(out PostModifiers postModifiers) {
		string name = null; var start_loc = NextLocation; var friends = new List<Identifier>(); var beforehands = new List<Identifier>(); 
		if (la.kind == 45) {
			Get();
			Expect(19);
			name = t.val; 
			Expect(56);
			beforehands.Add(AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(), ExpressoModifiers.None, null, start_loc)); 
			while (WeakSeparator(14,7,12) ) {
				start_loc = NextLocation; 
				Expect(19);
				name = t.val; 
				Expect(56);
				beforehands.Add(AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(), ExpressoModifiers.None, null, start_loc)); 
			}
		} else if (la.kind == 57) {
			Get();
			Expect(58);
			Expect(19);
			friends.Add(AstNode.MakeIdentifier(t.val, AstType.MakePlaceholderType(), ExpressoModifiers.None, null, start_loc)); 
			while (WeakSeparator(14,7,12) ) {
				Expect(19);
				friends.Add(AstNode.MakeIdentifier(t.val, AstType.MakePlaceholderType(), ExpressoModifiers.None, null, start_loc)); 
			}
		} else SynErr(157);
		postModifiers = AstNode.MakePostModifiers(friends, beforehands, null, start_loc, CurrentEndLocation); 
	}

	void Block(out BlockStatement block) {
		List<Statement> stmts = new List<Statement>();
		Statement stmt; var start_loc = NextLocation;
		Symbols.AddScope();
		
		Expect(6);
		GoDownScope();
		Symbols.Name = $"block`{ScopeId++}";
		
		Stmt(out stmt);
		stmts.Add(stmt); 
		while (StartOf(13)) {
			Stmt(out stmt);
			stmts.Add(stmt); 
		}
		while (!(la.kind == 0 || la.kind == 11)) {SynErr(158); Get();}
		Expect(11);
		block = Statement.MakeBlock(stmts, start_loc, CurrentEndLocation);
		GoUpScope();
		
	}

	void Parameter(out ParameterDeclaration param) {
		string name; Identifier identifier; Expression option = null; AstType type = null; bool is_variadic = false;
		var start_loc = NextLocation; AttributeSection attribute = null;
		
		if (la.kind == 39) {
			AttributeSection(out attribute);
		}
		Expect(19);
		name = t.val;
		if(CheckKeyword(name)){
		// Failed to parse a name.
		// Stop parsing parameters.
		param = null;
		return;
		}
		
		                                 var (has_symbol, is_in_module) = Symbols.HasSymbolInAnyScope(name);
		                                 if(defining_closure_parameters && has_symbol && !is_in_module)
		                                     SemanticError(start_loc, "ES0160", $"There's already another identifier named '{name}' in scope {Symbols.Name}.");
		
		if (la.kind == 2) {
			Get();
			is_variadic = true; 
		}
		if (la.kind == 7) {
			Get();
			Expect(59);
			Type(out type);
			if(is_variadic && (type == null || !(type is SimpleType simple) || simple.Name != "array"))
			SemanticError(start_loc, "ES0001", "The variadic parameter must be an array!");
			
		}
		if (la.kind == 17) {
			Get();
			Literal(out option);
		}
		if(!defining_closure_parameters && type == null && option == null)
		   SemanticError("ES0004", $"You cannot omit both the type annotation and the optional value in a function parameter definition!; '{name}'.");
		   
		var modifiers = (type is ReferenceType) ? ExpressoModifiers.None : ExpressoModifiers.Immutable;
		identifier = AstNode.MakeIdentifier(name, type ?? new PlaceholderType(CurrentLocation), modifiers, null, start_loc);
		
		Symbols.AddSymbol(name, identifier);
		param = EntityDeclaration.MakeParameter(identifier, option, attribute, is_variadic, start_loc);
		
	}

	void Literal(out Expression expr) {
		expr = null; string tmp;
		  var start_loc = NextLocation;
		
		switch (la.kind) {
		case 20: {
			Get();
			expr = CreateLiteral(t.val, start_loc); 
			break;
		}
		case 22: {
			Get();
			expr = Expression.MakeConstant("int", Convert.ToInt32(t.val, 16), start_loc); 
			break;
		}
		case 21: {
			Get();
			expr = CreateLiteral(t.val, start_loc); 
			break;
		}
		case 24: {
			Get();
			tmp = t.val;
			tmp = tmp.Substring(1, tmp.Length - 2);
			expr = HandleEscapes("char", tmp, start_loc);
			
			break;
		}
		case 25: {
			Get();
			tmp = t.val;
			tmp = tmp.Substring(1, tmp.Length - 2);
			expr = HandleEscapes("string", tmp, start_loc);
			
			break;
		}
		case 26: {
			Get();
			tmp = t.val.Substring(1);
			if(tmp.StartsWith("#", StringComparison.CurrentCulture)){
			   int index_double_quote = tmp.IndexOf('"');
			   int num_start_hashes = tmp.Substring(0, index_double_quote).Length;
			   int index_end_double_quote = tmp.LastIndexOf('"');
			   int num_end_hashes = tmp.Substring(index_end_double_quote + 1).Length;
			   if(num_start_hashes != num_end_hashes)
			       SemanticError(start_loc, "ES0005", "The number of opening and closing hash symbols in a raw string must match!");
			
			   tmp = tmp.Substring(index_double_quote, tmp.Length - num_start_hashes - num_end_hashes);
			}
			tmp = tmp.Substring(1, tmp.Length - 2);
			expr = Expression.MakeConstant("string", tmp, start_loc);
			
			break;
		}
		case 118: {
			Get();
			expr = Expression.MakeConstant("bool", true, start_loc); 
			break;
		}
		case 119: {
			Get();
			expr = Expression.MakeConstant("bool", false, start_loc); 
			break;
		}
		case 121: {
			SelfReferenceExpression(out expr);
			break;
		}
		case 122: {
			SuperReferenceExpression(out expr);
			break;
		}
		case 120: {
			Get();
			expr = Expression.MakeNullRef(start_loc); 
			break;
		}
		default: SynErr(159); break;
		}
	}

	void VarDef(out PatternWithType typed_pattern, out Expression option) {
		option = null; var loc = NextLocation; var replacer = new ParameterTypeReplacer(class_type_params);
		is_defining_variable = true;
		
		PatternWithType(out typed_pattern);
		if (la.kind == 17) {
			Get();
			CondExpr(out option);
		}
		if(typed_pattern.Pattern is IdentifierPattern ident_pat){
		   var name = ident_pat.Identifier.Name;
		   if(typed_pattern.Type is PlaceholderType && option == null)
		       SemanticError(loc, "ES0003", $"Give me some context or I cannot infer the type of '{name}'.");
		
		   ident_pat.Identifier.Type.AcceptTypeWalker(replacer);
		   typed_pattern.Type.AcceptTypeWalker(replacer);
		}
		
		is_defining_variable = false;
		
	}

	void TypeConstraint(out TypeConstraint constraint) {
		AstType type = null; var types = new List<AstType>(); var start_loc = NextLocation; 
		Expect(62);
		Expect(19);
		var param_type = AstType.MakeParameterType(t.val); 
		Expect(3);
		Type(out type);
		types.Add(type); 
		while (la.kind == 14) {
			Get();
			Type(out type);
			types.Add(type); 
		}
		constraint = AstNode.MakeTypeConstraint(param_type, types, start_loc); 
	}

	void TupleTypeSignature(out AstType type) {
		var inners = new List<AstType>(); var start_loc = NextLocation; 
		Expect(7);
		if (StartOf(11)) {
			Type(out type);
			inners.Add(type); 
		}
		while (la.kind == 14) {
			Get();
			Type(out type);
			inners.Add(type); 
		}
		Expect(10);
		type = AstType.MakeSimpleType("tuple", inners, start_loc, CurrentEndLocation); 
	}

	void TypePathExpression(out AstType type) {
		Expect(19);
		type = AstType.MakeSimpleType(t.val, CurrentLocation); 
		while (la.kind == 4) {
			Get();
			Expect(19);
			type = AstType.MakeMemberType(type, AstType.MakeSimpleType(t.val, CurrentLocation), CurrentEndLocation); 
		}
	}

	void GenericTypeSignature(bool isReference, ref AstType type) {
		var type_args = new List<AstType>(); AstType child_type; var replacer = new ParameterTypeReplacer(class_type_params); 
		Expect(8);
		Type(out child_type);
		type_args.Add(child_type); 
		while (la.kind == 14) {
			Get();
			Type(out child_type);
			type_args.Add(child_type); 
		}
		Expect(12);
		if(type is SimpleType generic_type){
		   type = AstType.MakeSimpleType(generic_type.Name, type_args, generic_type.StartLocation, CurrentEndLocation);
		   type.AcceptTypeWalker(replacer);
		}else if(type is MemberType member_type){
		   if(member_type.ChildType is SimpleType generic_type2){
		       var new_type = AstType.MakeSimpleType(generic_type2.Name, type_args, generic_type2.StartLocation, CurrentEndLocation);
		       generic_type2.ReplaceWith(new_type);
		       new_type.AcceptTypeWalker(replacer);
		   }
		}
		
		if(isReference)
		   type = AstType.MakeReferenceType(type, CurrentEndLocation);
		
	}

	void FunctionTypeSignature(out AstType type) {
		var start_loc = NextLocation; AstType inner_type; List<AstType> arg_types = new List<AstType>(); 
		Expect(18);
		if (IsStartOfAnotherType()) {
			Type(out inner_type);
			arg_types.Add(inner_type); 
			while (la.kind == 14) {
				Get();
				Type(out inner_type);
				arg_types.Add(inner_type); 
			}
		}
		Expect(18);
		Expect(48);
		Type(out inner_type);
		var type_params = arg_types.OfType<ParameterType>().Select(pt => pt.Clone()).ToList();
		if(inner_type is ParameterType param_type)
		   type_params.Add(param_type.Clone());
		
		type = AstType.MakeFunctionType("closure", inner_type, arg_types, type_params, start_loc, CurrentEndLocation);
		
	}

	void GenericTypeArguments(List<KeyValueType> typeArgs ) {
		AstType type = null; ParameterType param_type = null; 
		Expect(8);
		if(param_type == null)
		   param_type = AstType.MakeParameterType("");
		
		Type(out type);
		typeArgs.Add(AstType.MakeKeyValueType(param_type, type)); 
		while (la.kind == 14) {
			Get();
			param_type = null; 
			if(param_type == null)
			   param_type = AstType.MakeParameterType("");
			
			Type(out type);
			typeArgs.Add(AstType.MakeKeyValueType(param_type, type)); 
		}
		Expect(12);
	}

	void Stmt(out Statement stmt) {
		stmt = null; 
		if (StartOf(14)) {
			SimpleStmt(out stmt);
		} else if (StartOf(15)) {
			CompoundStmt(out stmt);
		} else SynErr(160);
	}

	void SimpleStmt(out Statement stmt) {
		var start_loc = NextLocation; stmt = null; 
		switch (la.kind) {
		case 19: case 121: case 122: {
			ExprStmt(out stmt);
			break;
		}
		case 60: case 61: {
			VarDeclStmt(out stmt);
			break;
		}
		case 38: {
			ReturnStmt(out stmt);
			break;
		}
		case 78: {
			BreakStmt(out stmt);
			break;
		}
		case 80: {
			ContinueStmt(out stmt);
			break;
		}
		case 81: {
			YieldStmt(out stmt);
			break;
		}
		case 82: {
			ThrowStmt(out stmt);
			break;
		}
		case 5: {
			EmptyStmt(out stmt);
			break;
		}
		default: SynErr(161); break;
		}
	}

	void CompoundStmt(out Statement stmt) {
		stmt = null; BlockStatement block = null; 
		switch (la.kind) {
		case 6: {
			Block(out block);
			stmt = block; 
			break;
		}
		case 94: {
			IfStmt(out stmt);
			break;
		}
		case 96: {
			WhileStmt(out stmt);
			break;
		}
		case 97: {
			DoWhileStmt(out stmt);
			break;
		}
		case 27: {
			ForStmt(out stmt);
			break;
		}
		case 98: {
			MatchStmt(out stmt);
			break;
		}
		case 99: {
			TryStmt(out stmt);
			break;
		}
		default: SynErr(162); break;
		}
	}

	void ExprStmt(out Statement stmt) {
		SequenceExpression lhs = null, seq = null;
		var start_loc = NextLocation; stmt = null;
		OperatorType op_type = OperatorType.None;
		        AssignmentExpression assign = null;
		
		LValueList(out lhs);
		if (StartOf(16)) {
			if (StartOf(17)) {
				AugmentedAssignOperators(ref op_type);
				RValueList(out seq);
				if(lhs.Count != seq.Count)  //See if both sides have the same number of items or not
				   SemanticError("ES0008", "An augmented assignment must have both sides balanced.");
				
				stmt = Statement.MakeAugmentedAssignment(op_type, lhs, seq, start_loc, CurrentEndLocation);
				
			} else {
				Get();
				RValueList(out seq);
				assign = Expression.MakeAssignment(lhs, seq); 
				while (la.kind == 17) {
					Get();
					RValueList(out seq);
					assign = Expression.MakeMultipleAssignment(assign, seq); 
				}
				stmt = Statement.MakeExprStmt(assign, start_loc, CurrentEndLocation); 
			}
		}
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(163); Get();}
		Expect(5);
		if(stmt == null)
		stmt = Statement.MakeExprStmt(lhs, start_loc, CurrentEndLocation);
		
	}

	void VarDeclStmt(out Statement stmt) {
		Expression rhs = null; var start_loc = NextLocation;
		PatternWithType pattern; 
		var patterns = new List<PatternWithType>(); var exprs = new List<Expression>();
		cur_modifiers = ExpressoModifiers.None;
		
		if (la.kind == 60) {
			Get();
			cur_modifiers = ExpressoModifiers.Immutable; 
		} else if (la.kind == 61) {
			Get();
		} else SynErr(164);
		VarDef(out pattern, out rhs);
		patterns.Add(pattern);
		exprs.Add(rhs ?? Expression.Null);
		rhs = null;
		
		while (WeakSeparator(14,18,19) ) {
			VarDef(out pattern, out rhs);
			patterns.Add(pattern);
			exprs.Add(rhs ?? Expression.Null);
			rhs = null;
			
		}
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(165); Get();}
		Expect(5);
		stmt = Statement.MakeVarDecl(patterns, exprs, cur_modifiers, start_loc, CurrentEndLocation); 
	}

	void ReturnStmt(out Statement stmt) {
		SequenceExpression items = null; var start_loc = NextLocation; 
		Expect(38);
		if (StartOf(20)) {
			RValueList(out items);
		}
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(166); Get();}
		Expect(5);
		stmt = Statement.MakeReturnStmt(items, start_loc); 
	}

	void BreakStmt(out Statement stmt) {
		int count = 1; var start_loc = NextLocation; 
		Expect(78);
		if (la.kind == 79) {
			Get();
			Expect(20);
			count = Convert.ToInt32(t.val); 
		}
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(167); Get();}
		Expect(5);
		stmt = Statement.MakeBreakStmt(Expression.MakeConstant("int", count, start_loc), start_loc, CurrentEndLocation); 
	}

	void ContinueStmt(out Statement stmt) {
		int count = 1; var start_loc = NextLocation; 
		Expect(80);
		if (la.kind == 79) {
			Get();
			Expect(20);
			count = Convert.ToInt32(t.val); 
		}
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(168); Get();}
		Expect(5);
		stmt = Statement.MakeContinueStmt(Expression.MakeConstant("int", count, start_loc), start_loc, CurrentEndLocation); 
	}

	void YieldStmt(out Statement stmt) {
		Expression expr; var start_loc = NextLocation; 
		Expect(81);
		CondExpr(out expr);
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(169); Get();}
		Expect(5);
		stmt = Statement.MakeYieldStmt(expr, start_loc, CurrentEndLocation); 
	}

	void ThrowStmt(out Statement stmt) {
		Expression obj; var start_loc = NextLocation; AstType type_path; 
		Expect(82);
		Type(out type_path);
		ObjectCreation(type_path, null, out obj);
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(170); Get();}
		Expect(5);
		stmt = Statement.MakeThrowStmt((ObjectCreationExpression)obj, start_loc); 
	}

	void EmptyStmt(out Statement stmt) {
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(171); Get();}
		Expect(5);
		stmt = Statement.MakeEmptyStmt(CurrentLocation); 
	}

	void RValueList(out SequenceExpression seq) {
		Expression tmp; var exprs = new List<Expression>(); 
		CondExpr(out tmp);
		exprs.Add(tmp); 
		while (la.kind == 14) {
			Get();
			CondExpr(out tmp);
			exprs.Add(tmp);	
		}
		seq = Expression.MakeSequenceExpression(exprs); 
	}

	void CondExpr(out Expression expr) {
		Expression true_expr, false_expr; expr = null; 
		if (StartOf(21)) {
			OrTest(out expr);
			if (la.kind == 102) {
				Get();
				OrTest(out true_expr);
				Expect(3);
				CondExpr(out false_expr);
				expr = Expression.MakeCondExpr(expr, true_expr, false_expr); 
			}
		} else if (la.kind == 18) {
			ClosureLiteral(out expr);
		} else SynErr(172);
	}

	void AugmentedAssignOperators(ref OperatorType type) {
		switch (la.kind) {
		case 83: {
			Get();
			type = OperatorType.Plus; 
			break;
		}
		case 84: {
			Get();
			type = OperatorType.Minus; 
			break;
		}
		case 85: {
			Get();
			type = OperatorType.Times; 
			break;
		}
		case 86: {
			Get();
			type = OperatorType.Divide; 
			break;
		}
		case 87: {
			Get();
			type = OperatorType.Power; 
			break;
		}
		case 88: {
			Get();
			type = OperatorType.Modulus; 
			break;
		}
		case 89: {
			Get();
			type = OperatorType.BitwiseAnd; 
			break;
		}
		case 90: {
			Get();
			type = OperatorType.BitwiseOr; 
			break;
		}
		case 91: {
			Get();
			type = OperatorType.ExclusiveOr; 
			break;
		}
		case 92: {
			Get();
			type = OperatorType.BitwiseShiftLeft; 
			break;
		}
		case 93: {
			Get();
			type = OperatorType.BitwiseShiftRight; 
			break;
		}
		default: SynErr(173); break;
		}
	}

	void LValueList(out SequenceExpression lhs) {
		var lvalues = new List<Expression>(); Expression tmp; 
		LhsPrimary(out tmp);
		lvalues.Add(tmp); 
		while (WeakSeparator(14,22,23) ) {
			LhsPrimary(out tmp);
			lvalues.Add(tmp); 
		}
		lhs = Expression.MakeSequenceExpression(lvalues); 
	}

	void LhsPrimary(out Expression expr) {
		expr = null; PathExpression path; 
		if (la.kind == 19) {
			PathExpression(out path);
			expr = path; 
		} else if (la.kind == 121) {
			SelfReferenceExpression(out expr);
		} else if (la.kind == 122) {
			SuperReferenceExpression(out expr);
		} else SynErr(174);
		while (StartOf(24)) {
			Trailer(ref expr);
		}
	}

	void IfStmt(out Statement stmt) {
		PatternConstruct pattern = null; BlockStatement true_block = null, false_block = null;
		      var start_loc = NextLocation; Statement else_stmt = null;
		   
		Expect(94);
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = $"if`{ScopeId++}";
		
		if (StartOf(25)) {
			ExpressionPattern(out pattern);
		} else if (la.kind == 60 || la.kind == 61) {
			ValueBindingPattern(out pattern);
		} else SynErr(175);
		Block(out true_block);
		if (la.kind == 95) {
			Get();
			if (la.kind == 6) {
				Block(out false_block);
			} else if (la.kind == 94) {
				IfStmt(out else_stmt);
			} else SynErr(176);
		}
		stmt = Statement.MakeIfStmt(pattern, true_block, false_block ?? else_stmt, start_loc);
		       GoUpScope();
		    
	}

	void WhileStmt(out Statement stmt) {
		Expression cond; BlockStatement body; var start_loc = NextLocation; 
		Expect(96);
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = $"while`{ScopeId++}";
		
		CondExpr(out cond);
		Block(out body);
		stmt = Statement.MakeWhileStmt(cond, body, start_loc);
		GoUpScope();
		
	}

	void DoWhileStmt(out Statement stmt) {
		var start_loc = NextLocation; Expression expr = null; BlockStatement body = null; 
		Expect(97);
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = $"do-while`{ScopeId++}";
		
		Block(out body);
		Expect(96);
		CondExpr(out expr);
		while (!(la.kind == 0 || la.kind == 5)) {SynErr(177); Get();}
		Expect(5);
		stmt = Statement.MakeDoWhileStmt(expr, body, start_loc, CurrentEndLocation);
		GoUpScope();
		
	}

	void ForStmt(out Statement stmt) {
		PatternWithType left = null; Expression rvalue; BlockStatement body; cur_modifiers = ExpressoModifiers.None;
		     var start_loc = NextLocation; bool has_variable = false;
		
		Expect(27);
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = $"for`{ScopeId++}";
		
		if (la.kind == 60 || la.kind == 61) {
			if (la.kind == 60) {
				Get();
				cur_modifiers = ExpressoModifiers.Immutable; 
			} else {
				Get();
			}
		}
		has_variable = true; 
		PatternWithType(out left);
		Expect(46);
		CondExpr(out rvalue);
		Block(out body);
		if(has_variable)
		   stmt = Statement.MakeValueBindingForStmt(cur_modifiers, left, rvalue, body, start_loc);
		else
		   stmt = Statement.MakeForStmt(left, rvalue, body, start_loc);
		
		                        GoUpScope();
		                        cur_modifiers = ExpressoModifiers.None;
		                     
	}

	void MatchStmt(out Statement stmt) {
		List<MatchPatternClause> matches; var start_loc = NextLocation; Expression expr; 
		Expect(98);
		CondExpr(out expr);
		Expect(6);
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = $"match`{ScopeId++}";
		
		MatchPatternList(out matches);
		Expect(11);
		stmt = Statement.MakeMatchStmt(expr, matches, start_loc, CurrentEndLocation);
		GoUpScope();
		
	}

	void TryStmt(out Statement stmt) {
		var start_loc = NextLocation; BlockStatement body; var catches = new List<CatchClause>(); FinallyClause @finally = null; 
		Expect(99);
		Block(out body);
		if (la.kind == 100) {
			CatchClauses(out catches);
		}
		if (la.kind == 101) {
			FinallyClause(out @finally);
		}
		if(catches.Count == 0 && @finally == null){
		   SemanticError(start_loc, "ES0007", "At least a try statement must have either a catch clause or the finally clause.");
		}
		stmt = Statement.MakeTryStmt(body, catches, @finally, start_loc);
		
	}

	void ExpressionPattern(out PatternConstruct pattern) {
		Expression expr, true_expr; PatternConstruct false_pattern; pattern = null; 
		PatternOrTest(out expr);
		if (la.kind == 102) {
			Get();
			PatternOrTest(out true_expr);
			Expect(3);
			ExpressionPattern(out false_pattern);
			pattern = PatternConstruct.MakeExpressionPattern(Expression.MakeCondExpr(expr, true_expr, ((ExpressionPattern)false_pattern).Expression)); 
		}
		if(pattern == null)
		   pattern = PatternConstruct.MakeExpressionPattern(expr);
		
	}

	void ValueBindingPattern(out PatternConstruct pattern) {
		PatternWithType pattern_with_type; Expression init; var start_loc = NextLocation;
		cur_modifiers = ExpressoModifiers.None;
		
		if (la.kind == 60) {
			Get();
			cur_modifiers = ExpressoModifiers.Immutable; 
		} else if (la.kind == 61) {
			Get();
		} else SynErr(178);
		VarDef(out pattern_with_type, out init);
		pattern = PatternConstruct.MakeValueBindingPattern(pattern_with_type, init, cur_modifiers, start_loc); 
	}

	void PatternWithType(out PatternWithType typed_pattern) {
		typed_pattern = null; var pattern = PatternConstruct.Null; 
		if (la.kind == 103) {
			WildcardPattern(out pattern);
		} else if (IsIdentifierPattern()) {
			IdentifierPattern(out pattern);
		} else if (la.kind == 7) {
			TuplePattern(out pattern);
		} else if (la.kind == 9 || la.kind == 19) {
			DestructuringPattern(out pattern);
		} else SynErr(179);
		AstType type = AstType.MakePlaceholderType(NextLocation); 
		if (la.kind == 7) {
			Get();
			Expect(59);
			Type(out type);
		}
		if(pattern is IdentifierPattern ident_pat)
		   ident_pat.Identifier.Type = type;
		
		typed_pattern = PatternConstruct.MakePatternWithType(pattern, type.Clone());
		
	}

	void MatchPatternList(out List<MatchPatternClause> clauses ) {
		clauses = new List<MatchPatternClause>(); List<PatternConstruct> pattern_list;
		Statement inner; Expression guard;
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = $"match-arm`{ScopeId++}";
		
		PatternList(out pattern_list, out guard);
		MatchArmStmt(out inner);
		clauses.Add(Statement.MakeMatchClause(pattern_list, guard, inner));
		    GoUpScope();
		 
		while (la.kind == 14) {
			while (!(la.kind == 0 || la.kind == 14)) {SynErr(180); Get();}
			Get();
			Symbols.AddScope();
			GoDownScope();
			Symbols.Name = $"match-arm`{ScopeId++}";
			
			PatternList(out pattern_list, out guard);
			MatchArmStmt(out inner);
			clauses.Add(Statement.MakeMatchClause(pattern_list, guard, inner));
			     GoUpScope();
			  
		}
	}

	void PatternList(out List<PatternConstruct> patterns, out Expression guard ) {
		patterns = new List<PatternConstruct>(); PatternConstruct tmp; guard = null; 
		Pattern(out tmp);
		patterns.Add(tmp); 
		while (la.kind == 18) {
			while (!(la.kind == 0 || la.kind == 18)) {SynErr(181); Get();}
			Get();
			Pattern(out tmp);
			patterns.Add(tmp); 
		}
		if (la.kind == 94) {
			Get();
			CondExpr(out guard);
		}
		Expect(16);
	}

	void MatchArmStmt(out Statement stmt) {
		stmt = null; BlockStatement block = null; 
		switch (la.kind) {
		case 6: {
			Block(out block);
			stmt = block; 
			break;
		}
		case 19: case 121: case 122: {
			ExprStmt(out stmt);
			break;
		}
		case 60: case 61: {
			VarDeclStmt(out stmt);
			break;
		}
		case 38: {
			ReturnStmt(out stmt);
			break;
		}
		case 78: {
			BreakStmt(out stmt);
			break;
		}
		case 80: {
			ContinueStmt(out stmt);
			break;
		}
		case 81: {
			YieldStmt(out stmt);
			break;
		}
		case 82: {
			ThrowStmt(out stmt);
			break;
		}
		case 5: {
			EmptyStmt(out stmt);
			break;
		}
		default: SynErr(182); break;
		}
	}

	void Pattern(out PatternConstruct pattern) {
		pattern = null; 
		if (la.kind == 103) {
			WildcardPattern(out pattern);
		} else if (la.kind == 7) {
			TuplePattern(out pattern);
		} else if (IsIdentifierPattern()) {
			IdentifierPattern(out pattern);
		} else if (DistinguishBetweenDestructuringPatternAndEnumMember()) {
			DestructuringPattern(out pattern);
		} else if (StartOf(26)) {
			RValuePattern(out pattern);
		} else if (la.kind == 19) {
			EnumMemberPattern(out pattern);
		} else SynErr(183);
	}

	void CatchClauses(out List<CatchClause> catches ) {
		catches = new List<CatchClause>(); CatchClause @catch; 
		CatchClause(out @catch);
		catches.Add(@catch); 
		while (la.kind == 100) {
			CatchClause(out @catch);
			catches.Add(@catch); 
		}
	}

	void FinallyClause(out FinallyClause @finally) {
		var start_loc = NextLocation; BlockStatement body; 
		Expect(101);
		Block(out body);
		@finally = Statement.MakeFinallyClause(body, start_loc); 
	}

	void CatchClause(out CatchClause @catch) {
		var start_loc = NextLocation; Identifier ident; BlockStatement body; 
		Expect(100);
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = $"catch`{ScopeId++}";
		cur_modifiers = ExpressoModifiers.None;
		
		Identifier(out ident);
		if(ident.Type is PlaceholderType)
		   SemanticError(start_loc, "ES0011", $"A CatchClause identifier has to be explicitly type annotated; {ident.Name}.");
		
		Symbols.AddSymbol(ident.Name, ident);
		
		Block(out body);
		@catch = Statement.MakeCatchClause(ident, body, start_loc);
		GoUpScope();
		
	}

	void Identifier(out Identifier ident) {
		string name; var start_loc = NextLocation; 
		Expect(19);
		name = t.val;
		if(CheckKeyword(t.val)){
		   ident = Ast.Identifier.Null;
		   return;
		}
		AstType type = AstType.MakePlaceholderType(NextLocation);
		
		if (la.kind == 7) {
			Get();
			Expect(59);
			Type(out type);
		}
		ident = AstNode.MakeIdentifier(name, type, cur_modifiers, null, start_loc); 
	}

	void WildcardPattern(out PatternConstruct pattern) {
		var start_loc = NextLocation; 
		Expect(103);
		pattern = PatternConstruct.MakeWildcardPattern(start_loc); 
	}

	void TuplePattern(out PatternConstruct pattern) {
		var inners = new List<PatternConstruct>(); pattern = null; 
		Expect(7);
		TupleElementPattern(out pattern);
		inners.Add(pattern); 
		Expect(14);
		if (StartOf(27)) {
			if (StartOf(28)) {
				TupleElementPattern(out pattern);
			} else {
				Get();
				pattern = PatternConstruct.MakeIgnoringRestPattern(CurrentLocation); 
			}
			inners.Add(pattern); 
		}
		while (la.kind == 14) {
			Get();
			if (StartOf(28)) {
				TupleElementPattern(out pattern);
			} else if (la.kind == 1) {
				Get();
				pattern = PatternConstruct.MakeIgnoringRestPattern(CurrentLocation); 
			} else SynErr(184);
			inners.Add(pattern); 
		}
		Expect(10);
		pattern = PatternConstruct.MakeTuplePattern(inners); 
	}

	void IdentifierPattern(out PatternConstruct pattern) {
		PatternConstruct inner = null; string name; var start_loc = NextLocation; 
		Expect(19);
		name = t.val;
		if(CheckKeyword(name)){
		   pattern = PatternConstruct.Null;
		   return;
		}
		var (has_symbol, is_in_module_scope) = Symbols.HasSymbolInAnyScope(name);
		if(is_defining_variable && has_symbol && !is_in_module_scope)
		   SemanticError(start_loc, "ES0160", $"There's already another identifier named '{name}' in scope {Symbols.Name}.");
		
		var type = new PlaceholderType(NextLocation);
		var ident = AstNode.MakeIdentifier(name, type, cur_modifiers, null, start_loc);
		Symbols.AddSymbol(name, ident);
		
		if (la.kind == 104) {
			Get();
			Pattern(out inner);
		}
		pattern = PatternConstruct.MakeIdentifierPattern(ident, inner); 
	}

	void DestructuringPattern(out PatternConstruct pattern) {
		pattern = PatternConstruct.Null; AstType type_path;
		var patterns = new List<PatternConstruct>(); bool is_vector = false; string key = null;
		
		if (la.kind == 19) {
			TypePathExpression(out type_path);
			Expect(6);
			if (StartOf(29)) {
				if (IsKeyIdentifier()) {
					Expect(19);
					key = t.val; 
					Expect(3);
				}
				Pattern(out pattern);
				if(key != null){
				   pattern = PatternConstruct.MakeKeyValuePattern(key, pattern);
				   key = null;
				}
				
				patterns.Add(pattern);
				
			}
			while (la.kind == 14) {
				Get();
				if (IsKeyIdentifier()) {
					Expect(19);
					key = t.val; 
					Expect(3);
				}
				if (StartOf(29)) {
					Pattern(out pattern);
					if(key != null){
					   pattern = PatternConstruct.MakeKeyValuePattern(key, pattern);
					   key = null;
					}
					
					patterns.Add(pattern);
					
				} else if (la.kind == 1) {
					Get();
					patterns.Add(PatternConstruct.MakeIgnoringRestPattern(CurrentLocation)); 
				} else SynErr(185);
			}
			Expect(11);
			pattern = PatternConstruct.MakeDestructuringPattern(type_path, patterns); 
		} else if (la.kind == 9) {
			Get();
			if (StartOf(29)) {
				Pattern(out pattern);
				patterns.Add(pattern); 
			}
			while (NotFinalComma()) {
				ExpectWeak(14, 30);
				if (StartOf(29)) {
					Pattern(out pattern);
					patterns.Add(pattern); 
				} else if (la.kind == 1) {
					Get();
					patterns.Add(PatternConstruct.MakeIgnoringRestPattern(CurrentLocation)); 
				} else SynErr(186);
			}
			if (la.kind == 14) {
				Get();
				Expect(2);
				is_vector = true; 
			}
			Expect(13);
			pattern = PatternConstruct.MakeCollectionPattern(patterns, is_vector); 
		} else SynErr(187);
	}

	void RValuePattern(out PatternConstruct pattern) {
		Expression expr; 
		LiteralIntSeqExpr(out expr);
		pattern = PatternConstruct.MakeExpressionPattern(expr); 
	}

	void EnumMemberPattern(out PatternConstruct pattern) {
		AstType type_path; 
		TypePathExpression(out type_path);
		pattern = PatternConstruct.MakeTypePathPattern(type_path); 
	}

	void PatternOrTest(out Expression expr) {
		Expression rhs; 
		PatternAndTest(out expr);
		if (la.kind == 18) {
			Get();
			Expect(18);
			PatternOrTest(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.ConditionalOr, expr, rhs); 
		}
	}

	void LiteralIntSeqExpr(out Expression expr) {
		Expression start = null, end = null, step = null; bool upper_inclusive = false; 
		Literal(out start);
		expr = start; 
		if (la.kind == 1 || la.kind == 2) {
			RangeOperator(ref upper_inclusive);
			Literal(out end);
			if (la.kind == 3) {
				Get();
				Literal(out step);
			}
			if(step == null)
			   step = Expression.MakeConstant("int", 1, TextLocation.Empty);
			
			expr = Expression.MakeIntSeq(start, end, step, upper_inclusive);
			
		}
	}

	void TupleElementPattern(out PatternConstruct pattern) {
		pattern = PatternConstruct.Null; 
		if (la.kind == 103) {
			WildcardPattern(out pattern);
		} else if (la.kind == 7) {
			TuplePattern(out pattern);
		} else if (IsIdentifierPattern()) {
			IdentifierPattern(out pattern);
		} else if (IsDestructuringPattern()) {
			DestructuringPattern(out pattern);
		} else if (StartOf(25)) {
			ExpressionPattern(out pattern);
		} else SynErr(188);
	}

	void OrTest(out Expression expr) {
		Expression rhs; 
		AndTest(out expr);
		if (la.kind == 18) {
			Get();
			Expect(18);
			OrTest(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.ConditionalOr, expr, rhs); 
		}
	}

	void ClosureLiteral(out Expression expr) {
		var parameters = new List<ParameterDeclaration>(); BlockStatement body_block = null;
		var empty_params = new List<ParameterType>(); Expression body_expr; var start_loc = NextLocation;
		
		Expect(18);
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = "closure`" + ScopeId++;
		defining_closure_parameters = true;
		
		if (la.kind == 19 || la.kind == 39) {
			ParamList(empty_params, parameters);
		}
		Expect(18);
		if(la.val != "->" && la.val != "{"){
		   Symbols.AddScope();
		   GoDownScope();
		   Symbols.Name = "block`" + ScopeId++;
		}
		defining_closure_parameters = false;
		AstType return_type = AstType.MakePlaceholderType(CurrentLocation);
		
		if (la.kind == 48) {
			Get();
			Type(out return_type);
			if(la.val != "{"){
			   Symbols.AddScope();
			   GoDownScope();
			   Symbols.Name = "block`" + ScopeId++;
			}
			
		}
		if (IsStartOfBlockScope()) {
			Block(out body_block);
		} else if (StartOf(20)) {
			CondExpr(out body_expr);
			var seq_expr = Expression.MakeSequenceExpression(body_expr);
			body_block = Statement.MakeBlock(Statement.MakeReturnStmt(seq_expr, seq_expr.StartLocation));
			
		} else SynErr(189);
		expr = Expression.MakeClosureExpression(parameters, return_type, body_block, start_loc);
		if(t.val != "}")
		   GoUpScope();
		
		GoUpScope();
		
	}

	void AndTest(out Expression expr) {
		Expression rhs; 
		Comparison(out expr);
		if (la.kind == 105) {
			Get();
			AndTest(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.ConditionalAnd, expr, rhs); 
		}
	}

	void Comparison(out Expression expr) {
		Expression rhs; OperatorType type; 
		IntSeqExpr(out expr);
		type = OperatorType.Equality; 
		if (StartOf(31)) {
			ComparisonOperator(out type);
			Comparison(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void IntSeqExpr(out Expression expr) {
		Expression start = null, end = null, step = null;
		bool upper_inclusive = true; var start_loc = NextLocation;
		
		BitOr(out start);
		expr = start; 
		if (la.kind == 1 || la.kind == 2) {
			RangeOperator(ref upper_inclusive);
			BitOr(out end);
			if (IsIntSeqColon()) {
				Expect(3);
				BitOr(out step);
			}
			if(step == null) step = Expression.MakeConstant("int", 1, TextLocation.Empty);
			expr = Expression.MakeIntSeq(start, end, step, upper_inclusive, start_loc, CurrentEndLocation);
			
		}
	}

	void ComparisonOperator(out OperatorType opType) {
		opType = OperatorType.None; 
		switch (la.kind) {
		case 106: {
			Get();
			opType = OperatorType.Equality; 
			break;
		}
		case 107: {
			Get();
			opType = OperatorType.InEquality; 
			break;
		}
		case 8: {
			Get();
			opType = OperatorType.LessThan; 
			break;
		}
		case 12: {
			Get();
			opType = OperatorType.GreaterThan; 
			break;
		}
		case 108: {
			Get();
			opType = OperatorType.LessThanOrEqual; 
			break;
		}
		case 109: {
			Get();
			opType = OperatorType.GreaterThanOrEqual; 
			break;
		}
		default: SynErr(190); break;
		}
	}

	void BitOr(out Expression expr) {
		Expression rhs; 
		BitXor(out expr);
		if (IsBitOr()) {
			Expect(18);
			BitOr(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.BitwiseOr, expr, rhs); 
		}
	}

	void RangeOperator(ref bool upper_inclusive) {
		if (la.kind == 1) {
			Get();
			upper_inclusive = false; 
		} else if (la.kind == 2) {
			Get();
			upper_inclusive = true; 
		} else SynErr(191);
	}

	void BitXor(out Expression expr) {
		Expression rhs; 
		BitAnd(out expr);
		if (la.kind == 110) {
			Get();
			BitXor(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.ExclusiveOr, expr, rhs); 
		}
	}

	void BitAnd(out Expression expr) {
		Expression rhs; 
		ShiftOp(out expr);
		if (la.kind == 63) {
			Get();
			BitAnd(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.BitwiseAnd, expr, rhs); 
		}
	}

	void ShiftOp(out Expression expr) {
		Expression rhs; OperatorType type; 
		AddOp(out expr);
		if (la.kind == 111 || la.kind == 112) {
			ShiftOperator(out type);
			ShiftOp(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void AddOp(out Expression expr) {
		Expression rhs; OperatorType type; 
		Term(out expr);
		if (la.kind == 59 || la.kind == 113) {
			AdditiveOperator(out type);
			AddOp(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void ShiftOperator(out OperatorType opType) {
		opType = OperatorType.None; 
		if (la.kind == 111) {
			Get();
			opType = OperatorType.BitwiseShiftLeft; 
		} else if (la.kind == 112) {
			Get();
			opType = OperatorType.BitwiseShiftRight; 
		} else SynErr(192);
	}

	void Term(out Expression expr) {
		Expression rhs; OperatorType type; 
		PowerOp(out expr);
		if (la.kind == 43 || la.kind == 114 || la.kind == 115) {
			MultiplicativeOperator(out type);
			Term(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void AdditiveOperator(out OperatorType opType) {
		opType = OperatorType.None; 
		if (la.kind == 113) {
			Get();
			opType = OperatorType.Plus; 
		} else if (la.kind == 59) {
			Get();
			opType = OperatorType.Minus; 
		} else SynErr(193);
	}

	void PowerOp(out Expression expr) {
		Expression rhs; 
		Casting(out expr);
		if (la.kind == 116) {
			Get();
			Casting(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.Power, expr, rhs); 
		}
	}

	void MultiplicativeOperator(out OperatorType opType) {
		opType = OperatorType.None; 
		if (la.kind == 43) {
			Get();
			opType = OperatorType.Times; 
		} else if (la.kind == 114) {
			Get();
			opType = OperatorType.Divide; 
		} else if (la.kind == 115) {
			Get();
			opType = OperatorType.Modulus; 
		} else SynErr(194);
	}

	void Casting(out Expression expr) {
		AstType type_path; 
		Factor(out expr);
		if (la.kind == 42) {
			Get();
			Type(out type_path);
			expr = Expression.MakeCastExpr(expr, type_path); 
		}
	}

	void Factor(out Expression expr) {
		OperatorType type; Expression factor; expr = null; var start_loc = NextLocation; 
		if (StartOf(32)) {
			Primary(out expr);
		} else if (StartOf(33)) {
			UnaryOperator(out type);
			Factor(out factor);
			expr = Expression.MakeUnaryExpr(type, factor, start_loc); 
		} else SynErr(195);
	}

	void Primary(out Expression expr) {
		expr = null; 
		if (la.kind == 19) {
			PathPrimary(out expr);
		} else if (StartOf(34)) {
			Atom(out expr);
		} else if (StartOf(35)) {
			PrimitivePrimary(out expr);
		} else SynErr(196);
		while (IsTrailer()) {
			Trailer(ref expr);
		}
	}

	void UnaryOperator(out OperatorType opType) {
		opType = OperatorType.None; 
		if (la.kind == 59 || la.kind == 113) {
			AdditiveOperator(out opType);
		} else if (la.kind == 117) {
			Get();
			opType = OperatorType.Not; 
		} else if (la.kind == 63) {
			Get();
			opType = OperatorType.Reference; 
		} else SynErr(197);
	}

	void PrimitiveTypes(out PrimitiveType primitiveType) {
		var start_loc = CurrentLocation; 
		switch (la.kind) {
		case 69: {
			Get();
			break;
		}
		case 64: {
			Get();
			break;
		}
		case 65: {
			Get();
			break;
		}
		case 72: {
			Get();
			break;
		}
		case 70: {
			Get();
			break;
		}
		case 71: {
			Get();
			break;
		}
		case 66: {
			Get();
			break;
		}
		case 67: {
			Get();
			break;
		}
		case 68: {
			Get();
			break;
		}
		case 76: {
			Get();
			break;
		}
		default: SynErr(198); break;
		}
		primitiveType = AstType.MakePrimitiveType(t.val, start_loc); 
	}

	void PathPrimary(out Expression expr) {
		expr = null; PathExpression path = null; AstType type_path = null; var type_args = new List<KeyValueType>(); 
		PathExpression(out path);
		if (ShouldContainGenerics()) {
			GenericTypeArguments(type_args);
		}
		if (IsObjectCreation()) {
			type_path = ConvertPathToType(path); 
			ObjectCreation(type_path, type_args, out expr);
		}
		if(expr == null){
		   path.TypeArguments.AddRange(type_args.Select(ta => ta.ValueType.Clone()));
		   expr = path;
		}
		
	}

	void PathExpression(out PathExpression path) {
		var paths = new List<Identifier>(); var start_loc = NextLocation; 
		Expect(19);
		var name = t.val;
		if(CheckKeyword(name)){
		   path = null;
		   return;
		}
		var ident = AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(CurrentEndLocation), ExpressoModifiers.None, null, start_loc);
		paths.Add(ident);
		
		while (la.kind == 4) {
			Get();
			start_loc = NextLocation; 
			Expect(19);
			name = t.val;
			if(CheckKeyword(name)){
			   path = null;
			   return;
			}
			var ident2 = AstNode.MakeIdentifier(name, AstType.MakePlaceholderType(CurrentEndLocation), ExpressoModifiers.None, null, start_loc);
			paths.Add(ident2);
			
		}
		path = Expression.MakePath(paths); 
	}

	void PrimitivePrimary(out Expression expr) {
		expr = null; PrimitiveType primitive_type = null; 
		PrimitiveTypes(out primitive_type);
		if (la.kind == 6 || la.kind == 9) {
			if (la.kind == 9) {
				ArrayInitializerSuffix(primitive_type, out expr);
			} else {
				ObjectCreation(primitive_type, null, out expr);
			}
		}
		if(expr == null){
		   var ident = AstNode.MakeIdentifier(
		       primitive_type.Name, AstType.MakePlaceholderType(primitive_type.EndLocation), ExpressoModifiers.None, null,
		       primitive_type.StartLocation
		   );
		   expr = Expression.MakePath(ident);
		}
		
	}

	void ArrayInitializerSuffix(PrimitiveType primitiveType, out Expression expr) {
		Expression inner_expr; 
		Expect(9);
		CondExpr(out inner_expr);
		Expect(13);
		expr = Expression.MakeArrayInitializer(primitiveType, inner_expr, CurrentLocation); 
	}

	void Atom(out Expression expr) {
		var exprs = new List<Expression>(); expr = null; bool seen_trailing_comma = false; var start_loc = NextLocation; 
		if (StartOf(26)) {
			Literal(out expr);
		} else if (la.kind == 7) {
			Get();
			if (la.kind == 10) {
				Get();
				expr = Expression.MakeParen(null, start_loc, CurrentEndLocation); 
			} else if (StartOf(20)) {
				CondExpr(out expr);
				exprs.Add(expr); 
				while (NotFinalComma()) {
					ExpectWeak(14, 36);
					CondExpr(out expr);
					exprs.Add(expr); 
				}
				if (la.kind == 14) {
					Get();
					seen_trailing_comma = true; 
				}
				Expect(10);
				if(exprs.Count == 1)
				   expr = Expression.MakeParen(seen_trailing_comma ? Expression.MakeSequenceExpression(exprs[0]) : exprs[0], start_loc, CurrentEndLocation);
				else
				   expr = Expression.MakeParen(Expression.MakeSequenceExpression(exprs), start_loc, CurrentEndLocation);
				
			} else SynErr(199);
		} else if (la.kind == 9) {
			Get();
			if (StartOf(37)) {
				SequenceMaker(out expr);
			}
			while (!(la.kind == 0 || la.kind == 13)) {SynErr(200); Get();}
			Expect(13);
			if(expr == null){
			   var type = CreateTypeWithArgs("array", AstType.MakePlaceholderType(start_loc));
			expr = Expression.MakeSequenceInitializer(type, Enumerable.Empty<Expression>(), start_loc, CurrentEndLocation);
			}
			
		} else if (la.kind == 6) {
			Get();
			if (StartOf(21)) {
				DictMaker(out expr);
			}
			while (!(la.kind == 0 || la.kind == 11)) {SynErr(201); Get();}
			Expect(11);
			if(expr == null){
			   var type = CreateTypeWithArgs("dictionary", AstType.MakePlaceholderType(start_loc), AstType.MakePlaceholderType(start_loc));
			   expr = Expression.MakeSequenceInitializer(type, Enumerable.Empty<Expression>(), start_loc, CurrentEndLocation);
			}
			
		} else SynErr(202);
	}

	void Trailer(ref Expression expr) {
		var args = new List<Expression>(); var start_loc = NextLocation; var type_args = new List<KeyValueType>(); 
		if (la.kind == 7 || la.kind == 8) {
			if (la.kind == 8) {
				GenericTypeArguments(type_args);
			}
			Expect(7);
			if (StartOf(20)) {
				ArgList(out args);
			}
			Expect(10);
			expr = Expression.MakeCallExpr(expr, args, type_args, CurrentEndLocation); 
		} else if (la.kind == 9) {
			Get();
			ArgList(out args);
			Expect(13);
			expr = Expression.MakeIndexer(expr, args, CurrentEndLocation); 
		} else if (la.kind == 15) {
			Get();
			Expect(19);
			var ident = AstNode.MakeIdentifier(t.val, AstType.MakePlaceholderType(CurrentEndLocation), ExpressoModifiers.None, null, start_loc);
			expr = Expression.MakeMemRef(expr, ident);
			
		} else SynErr(203);
	}

	void PatternAndTest(out Expression expr) {
		Expression rhs; 
		PatternComparison(out expr);
		if (la.kind == 105) {
			Get();
			PatternAndTest(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.ConditionalAnd, expr, rhs); 
		}
	}

	void PatternComparison(out Expression expr) {
		Expression rhs; OperatorType type = OperatorType.None; 
		PatternIntSeqExpr(out expr);
		if (StartOf(31)) {
			ComparisonOperator(out type);
			PatternComparison(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void PatternIntSeqExpr(out Expression expr) {
		Expression start = null, end = null, step = null;
		bool upper_inclusive = true;
		
		PatternBitOr(out start);
		expr = start; 
		if (la.kind == 1 || la.kind == 2) {
			RangeOperator(ref upper_inclusive);
			PatternBitOr(out end);
			if (IsIntSeqColon()) {
				Expect(3);
				PatternBitOr(out step);
			}
			if(step == null) step = Expression.MakeConstant("int", 1, TextLocation.Empty);
			expr = Expression.MakeIntSeq(start, end, step, upper_inclusive);
			
		}
	}

	void PatternBitOr(out Expression expr) {
		Expression rhs; 
		PatternBitXor(out expr);
		if (IsBitOr()) {
			Expect(18);
			PatternBitOr(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.BitwiseOr, expr, rhs); 
		}
	}

	void PatternBitXor(out Expression expr) {
		Expression rhs; 
		PatternBitAnd(out expr);
		if (la.kind == 110) {
			Get();
			PatternBitXor(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.ExclusiveOr, expr, rhs); 
		}
	}

	void PatternBitAnd(out Expression expr) {
		Expression rhs; 
		PatternShiftOp(out expr);
		if (la.kind == 63) {
			Get();
			PatternBitAnd(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.BitwiseAnd, expr, rhs); 
		}
	}

	void PatternShiftOp(out Expression expr) {
		Expression rhs; OperatorType type; 
		PatternAddOp(out expr);
		if (la.kind == 111 || la.kind == 112) {
			ShiftOperator(out type);
			PatternShiftOp(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void PatternAddOp(out Expression expr) {
		Expression rhs; OperatorType type; 
		PatternTerm(out expr);
		if (la.kind == 59 || la.kind == 113) {
			AdditiveOperator(out type);
			PatternAddOp(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void PatternTerm(out Expression expr) {
		Expression rhs; OperatorType type; 
		PatternPowerOp(out expr);
		if (la.kind == 43 || la.kind == 114 || la.kind == 115) {
			MultiplicativeOperator(out type);
			PatternTerm(out rhs);
			expr = Expression.MakeBinaryExpr(type, expr, rhs); 
		}
	}

	void PatternPowerOp(out Expression expr) {
		Expression rhs; 
		PatternFactor(out expr);
		if (la.kind == 116) {
			Get();
			PatternFactor(out rhs);
			expr = Expression.MakeBinaryExpr(OperatorType.Power, expr, rhs); 
		}
	}

	void PatternFactor(out Expression expr) {
		OperatorType type; Expression factor; expr = null; var start_loc = NextLocation; 
		if (StartOf(38)) {
			PatternPrimary(out expr);
		} else if (StartOf(33)) {
			UnaryOperator(out type);
			PatternFactor(out factor);
			expr = Expression.MakeUnaryExpr(type, factor, start_loc); 
		} else SynErr(204);
	}

	void PatternPrimary(out Expression expr) {
		expr = null; PathExpression path; AstType type_path = null; 
		if (la.kind == 19) {
			PathExpression(out path);
			expr = path; 
		} else if (StartOf(39)) {
			PatternAtom(out expr);
		} else SynErr(205);
		while (IsTrailer()) {
			Trailer(ref expr);
		}
		if (la.kind == 42) {
			Get();
			Type(out type_path);
			expr = Expression.MakeCastExpr(expr, type_path); 
		}
	}

	void PatternAtom(out Expression expr) {
		expr = null; var start_loc = NextLocation; 
		if (StartOf(26)) {
			Literal(out expr);
		} else if (la.kind == 7) {
			Get();
			PatternOrTest(out expr);
			Expect(10);
			expr = Expression.MakeParen(expr, start_loc, CurrentEndLocation); 
		} else SynErr(206);
	}

	void SelfReferenceExpression(out Expression expr) {
		var start_loc = NextLocation; 
		Expect(121);
		expr = Expression.MakeSelfRef(start_loc);
		// Don't add self symbol because we only need one ParameterExpression instance per a type.
		//Symbols.AddSymbol(cur_class_name + "self", self_expr.SelfIdentifier);
		
	}

	void SuperReferenceExpression(out Expression expr) {
		var start_loc = NextLocation; 
		Expect(122);
		expr = Expression.MakeSuperRef(start_loc);
		// Don't add super symbol because we only need one ParameterExpression instance per a type.
		//Symbols.AddSymbol(cur_class_name + "super", super_expr.SuperIdentifier);
		
	}

	void ArgList(out List<Expression> args ) {
		args = new List<Expression>(); Expression expr; 
		CondExpr(out expr);
		args.Add(expr); 
		while (la.kind == 14) {
			Get();
			CondExpr(out expr);
			args.Add(expr); 
		}
	}

	void SequenceMaker(out Expression expr) {
		var exprs = new List<Expression>();
		expr = null; ComprehensionIter comp = null;
		string seq_type_name = "array"; var start_loc = NextLocation;
		
		if (la.kind == 2) {
			Get();
			expr = Expression.MakeSequenceInitializer(CreateTypeWithArgs("vector", AstType.MakePlaceholderType(start_loc)), Enumerable.Empty<Expression>(), start_loc, CurrentEndLocation); 
		} else if (StartOf(20)) {
			CondExpr(out expr);
			exprs.Add(expr); 
			if (la.kind == 13 || la.kind == 14) {
				while (NotFinalComma()) {
					ExpectWeak(14, 36);
					CondExpr(out expr);
					exprs.Add(expr); 
				}
				if (la.kind == 14) {
					Get();
					Expect(2);
					seq_type_name = "vector"; 
				}
				var type = CreateTypeWithArgs(seq_type_name, AstType.MakePlaceholderType(start_loc));
				expr = Expression.MakeSequenceInitializer(type, exprs, start_loc, CurrentEndLocation);
				
			} else if (la.kind == 27) {
				CompFor(out comp);
				var type = CreateTypeWithArgs("vector", AstType.MakePlaceholderType(start_loc));
				expr = Expression.MakeComp(expr, (ComprehensionForClause)comp, type);
				GoUpScope();
				is_first_comprehension_for_clause = true;
				
			} else SynErr(207);
		} else SynErr(208);
	}

	void DictMaker(out Expression expr) {
		Expression key, val; var list = new List<KeyValueLikeExpression>();
		      KeyValueLikeExpression pair; ComprehensionIter comp; expr = null; var start_loc = NextLocation;
		      var type = CreateTypeWithArgs("dictionary", AstType.MakePlaceholderType(start_loc), AstType.MakePlaceholderType(start_loc));
		   
		BitOr(out key);
		Expect(3);
		CondExpr(out val);
		pair = Expression.MakeKeyValuePair(key, val);
		list.Add(pair);
		
		if (la.kind == 11 || la.kind == 14) {
			while (WeakSeparator(14,21,40) ) {
				BitOr(out key);
				Expect(3);
				CondExpr(out val);
				pair = Expression.MakeKeyValuePair(key, val);
				list.Add(pair);
				
			}
			expr = Expression.MakeSequenceInitializer(type, list, start_loc, CurrentEndLocation); 
		} else if (la.kind == 27) {
			CompFor(out comp);
			expr = Expression.MakeComp(pair, (ComprehensionForClause)comp, type);
			GoDownScope();
			is_first_comprehension_for_clause = true;
			
		} else SynErr(209);
	}

	void CompFor(out ComprehensionIter expr) {
		Expression rvalue = null; ComprehensionIter body = null; PatternWithType typed_pattern; 
		Expect(27);
		if(is_first_comprehension_for_clause){
		Symbols.AddScope();
		GoDownScope();
		Symbols.Name = "Comprehension`" + ScopeId++;
		is_first_comprehension_for_clause = false;
		}
		
		PatternWithType(out typed_pattern);
		Expect(46);
		CondExpr(out rvalue);
		if (la.kind == 27 || la.kind == 94) {
			CompIter(out body);
		}
		expr = Expression.MakeCompFor(typed_pattern, rvalue, body);
		
	}

	void CompIter(out ComprehensionIter expr) {
		expr = null; 
		if (la.kind == 27) {
			CompFor(out expr);
		} else if (la.kind == 94) {
			CompIf(out expr);
		} else SynErr(210);
	}

	void CompIf(out ComprehensionIter expr) {
		Expression tmp; ComprehensionIter body = null; 
		Expect(94);
		OrTest(out tmp);
		if (la.kind == 27 || la.kind == 94) {
			CompIter(out body);
		}
		expr = Expression.MakeCompIf(tmp, body);
		
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
		Expresso();
		Expect(0);

	}

	public (Expression, IEnumerable<SymbolTable>) ParseExpression()
	{
		la = new Token();
		la.val = "";
		Get();

		Expression expr = null;
		CondExpr(out expr);
		Expect(0);
		if(errors.count > 0)
			throw new FatalError("Invalid syntax found!");

		var closure_tables = Symbols.Children.SkipWhile(s => !s.Name.Contains("closure"));
		return (expr, closure_tables);
	}
	
	static readonly bool[,] set = {
		{_T,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_T, _x,_T,_T,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_x, _T,_x,_x,_x, _T,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_x, _x,_T,_T,_x, _x,_x,_x,_T, _x,_x,_x,_x, _T,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _T,_x,_x,_T, _x,_T,_T,_T, _T,_T,_T,_T, _x,_x,_x,_x, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_T, _T,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_T, _x,_T,_T,_T, _T,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _T,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _T,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_T,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_x,_x,_x, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_T,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_x,_x,_x, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _x},
		{_x,_T,_x,_x, _x,_x,_x,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _x},
		{_T,_T,_x,_x, _x,_T,_x,_T, _x,_T,_x,_T, _x,_T,_T,_x, _x,_x,_T,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_x, _T,_x,_x,_x, _T,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _T,_x,_x,_x, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_T,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_T, _T,_T,_T,_T, _T,_x,_x,_x, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_T,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_T, _T,_T,_T,_T, _T,_x,_x,_x, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_T,_x,_x,_x, _x,_T,_T,_T, _x,_T,_x,_T, _x,_T,_T,_x, _x,_x,_T,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_x, _T,_x,_x,_x, _T,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_x,_x,_x, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_T,_x, _x,_x,_T,_T, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_x,_x,_x, _T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_T,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_T,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x}

	};
} // end Parser


public class Errors {
	public int count = 0;                                    // number of errors detected
	public System.IO.TextWriter errorStream = Console.Out;   // error messages go to this stream
	public string errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text

	public virtual void SynErr (int line, int col, int n) {
		string s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "double_dots expected"; break;
			case 2: s = "triple_dots expected"; break;
			case 3: s = "colon expected"; break;
			case 4: s = "double_colon expected"; break;
			case 5: s = "semicolon expected"; break;
			case 6: s = "lcurly expected"; break;
			case 7: s = "lparen expected"; break;
			case 8: s = "langle_bracket expected"; break;
			case 9: s = "lbracket expected"; break;
			case 10: s = "rparen expected"; break;
			case 11: s = "rcurly expected"; break;
			case 12: s = "rangle_bracket expected"; break;
			case 13: s = "rbracket expected"; break;
			case 14: s = "comma expected"; break;
			case 15: s = "dot expected"; break;
			case 16: s = "thick_arrow expected"; break;
			case 17: s = "equal expected"; break;
			case 18: s = "vertical_pipe expected"; break;
			case 19: s = "ident expected"; break;
			case 20: s = "integer expected"; break;
			case 21: s = "float expected"; break;
			case 22: s = "hex_digit expected"; break;
			case 23: s = "unicode_escape expected"; break;
			case 24: s = "character_literal expected"; break;
			case 25: s = "string_literal expected"; break;
			case 26: s = "raw_string_literal expected"; break;
			case 27: s = "keyword_for expected"; break;
			case 28: s = "\"export\" expected"; break;
			case 29: s = "\"abstract\" expected"; break;
			case 30: s = "\"sealed\" expected"; break;
			case 31: s = "\"asm\" expected"; break;
			case 32: s = "\"module\" expected"; break;
			case 33: s = "\"class\" expected"; break;
			case 34: s = "\"enum\" expected"; break;
			case 35: s = "\"fld\" expected"; break;
			case 36: s = "\"meth\" expected"; break;
			case 37: s = "\"param\" expected"; break;
			case 38: s = "\"return\" expected"; break;
			case 39: s = "\"#[\" expected"; break;
			case 40: s = "\"import\" expected"; break;
			case 41: s = "\"from\" expected"; break;
			case 42: s = "\"as\" expected"; break;
			case 43: s = "\"*\" expected"; break;
			case 44: s = "\"interface\" expected"; break;
			case 45: s = "\"requires\" expected"; break;
			case 46: s = "\"in\" expected"; break;
			case 47: s = "\"def\" expected"; break;
			case 48: s = "\"->\" expected"; break;
			case 49: s = "\"public\" expected"; break;
			case 50: s = "\"protected\" expected"; break;
			case 51: s = "\"private\" expected"; break;
			case 52: s = "\"static\" expected"; break;
			case 53: s = "\"mutating\" expected"; break;
			case 54: s = "\"override\" expected"; break;
			case 55: s = "\"virtual\" expected"; break;
			case 56: s = "\"beforehand\" expected"; break;
			case 57: s = "\"friends\" expected"; break;
			case 58: s = "\"with\" expected"; break;
			case 59: s = "\"-\" expected"; break;
			case 60: s = "\"let\" expected"; break;
			case 61: s = "\"var\" expected"; break;
			case 62: s = "\"where\" expected"; break;
			case 63: s = "\"&\" expected"; break;
			case 64: s = "\"int\" expected"; break;
			case 65: s = "\"uint\" expected"; break;
			case 66: s = "\"bool\" expected"; break;
			case 67: s = "\"float\" expected"; break;
			case 68: s = "\"double\" expected"; break;
			case 69: s = "\"bigint\" expected"; break;
			case 70: s = "\"string\" expected"; break;
			case 71: s = "\"byte\" expected"; break;
			case 72: s = "\"char\" expected"; break;
			case 73: s = "\"vector\" expected"; break;
			case 74: s = "\"dictionary\" expected"; break;
			case 75: s = "\"slice\" expected"; break;
			case 76: s = "\"intseq\" expected"; break;
			case 77: s = "\"void\" expected"; break;
			case 78: s = "\"break\" expected"; break;
			case 79: s = "\"upto\" expected"; break;
			case 80: s = "\"continue\" expected"; break;
			case 81: s = "\"yield\" expected"; break;
			case 82: s = "\"throw\" expected"; break;
			case 83: s = "\"+=\" expected"; break;
			case 84: s = "\"-=\" expected"; break;
			case 85: s = "\"*=\" expected"; break;
			case 86: s = "\"/=\" expected"; break;
			case 87: s = "\"**=\" expected"; break;
			case 88: s = "\"%=\" expected"; break;
			case 89: s = "\"&=\" expected"; break;
			case 90: s = "\"|=\" expected"; break;
			case 91: s = "\"^=\" expected"; break;
			case 92: s = "\"<<=\" expected"; break;
			case 93: s = "\">>=\" expected"; break;
			case 94: s = "\"if\" expected"; break;
			case 95: s = "\"else\" expected"; break;
			case 96: s = "\"while\" expected"; break;
			case 97: s = "\"do\" expected"; break;
			case 98: s = "\"match\" expected"; break;
			case 99: s = "\"try\" expected"; break;
			case 100: s = "\"catch\" expected"; break;
			case 101: s = "\"finally\" expected"; break;
			case 102: s = "\"?\" expected"; break;
			case 103: s = "\"_\" expected"; break;
			case 104: s = "\"@\" expected"; break;
			case 105: s = "\"&&\" expected"; break;
			case 106: s = "\"==\" expected"; break;
			case 107: s = "\"!=\" expected"; break;
			case 108: s = "\"<=\" expected"; break;
			case 109: s = "\">=\" expected"; break;
			case 110: s = "\"^\" expected"; break;
			case 111: s = "\"<<\" expected"; break;
			case 112: s = "\">>\" expected"; break;
			case 113: s = "\"+\" expected"; break;
			case 114: s = "\"/\" expected"; break;
			case 115: s = "\"%\" expected"; break;
			case 116: s = "\"**\" expected"; break;
			case 117: s = "\"!\" expected"; break;
			case 118: s = "\"true\" expected"; break;
			case 119: s = "\"false\" expected"; break;
			case 120: s = "\"null\" expected"; break;
			case 121: s = "\"self\" expected"; break;
			case 122: s = "\"super\" expected"; break;
			case 123: s = "??? expected"; break;
			case 124: s = "invalid ModuleBody"; break;
			case 125: s = "invalid ModuleBody"; break;
			case 126: s = "invalid ModuleModifiers"; break;
			case 127: s = "this symbol not expected in ModuleNameDefinition"; break;
			case 128: s = "this symbol not expected in FuncDecl"; break;
			case 129: s = "invalid FuncDecl"; break;
			case 130: s = "this symbol not expected in ModuleVariableDecl"; break;
			case 131: s = "invalid ModuleVariableDecl"; break;
			case 132: s = "this symbol not expected in ModuleVariableDecl"; break;
			case 133: s = "this symbol not expected in ClassDecl"; break;
			case 134: s = "invalid ClassDecl"; break;
			case 135: s = "this symbol not expected in ClassDecl"; break;
			case 136: s = "this symbol not expected in InterfaceDecl"; break;
			case 137: s = "this symbol not expected in EnumDecl"; break;
			case 138: s = "invalid EnumDecl"; break;
			case 139: s = "this symbol not expected in EnumDecl"; break;
			case 140: s = "invalid AttributeTarget"; break;
			case 141: s = "invalid ObjectCreation"; break;
			case 142: s = "this symbol not expected in ObjectCreation"; break;
			case 143: s = "this symbol not expected in ImportDecl"; break;
			case 144: s = "invalid ImportDecl"; break;
			case 145: s = "this symbol not expected in ImportDecl"; break;
			case 146: s = "invalid ImportPaths"; break;
			case 147: s = "invalid ImportPaths"; break;
			case 148: s = "invalid ImportPaths"; break;
			case 149: s = "invalid Type"; break;
			case 150: s = "invalid Type"; break;
			case 151: s = "this symbol not expected in MethodSignature"; break;
			case 152: s = "this symbol not expected in MethodSignature"; break;
			case 153: s = "invalid Modifiers"; break;
			case 154: s = "this symbol not expected in FieldDecl"; break;
			case 155: s = "invalid FieldDecl"; break;
			case 156: s = "this symbol not expected in FieldDecl"; break;
			case 157: s = "invalid PostModifiers"; break;
			case 158: s = "this symbol not expected in Block"; break;
			case 159: s = "invalid Literal"; break;
			case 160: s = "invalid Stmt"; break;
			case 161: s = "invalid SimpleStmt"; break;
			case 162: s = "invalid CompoundStmt"; break;
			case 163: s = "this symbol not expected in ExprStmt"; break;
			case 164: s = "invalid VarDeclStmt"; break;
			case 165: s = "this symbol not expected in VarDeclStmt"; break;
			case 166: s = "this symbol not expected in ReturnStmt"; break;
			case 167: s = "this symbol not expected in BreakStmt"; break;
			case 168: s = "this symbol not expected in ContinueStmt"; break;
			case 169: s = "this symbol not expected in YieldStmt"; break;
			case 170: s = "this symbol not expected in ThrowStmt"; break;
			case 171: s = "this symbol not expected in EmptyStmt"; break;
			case 172: s = "invalid CondExpr"; break;
			case 173: s = "invalid AugmentedAssignOperators"; break;
			case 174: s = "invalid LhsPrimary"; break;
			case 175: s = "invalid IfStmt"; break;
			case 176: s = "invalid IfStmt"; break;
			case 177: s = "this symbol not expected in DoWhileStmt"; break;
			case 178: s = "invalid ValueBindingPattern"; break;
			case 179: s = "invalid PatternWithType"; break;
			case 180: s = "this symbol not expected in MatchPatternList"; break;
			case 181: s = "this symbol not expected in PatternList"; break;
			case 182: s = "invalid MatchArmStmt"; break;
			case 183: s = "invalid Pattern"; break;
			case 184: s = "invalid TuplePattern"; break;
			case 185: s = "invalid DestructuringPattern"; break;
			case 186: s = "invalid DestructuringPattern"; break;
			case 187: s = "invalid DestructuringPattern"; break;
			case 188: s = "invalid TupleElementPattern"; break;
			case 189: s = "invalid ClosureLiteral"; break;
			case 190: s = "invalid ComparisonOperator"; break;
			case 191: s = "invalid RangeOperator"; break;
			case 192: s = "invalid ShiftOperator"; break;
			case 193: s = "invalid AdditiveOperator"; break;
			case 194: s = "invalid MultiplicativeOperator"; break;
			case 195: s = "invalid Factor"; break;
			case 196: s = "invalid Primary"; break;
			case 197: s = "invalid UnaryOperator"; break;
			case 198: s = "invalid PrimitiveTypes"; break;
			case 199: s = "invalid Atom"; break;
			case 200: s = "this symbol not expected in Atom"; break;
			case 201: s = "this symbol not expected in Atom"; break;
			case 202: s = "invalid Atom"; break;
			case 203: s = "invalid Trailer"; break;
			case 204: s = "invalid PatternFactor"; break;
			case 205: s = "invalid PatternPrimary"; break;
			case 206: s = "invalid PatternAtom"; break;
			case 207: s = "invalid SequenceMaker"; break;
			case 208: s = "invalid SequenceMaker"; break;
			case 209: s = "invalid DictMaker"; break;
			case 210: s = "invalid CompIter"; break;

			default: s = "error " + n; break;
		}
		
		var prev_color = Console.ForegroundColor;
		Console.ForegroundColor = ConsoleColor.Red;
		errorStream.WriteLine(errMsgFormat, line, col, s);
		Console.ForegroundColor = prev_color;
		count++;
	}

	public virtual void SemErr (int line, int col, string s) {
		var prev_color = Console.ForegroundColor;
		Console.ForegroundColor = ConsoleColor.Red;
		errorStream.WriteLine(errMsgFormat, line, col, s);
		Console.ForegroundColor = prev_color;
		count++;
	}
	
	public virtual void SemErr (string s) {
		var prev_color = Console.ForegroundColor;
		Console.ForegroundColor = ConsoleColor.Red;
		errorStream.WriteLine(s);
		Console.ForegroundColor = prev_color;
		count++;
	}
	
	public virtual void Warning (int line, int col, string s) {
		var prev_color = Console.ForegroundColor;
		Console.ForegroundColor = ConsoleColor.Yellow;
		errorStream.WriteLine(errMsgFormat, line, col, s);
		Console.ForegroundColor = prev_color;
	}
	
	public virtual void Warning(string s) {
		var prev_color = Console.ForegroundColor;
		Console.ForegroundColor = ConsoleColor.Yellow;
		errorStream.WriteLine(s);
		Console.ForegroundColor = prev_color;
	}
} // Errors


public class FatalError: Exception {
	public FatalError(string m): base(m) {}
}
}