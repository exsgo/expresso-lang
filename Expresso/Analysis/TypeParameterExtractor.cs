﻿using System;
using System.Collections.Generic;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// This class is responsible for replacing <see cref="SimpleType"/> nodes with <see cref="ParameterType"/> nodes.
    /// </summary>
    public class TypeParameterExtractor : IAstTypeWalker
    {
        public List<ParameterType> ExtractedTypeParameters{get;} = new List<ParameterType>();

        public void VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            parameterType.Type.AcceptTypeWalker(this);
        }

        public void VisitFunctionType(FunctionType funcType)
        {
            funcType.ReturnType.AcceptTypeWalker(this);
            funcType.Parameters.AcceptTypeWalker(this);
        }

        public void VisitKeyValueType(KeyValueType keyValueType)
        {
        }

        public void VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptTypeWalker(this);
            VisitSimpleType(memberType.ChildType);
        }

        public void VisitParameterType(ParameterType paramType)
        {
            ExtractedTypeParameters.Add(paramType.Clone());
        }

        public void VisitPlaceholderType(PlaceholderType placeholderType)
        {
        }

        public void VisitPrimitiveType(PrimitiveType primitiveType)
        {
        }

        public void VisitReferenceType(ReferenceType referenceType)
        {
            throw new InvalidOperationException("Unexpected AstType!");
        }

        public void VisitSimpleType(SimpleType simpleType)
        {
            foreach(var type_arg in simpleType.TypeArguments)
                type_arg.AcceptTypeWalker(this);
        }

        public void VisitNullNode(AstNode nullNode)
        {
            throw new InvalidOperationException("Can not work on that node");
        }
    }
}
