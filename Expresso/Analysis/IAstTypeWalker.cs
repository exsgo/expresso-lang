﻿namespace Expresso.Ast
{
    /// <summary>
    /// Expresso AstTypeWalker interface.
    /// An AstType walker is a class that walks through all nodes that derive from <see cref="AstType"/> and processes each node.
	/// </summary>
    public interface IAstTypeWalker
	{
        void VisitSimpleType(SimpleType simpleType);
        void VisitPrimitiveType(PrimitiveType primitiveType);
        void VisitReferenceType(ReferenceType referenceType);
        void VisitMemberType(MemberType memberType);
        void VisitFunctionType(FunctionType funcType);
        void VisitParameterType(ParameterType paramType);
        void VisitPlaceholderType(PlaceholderType placeholderType);
        void VisitKeyValueType(KeyValueType keyValueType);
        void VisitFunctionParameterType(FunctionParameterType parameterType);

        void VisitNullNode(AstNode nullNode);
	}

    /// <summary>
    /// Expresso AstTypeWalker interface.
    /// An AstType walker is a class that walks through all nodes that derive from <see cref="AstType"/> and processes each node.
	/// </summary>
    public interface IAstTypeWalker<out TResult>
	{
        TResult VisitSimpleType(SimpleType simpleType);
        TResult VisitPrimitiveType(PrimitiveType primitiveType);
        TResult VisitReferenceType(ReferenceType referenceType);
        TResult VisitMemberType(MemberType memberType);
        TResult VisitFunctionType(FunctionType funcType);
        TResult VisitParameterType(ParameterType paramType);
        TResult VisitPlaceholderType(PlaceholderType placeholderType);
        TResult VisitKeyValueType(KeyValueType keyValueType);
        TResult VisitFunctionParameterType(FunctionParameterType parameterType);

        TResult VisitNullNode(AstNode nullNode);
	}

    /// <summary>
    /// Expresso AstTypeWalker interface.
    /// An AstType walker is a class that walks through all nodes that derive from <see cref="AstType"/> and processes each node.
	/// </summary>
    public interface IAstTypeWalker<in TData, out TResult>
	{
        TResult VisitSimpleType(SimpleType simpleType, TData data);
        TResult VisitPrimitiveType(PrimitiveType primitiveType, TData data);
        TResult VisitReferenceType(ReferenceType referenceType, TData data);
        TResult VisitMemberType(MemberType memberType, TData data);
        TResult VisitFunctionType(FunctionType funcType, TData data);
        TResult VisitParameterType(ParameterType paramType, TData data);
        TResult VisitPlaceholderType(PlaceholderType placeholderType, TData data);
        TResult VisitKeyValueType(KeyValueType keyValueType, TData data);
        TResult VisitFunctionParameterType(FunctionParameterType parameterType, TData data);

        TResult VisitNullNode(AstNode nullNode, TData data);
	}
}
