﻿using System;
using System.Collections.Generic;
using System.Linq;
using Expresso.CodeGen;
using Expresso.Runtime.Builtins;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// It's responsible for replacing type parameters of call expressions with concrete types.
    /// </summary>
    public class TypeParameterSubstituter : IAstTypeWalker<AstType, Unit>
    {
        SymbolTable cur_table;
        IEnumerable<KeyValueType> generic_types;

        public TypeParameterSubstituter(SymbolTable symbolTable, IEnumerable<KeyValueType> genericTypes)
        {
            cur_table = symbolTable;
            generic_types = genericTypes;
        }

        public Unit VisitFunctionParameterType(FunctionParameterType parameterType, AstType data)
        {
            parameterType.Type.AcceptTypeWalker(this, data);
            return null;
        }

        public Unit VisitFunctionType(FunctionType funcType, AstType data)
        {
            var data_func_type = data as FunctionType;
            if(data_func_type == null)
                throw new InvalidOperationException("data is expected to be a FunctionType");

            foreach(var pair in funcType.Parameters.Zip(data_func_type.Parameters, (l, r) => new {Parameter = l, Argument = r}))
                pair.Parameter.AcceptTypeWalker(this, pair.Argument);

            funcType.ReturnType.AcceptTypeWalker(this, data_func_type.ReturnType);
            return null;
        }

        public Unit VisitKeyValueType(KeyValueType keyValueType, AstType data)
        {
            return null;
        }

        public Unit VisitMatchClause(MatchPatternClause matchClause, AstType data)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public Unit VisitMemberType(MemberType memberType, AstType data)
        {
            var data_member = data as MemberType;
            if(data_member == null)
                throw new InvalidOperationException("data is expected to be a MemberType");

            VisitSimpleType(memberType.ChildType, data_member.ChildType);
            return null;
        }

        public Unit VisitParameterType(ParameterType paramType, AstType data)
        {
            var type_arg = generic_types.FirstOrDefault(arg => paramType.Name == arg.Name);
            if(type_arg != null && type_arg.ValueType is PlaceholderType){
                // We can't rewrite this as `var cloned = data.Clone()` because then `cloned` can't be ReplaceWith'ed.
                type_arg.ValueType.ReplaceWith(data.Clone());
                // Substitute nested type parameters when they are associated with concrete types
                ExpressoCompilerHelpers.SubstituteGenericTypes(type_arg.ValueType, generic_types);
            }

            return null;
        }

        public Unit VisitPlaceholderType(PlaceholderType placeholderType, AstType data)
        {
            throw new Exception("Unexpected AstType node!");
        }

        public Unit VisitPrimitiveType(PrimitiveType primitiveType, AstType data)
        {
            return null;
        }

        public Unit VisitReferenceType(ReferenceType referenceType, AstType data)
        {
            var data_reference = data as ReferenceType;
            if(data_reference == null)
                throw new InvalidOperationException("data is expected to be a ReferenceType");

            referenceType.BaseType.AcceptTypeWalker(this, data);
            return null;
        }

        public Unit VisitSimpleType(SimpleType simpleType, AstType data)
        {
            var data_simple = data as SimpleType;
            if(data_simple == null){
                var data_table = cur_table.GetTypeTable(data.Name);
                var (matched, matched_type) = data_table.IsMatch(simpleType, null);
                if(matched){
                    data_simple = matched_type;
                }else{
                    throw new InvalidOperationException($"`{data}` is not considered to be derived from `{simpleType}`");
                }
            }

            if(data_simple.Name != simpleType.Name) {
                throw new ParserException(
                    $"The generic type is different from the argument's type: Expected: `{data_simple.Name}`, found: `{simpleType.Name}`.",
                    "ES1210",
                    simpleType
                );
            }

            foreach(var pair in simpleType.TypeArguments.Zip(data_simple.TypeArguments, (l, r) => new {Parameter = l, Argument = r}))
                pair.Parameter.AcceptTypeWalker(this, pair.Argument);

            return null;
        }

        public Unit VisitNullNode(AstNode nullNode, AstType data)
        {
            throw new InvalidOperationException("Can not work on that node");
        }
    }
}
