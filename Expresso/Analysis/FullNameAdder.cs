﻿using System;
using Expresso.CodeGen;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// This class is responsible for adding a full name to <see cref="AstType"/> nodes except for <see cref="KeyValueType"/>, <see cref="ParameterType"/>
    /// and <see cref="PlaceholderType"/>.
    /// </summary>
    public class FullNameAdder : IAstTypeWalker
    {
        SymbolTable table;

        public FullNameAdder(SymbolTable table)
        {
            this.table = table;
        }

        public void VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            parameterType.Type.AcceptTypeWalker(this);
        }

        public void VisitFunctionType(FunctionType funcType)
        {
            foreach(var param in funcType.Parameters)
                param.AcceptTypeWalker(this);

            funcType.ReturnType.AcceptTypeWalker(this);
        }

        public void VisitKeyValueType(KeyValueType keyValueType)
        {
            throw new InvalidOperationException("Unexpected AstType node");
        }

        public void VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptTypeWalker(this);
            VisitSimpleType(memberType.ChildType);
        }

        public void VisitParameterType(ParameterType paramType)
        {
            //throw new InvalidOperationException("Unexpected AstType node");
        }

        public void VisitPlaceholderType(PlaceholderType placeholderType)
        {
            throw new InvalidOperationException("Unexpected AstType node");
        }

        public void VisitPrimitiveType(PrimitiveType primitiveType)
        {
        }

        public void VisitReferenceType(ReferenceType referenceType)
        {
            referenceType.BaseType.AcceptTypeWalker(this);
        }

        public void VisitSimpleType(SimpleType simpleType)
        {
            var symbol = table.GetTypeSymbol(simpleType.Name);
            ExpressoCompilerHelpers.ResolveTypeAlias(simpleType, symbol);

            foreach(var type_arg in simpleType.TypeArguments)
                type_arg.AcceptTypeWalker(this);
        }

        public void VisitNullNode(AstNode nullNode)
        {
            throw new InvalidOperationException("Can not work on that node");
        }
    }
}
