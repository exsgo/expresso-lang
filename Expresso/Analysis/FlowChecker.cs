using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Expresso.CodeGen;
using ICSharpCode.NRefactory.PatternMatching;


/**
 * The data flow.
 * 
 * Each local name is represented as 2 bits:
 * One is for definitive use, and the other is for uninitialized use detection.
 * 
 * The bit arrays in the flow checker hold the state and upon encountering Identifier we figure
 * out whether the name has not yet been initialized at all 
 * or whether it may be uninitialized, in which case we will show a warning.
 * 
 * More details on the bits.
 * 
 * First bit (item is used):
 *  1 .. item has been used at least once
 *  0 .. item is never used at this point
 * Second bit (variable is initialized to a value):
 *  1 .. variable is definitely initialized
 *  0 .. variable is not initialized at this point
 */

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// The flow checker is responsible for verifying correctness of flow control of each path
    /// in declarations, which includes mutability check, variable initialization check and method modifiers check.
    /// </summary>
    class FlowChecker : IAstWalker
	{
        const int BitArraySize = 2;//3;

		BitArray bits;
        Parser parser;
        SymbolTable symbols, cur_focusing_table;
        Dictionary<uint, List<uint>> call_map = new Dictionary<uint, List<uint>>();
        uint cur_func_ident_id;
        int scope_counter;
        bool method_is_mutating, inspecting_lvalue, inspecting_patterns, actually_mutating/*, inspecting_mutable_field*/, inspecting_call_target;
		
        FlowChecker(Parser parser)
		{
            this.parser = parser;
            symbols = parser.Symbols;
            Debug.Assert(symbols.Name == "programRoot", "When creating FlowChecker, symbols should point at 'programRoot'");
            // We have to use this expression because symbols include self and other implicit ones
            bits = new BitArray((int)(UniqueIdGenerator.CurrentId - ExpressoCompilerHelpers.EndOfImportedIdentifierIds) * BitArraySize + (BitArraySize - 1));
		}
		
		/*[Conditional("DEBUG")]
		public void Dump(BitArray bits)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.AppendFormat("FlowChecker ({0})", scope is FunctionDeclaration ? ((FunctionDeclaration)scope).Name :
			                scope is TypeDefinition ? ((TypeDefinition)scope).Name : "");
			sb.Append('{');
			bool comma = false;
			foreach(var binding in variables){
				if(comma) sb.Append(", ");
				else comma = true;

				int index = 2 * binding.Value.Offset;
				sb.AppendFormat("{0}:{1}{2}",
				                binding.Key,
				                bits.Get(index) ? "*" : "-",
				                bits.Get(index + 1) ? "-" : "*");
				if(binding.Value.ReadBeforeInitialized)
					sb.Append("#");
			}
			sb.Append('}');
			Debug.WriteLine(sb.ToString());
		}*/

        void SetCalled(Identifier ident, bool value)
        {
            if(ExpressoCompilerHelpers.StartOfIdentifierId <= ident.IdentifierId && ident.IdentifierId < ExpressoCompilerHelpers.EndOfImportedIdentifierIds)
                return;

            if(ident.IdentifierId == 0)
                throw new ArgumentException($"IdentifierId is invalid: '{ident.Name}'", nameof(ident));

            if(call_map.TryGetValue(cur_func_ident_id, out var call_list) && !call_list.Contains(ident.IdentifierId))
                call_list.Add(ident.IdentifierId);
            // FIXME: Can be deleted: 2019/8/21
            //bits.Set((int)(ident.IdentifierId - ExpressoCompilerHelpers.EndOfImportedIdentifierIds) * BitArraySize, value);
        }

        void SetUsed(Identifier ident, bool value)
		{
            // Temporal implementation
            if(ExpressoCompilerHelpers.StartOfIdentifierId <= ident.IdentifierId && ident.IdentifierId < ExpressoCompilerHelpers.EndOfImportedIdentifierIds)
                return;

            if(ident.IdentifierId == 0)
                throw new ArgumentException($"IdentifierId is invalid: '{ident.Name}'", nameof(ident));

            // IdentifierId starts from 1
            bits.Set((int)(ident.IdentifierId - ExpressoCompilerHelpers.EndOfImportedIdentifierIds) * BitArraySize, value);
		}
		
        void SetInitialized(Identifier ident, bool value)
		{
            // Temporal implementation
            if(ExpressoCompilerHelpers.StartOfIdentifierId <= ident.IdentifierId && ident.IdentifierId < ExpressoCompilerHelpers.EndOfImportedIdentifierIds)
                return;

            if(ident.IdentifierId == 0)
                throw new ArgumentException($"IdentifierId is invalid: '{ident.Name}'", nameof(ident));

            // IdentifierId starts from 1
            bits.Set((int)(ident.IdentifierId - ExpressoCompilerHelpers.EndOfImportedIdentifierIds) * BitArraySize + 1, value);
		}

        bool IsCalled(Identifier ident)
        {
            // It's temporal implementation because otherwise we would look at external symbols
            if(ExpressoCompilerHelpers.StartOfIdentifierId <= ident.IdentifierId && ident.IdentifierId < ExpressoCompilerHelpers.EndOfImportedIdentifierIds)
                return true;

            if(ident.IdentifierId == 0)
                throw new ArgumentException($"IdentifierId is invalid: '{ident.Name}'", nameof(ident));

            if(call_map.TryGetValue(cur_func_ident_id, out var call_list)){
                if(call_list.Contains(ident.IdentifierId))
                    return true;

                var prev_cur_func_ident_id = cur_func_ident_id;
                foreach(var calling_ident_id in call_list){
                    cur_func_ident_id = calling_ident_id;
                    if(IsCalled(ident)){
                        cur_func_ident_id = prev_cur_func_ident_id;
                        return true;
                    }
                }

                cur_func_ident_id = prev_cur_func_ident_id;
                return false;
            }else{
                return false;
            }
            //return bits.Get((int)(ident.IdentifierId - ExpressoCompilerHelpers.EndOfImportedIdentifierIds) * BitArraySize);
        }

        bool IsUsed(Identifier ident)
		{
            // It's temporal implementation because otherwise we would look at external symbols
            if(ExpressoCompilerHelpers.StartOfIdentifierId <= ident.IdentifierId && ident.IdentifierId < ExpressoCompilerHelpers.EndOfImportedIdentifierIds)
                return true;

            if(ident.IdentifierId == 0)
                throw new ArgumentException($"IdentifierId is invalid: '{ident.Name}'", nameof(ident));

            return bits.Get((int)(ident.IdentifierId - ExpressoCompilerHelpers.EndOfImportedIdentifierIds) * BitArraySize);
		}
		
        bool IsInitialized(Identifier ident)
		{
            // Temporal implementation
            if(ExpressoCompilerHelpers.StartOfIdentifierId <= ident.IdentifierId && ident.IdentifierId < ExpressoCompilerHelpers.EndOfImportedIdentifierIds)
                return true;

            if(ident.IdentifierId == 0)
                throw new ArgumentException($"IdentifierId is invalid: '{ident.Name}'", nameof(ident));

            return bits.Get((int)(ident.IdentifierId - ExpressoCompilerHelpers.EndOfImportedIdentifierIds) * BitArraySize + 1);
		}
		
        public static void Check(ExpressoAst ast, Parser parser)
		{
            var checker = new FlowChecker(parser);
            checker.VisitAst(ast);
		}
		
		public void Define(Identifier ident)
		{
            if(ident != null)
                SetInitialized(ident, true);
		}
		
        void DecendScope()
        {
            symbols = symbols.Children[scope_counter];
        }
        
        void AscendScope()
        {
            symbols = symbols.Parent;
        }
		
        #region IAstWalker implementation

        public void VisitAst(ExpressoAst ast)
        {
            Debug.Assert(symbols.Name == "programRoot", "When entering VisitAst, symbols should be 'programRoot'");
            scope_counter = 0;

            Utilities.CompilerOutput.WriteLine($"Checking flow in {ast.ModuleName}...");

            ast.Imports.AcceptWalker(this);
            ast.Declarations.AcceptWalker(this);

            Debug.Assert(symbols.Name == "programRoot", "When exiting VisitAst, symbols should be 'programRoot'");

            foreach(var symbol in symbols.AllSymbols){
                if(symbol.Name == "self" || symbol.Name.StartsWith(CSharpCompilerHelpers.HiddenMemberPrefix, StringComparison.CurrentCulture))
                    continue;

                var inside_interface = symbol.Ancestors.OfType<TypeDeclaration>()
                                             .Any(td => td.TypeKind == ClassType.Interface);
                if(!symbol.Modifiers.HasFlag(Modifiers.Export) && !(symbol.Type is ReferenceType) && !(symbol.Type is FunctionType) && !IsUsed(symbol)
                   && !inside_interface && !symbol.Name.StartsWith("_", StringComparison.CurrentCulture)){
                    parser.ReportWarning(
                        $"'{symbol.Name}' is never used!",
                        "ES0400",
                        symbol
                    );
                }
            }
        }

        public void VisitBlock(BlockStatement block)
        {
            if(block.IsNull)
                return;

            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            block.Statements.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitBreakStatement(BreakStatement breakStmt)
        {
            // no op
        }

        public void VisitContinueStatement(ContinueStatement continueStmt)
        {
            // no op
        }

        public void VisitDoWhileStatement(DoWhileStatement doWhileStmt)
        {
            VisitWhileStatement(doWhileStmt.Delegator);
        }

        public void VisitEmptyStatement(EmptyStatement emptyStmt)
        {
            // no op
        }

        public void VisitExpressionStatement(ExpressionStatement exprStmt)
        {
            exprStmt.Expression.AcceptWalker(this);
        }

        public void VisitForStatement(ForStatement forStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            forStmt.Target.AcceptWalker(this);

            forStmt.Left.AcceptWalker(this);

            VisitBlock(forStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            valueBindingForStmt.Initializer.AcceptWalker(this);

            valueBindingForStmt.Body.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitIfStatement(IfStatement ifStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            ifStmt.Condition.AcceptWalker(this);
            VisitBlock(ifStmt.TrueBlock);
            ifStmt.FalseStatement.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitReturnStatement(ReturnStatement returnStmt)
        {
            returnStmt.Expression.AcceptWalker(this);
        }

        public void VisitMatchStatement(MatchStatement matchStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            matchStmt.Target.AcceptWalker(this);
            matchStmt.Clauses.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitThrowStatement(ThrowStatement throwStmt)
        {
            VisitObjectCreationExpression(throwStmt.CreationExpression);
        }

        public void VisitTryStatement(TryStatement tryStmt)
        {
            VisitBlock(tryStmt.EnclosingBlock);
            tryStmt.CatchClauses.AcceptWalker(this);
            // Directly calling VisitFinally continues execution even if the node is null.
            tryStmt.FinallyClause.AcceptWalker(this);
        }

        public void VisitWhileStatement(WhileStatement whileStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            whileStmt.Condition.AcceptWalker(this);

            whileStmt.Body.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitYieldStatement(YieldStatement yieldStmt)
        {
            throw new NotImplementedException();
        }

        public void VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl)
        {
            varDecl.Variables.AcceptWalker(this);
        }

        public void VisitArrayInitializer(ArrayInitializerExpression arrayInitializer)
        {
            arrayInitializer.SizeExpression.AcceptWalker(this);
        }

        public void VisitAssignment(AssignmentExpression assignment)
        {
            inspecting_lvalue = true;
            assignment.Left.AcceptWalker(this);
            inspecting_lvalue = false;

            assignment.Right.AcceptWalker(this);
        }

        public void VisitBinaryExpression(BinaryExpression binaryExpr)
        {
            binaryExpr.Left.AcceptWalker(this);
            binaryExpr.Right.AcceptWalker(this);
        }

        public void VisitCallExpression(CallExpression callExpr)
        {
            var parental_call_target = inspecting_call_target;
            inspecting_call_target = true;
            callExpr.Target.AcceptWalker(this);
            inspecting_call_target = parental_call_target;

            callExpr.Arguments.AcceptWalker(this);
        }

        public void VisitCastExpression(CastExpression castExpr)
        {
            castExpr.Target.AcceptWalker(this);
            castExpr.ToType.AcceptWalker(this);
        }

        public void VisitCatchClause(CatchClause catchClause)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            Define(catchClause.Identifier);
            VisitBlock(catchClause.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitClosureLiteralExpression(ClosureLiteralExpression closure)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            closure.Parameters.AcceptWalker(this);
            VisitBlock(closure.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitComprehensionExpression(ComprehensionExpression comp)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            //BitArray save = bits;
            //bits = new BitArray(bits);

            comp.Body.AcceptWalker(this);
            comp.Item.AcceptWalker(this);

            //bits = save;

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitComprehensionForClause(ComprehensionForClause compFor)
        {
            compFor.Body.AcceptWalker(this);
        }

        public void VisitComprehensionIfClause(ComprehensionIfClause compIf)
        {
            compIf.Body.AcceptWalker(this);
        }

        public void VisitConditionalExpression(ConditionalExpression condExpr)
        {
            condExpr.Condition.AcceptWalker(this);
            condExpr.TrueExpression.AcceptWalker(this);
            condExpr.FalseExpression.AcceptWalker(this);
        }

        public void VisitFinallyClause(FinallyClause finallyClause)
        {
            VisitBlock(finallyClause.Body);
        }

        public void VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue)
        {
            keyValue.ValueExpression.AcceptWalker(this);
        }

        public void VisitLiteralExpression(LiteralExpression literal)
        {
            // no op
        }

        public void VisitIdentifier(Identifier ident)
        {
            if(ExpressoCompilerHelpers.StartOfIdentifierId <= ident.IdentifierId && ident.IdentifierId < ExpressoCompilerHelpers.EndOfImportedIdentifierIds)
                return;

            if(inspecting_lvalue && ident.Modifiers.HasFlag(Modifiers.Immutable)){
                if(ident.Parent is MemberReferenceExpression mem_ref2 && mem_ref2.Member == ident && mem_ref2.Target is PathExpression path
                   && path.AsIdentifier != null && IsInitialized(path.AsIdentifier)){
                    throw new ParserException(
                        $"Re-assignment on an immutable field: '{mem_ref2}'.",
                        "ES1902",
                        ident
                    );
                }else if(IsInitialized(ident) && ident.Parent is PathExpression path2 && path2.Parent is SequenceExpression seq && seq.Parent is AssignmentExpression){
                    bool is_parameter = false;
                    AstType parameter_type = null;
                    var tmp_table = symbols;
                    while(tmp_table.Parent != null){
                        parameter_type = tmp_table.GetSymbol(ident.Name)?.Type;
                        if(parameter_type != null){
                            is_parameter = tmp_table.Name.Contains("func");
                            break;
                        }

                        tmp_table = tmp_table.Parent;
                    }

                    if(is_parameter && parameter_type is PrimitiveType){
                        throw new ParserException(
                            $"Cannot assign to a normal parameter '{ident.Name}'.",
                            "ES1905",
                            ident
                        );
                    }else{
                        throw new ParserException(
                            $"Re-assignment on an immutable variable '{ident.Name}'.",
                            "ES1900",
                            ident
                        );
                    }
                }
            }

            if(inspecting_patterns && !IsInitialized(ident))
                Define(ident);

            if(!ident.Ancestors.Any(a => a is MemberReferenceExpression) && !IsInitialized(ident)){
                if(!inspecting_lvalue && !(ident.Type is FunctionType)){
                    parser.ReportSemanticError(
                        $"Use of a potentially uninitialized variable '{ident.Name}'.",
                        "ES0200",
                        ident
                    );
                }else if(inspecting_lvalue && !(ident.Type is FunctionType)){
                    SetInitialized(ident, true);
                }
            }

            var parent_contains_indexer = ident.Ancestors.Any(a => a is IndexerExpression);
            if(!inspecting_patterns && (!inspecting_lvalue || inspecting_lvalue && parent_contains_indexer) && !(ident.Type is FunctionType) && !IsUsed(ident))
                SetUsed(ident, true);

            if(inspecting_call_target){
                var type = ident.Type;
                if(!type.IsNull){
                    var tmp_table = symbols.GetTypeTable(type.Name);
                    if(tmp_table != null)
                        cur_focusing_table = tmp_table;

                    if(ident.Type is FunctionType)
                        SetCalled(ident, true);
                }
            }
        }

        public void VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq)
        {
            intSeq.Start.AcceptWalker(this);
            intSeq.End.AcceptWalker(this);
            intSeq.Step.AcceptWalker(this);
        }

        public void VisitIndexerExpression(IndexerExpression indexExpr)
        {
            indexExpr.Target.AcceptWalker(this);
            indexExpr.Arguments.AcceptWalker(this);
        }

        public void VisitMemberReference(MemberReferenceExpression memRef)
        {
            memRef.Target.AcceptWalker(this);
            VisitIdentifier(memRef.Member);

            if(memRef.Target is SelfReferenceExpression && !inspecting_lvalue){
                var type_decl = memRef.Ancestors.OfType<TypeDeclaration>().FirstOrDefault();
                if(type_decl != null){
                    var type_table = symbols.GetTypeTable(type_decl.Name);
                    if(type_table != null){
                        var symbol = type_table.GetSymbolInTypeChain(memRef.Member.Name);
                        if(symbol != null && symbol.Type is FunctionType && symbol.Modifiers.HasFlag(Modifiers.Mutating)){
                            if(method_is_mutating){
                                actually_mutating = true;
                            }else{
                                throw new ParserException(
                                    "A non-mutating method accidentally mutates `self`.",
                                    "ES4120",
                                    memRef
                                );
                            }
                        }// not being used
                        /*else if(symbol != null && !(symbol.Type is FunctionType) && !symbol.Modifiers.HasFlag(Modifiers.Immutable)){
                            //inspecting_mutable_field = true;
                        }*/
                    }
                }
            }/*else if(inspecting_mutable_field){

                else if(symbol != null && !(symbol.Type is FunctionType) && !symbol.Modifiers.HasFlag(Modifiers.Immutable)){
                    if(method_is_mutating){
                        actually_mutating = true;
                    }else{
                        throw new ParserException(
                            $"A non-mutating method cannot access to a mutable field '{memRef.Member.Name}'.",
                            "ES4121",
                            memRef
                        );
                    }
                }

            }*/

            if(inspecting_call_target){
                if(cur_focusing_table != null && !cur_focusing_table.IsForeignType){
                    var symbol = cur_focusing_table.GetSymbol(memRef.Member.Name);
                    if(symbol != null && symbol.Type is FunctionType && !symbol.PostModifiers.IsNull){
                        foreach(var beforehand in symbol.PostModifiers.RequiresToBeCalledBeforehand){
                            if(!IsCalled(beforehand)){
                                throw new ParserException(
                                    $"The method '{memRef.Member.Name}' requires the method '{beforehand.Name}' to be directly or indirectly called beforehand in the same method.",
                                    "ES4300",
                                    memRef
                                );
                            }
                        }

                        var cur_method_name = memRef.Ancestors
                                                    .OfType<FunctionDeclaration>()
                                                    .First()
                                                    .Name;
                        if(symbol.PostModifiers.FriendMethods.Any() && !symbol.PostModifiers.FriendMethods.Any(fm => fm.Name == cur_method_name)){
                            throw new ParserException(
                                "Only friend methods can access to this method.",
                                "ES4310",
                                memRef
                            );
                        }
                    }
                }

                if(memRef.Target is PathExpression target_path && target_path.AsIdentifier != null){
                    var type_symbol = symbols.GetTypeSymbolInAnyScope(target_path.AsIdentifier.Name);
                    if(type_symbol != null){
                        // This indicates that it points at a type
                        var member_ident = memRef.Member;
                        var member_type = member_ident.Type;
                        if(member_type is FunctionType && !member_ident.Modifiers.HasFlag(Modifiers.Static)){
                            throw new ParserException(
                                "An object reference is required for the instance method call.",
                                "ES0210",
                                memRef
                            );
                        }
                    }else{
                        var member_ident = memRef.Member;
                        var member_type = member_ident.Type;
                        if(member_type is FunctionType && member_ident.Modifiers.HasFlag(Modifiers.Static)){
                            throw new ParserException(
                                "A static method call requires a type reference.",
                                "ES0211",
                                memRef
                            );
                        }
                    }
                }
            }
        }

        public void VisitPathExpression(PathExpression pathExpr)
        {
            if(pathExpr.Items.Count == 1){
                VisitIdentifier(pathExpr.AsIdentifier);
            }else if(!inspecting_lvalue && pathExpr.Ancestors.Any(a => a is AssignmentExpression || a is VariableInitializer)){
                // In order to silence ES0400 warning when the path is used in the right hand side of assignments
                pathExpr.Items.AcceptWalker(this);
            }
            // In order to avoid flow-checking like TestModule::someFunction
        }

        public void VisitParenthesizedExpression(ParenthesizedExpression parensExpr)
        {
            parensExpr.Expression.AcceptWalker(this);
        }

        public void VisitObjectCreationExpression(ObjectCreationExpression creation)
        {
            creation.TypePath.AcceptWalker(this);
            if(creation.TypePath is SimpleType simple){
                var name = !simple.IdentifierNode.Type.IsNull ? simple.IdentifierNode.Type.Name : simple.Name;
                var creation_target_table = symbols.GetTypeTable(name);
                if(creation_target_table == null)
                    throw new InvalidOperationException($"The type table {simple.Name} is missing!");

                // FIXME: Can be deleted: 2019/8/8
                /*if(!creation_target_table.IsForeignType){
                    // Reset called statuses in case there are more than one instance of this type
                    foreach(var func_symbol in creation_target_table.Symbols
                                                                    .Where(s => s.Type is FunctionType && !s.Name.StartsWith("constructor", StringComparison.CurrentCulture))){
                        SetCalled(func_symbol, false);
                    }
                }*/
            }

            creation.Items.AcceptWalker(this);
        }

        public void VisitSequenceInitializer(SequenceInitializer seqInitializer)
        {
            seqInitializer.Items.AcceptWalker(this);
        }

        public void VisitMatchClause(MatchPatternClause matchClause)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            matchClause.Patterns.AcceptWalker(this);
            matchClause.Guard.AcceptWalker(this);
            matchClause.Body.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitSequenceExpression(SequenceExpression seqExpr)
        {
            seqExpr.Items.AcceptWalker(this);
        }

        public void VisitUnaryExpression(UnaryExpression unaryExpr)
        {
            unaryExpr.Operand.AcceptWalker(this);
        }

        public void VisitSelfReferenceExpression(SelfReferenceExpression selfRef)
        {
            if(method_is_mutating){
                if(inspecting_lvalue)
                    actually_mutating = true;
            }else{
                if(inspecting_lvalue){
                    throw new ParserException(
                        "A non-mutating method accidentally mutates `self`.",
                        "ES4120",
                        selfRef
                    );
                }
            }

            cur_focusing_table = symbols.GetTypeTable(selfRef.SelfIdentifier.Type.Name);
        }

        public void VisitSuperReferenceExpression(SuperReferenceExpression superRef)
        {
            throw new NotImplementedException();
        }

        public void VisitNullReferenceExpression(NullReferenceExpression nullRef)
        {
        }

        public void VisitCommentNode(CommentNode comment)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public void VisitTextNode(TextNode textNode)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public void VisitTypeConstraint(TypeConstraint constraint)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public void VisitPostModifiers(PostModifiers postModifiers)
        {
        }

        public void VisitSimpleType(SimpleType simpleType)
        {
            // Ignore these 2 types because they are not bound to IdentifierIds
            if(simpleType.Name == "array" || simpleType.Name == "tuple")
                return;

            SetUsed(simpleType.IdentifierNode, true);
        }

        public void VisitPrimitiveType(PrimitiveType primitiveType)
        {
        }

        public void VisitReferenceType(ReferenceType referenceType)
        {
        }

        public void VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptWalker(this);
            VisitSimpleType(memberType.ChildType);
        }

        public void VisitFunctionType(FunctionType funcType)
        {
        }

        public void VisitParameterType(ParameterType paramType)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public void VisitPlaceholderType(PlaceholderType placeholderType)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public void VisitKeyValueType(KeyValueType keyValueType)
        {
        }

        public void VisitFunctionParameterType(FunctionParameterType parameterType)
        {
        }

        public void VisitAttributeSection(AttributeSection section)
        {
            throw new InvalidOperationException();
        }

        public void VisitImportDeclaration(ImportDeclaration importDecl)
        {
            // An import declaration always introduces new variable(s) into the module scope.
            foreach(var alias in importDecl.AliasTokens)
                Define(alias);
        }

        public void VisitFunctionDeclaration(FunctionDeclaration funcDecl)
        {
            if(funcDecl.Body.IsNull){
                // We should early return to avoid ES4301 thrown
                ++scope_counter;
                return;
            }

            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            Define(funcDecl.NameToken);
            foreach(var p in funcDecl.Parameters){
                Define(p.NameToken);
                if(!p.Option.IsNull)
                    p.Option.AcceptWalker(this);
            }

            var parent_mutating = method_is_mutating;
            var parent_actually_mutating = actually_mutating;
            actually_mutating = false;
            if(funcDecl.Modifiers.HasFlag(Modifiers.Mutating))
                method_is_mutating = true;

            //foreach(var inside in funcDecl.PostModifiers.RequiresToBeCalledInside)
            //    SetCalled(inside, false);
            cur_func_ident_id = funcDecl.NameToken.IdentifierId;
            var tmp = new List<uint>();
            call_map.Add(cur_func_ident_id, tmp);

            VisitBlock(funcDecl.Body);

            if(method_is_mutating && !actually_mutating){
                parser.ReportWarning(
                    $"A mutating method '{funcDecl.Name}' does not actually mutates `self`.",
                    "ES4110",
                    funcDecl
                );
            }

            // We can't write this as VisitPostModifiers(funcDecl.PostModifiers) because then we will visit null nodes
            funcDecl.PostModifiers.AcceptWalker(this);

            foreach(var inside in funcDecl.PostModifiers.RequiresToBeCalledInside){
                if(!IsCalled(inside)){
                    throw new ParserException(
                        $"A derived method '{funcDecl.Name}' requires the method '{inside.Name}' to be called inside '{funcDecl.Name}'.",
                        "ES4301",
                        funcDecl
                    );
                }
            }

            method_is_mutating = parent_mutating;
            actually_mutating = parent_actually_mutating;

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitTypeDeclaration(TypeDeclaration typeDecl)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            Define(typeDecl.NameToken);

            // This causes external symbols to be marked, which would result in an exception
            // because we are currently ignoring external symbols in FlowChecker
            /*if(typeDecl.Modifiers.HasFlag(Modifiers.Export)){
                foreach(var field in typeDecl.Members.OfType<FieldDeclaration>()){
                    // Mark hypothetical uses
                    SetUsed(field.NameToken, true);
                }
            }else{*/
                typeDecl.Members.AcceptWalker(this);
            //}

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitAliasDeclaration(AliasDeclaration aliasDecl)
        {
            throw new NotImplementedException();
        }

        public void VisitFieldDeclaration(FieldDeclaration fieldDecl)
        {
            fieldDecl.Initializers.AcceptWalker(this);
        }

        public void VisitParameterDeclaration(ParameterDeclaration parameterDecl)
        {
            Define(parameterDecl.NameToken);
        }

        public void VisitVariableInitializer(VariableInitializer initializer)
        {
            if(!initializer.Initializer.IsNull){
                initializer.Initializer.AcceptWalker(this);

                if(initializer.Pattern is PatternWithType pattern)
                    MakePatternSet(pattern.Pattern);
                else
                    MakePatternSet(initializer.Pattern);
            }
        }

        public void VisitWildcardPattern(WildcardPattern wildcardPattern)
        {
        }

        public void VisitIdentifierPattern(IdentifierPattern identifierPattern)
        {
            var parent_inspecting_patterns = inspecting_patterns;
            inspecting_patterns = true;

            VisitIdentifier(identifierPattern.Identifier);

            // symbols should indicate the type that the parental destructuring pattern focuses on or the current scope
            var symbol = symbols.GetSymbol(identifierPattern.Identifier.Name);
            if(symbol != null)
                SetUsed(symbol, true);

            inspecting_patterns = parent_inspecting_patterns;
        }

        public void VisitCollectionPattern(CollectionPattern collectionPattern)
        {
            throw new NotImplementedException();
        }

        public void VisitDestructuringPattern(DestructuringPattern destructuringPattern)
        {
            inspecting_patterns = true;

            var current_table = symbols;
            var type_table = (destructuringPattern.TypePath is SimpleType simple) ? symbols.GetTypeTable(simple.Name)
                : (!destructuringPattern.IsEnum && destructuringPattern.TypePath is MemberType member) ? symbols.GetTypeTable(member.Target.Name).GetTypeTable(member.ChildType.Name)
                : current_table;
            symbols = type_table;

            destructuringPattern.TypePath.AcceptWalker(this);
            destructuringPattern.Items.AcceptWalker(this);

            symbols = current_table;

            inspecting_patterns = false;
        }

        public void VisitTuplePattern(TuplePattern tuplePattern)
        {
            inspecting_patterns = true;

            tuplePattern.Patterns.AcceptWalker(this);

            inspecting_patterns = false;
        }

        public void VisitExpressionPattern(ExpressionPattern exprPattern)
        {
            exprPattern.Expression.AcceptWalker(this);
        }

        public void VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
        {
        }

        public void VisitKeyValuePattern(KeyValuePattern keyValuePattern)
        {
            keyValuePattern.Value.AcceptWalker(this);
            // symbols should indicate the type that the destructuring pattern focuses on or the current scope
            var symbol = symbols.GetSymbol(keyValuePattern.KeyIdentifier.Name);
            if(symbol != null)
                SetUsed(symbol, true);
        }

        public void VisitPatternWithType(PatternWithType pattern)
        {
            pattern.Pattern.AcceptWalker(this);
        }

        public void VisitTypePathPattern(TypePathPattern pathPattern)
        {
            pathPattern.TypePath.AcceptWalker(this);
        }

        public void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
        {
            VisitVariableInitializer(valueBindingPattern.Initializer);
        }

        public void VisitNullNode(AstNode nullNode)
        {
        }

        public void VisitNewLine(NewLineNode newlineNode)
        {
            throw new NotImplementedException();
        }

        public void VisitWhitespace(WhitespaceNode whitespaceNode)
        {
            throw new NotImplementedException();
        }

        public void VisitExpressoTokenNode(ExpressoTokenNode tokenNode)
        {
            throw new NotImplementedException();
        }

        public void VisitPatternPlaceholder(AstNode placeholder, Pattern child)
        {
            throw new NotImplementedException();
        }
        #endregion

        void MakePatternSet(PatternConstruct pattern)
        {
            switch(pattern){
            case IdentifierPattern ident_pat:
                SetInitialized(ident_pat.Identifier, true);
                break;

            case TuplePattern tuple_pat:
                foreach(var pat in tuple_pat.Patterns)
                    MakePatternSet(pat);

                break;
            case DestructuringPattern destructuring:
                foreach(var item in destructuring.Items)
                    MakePatternSet(item);

                break;

            case ExpressionPattern _:
            case WildcardPattern _:
                break;

            default:
                throw new InvalidOperationException($"Unknown pattern: {pattern.GetType().Name}");
            }
        }
	}
}

