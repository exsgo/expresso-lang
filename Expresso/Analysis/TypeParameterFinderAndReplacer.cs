﻿using System;
using System.Collections.Generic;
using System.Linq;
using Expresso.Runtime.Builtins;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// This class is responsible for searching for type parameters in a particular <see cref="AstType"/>
    /// and return a <see cref="AstType"/> that corresponds to them.
    /// </summary>
    public class TypeParameterFinderAndReplacer : IAstTypeWalker<AstType, Unit>
    {
        private IEnumerable<ParameterType> type_parameters;
        private IEnumerable<AstType> arg_types;
        private AstType[] targets;

        public TypeParameterFinderAndReplacer(IEnumerable<ParameterType> typeParameters, IEnumerable<AstType> argumentTypes, AstType[] targets)
        {
            type_parameters = typeParameters;
            arg_types = argumentTypes;
            this.targets = targets;
        }

        public Unit VisitFunctionParameterType(FunctionParameterType parameterType, AstType data)
        {
            if(data is FunctionParameterType func_param_type)
                return parameterType.Type.AcceptTypeWalker(this, func_param_type.Type);
            else
                return null;
        }

        public Unit VisitFunctionType(FunctionType funcType, AstType data)
        {
            if(data == null){
                foreach(var arg_type in arg_types){
                    if(arg_type is FunctionType func && func.Name == funcType.Name)
                        data = arg_type;
                }
            }

            if(data is FunctionType func_type){
                foreach(var pair in funcType.Parameters.Zip(func_type.Parameters, (l, r) => new {First = l, Second = r}))
                    pair.First.AcceptTypeWalker(this, pair.Second);

                funcType.ReturnType.AcceptTypeWalker(this, func_type.ReturnType);
            }

            return null;
        }

        public Unit VisitKeyValueType(KeyValueType keyValueType, AstType data)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public Unit VisitMatchClause(MatchPatternClause matchClause, AstType data)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public Unit VisitMemberType(MemberType memberType, AstType data)
        {
            if(data == null){
                foreach(var arg_type in arg_types){
                    if(arg_type is MemberType member && member.Target.IsMatch(memberType.Target))
                        data = arg_type;
                }

            }

            if(data is MemberType member_type){
                memberType.ParentType.AcceptTypeWalker(this, member_type.ParentType);

                VisitSimpleType(memberType.ChildType, member_type.ChildType);
            }

            return null;
        }

        public Unit VisitParameterType(ParameterType paramType, AstType data)
        {
            foreach(var pair in Enumerable.Range(0, type_parameters.Count()).Zip(type_parameters, (l, r) => new {Index = l, TypeParameter = r})){
                if(pair.TypeParameter.Name == paramType.Name){
                    if(data == null)
                        data = arg_types.ElementAt(pair.Index);

                    targets[pair.Index] = data;
                }
            }

            return null;
        }

        public Unit VisitPlaceholderType(PlaceholderType placeholderType, AstType data)
        {
            throw new Exception("Unexpected placeholder type");
        }

        public Unit VisitPrimitiveType(PrimitiveType primitiveType, AstType data)
        {
            return null;
        }

        public Unit VisitReferenceType(ReferenceType referenceType, AstType data)
        {
            throw new Exception("Unexpected ReferenceType");
        }

        public Unit VisitSimpleType(SimpleType simpleType, AstType data)
        {
            if(data == null){
                foreach(var arg_type in arg_types){
                    if(arg_type is SimpleType simple && simple.Name == simpleType.Name)
                        data = arg_type;
                }
            }

            if(data is SimpleType simple_type){
                foreach(var pair in simpleType.TypeArguments.Zip(simple_type.TypeArguments, (l, r) => new {First = l, Second = r }))
                    pair.First.AcceptTypeWalker(this, pair.Second);
            }

            return null;
        }

        public Unit VisitNullNode(AstNode nullNode, AstType data)
        {
            throw new InvalidOperationException("Can not work on that node");
        }
    }
}
