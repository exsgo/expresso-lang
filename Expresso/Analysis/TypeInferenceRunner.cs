﻿using System;
using System.Collections.Generic;
using System.Linq;

using Expresso.CodeGen;
using Expresso.TypeSystem;

using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.Ast.Analysis
{
    public partial class TypeChecker
    {
        /// <summary>
        /// The type inference runner is responsible for inferring types when asked.
        /// It does the job by inferring and replacing old nodes with the calculated type nodes
        /// in the symbol table. 
        /// Note that the <see cref="AstType"/> nodes returned won't be cloned. So be careful when using it.
        /// </summary>
        /// <remarks>
        /// Currently, I must say it's just a temporal implementation since it messes around the AST itself
        /// in order to keep track of type relations.
        /// </remarks>
        public class TypeInferenceRunner : IAstWalker<AstType>
        {
            static IEnumerable<KeyValueType> CallAllTypeArgs;
            Parser parser;
            TypeChecker checker;
            FullNameAdder full_name_adder;
            KeyValueType[] type_type_args;

            public bool InspectsClosure{
                get; set;
            }

            public TypeInferenceRunner(Parser parser, TypeChecker checker)
            {
                this.parser = parser;
                this.checker = checker;
                full_name_adder = new FullNameAdder(checker.symbols);
            }

            #region IAstWalker implementation

            public AstType VisitAst(ExpressoAst ast)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitBlock(BlockStatement block)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitBreakStatement(BreakStatement breakStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitContinueStatement(ContinueStatement continueStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitDoWhileStatement(DoWhileStatement doWhileStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitEmptyStatement(EmptyStatement emptyStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitExpressionStatement(ExpressionStatement exprStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitForStatement(ForStatement forStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStatment)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitIfStatement(IfStatement ifStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitReturnStatement(ReturnStatement returnStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitMatchStatement(MatchStatement matchStmt)
            {
                return matchStmt.Target.AcceptWalker(this);
            }

            public AstType VisitThrowStatement(ThrowStatement throwStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitTryStatement(TryStatement tryStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitWhileStatement(WhileStatement whileStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitYieldStatement(YieldStatement yieldStmt)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitArrayInitializer(ArrayInitializerExpression arrayInitializer)
            {
                return AstType.MakeSimpleType("array", arrayInitializer.StartLocation, arrayInitializer.EndLocation, arrayInitializer.TypePath.Clone());
            }

            public AstType VisitAssignment(AssignmentExpression assignment)
            {
                // In an assignment, we usually see variables on the left hand side.
                // So let's take a look at the left hand side
                var type = assignment.Left.AcceptWalker(this);
                // In a compound assignment, we could see variables on the right hand side
                assignment.Right.AcceptWalker(this);
                return type.Clone();
            }

            public AstType VisitBinaryExpression(BinaryExpression binaryExpr)
            {
                var closure_expr = binaryExpr.Ancestors.OfType<ClosureLiteralExpression>()
                                             .FirstOrDefault();
                // We must type-check binary expressions here except when the binary expression is decendants of closure expressions
                // that have type parameters because then type-checking would issue an error due to operators not being able to be applied
                // to type parameters 
                if(closure_expr == null || !closure_expr.Parameters.Any(p => ExpressoCompilerHelpers.ContainsAnyParameterType(p.ReturnType))
                    && !ExpressoCompilerHelpers.ContainsAnyParameterType(closure_expr.ReturnType)){
                    return binaryExpr.AcceptWalker(checker);
                    //return binaryExpr.PreferredType;
                }

                return checker.FigureOutCommonType(binaryExpr.Left.AcceptWalker(this), binaryExpr.Right.AcceptWalker(this));
            }

            public AstType VisitCallExpression(CallExpression callExpr)
            {
                if(!callExpr.ResultType.IsNull)
                    return callExpr.ResultType;

                var arg_types = new AstType[callExpr.Arguments.Count];
                foreach(var pair in Enumerable.Range(0, callExpr.Arguments.Count).Zip(callExpr.Arguments, (l, r) => new {Index = l, Argument = r})){
                    var arg_type = pair.Argument.AcceptWalker(this);
                    arg_types[pair.Index] = arg_type;
                }

                var parent_types = checker.argument_types;
                checker.argument_types = arg_types;

                // Clone is needed because we could change it directly
                // No need to `Clone` it because we want the changes visible to TypeChecker
                var func_type = callExpr.Target.AcceptWalker(this)/*.Clone()*/ as FunctionType;
                if(func_type == null){
                    throw new ParserException(
                        $"'{callExpr.Target}' turns out not to be a callable.",
                        "ES1802",
                        callExpr
                    );
                }

                KeyValueType[] all_type_args = type_type_args?.Select(ta => (KeyValueType)ta.Clone()).ToArray();
                if(func_type.TypeParameters.Count > 0){
                    if(callExpr.TypeArguments.Any()){
                        // The call expression contains a generic type list
                        var type_params = func_type.TypeParameters;
                        callExpr.TypeArguments.ReplaceWith(
                            callExpr.TypeArguments.Zip(type_params, (l, r) => new {TypeArgument = l, TypeParameter = r})
                                                  .Select(pair => AstType.MakeKeyValueType(pair.TypeParameter.Clone(), pair.TypeArgument.ValueType.Clone()))
                        );

                        all_type_args = (type_type_args != null) ? callExpr.TypeArguments.Concat(type_type_args).ToArray() : callExpr.TypeArguments.ToArray();
                    }else{
                        if(func_type.IdentifierNode.Modifiers.HasFlag(Modifiers.Extension) && callExpr.Target is MemberReferenceExpression mem_ref2){
                            var extension_type_table = checker.symbols.GetTypeTable(func_type.IdentifierNode.Type.Name);
                            func_type = (FunctionType)extension_type_table.GetSymbol(func_type.Name).Type.Clone();

                            arg_types = new []{mem_ref2.Target.AcceptWalker(this)}.Concat(arg_types).ToArray();
                        }

                        var tmp_type_args = func_type.TypeParameters.Select(param => AstType.MakeKeyValueType(param.Clone(), AstType.MakePlaceholderType()));
                        all_type_args = (type_type_args != null) ? tmp_type_args.Concat(type_type_args).ToArray() : tmp_type_args.ToArray();
                        callExpr.TypeArguments.ReplaceWith(tmp_type_args);

                        var param_and_return_types = func_type.Parameters.Select(p => p).Concat(new []{func_type.ReturnType});
                        var arg_and_return_types = arg_types.Select(a => a).Concat(new []{func_type.ReturnType});
                        ExpressoCompilerHelpers.ExtractTypeArguments(checker.symbols, param_and_return_types, arg_and_return_types, all_type_args);

                        ExpressoCompilerHelpers.SubstituteTypeParameters(callExpr.TypeArguments, all_type_args);

                        if(all_type_args.Any(ta => ExpressoCompilerHelpers.IsPlaceholderType(ta.ValueType))){
                            // never thrown?
                            throw new ParserException(
                                "Can not infer some of the type parameters.",
                                "ES2005",
                                callExpr
                            );
                        }
                    }
                }

                // We need to substitute type parameters in closures within the arguments
                var inplace_replacer = new InPlaceTypeArgumentReplacer(all_type_args);
                // Rewind scope_counter before any closure arugment of this call expression
                var closure_count = callExpr.Arguments.Count(a => a is ClosureLiteralExpression);
                checker.scope_counter -= closure_count;
                var parental_type_args = CallAllTypeArgs;
                CallAllTypeArgs = all_type_args;
                foreach(var param in callExpr.Arguments){
                    if(param is ClosureLiteralExpression closure_literal){
                        foreach(var closure_param in closure_literal.Parameters)
                            closure_param.ReturnType.AcceptTypeWalker(inplace_replacer);

                        if(all_type_args != null && !all_type_args.Any(ta => ta.KeyType.MatchesParameterType(closure_literal.ReturnType) && ta.ValueType is ParameterType))
                            closure_literal.ReturnType.AcceptTypeWalker(inplace_replacer);

                        if(CallAllTypeArgs != null && !CallAllTypeArgs.Any(ta => ExpressoCompilerHelpers.ContainsAnyParameterType(ta.ValueType))){
                            // Substitute type parameters of the expressions that reside in the body block
                            checker.VisitBlock(closure_literal.Body);
                        }
                    }
                }
                CallAllTypeArgs = parental_type_args;

                if(!callExpr.OverloadSignature.IsNull){
                    foreach(var param in callExpr.OverloadSignature.Parameters)
                        param.AcceptTypeWalker(full_name_adder);

                    callExpr.OverloadSignature.ReturnType.AcceptTypeWalker(full_name_adder);
                }
                checker.argument_types = parent_types;

                var type_args = (all_type_args != null) ? all_type_args : callExpr.TypeArguments.ToArray();
                // In order to type-check in TypeChecker
                foreach(var param in func_type.Parameters)
                    ExpressoCompilerHelpers.SubstituteGenericTypes(param, type_args);

                callExpr.ResultType = ExpressoCompilerHelpers.SubstituteGenericTypes(checker.ResolveType(func_type.ReturnType), type_args).Clone();

                if(type_type_args != null)
                    type_type_args = null;

                return callExpr.ResultType;
            }

            public AstType VisitCastExpression(CastExpression castExpr)
            {
                return castExpr.ToType;
            }

            public AstType VisitCatchClause(CatchClause catchClause)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public AstType VisitClosureLiteralExpression(ClosureLiteralExpression closure)
            {
                // Because there are times when the closure is not inspected beforehand
                // examine it here
                if(!closure.HasInspectedInTypeChecker || InspectsClosure){
                    InspectsClosure = false;
                    checker.VisitClosureLiteralExpression(closure);
                }

                // Usually we don't need to descend or ascend scopes in TypeInferenceRunner
                // we do need to do that here because TypeChecker.VisitVariableInitializer can directly call this method.
                // Descend scopes 2 times because closure parameters also have thier own scope.
                int tmp_counter = checker.scope_counter;

                var param_types =
                    from p in closure.Parameters
                    select p.ReturnType.Clone();

                var return_type = closure.ReturnType.Clone();

                return AstType.MakeFunctionType("closure", return_type, param_types);
            }

            public AstType VisitComprehensionExpression(ComprehensionExpression comp)
            {
                int tmp_counter = checker.scope_counter;
                checker.DescendScope();
                checker.scope_counter = 0;

                var obj_type = comp.ObjectType;
                comp.Body.AcceptWalker(this);

                if(obj_type.Name == "dictionary"){
                    var key_value = comp.Item as KeyValueLikeExpression;
                    var key_type = key_value.KeyExpression.AcceptWalker(this);
                    var value_type = key_value.ValueExpression.AcceptWalker(this);
                    obj_type.TypeArguments.FirstOrNullObject().ReplaceWith(key_type.Clone());
                    obj_type.TypeArguments.LastOrNullObject().ReplaceWith(value_type.Clone());
                }else if(obj_type.Name == "array" || obj_type.Name == "vector"){
                    var element_type = comp.Item.AcceptWalker(this);
                    obj_type.TypeArguments.FirstOrNullObject().ReplaceWith(element_type.Clone());
                }else{
                    throw new InvalidOperationException("Unreachable!");
                }

                checker.AscendScope();
                checker.scope_counter = tmp_counter + 1;

                return obj_type;
            }

            public AstType VisitComprehensionForClause(ComprehensionForClause compFor)
            {
                var inferred = compFor.Target.AcceptWalker(this);
                var inferred_primitive = inferred as PrimitiveType;
                if(inferred_primitive != null){
                    // See if it is a IntSeq object
                    if(inferred_primitive.KnownTypeCode != KnownTypeCode.IntSeq){
                        parser.ReportSemanticError(
                            $"'{inferred_primitive}' is not a sequence type!",
                            "ES1302",
                            inferred_primitive
                        );
                    }

                    inferred = new PrimitiveType("int", inferred_primitive.StartLocation);
                }

                var inferred_simple = inferred as SimpleType;
                if(inferred_simple != null){
                    // See if it is a sequence object like array or vector
                    if(inferred_simple.Name != "array" && inferred_simple.Name != "vector"){
                        parser.ReportSemanticError(
                            $"'{inferred_simple}' is not a sequence type! A comprehension expects a sequence object.",
                            "ES1302",
                            inferred_simple
                        );
                    }

                    inferred = inferred_simple.TypeArguments.FirstOrDefault();
                }

                var identifier_ptn = compFor.Left as IdentifierPattern;
                if(identifier_ptn != null)
                    identifier_ptn.Identifier.Type.ReplaceWith(inferred);

                if(compFor.Body != null)
                    compFor.Body.AcceptWalker(this);
                
                return inferred;
            }

            public AstType VisitComprehensionIfClause(ComprehensionIfClause compIf)
            {
                if(compIf.Body != null)
                    compIf.Body.AcceptWalker(this);

                return AstType.Null;
            }

            public AstType VisitConditionalExpression(ConditionalExpression condExpr)
            {
                condExpr.AcceptWalker(checker);
                return checker.FigureOutCommonType(condExpr.TrueExpression.AcceptWalker(this), condExpr.FalseExpression.AcceptWalker(this));
            }

            public AstType VisitFinallyClause(FinallyClause finallyClause)
            {
                return VisitBlock(finallyClause.Body);
            }

            public AstType VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitLiteralExpression(LiteralExpression literal)
            {
                return literal.Type;
            }

            public AstType VisitIdentifier(Identifier ident)
            {
                // Can be deleted: 2019/7/11
                /*if(!ident.Type.IsNull && !(ident.Type is PlaceholderType))
                    return ResolveType(ident);*/

                if(ident.Type is PlaceholderType || ident.Type is ParameterType && ident.Ancestors.Any(a => a is ClosureLiteralExpression)){
                    var symbol = checker.symbols.GetSymbolInAnyScope(ident.Name);
                    if(symbol != null){
                        var cloned = symbol.Type.Clone();
                        ident.Type.ReplaceWith(cloned);

                        if(!ident.Type.IsNull && !(ident.Type is PlaceholderType))
                            return ResolveType(ident);
                        else
                            return symbol.Type;
                    }

                    var symbol2 = checker.symbols.GetTypeSymbolInAnyScope(ident.Name);
                    if(symbol2 == null){
                        // ES0102 or ES0120 thrown
                        throw new ParserException(
                            $"The symbol '{ident.Name}' is not defined in the current scope {checker.symbols.Name}.",
                            "ES1000",
                            ident
                        );
                    }else{
                        var cloned = symbol2.Type.Clone();
                        ident.Type.ReplaceWith(cloned);
                        return symbol2.Type;
                    }
                }else{
                    return ResolveType(ident);
                }
            }

            public AstType VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq)
            {
                return AstType.MakePrimitiveType("intseq", intSeq.StartLocation);
            }

            public AstType VisitIndexerExpression(IndexerExpression indexExpr)
            {
                var target_type = indexExpr.Target.AcceptWalker(this);
                if(target_type is SimpleType simple_type){
                    if(!checker.CanIndexType(simple_type)){
                        // We need to duplicate the following error message in InferenceRunner and TypeChecker
                        // because we won't be reaching some code paths
                        throw new ParserException(
                            $"Can not apply the indexer operator on type `{simple_type}`",
                            "ES3011",
                            indexExpr
                        );
                    }

                    if(indexExpr.Arguments.Count == 1){
                        var arg_type = indexExpr.Arguments.First().AcceptWalker(this);
                        if(arg_type is PrimitiveType primitive && primitive.KnownTypeCode == KnownTypeCode.IntSeq)
                            return AstType.MakeSimpleType("slice", new []{simple_type.Clone(), simple_type.TypeArguments.First().Clone()});
                    }

                    if(simple_type.Name == "array" || simple_type.Name == "vector" || simple_type.Name == "dictionary"){
                        return simple_type.TypeArguments.LastOrNullObject().Clone();
                    }else{
                        var name = !simple_type.IdentifierNode.Type.IsNull ? simple_type.IdentifierNode.Type.Name : simple_type.Name;
                        var table = checker.symbols.GetTypeTable(name);
                        // No need to check table is not null because we have already done that in CanIndexType
                        var sub_type = table.IsSubclassOf(AstType.MakeSimpleType("System.Collections.Generic.ICollection"));
                        return sub_type.TypeArguments.LastOrNullObject().Clone();
                    }
                }

                return target_type;
            }

            public AstType VisitMemberReference(MemberReferenceExpression memRef)
            {
                var target_type = memRef.Target.AcceptWalker(this);
                if(target_type is ParameterType){
                    var object_table = checker.symbols.GetTypeTable("object");
                    var object_symbol = object_table.GetSymbol(memRef.Member.Name);
                    if(object_symbol != null)
                        return object_symbol.Type;

                    var closure = memRef.Ancestors.OfType<ClosureLiteralExpression>().FirstOrDefault();
                    var parental_mem_ref = closure?.Ancestors.OfType<MemberReferenceExpression>().FirstOrDefault();
                    if(closure != null && parental_mem_ref != null){
                        // TODO: implement the closure case
                        /*var target_type2 = parental_mem_ref.Target.AcceptWalker(this);
                        var target_type_table = checker.symbols.GetTypeTable(target_type2.Name);
                        target_type_table.TypeParameters.*/
                    }else{
                        foreach(var func_decl in memRef.Ancestors.OfType<FunctionDeclaration>()
                                                       .Where(func => func.TypeConstraints.Any())){
                            foreach(var type_constraint in func_decl.TypeConstraints){
                                if(type_constraint.TypeParameter.Name == target_type.Name){
                                    foreach(var type_constraint2 in type_constraint.TypeConstraints){
                                        var constraint_type_table = checker.symbols.GetTypeTable(type_constraint2.Name);
                                        var constraint_symbol = constraint_type_table.GetSymbol(memRef.Member.Name);
                                        if(constraint_symbol != null)
                                            return constraint_symbol.Type;
                                    }

                                    if(type_constraint.TypeConstraints.Any()){
                                        throw new ParserException(
                                            $"None of the types on the type parameter `{type_constraint.TypeParameter}` does not have a field or method named '{memRef.Member.Name}'.",
                                            "ES2200",
                                            memRef
                                        );
                                    }
                                }
                            }
                        }
                    }

                    if(memRef.Target is MemberReferenceExpression child_memref)
                        return CheckTypeConstraintsOnType(target_type, child_memref, memRef);
                    else if(memRef.Parent is CallExpression call_expr)
                        return CheckTypeConstraintsOnType(target_type, memRef, memRef);

                    // ES2000 can be thrown because undeclared type parameters won't get replaced with `ParameterType`s
                    throw new Exception("Unreachable");
                }

                var another_type = (target_type is SimpleType target_simple) ? target_simple.IdentifierNode.Type : AstType.Null;

                // exclude cases where another type is a tuple or int
                // because they are enums
                var name = (target_type is MemberType member) ? member.Target.Name + "::" + member.MemberName :
                                                                      (!another_type.IsNull && another_type.Name != "tuple" && another_type.Name != "int") ?
                                                                      another_type.Name : target_type.Name;
                var type_table = checker.symbols.GetTypeTable(name);
                if(type_table == null){
                    // ES0101 can be thrown
                    throw new ParserException(
                        $"Although the expression '{memRef.Target}' is evaluated to the type `{target_type}`, there is not any type with that name in the scope '{checker.symbols.Name}'.",
                        "ES2000",
                        memRef.Target
                    );
                }
                    
                Identifier symbol = null;
                FunctionType overload_signature = null;
                if(memRef.Parent is CallExpression call_expr2 && checker.argument_types != null){
                    var func_type = AstType.MakeFunctionType(memRef.Member.Name, AstType.MakePlaceholderType(), checker.argument_types.Select(arg => arg.Clone()));
                    (symbol, overload_signature) = GetOverloadOfMethod(type_table, memRef.Member.Name, func_type, call_expr2, name.StartsWith("System", StringComparison.CurrentCulture));

                    // simple.TypeArguments could contain `ParameterType`s
                    // when their declaring type is a generic type definition
                    if(target_type is SimpleType simple){
                        var type_params = type_table.TypeParameters;
                        var type_args = simple.TypeArguments;
                        var keyvalue_types = type_params.Zip(type_args, (l, r) => new {TypeParameter = l, TypeArgument = r})
                                                        .Select(pair => (pair.TypeArgument is KeyValueType keyvalue) ? keyvalue : 
                                                            AstType.MakeKeyValueType(pair.TypeParameter.Clone(), pair.TypeArgument.Clone())
                                                        );

                        // This code is needed because otherwise, we would overwrite symbol.Type
                        var cloned = symbol.Clone();
                        // VisitCallExpression will take care of type parameters defined both in methods and types
                        // when it has its own type parameters
                        // otherwise replace them here so that we can verify type validity on arguments
                        if(symbol.Type is FunctionType symbol_func_type && !symbol_func_type.TypeParameters.Any()){
                            cloned.Type = ExpressoCompilerHelpers.SubstituteGenericTypes(cloned.Type, keyvalue_types);
                            symbol = cloned;
                        }

                        type_type_args = keyvalue_types.ToArray();

                        if(overload_signature != null){
                            overload_signature = (FunctionType)ExpressoCompilerHelpers.SubstituteGenericTypes(overload_signature.Clone(), keyvalue_types);
                            BindOverloadSignature(overload_signature);
                            call_expr2.OverloadSignature = (FunctionType)overload_signature.Clone();
                        }
                    }else{
                        if(overload_signature != null){
                            BindOverloadSignature(overload_signature);
                            call_expr2.OverloadSignature = (FunctionType)overload_signature.Clone();
                        }
                    }
                }else{
                    symbol = type_table.GetSymbolInTypeChain(memRef.Member.Name);
                }

                if(symbol == null){
                    var assignment = memRef.Ancestors
                                           .OfType<AssignmentExpression>()
                                           .FirstOrDefault();
                    if(assignment != null && assignment.Left.Descendants.Any(descendant => ReferenceEquals(descendant, memRef)))
                        symbol = type_table.GetSymbol("set_" + memRef.Member.Name);
                    else
                        symbol = type_table.GetSymbol("get_" + memRef.Member.Name);

                    if(symbol == null){
                        if(memRef.Parent is CallExpression call && call.Target == memRef){
                            // ES1600 thrown
                            throw new ParserException(
                                $"The type `{target_type}` does not have a method '{memRef.Member.Name}({ExpressoCompilerHelpers.StringifyList(checker.argument_types)})'!",
                                "ES2002",
                                memRef.Member
                            );
                        }else{
                            throw new ParserException(
                                $"The type `{target_type}` does not have a field or a property '{memRef.Member.Name}'!",
                                "ES2001",
                                memRef.Member
                            );
                        }
                    }
                }

                // We can't move this code to FlowChecker because it uses target_type
                var modifiers = symbol.Modifiers;
                if(modifiers.HasFlag(Modifiers.Private)){
                    var surrounding_type = memRef.Ancestors.OfType<TypeDeclaration>().FirstOrDefault();
                    if(PrivateIsInvalid(target_type, memRef)){
                        throw new ParserException(
                            $"A private field cannot be accessed from outside its surrounding scope: `{memRef}`.",
                            "ES3300",
                            memRef.Member
                        );
                    }
                }

                if(modifiers.HasFlag(Modifiers.Protected)){
                    if(PrivateIsInvalid(target_type, memRef) && ProtectedIsInvalid(target_type, memRef)){
                        throw new ParserException(
                            $"A protected field cannot be accessed from outside its surrounding or derived classes: `{memRef}`.",
                            "ES3301",
                            memRef.Member
                        );
                    }
                }

                memRef.Member.Type.ReplaceWith(symbol.Type.Clone());
                // We need to call ResolveType in order to substitute type parameters
                var real_type = ResolveType(memRef.Member);
                // Resolve the name here because we defer it until this point
                memRef.Member.IdentifierId = symbol.IdentifierId;
                memRef.Member.Modifiers = modifiers;
                if(symbol.Name.StartsWith("get_", StringComparison.CurrentCulture) && real_type is FunctionType func_type2)
                    return func_type2.ReturnType;
                else
                    return real_type;
            }

            public AstType VisitPathExpression(PathExpression pathExpr)
            {
                if(pathExpr.Items.Count == 1){
                    var type = VisitIdentifier(pathExpr.AsIdentifier);
                    // This indicates that the Identifier is likely to represent a type.
                    if(type.IsNull){
                        var type_name = pathExpr.AsIdentifier.Name;
                        var type_table = checker.symbols.GetTypeTable(type_name);
                        if(type_table.TypeParameters.Any() && !pathExpr.TypeArguments.Any()){
                            throw new ParserException(
                                $"The type reference `{type_name}` must contain a generic type list(<...>).",
                                "ES2500",
                                pathExpr
                            ){
                                HelpObject = type_table.TypeParameters.Count
                            };
                        }

                        // The type arguments will be used in methods like TypeInferenceRunner.VisitMemberReference
                        return AstType.MakeSimpleType(type_name, pathExpr.TypeArguments.Select(ta => ta.Clone()));
                    }else{
                        return type;
                    }
                }else{
                    var table = checker.symbols;
                    AstType result = AstType.Null;
                    foreach(var item in pathExpr.Items){
                        if(table.HasTypeSymbolInAnyScope(item.Name)){
                            var tmp_type = table.GetTypeSymbolInAnyScope(item.Name);
                            result = tmp_type.Type.Clone();
                            item.Type.ReplaceWith(result);
                            table = table.GetTypeTable(item.Name);
                        }else if(table.HasSymbolInAnyScope(item.Name).Item1){
                            var tmp = table.GetSymbolInAnyScope(item.Name);
                            result = tmp.Type.Clone();
                            item.Type.ReplaceWith(result);
                        }else{
                            throw new ParserException(
                                $"The type or symbol name '{item.Name}' is not declared.",
                                "ES1700",
                                item
                            );
                        }
                    }

                    return result.Clone();
                }
            }

            public AstType VisitParenthesizedExpression(ParenthesizedExpression parensExpr)
            {
                return parensExpr.Expression.AcceptWalker(this);
            }

            public AstType VisitObjectCreationExpression(ObjectCreationExpression creation)
            {
                if(!creation.TypeArguments.Any()){
                    // Infer the type arguments here for generic class object creation
                    var arg_types = creation.Items
                                            .Select(item => item.ValueExpression.AcceptWalker(this))
                                            .ToList();
                    var type_path = creation.TypePath;
                    var tmp_table = (type_path is MemberType member3) ? checker.symbols.GetTypeTable(member3.Target.Name) : checker.symbols;
                    if(tmp_table.TypeKind != ClassType.Enum){
                        // On this path, `tmp_table` must represent the current scope
                        var type_table = tmp_table.GetTypeTable(!type_path.IdentifierNode.Type.IsNull ? type_path.IdentifierNode.Type.Name : type_path.Name);

                        if(type_table != null){
                            var ctor_symbols = type_table.GetSymbols("constructor");
                            if(ctor_symbols != null){
                                var ctor_symbol = ctor_symbols.First();
                                var type_params = ((FunctionType)ctor_symbol.Type).TypeParameters;
                                var type_args = type_params.Zip(arg_types, (l, r) => new {ParameterType = l, ArgumentType = r})
                                                            .Where(args => args.ParameterType is ParameterType)
                                                            .Select(args => AstType.MakeKeyValueType(args.ParameterType.Clone(), args.ArgumentType.Clone()));
                                creation.TypeArguments.ReplaceWith(type_args);
                            }
                        }
                    }else{
                        // On this path, `tmp_table` must represent the enum
                        if(tmp_table.TypeParameters != null && tmp_table.TypeParameters.Any()){
                            throw new ParserException(
                                "Type inference on enum object creations is not supported.",
                                "ES1410",
                                creation
                            );
                        }
                    }
                }

                if(creation.TypeArguments.Any()){
                    if(creation.TypePath is SimpleType simple)
                        simple.TypeArguments.ReplaceWith(creation.TypeArguments.Select(ta => ta.ValueType.Clone()));
                    else if(creation.TypePath is MemberType member_typepath)
                        member_typepath.ChildType.TypeArguments.ReplaceWith(creation.TypeArguments.Select(ta => ta.ValueType.Clone()));
                    else
                        throw new InvalidOperationException("Unknown AstType!");
                }

                foreach(var keyvalue in creation.Items){
                    // Walk through creation.Items in order to make them type-aware
                    keyvalue.ValueExpression.AcceptWalker(this);
                }

                var table = (creation.TypePath is MemberType member) ? checker.symbols.GetTypeTable(member.Target.Name) : checker.symbols;
                if(table.TypeKind == ClassType.Enum && creation.TypePath is MemberType member2){
                    var enum_type_symbol = checker.symbols.GetTypeSymbolInAnyScope(member2.Target.Name);
                    var enum_member = table.GetSymbol(creation.TypePath.Name);
                    if(enum_member == null){
                        throw new ParserException(
                            $"The enum `{table.Name}` does not have a variant named '{member2.MemberName}'.",
                            "ES1503",
                            creation
                        ){
                            HelpObject = ExpressoCompilerHelpers.StringifyList(table.Symbols.Where(s => s.Type is SimpleType))
                        };
                    }

                    // We need to clone it because there can be another code that instantiates the enum with other types
                    var enum_member_type = (SimpleType)enum_member.Type.Clone();
                    enum_member_type.TypeArguments.ReplaceWith(creation.TypeArguments.Select(ta => ta.Clone()));
                    // This code is needed because we need to substitute the other type of a variant
                    ExpressoCompilerHelpers.SubstituteGenericTypes(enum_member_type.IdentifierNode.Type, creation.TypeArguments);
                    ExpressoCompilerHelpers.BindTypeName(checker.symbols, enum_member_type.IdentifierNode.Type);
                    return enum_member_type;
                }

                var type_symbol = table.GetTypeSymbolInAnyScope(creation.TypePath.Name);
                if(type_symbol == null){
                    throw new ParserException(
                        $"The type `{creation.TypePath}` is not found or accessible from the scope {creation.TypePath.Name}.",
                        "ES1501",
                        creation
                    );
                }else{
                    creation.TypePath.IdentifierNode.Type = type_symbol.Type.Clone();
                }

                return creation.TypePath;
            }

            public AstType VisitSequenceInitializer(SequenceInitializer seqInitializer)
            {
                // If the node given represents an empty sequence
                // then we are giving up inferring the type of the elements
                if(seqInitializer.Items.Count == 0)
                    return seqInitializer.ObjectType;

                // The type of the elements can be seen as the most restricted type
                // between all the elements.
                if(seqInitializer.ObjectType.Identifier == "dictionary"){
                    var first_elem = seqInitializer.Items.FirstOrNullObject() as KeyValueLikeExpression;
                    if(first_elem == null)
                        throw new InvalidOperationException("The first element is not a key value expression. Something wrong has occurred.");

                    var key_type = first_elem.KeyExpression.AcceptWalker(this);
                    var value_type = first_elem.ValueExpression.AcceptWalker(this);
                    foreach(var item in seqInitializer.Items.Skip(1).Cast<KeyValueLikeExpression>()){
                        var tmp_key = item.KeyExpression.AcceptWalker(this);
                        var tmp_value = item.ValueExpression.AcceptWalker(this);

                        if(checker.IsCompatibleWith(key_type, tmp_key) == TriBool.False){
                            parser.ReportSemanticErrorRegional(
                                $"The key types differ; Expected: `{key_type}`, found: `{tmp_key}`.",
                                "ES1102",
                                first_elem.KeyExpression, item.KeyExpression
                            );
                        }

                        if(checker.IsCompatibleWith(value_type, tmp_value) == TriBool.False){
                            parser.ReportSemanticErrorRegional(
                                $"The value types differ; Expected: `{value_type}`, found: `{tmp_value}`.",
                                "ES1103",
                                first_elem.ValueExpression, item.ValueExpression
                            );
                        }
                    }
                    seqInitializer.ObjectType.TypeArguments.FirstOrNullObject().ReplaceWith(key_type.Clone());
                    seqInitializer.ObjectType.TypeArguments.LastOrNullObject().ReplaceWith(value_type.Clone());

                    return AstType.MakeSimpleType("dictionary", new []{
                        key_type.Clone(), value_type.Clone()
                    });
                }else{
                    var first_item = seqInitializer.Items.FirstOrNullObject();
                    var sequence_item_type = first_item.AcceptWalker(this);
                    if(sequence_item_type is PrimitiveType primitive && primitive.KnownTypeCode == KnownTypeCode.IntSeq){
                        seqInitializer.ObjectType.TypeArguments.FirstOrNullObject().ReplaceWith(AstType.MakePrimitiveType("int"));
                    }else{
                        foreach(var item in seqInitializer.Items.Skip(1)){
                            var item_type = item.AcceptWalker(this);
                            if(checker.IsCompatibleWith(sequence_item_type, item_type) == TriBool.False){
                                throw new ParserException(
                                    $"The item types differ; Expected: `{sequence_item_type}`, found: `{item_type}`.",
                                    "ES1104",
                                    first_item, item
                                );
                            }
                        }

                        seqInitializer.ObjectType.TypeArguments.FirstOrNullObject().ReplaceWith(sequence_item_type.Clone());
                    }

                    return seqInitializer.ObjectType.Clone();
                }
            }

            public AstType VisitMatchClause(MatchPatternClause matchClause)
            {
                return matchClause.Parent.AcceptWalker(this);
            }

            public AstType VisitSequenceExpression(SequenceExpression seqExpr)
            {
                // The type of the element of a sequence can be seen as the most common type
                // of the whole sequence.
                var types = 
                    from item in seqExpr.Items
                    select item.AcceptWalker(this).Clone();

                return (seqExpr.Items.Count == 1) ? types.First() : AstType.MakeSimpleType("tuple", types, seqExpr.StartLocation, seqExpr.EndLocation);
            }

            public AstType VisitUnaryExpression(UnaryExpression unaryExpr)
            {
                var tmp = unaryExpr.Operand.AcceptWalker(this);
                switch(unaryExpr.Operator){
                case OperatorType.Reference:
                    return AstType.MakeReferenceType(tmp.Clone());

                case OperatorType.Plus:
                case OperatorType.Minus:
                    {
                        var primitive_type = tmp as PrimitiveType;
                        if(primitive_type != null){
                            if(primitive_type.KnownTypeCode == KnownTypeCode.Int
                                || primitive_type.KnownTypeCode == KnownTypeCode.UInt
                                || primitive_type.KnownTypeCode == KnownTypeCode.Float
                                || primitive_type.KnownTypeCode == KnownTypeCode.Double
                                || primitive_type.KnownTypeCode == KnownTypeCode.Byte
                                || primitive_type.KnownTypeCode == KnownTypeCode.BigInteger)
                                return primitive_type;
                        }

                        // We have to report the error here because otherwise AstType.Null cause trouble
                        parser.ReportSemanticError(
                            $"Can not apply the unary operator '{unaryExpr.OperatorToken}' on type `{tmp.Name}`.",
                            "ES3200",
                            unaryExpr
                        );
                        return AstType.Null;
                    }

                case OperatorType.Not:
                    return AstType.MakeParameterType("bool");

                default:
                    throw new InvalidOperationException("Unknown unary operator!");
                }
            }

            public AstType VisitSelfReferenceExpression(SelfReferenceExpression selfRef)
            {
                var decl_types =
                    from type in selfRef.Ancestors.OfType<TypeDeclaration>()
                    select type.ReturnType;

                // self_type doesn't need type parameters
                var self_type = decl_types.First();
                selfRef.SelfIdentifier.Type.ReplaceWith(self_type.Clone());

                checker.BindTypeName(selfRef.SelfIdentifier.Type.IdentifierNode);
                return self_type;
            }

            public AstType VisitSuperReferenceExpression(SuperReferenceExpression superRef)
            {
                var decls =
                    from type in superRef.Ancestors.OfType<TypeDeclaration>()
                    from pt in type.BaseTypes
                    select pt as AstType;

                var super_type = decls.First();
                superRef.SuperIdentifier.Type.ReplaceWith(super_type.Clone());

                checker.BindTypeName(superRef.SuperIdentifier.Type.IdentifierNode);
                return super_type;
            }

            public AstType VisitNullReferenceExpression(NullReferenceExpression nullRef)
            {
                nullRef.AcceptWalker(checker);
                return SimpleType.Null;
            }

            public AstType VisitCommentNode(CommentNode comment)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitTextNode(TextNode textNode)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitTypeConstraint(TypeConstraint constraint)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitPostModifiers(PostModifiers postModifiers)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitSimpleType(SimpleType simpleType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitPrimitiveType(PrimitiveType primitiveType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitReferenceType(ReferenceType referenceType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitMemberType(MemberType memberType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitFunctionType(FunctionType funcType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitParameterType(ParameterType paramType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitPlaceholderType(PlaceholderType placeholderType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitKeyValueType(KeyValueType keyValueType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitFunctionParameterType(FunctionParameterType parameterType)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitAttributeSection(AttributeSection section)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public AstType VisitAliasDeclaration(AliasDeclaration aliasDecl)
            {
                return aliasDecl.Path.AcceptWalker(this);
            }

            public AstType VisitImportDeclaration(ImportDeclaration importDecl)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitFunctionDeclaration(FunctionDeclaration funcDecl)
            {
                return FigureOutReturnType(funcDecl.Body);
            }

            public AstType VisitTypeDeclaration(TypeDeclaration typeDecl)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitFieldDeclaration(FieldDeclaration fieldDecl)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitParameterDeclaration(ParameterDeclaration parameterDecl)
            {
                /*if(parameterDecl.Option.IsNull){
                    // ES0004 thrown
                    parser.ReportSemanticErrorRegional(
                        $"Can not infer the type of the parameter '{parameterDecl.Name}' because it doesn't have an optional value.",
                        "ES1310",
                        parameterDecl.NameToken, parameterDecl.NameToken
                    );
                }*/

                var option_type = parameterDecl.Option.AcceptWalker(this);
                parameterDecl.NameToken.Type.ReplaceWith(option_type.Clone());
                return option_type;
            }

            public AstType VisitVariableInitializer(VariableInitializer initializer)
            {
                /*if(initializer.Initializer.IsNull){
                    // ES0003 thrown
                    parser.ReportSemanticErrorRegional(
                        $"Can not infer the expression '{initializer}' because it doesn't have any context.",
                        "ES1311",
                        initializer.Pattern, initializer.Initializer
                    );
                }*/

                var init_type = initializer.Initializer.AcceptWalker(this);
                MakePatternTypeAware(initializer.Pattern.Pattern, init_type);
                
                return init_type;
            }

            public AstType VisitWildcardPattern(WildcardPattern wildcardPattern)
            {
                return SimpleType.Null;
            }

            public AstType VisitIdentifierPattern(IdentifierPattern identifierPattern)
            {
                if(identifierPattern.Ancestors.Any(a => a is PatternWithType) && !checker.inspecting_value_binding_pattern){
                    // In order to give IdentifierIds to enum variants' other types
                    if(identifierPattern.Identifier.Type.IdentifierNode.Type is SimpleType simple)
                        checker.BindSimpleType(simple);

                    return VisitIdentifier(identifierPattern.Identifier);
                }else if(identifierPattern.InnerPattern.IsNull){
                    var prev_inspecting = checker.inspecting_value_binding_pattern;
                    if(checker.inspecting_value_binding_pattern)
                        checker.inspecting_value_binding_pattern = false;

                    var type = identifierPattern.Parent.AcceptWalker(this);
                    checker.inspecting_value_binding_pattern = prev_inspecting;
                    if(IsTupleType(type)){
                        type = MakeOutTupleElementType(identifierPattern, (SimpleType)type);
                    }else if(IsContainerType(type)){
                        type = MakeOutElementType(type);
                    }else if(IsIntSeqType(type)){
                        type = MakeOutElementType(type);
                    }else if(IsDictionaryType(type)){
                        
                    }else{
                        // type is a user defined type
                        var table = checker.symbols.GetTypeTable(type.Name);
                        if(table == null && type is MemberType member){
                            // This represents a tuple-like enum
                            table = checker.symbols.GetTypeTable(member.Target.Name);
                            if(table == null){
                                parser.ReportSemanticError(
                                    $"The type symbol '{type}' turns out not to be declared or accessible in the current scope {checker.symbols.Name}!",
                                    "ES0101",
                                    identifierPattern
                                );
                            }else{
                                var symbol = table.GetSymbol(type.Name);
                                var tuple_type = ((SimpleType)symbol.Type).IdentifierNode.Type.Clone();
                                if(checker.cur_context_type is SimpleType simple_type && simple_type.TypeArguments.Any()){
                                    var type_args = simple_type.TypeArguments;
                                    var keyvalue_pairs = table.TypeParameters.Zip(type_args, (l, r) => new {Parameter = l, Argument = r})
                                                              .Select(pair => AstType.MakeKeyValueType(pair.Parameter.Clone(), pair.Argument.Clone()));
                                    type = ExpressoCompilerHelpers.SubstituteGenericTypes(tuple_type, keyvalue_pairs);
                                }
                                type = MakeOutTupleElementType(identifierPattern, (SimpleType)tuple_type);
                            }
                        }else if(table == null){
                            parser.ReportSemanticError(
                                $"The type symbol '{type.Name}' turns out not to be declared or accessible in the current scope {checker.symbols.Name}!",
                                "ES0101",
                                identifierPattern
                            );
                        }else{
                            var symbol = table.GetSymbol(identifierPattern.Identifier.Name);
                            if(symbol == null) {
                                throw new ParserException(
                                    $"The type `{type}` does not have a field named '{identifierPattern.Identifier.Name}'.",
                                    "ES2410",
                                    identifierPattern
                                );
                            }
                            type = symbol.Type;
                        }
                    }

                    return type;
                }else{
                    var type = identifierPattern.InnerPattern.AcceptWalker(this);
                    if(IsIntSeqType(type))
                        type = AstType.MakePrimitiveType("int", type.StartLocation);

                    return type;
                }
            }

            public AstType VisitTuplePattern(TuplePattern tuplePattern)
            {
                if(tuplePattern.Ancestors.Any(a => a is PatternWithType)){
                    var elem_types =
                        from elem in tuplePattern.Patterns
                        select elem.AcceptWalker(this).Clone();
                    return AstType.MakeSimpleType("tuple", elem_types);
                }else{
                    return tuplePattern.Parent.AcceptWalker(this);
                }
            }

            public AstType VisitCollectionPattern(CollectionPattern collectionPattern)
            {
                return collectionPattern.Parent.AcceptWalker(this);
            }

            public AstType VisitDestructuringPattern(DestructuringPattern destructuringPattern)
            {
                var type_path = destructuringPattern.TypePath;
                if(type_path is MemberType member){
                    var type_table = checker.symbols.GetTypeTable(member.Target.Name);
                    if(type_table != null && type_table.TypeKind == ClassType.Enum){
                        destructuringPattern.IsEnum = true;
                        // This code is needed because VisitIdentifierPattern expects full paths
                        return checker.inspecting_value_binding_pattern ? member.Target : type_path;
                    }else{
                        return member.ChildType;
                    }
                }else{
                    return destructuringPattern.TypePath;
                }
            }

            public AstType VisitExpressionPattern(ExpressionPattern exprPattern)
            {
                return exprPattern.Expression.AcceptWalker(this);
            }

            public AstType VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
            {
                return SimpleType.Null;
            }

            public AstType VisitKeyValuePattern(KeyValuePattern keyValuePattern)
            {
                return keyValuePattern.Parent.AcceptWalker(this);
            }

            public AstType VisitPatternWithType(PatternWithType pattern)
            {
                // TODO: consider the match statement
                var type = pattern.Pattern.AcceptWalker(this);
                if(pattern.Type is PlaceholderType)
                    return type;

                if(type is SimpleType simple && pattern.Type is SimpleType tuple){
                    if(simple.Name != "tuple")
                        return type;
                    
                    // TODO: take resolving type name into account
                    foreach(var pair in tuple.TypeArguments.Zip(simple.TypeArguments, (l, r) => new {Lhs = l, Rhs = r}))
                        pair.Rhs.ReplaceWith(pair.Lhs.Clone());

                    return pattern.Type;
                }else if(type is SimpleType && pattern.Type is PrimitiveType primitive && primitive.Name == "int"){
                    return pattern.Type;
                }else if(type is SimpleType val_tuple2 && !(pattern.Type is PlaceholderType) || pattern.Type is SimpleType tuple2 && !(type is PlaceholderType)){
                    throw new ParserException(
                        $"The type in the type annotation `{pattern.Type}` conflicts with the expression type `{type}`.",
                        "ES1304",
                        pattern
                    );
                }else{
                    type.ReplaceWith(pattern.Type.Clone());
                    if(pattern.Pattern is IdentifierPattern ident_pat){
                        ResolveType(ident_pat.Identifier);
                        return pattern.Type;
                    }else{
                        throw new Exception("Unreachable");
                    }
                }
            }

            public AstType VisitTypePathPattern(TypePathPattern pathPattern)
            {
                if(pathPattern.TypePath is MemberType member_type)
                    return member_type.Target;
                else
                    throw new InvalidOperationException("A TypePathPattern expects its type path to be a MemberType. Something wrong has occurred.");
            }

            public AstType VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
            {
                return VisitVariableInitializer(valueBindingPattern.Initializer);
            }

            public AstType VisitNullNode(AstNode nullNode)
            {
                // Just ignore it.
                return AstType.Null;
            }

            public AstType VisitNewLine(NewLineNode newlineNode)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitWhitespace(WhitespaceNode whitespaceNode)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitExpressoTokenNode(ExpressoTokenNode tokenNode)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            public AstType VisitPatternPlaceholder(AstNode placeholder, Pattern child)
            {
                throw new InvalidOperationException("Can not work on that node!");
            }

            #endregion

            static bool IsIntSeqType(AstType type)
            {
                return type is PrimitiveType primitive && primitive.KnownTypeCode == KnownTypeCode.IntSeq;
            }

            internal AstType FigureOutReturnType(Statement statement)
            {
                if(statement is BlockStatement block){
                    return FigureOutReturnType(block.Statements.Last());
                }else if(statement is ReturnStatement return_stmt){
                    return return_stmt.Expression.IsNull ? AstType.MakeSimpleType("tuple") : return_stmt.Expression.AcceptWalker(this);
                }else if(statement is IfStatement last_if){
                    var func = statement.Parent.Parent as FunctionDeclaration;
                    var closure = statement.Parent.Parent as ClosureLiteralExpression;
                    if(func != null && ExpressoCompilerHelpers.IsVoidType(func.ReturnType))
                        return func.ReturnType;
                    else if(closure != null && ExpressoCompilerHelpers.IsVoidType(closure.ReturnType))
                        return closure.ReturnType;

                    if(last_if.FalseStatement.IsNull){
                        return FigureOutReturnType(last_if.TrueBlock);
                    }else{
                        // FIXME: This code can't take else if statements into account
                        // REVISE: A series of if statements becomes a tree construct
                        // so we may ignore the above comment
                        var true_return_type = FigureOutReturnType(last_if.TrueBlock);
                        var false_return_type = FigureOutReturnType(last_if.FalseStatement);
                        if(!ExpressoCompilerHelpers.IsVoidType(true_return_type) && !ExpressoCompilerHelpers.IsVoidType(false_return_type)){
                            return checker.FigureOutCommonType(true_return_type, false_return_type);
                        }else if(func != null && ExpressoCompilerHelpers.IsPlaceholderType(func.ReturnType) ||
                                      closure != null && ExpressoCompilerHelpers.IsPlaceholderType(closure.ReturnType)){
                            if(ExpressoCompilerHelpers.IsVoidType(true_return_type) && ExpressoCompilerHelpers.IsVoidType(false_return_type))
                                return AstType.MakeSimpleType("tuple");

                            parser.ReportSemanticErrorRegional(
                                $"All code paths must return something. {(true_return_type.IsNull ? "The true block" : "The false block")} returns nothing.",
                                "ES1800",
                                last_if.TrueBlock, last_if.FalseStatement
                            );
                        }
                    }
                }else if(statement is ForStatement || statement is ValueBindingForStatement || statement is TryStatement || statement is BreakStatement
                    || statement is ContinueStatement || statement is EmptyStatement){
                    return AstType.MakeSimpleType("tuple");
                }else if(statement is MatchStatement match_stmt){
                    return FigureOutReturnType(match_stmt.Clauses.First().Body);
                }else if(statement is ThrowStatement){
                    // This indicates a different case from using a void type
                    return SimpleType.Null;
                }

                return AstType.MakeSimpleType("tuple");
            }

            void MakePatternTypeAware(PatternConstruct pattern, AstType type)
            {
                if(pattern is IdentifierPattern ident_pat){
                    ident_pat.Identifier.Type.ReplaceWith(type);
                }else if(pattern is TuplePattern tuple_pat && type is SimpleType tuple_type){
                    foreach(var pair in tuple_pat.Patterns.Zip(tuple_type.TypeArguments, (l, r) => new {Pattern = l, Type = r})){
                        if(pair.Pattern is IdentifierPattern ident_pat2)
                            ident_pat2.Identifier.Type.ReplaceWith(pair.Type);
                    }
                }
            }

            AstType ResolveType(Identifier ident)
            {
                if(!ident.Type.IdentifierNode.Type.IsNull || !(ident.Type is SimpleType)){
                    if(ident.Type is ParameterType param_type && InheritedTypeParameters != null){
                        // Resolve type parameters on variables here
                        var type_param = InheritedTypeParameters.First(tp => tp.KeyType.Name == param_type.Name);
                        ident.Type.ReplaceWith(type_param.ValueType.Clone());
                    }else if(ident.Type is ParameterType param_type2 && CallAllTypeArgs != null){
                        var type_param = CallAllTypeArgs.First(ta => ta.KeyType.Name == param_type2.Name);
                        ident.Type.ReplaceWith(type_param.Clone());
                    }

                    return ident.Type;
                }
                
                var type_symbol = checker.symbols.GetTypeSymbolInAnyScope(ident.Type.Name);
                if(type_symbol != null && ident.Type.Name != type_symbol.Type.Name)
                    ExpressoCompilerHelpers.ResolveTypeAlias((SimpleType)ident.Type, type_symbol);

                return ident.Type;
            }

            (Identifier, FunctionType) GetOverloadOfMethod(SymbolTable table, string methodName, FunctionType realFuncType, CallExpression callExpression,
                                                           bool tryingToCallMethodInSystemNamespace)
            {
                var symbols = table.GetSymbolsInTypeChain(methodName);
                if(symbols == null){
                    throw new ParserException(
                        $"There is no candidate for '{methodName}' in the type table {table.Name} and in any parent types.",
                        "ES1600",
                        callExpression
                    ){
                        HelpObject = tryingToCallMethodInSystemNamespace
                    };
                }

                if(symbols.Count() == 1){
                    var symbol = symbols.First();
                    var symbol_func_type = symbol.Type.Clone() as FunctionType;
                    if(symbol_func_type == null){
                        var surrounding_func = callExpression.Ancestors.OfType<FunctionDeclaration>().First();
                        var afterward_func = surrounding_func.GetNextSibling(s => s is FunctionDeclaration func_decl && func_decl.Name == methodName);
                        // We need to duplicate these errors because otherwise the code continues with symbol_func_type being null
                        if(surrounding_func.Name == methodName){
                            throw new ParserException(
                                "Function signatures must be completed when they are called recursively.",
                                "ES4100",
                                callExpression
                            );
                        }else if(afterward_func != null){
                            throw new ParserException(
                                "Function signatures must be completed when they are defined after the callee.",
                                "ES4101",
                                callExpression
                            );
                        }

                        // This means that we've hit a method that is defined after this type declaration
                        var next_type_decl = callExpression.Ancestors.OfType<TypeDeclaration>()
                                                                     .First()
                                                                     .NextSibling as TypeDeclaration;
                        if(next_type_decl != null){
                            var wanted_method = next_type_decl.Members.OfType<FunctionDeclaration>()
                                                                      .FirstOrDefault(m => m.Name == methodName);
                            if(wanted_method == null){
                                // ES1600 thrown
                                throw new ParserException(
                                    $"'{methodName}' should be defined in the type right after the current type.",
                                    "ES0122",
                                    callExpression
                                );
                            }

                            // Check type validity on the pre-called method
                            var cur_table = checker.symbols;
                            var tmp_counter = checker.scope_counter;
                            SetSymbolTable(wanted_method);
                            checker.VisitFunctionDeclaration(wanted_method);

                            checker.pre_inspected_funcs.Add(wanted_method.NameToken.IdentifierId);

                            checker.symbols = cur_table;
                            checker.scope_counter = tmp_counter;

                            symbol_func_type = (FunctionType)wanted_method.NameToken.Type;
                        }else{
                            // ES0101 thrown
                            throw new ParserException(
                                "We expect that the item immediately after the current type should be a type declaration.",
                                "ES0123",
                                callExpression
                            );
                        }
                    }
                    // This couldn't happen?
                    // We don't need to substitute type parameters here
                    /*foreach(var pair in symbol_func_type.Parameters.Cast<FunctionParameterType>()
                                                        .Zip(realFuncType.Parameters, (l, r) => new {Parameter = l, Argument = r})){
                        if(pair.Parameter.Type is ParameterType)
                            pair.Parameter.ReplaceWith(pair.Argument.Clone());
                    }

                    if(symbol_func_type.TypeParameters.Count > 0 || !callExpression.TypeArguments.Any()){
                        callExpression.TypeArguments.ReplaceWith(symbol_func_type.TypeParameters.Select(tp => AstType.MakeKeyValueType((ParameterType)tp.Clone(), AstType.MakePlaceholderType())));
                        ExpressoCompilerHelpers.SubstituteGenericTypes(symbol_func_type, checker.argument_types, callExpression.TypeArguments);
                    }*/

                    // When there is a call expression, we should always return an overload signature
                    // because otherwise we can't find method overload
                    return (symbol, symbol_func_type);
                }

                if(realFuncType.Parameters.Count == 0){
                    var symbol = table.GetSymbol(methodName, realFuncType);
                    return (symbol, (FunctionType)symbol.Type.Clone());
                }

                foreach(var pair in Enumerable.Range(0, realFuncType.Parameters.Count).Reverse().Zip(realFuncType.Parameters.Reverse(),
                                                                                                     (l, r) => new {Index = l, Parameter = r})){
                    var new_parameters = realFuncType.Parameters
                                                     .Take(pair.Index + 1)
                                                     .Select(t => t.Clone());
                    var new_func_type = AstType.MakeFunctionType(realFuncType.Name, realFuncType.ReturnType.Clone(), new_parameters);
                    var symbol = table.GetSymbol(methodName, new_func_type);
                    if(symbol != null){
                        var symbol_type = (FunctionType)symbol.Type.Clone();
                        return (symbol, symbol_type);
                    }

                    var new_parameters2 = realFuncType.Parameters
                                                      .Take(pair.Index)
                                                      .Select(t => t.Clone())
                                                      .Concat(
                                                          Enumerable.Range(0, realFuncType.Parameters.Count - pair.Index)
                                                                    .Select(_ => AstType.MakeSimpleType(AstNode.MakeIdentifier("object")))
                                                      );
                    var new_func_type2 = AstType.MakeFunctionType(realFuncType.Name, realFuncType.ReturnType.Clone(), new_parameters2);
                    var symbol2 = table.GetSymbol(methodName, new_func_type2);
                    if(symbol2 != null){
                        var symbol2_type = (FunctionType)symbol2.Type.Clone();
                        return (symbol2, symbol2_type);
                    }

                    var new_parameters3 = realFuncType.Parameters
                                                      .Take(pair.Index)
                                                      .Select(t => t.Clone())
                                                      .Concat(
                                                          new []{AstType.MakeSimpleType(
                                                              "array",
                                                              TextLocation.Empty,
                                                              TextLocation.Empty,
                                                              AstType.MakeSimpleType(AstNode.MakeIdentifier("object"))
                                                          )});
                    var new_func_type3 = AstType.MakeFunctionType(realFuncType.Name, realFuncType.ReturnType.Clone(), new_parameters3);
                    var symbol3 = table.GetSymbol(methodName, new_func_type3);
                    if(symbol3 != null){
                        var symbol3_type = (FunctionType)symbol3.Type.Clone();
                        return (symbol3, symbol3_type);
                    }
                }

                throw new ParserException(
                    $"There is no method overload best matched for '{methodName}' with `{realFuncType}`.",
                    "ES1601",
                    callExpression
                );
            }

            void SetSymbolTable(FunctionDeclaration funcDecl)
            {
                var tmp = checker.symbols;
                while(!tmp.Name.StartsWith("type", StringComparison.CurrentCulture))
                    tmp = tmp.Parent;

                var type_parent = tmp.Parent;
                var type_index = type_parent.Children.IndexOf(tmp);
                var target_type = type_parent.Children[type_index + 1];
                var table = target_type.GetChildTableWithFuncName(funcDecl.Name);

                var target_func_index = target_type.Children.IndexOf(table);
                checker.symbols = target_type;
                checker.scope_counter = target_func_index;
            }

            AstType CheckTypeConstraintsOnType(AstType targetType, MemberReferenceExpression childMemRef, MemberReferenceExpression memRef)
            {
                var constrained_type = childMemRef.Target.AcceptWalker(this);
                var type_table = checker.symbols.GetTypeTable(constrained_type.Name);
                foreach(var type_constraint in type_table.TypeConstraints){
                    if(type_constraint.TypeParameter.Name == targetType.Name){
                        foreach(var type_constraint2 in type_constraint.TypeConstraints){
                            var constraint_type_table = checker.symbols.GetTypeTable(type_constraint2.Name);
                            var member_symbol = constraint_type_table.GetSymbol(memRef.Member.Name);
                            if(member_symbol == null){
                                throw new ParserException(
                                    $"The concrete type `{type_constraint2.Name}` does not have a field or method named '{memRef.Member.Name}'.",
                                    "ES2004",
                                    memRef.Member
                                );
                            }

                            memRef.Member.Type.ReplaceWith(member_symbol.Type.Clone());
                            memRef.Member.IdentifierId = member_symbol.IdentifierId;
                            memRef.Member.Modifiers = member_symbol.Modifiers;

                            return member_symbol.Type;
                        }

                        if(type_constraint.TypeConstraints.Any()){
                            // ES2004 thrown
                            throw new ParserException(
                                $"None of the types on the type parameter `{type_constraint.TypeParameter}` does not have a field or method named '{memRef.Member.Name}'.",
                                "ES2200",
                                memRef
                            );
                        }else{
                            throw new ParserException(
                                $"The type parameter `{targetType}` should have type constraints in order to refer to their methods or fields.",
                                "ES2201",
                                memRef
                            );
                        }
                    }
                }

                return null;
            }

            bool PrivateIsInvalid(AstType targetType, MemberReferenceExpression memRef)
            {
                var surrounding_type = memRef.Ancestors.OfType<TypeDeclaration>().FirstOrDefault();
                return surrounding_type == null || targetType.Name != surrounding_type.Name;
            }

            bool ProtectedIsInvalid(AstType targetType, MemberReferenceExpression memRef)
            {
                var surrounding_type = memRef.Ancestors.OfType<TypeDeclaration>().FirstOrDefault();
                if(surrounding_type == null)
                    return true;

                return ProtectedIsInvalidImpl(targetType, AstType.MakeSimpleType(surrounding_type.Name));
            }

            bool ProtectedIsInvalidImpl(AstType targetType, AstType inspectingType)
            {
                if(targetType.Name == inspectingType.Name)
                    return false;

                var table = checker.symbols.GetTypeTable(inspectingType.Name);
                if(table == null)
                    throw new InvalidOperationException("table is missing.");

                foreach(var parent_type in table.ParentTypes){
                    if(ProtectedIsInvalidImpl(targetType, parent_type))
                        return false;
                }

                return true;
            }

            // This method is for resolving parameter types and then being used for resolving a correct method
            void BindOverloadSignature(FunctionType overloadSignature)
            {
                foreach(var param_type in overloadSignature.Parameters.OfType<FunctionParameterType>()){
                    if(param_type.Type is SimpleType simple)
                        checker.BindSimpleType(simple);
                }
            }

            static AstType MakeOutTupleElementType(AstNode node, SimpleType type)
            {
                var parent = node.Parent;
                var i = 0;
                if(parent is DestructuringPattern destructuring){
                    destructuring.Items.Any(item => {
                        if(item.IsMatch(node)){
                            return true;
                        }else{
                            ++i;
                            return false;
                        }
                    });
                }else if(parent is TuplePattern tuple){
                    tuple.Patterns.Any(p => {
                        if(p.IsMatch(node)){
                            return true;
                        }else{
                            ++i;
                            return false;
                        }
                    });
                }

                return type.TypeArguments.ElementAt(i);
            }
        }
    }
}

