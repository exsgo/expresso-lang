﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// It's responsible for replacing type parameters of call expressions with concrete types in place.
    /// </summary>
    public class InPlaceTypeArgumentReplacer : IAstTypeWalker<AstType>
    {
        IEnumerable<KeyValueType> type_arguments;

        public InPlaceTypeArgumentReplacer(IEnumerable<KeyValueType> typeArgs)
        {
            type_arguments = typeArgs;
        }

        public AstType VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            return parameterType.Type.AcceptTypeWalker(this);
        }

        public AstType VisitFunctionType(FunctionType funcType)
        {
            foreach(var param in funcType.Parameters)
                param.AcceptTypeWalker(this);

            funcType.ReturnType.AcceptTypeWalker(this);
            return funcType;
        }

        public AstType VisitKeyValueType(KeyValueType keyValueType)
        {
            return keyValueType;
        }

        public AstType VisitMemberType(MemberType memberType)
        {
            return VisitSimpleType(memberType.ChildType);
        }

        public AstType VisitParameterType(ParameterType paramType)
        {
            if(type_arguments == null)
                throw new InvalidOperationException(nameof(type_arguments));

            var type_arg = type_arguments.FirstOrDefault(arg => paramType.Name == arg.Name);
            if(type_arg != null){
                var cloned = type_arg.ValueType.Clone();
                paramType.ReplaceWith(cloned);
                // substitute type arguments
                if(!(cloned is ParameterType))
                    cloned.AcceptTypeWalker(this);

                return cloned;
            }

            return paramType;
        }

        public AstType VisitPlaceholderType(PlaceholderType placeholderType)
        {
            // This should return the placeholder type in order to issue the ES4100 error
            return placeholderType;
        }

        public AstType VisitPrimitiveType(PrimitiveType primitiveType)
        {
            return primitiveType;
        }

        public AstType VisitReferenceType(ReferenceType referenceType)
        {
            referenceType.BaseType.AcceptTypeWalker(this);
            return referenceType;
        }

        public AstType VisitSimpleType(SimpleType simpleType)
        {
            foreach(var type_arg in simpleType.TypeArguments)
                type_arg.AcceptTypeWalker(this);

            return simpleType;
        }

        public AstType VisitNullNode(AstNode nullNode)
        {
            throw new InvalidOperationException("Can not work on that node");
        }
    }
}
