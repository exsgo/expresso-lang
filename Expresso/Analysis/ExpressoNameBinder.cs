﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Expresso.CodeGen;
using Expresso.Utils;
using ICSharpCode.NRefactory.PatternMatching;


/*
 * The name binding and type resolving:
 *
 * The name binding happens in 2 passes.
 * In the first pass (full recursive walk of the AST) we resolve locals and infer types,
 * meaning when reaching a variable whose type is expressed as inference type, it will try to
 * infer the type of that variable.
 * The second pass uses the "processed" list of all context statements (functions and class
 * bodies) and has each context statement resolve its free variables to determine whether
 * references to lexically enclosing scopes.
 *
 * The second pass happens in post-order (the context statement is added into the "processed"
 * list after processing its nested functions/statements). This way, when the function is
 * processing its free variables, it also knows already which of its locals are being lifted
 * to the closure and can report error if such closure variable is being deleted.
 * 
 * The following list names constructs that will introduce new variables into the enclosing scope:
 *    TypeDeclaration,
 *    FunctionDeclaration,
 *    VariableDeclarationStatement,
 *    ValueBindingForStatement,
 *    ImportDeclaration,
 *    Comprehension(ComprehensionForClause expression)
 */

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// The name binding and name resolving:
    /// During name binding, we first define names on variables, fields, methods and types.
    /// And then bind names on those items except for types.
    /// For types we'll bind them during type validity check.
    /// By name binding, I mean that we give the same id defined on the name definition phase.
    /// That means that it is not guaranteed that the type of the target object is resolved.
    /// </summary>
    class ExpressoNameBinder : IAstWalker
	{
        int scope_counter;
        bool check_shadowing;
        SymbolTable symbol_table;
		Parser parser;
		
		ExpressoNameBinder(Parser parentalParser)
		{
			parser = parentalParser;
            symbol_table = parentalParser.Symbols;
        }
		
		#region Public surface
        /// <summary>
        /// The public surface of post-parse processing.
        /// In this method, we are binding names, checking type validity and doing some flow analisys.
        /// Note that we are NOT doing any AST-wide optimizations here.
        /// </summary>
		internal static void BindAst(ExpressoAst ast, Parser parser)
		{
			ExpressoNameBinder binder = new ExpressoNameBinder(parser);
			binder.Bind(ast);
            #if DEBUG
            Utilities.CompilerOutput.WriteLine($"We have given ids on total of {UniqueIdGenerator.CurrentId - 2u} identifiers.");
            #endif

            TypeChecker.Check(ast, parser);
            FlowChecker.Check(ast, parser);
		}
		#endregion
		
		void Bind(ExpressoAst unboundAst)
		{
            Debug.Assert(unboundAst != null, "We have to hold something to bind to");

            // This code is needed because otherwise we can't define types that point at each other
            PreDefineTypeAndFunctionIds(unboundAst);

            VisitAst(unboundAst);
		}

        void DecendScope()
		{
            symbol_table = symbol_table.Children[scope_counter];
		}
		
		void AscendScope()
		{
            symbol_table = symbol_table.Parent;
		}
		
		#region IAstWalker implementation

        public void VisitAst(ExpressoAst ast)
        {
            Debug.Assert(symbol_table.Name == "programRoot", "When entering VisitAst, symbol_table should be programRoot");
            scope_counter = 0;

            Utilities.CompilerOutput.WriteLine($"Resolving names in {ast.ModuleName}...");

            // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
            ast.Attributes.AcceptWalker(this);
            ast.Imports.AcceptWalker(this);
            ast.Declarations.AcceptWalker(this);

            Debug.Assert(symbol_table.Name == "programRoot", "When exiting VisitAst, symbol_table should be programRoot");
        }

        public void VisitBlock(BlockStatement block)
        {
            if(block.IsNull)
                return;
            
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            block.Statements.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitBreakStatement(BreakStatement breakStmt)
        {
            //no op
        }

        public void VisitContinueStatement(ContinueStatement continueStmt)
        {
            //no op
        }

        public void VisitDoWhileStatement(DoWhileStatement doWhileStmt)
        {
            VisitWhileStatement(doWhileStmt.Delegator);
        }

        public void VisitEmptyStatement(EmptyStatement emptyStmt)
        {
            //no op
        }

        public void VisitForStatement(ForStatement forStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            forStmt.Left.AcceptWalker(this);
            forStmt.Target.AcceptWalker(this);
            VisitBlock(forStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            check_shadowing = true;
            valueBindingForStmt.Initializer.AcceptWalker(this);
            check_shadowing = false;
            VisitBlock(valueBindingForStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitIfStatement(IfStatement ifStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            ifStmt.Condition.AcceptWalker(this);
            VisitBlock(ifStmt.TrueBlock);
            // We can't rewrite this to VisitBlock(ifStmt.FalseBlock);
            // because doing so can continue execution even if the node is null
            ifStmt.FalseStatement.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitReturnStatement(ReturnStatement returnStmt)
        {
            returnStmt.Expression.AcceptWalker(this);
        }

        public void VisitMatchStatement(MatchStatement matchStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            matchStmt.Target.AcceptWalker(this);
            matchStmt.Clauses.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitThrowStatement(ThrowStatement throwStmt)
        {
            throwStmt.CreationExpression.AcceptWalker(this);
        }

        public void VisitTryStatement(TryStatement tryStmt)
        {
            VisitBlock(tryStmt.EnclosingBlock);
            tryStmt.CatchClauses.AcceptWalker(this);
            // Directly calling VisitFinally continues execution even if the node is null.
            tryStmt.FinallyClause.AcceptWalker(this);
        }

        public void VisitWhileStatement(WhileStatement whileStmt)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            whileStmt.Condition.AcceptWalker(this);
            VisitBlock(whileStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitYieldStatement(YieldStatement yieldStmt)
        {
            yieldStmt.Expression.AcceptWalker(this);
        }

        public void VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl)
        {
            check_shadowing = true;
            varDecl.Variables.AcceptWalker(this);
            check_shadowing = false;
        }

        public void VisitArrayInitializer(ArrayInitializerExpression arrayInitializer)
        {
            arrayInitializer.TypePath.AcceptWalker(this);
        }

        public void VisitAssignment(AssignmentExpression assignment)
        {
            assignment.Left.AcceptWalker(this);
            assignment.Right.AcceptWalker(this);
        }

        public void VisitBinaryExpression(BinaryExpression binaryExpr)
        {
            binaryExpr.Left.AcceptWalker(this);
            binaryExpr.Right.AcceptWalker(this);
        }

        public void VisitCallExpression(CallExpression callExpr)
        {
            callExpr.Target.AcceptWalker(this);
            callExpr.Arguments.AcceptWalker(this);
        }

        public void VisitCastExpression(CastExpression castExpr)
        {
            castExpr.Target.AcceptWalker(this);
            castExpr.ToType.AcceptWalker(this);
        }

        public void VisitCatchClause(CatchClause catchClause)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            UniqueIdGenerator.DefineNewId(catchClause.Identifier);
            VisitBlock(catchClause.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitClosureLiteralExpression(ClosureLiteralExpression closure)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            closure.Parameters.AcceptWalker(this);
            VisitBlock(closure.Body);
            closure.ReturnType.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitComprehensionExpression(ComprehensionExpression comp)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            comp.Body.AcceptWalker(this);
            comp.Item.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitComprehensionForClause(ComprehensionForClause compFor)
        {
            compFor.Left.AcceptWalker(this);
            compFor.Target.AcceptWalker(this);
            compFor.Body.AcceptWalker(this);
        }

        public void VisitComprehensionIfClause(ComprehensionIfClause compIf)
        {
            compIf.Condition.AcceptWalker(this);
            compIf.Body.AcceptWalker(this);
        }

        public void VisitConditionalExpression(ConditionalExpression condExpr)
        {
            condExpr.Condition.AcceptWalker(this);
            condExpr.TrueExpression.AcceptWalker(this);
            condExpr.FalseExpression.AcceptWalker(this);
        }

        public void VisitFinallyClause(FinallyClause finallyClause)
        {
            VisitBlock(finallyClause.Body);
        }

        public void VisitLiteralExpression(LiteralExpression literal)
        {
            // no op
        }

        public void VisitIdentifier(Identifier ident)
        {
            BindNameOrTypeName(ident);
        }

        public void VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq)
        {
            intSeq.Start.AcceptWalker(this);
            intSeq.End.AcceptWalker(this);
            intSeq.Step.AcceptWalker(this);
        }

        public void VisitIndexerExpression(IndexerExpression indexExpr)
        {
            indexExpr.Target.AcceptWalker(this);
            indexExpr.Arguments.AcceptWalker(this);
        }

        public void VisitMemberReference(MemberReferenceExpression memRef)
        {
            // At this point we can't figure out which scope to use for the member expression
            // because we don't know anything about the type of the target expression.
            // So for now bind only the target expression.
            // Instead we'll do that in type check phase.
            memRef.Target.AcceptWalker(this);
        }

        public void VisitParenthesizedExpression(ParenthesizedExpression parensExpr)
        {
            parensExpr.Expression.AcceptWalker(this);
        }

        public void VisitSequenceInitializer(SequenceInitializer seqInitializer)
        {
            seqInitializer.Items.AcceptWalker(this);
        }

        public void VisitMatchClause(MatchPatternClause matchClause)
        {
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            matchClause.Patterns.AcceptWalker(this);
            matchClause.Guard.AcceptWalker(this);
            matchClause.Body.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitSequenceExpression(SequenceExpression seqExpr)
        {
            seqExpr.Items.AcceptWalker(this);
        }

        public void VisitUnaryExpression(UnaryExpression unaryExpr)
        {
            unaryExpr.Operand.AcceptWalker(this);
        }

        public void VisitSelfReferenceExpression(SelfReferenceExpression selfRef)
        {
            if(!selfRef.Ancestors.Any(a => a is TypeDeclaration)){
                throw new ParserException(
                    "The `self` keyword can only be used in methods!",
                    "ES0201",
                    selfRef
                );
            }

            // Defer binding the name until type check phase if the self identifier isn't type-aware
            BindName(selfRef.SelfIdentifier);
        }

        public void VisitSuperReferenceExpression(SuperReferenceExpression superRef)
        {
            BindName(superRef.SuperIdentifier);
        }

        public void VisitNullReferenceExpression(NullReferenceExpression nullRef)
        {
            // no op
        }

        public void VisitCommentNode(CommentNode comment)
        {
            // no op
        }

        public void VisitTextNode(TextNode textNode)
        {
            // no op
        }

        public void VisitTypeConstraint(TypeConstraint constraint)
        {
            constraint.TypeConstraints.AcceptWalker(this);
        }

        public void VisitPostModifiers(PostModifiers postModifiers)
        {
            foreach(var beforehand in postModifiers.RequiresToBeCalledBeforehand)
                BindName(beforehand);

            // This code is needed because they won't be linked
            var cur_func = (FunctionDeclaration)postModifiers.Parent;
            // symbol_table.Parent should point at the current type
            var symbol = symbol_table.Parent.GetSymbol(cur_func.Name);
            foreach(var beforehand in symbol.PostModifiers.RequiresToBeCalledBeforehand)
                BindName(beforehand);

            // For inside, we will look at it in TypeChecker
            /*foreach(var inside in postModifiers.RequiresToBeCalledInside)
                BindName(inside);

            var symbol2 = symbol_table.Parent.GetSymbol(cur_func.Name);
            foreach(var inside in symbol2.PostModifiers.RequiresToBeCalledInside)
                BindName(inside);*/
        }

        public void VisitSimpleType(SimpleType simpleType)
        {
            // Ignore these types because they can be different types depending on the type arguments
            if(simpleType.Name != "array" & simpleType.Name != "tuple")
                BindTypeName(simpleType.IdentifierNode);

            // At this point, type arguments of variant enums would be type parameters
            // so we can't bind them
            //if(!simpleType.IdentifierNode.Type.IsNull && simpleType.IdentifierNode.Type is SimpleType simple && !simple.Name.Contains("."))
            //    VisitSimpleType(simple);

            foreach(var type_arg in simpleType.TypeArguments)
                type_arg.AcceptWalker(this);
        }

        public void VisitPrimitiveType(PrimitiveType primitiveType)
        {
            // Can't use BindTypeName because primitiveType.IdentifierNode is always null Identifier
            //BindTypeName(primitiveType.IdentifierNode);
        }

        public void VisitReferenceType(ReferenceType referenceType)
        {
            // no op
        }

        public void VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptWalker(this);

            var original_type_table = symbol_table;
            var type_table = symbol_table.GetTypeTable(memberType.Target.Name);
            symbol_table = type_table;
            VisitIdentifier(memberType.IdentifierNode);
            symbol_table = original_type_table;
        }

        public void VisitFunctionType(FunctionType funcType)
        {
            funcType.Parameters.AcceptWalker(this);
            funcType.ReturnType.AcceptWalker(this);
        }

        public void VisitParameterType(ParameterType paramType)
        {
            // no op
        }

        public void VisitPlaceholderType(PlaceholderType placeholderType)
        {
            // no op
        }

        public void VisitKeyValueType(KeyValueType keyValueType)
        {
            // no op
        }

        public void VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public void VisitAttributeSection(AttributeSection section)
        {
            section.Attributes.AcceptWalker(this);
        }

        public void VisitAliasDeclaration(AliasDeclaration aliasDecl)
        {
            // An alias name is another name for an item
            // so resolve the target name first and then bind the name to the alias.
            aliasDecl.Path.AcceptWalker(this);
            aliasDecl.AliasToken.IdentifierId = aliasDecl.Path.Items.Last().IdentifierId;
        }

        public void VisitFieldDeclaration(FieldDeclaration fieldDecl)
        {
            if(fieldDecl.Modifiers.HasFlag(Modifiers.Public) && fieldDecl.Initializers.Any(init => !init.Name.StartsWith("_", StringComparison.CurrentCulture)
               && !init.Name.StartsWithCapitalLetter())){
                parser.ReportWarning(
                    "Public field names should start with a capital letter.",
                    "ES4202",
                    fieldDecl
                );
            }else if((fieldDecl.Modifiers.HasFlag(Modifiers.Protected) || fieldDecl.Modifiers.HasFlag(Modifiers.Private))
                     && fieldDecl.Initializers.Any(init => !init.Name.StartsWith("_", StringComparison.CurrentCulture) && init.Name.StartsWithCapitalLetter())){
                parser.ReportWarning(
                    "Non-public field names should start with a lower case letter.",
                    "ES4203",
                    fieldDecl
                );
            }

            // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
            fieldDecl.Attribute.AcceptWalker(this);
            fieldDecl.Initializers.AcceptWalker(this);
        }

        public void VisitParameterDeclaration(ParameterDeclaration parameterDecl)
        {
            // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
            parameterDecl.Attribute.AcceptWalker(this);
            UniqueIdGenerator.DefineNewId(parameterDecl.NameToken);
            parameterDecl.NameToken.Type.AcceptWalker(this);
        }

        public void VisitVariableInitializer(VariableInitializer initializer)
        {
            VisitPatternWithType(initializer.Pattern);
            initializer.Initializer.AcceptWalker(this);
        }

        public void VisitNullNode(AstNode nullNode)
        {
            // no op
        }

        public void VisitNewLine(NewLineNode newlineNode)
        {
            // no op
        }

        public void VisitWhitespace(WhitespaceNode whitespaceNode)
        {
            // no op
        }

        public void VisitExpressoTokenNode(ExpressoTokenNode tokenNode)
        {
            // no op
        }

        public void VisitPatternPlaceholder(AstNode placeholder, Pattern child)
        {
            // no op
        }

        public void VisitExpressionStatement(ExpressionStatement exprStmt)
        {
            exprStmt.Expression.AcceptWalker(this);
        }

        public void VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue)
        {
            keyValue.ValueExpression.AcceptWalker(this);
        }

        public void VisitPathExpression(PathExpression pathExpr)
        {
            // Side effect: Inside this method, the symbol_table field will be set to
            // the type table corresponding to the most descendant type the path represents.
            if(pathExpr.Items.Count == 1){
                VisitIdentifier(pathExpr.AsIdentifier);
            }else{
                var old_table = symbol_table;
                foreach(var ident in pathExpr.Items){
                    var tmp_table = symbol_table.GetTypeTable(ident.Name);
                    if(tmp_table == null){
                        VisitIdentifier(ident);
                    }else{
                        var type_symbol = symbol_table.GetTypeSymbolInAnyScope(ident.Name);
                        if(type_symbol == null){
                            parser.ReportSemanticError(
                                $"`{ident.Name}` in the path '{pathExpr}' does not represent a type or a module.",
                                "ES1500",
                                ident
                            );
                            return;
                        }
                        // MAYBE This part is not the bussiness of it. Move it to TypeChecker?
                        ident.IdentifierId = type_symbol.IdentifierId;
                        ident.Type = type_symbol.Type.Clone();
                        symbol_table = tmp_table;
                    }
                }

                symbol_table = old_table;
            }
        }

        public void VisitObjectCreationExpression(ObjectCreationExpression creation)
        {
            creation.TypePath.AcceptWalker(this);
            if(creation.TypeArguments.Any() && creation.TypeArguments.All(ta => ta.KeyType.Name == "")){
                var type_path = creation.TypePath;
                var table = (type_path is MemberType member) ? symbol_table.GetTypeTable(member.Target.Name) : symbol_table;
                var type_table = table.GetTypeTable(!type_path.IdentifierNode.Type.IsNull ? type_path.IdentifierNode.Type.Name : type_path.Name);

                foreach(var args in creation.TypeArguments.Zip((type_table ?? table).TypeParameters, (l, r) => new {TypeArgument = l, TypeParameter = r}))
                    args.TypeArgument.KeyType.IdentifierNode.ReplaceWith(args.TypeParameter.IdentifierNode.Clone());
            }

            foreach(var type_arg in creation.TypeArguments){
                if(type_arg.ValueType is SimpleType)
                    BindTypeName(type_arg.ValueType.IdentifierNode);
            }

            foreach(var keyvalue in creation.Items)
                keyvalue.ValueExpression.AcceptWalker(this);
        }

        public void VisitImportDeclaration(ImportDeclaration importDecl)
        {
        }

        public void VisitFunctionDeclaration(FunctionDeclaration funcDecl)
        {
            if(!funcDecl.Modifiers.HasFlag(Modifiers.Override) && !funcDecl.Name.StartsWith("_", StringComparison.CurrentCulture)
               && funcDecl.Name.StartsWithCapitalLetter()){
                parser.ReportWarning(
                    "Function and method names should start with a lower case letter.",
                    "ES4201",
                    funcDecl
                );
            }

            //UniqueIdGenerator.DefineNewId(funcDecl.NameToken);
            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
            funcDecl.Attribute.AcceptWalker(this);

            funcDecl.Parameters.AcceptWalker(this);
            funcDecl.ReturnType.AcceptWalker(this);
            VisitBlock(funcDecl.Body);

            // We can't write this as VisitPostModifiers(funcDecl.PostModifiers) because then we will visit null nodes
            if(funcDecl.Parent is TypeDeclaration type_decl && type_decl.TypeKind != ClassType.Interface){
                funcDecl.PostModifiers.AcceptWalker(this);
            }else if(funcDecl.Parent is ExpressoAst && (funcDecl.PostModifiers.RequiresToBeCalledBeforehand.Any() || funcDecl.PostModifiers.RequiresToBeCalledInside.Any())){
                throw new ParserException(
                    "Post modifiers must be only attached to methods.",
                    "ES4302",
                    funcDecl
                );
            }

            // This code is needed in order to enable recursive calls
            if(!funcDecl.Parameters.Any(p => ExpressoCompilerHelpers.IsPlaceholderType(p.ReturnType)) && !ExpressoCompilerHelpers.IsPlaceholderType(funcDecl.ReturnType)){
                var func_type = ExpressoCompilerHelpers.ConvertToFunctionType(funcDecl);
                funcDecl.NameToken.Type.ReplaceWith(func_type);
            }

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitTypeDeclaration(TypeDeclaration typeDecl)
        {
            if(!typeDecl.Name.StartsWith("_", StringComparison.CurrentCulture) && !typeDecl.Name.StartsWithCapitalLetter()){
                parser.ReportWarning(
                    "Type names should start with a capital letter.",
                    "ES4200",
                    typeDecl
                );
            }

            // We don't call VisitAttributeSection directly so that we can avoid unnecessary method calls
            typeDecl.Attribute.AcceptWalker(this);

            // Add Return type node
            var type_node = AstType.MakeSimpleType(typeDecl.Name);
            typeDecl.AddChild(type_node, Roles.Type);

            foreach(var base_type in typeDecl.BaseTypes)
                base_type.AcceptWalker(this);

            int tmp_counter = scope_counter;
            DecendScope();
            scope_counter = 0;

            typeDecl.TypeConstraints.AcceptWalker(this);

            // Add the self symbol to the scope of the type
            var self_ident = AstNode.MakeIdentifier("self");
            symbol_table.AddSymbol("self", self_ident);
            UniqueIdGenerator.DefineNewId(self_ident);

            // enums don't support inheritance
            if(typeDecl.TypeKind != ClassType.Enum){
                var (result, message, node) = symbol_table.SetParentClass();
                if(!result){
                    parser.ReportSemanticError(
                        message,
                        "ES0100",
                        node
                    );
                }

                symbol_table.CheckImplementedParentTypes();
            }

            typeDecl.Members.AcceptWalker(this);

            var value_symbol = symbol_table.GetSymbol(Utilities.RawValueEnumValueFieldName);
            if(typeDecl.TypeKind == ClassType.Enum && value_symbol != null)
                UniqueIdGenerator.DefineNewId(value_symbol);

            // We don't need constructors on enums
            if(typeDecl.TypeKind == ClassType.Class){
                // Add the constructor here so that we can use it before the type is inspected in TypeChecker
                var type_param_extractor = new TypeParameterExtractor();
                foreach(var fd in typeDecl.Members.OfType<FieldDeclaration>()){
                    foreach(var initializer in fd.Initializers)
                        initializer.Pattern.Type.AcceptTypeWalker(type_param_extractor);
                }

                var field_types = typeDecl.Members
                                          .OfType<FieldDeclaration>()
                                          .SelectMany(fd => fd.Initializers.Select(init => init.NameToken.Type.Clone()));
                var name = "constructor";
                var return_type = AstType.MakeSimpleType("tuple");
                var ctor_type = AstType.MakeFunctionType(name, return_type, field_types, type_param_extractor.ExtractedTypeParameters);
                symbol_table.AddSymbol(name, ctor_type);

                var ctor_symbol = symbol_table.GetSymbol(name, ctor_type);
                UniqueIdGenerator.DefineNewId(ctor_symbol);
            }

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitWildcardPattern(WildcardPattern wildcardPattern)
        {
            // no op
        }

        public void VisitIdentifierPattern(IdentifierPattern identifierPattern)
        {
            if(check_shadowing){
                var tmp_table = symbol_table.Parent;
                while(tmp_table.Name != "programRoot")
                    tmp_table = tmp_table.Parent;
                
                var symbol = tmp_table.GetSymbol(identifierPattern.Identifier.Name);
                if(symbol != null){
                    var shadowed_info = (symbol.Type is PlaceholderType) ? $"this_module.{symbol}" : symbol.ToString();
                    throw new ParserException(
                        $"Local bindings can not shadow module variables: '{identifierPattern.Identifier}' tries to shadow {shadowed_info}.",
                        "ES3100",
                        identifierPattern
                    );
                }
            }

            // variants will have been assigned IdentifierIds in pre definition phase
            if(identifierPattern.Identifier.IdentifierId == 0u)
                UniqueIdGenerator.DefineNewId(identifierPattern.Identifier);

            identifierPattern.Identifier.Type.AcceptWalker(this);
            identifierPattern.InnerPattern.AcceptWalker(this);
        }

        public void VisitCollectionPattern(CollectionPattern collectionPattern)
        {
            collectionPattern.Items.AcceptWalker(this);
        }

        public void VisitDestructuringPattern(DestructuringPattern destructuringPattern)
        {
            destructuringPattern.TypePath.AcceptWalker(this);
            destructuringPattern.Items.AcceptWalker(this);
        }

        public void VisitTuplePattern(TuplePattern tuplePattern)
        {
            tuplePattern.Patterns.AcceptWalker(this);
        }

        public void VisitExpressionPattern(ExpressionPattern exprPattern)
        {
            exprPattern.Expression.AcceptWalker(this);
        }

        public void VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
        {
            // no op
        }

        public void VisitKeyValuePattern(KeyValuePattern keyValuePattern)
        {
            keyValuePattern.Value.AcceptWalker(this);
        }

        public void VisitPatternWithType(PatternWithType pattern)
        {
            pattern.Pattern.AcceptWalker(this);
        }

        public void VisitTypePathPattern(TypePathPattern pathPattern)
        {
            pathPattern.TypePath.AcceptWalker(this);
        }

        public void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
        {
            VisitVariableInitializer(valueBindingPattern.Initializer);
        }
		#endregion

        void BindName(Identifier ident)
        {
            var referenced = symbol_table.GetSymbolInAnyScope(ident.Name);
            if(referenced != null){
                ident.IdentifierId = referenced.IdentifierId;
                ident.Modifiers = referenced.Modifiers;
            }

            if(ident.IdentifierId == 0){
                var native = SymbolTable.GetNativeSymbol(ident.Name);
                if(native == null){
                    // never reached
                    // because the self identifier must always be added
                    if(referenced != null){
                        parser.ReportSemanticError(
                            $"You cannot use '{ident.Name}' before it is declared!",
                            "ES0120",
                            ident
                        );
                    }else{
                        parser.ReportSemanticError(
                            $"The name '{ident.Name}' turns out not to be declared or accessible in the current scope {symbol_table.Name}!",
                            "ES0100",
                            ident
                        );
                    }
                }else{
                    ident.IdentifierId = native.IdentifierId;
                }
            }
        }

        void BindTypeName(Identifier ident)
        {
            var type_symbol = symbol_table.GetTypeSymbolInAnyScope(ident.Name);
            if(type_symbol != null){
                ident.IdentifierId = type_symbol.IdentifierId;
                if(!type_symbol.Type.IsNull)
                    ident.Type = type_symbol.Type.Clone();
            }

            if(ident.IdentifierId == 0){
                if(type_symbol != null){
                    // never reached
                    /*parser.ReportSemanticError(
                        $"You cannot use the type symbol '{ident.Name}' before defined!",
                        "ES0121",
                        ident
                    );*/
                }else{
                    parser.ReportSemanticError(
                        $"The type symbol '{ident.Name}' turns out not to be declared or accessible in the current scope {symbol_table.Name}!",
                        "ES0101",
                        ident
                    );
                }
            }
        }

        void BindNameOrTypeName(Identifier ident)
        {
            Identifier symbol = null;
            try{
                symbol = symbol_table.GetSymbolInAnyScopeThrowIfOutsideFunction(ident.Name);
            }
            catch(OutsideFunctionException){
                throw new ParserException(
                    "Missing `self`!",
                    "ES2300",
                    ident
                );
            }

            if(symbol != null){
                ident.IdentifierId = symbol.IdentifierId;
                ident.Modifiers = symbol.Modifiers;
            }

            if(ident.IdentifierId == 0){
                var native = SymbolTable.GetNativeSymbol(ident.Name);
                if(native != null)
                    ident.IdentifierId = native.IdentifierId;
            }

            var type_symbol = symbol_table.GetTypeSymbolInAnyScope(ident.Name);
            if(type_symbol != null)//{
                ident.IdentifierId = type_symbol.IdentifierId;
                /*if(ident.Type is PlaceholderType)
                    ident.Type = !type_symbol.Type.IsNull ? type_symbol.Type.Clone() : AstType.MakeSimpleType(type_symbol.Name);
            }*/

            if(ident.IdentifierId == 0){
                if(symbol != null || type_symbol != null){
                    parser.ReportSemanticError(
                        $"You cannot use '{ident.Name}' before it is defined or declared!",
                        "ES0120",
                        ident
                    );
                }else{
                    parser.ReportSemanticError(
                        $"The symbol '{ident.Name}' turns out not to be declared or accessible in the current scope {symbol_table.Name}!",
                        "ES0102",
                        ident
                    );
                }
            }
        }

        void PreDefineTypeAndFunctionIds(ExpressoAst ast)
        {
            // This code is needed in order to create types that point at each other
            // members will be identified in TypeChecker so IdentifierIds will have already been assigned
            foreach(var entity in ast.Declarations.Where(d => d is TypeDeclaration || d is FunctionDeclaration)){
                UniqueIdGenerator.DefineNewId(entity.NameToken);

                if(entity is TypeDeclaration type_decl){
                    // This code is needed in order to enable Use guide
                    foreach(var func_decl in type_decl.Members.OfType<FunctionDeclaration>())
                        UniqueIdGenerator.DefineNewId(func_decl.NameToken);

                    // In order to enable that enums point at each other
                    if(type_decl.TypeKind == ClassType.Enum){
                        foreach(var variant_decl in type_decl.Members.OfType<FieldDeclaration>()){
                            foreach(var initializer in variant_decl.Initializers){
                                if(initializer.Pattern.Pattern is IdentifierPattern ident_pat)
                                    UniqueIdGenerator.DefineNewId(ident_pat.Identifier);
                            }
                        }
                    }
                }
            }
        }

        // FIXME: Can be deleted: 2019/8/8
        /*void ConvertPostModifiers(ExpressoAst ast)
        {
            foreach(var type_decl in ast.Declarations.OfType<TypeDeclaration>()){
                var type_table = symbol_table.GetTypeTable(type_decl.Name);
                foreach(var func_decl in type_decl.Members.OfType<FunctionDeclaration>()){
                    foreach(var afterwards in func_decl.PostModifiers.RequiresToBeCalledAfterwards){
                        var target_func_decl = type_decl.Members.OfType<FunctionDeclaration>().FirstOrDefault(m => m.Name == afterwards.Name);
                        if(target_func_decl == null){
                            throw new ParserException(
                                $"The name '{afterwards.Name}' turns out not to be declared or accessible in the current scope {symbol_table.Name}!",
                                "ES0100",
                                afterwards
                            );
                        }

                        SetNewPostModifiers(target_func_decl, afterwards.Clone());

                        var target_symbol = type_table.GetSymbol(afterwards.Name);
                        SetNewPostModifiers(target_symbol, afterwards.Clone());
                    }

                    func_decl.PostModifiers.RequiresToBeCalledAfterwards.Clear();

                    // This code is needed because they won't be linked
                    var symbol = type_table.GetSymbol(func_decl.Name);
                    symbol.PostModifiers.RequiresToBeCalledAfterwards.Clear();
                }
            }
        }

        void SetNewPostModifiers(AstNode node, Identifier ident)
        {
            if(node is FunctionDeclaration func_decl && func_decl.PostModifiers.IsNull){
                var new_post_modifiers = AstNode.MakePostModifiers(
                    null, new List<Identifier>{ident}, null, func_decl.PostModifiers.StartLocation, func_decl.PostModifiers.EndLocation
                );
                func_decl.PostModifiers = new_post_modifiers;
            }else if(node is Identifier ident2 && ident2.PostModifiers.IsNull){
                var new_post_modifiers = AstNode.MakePostModifiers(
                    null, new List<Identifier>{ident}, null, ident2.PostModifiers.StartLocation, ident2.PostModifiers.EndLocation
                );
                ident2.PostModifiers = new_post_modifiers;
            }else{
                if(node is Identifier ident3)
                    ident3.PostModifiers.RequiresToBeCalledBeforehand.Add(ident);
                else if(node is FunctionDeclaration func_decl2)
                    func_decl2.PostModifiers.RequiresToBeCalledBeforehand.Add(ident);
            }

        }*/
    }
}


