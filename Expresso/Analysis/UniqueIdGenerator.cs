﻿

using Expresso.CodeGen;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// A UniqueIdGenerator generates a unique id throughout the whole program at a time.
    /// </summary>
    public static class UniqueIdGenerator
    {
        static uint Id = ExpressoCompilerHelpers.EndOfPrimitiveTypeIdentifierIds;

        /// <summary>
        /// Peeks at the next unique id.
        /// </summary>
        public static uint CurrentId => Id;

        /// <summary>
        /// Assigns the next id to the given identifier.
        /// </summary>
        public static void DefineNewId(Identifier ident)
        {
            if(ident.IdentifierId != 0u)
                return;
            
            ident.IdentifierId = Id++;
        }
    }
}

