﻿using System;
namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// Represents using symbols outside the current function.
    /// </summary>
    public class OutsideFunctionException : Exception
    {
        public OutsideFunctionException() : base("outside function")
        {
        }
    }
}
