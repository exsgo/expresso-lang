﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using Expresso.CodeGen;
using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.PatternMatching;
using Microsoft.Collections.Extensions;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// Represents the symbol table. The symbol table is represented by a n-branched tree
    /// with each node representing one scope(one SymbolTable instance).
    /// In Expresso, there are 2 kinds of namespaces.
    /// One is for types, and the other is for local variables, parameters, function names and method names.
    /// </summary>
#if LIBRARY
    public class SymbolTable : ISerializable, IGenericCloneable<SymbolTable>
#else
    public class SymbolTable : ISerializable, ICloneable<SymbolTable>
#endif
    {
        static readonly Dictionary<string, Identifier> NativeMapping;
        static readonly string TypeTablePrefix = "type_";
#if LIBRARY
        public static readonly SymbolTable PrimitiveTypeSymbolTables = new SymbolTable{Name = "PrimitiveTypes"};
#else
        internal static readonly SymbolTable PrimitiveTypeSymbolTables = new SymbolTable{Name = "PrimitiveTypes"};
#endif

        readonly Dictionary<string, Identifier> type_table;
        readonly MultiValueDictionary<string, Identifier> table;

        /// <summary>
        /// The name for this symbol scope.
        /// </summary>
        public string Name{
            get; set;
        }

        /// <summary>
        /// The parent symbol scope.
        /// </summary>
        public SymbolTable Parent{
            get; set;
        }

        /// <summary>
        /// Represents the parent class.
        /// </summary>
        /// <value>The type of the parent.</value>
        public SymbolTable ParentClass{
            get; internal set;
        }

        /// <summary>
        /// The child symbol scopes.
        /// Even though it is defined as an array, it acts most of the times as if it were defined as
        /// bi-directional linked list.
        /// </summary>
        public List<SymbolTable> Children{
            get; set;
        }

        /// <summary>
        /// Gets or sets the type constraints that the type has.
        /// </summary>
        /// <value>The type constraints.</value>
        public List<TypeConstraint> TypeConstraints{
            get; set;
        }

        /// <summary>
        /// Gets all the symbols as an enumerator.
        /// </summary>
        public IEnumerable<Identifier> Symbols => table.Values.SelectMany(col => col.Select(ident => ident));

        /// <summary>
        /// Gets the number of symbols within this scope.
        /// </summary>
        public int NumberOfSymbols => Symbols.Count();

        /// <summary>
        /// Gets all the symbols defined in scopes rooting at this table.
        /// </summary>
        /// <value>All symbols.</value>
        public IEnumerable<Identifier> AllSymbols{
            get {
                foreach(var symbol in Symbols)
                    yield return symbol;

                foreach(var child in Children){
                    foreach(var symbol in child.AllSymbols)
                        yield return symbol;
                }
            }
        }

        /// <summary>
        /// Represents the kind of the type.
        /// </summary>
        /// <value>The kind of the type.</value>
        public ClassType TypeKind{
            get;
        }

        /// <summary>
        /// Gets the value indicating whether the symbol table represents a type in foreign codes.
        /// </summary>
        /// <value><c>true</c> if it is a foreign type; otherwise, <c>false</c>.</value>
        public bool IsForeignType{
            get;
        }

        /// <summary>
        /// Represents the type parameters for this type.
        /// </summary>
        /// <value>The type parameters.</value>
        public List<ParameterType> TypeParameters{
            get; private set;
        }

        // We can't make it SimpleType because there is a chance that we can parse it as PrimitiveType
        /// <summary>
        /// Represents the interfaces and classes that the type this symbol table represents derives.
        /// </summary>
        /// <value>The implemented interfaces.</value>
        public List<AstType> ParentTypes{
            get; internal set;
        }
        
        static SymbolTable()
        {
            NativeMapping = new Dictionary<string, Identifier>();
            var print_ident = AstNode.MakeIdentifier(
                "Write",
                AstType.MakeFunctionType(
                    "print",
                    AstType.MakeSimpleType("tuple", TextLocation.Empty),    //The void type
                    TextLocation.Empty,
                    TextLocation.Empty,
                    null,
                    AstType.MakeFunctionParameterType(AstType.MakePrimitiveType("string"), false, false),
                    AstType.MakeFunctionParameterType(
                        AstType.MakeSimpleType(
                            "array",
                            TextLocation.Empty,
                            TextLocation.Empty,
                            AstType.MakeSimpleType("object")
                        ),
                        false,
                        true
                    )
                )
            );
            print_ident.IdentifierId = ExpressoCompilerHelpers.StartOfIdentifierId;
            NativeMapping.Add("print", print_ident);

            var println_ident = AstNode.MakeIdentifier(
                "WriteLine",
                AstType.MakeFunctionType(
                    "println",
                    AstType.MakeSimpleType("tuple", TextLocation.Empty),    //The void type
                    TextLocation.Empty,
                    TextLocation.Empty,
                    null,
                    AstType.MakeFunctionParameterType(AstType.MakePrimitiveType("string"), false, false),
                    AstType.MakeFunctionParameterType(
                        AstType.MakeSimpleType(
                            "array",
                            TextLocation.Empty,
                            TextLocation.Empty,
                            AstType.MakeSimpleType("object")
                        ),
                        false,
                        true
                    )
                )
            );
            println_ident.IdentifierId = ExpressoCompilerHelpers.StartOfIdentifierId + 1u;
            NativeMapping.Add("println", println_ident);

            ExpressoCompilerHelpers.InitializePrimitiveTypeSymbolTables(PrimitiveTypeSymbolTables);
        }

        public SymbolTable(ClassType typeKind = ClassType.NotType, bool isForeignType = false)
        {
            type_table = new Dictionary<string, Identifier>();
            table = new MultiValueDictionary<string, Identifier>();
            Children = new List<SymbolTable>();
            TypeKind = typeKind;
            IsForeignType = isForeignType;
        }

        /// <summary>
        /// Creates a new instance of the SymbolTable class.
        /// </summary>
        /// <returns>The create.</returns>
        public static SymbolTable Create()
        {
            var table = new SymbolTable();
            table.Name = "programRoot";
            var table2 = new SymbolTable();
            table2.Name = "root";
            table.Parent = table2;
            table2.Children.Add(table);

            ExpressoCompilerHelpers.RegisterPrimitiveTypesAndTheirBaseTypeTables(table2);
            return table;
        }

        public static Identifier GetNativeSymbol(string name)
        {
            if(NativeMapping.TryGetValue(name, out var result))
                return result;
            else
                return null;
        }

        #region ISerializable implementation

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach(var entry in type_table){
                var ast_type = entry.Value.Type;
                info.AddValue(entry.Key, ast_type, ast_type.GetType());
            }

            foreach(var entry2 in table){
                foreach(var list_value in entry2.Value){
                    var ast_type = list_value.Type;
                    info.AddValue(entry2.Key, ast_type, ast_type.GetType());
                }
            }
        }

        #endregion

        /// <summary>
        /// Adds the table as a child to this table.
        /// </summary>
        /// <param name="childTable">The table to be added.</param>
        public void AddTypeTable(SymbolTable childTable)
        {
            childTable.Parent = this;
            Children.Add(childTable);
        }

        /// <summary>
        /// Adds the tables as children to this table.
        /// Note that it doesn't clone the children.
        /// </summary>
        /// <param name="childTables"></param>
        public void AddChildTables(IEnumerable<SymbolTable> childTables)
        {
            foreach(var child_table in childTables)
                AddTypeTable(child_table);
        }

        /// <summary>
        /// Gets the type table named `name` in any parent scopes.
        /// </summary>
        /// <returns>The type table.</returns>
        /// <param name="name">Name.</param>
        public SymbolTable GetTypeTable(string name)
        {
            if(name == null)
                throw new ArgumentNullException(nameof(name));
            
            var parent = this;
            while(parent != null && parent.Children.Count == 0)
                parent = parent.Parent;

            if(parent == null)
                return null;
            
            var tmp = parent.Children[0];
            var type_name = TypeTablePrefix + name;
            var regex = new Regex($@"{type_name.Replace(".", "\\.")}(?![:\.\w]+)");
            int child_counter = 1;
            while(tmp != null){
                if(regex.IsMatch(tmp.Name))
                    return tmp;

                if(child_counter >= parent.Children.Count){
                    child_counter = 1;
                    do{
                        parent = parent.Parent;
                    }while(parent != null && parent.Children.Count == 0);
                    if(parent == null)
                        break;
                    
                    tmp = parent.Children[0];
                }else{
                    tmp = parent.Children[child_counter++];
                }
            }

            return null;
        }

        /// <summary>
        /// Determines whether `name` is a type symbol.
        /// </summary>
        /// <returns><c>true</c> if `name` is a type name; otherwise, <c>false</c>.</returns>
        /// <param name="name">The name to test.</param>
        public bool HasTypeSymbol(string name)
        {
            return type_table.ContainsKey(name);
        }

        public bool HasTypeSymbolInAnyScope(string name)
        {
            if(type_table.ContainsKey(name))
                return true;
            else if(Parent == null)
                return false;
            else
                return Parent.HasTypeSymbolInAnyScope(name);
        }

        /// <summary>
        /// Determines whether `name` is a symbol name(variables or functions).
        /// </summary>
        /// <returns><c>true</c> if `name` is a symbol name; otherwise, <c>false</c>.</returns>
        /// <param name="name">The name to test.</param>
        public bool HasSymbol(string name)
        {
            return table.ContainsKey(name);
        }

        /// <summary>
        /// Determines whether `name` is a symbol name in any parent scopes.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public (bool, bool) HasSymbolInAnyScope(string name)
        {
            if(table.ContainsKey(name))
                return (true, Name.Contains("programRoot"));
            else if(Parent == null)
                return (false, false);
            else
                return Parent.HasSymbolInAnyScope(name);
        }

        /// <summary>
        /// Determines whether `name` represents any type symbol name in the current and all the parent scopes.
        /// </summary>
        /// <returns><c>true</c>, if type symbol was ised, <c>false</c> otherwise.</returns>
        /// <param name="name">Name.</param>
        public bool IsTypeSymbol(string name)
        {
            if(HasTypeSymbol(name))
                return true;
            else if(Parent != null)
                return Parent.IsTypeSymbol(name);
            else
                return false;
        }

        /// <summary>
        /// Adds a new name to the type namespace.
        /// Note that you must also specify the corresponding type that the name represents.
        /// At the first glance, `type` is unnecessary because type names almost always represent themselves
        /// but consider the situations where alias names are declared.
        /// The symbol table is responsible for managing ALL the names declared in scopes,
        /// so we must know which name represents which type in a certain scope.
        /// </summary>
        /// <param name="name">The symbol name to be added. It can be an alias name.</param>
        /// <param name="type">The type that corresponds to the name in the current or child scopes.</param>
        public void AddTypeSymbol(string name, AstType type, Modifiers modifiers = Modifiers.None)
        {
            AddTypeSymbol(name, AstNode.MakeIdentifier(name, type, modifiers));
        }

        /// <summary>
        /// Adds a new name to the type namespace.
        /// Use this overload when encountered with a new type declaration.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="ident">Ident.</param>
        public void AddTypeSymbol(string name, Identifier ident)
        {
            try{
                type_table.Add(name, ident);
            }
            catch(ArgumentException){
                throw new ParserException(
                    $"The name '{name}' is already defined in the type namespace in the current scope '{Name}'.",
                    "ES3014",
                    ident
                );
            }
        }

        /// <summary>
        /// Adds a new name to the symbol scope.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="type">Type.</param>
        public void AddSymbol(string name, AstType type)
        {
            AddSymbol(name, AstNode.MakeIdentifier(name, type), false);
        }

        /// <summary>
        /// Adds a new name with a certain identifier.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="ident">Ident.</param>
        public void AddSymbol(string name, Identifier ident, bool userDefinedSymbol = true)
        {
            // userDefinedSymbol is intended to be used in another overload of AddSymbol
            if(userDefinedSymbol && (Name.StartsWith("block", StringComparison.CurrentCulture) || Name.StartsWith("type", StringComparison.CurrentCulture))
               && table.ContainsKey(name)){
                // ES0160 thrown
                throw new ParserException(
                    $"The name '{name}' is already defined in the current scope {Name}.",
                    "ES3015",
                    ident
                );
            }

            table.Add(name, ident);
        }

        /// <summary>
        /// Gets the corresponding <see cref="Expresso.Ast.Identifier"/> node to type `name`.
        /// </summary>
        public Identifier GetTypeSymbol(string name)
        {
            if(name == null)
                throw new ArgumentNullException(nameof(name));

            if(!type_table.TryGetValue(name, out var result))
                return null;

            return result;
        }

        public Identifier GetTypeSymbolInAnyScope(string name)
        {
            var symbol = GetTypeSymbol(name);
            if(symbol == null){
                if(Parent != null)
                    return Parent.GetTypeSymbolInAnyScope(name);
                else
                    return null;
            }

            return symbol;
        }

        /// <summary>
        /// Gets the corresponding <see cref="Expresso.Ast.Identifier"/> node to `name`.
        /// </summary>
        public Identifier GetSymbol(string name)
        {
            if(!table.TryGetValue(name, out var results))
                return null;

            if(results.Count > 1)
                throw new InvalidOperationException($"There are more than 1 candidate for '{name}'");
            
            return results.FirstOrDefault();
        }

        /// <summary>
        /// Gets the corresponding <see cref="Identifier"/> node to `name` that also matches to the `type`.
        /// </summary>
        /// <returns>The symbol.</returns>
        /// <param name="name">Name.</param>
        /// <param name="type">Type.</param>
        public Identifier GetSymbol(string name, AstType type)
        {
            if(!table.TryGetValue(name, out var results))
                return null;

            return results.FirstOrDefault(ident => ident.Type.IsMatch(type));
        }

        /// <summary>
        /// Gets all the symbols corresponding to `name`.
        /// </summary>
        /// <returns>The symbols.</returns>
        /// <param name="name">Name.</param>
        public IReadOnlyCollection<Identifier> GetSymbols(string name)
        {
            if(!table.TryGetValue(name, out var results))
                return null;
            else
                return results;
        }

        /// <summary>
        /// Gets all the symbols corresponding to `name` in any parent scopes.
        /// </summary>
        /// <returns>The symbols in any scope.</returns>
        /// <param name="name">Name.</param>
        public IReadOnlyCollection<Identifier> GetSymbolsInTypeChain(string name)
        {
            if(!table.TryGetValue(name, out var results)){
                if(ParentTypes != null){
                    foreach(var base_type_table in ParentTypes.Select(pt => GetTypeTable(!pt.IdentifierNode.Type.IsNull ? pt.IdentifierNode.Type.Name : pt.Name))){
                        if(base_type_table == null)
                            throw new InvalidOperationException("table is missing");

                        var base_results = base_type_table.GetSymbolsInTypeChain(name);
                        if(base_results != null)
                            return base_results;
                    }
                }

                return null;
            }else{
                return results;
            }
        }

        /// <summary>
        /// Gets the corresponding <see cref="Expresso.Ast.Identifier"/> node to `name`, trying to track up, at most, n scopes.
        /// </summary>
        /// <returns>The symbol</returns>
        /// <param name="name">The symbol name to be fetched.</param>
        /// <param name="limit">The maximum count to track up scopes, while trying to find the symbol.</param>
        public Identifier GetSymbolInNScopesAbove(string name, int limit)
        {
            if(!table.TryGetValue(name, out var results)){
                if(Parent != null && limit > 0){
                    return Parent.GetSymbolInNScopesAbove(name, limit - 1);
                }else{
                    if(limit > 0 && NativeMapping.TryGetValue(name, out var result))
                        return result;
                    else
                        return null;
                }
            }

            if(results.Count > 1)
                throw new InvalidOperationException($"There are more than 1 candidate for '{name}'");

            return results.FirstOrDefault();
        }

        /// <summary>
        /// Gets the corresponding <see cref="Identifier"/> node to `name` in any parent scopes.
        /// </summary>
        public Identifier GetSymbolInAnyScope(string name)
        {
            if(!table.TryGetValue(name, out var results)){
                if(Parent != null){
                    return Parent.GetSymbolInAnyScope(name);
                }else{
                    if(NativeMapping.TryGetValue(name, out var result))
                        return result;
                    else
                        return null;
                }
            }

            if(results.Count > 1)
                throw new InvalidOperationException($"There are more than 1 candidate for '{name}'");

            return results.FirstOrDefault();
        }

        /// <summary>
        /// Gets corresponding <see cref="Identifier"/> node to `name` in any parent scopes except for those outside any function.
        /// </summary>
        /// <returns>The symbol in any scope throw if outside function.</returns>
        /// <param name="name">Name.</param>
        /// <param name="outsideFunction">If set to <c>true</c> outside function.</param>
        public Identifier GetSymbolInAnyScopeThrowIfOutsideFunction(string name, bool outsideFunction = false)
        {
            if(!table.TryGetValue(name, out var results)){
                if(Parent != null){
                    var outside_function = outsideFunction || Name.Contains("func");
                    return Parent.GetSymbolInAnyScopeThrowIfOutsideFunction(name, outside_function);
                }else{
                    if(NativeMapping.TryGetValue(name, out var result))
                        return result;
                    else
                        return null;
                }
            }

            if(results.Count > 1)
                throw new InvalidOperationException($"There are more than 1 candidate for '{name}'");
            else if(outsideFunction && Name.Contains("type"))
                throw new OutsideFunctionException();

            return results.FirstOrDefault();
        }

        /// <summary>
        /// Searches for a symbol with the name in this type and any parent types.
        /// </summary>
        /// <returns>The symbol in type chain.</returns>
        /// <param name="name">Name.</param>
        public Identifier GetSymbolInTypeChain(string name)
        {
            if(!table.TryGetValue(name, out var results)){
                if(ParentTypes != null){
                    var tables = ParentTypes.Select(pt => GetTypeTable(!pt.IdentifierNode.Type.IsNull ? pt.IdentifierNode.Type.Name : pt.Name));
                    foreach(var base_type_table in ParentTypes.Select(pt => GetTypeTable(!pt.IdentifierNode.Type.IsNull ? pt.IdentifierNode.Type.Name : pt.Name))){
                        if(base_type_table == null)
                            throw new InvalidOperationException("table is missing");

                        var base_result = base_type_table.GetSymbolInTypeChain(name);
                        if(base_result != null)
                            return base_result;
                    }
                }

                return null;
            }

            if(results.Count > 1)
                throw new InvalidOperationException($"There are more than 1 candidate for '{name}'");

            return results.FirstOrDefault();
        }

        /// <summary>
        /// Gets the corresponding <see cref="Identifier"/> node to `name` in parent scopes that also matches to `type`.
        /// </summary>
        /// <returns>The symbol in any scope.</returns>
        /// <param name="name">Name.</param>
        /// <param name="type">Type.</param>
        public Identifier GetSymbolInAnyScope(string name, AstType type)
        {
            if(!table.TryGetValue(name, out var results)){
                if(Parent != null){
                    return Parent.GetSymbolInAnyScope(name, type);
                }else{
                    if(NativeMapping.TryGetValue(name, out var result))
                        return result;
                    else
                        return null;
                }
            }

            return results.FirstOrDefault(ident => ident.Type.IsMatch(type));
        }

        /// <summary>
        /// Gets the corresponding <see cref="Expresso.Ast.Identifier"/> node to `name` except for the natives in any parent scopes.
        /// </summary>
        /// <returns>The symbol in any scope without native.</returns>
        /// <param name="name">Name.</param>
        public Identifier GetSymbolInAnyScopeWithoutNative(string name, out bool nativeSearched)
        {
            nativeSearched = false;
            if(!table.TryGetValue(name, out var results)){
                if(Parent != null){
                    return Parent.GetSymbolInAnyScopeWithoutNative(name, out nativeSearched);
                }else{
                    nativeSearched = true;
                    return null;
                }
            }

            if(results.Count > 1)
                throw new InvalidOperationException($"There are more than 1 candidate for '{name}'");

            return results.FirstOrDefault();
        }

        public void AddScope(ClassType typeKind = ClassType.NotType)
        {
            var child = new SymbolTable(typeKind){
                Parent = this
            };
            Children.Add(child);
        }

        /// <summary>
        /// Gets a func child table with the name.
        /// </summary>
        /// <returns>The child table with the func name.</returns>
        /// <param name="funcName">Func name.</param>
        public SymbolTable GetChildTableWithFuncName(string funcName)
        {
            var name = "func_" + funcName;
            return Children.First(c => c.Name.StartsWith(name, StringComparison.CurrentCulture));
        }

        /// <summary>
        /// Adds external symbols as a new scope.
        /// It will put functions and variables under the external symbol table
        /// and will put external types on the 'root' table, modifying the name.
        /// </summary>
        /// <param name="externalTable">External table.</param>
        /// <param name="aliasName">Alias name.</param>
        public void AddExternalSymbols(SymbolTable externalTable, string aliasName)
        {
            Debug.Assert(Name == "programRoot", "External symbols must be added on 'programRoot'.");

            var cloned = externalTable.Clone();
            var imported_name = TypeTablePrefix + aliasName;
            cloned.Name = imported_name;

            foreach(var symbol in cloned.Symbols){
                if(symbol.Type is FunctionType func_type){
                    var return_type = func_type.ReturnType;
                    var return_type_table = externalTable.GetTypeTable(return_type.IdentifierNode.Type.IsNull ? return_type.Name : return_type.IdentifierNode.Type.Name);
                    if(!return_type_table.IsForeignType && externalTable.GetTypeSymbol(return_type.Name) != null)
                        return_type.IdentifierNode.Type = AstType.MakeMemberType(AstType.MakeSimpleType(aliasName), AstType.MakeSimpleType(return_type.Name));
                }
            }

            foreach(var external_type_table in externalTable.Children){
                if(external_type_table.TypeKind != ClassType.Class && external_type_table.TypeKind != ClassType.Interface)
                    continue;
                
                var cloned2 = external_type_table.Clone();
                cloned2.Name = TypeTablePrefix + aliasName + "::" + external_type_table.Name.Substring(TypeTablePrefix.Length);
                cloned2.Parent = this;
                Children.Add(cloned2);
            }

            var tmp = this;
            while(tmp.Parent != null)
                tmp = tmp.Parent;

            cloned.Parent = tmp;
            tmp.Children.Add(cloned);
        }

        /// <summary>
        /// Adds external tables and symbols with certain paths to this table with alias names.
        /// It will put external symbols on 'programRoot'.
        /// </summary>
        /// <param name="externalTable">External table.</param>
        /// <param name="importPaths">Import paths.</param>
        /// <param name="aliasTokens">Alias tokens.</param>
        public void AddExternalSymbols(SymbolTable externalTable, AstNodeCollection<SimpleType> importPaths, AstNodeCollection<Identifier> aliasTokens)
        {
            Debug.Assert(Name == "programRoot", "External tables must be added on 'programRoot'.");

            var root_table = this;
            while(root_table.Parent != null)
                root_table = root_table.Parent;

            var first_path = importPaths.First();
            if(first_path.Name.Contains("*")){
                var import_path_list = new List<SimpleType>();
                var alias_list = new List<Identifier>();
                var path_location = importPaths.First().StartLocation;
                var alias_location = aliasTokens.First().StartLocation;

                foreach(var child in externalTable.Children){
                    var index = child.Name.IndexOf('`');
                    var full_type_name = (index != -1) ? child.Name.Substring(ExpressoCompilerHelpers.TypePrefix.Length, index - 1)
                                                       : child.Name.Substring(ExpressoCompilerHelpers.TypePrefix.Length);

                    var last_dot_index = full_type_name.LastIndexOf('.');
                    var namespace_name = (last_dot_index == -1) ? full_type_name : full_type_name.Substring(0, last_dot_index);
                    if(!first_path.Name.StartsWith(namespace_name, StringComparison.CurrentCulture))
                        continue;

                    var type_name = full_type_name.Substring(last_dot_index + 1);
                    var type_symbol = externalTable.GetTypeSymbol(full_type_name);
                    if(type_symbol == null)
                        throw new InvalidOperationException($"An external type symbol '{full_type_name}' is not found. Something wrong has occurred!");
                    else if(root_table.IsTypeSymbol(type_name) || !type_symbol.Modifiers.HasFlag(Modifiers.Export))
                        continue;
                    
                    var cloned = child.Clone();
                    root_table.AddTypeTable(cloned);
                    root_table.AddTypeSymbol(type_name, type_symbol);

                    var type_params = child.TypeParameters?.Select(_ => AstType.MakePlaceholderType());
                    var path_type = AstType.MakeSimpleType(full_type_name, type_params, path_location);
                    path_type.IdentifierNode.IdentifierId = type_symbol.IdentifierId;
                    import_path_list.Add(path_type);

                    var alias_ident = AstNode.MakeIdentifier(type_name, Modifiers.None, alias_location);
                    alias_ident.IdentifierId = type_symbol.IdentifierId;
                    alias_list.Add(alias_ident);
                }

                importPaths.ReplaceWith(import_path_list);
                aliasTokens.ReplaceWith(alias_list);
            }else{
                foreach(var pair in importPaths.Zip(aliasTokens, (l, r) => new {Path = l, Alias = r})){
                    var last_index = pair.Path.Name.LastIndexOf("::", StringComparison.CurrentCulture);
                    var target_name = (last_index == -1) ? pair.Path.Name : pair.Path.Name.Substring(last_index + "::".Length);
                    var external_target_table = externalTable.GetTypeTable(target_name);

                    if(external_target_table == null){
                        var target_name2 = target_name.Substring(target_name.LastIndexOf(".", StringComparison.CurrentCulture) + ".".Length);
                        var symbol = externalTable.GetSymbol(target_name2);
                        if(symbol == null){
                            throw new ParserException(
                                $"An external symbol '{pair.Path}'(searching with '{target_name2}') is not found.",
                                "ES0103",
                                pair.Path
                            ){
                                HelpObject = target_name.StartsWith("System", StringComparison.CurrentCulture)
                            };
                        }else{
                            if(!symbol.Modifiers.HasFlag(Modifiers.Export))
                                ReportExportMissingError(pair.Path.IdentifierToken);

                            pair.Alias.IdentifierId = symbol.IdentifierId;
                            root_table.AddSymbol(pair.Alias.Name, symbol);
                        }
                    }else{
                        var type_symbol = externalTable.GetTypeSymbol(target_name);
                        if(!type_symbol.Modifiers.HasFlag(Modifiers.Export))
                            ReportExportMissingError(pair.Path.IdentifierToken);
                    
                        var cloned = external_target_table.Clone();
                        root_table.AddTypeTable(cloned);
                        root_table.AddTypeSymbol(pair.Alias.Name, type_symbol);

                        pair.Path.IdentifierToken.IdentifierId = type_symbol.IdentifierId;
                        pair.Alias.IdentifierId = type_symbol.IdentifierId;
                    }
                }
            }
        }

        /// <summary>
        /// Adds a native symbol's table.
        /// </summary>
        /// <param name="symbol">Symbol.</param>
        public void AddNativeSymbolTable(Identifier symbol)
        {
            if(Parent.Name != "root")
                throw new InvalidOperationException($"Expected to call this method on programRoot but you called it on `{Name}`");
            
            ExpressoCompilerHelpers.AddNativeSymbolTable(symbol, Parent);
        }

        /// <summary>
        /// Adds type parameters.
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        internal void AddTypeParameters(IEnumerable<ParameterType> parameters)
        {
            TypeParameters = new List<ParameterType>();
            TypeParameters.AddRange(parameters);
        }

        internal (bool, string, AstNode) SetParentClass()
        {
            var parent_type = ParentTypes.LastOrDefault();
            if(parent_type == null){
                if(TypeKind == ClassType.Class){
                    var object_type_table = GetTypeTable("object");
                    ParentClass = object_type_table;
                    ParentTypes.Add(AstType.MakeSimpleType(AstNode.MakeIdentifier("object")));
                }

                return (true, null, null);
            }

            var parent_type_name = parent_type is SimpleType simple && !simple.IdentifierNode.Type.IsNull ? simple.IdentifierNode.Type.Name : parent_type.Name;
            var parent_type_table = GetTypeTable(parent_type_name);
            if(parent_type_table == null){
                // ES0101 can be thrown
                return (false, $"'{parent_type.Name}' turns out not to be declared or accessible in the current scope {Name}!", parent_type);
            }

            if(parent_type_table.TypeKind == ClassType.Class){
                ParentClass = parent_type_table;
            }else{
                if(TypeKind == ClassType.Class){
                    var object_type_table = GetTypeTable("object");
                    ParentClass = object_type_table;
                    ParentTypes.Add(AstType.MakeSimpleType(AstNode.MakeIdentifier("object")));
                }
            }

            return (true, null, null);
        }

        internal void CheckImplementedParentTypes()
        {
            foreach(var parental_ast_type in ParentTypes){
                var name = !parental_ast_type.IdentifierNode.Type.IsNull ? parental_ast_type.IdentifierNode.Type.Name : parental_ast_type.Name;
                var parental_type_table = GetTypeTable(name);
                if(parental_type_table == null){
                    return;
                    // Don't report parent type missing error here because SetParent has already done that
                    /*throw new ParserException(
                        $"'{parent_ast_type.Name}' turns out not to be declared or accessible in the current scope {Name}!",
                        "ES0100",
                        parent_ast_type
                    );*/
                }

                if(parental_type_table.TypeKind == ClassType.Class){
                    if(TypeKind == ClassType.Interface){
                        throw new ParserException(
                            $"Interfaces must only implement interfaces. `{parental_ast_type.Name}` is a class.",
                            "ES1010",
                            parental_ast_type
                        );
                    }

                    if(ParentClass != null && !ParentClass.Name.Contains("object") && ParentClass != parental_type_table){
                        throw new ParserException(
                            "You can not set more than 1 parent class.",
                            "ES0130",
                            parental_ast_type
                        ){
                            HelpObject = parental_ast_type.Name
                        };
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether we can upcast from the type that this type table represents to <paramref name="toType"/>.
        /// </summary>
        /// <returns><c>true</c>, if upcast can be, <c>false</c> otherwise.</returns>
        /// <param name="toType">To type.</param>
        public bool CanUpcast(SimpleType toType)
        {
            if(ParentTypes == null && TypeKind == ClassType.Class)
                throw new InvalidOperationException($"{Name} is a class, but ParentTypes is null.");

            if(ParentTypes == null)
                return false;

            foreach(var parent_type in ParentTypes){
                var name = parent_type.Name;
                if(name.EndsWith(name.Contains(".") ? $".{toType.Name}" : toType.Name, StringComparison.CurrentCulture)){
                    return true;
                }else{
                    var parent_type_name = !parent_type.IdentifierNode.Type.IsNull ? parent_type.IdentifierNode.Type.Name : parent_type.Name;
                    var parent_type_table = GetTypeTable(ExpressoCompilerHelpers.GetExpressoTypeName(parent_type_name));
                    if(parent_type_table == null){
                        // ES0101 thrown
                        throw new ParserException(
                            $"A parent type is missing: {parent_type.Name}; You are trying to cast to `{toType}`.",
                            "ES1004",
                            toType
                        );
                    }

                    if(parent_type_table.CanUpcast(toType))
                        return true;
                }
            }

            return false;
        }

        public (bool, SimpleType) IsMatch(SimpleType referenceType, SimpleType selfType)
        {
            if(Name.EndsWith(Name.Contains(".") ? "." + referenceType.Name : referenceType.Name, StringComparison.CurrentCulture)
               && referenceType.TypeArguments.Count == TypeParameters.Count){
                var simple = AstType.MakeSimpleType(referenceType.Name, TypeParameters.Select(tp => tp.Clone()));
                return (true, simple);
            }else{
                if(ParentTypes == null || !ParentTypes.Any())
                    return (false, null);

                bool result = false;
                SimpleType result_type = null;
                foreach(var parent_type in ParentTypes){
                    if(parent_type is SimpleType simple){
                        var name = !simple.IdentifierNode.Type.IsNull ? simple.IdentifierNode.Type.Name : parent_type.Name;
                        var parent_table = GetTypeTable(name);
                        if(parent_table != null)
                            (result, result_type) = parent_table.IsMatch(referenceType, simple);

                        if(result)
                            return (result, result_type);
                    }
                }

                return (false, null);
            }
        }

        public SimpleType IsSubclassOf(SimpleType type)
        {
            var name = type.Name;
            if(Name.EndsWith(name, StringComparison.CurrentCulture)){
                return type;
            }else{
                if(ParentTypes == null || !ParentTypes.Any())
                    return null;

                SimpleType result_type = null;
                foreach(var parent_type in ParentTypes){
                    if(parent_type is SimpleType simple){
                        var name2 = !simple.IdentifierNode.Type.IsNull ? simple.IdentifierNode.Type.Name : simple.Name;
                        var parent_table = GetTypeTable(name2);
                        if(parent_table != null)
                            result_type = parent_table.IsSubclassOf(type);

                        if(result_type != null)
                            return simple;
                    }
                }

                return null;
            }
        }

        public override string ToString()
        {
            return $"<SymbolTable`{Name}: symbolsCount={Symbols.Count()} typeSymbolsCount={type_table.Values.Count} childrenCount={Children.Count} type={TypeKind}>";
        }

        /// <summary>
        /// Clones this instance so it has the same values as this instance.
        /// </summary>
        /// <returns>The clone.</returns>
        public SymbolTable Clone()
        {
            var cloned = new SymbolTable(TypeKind, IsForeignType){
                Name = Name
            };

            foreach(var key in table.Keys){
                if(!table.TryGetValue(key, out var value))
                    return null;
                else
                    cloned.table.AddRange(key, value);
            }

            foreach(var key in type_table.Keys){
                if(!type_table.TryGetValue(key, out var value))
                    return null;
                else
                    cloned.type_table.Add(key, value);
            }

            foreach(var child in Children){
                var cloned_child = child.Clone();
                cloned_child.Parent = cloned;
                cloned.Children.Add(cloned_child);
            }

            if(TypeParameters != null)
                cloned.AddTypeParameters(TypeParameters.Select(p => p.Clone()));

            if(ParentTypes != null)
                cloned.ParentTypes = ParentTypes.Select(pt => pt).ToList();

            if(TypeConstraints != null)
                cloned.TypeConstraints = TypeConstraints.Select(tc => tc).ToList();

            cloned.ParentClass = ParentClass;

            return cloned;
        }

        static void ReportExportMissingError(Identifier ident)
        {
            throw new ParserException(
                $"'{ident.Name}' does not have the `export` flag.\nYou cannot import an unexported item.",
                "ES3302",
                ident
            );
        }
    }
}
