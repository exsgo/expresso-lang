﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Expresso.Ast
{
    /// <summary>
    /// This class is responsible for replacing <see cref="SimpleType"/> nodes with <see cref="ParameterType"/> nodes.
    /// </summary>
    public class ParameterTypeReplacer : IAstTypeWalker
    {
        IEnumerable<ParameterType> type_params;

        public ParameterTypeReplacer(IEnumerable<ParameterType> typeParams)
        {
            type_params = typeParams;
        }

        public void VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            parameterType.Type.AcceptTypeWalker(this);
        }

        public void VisitFunctionType(FunctionType funcType)
        {
            funcType.ReturnType.AcceptTypeWalker(this);
            funcType.Parameters.AcceptTypeWalker(this);
        }

        public void VisitKeyValueType(KeyValueType keyValueType)
        {
        }

        public void VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptTypeWalker(this);
            VisitSimpleType(memberType.ChildType);
        }

        public void VisitParameterType(ParameterType paramType)
        {
        }

        public void VisitPlaceholderType(PlaceholderType placeholderType)
        {
        }

        public void VisitPrimitiveType(PrimitiveType primitiveType)
        {
        }

        // Can't ignore reference types
        public void VisitReferenceType(ReferenceType referenceType)
        {
        }

        public void VisitSimpleType(SimpleType simpleType)
        {
            foreach(var type_arg in simpleType.TypeArguments)
                type_arg.AcceptTypeWalker(this);

            if(type_params.Any(arg => arg.Name == simpleType.Name)){
                var param_type = type_params.First(arg => arg.Name == simpleType.Name);
                simpleType.ReplaceWith(param_type.Clone());
            }
        }

        public void VisitNullNode(AstNode nullNode)
        {
            throw new InvalidOperationException("Can not work on that node");
        }
    }
}
