﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Expresso.CodeGen;
using Expresso.TypeSystem;

using ICSharpCode.NRefactory.PatternMatching;

/**
 * Policy: Do not call AstType.Clone() until needed.
 */
namespace Expresso.Ast.Analysis
{
    class TypeArgumentEqualityComparer : EqualityComparer<AstType>
    {
        public override bool Equals(AstType x, AstType y)
        {
            var x_keyvalue = x as KeyValueType;
            var y_keyvalue = y as KeyValueType;
            if(x_keyvalue != null)
                return x_keyvalue.ValueType.IsMatch(y);
            else if(y_keyvalue != null)
                return y_keyvalue.ValueType.IsMatch(x);
            else
                return false;
        }

        public override int GetHashCode(AstType obj)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A type checker is responsible for type validity check as well as type inference, if needed.
    /// All <see cref="PlaceholderType"/> nodes will be replaced with real types
    /// inferred from the context.
    /// </summary>
    partial class TypeChecker : IAstWalker<AstType>
    {
        /// <summary>
        /// Contains parental type's type parameters and type arguments.
        /// </summary>
        static List<KeyValueType> InheritedTypeParameters = null;
        bool? is_main_function_defined;
        bool inspecting_value_binding_pattern, inspecting_lhs_of_vbp;
        int scope_counter;
        AstType cur_context_type, cur_target_type;
        Parser parser;
        SymbolTable symbols;  //keep a SymbolTable reference in a private field for convenience
        TypeInferenceRunner inference_runner;
        ClosureParameterInferencer closure_parameter_inferencer;
        NullCheckWalker null_checker;
        AstType[] argument_types;
        List<int> seen_enum_members;
        FieldDeclaration first_raw_value_enum_member;
        List<uint> pre_inspected_funcs = new List<uint>();
        //List<string> seen_patterns = new List<string>();

        public TypeChecker(Parser parser)
        {
            this.parser = parser;
            symbols = parser.Symbols;
            inference_runner = new TypeInferenceRunner(parser, this);
            closure_parameter_inferencer = new ClosureParameterInferencer(parser, this);
            null_checker = new NullCheckWalker(this);
        }

        public static void Check(ExpressoAst ast, Parser parser)
        {
            var checker = new TypeChecker(parser);

            Debug.Assert(checker.symbols.Name == "programRoot", "symbols should point at 'programRoot' before inspecting the ast in TypeChecker");
            checker.VisitAst(ast);
            Debug.Assert(checker.symbols.Name == "programRoot", "symbols should point at 'programRoot' after inspecting the ast in TypeChecker");
        }

        internal void DescendScope()
        {
            symbols = symbols.Children[scope_counter];
        }

        internal void AscendScope()
        {
            symbols = symbols.Parent;
        }

        #region IAstWalker implementation

        public AstType VisitAst(ExpressoAst ast)
        {
            Utilities.CompilerOutput.WriteLine($"Checking types in {ast.ModuleName}...");

            if(ast.Name == "main")
                is_main_function_defined = false;

            foreach(var attribute in ast.Attributes)
                VisitAttributeSection(attribute);

            foreach(var import_decl in ast.Imports)
                VisitImportDeclaration(import_decl);
            
            foreach(var decl in ast.Declarations)
                decl.AcceptWalker(this);

            var does_main_function_exist = is_main_function_defined ?? false;
            if(is_main_function_defined != null && !does_main_function_exist){
                throw new ParserException(
                    "Even though the module is named \"main\", there is no main function defined in the module.",
                    "ES4020",
                    ast
                );
            }
                
            return null;
        }

        public AstType VisitBlock(BlockStatement block)
        {
            if(block.IsNull)
                return null;
            
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            foreach(var stmt in block.Statements)
                stmt.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitBreakStatement(BreakStatement breakStmt)
        {
            int loop_count = (int)breakStmt.CountExpression.Value;
            int actual_count = breakStmt.Ancestors.TakeWhile(a => !(a is FunctionDeclaration))
                                        .Count(n => n is WhileStatement || n is DoWhileStatement || n is ForStatement || n is ValueBindingForStatement);
            if(actual_count < loop_count){
                throw new ParserException(
                    $"If we break out of loops {loop_count} times with the break statement at this point, we'll enter into nothing.",
                    "ES4010",
                    breakStmt
                ){
                    HelpObject = actual_count
                };
            }

            return null;
        }

        public AstType VisitContinueStatement(ContinueStatement continueStmt)
        {
            int loop_count = (int)continueStmt.CountExpression.Value;
            int actual_count = continueStmt.Ancestors.TakeWhile(a => !(a is FunctionDeclaration))
                                           .Count(n => n is WhileStatement || n is DoWhileStatement || n is ForStatement || n is ValueBindingForStatement);
            if(actual_count < loop_count){
                throw new ParserException(
                    $"If we break out of loops {loop_count} times with the continue statement at this point, we'll enter into nothing.",
                    "ES4011",
                    continueStmt
                ){
                    HelpObject = actual_count
                };
            }

            return null;
        }

        public AstType VisitDoWhileStatement(DoWhileStatement doWhileStmt)
        {
            return VisitWhileStatement(doWhileStmt.Delegator);
        }

        public AstType VisitEmptyStatement(EmptyStatement emptyStmt)
        {
            return AstType.Null;
        }

        public AstType VisitExpressionStatement(ExpressionStatement exprStmt)
        {
            return exprStmt.Expression.AcceptWalker(this);
        }

        public AstType VisitForStatement(ForStatement forStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var left_type = forStmt.Left.AcceptWalker(this);
            var walker = ExpressoCompilerHelpers.IsPlaceholderType(left_type) ? (IAstWalker<AstType>)inference_runner : this;
            var target_type = forStmt.Target.AcceptWalker(walker);

            if(!IsSequenceType(target_type)){
                parser.ReportSemanticError(
                    $"`{left_type}` is not a sequence type!",
                    "ES1301",
                    forStmt.Target
                );
            }else{
                var elem_type = MakeOutElementType(target_type);
                // We don't need to issue a type mismatch error here because it is already done in VariableInitializer
                left_type.ReplaceWith(elem_type);
            }

            VisitBlock(forStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStatment)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            VisitVariableInitializer(valueBindingForStatment.Initializer);

            var pat_with_type = valueBindingForStatment.Initializer.Pattern;
            if(!(pat_with_type.Pattern is IdentifierPattern) && !(pat_with_type.Pattern is TuplePattern)){
                throw new ParserException (
                    $"{pat_with_type.Pattern} is not suitable for a for statement declaration.",
                    "ES4040",
                    valueBindingForStatment
                );
            }

            VisitBlock(valueBindingForStatment.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitIfStatement(IfStatement ifStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var condition_type = ifStmt.Condition.AcceptWalker(this);
            if(!(ifStmt.Condition is ValueBindingPattern) && condition_type is PrimitiveType primitive && primitive.KnownTypeCode != KnownTypeCode.Bool){
                parser.ReportSemanticError(
                    "The conditional expression has to be of type `bool`.",
                    "ES4000",
                    ifStmt.Condition
                );
            }
            VisitBlock(ifStmt.TrueBlock);
            // We can't rewrite this to VisitBlock(ifStmt.FalseBlock);
            // because doing so can continue execution even if it is null.
            ifStmt.FalseStatement.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitReturnStatement(ReturnStatement returnStmt)
        {
            //returnStmt.Expression.AcceptWalker(inference_runner);
            returnStmt.Expression.AcceptWalker(this);
            return null;
        }

        public AstType VisitMatchStatement(MatchStatement matchStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var target_type = matchStmt.Target.AcceptWalker(this);
            var prev_context_type = cur_context_type;
            cur_context_type = target_type;
            foreach(var clause in matchStmt.Clauses){
                var type = VisitMatchClause(clause);
                if(IsCompatibleWith(target_type, type) == TriBool.False){
                    parser.ReportSemanticErrorRegional(
                        $"Mismatched types found in match patterns! Expected `{target_type}`, found `{type}`.",
                        "ES1022",
                        matchStmt, clause
                    );
                }
            }

            cur_context_type = prev_context_type;

            ExhaustivenessChecker.Execute(symbols, matchStmt, target_type);

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitThrowStatement(ThrowStatement throwStmt)
        {
            throwStmt.CreationExpression.AcceptWalker(this);
            return null;
        }

        public AstType VisitTryStatement(TryStatement tryStmt)
        {
            tryStmt.EnclosingBlock.AcceptWalker(this);
            foreach(var clause in tryStmt.CatchClauses)
                clause.AcceptWalker(this);

            // We can't rewrite this to directly calling VisitFinally
            // because it can be null.
            tryStmt.FinallyClause.AcceptWalker(this);
            return null;
        }

        public AstType VisitWhileStatement(WhileStatement whileStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            whileStmt.Condition.AcceptWalker(this);
            whileStmt.Body.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitYieldStatement(YieldStatement yieldStmt)
        {
            yieldStmt.Expression.AcceptWalker(this);
            return null;
        }

        public AstType VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl)
        {
            foreach(var variable in varDecl.Variables)
                VisitVariableInitializer(variable);

            return null;
        }

        public AstType VisitArrayInitializer(ArrayInitializerExpression arrayInitializer)
        {
            var size_type = arrayInitializer.SizeExpression.AcceptWalker(inference_runner);
            if(!(size_type is PrimitiveType) || size_type.Name != "int"){
                throw new ParserException(
                    "Array size must be an `int`!",
                    "ES1710",
                    arrayInitializer
                );
            }

            return AstType.MakeSimpleType("array", arrayInitializer.StartLocation, arrayInitializer.EndLocation, arrayInitializer.TypePath.Clone());
        }

        public AstType VisitAssignment(AssignmentExpression assignment)
        {
            assignment.AcceptWalker(inference_runner);

            if((assignment.Left is SequenceExpression sequence1 && sequence1.Items.Count > 1 || assignment.Right is SequenceExpression sequence2 && sequence2.Items.Count > 1)
               && assignment.Operator != OperatorType.Assign){
                throw new ParserException(
                    "Augmented assignments cannot have multiple items on each side.",
                    "ES2101",
                    assignment
                );
            }

            var left_type = assignment.Left.AcceptWalker(this);

            if(left_type is SimpleType simple_left && simple_left.Name == "tuple"){
                // We see the left hand side is a sequence expression so validate each item on both sides.
                var left_types = simple_left.TypeArguments.ToList();
                var right_type = assignment.Right.AcceptWalker(this);
                // Don't validate the number of elements because we have already done that in parse phase.
                if(right_type is SimpleType simple_right && simple_right.Name == "tuple"){
                    var right_types = simple_right.TypeArguments.ToList();
                    foreach(var triple in left_types.Zip(right_types, (l, r) => new {L = l, R = r})
                                                    .Zip(Enumerable.Range(0, left_types.Count), (l, r) => new {Left = l.L, Right = l.R, Index = r})){
                        var flattened = (triple.Left is FunctionType func_type && func_type.Name.StartsWith("set_", StringComparison.CurrentCulture)) ? func_type.Parameters.First()
                                                                                                                                                                   : triple.Left;
                        flattened = (triple.Left is ReferenceType ref_type) ? ref_type.BaseType : flattened;

                        if(ExpressoCompilerHelpers.IsPlaceholderType(flattened)){
                            var inferred_type = triple.Right.Clone();
                            triple.Left.ReplaceWith(inferred_type);
                        }else if(IsCompatibleWith(flattened, triple.Right) == TriBool.False){
                            var lhs_seq = assignment.Left as SequenceExpression;
                            var rhs_seq = assignment.Right as SequenceExpression;
                            parser.ReportSemanticErrorRegional(
                                $"There is a type mismatch; left=`{triple.Left}`, right=`{triple.Right}`.",
                                "ES1100",
                                lhs_seq.Items.ElementAt(triple.Index), rhs_seq.Items.ElementAt(triple.Index)
                            );
                        }
                    }
                }

                return AstType.Null;
            }else if(ExpressoCompilerHelpers.IsPlaceholderType(left_type)){
                // never reached?
                var inferred_type = assignment.Right.AcceptWalker(inference_runner);
                left_type.ReplaceWith(inferred_type);

                assignment.Right.AcceptWalker(this);
                return inferred_type;
            }else{
                // never reached?
                var right_type = assignment.Right.AcceptWalker(this);
                if(IsCompatibleWith(left_type, right_type) == TriBool.False){
                    parser.ReportSemanticErrorRegional(
                        $"The type `{left_type}` on the left hand side is not compatible with the type `{right_type}` on the right hand side.",
                        "ES1001",
                        assignment.Left, assignment.Right
                    );
                }
                return left_type;
            }
        }

        // Note on VisitBinaryExpression that it can also be inspected from InferenceRunner
        public AstType VisitBinaryExpression(BinaryExpression binaryExpr)
        {
            var lhs_type = binaryExpr.Left.AcceptWalker(this);
            if(ExpressoCompilerHelpers.IsPlaceholderType(lhs_type)){
                lhs_type = binaryExpr.Left.AcceptWalker(inference_runner);
                // Do not replace the type node because inference runner has already done that.
                //lhs_type.ReplaceWith(inferred_type);
                //lhs_type = inferred_type;
            }

            var rhs_type = binaryExpr.Right.AcceptWalker(this);
            if(ExpressoCompilerHelpers.IsPlaceholderType(rhs_type)){
                rhs_type = binaryExpr.Right.AcceptWalker(inference_runner);
                //rhs_type.ReplaceWith(inferred_type2);
                //rhs_type = inferred_type2;
            }

            if(lhs_type is FunctionType property1 && property1.Name.StartsWith("get_", StringComparison.Ordinal))
                lhs_type = property1.ReturnType;

            if(rhs_type is FunctionType property2 && property2.Name.StartsWith("get_", StringComparison.Ordinal))
                rhs_type = property2.ReturnType;
            
            if(IsCompatibleWith(lhs_type, rhs_type) == TriBool.False){
                // Invalid operators must lead to this code path
                parser.ReportSemanticErrorRegional(
                    $"Can not apply the operator '{binaryExpr.OperatorToken}' on `{lhs_type}` and `{rhs_type}`.",
                    "ES1002",
                    binaryExpr.Left, binaryExpr.Right
                );
                return null;
            }else if(lhs_type is ParameterType || rhs_type is ParameterType){
                throw new ParserException(
                    $"Can not apply the operator '{binaryExpr.OperatorToken}' on generic types `{lhs_type}` and `{rhs_type}`.",
                    "ES1007",
                    binaryExpr.Left, binaryExpr.Right
                );
            }

            var preferable_type = FigureOutCommonType(lhs_type, rhs_type);
            binaryExpr.PreferredType = preferable_type.Clone();

            //var lhs_primitive = lhs_type as PrimitiveType;
            //var rhs_primitive = rhs_type as PrimitiveType;
            switch(binaryExpr.Operator){
            case OperatorType.ConditionalAnd:
            case OperatorType.ConditionalOr:
            case OperatorType.Equality:
            case OperatorType.InEquality:
            case OperatorType.LessThan:
            case OperatorType.LessThanOrEqual:
            case OperatorType.GreaterThan:
            case OperatorType.GreaterThanOrEqual:
                return AstType.MakePrimitiveType("bool", binaryExpr.StartLocation);

            case OperatorType.Plus:
            case OperatorType.Minus:
            case OperatorType.Times:
            case OperatorType.Divide:
            case OperatorType.Power:
            case OperatorType.Modulus:
                /*if(!IsNumericalType(lhs_type) || !IsNumericalType(rhs_type)){
                    parser.ReportSemanticErrorRegional(
                        "Error ES1005: Can not apply the operator '{0}' on `{1}` and `{2}`",
                        binaryExpr.Left, binaryExpr.Right,
                        binaryExpr.OperatorToken, lhs_type, rhs_type
                    );
                    return null;
                }*/

                return lhs_type;

            case OperatorType.BitwiseAnd:
            case OperatorType.BitwiseOr:
            case OperatorType.BitwiseShiftLeft:
            case OperatorType.BitwiseShiftRight:
            case OperatorType.ExclusiveOr:
                /*if(lhs_primitive.KnownTypeCode == KnownTypeCode.Float || lhs_primitive.KnownTypeCode == KnownTypeCode.Double){
                    parser.ReportSemanticError(
                        "Error ES1010: Can not apply the operator '{0}' on the left hand side '{1}'",
                        binaryExpr.Left,
                        binaryExpr.OperatorToken, binaryExpr.Left
                    );
                    return null;
                }else if(rhs_primitive.KnownTypeCode == KnownTypeCode.Float || rhs_primitive.KnownTypeCode == KnownTypeCode.Double){
                    parser.ReportSemanticError(
                        "Error ES1011: Can not apply the operator '{0}' on the right hand side '{1}'",
                        binaryExpr.Right,
                        binaryExpr.OperatorToken, binaryExpr.Right
                    );
                    return null;
                }else{*/
                    return lhs_type;
                //}

            default:
                throw new ArgumentException("Unknown operator found!");
            }
        }

        public AstType VisitCallExpression(CallExpression callExpr)
        {
            // There are times when inference_runner.VisitCallExpression hasn't been called
            if(callExpr.ResultType == null || callExpr.ResultType.IsNull)
                inference_runner.VisitCallExpression(callExpr);

            var arg_types = new AstType[callExpr.Arguments.Count];
            foreach(var pair in Enumerable.Range(0, callExpr.Arguments.Count).Zip(callExpr.Arguments, (l, r) => new {Index = l, Argument = r})){
                var arg_type = pair.Argument.AcceptWalker(this);
                // FIXME?: Think about changing the property methods' types
                // arg_type doesn't need to be cloned because the user of this field will clone them
                arg_type = (arg_type is FunctionType func_type && func_type.Name.StartsWith("get_", StringComparison.CurrentCulture)) ? func_type.ReturnType : arg_type;
                // The literal `null` returns the null AstType
                arg_types[pair.Index] = arg_type.IsNull ? arg_type : ExpressoCompilerHelpers.SubstituteGenericTypes(arg_type, callExpr.TypeArguments);
            }

            var parent_types = argument_types;
            argument_types = arg_types;

            var func_type2 = callExpr.Target.AcceptWalker(this) as FunctionType;
            // Don't call inference_runner.VisitCallExpression here
            // because doing so causes VisitIdentifier to be invoked twice
            // and show the same message twice
            /*if(IsPlaceholderType(func_type)){
                var inferred = inference_runner.VisitCallExpression(callExpr);
                // Don't replace nodes here because the above code does that
                //func_type.ReplaceWith(inferred);
                return inferred;
            }*/

            if(func_type2 == null){
                throw new ParserException(
                    $"'{callExpr.Target}' turns out not to be a function.",
                    "ES1802",
                    callExpr
                );
            }else if(func_type2.Parameters.Any(ExpressoCompilerHelpers.IsPlaceholderType) || ExpressoCompilerHelpers.IsPlaceholderType(func_type2.ReturnType)){
                var surrounding_func = callExpr.Ancestors
                                               .OfType<FunctionDeclaration>()
                                               .First();
                if(surrounding_func.Name == func_type2.Name){
                    throw new ParserException(
                        "Function signatures must be completed when they are called recursively.",
                        "ES4100",
                        callExpr
                    );
                }

                var called_func = surrounding_func.GetNextSibling(s => s is FunctionDeclaration func_decl && func_decl.Name == func_type2.Name) as FunctionDeclaration;
                if(called_func != null && called_func.Name == func_type2.Name){
                    throw new ParserException(
                        "Function signatures must be completed when they are defined after the callee.",
                        "ES4101",
                        callExpr
                    );
                }
            }

            if(callExpr.Parent is CallExpression inner_call && inner_call.Target is MemberReferenceExpression memref
            && memref.Target is PathExpression path_expr && path_expr.AsIdentifier != null && path_expr.AsIdentifier.Name == "string"
            && memref.Member.Name == "Format" && ExpressoCompilerHelpers.IsVoidType(func_type2.ReturnType)){
                throw new ParserException(
                    "Functions or methods in a string interpolation must return something.",
                    "ES1801",
                    callExpr
                );
            }

            var arg_count = arg_types.Length;
            var last_param = func_type2.Parameters.LastOrDefault();
            if(last_param != null && last_param is FunctionParameterType func_param_type && !func_param_type.Modifiers.HasFlag(FunctionParameterModifiers.Variadic)
               && arg_count > func_type2.Parameters.Count || arg_count > func_type2.Parameters.Count && last_param == null){
                parser.ReportSemanticError(
                    $"'{func_type2.Name}' takes {func_type2.Parameters.Count} parameters.",
                    "ES2031",
                    callExpr
                );
            }else if(arg_count < func_type2.Parameters.Count && func_type2.Parameters.ElementAt(arg_count) is FunctionParameterType func_param_type2
                && !func_param_type2.Modifiers.HasFlag(FunctionParameterModifiers.Optional) && !func_param_type2.Modifiers.HasFlag(FunctionParameterModifiers.Variadic)){
                var first_optional_param = func_type2.Parameters
                                                     .Cast<FunctionParameterType>()
                                                     .FirstOrDefault(p => p.Modifiers.HasFlag(FunctionParameterModifiers.Optional));
                var first_optional_index = func_type2.Parameters.ToList().IndexOf(first_optional_param);
                var expected_param_count_str = (first_optional_param != null) ? $"{first_optional_index} ~ {func_type2.Parameters.Count}" : null;

                parser.ReportSemanticError(
                    $"'{func_type2.Name}' takes {expected_param_count_str ?? func_type2.Parameters.Count.ToString()} parameters.",
                    "ES2030",
                    callExpr
                );
            }

            foreach(var triple in Enumerable.Range(0, func_type2.Parameters.Count)
                                            .Zip(func_type2.Parameters, (l, r) => new {Index = l, Parameter = r})
                                            .Zip(arg_types, (l, r) => new {l.Index, l.Parameter, Argument = r})){
                AstType param_type = null;
                if(triple.Parameter is FunctionParameterType func_param_type2){
                    if(func_param_type2.Modifiers.HasFlag(FunctionParameterModifiers.Variadic) && func_param_type2.Type is SimpleType simple)
                        param_type = simple.TypeArguments.First();
                    else
                        param_type = func_param_type2.Type;
                }else{
                    param_type = triple.Parameter;
                }

                var arg_type = triple.Argument;
                if(IsCompatibleWith(param_type, arg_type) == TriBool.False){
                    throw new ParserException(
                        $"Types mismatched in call; Expected: `{param_type}`, found: `{arg_type}`.",
                        "ES1303",
                        callExpr.Arguments.ElementAt(triple.Index)
                    );
                }
            }

            argument_types = parent_types;

            CheckDefaultValues(callExpr, func_type2);

            if(func_type2.IdentifierNode.Modifiers.HasFlag(Modifiers.Extension) && callExpr.Target is MemberReferenceExpression mem_ref2){
                // If we are calling an extension method, transform the arguments here where we have already checked argument validity
                var original_args = callExpr.Arguments.Select(a => a.Clone()).ToArray();
                callExpr.Arguments.Clear();
                callExpr.Arguments.Add(mem_ref2.Target.Clone());
                callExpr.Arguments.AddRange(original_args);
                callExpr.CallAsExtension = true;
            }

            return callExpr.ResultType;
        }

        public AstType VisitCastExpression(CastExpression castExpr)
        {
            var target_type = castExpr.ToType.AcceptWalker(this);
            var expression_type = castExpr.Target.AcceptWalker(this);
            var expression_type_symbol = symbols.GetTypeSymbolInAnyScope(expression_type.Name);
            if(expression_type_symbol == null){
                throw new ParserException(
                    $"Although the expression '{castExpr.Target}' is evaluated to the type `{expression_type}`, there is not any type with that name in the scope '{symbols.Name}'.",
                    "ES2000",
                    castExpr.Target
                );
            }

            if(IsCastable(expression_type, target_type) == TriBool.False){
                throw new ParserException(
                    $"Can not cast the type `{expression_type}` to the type `{target_type}`.",
                    "ES1003",
                    castExpr.Target, castExpr.ToType
                );
            }

            return target_type;
        }

        public AstType VisitCatchClause(CatchClause catchClause)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            VisitIdentifier(catchClause.Identifier);
            //inference_runner.VisitCatchClause(catchClause);
            VisitBlock(catchClause.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;

            return null;
        }

        public AstType VisitClosureLiteralExpression(ClosureLiteralExpression closure)
        {
            if(closure.HasInspectedInTypeChecker){
                //++scope_counter;
                var param_types2 =
                    from p in closure.Parameters
                    select p.ReturnType.Clone();
                return AstType.MakeFunctionType("closure", closure.ReturnType.Clone(), param_types2);
            }

            bool discovered_return_type = false;
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            // Set to true here because this method could call inference_runner.VisitClosureExpression
            closure.HasInspectedInTypeChecker = true;

            foreach(var param in closure.Parameters){
                var param_type = param.AcceptWalker(this);
                if(ExpressoCompilerHelpers.IsPlaceholderType(param_type))
                    closure_parameter_inferencer.VisitParameterDeclaration(param);
            }

            if(closure.ReturnType is PlaceholderType return_placeholder)
                closure_parameter_inferencer.VisitPlaceholderType(return_placeholder);

            bool has_inspected_body = false;
            if(!closure.Parameters.Any(p => ExpressoCompilerHelpers.ContainsAnyParameterType(p.ReturnType))){
                // When any of the parameters contains `ParameterType`, we can't VisitBlock
                // because doing so can cause a type mismatch error on binary expressions
                VisitBlock(closure.Body);
                has_inspected_body = true;
            }

            // Delay discovering the return type because the body statements should be type-aware
            // before the return type is started to be inferred
            if(ExpressoCompilerHelpers.IsPlaceholderType(closure.ReturnType) || ExpressoCompilerHelpers.ContainsAnyParameterType(closure.ReturnType)){
                // Because VisitBlock has made scope_counter one step forward
                if(has_inspected_body)
                    --scope_counter;

                // Descend scopes 2 times because closure parameters have their own scope
                int tmp_counter2 = scope_counter;
                DescendScope();
                scope_counter = 0;

                //inference_runner.InspectsClosure = false;
                var return_type = inference_runner.FigureOutReturnType(closure.Body);
                closure.ReturnType.ReplaceWith(return_type.Clone());

                AscendScope();
                scope_counter = tmp_counter2;
                discovered_return_type = true;
            }

            if(closure.LiftedIdentifiers == null){
                // The same reason as if(IsPlaceholderType(closure.ReturnType)) block
                if(!discovered_return_type && has_inspected_body)
                    --scope_counter;

                var inspecter = new ClosureInspecter(parser, this, closure);
                inspecter.VisitClosureLiteralExpression(closure);
            }

            var param_types = 
                from p in closure.Parameters
                select p.ReturnType.Clone();

            AscendScope();
            scope_counter = tmp_counter + 1;
            
            return AstType.MakeFunctionType("closure", closure.ReturnType.Clone(), param_types);
        }

        public AstType VisitComprehensionExpression(ComprehensionExpression comp)
        {
            return comp.ObjectType;
        }

        public AstType VisitComprehensionForClause(ComprehensionForClause compFor)
        {
            return null;
        }

        public AstType VisitComprehensionIfClause(ComprehensionIfClause compIf)
        {
            return null;
        }

        public AstType VisitConditionalExpression(ConditionalExpression condExpr)
        {
            condExpr.Condition.AcceptWalker(this);
            var true_type = condExpr.TrueExpression.AcceptWalker(this);
            var false_type = condExpr.FalseExpression.AcceptWalker(this);
            if(IsCompatibleWith(true_type, false_type) == TriBool.False){
                parser.ReportSemanticErrorRegional(
                    $"A conditional expression must return one type! And `{true_type}` is not compatible with `{false_type}`.",
                    "ES1005",
                    condExpr.Condition, condExpr.FalseExpression
                );
            }

            return true_type;
        }

        public AstType VisitFinallyClause(FinallyClause finallyClause)
        {
            return VisitBlock(finallyClause.Body);
        }

        public AstType VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue)
        {
            return null;
        }

        public AstType VisitLiteralExpression(LiteralExpression literal)
        {
            return literal.Type;
        }

        public AstType VisitIdentifier(Identifier ident)
        {
            // Infer and spread the type of the identifier to this node
            inference_runner.VisitIdentifier(ident);

            var type_table = (!ident.Type.IsNull && ident.Type.IdentifierNode.Type.IsNull) ? symbols.GetTypeTable(ident.Type.Name) : null;
            var value_symbol = type_table?.GetSymbol(Utilities.RawValueEnumValueFieldName);
            var mem_ref = (value_symbol != null) ? ident.Ancestors.OfType<MemberReferenceExpression>().FirstOrDefault() : null;
            if(type_table != null && type_table.TypeKind == ClassType.Enum && value_symbol != null && !ident.Ancestors.Any(a => a is ObjectCreationExpression)
               && (mem_ref == null || mem_ref.Member.IsMatch(ident) && ident.Name != Utilities.RawValueEnumValueFieldName)){
                // We recognize an identifier whose type is an enum and which is the last child
                // as specifying the enum value
                // We know that an identifier that represents an enum object will never show up on its own as an identifier
                var parent = (Expression)ident.Parent;
                var new_mem_ref = Expression.MakeMemRef(parent.Clone(), AstNode.MakeIdentifier(Utilities.RawValueEnumValueFieldName, AstType.MakePlaceholderType()));
                ident.Parent.ReplaceWith(new_mem_ref);

                // We replace the Type property of Member in case this node will be inspected more than once
                // and it would if the reference to an enum resides in normal expressions
                new_mem_ref.Member.IdentifierId = value_symbol.IdentifierId;
                new_mem_ref.Member.Type.ReplaceWith(ident.Type.Clone());
            }

            return ident.Type;
        }

        public AstType VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq)
        {
            var start_type = intSeq.Start.AcceptWalker(inference_runner);
            var end_type = intSeq.End.AcceptWalker(inference_runner);
            var step_type = intSeq.Step.AcceptWalker(inference_runner);

            if(!IsSmallIntegerType(start_type)){
                throw new ParserException(
                    $"`{start_type}` is not an `int` type!",
                    "ES4001",
                    intSeq.Start
                );
            }

            if(!IsSmallIntegerType(end_type)){
                throw new ParserException(
                    $"`{end_type}` is not an `int` type!",
                    "ES4001",
                    intSeq.End
                );
            }

            if(!IsSmallIntegerType(step_type)){
                throw new ParserException(
                    $"`{step_type}` is not an `int` type!",
                    "ES4001",
                    intSeq.Step
                );
            }

            if((intSeq.Start is LiteralExpression || intSeq.Start is UnaryExpression unary && unary.Operand is LiteralExpression) &&
               (intSeq.End is LiteralExpression || intSeq.End is UnaryExpression unary2 && unary2.Operand is LiteralExpression)
               && (intSeq.Step is LiteralExpression || intSeq.Step is UnaryExpression unary3 && unary3.Operand is LiteralExpression)){
                var start = MakeOutIntFromIntSeq(intSeq.Start);
                var end = MakeOutIntFromIntSeq(intSeq.End);
                var step = MakeOutIntFromIntSeq(intSeq.Step);

                if(start < end && step < 0){
                    parser.ReportWarning(
                        $"Although `step` is negative, `start` is smaller than `end`.\n Did you mean {end}{(intSeq.UpperInclusive ? "..." : "..")}{start}:{step}?",
                        "ES4003",
                        intSeq
                    );
                }else if(start > end && step > 0){
                    parser.ReportWarning(
                        $"Although `step` is positive, `start` is larger than `end`.\n Did you mean {end}{(intSeq.UpperInclusive ? "..." : "..")}{start}:{step}?",
                        "ES4004",
                        intSeq
                    );
                }else if(step == 0){
                    parser.ReportWarning(
                        "`step` is 0! It will result in an inifite series of a sequence!",
                        "ES4005",
                        intSeq
                    );
                }else if(start == end){
                    parser.ReportWarning(
                        "`start` is equal to `end`. It will result in a sequence with zero elements!",
                        "ES4006",
                        intSeq
                    );
                }
            }

            return AstType.MakePrimitiveType("intseq", intSeq.StartLocation);
        }

        public AstType VisitIndexerExpression(IndexerExpression indexExpr)
        {
            var type = indexExpr.Target.AcceptWalker(this);
            if(ExpressoCompilerHelpers.IsPlaceholderType(type)){
                var return_type = inference_runner.VisitIndexerExpression(indexExpr);
                return return_type;
            }

            if(type is SimpleType simple_type){
                if(!CanIndexType(simple_type)){
                    throw new ParserException(
                        $"Can not apply the indexer operator on the type `{simple_type}`.",
                        "ES3011",
                        indexExpr
                    );
                }

                if(indexExpr.Arguments.Count == 1){
                    var arg_type = indexExpr.Arguments.First().AcceptWalker(this);
                    if(arg_type is PrimitiveType primitive && primitive.KnownTypeCode == KnownTypeCode.IntSeq){
                        if(simple_type.Identifier == "dictionary"){
                            throw new ParserException(
                                "Can not apply the indexer operator on a dictionary with an `intseq`.",
                                "ES3012",
                                indexExpr
                            );
                        }

                        return AstType.MakeSimpleType("slice", new []{simple_type.Clone(), simple_type.TypeArguments.First().Clone()});
                    }
                }

                if(simple_type.Name == "array" || simple_type.Name == "vector" || simple_type.Name == "dictionary"){
                    return simple_type.TypeArguments.LastOrNullObject().Clone();
                }else{
                    var name = !simple_type.IdentifierNode.Type.IsNull ? simple_type.IdentifierNode.Type.Name : simple_type.Name;
                    var table = symbols.GetTypeTable(name);
                    // No need to check table is not null because we have already done that in CanIndexType
                    var sub_type = table.IsSubclassOf(AstType.MakeSimpleType("System.Collections.Generic.ICollection"));
                    return sub_type.TypeArguments.LastOrNullObject().Clone();
                }
            }

            throw new ParserException(
                $"Can not index into a value of type `{type}`.",
                "ES3013",
                indexExpr
            );
        }

        public AstType VisitMemberReference(MemberReferenceExpression memRef)
        {
            if(memRef.Member.IdentifierId == 0u)
                inference_runner.VisitMemberReference(memRef);

            var type = memRef.Target.AcceptWalker(this);
            cur_target_type = type;
            if(ExpressoCompilerHelpers.IsPlaceholderType(type)){
                var inferred = memRef.Target.AcceptWalker(inference_runner);
                // Don't replace the type node because InferenceRunner has already done that
                //type.ReplaceWith(inferred);
                inference_runner.VisitMemberReference(memRef);
                type = inferred;
            }

            /*if(type == null){
                throw new ParserException(
                    "The expression '{0}' is not resolved to a type",
                    "ES3302",
                    memRef,
                    memRef.Target.ToString()
                );
            }*/

            VisitIdentifier(memRef.Member);
            var name = (type is MemberType member) ? member.Target.Name + "::" + member.MemberName :
                                                           !type.IdentifierNode.Type.IsNull ? type.IdentifierNode.Type.Name : type.Name;
            var type_table = symbols.GetTypeTable(name);
            // name could be a ParameterType's one so we should first check for null
            if(type_table != null && !type_table.IsForeignType){
                var symbol = type_table.GetSymbolInTypeChain(memRef.Member.Name);
                if(memRef.Target is PathExpression path && path.AsIdentifier.Modifiers.HasFlag(Modifiers.Immutable) && symbol.Modifiers.HasFlag(Modifiers.Mutating)){
                    throw new ParserException(
                        $"A mutating method '{symbol.Name}' cannot be called on an immutable variable.",
                        "ES2100",
                        memRef
                    );
                }
            }

            return memRef.Member.Type;
        }

        public AstType VisitPathExpression(PathExpression pathExpr)
        {
            if(pathExpr.Items.Count == 1){
                var type = VisitIdentifier(pathExpr.AsIdentifier);
                if(type.IsNull){
                    // This indicates that the Identifier is likely to represent a type
                    return AstType.MakeSimpleType(pathExpr.AsIdentifier.Name);
                }
                return type;
            }else{
                var old_table = symbols;
                AstType result = null;
                foreach(var item in pathExpr.Items){
                    var tmp_table = symbols.GetTypeTable(item.Name);
                    if(tmp_table == null)
                        result = VisitIdentifier(item);
                    else
                        symbols = tmp_table;
                }

                symbols = old_table;
                return result;
            }
        }

        public AstType VisitParenthesizedExpression(ParenthesizedExpression parensExpr)
        {
            return parensExpr.Expression.AcceptWalker(this);
        }

        public AstType VisitObjectCreationExpression(ObjectCreationExpression creation)
        {
            // We don't need this because VariableInitializer should have already done that
            //inference_runner.VisitObjectCreationExpression(creation);
            var type_path = creation.TypePath;
            var table = (type_path is MemberType member) ? symbols.GetTypeTable(member.Target.Name) : symbols;

            // It is guaranteed that if the type table is for an enum, member2 is valid
            if(table.TypeKind == ClassType.Enum && type_path is MemberType member2){
                var enum_member = table.GetSymbol(member2.MemberName);
                // Don't report member missing error because InferenceRunner has already done that
                //if(enum_member == null){

                var arg_types2 = creation.Items
                                         .Select(item => item.ValueExpression.AcceptWalker(this))
                                         .ToList();
                var type_params = table.TypeParameters;

                var variant_type = (SimpleType)enum_member.Type;
                var variant_real_type = (SimpleType)variant_type.IdentifierNode.Type;
                if(arg_types2.Count < variant_real_type.TypeArguments.Count){
                    throw new ParserException(
                        $"The variant '{enum_member.Name}' takes {variant_real_type.TypeArguments.Count} parameters.",
                        "ES2011",
                        creation
                    );
                }else if(arg_types2.Count > variant_real_type.TypeArguments.Count){
                    throw new ParserException(
                        $"The variant '{enum_member.Name}' takes {variant_real_type.TypeArguments.Count} parameters.",
                        "ES2012",
                        creation
                    );
                }

                foreach(var triple in variant_real_type.TypeArguments.Zip(arg_types2, (l, r) => new {Ta = l, At = r })
                                                       .Zip(creation.Items, (l, r) => new {TypeArgument = l.Ta, ArgumentType = l.At, Item = r})){
                    if(IsCompatibleWith(triple.TypeArgument, triple.ArgumentType) == TriBool.False){
                        throw new ParserException(
                            $"Types mismatched in variant construction; Expected: `{triple.TypeArgument}`, found: `{triple.ArgumentType}`.",
                            "ES2013",
                            triple.Item
                        ){
                            HelpObject = ExpressoCompilerHelpers.StringifyList(variant_real_type.TypeArguments)
                        };
                    }
                }

                if(creation.TypeArguments.Any())
                    CheckTypeArgumentsForEnum(creation, creation.TypeArguments, type_params, arg_types2, variant_real_type.TypeArguments);

                // We need to clone it because there can be another code that instantiates the enum with other types
                var enum_type_symbol = table.GetTypeSymbolInAnyScope(member2.Target.Name);
                var enum_type = AstType.MakeSimpleType(enum_type_symbol.Name);
                enum_type.TypeArguments.ReplaceWith(creation.TypeArguments.Select(ta => ta.Clone()));
                return enum_type;
            }

            var type_table = table.GetTypeTable(!type_path.IdentifierNode.Type.IsNull ? type_path.IdentifierNode.Type.Name : type_path.Name);
            // Don't report type table missing error because InferenceRunner has already done that
            //if(type_table == null){
                // Report type table missing error because InferenceRunner doesn't always do that
                /*throw new ParserException(
                    "The type `{0}` is not found or accessible from the scope {1}.",
                    "ES1501",
                    creation,
                    creation.TypePath.ToString(), symbols.Name
                );
            }*/

            if(type_table.TypeKind == ClassType.Interface){
                throw new ParserException(
                    "Interfaces cannot be instantiated.",
                    "ES1400",
                    creation
                );
            }else if(type_table.TypeKind == ClassType.Enum){
                throw new ParserException(
                    "A variant name is missing in enum construction.",
                    "ES2400",
                    creation
                ){
                    HelpObject = ExpressoCompilerHelpers.StringifyList(type_table.Symbols.Where(s => s.Type is SimpleType))
                };
            }

            var type_symbol = table.GetTypeSymbolInAnyScope(type_path.Name);
            if(type_table.TypeKind == ClassType.Class && type_symbol.Modifiers.HasFlag(Modifiers.Abstract)){
                throw new ParserException(
                    "Abstract classes cannot be instantiated.",
                    "ES1401",
                    creation
                );
            }

            var arg_types = new AstType[creation.Items.Count];
            var field_symbols = type_table.Symbols.Where(s => !(s.Type is FunctionType) && s.Name != "self");
            var arranged_items = new KeyValueLikeExpression[creation.Items.Count];
            bool has_arranged = false;
            foreach(var pair in Enumerable.Range(0, creation.Items.Count)
                                          .Zip(creation.Items, (l, r) => new {Index = l, KeyValueExpression = r})){
                var key_path = pair.KeyValueExpression.KeyExpression as PathExpression;
                if(key_path == null)
                    throw new InvalidOperationException($"{pair.KeyValueExpression.KeyExpression} is not a path expression. Something wrong has occurred.");

                if(type_table.IsForeignType){
                    var value_type = pair.KeyValueExpression.ValueExpression.AcceptWalker(this);
                    arg_types[pair.Index] = value_type.Clone();
                }else{
                    var key = type_table.GetSymbolInTypeChain(key_path.AsIdentifier.Name);
                    if(key == null){
                        throw new ParserException(
                            $"The constructor of the type `{creation.TypePath}` is not be defined to have a field named '{key_path.AsIdentifier.Name}'.",
                            "ES1502",
                            pair.KeyValueExpression.KeyExpression
                        );
                    }


                    var index = 0;
                    field_symbols.Any(fs => {
                        if(fs.Name == key.Name)
                            return true;

                        ++index;
                        return false;
                    });

                    var value_type = pair.KeyValueExpression.ValueExpression.AcceptWalker(this);
                    arg_types[index] = value_type.Clone();
                    arranged_items[index] = (KeyValueLikeExpression)pair.KeyValueExpression.Clone();
                    if(index != pair.Index)
                        has_arranged = true;

                    if(IsCastable(value_type, key.Type) == TriBool.False){
                        parser.ReportSemanticErrorRegional(
                            $"The field '{key_path.AsIdentifier.Name}' expects the value to be of type `{key.Type}`, but it actually is `{value_type}`.",
                            "ES2003",
                            pair.KeyValueExpression.KeyExpression, pair.KeyValueExpression.ValueExpression
                        );
                    }
                }
            }

            if(has_arranged){
                var arranged_creation = new ObjectCreationExpression(creation.TypePath.Clone(), arranged_items,
                                                                     creation.TypeArguments.Select(ta => (KeyValueType)ta.Clone()), creation.EndLocation);
                creation.ReplaceWith(arranged_creation);
                creation = arranged_creation;
            }

            var ctor_name = "constructor";
            var ctor_type = AstType.MakeFunctionType(ctor_name, AstType.MakeSimpleType("tuple"), arg_types);
            // This code is needed
            ctor_type = ExpressoCompilerHelpers.ReplaceKeyValueTypes(ctor_type);
            var ctor_symbol = type_table.GetSymbol(ctor_name, ctor_type);
            if(ctor_symbol == null){
                throw new ParserException(
                    $"There are no constructors in the type `{creation.TypePath}` whose parameter types are ({ExpressoCompilerHelpers.StringifyList(ctor_type.Parameters)}).",
                    "ES2010",
                    creation
                ){
                    HelpObject = ExpressoCompilerHelpers.StringifyList(type_table.GetSymbols(ctor_name))
                };
            }

            var func_ctor_type = (FunctionType)ctor_symbol.Type.Clone();
            if(func_ctor_type.TypeParameters.Any()){
                var type_params = func_ctor_type.TypeParameters;
                AstType[] actual_type_args = null;
                if(type_table.IsForeignType){
                    // TODO: implement it properly(extract type arguments)
                    actual_type_args = arg_types;
                }else{
                    actual_type_args = new AstType[type_params.Count];
                    var finder = new TypeParameterFinderAndReplacer(type_params, arg_types, actual_type_args);
                    foreach(var item in creation.Items){
                        var key_path = (PathExpression)item.KeyExpression;
                        var key = type_table.GetSymbolInTypeChain(key_path.AsIdentifier.Name);
                        key.Type.AcceptTypeWalker(finder, null);
                    }
                }

                if(creation.TypeArguments.Any())
                    CheckTypeArgumentsForObject(creation, creation.TypeArguments, type_params, actual_type_args);
                else
                    throw new Exception("Unreachable");

                ExpressoCompilerHelpers.SubstituteGenericTypes(func_ctor_type, creation.TypeArguments);
            }

            foreach(var param_type in func_ctor_type.Parameters){
                var param_type_symbol = symbols.GetTypeSymbolInAnyScope(param_type.Name);
                if(param_type_symbol != null && param_type.IdentifierNode.Type.IsNull)
                    param_type.IdentifierNode.Type = param_type_symbol.Type.Clone();
            }

            if(!ExpressoCompilerHelpers.IsVoidType(func_ctor_type.ReturnType)){
                var return_type_symbol = symbols.GetTypeSymbolInAnyScope(func_ctor_type.ReturnType.Name);
                if(return_type_symbol != null && func_ctor_type.ReturnType.IdentifierNode.Type.IsNull)
                    func_ctor_type.ReturnType.IdentifierNode.Type = return_type_symbol.Type.Clone();
            }
            creation.CtorType = (FunctionType)func_ctor_type.Clone();
            return creation.TypePath;
        }

        public AstType VisitSequenceInitializer(SequenceInitializer seqInitializer)
        {
            if(seqInitializer.ObjectType.TypeArguments.First() is PlaceholderType)
                inference_runner.VisitSequenceInitializer(seqInitializer);

            // Accepts each item as it replaces placeholder nodes with real type nodes
            // We don't validate the type of each item because inference phase has done that
            foreach(var item in seqInitializer.Items)
                item.AcceptWalker(this);

            return seqInitializer.ObjectType;
        }

        public AstType VisitMatchClause(MatchPatternClause matchClause)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            var clause_type = matchClause.Patterns.First().AcceptWalker(this);
            foreach(var pattern in matchClause.Patterns.Skip(1)){
                var pattern_type = pattern.AcceptWalker(this);
                if(IsCompatibleWith(clause_type, pattern_type) == TriBool.False){
                    throw new ParserException(
                        $"There is a type mismatch in match patterns; Expected: `{clause_type}`, found: `{pattern_type}`.",
                        "ES1101",
                        matchClause
                    );
                }
            }

            matchClause.Guard.AcceptWalker(this);
            matchClause.Body.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
            return clause_type;
        }

        public AstType VisitSequenceExpression(SequenceExpression seqExpr)
        {
            var types = seqExpr.Items.Select(i => i.AcceptWalker(this).Clone());

            return AstType.MakeSimpleType("tuple", types);
        }

        public AstType VisitUnaryExpression(UnaryExpression unaryExpr)
        {
            var type = unaryExpr.Operand.AcceptWalker(this);
            if(type is FunctionType property && property.Name.StartsWith("get_", StringComparison.Ordinal))
                type = property.ReturnType;

            switch(unaryExpr.Operator){
            case OperatorType.Reference:
                if(unaryExpr.Operand is PathExpression path_expr && path_expr.AsIdentifier != null){
                    var symbol = symbols.GetSymbolInAnyScope(path_expr.AsIdentifier.Name);
                    if(symbol != null && symbol.Modifiers.HasFlag(Modifiers.Immutable)){
                        throw new ParserException(
                            "Immutable variables cannot be taken reference.",
                            "ES1904",
                            unaryExpr
                        );
                    }
                }

                return AstType.MakeReferenceType(type.Clone());

            case OperatorType.Plus:
            case OperatorType.Minus:
                {
                    if(ExpressoCompilerHelpers.IsPlaceholderType(type)){
                        var inferred_type = inference_runner.VisitUnaryExpression(unaryExpr);
                        if(inferred_type.IsNull)
                            return inferred_type;

                        type.ReplaceWith(inferred_type);
                        type = inferred_type;

                        if(type is FunctionType property2 && property2.Name.StartsWith("get_", StringComparison.CurrentCulture))
                            type = property2.ReturnType;
                    }

                    var primitive_type = type as PrimitiveType;
                    if(primitive_type == null || type.IsNull || primitive_type.KnownTypeCode == KnownTypeCode.Char || primitive_type.KnownTypeCode == KnownTypeCode.Bool
                       || primitive_type.KnownTypeCode == KnownTypeCode.IntSeq || primitive_type.KnownTypeCode == KnownTypeCode.String){
                        // ES3200 thrown
                        /*parser.ReportSemanticError(
                            $"Can not apply the operator '{unaryExpr.OperatorToken}' on the type `{type.Name}`.",
                            "ES1201",
                            unaryExpr
                        );*/

                        return null;
                    }
                    return type;
                }

            case OperatorType.Not:
                if(!(type is PrimitiveType) || ((PrimitiveType)type).KnownTypeCode != KnownTypeCode.Bool){
                    parser.ReportSemanticError(
                        $"Can not apply the '!' operator on the type `{type}`!",
                        "ES1200",
                        unaryExpr
                    );

                    return null;
                }
                return type;

            default:
                throw new FatalError("Unknown unary operator!");
            }
        }

        public AstType VisitSelfReferenceExpression(SelfReferenceExpression selfRef)
        {
            // We need to infer this here because self can appear by itself
            if(selfRef.SelfIdentifier.Type is PlaceholderType){
                inference_runner.VisitSelfReferenceExpression(selfRef);
                BindTypeName(selfRef.SelfIdentifier.Type.IdentifierNode);
            }

            var type_table = symbols.GetTypeTable(selfRef.SelfIdentifier.Type.Name);
            var next_sibling = selfRef.NextSibling;
            var value_symbol = type_table.GetSymbol(Utilities.RawValueEnumValueFieldName);
            if(type_table != null && type_table.TypeKind == ClassType.Enum && value_symbol != null && (next_sibling == null || next_sibling.NodeType == NodeType.Expression
                                                                                                       || next_sibling.NodeType == NodeType.Statement)){
                // We recognize a self identifier whose type is an enum and which is the last child
                // as referencing the enum value
                var mem_ref = Expression.MakeMemRef(selfRef.Clone(), AstNode.MakeIdentifier(Utilities.RawValueEnumValueFieldName, AstType.MakePlaceholderType()));
                // We leave the Type property of symbol as is because we don't use it
                var symbol = type_table.GetSymbol(Utilities.RawValueEnumValueFieldName);
                mem_ref.Member.IdentifierId = symbol.IdentifierId;
                //
                //mem_ref.Member.Type = selfRef.SelfIdentifier.Type.Clone();

                selfRef.ReplaceWith(mem_ref);
            }

            return selfRef.SelfIdentifier.Type;
        }

        public AstType VisitSuperReferenceExpression(SuperReferenceExpression superRef)
        {
            // superRef.SuperIdentifier is always type-aware at this point because MemberReference infers its target's type
            /*if(superRef.SuperIdentifier.Type is PlaceholderType){
                inference_runner.VisitSuperReferenceExpression(superRef);
                BindTypeName(superRef.SuperIdentifier.Type.IdentifierNode);
            }*/

            return superRef.SuperIdentifier.Type;
        }

        public AstType VisitNullReferenceExpression(NullReferenceExpression nullRef)
        {
            try{
                null_checker.VisitNullReferenceExpression(nullRef);
            }
            catch(InvalidOperationException){
                throw new ParserException(
                    "In Expresso, null literals can only be used in contexts with foreign code, including .NET.",
                    "ES1020",
                    nullRef
                );
            }

            return SimpleType.Null;
        }

        public AstType VisitCommentNode(CommentNode comment)
        {
            return null;
        }

        public AstType VisitTextNode(TextNode textNode)
        {
            return null;
        }

        public AstType VisitTypeConstraint(TypeConstraint constraint)
        {
            foreach(var type_constraint in constraint.TypeConstraints){
                var type_symbol = symbols.GetTypeSymbolInAnyScope(type_constraint.Name);
                if(type_symbol == null){
                    // ES0101 thrown
                    throw new ParserException(
                        $"Cannot find `{type_constraint.Name}`.",
                        "ES1504",
                        constraint,
                        type_constraint
                    );
                }
            }

            return null;
        }

        public AstType VisitPostModifiers(PostModifiers postModifiers)
        {
            return null;
        }

        public AstType VisitSimpleType(SimpleType simpleType)
        {
            // FIXME: Can be deleted: 2019/9/12
            //BindFullyQualifiedTypeName(simpleType.IdentifierToken);
            // If the type arguments contain any unsubstituted ones(placeholder nodes)
            // return the statically defined placeholder type node to indicate that it needs to be inferred
            if(simpleType.TypeArguments.Any(ExpressoCompilerHelpers.IsPlaceholderType))
                return AstType.MakePlaceholderType();
            else
                return simpleType;
        }

        public AstType VisitPrimitiveType(PrimitiveType primitiveType)
        {
            return primitiveType;
        }

        public AstType VisitReferenceType(ReferenceType referenceType)
        {
            return referenceType.BaseType;
        }

        public AstType VisitMemberType(MemberType memberType)
        {
            return null;
        }

        public AstType VisitFunctionType(FunctionType funcType)
        {
            return funcType.ReturnType;
        }

        public AstType VisitParameterType(ParameterType paramType)
        {
            return null;
        }

        public AstType VisitPlaceholderType(PlaceholderType placeholderType)
        {
            return AstType.Null;
        }

        public AstType VisitKeyValueType(KeyValueType keyValueType)
        {
            return null;
        }

        public AstType VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public AstType VisitAttributeSection(AttributeSection section)
        {
            foreach(var attribute in section.Attributes){
                VisitObjectCreationExpression(attribute);

                foreach(var arg in attribute.Items){
                    var value_expr = arg.ValueExpression;
                    var value_type = value_expr.AcceptWalker(this);
                    var value_type_table = symbols.GetTypeTable(!value_type.IdentifierNode.Type.IsNull ? value_type.IdentifierNode.Type.Name : value_type.Name);
                    if(!(value_expr is LiteralExpression) && !(value_expr is MemberReferenceExpression) &&
                       value_type_table != null && value_type_table.TypeKind != ClassType.Enum){
                        throw new ParserException(
                            "This argument is not suitable for attributes.",
                            "ES4022",
                            arg
                        );
                    }
                }
            }

            return null;
        }

        public AstType VisitAliasDeclaration(AliasDeclaration aliasDecl)
        {
            var original_type = aliasDecl.Path.AcceptWalker(this);
            aliasDecl.AliasToken.IdentifierId = original_type.IdentifierNode.IdentifierId;
            return null;
        }

        public AstType VisitImportDeclaration(ImportDeclaration importDecl)
        {
            foreach(var path in importDecl.ImportPaths){
                var colon_index = path.Name.IndexOf("::", StringComparison.CurrentCulture);
                var type_name = path.Name.Substring((colon_index == -1) ? 0 : colon_index + "::".Length);
                var type_table = symbols.GetTypeTable(type_name);
                if(type_table != null && type_table.TypeParameters != null){
                    if(type_table.TypeParameters.Any() && !path.TypeArguments.Any()){
                        throw new ParserException(
                            $"Although the import path `{path.Name}` does not contain any type parameters, the actual type does.",
                            "ES1510",
                            path
                        );
                    }else if(!type_table.TypeParameters.Any() && path.TypeArguments.Any()){
                        throw new ParserException(
                            $"Although the import path `{path.Name}` contains type parameters, the actural type does not.",
                            "ES1511",
                            path
                        );
                    }else if(type_table.TypeParameters.Count != path.TypeArguments.Count) {
                        throw new ParserException(
                            $"The number of type parameters in the import path does not match to that of the actual type.",
                            "ES1512",
                            path
                        );
                    }
                }
            }
            return null;
        }

        public AstType VisitFunctionDeclaration(FunctionDeclaration funcDecl)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            // This code is needed because otherwise we can't define types that point at each other
            if(pre_inspected_funcs.Contains(funcDecl.NameToken.IdentifierId)){
                AscendScope();
                scope_counter = tmp_counter + 1;
                return null;
            }

            if(funcDecl.Parent is TypeDeclaration type_decl){
                foreach(var base_type in type_decl.BaseTypes){
                    var parent_type = symbols.GetTypeTable(base_type.Name);
                    var ident = parent_type.GetSymbol(funcDecl.Name);
                    if(ident != null){
                        if(!(ident.Modifiers & Modifiers.VisibilityMask).Equals(funcDecl.Modifiers & Modifiers.VisibilityMask)){
                            throw new ParserException(
                                $"The parent type's '{funcDecl.Name}' is {ident.Modifiers}, but this type's '{funcDecl.Name}' is {funcDecl.Modifiers}.",
                                "ES1030",
                                funcDecl
                            );
                        }

                        if(!ident.Modifiers.HasFlag(Modifiers.Virtual) && funcDecl.Modifiers.HasFlag(Modifiers.Override)){
                            throw new ParserException(
                                $"The '{funcDecl.Name}' on `{base_type.Name}` has to be virtual.",
                                "ES1032",
                                funcDecl
                            );
                        }
                    }else{
                        if(funcDecl.Modifiers.HasFlag(Modifiers.Override)){
                            throw new ParserException(
                                $"There is no method named '{funcDecl.Name}' to be overridden.",
                                "ES1031",
                                funcDecl
                            );
                        }
                    }
                }
            }

            // We can't use VisitAttributeSection directly here because funcDecl.Attribute can be Null
            funcDecl.Attribute.AcceptWalker(this);

            foreach(var param in funcDecl.Parameters){
                var param_type = param.AcceptWalker(this);
                if(ExpressoCompilerHelpers.IsPlaceholderType(param_type)){
                    param.AcceptWalker(inference_runner);
                }else{
                    if(!param.Option.IsNull){
                        var option_type = param.Option.AcceptWalker(this);
                        if(IsCastable(option_type, param_type) == TriBool.False){
                            throw new ParserException(
                                $"Invalid optional value; `{option_type}` is not compatible with `{param_type}`.",
                                "ES1110",
                                param.NameToken, param.Option
                            ){
                                HelpObject = param.NameToken.Type.Name
                            };
                        }
                    }
                }
            }

            // In case of recursive calls, we'll first discover the function type.
            if(ExpressoCompilerHelpers.IsPlaceholderType(funcDecl.NameToken.Type)){
                var func_type = ExpressoCompilerHelpers.ConvertToFunctionType(funcDecl);
                funcDecl.NameToken.Type.ReplaceWith(func_type);
            }

            if(funcDecl.Name == "main"){
                var next = funcDecl.GetNextNode();
                if(next != null && next is FunctionDeclaration){
                    // abandoned
                    /*parser.ReportSemanticError(
                        "Can not define functions after the main function.",
                        "ES1111",
                        next
                    );*/
                }

                if(is_main_function_defined != null)
                    is_main_function_defined = true;
            }

            VisitBlock(funcDecl.Body);

            if(!ExpressoCompilerHelpers.IsPlaceholderType(funcDecl.ReturnType) && !funcDecl.Body.IsNull){
                var inferred_return_type = inference_runner.VisitFunctionDeclaration(funcDecl);
                if(IsCompatibleWith(funcDecl.ReturnType, inferred_return_type) == TriBool.False){
                    var surrounding_type = funcDecl.Ancestors.OfType<TypeDeclaration>().FirstOrDefault();
                    var name = (surrounding_type != null) ? $"{surrounding_type.Name}.{funcDecl.Name}" : funcDecl.Name;

                    throw new ParserException(
                        $"The specified return type of '{name}' is different from the inferred one; Specified: `{funcDecl.ReturnType}`, inferred: `{inferred_return_type}`.",
                        "ES1610",
                        funcDecl.ReturnType
                    );
                }
            }

            // Delay discovering the return type because the body statements should be type-aware
            // before the return type starts to be inferred
            if(ExpressoCompilerHelpers.IsPlaceholderType(funcDecl.ReturnType)){
                if(funcDecl.Parent is TypeDeclaration type_decl2 && type_decl2.TypeKind == ClassType.Interface){
                    throw new ParserException(
                        $"The method signature '{funcDecl.Name}' in an interface must make the return type explicit.",
                        "ES1602",
                        funcDecl
                    );
                }

                if(funcDecl.Body.Statements.Count == 0){
                    // invalid Stmt thrown
                    throw new ParserException(
                        $"Can not infer the return type of '{funcDecl.Name}' because the body is empty!",
                        "ES1901",
                        funcDecl
                    );
                }
                // Descend scopes 2 times because a function name has its own scope
                int tmp_counter2 = scope_counter;
                --scope_counter;
                DescendScope();
                scope_counter = 0;

                var return_type = inference_runner.VisitFunctionDeclaration(funcDecl);
                funcDecl.ReturnType.ReplaceWith(return_type);
                // Replace the return type of the function type with an appropriate ast type object
                ((FunctionType)funcDecl.NameToken.Type).ReturnType.ReplaceWith(funcDecl.ReturnType.Clone());

                AscendScope();
                scope_counter = tmp_counter2;
            }

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitTypeDeclaration(TypeDeclaration typeDecl)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            // We can't use VisitAttributeSection directly here because typeDecl.Attribute can be Null
            typeDecl.Attribute.AcceptWalker(this);

            var require_methods = new List<string>();
            foreach(var super_type in typeDecl.BaseTypes){
                super_type.AcceptWalker(this);
                if(super_type is PrimitiveType || super_type is SimpleType simple && (simple.Name == "vector" || simple.Name == "tuple" || simple.Name == "dictionary"
                                                                                      || simple.Name == "array" || simple.Name == "slice")){
                    throw new ParserException(
                        $"`{super_type.Name}` cannot be derived.",
                        "ES1913",
                        super_type
                    );
                }else if(super_type is SimpleType simple2){
                    var super_type_name = !simple2.IdentifierNode.Type.IsNull ? simple2.IdentifierNode.Type.Name : simple2.Name;
                    var super_type_table = symbols.GetTypeTable(super_type_name);
                    if(super_type_table == null){
                        // ES0101 thrown
                        throw new ParserException(
                            $"The base type `{simple2}` is missing.",
                            "ES1912",
                            simple2
                        );
                    }

                    if(simple2.TypeArguments.Any() && simple2.TypeArguments.All(ta => !(ta is ParameterType))){
                        var type_params = super_type_table.TypeParameters.Zip(simple2.TypeArguments, (l, r) => new {TypeParameter = l, TypeArgument = r})
                                                          .Select(pair => AstType.MakeKeyValueType(pair.TypeParameter.Clone(), pair.TypeArgument.Clone()))
                                                          .ToList();
                        InheritedTypeParameters = type_params;
                    }

                    if(super_type_table.TypeKind == ClassType.Interface){
                        require_methods.AddRange(super_type_table.Symbols.Select(s => s.Name));

                        foreach(var method_symbol in super_type_table.Symbols){
                            // Inherit in post modifiers from the parent
                            foreach(var inside in method_symbol.PostModifiers.RequiresToBeCalledInside){
                                var func_decl = typeDecl.Members.OfType<FunctionDeclaration>().First(fd => fd.Name == method_symbol.Name);
                                var new_inside = AstNode.MakeIdentifier(inside.Name, Modifiers.None, inside.StartLocation);
                                BindName(new_inside);

                                if(func_decl.PostModifiers.IsNull){
                                    var new_post_modifiers = AstNode.MakePostModifiers(
                                        null, null, new List<Identifier>{new_inside}, method_symbol.PostModifiers.StartLocation,
                                        method_symbol.PostModifiers.EndLocation
                                    );

                                    func_decl.PostModifiers = new_post_modifiers;
                                }else{
                                    func_decl.PostModifiers.RequiresToBeCalledInside.Add(new_inside);
                                }
                            }
                        }
                    }else if(super_type_table.TypeKind == ClassType.Class){
                        if(!super_type.IsMatch(typeDecl.BaseTypes.Last())){
                            throw new ParserException(
                                $"The parent class `{super_type}` must be specified in the last position in the base type list.",
                                "ES1011",
                                typeDecl
                            );
                        }
                    }
                }else{
                    throw new ParserException(
                        $"A class cannot be derived from `{super_type.Name}`.",
                        "ES1911",
                        super_type
                    );
                }

                var type_symbol = symbols.GetTypeSymbolInAnyScope(super_type.Name);
                if(type_symbol.Modifiers.HasFlag(Modifiers.Sealed)){
                    throw new ParserException(
                        $"Sealed classes cannot be derived: `{super_type.Name}`.",
                        "ES1914",
                        super_type
                    );
                }
            }

            var fields = typeDecl.Members.OfType<FieldDeclaration>();
            foreach(var constraint in typeDecl.TypeConstraints){
                VisitTypeConstraint(constraint);

                var type_param = constraint.TypeParameter;
                if(typeDecl.TypeKind != ClassType.Interface && !fields.Any(fd => fd.Initializers.Any(init => type_param.MatchesParameterType(init.Pattern.Type)))){
                    throw new ParserException(
                        $"At least, class-wide or enum-wide type parameters have to be used on one field: `{constraint.TypeParameter}`.",
                        "ES2202",
                        typeDecl
                    );
                }
            }

            while(require_methods.Contains("self"))
                require_methods.Remove("self");

            while(require_methods.Contains("constructor"))
                require_methods.Remove("constructor");

            if(typeDecl.TypeKind == ClassType.Enum)
                seen_enum_members = new List<int>();
            
            foreach(var member in typeDecl.Members){
                if(member is FunctionDeclaration method)
                    require_methods.Remove(method.Name);
                
                member.AcceptWalker(this);
            }

            if(typeDecl.TypeKind == ClassType.Enum)
                seen_enum_members = null;

            if(require_methods.Any() && typeDecl.TypeKind == ClassType.Class){
                foreach(var require_method_name in require_methods){
                    parser.ReportSemanticError(
                        $"The class `{typeDecl.Name}` does not implement a method '{require_method_name}' but an interface requires you to implement it.",
                        "ES1910",
                        typeDecl
                    );
                }
            }

            if(InheritedTypeParameters != null)
                InheritedTypeParameters = null;

            AscendScope();
            scope_counter = tmp_counter + 1;
            return null;
        }

        public AstType VisitFieldDeclaration(FieldDeclaration fieldDecl)
        {
            // We can't use VisitAttributeSection directly here because fieldDecl.Attribute can be Null
            fieldDecl.Attribute.AcceptWalker(this);

            foreach(var init in fieldDecl.Initializers){
                var field_type = VisitVariableInitializer(init);
                if(ExpressoCompilerHelpers.IsPlaceholderType(field_type)){
                    var inferred_type = inference_runner.VisitVariableInitializer(init);
                    field_type.ReplaceWith(inferred_type);
                }else{
                    if(!init.Initializer.IsNull){
                        if(seen_enum_members != null && init.Initializer is LiteralExpression literal){
                            var value = (int)literal.Value;
                            if(seen_enum_members.Any(seen => seen == value)){
                                throw new ParserException(
                                    $"A descriminant value `{value}` already exists.",
                                    "ES4030",
                                    fieldDecl
                                );
                            }
                            seen_enum_members.Add(value);

                            if(first_raw_value_enum_member == null)
                                first_raw_value_enum_member = fieldDecl;
                        }else{
                            if(fieldDecl.Ancestors.Any(a => a is TypeDeclaration)){
                                throw new ParserException(
                                    "Expresso does not allow fields to have default values.",
                                    "ES0110",
                                    init
                                );
                            }

                            var init_type = init.Initializer.AcceptWalker(this);
                            if(IsCastable(init_type, field_type) == TriBool.False){
                                parser.ReportSemanticErrorRegional(
                                    $"Can not initialize the module variable '{init.Pattern}' with a value which is of type `{field_type}`.",
                                    "ES0111",
                                    init.Pattern, init.Initializer
                                );
                            }
                        }
                    }else if(seen_enum_members != null && seen_enum_members.Any()){
                        throw new ParserException(
                            "Descriminator values can only be used with a field-less enum.",
                            "ES4031",
                            first_raw_value_enum_member
                        );
                    }
                }
            }

            return null;
        }

        public AstType VisitParameterDeclaration(ParameterDeclaration parameterDecl)
        {
            // We can't use VisitAttributeSection directly here because parameterDecl.Attribute can be Null
            parameterDecl.Attribute.AcceptWalker(this);
            // Don't check NameToken.Type is a placeholder type node.
            // It is the parent's job to resolve and replace this node with an actual type node.
            return parameterDecl.NameToken.Type;
        }

        // Note on VisitVariableInitializer that it can inspect Initializer more than once
        // so `Initializer`s need to expect that
        public AstType VisitVariableInitializer(VariableInitializer initializer)
        {
            inspecting_lhs_of_vbp = true;
            var lhs_type = VisitPatternWithType(initializer.Pattern);
            inspecting_lhs_of_vbp = false;
            var simple = lhs_type as SimpleType;

            if(ExpressoCompilerHelpers.IsPlaceholderType(lhs_type) || simple != null && ContainsPlaceholderType(simple)){
                inference_runner.InspectsClosure = true;
                var inferred_type = initializer.Initializer.AcceptWalker(inference_runner);
                // This code is needed because other methods could inspect closures
                inference_runner.InspectsClosure = true;
                if(IsCollectionType(inferred_type) && ((SimpleType)inferred_type).TypeArguments.Any(t => t is PlaceholderType)){
                    parser.ReportSemanticErrorRegional(
                        $"Can not infer the inner type of the container `{inferred_type}` because it lacks an initial value.",
                        "ES1310",
                        initializer.Pattern, initializer.Initializer
                    );
                }

                if(simple != null && simple.Name == "tuple" && inferred_type is SimpleType right_simple){
                    // expects IEnumerable<tuple<...>>
                    if(initializer.Parent is ValueBindingForStatement){
                        if(right_simple.Name.StartsWith("IEnumerable", StringComparison.Ordinal))
                            right_simple = right_simple.TypeArguments.First() as SimpleType;
                        else if(right_simple.Name == "dictionary")
                            right_simple = AstType.MakeSimpleType("tuple", right_simple.TypeArguments.Select(ta => ta.Clone()));
                    }

                    var content = (right_simple.Name == "tuple") ? right_simple : right_simple.TypeArguments.First() as SimpleType;

                    if(content == null || content != null && IsCompatibleWith(lhs_type, content) == TriBool.False){
                        throw new ParserException(
                            $"The type `{lhs_type}` on the left hand side is not compatible with `{content ?? right_simple.TypeArguments.First()}` on the right hand side.",
                            "ES1300",
                            initializer.Pattern, initializer.Initializer
                        );
                    }

                    var tuple_pat = (TuplePattern)initializer.Pattern.Pattern;
                    foreach(var pair in tuple_pat.Patterns.OfType<IdentifierPattern>().Zip(content.TypeArguments, (l, r) => new {Pattern = l, AstType = r}))
                        pair.Pattern.Identifier.Type.ReplaceWith(pair.AstType.Clone());
                }

                if(initializer.Parent is ValueBindingForStatement){
                    if(!IsSequenceType(inferred_type)){
                        throw new ParserException(
                            $"`{inferred_type}` is not a sequence type!",
                            "ES1301",
                            initializer.Initializer
                        );
                    }

                    var elem_type = MakeOutElementType(GetIEnumerableType(inferred_type));
                    lhs_type.ReplaceWith(elem_type);
                    lhs_type = elem_type;
                }else{
                    // FIXME: Is this really needed?
                    var type_table = symbols.GetTypeTable(inferred_type.Name);
                    if(type_table != null && type_table.TypeKind == ClassType.Enum && inferred_type.IdentifierNode.Type.Name == "int"){
                        var new_type = AstType.MakeSimpleType(inferred_type.Name, inferred_type.StartLocation);
                        new_type.IdentifierNode.IdentifierId = inferred_type.IdentifierNode.IdentifierId;
                        inferred_type = new_type;
                    }

                    lhs_type.ReplaceWith(inferred_type.Clone());
                    lhs_type = inferred_type;
                }
            }

            var rhs_type = initializer.Initializer.AcceptWalker(this);
            if(inspecting_value_binding_pattern && rhs_type is SimpleType rhs_simple2){
                // rhs_simple2 can be like Option<T = int> (- tuple<int> (- <null>
                cur_context_type = AstType.MakeSimpleType(
                    rhs_type.Name, rhs_simple2.TypeArguments.Select(ta => ta is KeyValueType keyvalue ? keyvalue.ValueType.Clone() : ta.Clone())
                );
                VisitPatternWithType(initializer.Pattern);
            }

            if(initializer.Parent is ValueBindingForStatement)
                rhs_type = MakeOutElementType(GetIEnumerableType(rhs_type));

            if(IsCollectionType(lhs_type) && ContainsPlaceholderType(lhs_type as SimpleType)){
                // The left hand side lacks the types of the contents so infer them from the right hand side
                var rhs_simple = rhs_type as SimpleType;
                foreach(var pair in simple.TypeArguments.Zip(rhs_simple.TypeArguments, (l, r) => new {Lhs = l, Rhs = r}))
                    pair.Lhs.ReplaceWith(pair.Rhs.Clone());
            }else if(rhs_type is SimpleType rhs_simple && IsCollectionType(rhs_type) && ContainsPlaceholderType(rhs_simple)){
                // The right hand side contains some placeholders, so infer them
                foreach(var pair in simple.TypeArguments.Zip(rhs_simple.TypeArguments, (l, r) => new {Lhs = l, Rhs = r}))
                    pair.Rhs.ReplaceWith(pair.Lhs.Clone());
            }else if(rhs_type != null && IsCompatibleWith(lhs_type, rhs_type) == TriBool.False){
                parser.ReportSemanticErrorRegional(
                    $"The type `{lhs_type}` on the left hand side is not compatible with `{rhs_type}` on the right hand side.",
                    "ES1300",
                    initializer.Pattern, initializer.Initializer
                );
            }

            return lhs_type;
        }

        public AstType VisitWildcardPattern(WildcardPattern wildcardPattern)
        {
            return SimpleType.Null;
        }

        public AstType VisitIdentifierPattern(IdentifierPattern identifierPattern)
        {
            if(identifierPattern.Ancestors.Any(a => a is PatternWithType) && !inspecting_value_binding_pattern){
                VisitIdentifier(identifierPattern.Identifier);
                if(identifierPattern.Identifier.Type is SimpleType simple)
                    BindSimpleType(simple);

                return identifierPattern.Identifier.Type;
            }else{
                var type = inference_runner.VisitIdentifierPattern(identifierPattern);
                identifierPattern.Identifier.Type.ReplaceWith(type.Clone());
                identifierPattern.InnerPattern.AcceptWalker(this);
                return type;
            }
        }

        public AstType VisitCollectionPattern(CollectionPattern collectionPattern)
        {
            AstType item_type = null;
            foreach(var item in collectionPattern.Items){
                var type = item.AcceptWalker(this);
                if(item_type == null && !type.IsNull)
                    item_type = type.Clone();
            }

            collectionPattern.CollectionType
                             .TypeArguments
                             .First()
                             .ReplaceWith(item_type);
            return collectionPattern.CollectionType;
        }

        public AstType VisitDestructuringPattern(DestructuringPattern destructuringPattern)
        {
            SymbolTable type_table = null;
            if(destructuringPattern.TypePath is MemberType member){
                type_table = symbols.GetTypeTable(member.Target.Name);
                if(type_table != null && type_table.TypeKind == ClassType.Enum){
                    destructuringPattern.IsEnum = true;
                    var variant_symbol = type_table.GetSymbol(member.MemberName);
                    if(variant_symbol == null){
                        // ES0102 thrown
                        throw new ParserException(
                            $"Enum `{member.Target.Name}` does not have a variant named '{member.MemberName}'.",
                            "ES2401",
                            destructuringPattern
                        );
                    }

                    var variant_type = (SimpleType)variant_symbol.Type.IdentifierNode.Type;
                    var item_count = destructuringPattern.Items.Count;
                    var type_count = variant_type.TypeArguments.Count;
                    if(!destructuringPattern.Items.Any(i => i is IgnoringRestPattern) && type_count != item_count){
                        throw new ParserException(
                            $"This pattern has {Utilities.TreatSimplePlural(item_count, "field")}, but the corresponding tuple variant has {Utilities.TreatSimplePlural(type_count, "field")}.",
                            "ES1810",
                            destructuringPattern
                        );
                    }

                    foreach(var pair in destructuringPattern.Items.Zip(variant_type.TypeArguments, (l, r) => new {Item = l, VariantItemType = r})){
                        var item_type = pair.Item.AcceptWalker(this);
                        // We can't exchange the arguments because item_type can be SimpleType.Null
                        if(IsCompatibleWith(pair.VariantItemType, item_type) == TriBool.False){
                            throw new ParserException(
                                $"Types are mismatched on destructuring pattern; Expected `{pair.VariantItemType}`, found: `{item_type}`.",
                                "ES1811",
                                pair.Item
                            ){
                                HelpObject = $"({ExpressoCompilerHelpers.StringifyList(variant_type.TypeArguments)})"
                            };
                        }
                    }

                    return member.Target;
                }else if(type_table != null && type_table.TypeKind == ClassType.NotType){
                    // type_table represents an external module scope
                    type_table = type_table.GetTypeTable(member.ChildType.Name);
                    if(type_table == null){
                        // ES0101 thrown?
                        throw new Exception("unreachable");
                    }
                }else{
                    // ES0101 thrown
                    throw new ParserException(
                        $"The type `{member.Target.Name}` is missing!",
                        "ES1812",
                        destructuringPattern
                    );
                }
            }else if(destructuringPattern.TypePath is SimpleType simple){
                type_table = symbols.GetTypeTable(simple.Name);
                if(type_table == null){
                    // ES0101 thrown
                    throw new Exception("unreachable");
                }
            }

            var field_names = type_table.Symbols.Where(s => !(s.Type is FunctionType) && s.Name != "self")
                                                .Select(s => s.Name)
                                                .ToList();

            foreach(var item in destructuringPattern.Items){
                var item_type = item.AcceptWalker(this);
               
                var key = (item is KeyValuePattern keyvalue) ? keyvalue.KeyIdentifier :
                          (item is IdentifierPattern ident_pat) ? ident_pat.Identifier : null;
                // if item is WildcardPattern or IgnoreRestPattern, key is null
                if(key == null){
                    if(item is WildcardPattern)
                        field_names.RemoveAt(0);
                    else if(item is IgnoringRestPattern)
                        field_names.Clear();
                    else
                        throw new Exception("Unreachable");

                    continue;
                }

                var field_symbol = type_table.GetSymbol(key.Name);
                if(field_symbol == null){
                    // Duplicate this code because there is times when ES2410 wasn't thrown
                    throw new ParserException(
                        $"The type `{destructuringPattern.TypePath}` does not have a field named '{key.Name}'.",
                        "ES2410",
                        destructuringPattern
                    );
                }

                // We can't exchange the arguments because item_type can be SimpleType.Null
                if(IsCompatibleWith(field_symbol.Type, item_type) == TriBool.False){
                    throw new ParserException(
                        $"Types are mismatched in destructuring pattern; Expected: `{field_symbol.Type}`, found: `{item_type}`.",
                        "ES1820",
                        item
                    );
                }

                field_names.Remove(key.Name);
            }

            if(field_names.Any()){
                throw new ParserException(
                    $"The pattern does not mention {ExpressoCompilerHelpers.StringifyList(field_names)}.",
                    "ES1821",
                    destructuringPattern
                );
            }

            return destructuringPattern.TypePath;
        }

        public AstType VisitTuplePattern(TuplePattern tuplePattern)
        {
            var types = 
                from p in tuplePattern.Patterns
                select p.AcceptWalker(this).Clone();
            // TODO: consider the case that the tuple contains an IgnoringRestPattern
            return AstType.MakeSimpleType("tuple", types, tuplePattern.StartLocation, tuplePattern.EndLocation);
        }

        public AstType VisitExpressionPattern(ExpressionPattern exprPattern)
        {
            return exprPattern.Expression.AcceptWalker(this);
        }

        public AstType VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
        {
            return SimpleType.Null;
        }

        public AstType VisitKeyValuePattern(KeyValuePattern keyValuePattern)
        {
            return keyValuePattern.Value.AcceptWalker(this);
        }

        public AstType VisitPatternWithType(PatternWithType pattern)
        {
            // We need to replace pattern.Type because every AstType nodes that will be replaced
            // has to have a parent
            // tuple nodes has been created on their own
            var type = inference_runner.VisitPatternWithType(pattern);
            // This code is needed because otherwise we can't inspect destructuring patterns in value binding patterns
            if(inspecting_value_binding_pattern && !inspecting_lhs_of_vbp)
                pattern.Pattern.AcceptWalker(this);

            if(pattern.Type is PlaceholderType && type is SimpleType tuple && tuple.Name == "tuple"){
                pattern.Type.ReplaceWith(type.Clone());
                return pattern.Type;
            }else{
                return type;
            }
        }

        public AstType VisitTypePathPattern(TypePathPattern pathPattern)
        {
            if(pathPattern.TypePath is MemberType member_type)
                return member_type.Target;
            else
                throw new InvalidOperationException("A TypePathPattern expects its type path to be a MemberType. Something wrong has occurred.");
        }

        public AstType VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
        {
            inspecting_value_binding_pattern = true;
            var type = VisitVariableInitializer(valueBindingPattern.Initializer);
            inspecting_value_binding_pattern = false;
            return type;
        }

        public AstType VisitNullNode(AstNode nullNode)
        {
            return null;
        }

        public AstType VisitNewLine(NewLineNode newlineNode)
        {
            return null;
        }

        public AstType VisitWhitespace(WhitespaceNode whitespaceNode)
        {
            return null;
        }

        public AstType VisitExpressoTokenNode(ExpressoTokenNode tokenNode)
        {
            return null;
        }

        public AstType VisitPatternPlaceholder(AstNode placeholder, Pattern child)
        {
            return null;
        }

        #endregion

        /// <summary>
        /// In Expresso, there are 3 valid cast cases.
        /// The first is the identity cast. An identity cast casts itself to itself.
        /// The second is the upcast. Upcasts are usually valid as long as the types are derived.
        /// And the third is the downcast. Downcasts are valid if and only if the target type is derived from the current type.
        /// </summary>
        /// <returns><c>true</c> if <c>fromType</c> can be casted to <c>totype</c>; otherwise, <c>false</c>.</returns>
        TriBool IsCastable(AstType fromType, AstType toType)
        {
            if(fromType.Name == toType.Name){
                return TriBool.True;
            }else if(fromType is PrimitiveType primitive1 && toType is PrimitiveType primitive2){
                if(primitive1.KnownTypeCode == KnownTypeCode.Int && (primitive2.KnownTypeCode == KnownTypeCode.Float || primitive2.KnownTypeCode == KnownTypeCode.Double
                    || primitive2.KnownTypeCode == KnownTypeCode.Char || primitive2.KnownTypeCode == KnownTypeCode.BigInteger || primitive2.KnownTypeCode == KnownTypeCode.Byte
                    || primitive2.KnownTypeCode == KnownTypeCode.UInt))
                    return TriBool.True;
                else if(primitive1.KnownTypeCode == KnownTypeCode.UInt && (primitive2.KnownTypeCode == KnownTypeCode.Float || primitive2.KnownTypeCode == KnownTypeCode.Double
                        || primitive2.KnownTypeCode == KnownTypeCode.Char || primitive2.KnownTypeCode == KnownTypeCode.BigInteger || primitive2.KnownTypeCode == KnownTypeCode.Int
                        || primitive2.KnownTypeCode == KnownTypeCode.Byte))
                    return TriBool.True;
                else if(primitive1.KnownTypeCode == KnownTypeCode.Float && (primitive2.KnownTypeCode == KnownTypeCode.Double || primitive2.KnownTypeCode == KnownTypeCode.Int
                        || primitive2.KnownTypeCode == KnownTypeCode.UInt || primitive2.KnownTypeCode == KnownTypeCode.Char || primitive2.KnownTypeCode == KnownTypeCode.Byte
                        || primitive2.KnownTypeCode == KnownTypeCode.BigInteger))
                    return TriBool.True;
                else if(primitive1.KnownTypeCode == KnownTypeCode.Double && (primitive2.KnownTypeCode == KnownTypeCode.Float || primitive2.KnownTypeCode == KnownTypeCode.Int
                        || primitive2.KnownTypeCode == KnownTypeCode.UInt || primitive2.KnownTypeCode == KnownTypeCode.Char || primitive2.KnownTypeCode == KnownTypeCode.Byte
                        || primitive2.KnownTypeCode == KnownTypeCode.BigInteger))
                    return TriBool.True;
                else if(primitive1.KnownTypeCode == KnownTypeCode.Byte && (primitive2.KnownTypeCode == KnownTypeCode.Int || primitive2.KnownTypeCode == KnownTypeCode.UInt))
                    return TriBool.True;
                else if(primitive1.KnownTypeCode == KnownTypeCode.Char && (primitive2.KnownTypeCode == KnownTypeCode.Int || primitive2.KnownTypeCode == KnownTypeCode.UInt))
                    return TriBool.True;
                else
                    return TriBool.False;
            }else if(fromType is PrimitiveType primitive3 && toType is SimpleType simple){
                if(simple.Name.StartsWith("Int", StringComparison.CurrentCulture) || simple.Name.StartsWith("UInt", StringComparison.CurrentCulture)){
                    if(primitive3.KnownTypeCode == KnownTypeCode.Int && simple.Name.StartsWith("Int", StringComparison.CurrentCulture))
                        return TriBool.True;
                    else if(primitive3.KnownTypeCode == KnownTypeCode.UInt && simple.Name.StartsWith("UInt", StringComparison.CurrentCulture))
                        return TriBool.True;
                    else
                        return TriBool.False;
                }

                var from_table = symbols.GetTypeTable(primitive3.Name);
                if(from_table.CanUpcast(simple))
                    return TriBool.True;
                else
                    return TriBool.False;
            }else if(fromType is SimpleType simple2 && toType is PrimitiveType primitive4){
                if(simple2.Name.StartsWith("Int", StringComparison.CurrentCulture) || simple2.Name.StartsWith("UInt", StringComparison.CurrentCulture)){
                    if(primitive4.KnownTypeCode == KnownTypeCode.Int && simple2.Name.StartsWith("Int", StringComparison.CurrentCulture))
                        return TriBool.True;
                    else if(primitive4.KnownTypeCode == KnownTypeCode.UInt && simple2.Name.StartsWith("UInt", StringComparison.CurrentCulture))
                        return TriBool.True;
                    else
                        return TriBool.False;
                }

                return TriBool.False;
            }else{
                return IsCompatibleWith(fromType, toType);
            }
        }

        /// <summary>
        /// Determines whether <c>first</c> is compatible with the specified <c>second</c>.
        /// </summary>
        /// <returns><c>true</c> if first is compatible with the specified second; otherwise, <c>false</c>.</returns>
        /// <param name="first">First.</param>
        /// <param name="second">Second.</param>
        TriBool IsCompatibleWith(AstType first, AstType second)
        {
            if(first == null)
                throw new ArgumentNullException(nameof(first));
            if(second == null)
                throw new ArgumentNullException(nameof(second));

            if(first is PrimitiveType primitive1 && second is PrimitiveType primitive2){
                if(primitive1.IsMatch(primitive2)){
                    return TriBool.True;
                }else{
                    return TriBool.False;
                }
            }

            var simple1 = first as SimpleType;
            var simple2 = second as SimpleType;
            if(simple1 != null && !simple1.IsNull && simple1.Name.ToLower() == "object")
                return TriBool.True;

            if(simple2 != null && simple2.IsNull){
                // This indicates that the right hand side represents, say, the wildcard pattern
                return TriBool.True;
            }

            if(simple1 != null && !simple1.IsNull && simple2 != null){
                if(simple1.Name == "tuple" && simple2.Name == "tuple" && simple1.TypeArguments.Count > simple2.TypeArguments.Count)
                    return TriBool.True;
                // When they have other types
                if(simple1.Name == simple2.Name)
                    return TriBool.Intermmediate;

                var from_name = !simple1.IdentifierNode.Type.IsNull ? simple1.IdentifierNode.Type.Name : simple1.Name;
                var from_table = symbols.GetTypeTable(from_name);
                // See if we can upcast to simple2
                if(from_table != null && from_table.CanUpcast(simple2))
                    return TriBool.True;

                var to_name = !simple2.IdentifierNode.Type.IsNull ? simple2.IdentifierNode.Type.Name : simple2.Name;
                var to_table = symbols.GetTypeTable(to_name);
                // See if we can downcast to simple2
                if(to_table != null && to_table.CanUpcast(simple1))
                    return TriBool.True;

                //if(simple1 != null && !simple1.IdentifierNode.Type.IsNull && simple2 != null && !simple2.IdentifierNode.Type.IsNull)
                //    return IsCompatibleWith(simple1.IdentifierNode.Type, simple2.IdentifierNode.Type);

                //if(simple2 != null && !simple2.IdentifierNode.Type.IsNull)
                //    return IsCompatibleWith(first, simple2.IdentifierNode.Type);

                if (simple1.Name != simple2.Name || simple1.TypeArguments.Count != simple2.TypeArguments.Count)
                    return TriBool.False;

                foreach(var pair in simple1.TypeArguments.Zip(simple2.TypeArguments, (l, r) => new {Lhs = l, Rhs = r})){
                    if(IsCompatibleWith(pair.Lhs, pair.Rhs) == TriBool.False)
                        return TriBool.False;
                }

                return TriBool.True;
            }

            if(simple1 != null){
                if(second is MemberType member){
                    if(simple1.Name == member.MemberName){
                        return TriBool.True;
                    }
                }
            }

            if(first is MemberType member1 && second is MemberType member2){
                if(member1.IsMatch(member2))
                    return TriBool.True;
                else
                    return TriBool.False;
            }

            if(first is FunctionType func1 && second is FunctionType func2){
                if(func1.IsMatch(func2))
                    return TriBool.True;
                else
                    return TriBool.False;
            }

            if(first is ReferenceType ref1){
                if(second is ReferenceType ref2){
                    if(ref1.BaseType.IsMatch(ref2.BaseType))
                        return TriBool.True;
                }else{
                    return IsCompatibleWith(ref1.BaseType, second);
                }
            }

            if(first is KeyValueType keyvalue1 && second is KeyValueType keyvalue2){
                if(keyvalue1.IsMatch(keyvalue2))
                    return TriBool.True;
            }

            if(first is ParameterType || second is ParameterType)
                return TriBool.True;

            if(first is FunctionParameterType func_param)
                return IsCompatibleWith(func_param.Type, second);

            if(first is PlaceholderType)
                return TriBool.True;

            return TriBool.False;
        }

        /// <summary>
        /// Given 2 expressions, it tries to figure out the most common type.
        /// </summary>
        /// <returns>The common type between `lhs` and `rhs`.</returns>
        AstType FigureOutCommonType(AstType lhs, AstType rhs)
        {
            if(lhs == null)
                throw new ArgumentNullException(nameof(lhs));

            if(rhs == null)
                throw new ArgumentNullException(nameof(rhs));
            
            if(lhs.IsNull)
                return rhs;

            if(rhs.IsNull)
                return lhs;

            if(lhs is PrimitiveType primitive1 && rhs is PrimitiveType primitive2){
                // If both are primitives, check first if both are exactly the same type
                if(primitive1.KnownTypeCode == primitive2.KnownTypeCode){
                    return primitive1;
                }else if(IsNumericalType(primitive1) && IsNumericalType(primitive2)){
                    // If not, then check if both are numeric types or not
                    if(primitive1.KnownTypeCode == KnownTypeCode.Double || primitive2.KnownTypeCode == KnownTypeCode.Double)
                        return AstType.MakePrimitiveType("double");
                    else if(primitive1.KnownTypeCode == KnownTypeCode.Float || primitive2.KnownTypeCode == KnownTypeCode.Float)
                        return AstType.MakePrimitiveType("float");
                    else if(primitive1.KnownTypeCode == KnownTypeCode.BigInteger || primitive2.KnownTypeCode == KnownTypeCode.BigInteger)
                        return AstType.Null;
                    else if(primitive1.KnownTypeCode == KnownTypeCode.Byte)
                        return primitive2;
                    else if(primitive2.KnownTypeCode == KnownTypeCode.Byte)
                        return primitive1;
                    else if(primitive1.KnownTypeCode == KnownTypeCode.UInt || primitive2.KnownTypeCode == KnownTypeCode.UInt)
                        return AstType.MakePrimitiveType("uint");
                    else
                        return AstType.MakePrimitiveType("int");
                }else{
                    // If both aren't the case, then we must say there is no common types between these 2 expressions
                    parser.ReportWarning(
                        $"Can not guess the common type between `{lhs}` and `{rhs}`.",
                        "ES1202",
                        lhs.Parent
                    );

                    return AstType.Null;
                }
            }

            if(lhs is SimpleType simple1 && rhs is SimpleType simple2){
                if(simple1.Name != "tuple" && simple1.Name == simple2.Name || simple1.IsMatch(simple2))
                    return simple1;

                var first_type_table = symbols.GetTypeTable(simple1.Name);
                if(first_type_table != null && first_type_table.CanUpcast(simple2))
                    return simple2;

                var second_type_table = symbols.GetTypeTable(simple2.Name);
                if(second_type_table != null && second_type_table.CanUpcast(simple1))
                    return simple1;
            }

            if(lhs is SimpleType simple && !simple.IdentifierNode.Type.IsNull)
                return FigureOutCommonType(simple.IdentifierNode.Type, rhs);

            if(lhs is ParameterType lhs_param){
                if(rhs is ParameterType param2){
                    if(lhs_param.Name == param2.Name)
                        return lhs_param;
                }

                return lhs_param;
            }

            if(rhs is ParameterType rhs_param)
                return rhs_param;

            parser.ReportWarning(
                $"Can not guess the common type between `{lhs}` and `{rhs}`.",
                "ES1202",
                lhs.Parent
            );

            return AstType.Null;
        }

        SimpleType GetIEnumerableType(AstType type)
        {
            if(type is SimpleType ienumerable && ienumerable.Name.EndsWith("IEnumerable", StringComparison.CurrentCulture))
                return ienumerable;
            else if(type is SimpleType dictionary && dictionary.Name == "dictionary")
                return ExpressoCompilerHelpers.CreateDictionaryAdaptorEnumerableExpressoType(dictionary.TypeArguments);
            else if(type is SimpleType array && array.Name == "array")
                return ExpressoCompilerHelpers.CreateArrayEnumerableExpressoType(array.TypeArguments);
            else if(type is SimpleType slice && slice.Name == "slice")
                return ExpressoCompilerHelpers.CreateSliceEnumerableExpressoType(slice.TypeArguments);

            var name = (type is SimpleType simple && !simple.IdentifierNode.Type.IsNull) ? simple.IdentifierNode.Type.Name : type.Name;
            var table = symbols.GetTypeTable(name);
            var simple_type = table.IsSubclassOf(AstType.MakeSimpleType("System.Collections.Generic.IEnumerable"));

            // TODO: take into account cases such as SomeClass<T, U>: IEnumerable<U>
            if(simple_type != null && type is SimpleType simple2 && simple2.TypeArguments.Count == 1){
                simple_type.TypeArguments.First().ReplaceWith(simple2.TypeArguments.First().Clone());
                return simple_type;
            }else if(simple_type != null){
                return simple_type;
            }else{
                return null;
            }
        }

        AstType ResolveType(AstType type)
        {
            // Functions that only throw will return a SimpleType.Null
            if(type.IsNull)
                return AstType.MakeSimpleType("tuple");
            else if(!type.IdentifierNode.Type.IsNull || !(type is SimpleType))
                return type;

            var type_symbol = symbols.GetTypeSymbolInAnyScope(type.Name);
            ExpressoCompilerHelpers.ResolveTypeAlias((SimpleType)type, type_symbol);

            return type;
        }

        void CheckDefaultValues(CallExpression call, FunctionType functionType)
        {
            foreach(var param in functionType.Parameters.Skip(call.Arguments.Count)){
                if(param is FunctionParameterType func_param_type){
                    if(func_param_type.Type is SimpleType && func_param_type.Modifiers.HasFlag(FunctionParameterModifiers.Optional)){
                        throw new ParserException(
                            "Default parameters on object types can not be used in Expresso.",
                            "ES4500",
                            call
                        );
                    }else if(func_param_type.Type.Name == "string" && func_param_type.Modifiers.HasFlag(FunctionParameterModifiers.Optional)
                        && func_param_type.DefaultValue == null){
                        throw new ParserException(
                            "'null' can not be specified as a default parameter in foreign code.",
                            "ES1021",
                            call
                        );
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether `type` is a number type.
        /// </summary>
        /// <returns><c>true</c> if `type` is a number type; otherwise, <c>false</c>.</returns>
        /// <param name="type">Type.</param>
        static bool IsNumericalType(AstType type)
        {
            return type.Name == "int" || type.Name == "uint" || type.Name == "float" || type.Name == "double" || type.Name == "bigint" || type.Name == "byte";
        }

        /// <summary>
        /// Determines whether `type` a small integer type.
        /// </summary>
        /// <returns><c>true</c>, if `type` is a small integer type, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        static bool IsSmallIntegerType(AstType type)
        {
            return type.Name == "int";
        }

        /// <summary>
        /// Determines whether `type` is a collection type.
        /// </summary>
        /// <returns><c>true</c>, if `type` is a collection type, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        static bool IsCollectionType(AstType type)
        {
            if(type is SimpleType simple)
                return simple.Identifier == "array" || simple.Identifier == "vector" || simple.Identifier == "dictionary" || simple.Identifier == "tuple" || simple.Identifier == "slice";
            else
                return false;
        }

        /// <summary>
        /// Determines whether the `type` represents the tuple type
        /// </summary>
        /// <returns><c>true</c>, if `type` represents the tuple type, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        static bool IsTupleType(AstType type)
        {
            if(type is SimpleType simple)
                return simple.Identifier == "tuple";
            else
                return false;
        }

        /// <summary>
        /// Determines whether the `type` represents a container type.
        /// </summary>
        /// <returns><c>true</c>, if `type` represents a container type, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        static bool IsContainerType(AstType type)
        {
            if(type is SimpleType simple)
                return simple.Identifier == "array" || simple.Identifier == "vector";
            else
                return false;
        }

        /// <summary>
        /// Determines whether the `type` represents the dictionary type.
        /// </summary>
        /// <returns><c>true</c>, if `type` represents the dictionary type, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        static bool IsDictionaryType(AstType type)
        {
            if(type is SimpleType simple)
                return simple.Identifier == "dictionary";
            else
                return false;
        }

        /// <summary>
        /// Determines whether `type` is a container type and whether it contains a placeholder type.
        /// </summary>
        /// <returns><c>true</c>, if placeholder type was contained, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        static bool ContainsPlaceholderType(SimpleType type)
        {
            if(type == null || !IsCollectionType(type))
                return false;
            
            return type.TypeArguments.Any(ExpressoCompilerHelpers.IsPlaceholderType);
        }

        bool CanIndexType(SimpleType simple)
        {
            if(simple.Identifier == "array" || simple.Identifier == "vector" || simple.Identifier == "dictionary")
                return true;

            var name = !simple.IdentifierNode.Type.IsNull ? simple.IdentifierNode.Type.Name : simple.Name;
            var table = symbols.GetTypeTable(name);
            if(table == null){
                throw new ParserException(
                    $"The type symbol '{name}' turns out not to be declared or accessible in the current scope {symbols.Name}!",
                    "ES0101",
                    simple
                );
            }

            if(table.IsSubclassOf(AstType.MakeSimpleType("System.Collections.Generic.ICollection")) != null)
                return true;
            else
                return false;
        }

        bool IsSequenceType(AstType type)
        {
            var name = (type is SimpleType simple && !simple.IdentifierNode.Type.IsNull) ? simple.IdentifierNode.Type.Name : type.Name;
            var table = symbols.GetTypeTable(name);
            if(table == null){
                throw new ParserException(
                    $"The type symbol '{name}' turns out not to be declared or accessible in the current scope {symbols.Name}!",
                    "ES0101",
                    type
                );
            }

            if(table.IsSubclassOf(AstType.MakeSimpleType("System.Collections.Generic.IEnumerable")) != null)
                return true;
            else if(type is SimpleType simple2 && simple2.Name.StartsWith("IEnumerable", StringComparison.CurrentCulture))
                return true;
            else
                return IsCollectionType(type);
        }

        static AstType MakeOutElementType(AstType type)
        {
            if(type is PrimitiveType primitive){
                if(primitive.KnownTypeCode == KnownTypeCode.IntSeq)
                    return AstType.MakePrimitiveType("int");
                if(primitive.KnownTypeCode == KnownTypeCode.String)
                    return AstType.MakePrimitiveType("char");
            }

            if(type is SimpleType simple){
                if(simple.Identifier == "slice")
                    return simple.TypeArguments.Last().Clone();
                else if(simple.TypeArguments.Count == 1)
                    return simple.TypeArguments.FirstOrNullObject().Clone();
                else
                    return AstType.MakeSimpleType("tuple", simple.TypeArguments.Select(ta => ta.Clone()));
            }

            return AstType.Null;
        }

        static int MakeOutIntFromIntSeq(Expression expr)
        {
            if(expr is UnaryExpression unary){
                var tmp = (int)((LiteralExpression)unary.Operand).Value;
                if(unary.Operator == OperatorType.Minus)
                    tmp = -tmp;

                return tmp;
            }

            return (int)((LiteralExpression)expr).Value;
        }

        // FIXME: Can be deleted: 2019/9/12
        /*void BindFullyQualifiedTypeName(Identifier ident)
        {
            var referenced = symbols.GetTypeSymbolInAnyScope(ident.Name);
            if(referenced != null && !referenced.Type.IsNull){
                ident.Type = referenced.Type.Clone();
                BindSimpleType((SimpleType)ident.Type);
            }
        }*/

        void CheckTypeArgumentsForObject(AstNode node, IEnumerable<KeyValueType> typeArgs, IEnumerable<ParameterType> typeParams, IEnumerable<AstType> argTypes)
        {
            var param_count = typeParams.Count();
            var arg_count = typeArgs.Count();
            if(arg_count < param_count){
                throw new ParserException(
                    $"There are too few type arguments provided: expected {param_count} arguments, {arg_count} provided.",
                    "ES2020",
                    node
                );
            }else if(arg_count > param_count){
                throw new ParserException(
                    $"There are too many type arguments provided: expected {param_count} arguments, {arg_count} provided.",
                    "ES2021",
                    node
                );
            }

            foreach(var pair in argTypes.Zip(typeParams, (l, r) => new {ArgumentType = l, ParameterType = r})){
                var type_arg = typeArgs.First(ta => ta.KeyType.IsMatch(pair.ParameterType));
                if(type_arg.ValueType.IsNull || ExpressoCompilerHelpers.IsPlaceholderType(type_arg.ValueType)){
                    throw new ParserException(
                        $"A necessary type argument `{pair.ParameterType}` is missing!",
                        "ES2023",
                        node
                    );
                }

                if(!type_arg.ValueType.IsMatch(pair.ArgumentType)){
                    throw new ParserException(
                        $"Mismatched types found in the type arguments and arguments! Expected: `{type_arg.ValueType}`, found: `{pair.ArgumentType}`.",
                        "ES2022",
                        node
                    );
                }
            }
        }

        void CheckTypeArgumentsForEnum(AstNode node, IEnumerable<KeyValueType> typeArgs, IEnumerable<ParameterType> typeParams, IEnumerable<AstType> argTypes,
                                       IEnumerable<AstType> variantElementTypes)
        {
            var param_count = typeParams.Count();
            var arg_count = typeArgs.Count();
            if(arg_count < param_count){
                throw new ParserException(
                    $"There are too few type arguments provided: expected {param_count} arguments, {arg_count} provided.",
                    "ES2020",
                    node
                );
            }else if(arg_count > param_count){
                throw new ParserException(
                    $"There are too many type arguments provided: expected {param_count} arguments, {arg_count} provided.",
                    "ES2021",
                    node
                );
            }

            foreach(var triple in argTypes.Zip(typeParams, (l, r) => new {A = l, P = r})
                                          .Zip(variantElementTypes, (l, r) => new {ArgumentType = l.A, ParameterType = l.P, VariantElementType = r})){
                var type_arg = typeArgs.First(ta => ta.KeyType.IsMatch(triple.VariantElementType));
                if(type_arg.ValueType.IsNull || ExpressoCompilerHelpers.IsPlaceholderType(type_arg.ValueType)){
                    throw new ParserException(
                        $"A necessary type argument `{triple.ParameterType}` is missing!",
                        "ES2023",
                        node
                    );
                }

                if(!type_arg.ValueType.IsMatch(triple.ArgumentType)){
                    throw new ParserException(
                        $"Mismatched types found in type arguments and arguments! Expected: `{type_arg}`, found: `{triple.ArgumentType}`.",
                        "ES2022",
                        node
                    );
                }
            }
        }

        void BindName(Identifier ident)
        {
            var symbol = symbols.GetSymbolInAnyScope(ident.Name);
            if(symbol != null)
                ident.IdentifierId = symbol.IdentifierId;

            //if(!ident.Type.IdentifierNode.Type.IsNull)
            //    BindTypeName(ident.Type.IdentifierNode.Type.IdentifierNode);

            if(ident.IdentifierId == 0u){
                if(symbol != null){
                    // never reached?
                    parser.ReportSemanticError(
                        $"You cannot use the type symbol '{ident.Name}' before defined!",
                        "ES0121",
                        ident
                    );
                }else{
                    parser.ReportSemanticError(
                        $"The type symbol '{ident.Name}' turns out not to be declared or accessible in the current scope {symbols.Name}!",
                        "ES0101",
                        ident
                    );
                }
            }
        }

        void BindTypeName(Identifier ident)
        {
            var symbol = symbols.GetTypeSymbolInAnyScope(ident.Name);
            if(symbol != null)
                ident.IdentifierId = symbol.IdentifierId;

            //if(!ident.Type.IdentifierNode.Type.IsNull)
            //    BindTypeName(ident.Type.IdentifierNode.Type.IdentifierNode);

            if(ident.IdentifierId == 0u){
                if(symbol != null){
                    // never reached?
                    parser.ReportSemanticError(
                        $"You cannot use the type symbol '{ident.Name}' before defined!",
                        "ES0121",
                        ident
                    );
                }else{
                    parser.ReportSemanticError(
                        $"The type symbol '{ident.Name}' turns out not to be declared or accessible in the current scope {symbols.Name}!",
                        "ES0101",
                        ident
                    );
                }
            }
        }

        void BindSimpleType(SimpleType simple)
        {
            var symbol = symbols.GetTypeSymbolInAnyScope(simple.Name);
            if(symbol != null)
                simple.IdentifierNode.IdentifierId = symbol.IdentifierId;

            if(simple.IdentifierNode.IdentifierId == 0u){
                if(symbol != null){
                    // never reached?
                    parser.ReportSemanticError(
                        $"You cannot use the type symbol '{simple.Name}' before defined!",
                        "ES0121",
                        simple
                    );
                }else{
                    parser.ReportSemanticError(
                        $"The type symbol '{simple.Name}' turns out not to be declared or accessible in the current scope {symbols.Name}!",
                        "ES0101",
                        simple
                    );
                }
            }

            foreach(var type_arg in simple.TypeArguments.OfType<SimpleType>())
                BindSimpleType(type_arg);
        }
    }
}

