﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Expresso.CodeGen;

namespace Expresso.Ast.Analysis
{
    partial class TypeChecker : IAstWalker<AstType>
    {
        /// <summary>
        /// Exhaustiveness checker for match patterns.
        /// </summary>
        public class ExhaustivenessChecker : IAstPatternWalker
        {
            ExhaustivenessCheckerEntity entity;
            string cur_name;
            SymbolTable symbol_table;

            public ExhaustivenessChecker(SymbolTable symbolTable)
            {
                symbol_table = symbolTable;
            }

            static string Format(List<Identifier> elements)
            {
                if(!elements.Any())
                    throw new InvalidOperationException("elements is empty!");

                var first_element = elements.First();
                var last_element = elements.Last();
                if(first_element == last_element){
                    if(first_element.Name == "")
                        return "Missing _ or a variable pattern.";
                    else
                        return $"Missing _ or a variable pattern for '{first_element.Name}'.";
                }

                var builder = new StringBuilder("'" + first_element.Name + "'");
                foreach(var elem in elements.Skip(1).Take(elements.Count - 1)){
                    if(elem != last_element){
                        builder.Append(", '");
                        builder.Append(elem.Name);
                        builder.Append("'");
                    }
                }

                return $"Missing _ or a variable pattern for {builder} and '{last_element.Name}'.";
            }

            static string FormatTuple(List<int> indexes)
            {
                if(!indexes.Any())
                    throw new InvalidOperationException("indexes is empty!");

                var first_index = indexes.First();
                var last_index = indexes.Last();
                if(first_index == last_index)
                    return $"Missing _ or a variable pattern for #{first_index + 1} element of the tuple";

                var builder = new StringBuilder("#" + (first_index + 1).ToString());
                foreach(var index in indexes.Skip(1).Take(indexes.Count - 1)){
                    if(index != last_index){
                        builder.Append(", #");
                        builder.Append((index + 1).ToString());
                    }
                }

                return $"Missing _ or a variable pattern for {builder} and #{last_index + 1} elements of the tuple.";
            }

            public static void Execute(SymbolTable symbolTable, MatchStatement matchStmt, AstType targetType)
            {
                var checker = new ExhaustivenessChecker(symbolTable);
                foreach(var clause in matchStmt.Clauses)
                    clause.AcceptPatternWalker(checker);

                if(checker.entity.TypeName == "tuple" && targetType is SimpleType simple && simple.Name == "tuple"){
                    var indexes = Enumerable.Range(0, simple.TypeArguments.Count).ToList();
                    indexes.RemoveAll(index => checker.entity.Fields.Any(f => f.Name == index.ToString() && (f.IsVariable || f.IsWildcard)));

                    if(indexes.Any() && !checker.entity.Fields.Any(f => string.IsNullOrEmpty(f.Name) && f.IsWildcard)){
                        throw new ParserException(
                            FormatTuple(indexes),
                            "ES0300",
                            matchStmt
                        );
                    }
                }else if(checker.entity.TypeName != ""){
                    var type_table = symbolTable.GetTypeTable(checker.entity.TypeName);

                    var symbols = type_table.Symbols.Where(s => !(s.Type is FunctionType) && !(s.Type is PlaceholderType) && s.Name != "self" 
                                                           && !s.Name.StartsWith(CSharpCompilerHelpers.HiddenMemberPrefix, StringComparison.CurrentCulture))
                                                    .ToList();
                    symbols.RemoveAll(ident => checker.entity.Fields.Any(f => ident.Name == f.Name && (f.IsVariable || f.IsWildcard)));

                    if(type_table.TypeKind != ClassType.Enum && symbols.Any()
                       || type_table.TypeKind == ClassType.Enum && !checker.entity.Fields.Any(f => f.IsWildcard) && symbols.Any()){
                        throw new ParserException(
                            Format(symbols),
                            "ES0300",
                            matchStmt
                        );
                    }
                }else{
                    var field = checker.entity.Fields.FirstOrDefault();
                    if(field == null){
                        throw new ParserException(
                            "Missing _ or a variable pattern.",
                            "ES0300",
                            matchStmt
                        );
                    }
                }
            }

            public void VisitCollectionPattern(CollectionPattern collectionPattern)
            {
            }

            public void VisitDestructuringPattern(DestructuringPattern destructuringPattern)
            {
                if(entity == null){
                    if(destructuringPattern.TypePath is MemberType member_type){
                        var type_table = symbol_table.GetTypeTable(member_type.Target.Name);
                        entity = new ExhaustivenessCheckerEntity{TypeName = (type_table != null) ? member_type.Target.Name : member_type.ChildType.Name};
                    }else if(destructuringPattern.TypePath is SimpleType simple_type){
                        entity = new ExhaustivenessCheckerEntity{TypeName = !simple_type.IdentifierNode.Type.IsNull ? simple_type.IdentifierNode.Type.Name : simple_type.Name};
                    }
                }

                if(destructuringPattern.IsEnum){
                    entity.Fields.Add(new ExhaustivenessCheckerEntityElement{Name = destructuringPattern.TypePath.Name, IsVariable = true, IsWildcard = false});
                }else{
                    foreach(var item in destructuringPattern.Items)
                        item.AcceptPatternWalker(this);
                }
            }

            public void VisitExpressionPattern(ExpressionPattern exprPattern)
            {
                if(entity == null)
                    entity = new ExhaustivenessCheckerEntity{TypeName = ""};
            }

            public void VisitIdentifierPattern(IdentifierPattern identifierPattern)
            {
                if(entity == null)
                    entity = new ExhaustivenessCheckerEntity{TypeName = ""};

                var element = new ExhaustivenessCheckerEntityElement{
                    Name = (entity.TypeName == "tuple") ? cur_name : identifierPattern.Identifier.Name, IsVariable = identifierPattern.InnerPattern.IsNull,
                    IsWildcard = false
                };
                entity.Fields.Add(element);
            }

            public void VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
            {
                var type_table = symbol_table.GetTypeTable(entity.TypeName);
                var symbols = type_table.Symbols.Where(s => !(s.Type is FunctionType) && s.Name != "self");
                foreach(var symbol in symbols){
                    if(!entity.Fields.Any(f => symbol.Name == f.Name))
                        entity.Fields.Add(new ExhaustivenessCheckerEntityElement{Name = symbol.Name, IsVariable = false, IsWildcard = true});
                }
            }

            public void VisitKeyValuePattern(KeyValuePattern keyValuePattern)
            {
                cur_name = keyValuePattern.KeyIdentifier.Name;
                keyValuePattern.Value.AcceptPatternWalker(this);
            }

            public void VisitMatchClause(MatchPatternClause matchClause)
            {
                foreach(var pattern in matchClause.Patterns)
                    pattern.AcceptPatternWalker(this);
            }

            public void VisitTuplePattern(TuplePattern tuplePattern)
            {
                if(entity == null)
                    entity = new ExhaustivenessCheckerEntity{TypeName = "tuple"};

                var parent_cur_name = cur_name;
                foreach(var pair in Enumerable.Range(0, tuplePattern.Patterns.Count).Zip(tuplePattern.Patterns, (l, r) => new {Index = l, Pattern = r})){
                    cur_name = pair.Index.ToString();
                    pair.Pattern.AcceptPatternWalker(this);
                }
                cur_name = parent_cur_name;
            }

            public void VisitTypePathPattern(TypePathPattern pathPattern)
            {
                if(entity == null && pathPattern.TypePath is MemberType member)
                    entity = new ExhaustivenessCheckerEntity{TypeName = member.Target.Name};

                entity.Fields.Add(new ExhaustivenessCheckerEntityElement{Name = pathPattern.TypePath.Name, IsVariable = false, IsWildcard = true});
            }

            public void VisitWildcardPattern(WildcardPattern wildcardPattern)
            {
                if(entity == null)
                    entity = new ExhaustivenessCheckerEntity{TypeName = ""};

                entity.Fields.Add(new ExhaustivenessCheckerEntityElement{Name = cur_name, IsVariable = false, IsWildcard = true});
            }

            public void VisitPatternWithType(PatternWithType pattern)
            {
                throw new InvalidOperationException("Can not work on that node");
            }

            public void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
            {
                // never thrown
                throw new ParserException(
                    "Value binding patterns can not appear in a match clause context.",
                    "ES0301",
                    valueBindingPattern
                );
            }

            public void VisitNullNode(AstNode nullNode) => throw new InvalidOperationException("Can not work on that node");
        }
    }
}
