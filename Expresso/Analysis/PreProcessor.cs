﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Expresso.CodeGen;
using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.PatternMatching;

namespace Expresso.Ast.Analysis
{
    using RegexMatch = System.Text.RegularExpressions.Match;

    /// <summary>
    /// Manages the pre process phase.
    /// During the pre process phase, we will interpolate strings, and resolve import items.
    /// </summary>
    class PreProcessor : IAstWalker
    {
        static Regex FasterInterpolationTargetFinder = new Regex(@"\$\{([^}]+)}", RegexOptions.Compiled);
        static Regex SlowerInterpolationTargetFinder = new Regex(@"\$\{((?:[^{]+|{.+?})+)}", RegexOptions.Compiled);
        Parser parser;
        SymbolTable symbols;

        // We need to track scopes because we could add scopes on discovery of string interpolations.
        int scope_counter;

        PreProcessor(Parser parser)
        {
            this.parser = parser;
            symbols = parser.Symbols;
        }

        void DescendScope()
        {
            symbols = symbols.Children[scope_counter];
        }

        void AscendScope()
        {
            symbols = symbols.Parent;
        }

        #region Public surface
        internal static void PerformPreProcess(ExpressoAst ast, Parser parser)
        {
            var processor = new PreProcessor(parser);

            Debug.Assert(processor.symbols.Name == "programRoot", "symbols should point at 'programRoot' before inspecting the ast in PreProcessor");
            processor.VisitAst(ast);
            Debug.Assert(processor.symbols.Name == "programRoot", "symbols should point at 'programRoot' after inspecting the ast in PreProcessor");
        }
        #endregion

        public void VisitAliasDeclaration(AliasDeclaration aliasDecl)
        {
            // no op
        }

        public void VisitArrayInitializer(ArrayInitializerExpression arrayInitializer)
        {
            // no op
        }

        public void VisitAssignment(AssignmentExpression assignment)
        {
            assignment.Left.AcceptWalker(this);
            assignment.Right.AcceptWalker(this);
        }

        public void VisitAst(ExpressoAst ast)
        {
            Utilities.CompilerOutput.WriteLine($"PreProcessing {ast.ModuleName}...");

            ast.Imports.AcceptWalker(this);
            ast.Declarations.AcceptWalker(this);

            ExpressoCompilerHelpers.EndOfImportedIdentifierIds = UniqueIdGenerator.CurrentId - 1u;
        }

        public void VisitAttributeSection(AttributeSection section)
        {
            VisitIdentifier(section.AttributeTargetToken);
            section.Attributes.AcceptWalker(this);
        }

        public void VisitBinaryExpression(BinaryExpression binaryExpr)
        {
            binaryExpr.Left.AcceptWalker(this);
            binaryExpr.Right.AcceptWalker(this);
        }

        public void VisitBlock(BlockStatement block)
        {
            if(block.IsNull)
                return;

            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            block.Statements.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitBreakStatement(BreakStatement breakStmt)
        {
            // no op
        }

        public void VisitCallExpression(CallExpression callExpr)
        {
            callExpr.Target.AcceptWalker(this);
            callExpr.Arguments.AcceptWalker(this);
        }

        public void VisitCastExpression(CastExpression castExpr)
        {
            castExpr.Target.AcceptWalker(this);
            castExpr.ToType.AcceptWalker(this);
        }

        public void VisitCatchClause(CatchClause catchClause)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            VisitBlock(catchClause.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitClosureLiteralExpression(ClosureLiteralExpression closure)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            closure.Parameters.AcceptWalker(this);
            VisitBlock(closure.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitCollectionPattern(CollectionPattern collectionPattern)
        {
            collectionPattern.Items.AcceptWalker(this);
        }

        public void VisitCommentNode(CommentNode comment)
        {
            // no op
        }

        public void VisitComprehensionExpression(ComprehensionExpression comp)
        {
            VisitComprehensionForClause(comp.Body);
        }

        public void VisitComprehensionForClause(ComprehensionForClause compFor)
        {
            compFor.Left.AcceptWalker(this);
            compFor.Target.AcceptWalker(this);
            compFor.Body.AcceptWalker(this);
        }

        public void VisitComprehensionIfClause(ComprehensionIfClause compIf)
        {
            compIf.Condition.AcceptWalker(this);
            compIf.Body.AcceptWalker(this);
        }

        public void VisitConditionalExpression(ConditionalExpression condExpr)
        {
            condExpr.Condition.AcceptWalker(this);
            condExpr.TrueExpression.AcceptWalker(this);
            condExpr.FalseExpression.AcceptWalker(this);
        }

        public void VisitContinueStatement(ContinueStatement continueStmt)
        {
            // no op
        }

        public void VisitDestructuringPattern(DestructuringPattern destructuringPattern)
        {
            destructuringPattern.TypePath.AcceptWalker(this);
            destructuringPattern.Items.AcceptWalker(this);
        }

        public void VisitDoWhileStatement(DoWhileStatement doWhileStmt)
        {
            VisitWhileStatement(doWhileStmt.Delegator);
        }

        public void VisitEmptyStatement(EmptyStatement emptyStmt)
        {
            // no op
        }

        public void VisitExpressionPattern(ExpressionPattern exprPattern)
        {
            exprPattern.Expression.AcceptWalker(this);
        }

        public void VisitExpressionStatement(ExpressionStatement exprStmt)
        {
            exprStmt.Expression.AcceptWalker(this);
        }

        public void VisitExpressoTokenNode(ExpressoTokenNode tokenNode)
        {
            // no op
        }

        public void VisitFieldDeclaration(FieldDeclaration fieldDecl)
        {
            fieldDecl.Initializers.AcceptWalker(this);
        }

        public void VisitFinallyClause(FinallyClause finallyClause)
        {
            VisitBlock(finallyClause.Body);
        }

        public void VisitForStatement(ForStatement forStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            forStmt.Left.AcceptWalker(this);
            forStmt.Target.AcceptWalker(this);
            VisitBlock(forStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitFunctionDeclaration(FunctionDeclaration funcDecl)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            funcDecl.Parameters.AcceptWalker(this);
            funcDecl.ReturnType.AcceptWalker(this);
            VisitBlock(funcDecl.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitFunctionParameterType(FunctionParameterType parameterType)
        {
            parameterType.Type.AcceptWalker(this);
        }

        public void VisitFunctionType(FunctionType funcType)
        {
            funcType.ReturnType.AcceptWalker(this);
            funcType.Parameters.AcceptWalker(this);
        }

        public void VisitIdentifier(Identifier ident)
        {
            // no op
        }

        public void VisitIdentifierPattern(IdentifierPattern identifierPattern)
        {
            identifierPattern.InnerPattern.AcceptWalker(this);
        }

        public void VisitIfStatement(IfStatement ifStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            ifStmt.Condition.AcceptWalker(this);
            VisitBlock(ifStmt.TrueBlock);
            ifStmt.FalseStatement.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitIgnoringRestPattern(IgnoringRestPattern restPattern)
        {
            // no op
        }

        public void VisitImportDeclaration(ImportDeclaration importDecl)
        {
            // Here's the good place to import names from other files
            // All external names will be imported into the module scope we are currently compiling
            if(!importDecl.TargetFile.IsNull){
                SymbolTable external_table = null;
                var first_import = importDecl.ImportPaths.First();
                if(importDecl.TargetFilePath == "std.exs" || importDecl.TargetFilePath.EndsWith(".exs", StringComparison.CurrentCulture)){
                    var colon_index = first_import.Identifier.IndexOf("::", StringComparison.CurrentCulture);
                    var external_module_name = first_import.Identifier.Substring(0, (colon_index == -1) ? first_import.Identifier.Length : colon_index);
                    if(!Parser.CachedSymbolTables.ContainsKey(external_module_name)){
                        var scanner = (importDecl.TargetFilePath == "std.exs") ? parser.scanner.OpenStdFile() : parser.scanner.OpenChildFile(importDecl.TargetFilePath);
                        var inner_parser = new Parser(scanner);
                        inner_parser.Parse();

                        // In order to avoid recursive imports causing deadlock
                        Parser.CachedSymbolTables.Add(external_module_name, inner_parser.Symbols);

                        // In order for external symbols to be resolved
                        PerformPreProcess(inner_parser.TopmostAst, inner_parser);
                        ExpressoNameBinder.BindAst(inner_parser.TopmostAst, inner_parser);

                        parser.InnerParsers.Add(inner_parser);

                        external_table = inner_parser.Symbols;

                        if(importDecl.Parent is ExpressoAst ast)
                            ast.ExternalModules.Add(inner_parser.TopmostAst);
                    }else{
                        Console.WriteLine($"Skipping <module {external_module_name}>...");
                        external_table = Parser.CachedSymbolTables[external_module_name];
                    }
                }

                var table = importDecl.TargetFilePath.EndsWith(".dll", StringComparison.CurrentCulture)
                    ? ExpressoCompilerHelpers.GetSymbolTableForAssembly(parser.scanner.GetFullPath(importDecl.TargetFilePath), parser.Symbols) : external_table;
                if(!first_import.Name.Contains("::") && !first_import.Name.Contains("."))
                    parser.Symbols.AddExternalSymbols(table, importDecl.AliasTokens.First().Name);
                else
                    parser.Symbols.AddExternalSymbols(table, importDecl.ImportPaths, importDecl.AliasTokens);
            }

            // Make the module name type-aware
            foreach(var pair in importDecl.ImportPaths.Zip(importDecl.AliasTokens, (l, r) => new {ImportPath = l, Alias = r})){
                var type = pair.ImportPath;
                //pair.ImportPath.IdentifierNode.Type = type;
                // These 2 statements are needed because otherwise aliases won't get IdentifierIds
                UniqueIdGenerator.DefineNewId(type.IdentifierToken);

                // Types from the standard library and external dlls should know the real name
                // in order for the compiler to keep track of them.
                if(type.Name.StartsWith("System.", StringComparison.CurrentCulture) || importDecl.TargetFilePath.EndsWith(".dll", StringComparison.CurrentCulture))
                    pair.Alias.Type = type.Clone();

                // These 2 statements are needed because otherwise aliases won't get IdentifierIds
                UniqueIdGenerator.DefineNewId(pair.Alias);
            }
        }

        public void VisitIndexerExpression(IndexerExpression indexExpr)
        {
            indexExpr.Target.AcceptWalker(this);
            indexExpr.Arguments.AcceptWalker(this);
        }

        public void VisitIntegerSequenceExpression(IntegerSequenceExpression intSeq)
        {
            intSeq.Start.AcceptWalker(this);
            intSeq.End.AcceptWalker(this);
            intSeq.Step.AcceptWalker(this);
        }

        public void VisitKeyValueLikeExpression(KeyValueLikeExpression keyValue)
        {
            keyValue.KeyExpression.AcceptWalker(this);
            keyValue.ValueExpression.AcceptWalker(this);
        }

        public void VisitKeyValuePattern(KeyValuePattern keyValuePattern)
        {
            keyValuePattern.Value.AcceptWalker(this);
        }

        public void VisitKeyValueType(KeyValueType keyValueType)
        {
            throw new InvalidOperationException("Can not work on that node");
        }

        public void VisitLiteralExpression(LiteralExpression literal)
        {
            if(literal.Type.Name == "string"){
                var template = (string)literal.Value;
                var (curlybrace_found, interpolation_count) = CheckForCurlyBracesAndCountInterpolations(template);
                if(curlybrace_found && interpolation_count > 9){
                    parser.ReportWarning(
                        "There are too many substrings that need interpolation.",
                        "ES4400",
                        literal
                    );
                }

                var matches = curlybrace_found ? SlowerInterpolationTargetFinder.Matches(template) : FasterInterpolationTargetFinder.Matches(template);

                var exprs = new List<Expression>();
                foreach(RegexMatch match in matches){
                    var groups = match.Groups;
                    var inner_parser = new Parser(
                        new Scanner(new MemoryStream(Encoding.UTF8.GetBytes(groups[1].Value))),
                        // Index + 1 because of '"'
                        new TextLocation(literal.StartLocation.Line, literal.StartLocation.Column + groups[1].Index + 1)
                    );

                    try{
                        var (expr, closure_tables) = inner_parser.ParseExpression();
                        exprs.Add(expr);
                        symbols.AddChildTables(closure_tables);
                    }
                    catch(FatalError){
                        // ParserExpression only throws FatalError. That is, syntax errors.
                        throw new ParserException(
                            "An error occurred while parsing the expression in a string interpolation: syntax error(for more, see the previous errors).",
                            "ES3000",
                            literal
                        );
                    }
                }

                int counter = 0;
                var replaced_string = curlybrace_found ? SlowerInterpolationTargetFinder.Replace(template, match => $"{{{counter++.ToString()}}}")
                    : FasterInterpolationTargetFinder.Replace(template, match => $"{{{counter++.ToString()}}}");
                replaced_string = replaced_string.Replace("$$", "$");
                literal.SetValue(replaced_string, replaced_string);

                if(matches.Count > 0){
                    var call_expr = Expression.MakeCallExpr(
                        Expression.MakeMemRef(
                            Expression.MakePath(
                                AstNode.MakeIdentifier("string", AstType.MakePlaceholderType(), Modifiers.None, null, literal.StartLocation)
                            ),
                            AstNode.MakeIdentifier("Format", AstType.MakePlaceholderType(), Modifiers.None, null, literal.StartLocation)
                        ),
                        new []{literal.Clone()}.Concat(exprs),
                        null,
                        literal.StartLocation
                    );
                    literal.ReplaceWith(call_expr);

                    VisitCallExpression(call_expr);
                }
            }
        }

        public void VisitMatchClause(MatchPatternClause matchClause)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            matchClause.Patterns.AcceptWalker(this);
            matchClause.Body.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitMatchStatement(MatchStatement matchStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            matchStmt.Target.AcceptWalker(this);
            matchStmt.Clauses.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitMemberReference(MemberReferenceExpression memRef)
        {
            memRef.Target.AcceptWalker(this);
        }

        public void VisitMemberType(MemberType memberType)
        {
            memberType.Target.AcceptWalker(this);
        }

        public void VisitNewLine(NewLineNode newlineNode)
        {
            // no op
        }

        public void VisitNullNode(AstNode nullNode)
        {
            // no op
        }

        public void VisitNullReferenceExpression(NullReferenceExpression nullRef)
        {
            // no op
        }

        public void VisitObjectCreationExpression(ObjectCreationExpression creation)
        {
            creation.TypePath.AcceptWalker(this);
            creation.Items.AcceptWalker(this);
        }

        public void VisitParameterDeclaration(ParameterDeclaration parameterDecl)
        {
            parameterDecl.Option.AcceptWalker(this);
        }

        public void VisitParameterType(ParameterType paramType)
        {
            // no op
        }

        public void VisitParenthesizedExpression(ParenthesizedExpression parensExpr)
        {
            parensExpr.Expression.AcceptWalker(this);
        }

        public void VisitPathExpression(PathExpression pathExpr)
        {
            pathExpr.Items.AcceptWalker(this);
        }

        public void VisitPatternPlaceholder(AstNode placeholder, Pattern child)
        {
            // no op
        }

        public void VisitPatternWithType(PatternWithType pattern)
        {
            pattern.Pattern.AcceptWalker(this);
            pattern.Type.AcceptWalker(this);
        }

        public void VisitPlaceholderType(PlaceholderType placeholderType)
        {
            // no op
        }

        public void VisitPostModifiers(PostModifiers postModifiers)
        {
            throw new InvalidOperationException("Can not work on that node!");
        }

        public void VisitPrimitiveType(PrimitiveType primitiveType)
        {
            // no op
        }

        public void VisitReferenceType(ReferenceType referenceType)
        {
            // no op
        }

        public void VisitReturnStatement(ReturnStatement returnStmt)
        {
            returnStmt.Expression.AcceptWalker(this);
        }

        public void VisitSelfReferenceExpression(SelfReferenceExpression selfRef)
        {
            // no op
        }

        public void VisitSequenceExpression(SequenceExpression seqExpr)
        {
            seqExpr.Items.AcceptWalker(this);
        }

        public void VisitSequenceInitializer(SequenceInitializer seqInitializer)
        {
            VisitSimpleType(seqInitializer.ObjectType);
            seqInitializer.Items.AcceptWalker(this);
        }

        public void VisitSimpleType(SimpleType simpleType)
        {
            simpleType.TypeArguments.AcceptWalker(this);
        }

        public void VisitSuperReferenceExpression(SuperReferenceExpression superRef)
        {
            // no op
        }

        public void VisitTextNode(TextNode textNode)
        {
            // no op
        }

        public void VisitThrowStatement(ThrowStatement throwStmt)
        {
            VisitObjectCreationExpression(throwStmt.CreationExpression);
        }

        public void VisitTryStatement(TryStatement tryStmt)
        {
            VisitBlock(tryStmt.EnclosingBlock);
            tryStmt.CatchClauses.AcceptWalker(this);
        }

        public void VisitTuplePattern(TuplePattern tuplePattern)
        {
            tuplePattern.Patterns.AcceptWalker(this);
        }

        public void VisitTypeConstraint(TypeConstraint constraint)
        {
            VisitParameterType(constraint.TypeParameter);
            constraint.TypeConstraints.AcceptWalker(this);
        }

        public void VisitTypeDeclaration(TypeDeclaration typeDecl)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            typeDecl.BaseTypes.AcceptWalker(this);
            typeDecl.Members.AcceptWalker(this);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitTypePathPattern(TypePathPattern pathPattern)
        {
            pathPattern.TypePath.AcceptWalker(this);
        }

        public void VisitUnaryExpression(UnaryExpression unaryExpr)
        {
            unaryExpr.Operand.AcceptWalker(this);
        }

        public void VisitValueBindingForStatement(ValueBindingForStatement valueBindingForStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            valueBindingForStmt.Initializer.AcceptWalker(this);
            VisitBlock(valueBindingForStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern)
        {
            VisitVariableInitializer(valueBindingPattern.Initializer);
        }

        public void VisitVariableDeclarationStatement(VariableDeclarationStatement varDecl)
        {
            varDecl.Variables.AcceptWalker(this);
        }

        public void VisitVariableInitializer(VariableInitializer initializer)
        {
            initializer.Initializer.AcceptWalker(this);
        }

        public void VisitWhileStatement(WhileStatement whileStmt)
        {
            int tmp_counter = scope_counter;
            DescendScope();
            scope_counter = 0;

            whileStmt.Condition.AcceptWalker(this);
            VisitBlock(whileStmt.Body);

            AscendScope();
            scope_counter = tmp_counter + 1;
        }

        public void VisitWhitespace(WhitespaceNode whitespaceNode)
        {
            // no op
        }

        public void VisitWildcardPattern(WildcardPattern wildcardPattern)
        {
            // no op
        }

        public void VisitYieldStatement(YieldStatement yieldStmt)
        {
            yieldStmt.Expression.AcceptWalker(this);
        }

        static (bool, int) CheckForCurlyBracesAndCountInterpolations(string str)
        {
            int interpolation_count = 0;
            bool dollar_found = false, interpolation_found = false, curlybrace_found = false;
            foreach(var ch in str){
                if(ch == '$'){
                    if(!interpolation_found)
                        dollar_found = true;
                }else if(dollar_found && ch == '{'){
                    interpolation_found = true;
                    dollar_found = false;
                    ++interpolation_count;
                }else if(interpolation_found && ch == '{'){
                    curlybrace_found = true;
                }else if(dollar_found && ch != '{'){
                    dollar_found = false;
                }else if(interpolation_found && ch == '}'){
                    interpolation_found = false;
                }
            }

            return (curlybrace_found, interpolation_count);
        }
    }
}
