﻿namespace Expresso.Ast
{
    /// <summary>
    /// An specialized <see cref="IAstWalker"/> for patterns.
    /// </summary>
    public interface IAstPatternWalker
    {
        void VisitMatchClause(MatchPatternClause matchClause);

        void VisitWildcardPattern(WildcardPattern wildcardPattern);
        void VisitIdentifierPattern(IdentifierPattern identifierPattern);
        void VisitCollectionPattern(CollectionPattern collectionPattern);
        void VisitDestructuringPattern(DestructuringPattern destructuringPattern);
        void VisitTuplePattern(TuplePattern tuplePattern);
        void VisitExpressionPattern(ExpressionPattern exprPattern);
        void VisitIgnoringRestPattern(IgnoringRestPattern restPattern);
        void VisitKeyValuePattern(KeyValuePattern keyValuePattern);
        void VisitTypePathPattern(TypePathPattern pathPattern);
        void VisitPatternWithType(PatternWithType pattern);
        void VisitValueBindingPattern(ValueBindingPattern valueBindingPattern);

        void VisitNullNode(AstNode nullNode);
    }

    /// <summary>
    /// An specialized <see cref="IAstWalker{TResult}"/> for patterns.
    /// </summary>
    public interface IAstPatternWalker<TResult>
    {
        TResult VisitMatchClause(MatchPatternClause matchClause);

        TResult VisitWildcardPattern(WildcardPattern wildcardPattern);
        TResult VisitIdentifierPattern(IdentifierPattern identifierPattern);
        TResult VisitCollectionPattern(CollectionPattern collectionPattern);
        TResult VisitDestructuringPattern(DestructuringPattern destructuringPattern);
        TResult VisitTuplePattern(TuplePattern tuplePattern);
        TResult VisitExpressionPattern(ExpressionPattern exprPattern);
        TResult VisitIgnoringRestPattern(IgnoringRestPattern restPattern);
        TResult VisitKeyValuePattern(KeyValuePattern keyValuePattern);
        TResult VisitTypePathPattern(TypePathPattern pathPattern);
        TResult VisitPatternWithType(PatternWithType pattern);
        TResult VisitValueBindingPattern(ValueBindingPattern valueBindingPattern);

        TResult VisitNullNode(AstNode nullNode);
    }
}
