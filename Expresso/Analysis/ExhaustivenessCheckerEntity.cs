﻿using System;
using System.Collections.Generic;

namespace Expresso.Ast.Analysis
{
    /// <summary>
    /// Used in ExhaustivenessChecker for tracing which values and fields were mentioned.
    /// </summary>
    public class ExhaustivenessCheckerEntity
    {
        public string TypeName{
            get; set;
        }

        public List<ExhaustivenessCheckerEntityElement> Fields{
            get;
        } = new List<ExhaustivenessCheckerEntityElement>();
    }

    /// <summary>
    /// Represents an element for ExhuastivenessCheckerEntity.
    /// </summary>
    public class ExhaustivenessCheckerEntityElement
    {
        public string Name{
            get; set;
        }

        public bool IsVariable{
            get; set;
        }

        public bool IsWildcard{
            get; set;
        }
    }
}
