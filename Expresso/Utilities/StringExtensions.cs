﻿using System;
using System.Linq;

namespace Expresso.Utils
{
    /// <summary>
    /// Contains string extension methods.
    /// </summary>
    public static class StringExtensions
    {
        private static string[] Capitals = {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        };

        public static bool StartsWithCapitalLetter(this string str)
        {
            return Capitals.Any(c => str.StartsWith(c, StringComparison.CurrentCulture));
        }
    }
}
