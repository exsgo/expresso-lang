﻿using System;
using System.IO;

namespace Expresso
{
    public static class Utilities
    {
        public const string RawValueEnumValueFieldName = "<>__value";

        public static TextWriter CompilerOutput = Console.Out;

        public static string TreatSimplePlural(int count, string noun)
        {
            return $"{count} {noun}{(count == 1 ? "" : "s")}";
        }
    }
}
