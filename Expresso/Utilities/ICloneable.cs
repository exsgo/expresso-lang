﻿namespace Expresso
{
    /// <summary>
    /// Public interface for a cloneable object.
    /// </summary>
#if LIBRARY
    public interface IGenericCloneable<T>
#else
    public interface ICloneable<T>
#endif
    {
        T Clone();
    }
}
