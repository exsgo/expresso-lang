module main;


import System.Type as Type;

def main()
{
    let some_int = 0;
    let types (- Type[] = [some_int.GetType(), some_int.GetType(), some_int.GetType()];
    println("${types}");
}