module main;


def main()
{
    let series (- int[] = [1..10];

    for let elem in series {
        print("${elem}");
    }

    println("${series}");
}