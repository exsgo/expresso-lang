module main;


def main()
{
    let natural_numbers (- vector<int> = [0, 1, 2, 3, 4, 5, ...];

    println("${natural_numbers}");
}