module main;

interface Addable<T>
{
    def add(rhs (- T) -> T;
}

class IntAdder : Addable<int>
{
    private let lhs (- int;
    
    public def add(rhs (- int) -> int
    {
        return self.lhs + rhs;
    }
}

class DoubleAdder : Addable<double>
{
    private let lhs (- double;
    
    public def add(rhs (- double) -> double
    {
        return self.lhs + rhs;
    }
}

class GenericAdder<T, U>
where T: Addable<U>
{
    private let lhs (- T, rhs (- U;
    
    public def calculate() -> U
    {
        return self.lhs.add(self.rhs);
    }
}

def main()
{
    let inst = GenericAdder{lhs: IntAdder{lhs: 10}, rhs: 1};
    println("int: ${inst.calculate()}");

    let inst2 = GenericAdder{lhs: DoubleAdder{lhs: 10.0}, rhs: 2.0};
    println("double: ${inst2.calculate()}");
}