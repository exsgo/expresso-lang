module main;


import System.{Console, ConsoleColor} as {Console, ConsoleColor};
import System.Threading.Thread as Thread;


let greenDuration = 1_000;
let yellowDuration = 300;
let redDuration = 2_000;

enum TrafficLight
{
    Green(int),
    Yellow(int),
    Red(int)
    
    public def canGo()
    {
        match self {
            TrafficLight::Green{_} => return true;,
            _ => return false;
        }
    }

    public def shouldStop()
    {
        match self {
            TrafficLight::Red{_} => return true;,
            _ => return false;
        }
    }
}

def changeState(trafficLight (- TrafficLight)
{
    let prev_color = Console.ForegroundColor;
    match trafficLight {
        TrafficLight::Green{milliseconds} => {
            Console.ForegroundColor = ConsoleColor.Green;
            println("Green");
            Thread.Sleep(milliseconds);
 
            Console.ForegroundColor = ConsoleColor.Yellow;
            println("Next yellow");
            Console.ForegroundColor = prev_color;
            return TrafficLight::Yellow{0: yellowDuration};
        },
        TrafficLight::Yellow{milliseconds} => {
            Console.ForegroundColor = ConsoleColor.Yellow;
            println("Yellow");
            Thread.Sleep(milliseconds);

            Console.ForegroundColor = ConsoleColor.Red;
            println("Next red");
            Console.ForegroundColor = prev_color;
            return TrafficLight::Red{0: redDuration};
        },
        TrafficLight::Red{milliseconds} => {
            Console.ForegroundColor = ConsoleColor.Red;
            println("Red");
            Thread.Sleep(milliseconds);

            Console.ForegroundColor = ConsoleColor.Green;
            println("Next green");
            Console.ForegroundColor = prev_color;
            return TrafficLight::Green{0: greenDuration};
        }
    }
}

def main()
{
    var traffic_light = TrafficLight::Green{0: greenDuration};

    traffic_light = changeState(traffic_light);
    traffic_light = changeState(traffic_light);
    traffic_light = changeState(traffic_light);
}