module main;


def main()
{
    let ary = [1, 2, 3, 4, 5];
    var i = 0;
    while i < ary.Length {
        println("${ary[i]}");
        i += 1;
    }
}