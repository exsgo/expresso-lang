module main;


import System.Exception as Exception;

def throwException() -> int
{
    throw Exception{message: "thrown"};
}

def main()
{
    try{
        throwException();
    }
    catch e (- Exception {
        println(e.Message);
    }
}