module main;


def main()
{
    var result = 1;
    var i = 0;
    while i < 10 {
        result *= 2;
        i += 1;
    }
    println("${result}");
}