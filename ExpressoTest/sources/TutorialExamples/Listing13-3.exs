module main;



class Stack<T>
{
    private var items (- vector<T>;

    public def push(element (- T)
    {
        self.items.Add(element);
    }

    public def pop()
    {
        let count = self.items.Count;
        let last = self.items[count - 1];
        self.items.RemoveAt(count - 1);
        return last;
    }
}

def main()
{
    var stack = Stack<int>{items: [1, 2, 3, ...]};
    stack.push(4);
    stack.push(5);
    
    let top = stack.pop();
    println("${top}");  //prints 5
    
    var stack2 = Stack<double>{items: [1.0, 2.0, 3.0, ...]};
    stack2.push(4.0);
    stack2.push(5.0);
    
    let top2 = stack2.pop();
    println("${top2}"); //prints 5
}