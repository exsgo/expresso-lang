module main;


import System.Exception as Exception;

def throwException() -> void
{
    throw Exception{message: "thrown"};
}

def main()
{
    try{
        throwException();
    }
    catch e (- Exception {
        println(e.Message);
    }
}