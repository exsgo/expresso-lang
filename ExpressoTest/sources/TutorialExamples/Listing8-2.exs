module main;


import System.Exception as Exception;

def parseZero(value (- string) -> int
{
    if value.Equals("0") {
        return 0;
    }else{
        throw Exception{message: "We can't parse strings other than '0'!"};
    }
}

def main()
{
    let a = parseZero("0");
    println("${a}");
}