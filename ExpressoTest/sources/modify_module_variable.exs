module main;


import reference_other_module::ReferenceClass from "./reference_other_module.exs" as ReferenceClass;
import other_module::ReferencedVariable from "./other_module.exs" as ReferencedVariable;

def main()
{
    println("before: ${ReferencedVariable}");
    ReferenceClass.changeVariable();
    println("after: ${ReferencedVariable}");
}