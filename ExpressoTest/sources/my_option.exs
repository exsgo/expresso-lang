module my_option;


import System.Exception as Exception;

export enum MyOption<T>
{
    Some(T),
    None()

    public def unwrap() -> T
    {
        match self {
            MyOption::Some{value} => return value;,
            MyOption::None{} => throw Exception{message: "option is empty"};
        }
    }
}