module main;


//import System.{Convert, Int64} as {Convert, Int64};
//import System.IO.{Stream, StreamReader} as {Stream, StreamReader};

/*class SomeClass
{
    public var flag (- bool;

    public def isMatch(number (- int)
    {
        match number {
            some_number @ 0 | 1 if self.flag => println("${some_number} and true");,
            0 => println("0");,
            1 | 2 => println("1 or 2");,
            _ => println("otherwise");
        }
    }
}

def main()
{
    var inst = SomeClass{flag: false};
    inst.isMatch(0);

    inst.flag = true;
    inst.isMatch(0);

    inst.isMatch(1);
}*/

/*class SomeClass
{
    private def someMethod2(number (- int)
    {
        println("${number}");
    }

    public def someMethod()
    {
        let number = 1;
        self.someMethod2(number);
    }
}*/

/*def main()
{
    var reader = StreamReader{path: "./test.txt", detectEncodingFromByteOrderMarks: true};
    println("CanSeek: ${reader.BaseStream.CanSeek}");
    let a = reader.Read();
    println("'${a as char}'");

    let buf = char[3];
    buf[0] = a as char;
    let num_read = reader.Read(buf, 1, 2);
    println("${num_read}: '${buf[0]}''${buf[1]}''${buf[2]}'");
}*/

/*def main()
{
    let a = 'a';
    if a != 'a' {
        println("not matched");
    }else{
        println("matched");
    }
}*/

/*def main()
{
    let a = true && (false || false);
    println("${a}");
}*/

/*enum SomeEnum
{
    A(int)

    public mutating def doSomething() -> SomeEnum
    {
        match self {
            _ => {
                self = SomeEnum::A{0: 11};
                return self;
            }
        }
    }

    public def print()
    {
        match self {
            SomeEnum::A{value} => println("SomeEnum::A(${value})");
        }
    }
}

def main()
{
    var some_enum = SomeEnum::A{0: 10};
    some_enum.print();
    
    some_enum = some_enum.doSomething();
    some_enum.print();
}*/

/*import System.Console as Console;
import std_expresso::Option<> from "std.exs" as Option;

def readAbc(str (- string) -> Option<string>
{
    if str.Equals("abc") {
        return Option::Some<string>{0: "abc"};
    }else{
        return Option::None<string>{};
    }
}

def main()
{
    var option (- Option<string> = Option::None<string>{};
    var line (- string = Console.ReadLine();
    while !line.Equals("quit") {
        option = readAbc(line);
        match option {
            Option::Some{_} => break;,
            Option::None{} => line = Console.ReadLine();
        }
    }

    if option.isSome() {
        println("${option.unwrap()}");
    }
}*/

/*import System.Text.RegularExpressions.{Match, Regex, GroupCollection} as {Match, Regex, GroupCollection};

def main()
{
    let str = r#"treatUnorderedList:");"#;
    let finder = Regex{pattern: r#"\\?['"]"#};
    println("matched = ${finder.IsMatch(str)}");
    let m = finder.Match(str);
    for let group in m.Groups {
        println("group: ${group.Value}");
    }
}*/

/*interface SomeInterface
{
    def getX() -> int;
}

class SomeClass : SomeInterface
{
    public def getX() -> int
    {
        return 10;
    }
}

def main()
{
    ;
}*/

/*import System.Int64 as Int64;

def main()
{
    let int1 = 10 as Int64;
    let negative = -1 as Int64;
    let int2 = int1 + negative;
    println("${int2}");
}*/

/*import std_expresso::{Option<>, Result<,>} from "std.exs" as {Option, Result};

def main()
{
    let option = Option::None<string>{};
    let is_tuesday = false;
    let age = Result::Ok<int, string>{0: 34};

    if let Option::Some{favorite_color} = option {
        println("Using your favorite color ${favorite_color} as the background!");
    }else if is_tuesday {
        println("Tuesday is green day!");
    }else if let Result::Ok{real_age} = age {
        if real_age > 30 {
            println("Using purple as the background color.");
        }else{
            println("Using orange as the background color.");
        }
    }else{
        println("Using blue as the background color.");
    }
}*/

def main()
{
    let dict = {"a" : 10, "b" : 20};
    for let (key, value) in dict {
        println("${key}, ${value}");
    }
}

/*def main()
{
    let tup = (1, 2);
    match tup {
        (1, 1) => println("(1, 1)");,
        _ => println("otherwise");
    }
}*/
