module main;


enum SomeEnum
{
    A = 0,
    B
}

def someFunction(value (- SomeEnum)
{
    match value {
        SomeEnum::A => println("SomeEnum::A");,
        SomeEnum::B => println("SomeEnum::B");
    }
}

def main()
{
    someFunction(SomeEnum::A);
    someFunction(SomeEnum::B);
}