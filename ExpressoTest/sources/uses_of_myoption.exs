module main;


import my_option::MyOption<> from "./my_option.exs" as MyOption;

def main()
{
    let some1 = MyOption::Some<int>{0: 10};
    let some2 = MyOption::Some<string>{0: "10"};

    println("${some1}");
    println("${some2}");
}