module main;

import System.Collections.Generic.IEnumerable<> as IEnumerable;
import System.Linq.Enumerable as Enumerable;


def isPalindrome(s (- string) -> bool
{
	let reversed = Enumerable.Reverse(s as IEnumerable<char>);
	for let (ch1, ch2) in Enumerable.Zip(s as IEnumerable<char>, reversed, |l, r| (l, r)) {
		if !ch1.Equals(ch2) {
			return false;
		}
	}

	return true;
}

def main()
{
	let a = "racecar";
	let b = "noon";
	let c = "civic";
	let d = "radar";
	let e = "characters";

	println("${isPalindrome(a)}");
	println("${isPalindrome(b)}");
	println("${isPalindrome(c)}");
	println("${isPalindrome(d)}");
	println("${isPalindrome(e)}");
}