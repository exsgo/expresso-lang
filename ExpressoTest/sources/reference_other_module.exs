module reference_test_module;


import other_module::ReferencedVariable from "./other_module.exs" as ReferencedVariable;

export class ReferenceClass
{
    public static def changeVariable()
    {
        ReferencedVariable = 100;
    }
}