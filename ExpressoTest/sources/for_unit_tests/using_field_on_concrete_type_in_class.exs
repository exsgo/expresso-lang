module main;


class MyClass4
{
    public let X (- int;

    public def getX() -> int
    {
        return self.X;
    }
}

class GenericClass2<T>
{
    private let x (- T;

    public def getX() -> T
    {
        return self.x;
    }
}

def main()
{
    let inst = GenericClass2<MyClass4>{x: MyClass4{X: 2}};

    println("field: ${inst.getX().X}, method: ${inst.getX().getX()}");
    // Member references won't substitute type parameters
    //println("from field; field: ${inst.X.X}, method: ${inst.X.getX()}");
}