module main;


interface SomeInterface<T>
{
    def getX() -> T;
}

class GenericClass<T> : SomeInterface<T>
{
    let x (- T;

    public def getX() -> T
    {
        return self.x;
    }
}

enum MyResult<T, E>
{
    Ok(T),
    Error(E)
}

def main()
{
    let a = GenericClass<int>{x: 10};
    let b = GenericClass{x: 20};
    let c = MyResult::Ok<int, string>{0: 10};
    let d = MyResult::Error<int, string>{0: "Error"};

    println("${a}, ${b}, ${c}, ${d}");
}