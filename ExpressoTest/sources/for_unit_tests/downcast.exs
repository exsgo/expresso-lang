module main;


import System.Type as Type;


class SomeClass
{
    public virtual def doSomething()
    {
        println("called with SomeClass");
    }
}

class DerivedClass : SomeClass
{
    public override def doSomething()
    {
        println("called with DerivedClass");
    }
}

def main()
{
    let inst = DerivedClass{} as SomeClass;
    inst.doSomething();
    let inst_type = inst.GetType();
    println("inst: ${inst_type.Name}");
    let derived = inst as DerivedClass;
    derived.doSomething();
    let derived_type = derived.GetType();
    println("derived: ${derived_type.Name}");
}