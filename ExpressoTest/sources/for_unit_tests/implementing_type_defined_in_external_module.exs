module main;


import test_module4::SimpleInterface from "./test_module4.exs" as SimpleInterface;

class SimpleClass : SimpleInterface
{
    public def someMethod() -> int
    {
        return 10;
    }
}

def main()
{
    let inst = SimpleClass{};
    println("${inst.someMethod()}");
}