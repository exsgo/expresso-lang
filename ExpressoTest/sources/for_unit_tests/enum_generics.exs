module main;


import my_option::MyOption<> from "../my_option.exs" as MyOption;

def isMatch(value (- MyOption<int>)
{
    match value {
        MyOption::Some{value} => println("${value}");,
        MyOption::None{} => println("None");
    }
}

def main()
{
    let some = MyOption::Some<int>{0: 1};
    let none = MyOption::None<int>{};

    isMatch(some);
    isMatch(none);
}