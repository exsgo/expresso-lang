module main;


import std_expresso::{Option<>, Result<,>} from "std.exs" as {Option, Result};

def printResult(result (- Result<int, string>) -> string
{
    match result {
        Result::Ok{value} => return "${value}";,
        Result::Error{error} => return error;
    }
}

def main()
{
    let result = Result::Ok<int, string>{0: 10};
    let result_error = Result::Error<int, string>{0: "error"};
    let result_b = Result::Ok<int, string>{0: 5};
    //let c2 = |value (- int| -> string "${value}";

    println("isOk: ${result.isOk()}");
    println("isError: ${result.isError()}");
    println("ok: ${result.ok().unwrap()}");
    println("error: ${result_error.error().unwrap()}");
    println("map: ${result.map(|value| value + 1).unwrapOr(20)}");
    println(r#"mapOrElse: ${result.mapOrElse<string>(|error| error, |value| "${value}")}"#);
    println("mapError: ${result_error.mapError(|error| error).unwrapOr(20)}");
    println("and: ${result.and(result_b).unwrapOr(20)}");
    println("andThen: ${result.andThen(|value| value + 1).unwrapOr(20)}");
    println("or: ${result_error.or(result_b).unwrapOr(20)}");
    println("orElse: ${printResult(result_error.orElse(|error| error))}");
    println("unwrapOrElse: ${result_error.unwrapOrElse(|_error| 20)}");
}