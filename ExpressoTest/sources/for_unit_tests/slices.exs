module main;

import System.Linq.Enumerable as Enumerable;
import System.Collections.Generic.IEnumerable<> as IEnumerable;


def main()
{
	let a = [0..10];
	let b = a[2...4];

	let c = [0..10, ...];
	let d = c[2...4];

	let negative_seq = [-10..0:1];
	let to_negative = [0..-10:-1];
	println("Legend: (f(x) = x - 10, g(x) = -x)");
	println("${negative_seq}");
	println("${to_negative}");

	for let (x, to_n) in Enumerable.Zip(negative_seq as IEnumerable<int>, to_negative as IEnumerable<int>, |l, r| (l, r)) {
		print("(f(x), g(x)) = (${x}, ${to_n}),");
		if x == to_n {
			println("Crossed over");
		}
	}

	println("");

	let first_half (- slice<int[], int> = a[0..5];
	let second_half (- slice<int[], int> = a[5..10];

	for let (a, b) in Enumerable.Zip(first_half as IEnumerable<int>, second_half as IEnumerable<int>, |l, r| (l, r)) {
		print("${a}, ${b}, ");
	}

	println("${b}, ${d}");
}