module main;


def main()
{
    var a = [1, 2, 3];
    a[0] = 4;
    println("${a}");
    
    let b = 2;
    a[b] = 8;
    println("${a}");

    a[b] += 2;
    println("${a}");

    var a2 = [1, 2, 3, ...];
    a2[0] = 4;
    println("${a2}");

    a2[b] = 8;
    println("${a2}");

    a2[b] += 2;
    println("${a2}");
}