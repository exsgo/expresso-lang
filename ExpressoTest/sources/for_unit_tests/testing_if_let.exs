module main;


import std_expresso::{Option<>, Result<,>} from "std.exs" as {Option, Result};

def main() -> void
{
    let option = Option::Some<int>{0: 10};
    if let Option::Some{value} = option {
        println("Option::Some{{${value}}}");
    }

    let error = Result::Error<int, string>{0: "error"};
    if let Result::Error{msg} = error {
        println("Result::Error{{${msg}}}");
    }
}