module main;


def someFunc(a = 1, b = true)
{
    println("${a}, ${b}");
}

def main()
{
    someFunc();
    someFunc(2, false);
}
