module main;


class SomeClass
{
    public def callee()
    {
        println("callee called");
        self.calledMethod();
    }

    private def calledMethod() -> void
    {
        println("calledMethod called");
    }
}

def main()
{
    let inst = SomeClass{};
    inst.callee();
}