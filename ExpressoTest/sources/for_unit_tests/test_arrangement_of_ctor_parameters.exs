module main;


class SomeClass
{
    let a (- int, b (- int;

    public def print()
    {
        println("SomeClass{{${self.a}, ${self.b}}}");
    }
}

def main()
{
    let inst = SomeClass{b: 20, a: 10};
    inst.print();
}