module main;


import my_option::MyOption<> from "../my_option.exs" as MyOption;

// The enum must be named so because AST would cause trouble in Exsdoc and ExsdocParser
enum SimpleAST
{
    Ident(SimpleAstNode),
    Expr(SimpleAstNode)

    public def getChild() -> MyOption<SimpleAST>
    {
        match self {
            SimpleAST::Ident{_} => return MyOption::None<SimpleAST>{};,
            SimpleAST::Expr{node} => return node.Child;
        }
    }

    public def print() -> string
    {
        match self {
            SimpleAST::Ident{_} => return "SimpleAST::Ident()";,
            SimpleAST::Expr{_} => return "SimpleAST::Expr(${self.getChild().unwrap().print()})";
        }
    }
}

class SimpleAstNode
{
    public var Child (- MyOption<SimpleAST>;
}

def createAstNode(option (- MyOption<SimpleAST>)
{
    return SimpleAstNode{Child: option};
}

def main()
{
    let child = SimpleAST::Ident{0: createAstNode(MyOption::None<SimpleAST>{})};
    let parent = SimpleAST::Expr{0: createAstNode(MyOption::Some<SimpleAST>{0: child})};

    println("${parent.print()}");
}