module main;



interface PostModifierInterface
{
    def c() -> void requires d in;
    def d() -> void;
}

class PostModifierTest : PostModifierInterface
{
    public def a() -> void
    {
        println("a called");
    }

    public def b() -> void
    requires a beforehand
    {
        println("b called");
    }

    public def c() -> void
    {
        println("c called");
        self.d();
    }

    public def d() -> void
    {
        println("d called");
    }
}

def main()
{
    let inst = PostModifierTest{};
    inst.a();
    inst.b();

    inst.c();
}