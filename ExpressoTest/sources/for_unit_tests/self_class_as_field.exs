module main;



import System.Exception as Exception;

enum MyOption<T>
{
    Some(T),
    None()

    public def unwrap()
    {
        match self {
            MyOption::Some{value} => return value;,
            MyOption::None{} => throw Exception{message: "option is empty"};
        }
    }
}

class Node
{
    public var Next (- MyOption<Node>;
    private let value (- int;

    public def getValue() -> int
    {
        return self.value;
    }

    public def print()
    {
        println("${self.value}, Next: ${self.Next.unwrap().value}, OneAfterNext: ${self.Next.unwrap().Next.unwrap().getValue()}");
    }
}

def main()
{
    let node = Node{Next: MyOption::Some<Node>{0: Node{Next: MyOption::Some<Node>{0: Node{Next: MyOption::None<Node>{}, value: 30}}, value: 20}}, value: 10};
    node.print();
}