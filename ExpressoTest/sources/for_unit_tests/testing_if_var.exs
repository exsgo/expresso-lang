module main;


import std_expresso::Option<> from "std.exs" as Option;

def main()
{
    let option = Option::Some<int>{0: 10};
    if var Option::Some{value} = option {
        println("${value}");
        value = 20;
        println("${value}");
    }
}