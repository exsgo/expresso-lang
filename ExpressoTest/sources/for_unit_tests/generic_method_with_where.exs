module main;


class SomeClass2
{
    let x (- int;

    public override def ToString()
    {
        return "${self.x}";
    }
}

class SomeClass
{
    public static def printT<T>(value (- T)
    where T: SomeClass2
    {
        println("${value}");
    }
}

def main()
{
    let a = SomeClass2{x: 10};

    SomeClass.printT<SomeClass2>(a);
}