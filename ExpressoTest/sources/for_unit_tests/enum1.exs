module main;


enum Union
{
    A(intseq),
    B(int, uint),
    C(string, char),
    D()

    public def print()
    {
        match self {
            Union::A{some_intseq} => println("${some_intseq}");,
            Union::B{some_int, some_uint} => println("${some_int}, ${some_uint}");,
            Union::C{some_str, some_char} => println("${some_str}, ${some_char}");,
            Union::D{} => println("D");
        }
    }
}

def main()
{
    let some_enum = Union::A{0: 1..10};
    let some_enum2 = Union::B{0: 1, 1: 2u};

    println("${some_enum}");
    println("${some_enum2}");

    match some_enum {
        Union::A{some_intseq} => println("${some_intseq}");,
        Union::B{some_int, some_uint} => println("${some_int}, ${some_uint}");,
        Union::C{some_str, some_char} => println("${some_str}, ${some_char}");,
        Union::D{} => println("D");
    }

    some_enum2.print();
}