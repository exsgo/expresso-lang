module main;


import my_option::MyOption<> from "../my_option.exs" as MyOption;

class MyClass3
{
    public let X (- int;

    public def getX() -> int
    {
        return self.X;
    }
}

def main()
{
    let some = MyOption::Some<MyClass3>{0: MyClass3{X: 1}};

    println("field: ${some.unwrap().X}, method: ${some.unwrap().getX()}");
}