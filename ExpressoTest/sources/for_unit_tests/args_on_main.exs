module main;


def main(args (- string[])
{
    if args.Length < 2 {
        println("The length of args is less than what we expected: ${args.Length}");
    }else if !args[0].Equals("parameter") {
        println("Expected the first parameter is 'parameter'");
    }else if !args[1].Equals("second") {
        println("Unexpected second parameter");
    }
}