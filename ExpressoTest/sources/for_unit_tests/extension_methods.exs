module main;


import System.Linq.Enumerable as Enumerable;
import System.Collections.Generic.IEnumerable<> as IEnumerable;
import InteroperabilityTest.ExtensionClass from "./InteroperabilityTest.dll" as ExtensionClass;

def main()
{
    let a = "a";
    let connected = a.Concatenate("bc");
    println("${connected}");

    let ary = [1, 3, 5];
    for let pair in (ary as IEnumerable<int>).Zip([2, 4, 6] as IEnumerable<int>, |l, r| (l, r)) {
        println("${pair}");
    }
}