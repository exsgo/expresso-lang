/**
 * Test program for match statements in Expresso.
 */
module main;


class TestClass6
{
    public let X (- int, Y (- int, _Z (- int;
}

def main()
{
	let tmp = "akarichan";
	match tmp {
		"akarichan" => println("kawakawa");,
		"chinatsu" => println("ankokuthunder!");,
        "kyoko" => println("gaichiban!");,
    	"yui" => println("doyaxtu!");,
        _ => println("otherwise");
	}

    let tmp2 = 1;
    let flag = true;
    match tmp2 {
        0 => println("0");,
        1 | 2 if flag => println("1 or 2");,
        i @ 3...10 => println("${i} is in the range of 3 to 10");,
        _ => println("otherwise");
    }

    let tmp3 = TestClass6{X: 1, Y: 2, _Z: 3};
    match tmp3 {
        TestClass6{X, ..} => println("X is ${X}");,
        TestClass6{X, Y, _} => println("X is ${X} and Y is ${Y}");
    }

    /*let tmp4 = [1, 2, 3, 4, ...];
    match tmp4 {
        [x, y, .., ...] => println("x and y are both vector's elements and the values are ${x} and ${y} respectively");,
        [1, 2, 3, _, ...] => print("tmp4 is a vector");
    }

    let tmp5 = [1, 2, 3, 4];
    match tmp5 {
        [x, y, ..] => println("x and y are both array's elements and the values are ${x} and ${y} respectively");,
        [1, 2, 3, _] => print("tmp5 is an array");
    }*/

    let tmp6 = (4, 5);
    match tmp6 {
        (x, ..) => println("x is ${x}");,
        (x, y) => println("x is ${x} and y is ${y}");
    }
}