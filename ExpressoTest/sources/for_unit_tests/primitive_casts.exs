module main;


def main()
{
    let a = 1;
    let b = 1u;
    //let c = 1l;
    let d = 1.0f;
    let e = 1.0;

    let b2 = a as uint;
    let c2 = a as float;
    let d2 = a as double;
    let e2 = a as byte;

    let a3 = b as int;
    let c3 = b as float;
    let d3 = b as double;
    let e3 = b as byte;

    //let a4 = c as int;
    //let b4 = c as uint;
    //let d4 = c as float;

    let a5 = d as int;
    let b5 = d as uint;
    let c5 = d as double;
    let e5 = d as byte;

    let a6 = e as int;
    let b6 = e as uint;
    let c6 = e as float;
    let d6 = e as byte;

    println("from int: ${b2}, ${c2}, ${d2}, ${e2}");
    println("from uint: ${a3}, ${c3}, ${d3}, ${e3}");
    //println("from bigint: ${a4}, ${b4}, ${d4}");
    println("from float: ${a5}, ${b5}, ${c5}, ${e5}");
    println("from double: ${a6}, ${b6}, ${c6}, ${d6}");
}