module main;


class SomeClass2
{
    public let X (- int;

    public def getX()
    {
        return self.X;
    }

    public override def ToString() -> string
    {
        return "${self.X}";
    }
}

class SomeClass<T>
where T: SomeClass2
{
    let x (- T;

    public def print()
    {
        return "field: ${self.x.X}, method: ${self.x.getX()}";
    }
}

def main()
{
    let a = SomeClass2{X: 10};
    let inst = SomeClass<SomeClass2>{x: a};
    println("${inst.print()}");

    let inst2 = SomeClass{x: a};
    println("${inst2.print()}");
}