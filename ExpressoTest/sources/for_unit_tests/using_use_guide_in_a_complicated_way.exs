module main;


class PostModifierTest2
{
    public def a() -> void
    {
        println("called a");
    }

    public def b() -> void
    {
        println("called b");
        self.a();
    }

    public def c() -> void 
    requires a beforehand
    {
        println("called c");
    }

    public def d() -> void
    {
        println("called d");
        self.b();
    }
}

def main()
{
    let inst = PostModifierTest2{};
    inst.d();
    inst.c();
}