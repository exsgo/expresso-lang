module test_module4;

export interface SimpleInterface
{
    def someMethod() -> int;
}