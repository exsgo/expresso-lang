module main;


import my_option::MyOption<> from "../my_option.exs" as MyOption;

class Husband
{
    public var W (- MyOption<Wife>;
    private let age (- int;

    public mutating def setWife(newWife (- Wife)
    {
        self.W = MyOption::Some<Wife>{0: newWife};
    }

    public def print()
    {
        println("Husband: ${self.age}, Wife(field): ${self.W.unwrap().Age}, Wife(method): ${self.W.unwrap().getAge()}");
    }
}

class Wife
{
    public var _H (- MyOption<Husband>;
    public let Age (- int;

    public def getAge()
    {
        return self.Age;
    }

    public mutating def setHusband(h (- Husband)
    {
        self._H = MyOption::Some<Husband>{0: h};
    }
}

def main()
{
    var h = Husband{W: MyOption::Some<Wife>{0: Wife{_H: MyOption::None<Husband>{}, Age: 23}}, age: 24};
    h.W.unwrap().setHusband(h);
    h.print();
    
    h.setWife(Wife{_H: MyOption::Some<Husband>{0: h}, Age: 20});
    h.print();
}