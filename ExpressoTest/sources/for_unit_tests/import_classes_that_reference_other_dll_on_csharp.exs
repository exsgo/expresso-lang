module main;


import InteroperabilityTest.Additionals.{ReferenceOtherDll, ConcreteReferenceOtherDll} from "./InteroperabilityTest.dll" as {ReferenceOtherDll, ConcreteReferenceOtherDll};

def main()
{
    let inst = ReferenceOtherDll{x: 10};
    println("${inst.Get()}");

    let inst2 = ConcreteReferenceOtherDll{x: 20};
    println("${inst2.Get()}");
}