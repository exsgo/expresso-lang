module main;


enum EnumA<T>
{
    A(),
    B(T)

    public def convert() -> EnumB<T>
    {
        match self {
            EnumA::A{} => return EnumB::C<T>{};,
            EnumA::B{value} => return EnumB::D<T>{0: value};
        }
    }
}

enum EnumB<T>
{
    C(),
    D(T)

    public def convert() -> EnumA<T>
    {
        match self {
            EnumB::C{} => return EnumA::A<T>{};,
            EnumB::D{value} => return EnumA::B<T>{0: value};
        }
    }

    public def toString() -> string
    {
        match self {
            EnumB::C{} => return "EnumB::C{}";,
            EnumB::D{value} => return "EnumB::D{{${value}}}";
        }
    }
}

def main()
{
    let enum_a = EnumA::A<int>{};
    let converted_a = enum_a.convert();
    println("${converted_a.toString()}");

    let enum_b = EnumA::B<int>{0: 10};
    let converted_b = enum_b.convert();
    println("${converted_b.toString()}");
}