module main;


import std_expresso::Option<> from "std.exs" as Option;

def main()
{
    let option = Option::None<int>{};

    if let Option::Some{value} = option {
        println("Option is some and the value is ${value}.");
    }else{
        println("Option::None{}");
    }
}