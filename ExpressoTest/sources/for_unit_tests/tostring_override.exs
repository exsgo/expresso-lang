module main;


class SomeClass
{
    public let x (- int, y (- int;

    public virtual def getX()
    {
        return self.x;
    }

    public override def ToString()
    {
        return "x: ${self.x}, y: ${self.y}";
    }
}

class ChildClass : SomeClass
{
    public override def getX()
    {
        return self.y;
    }
}

abstract class AbstractClass
{
    public abstract def getX();
}

def main()
{
    let child_inst = ChildClass{x: 1, y: 2};
    let seq = [SomeClass{x: 1, y: 2}, SomeClass{x: 3, y: 4}, SomeClass{x: 5, y: 6}, ...];
    let y = child_inst.getX();

    println("${seq}, ${y}");
}
