module main;


import InteroperabilityTest.ArgumentTest from "./InteroperabilityTest.dll" as ArgumentTest;

def someFunction(x (- int, y = 1)
{
    println("${x}, ${y}");
}

def someFunction2(x (- int, y... (- int[])
{
    println("${x}, ${y}");
}

def main()
{
    someFunction(1);
    someFunction(1, 2);

    someFunction2(3);
    someFunction2(3, 4, 5, 6, 7);

    ArgumentTest.MethodWithOptional(10);
    ArgumentTest.MethodWithOptional(10, 11);

    ArgumentTest.MethodWithParams(12);
    ArgumentTest.MethodWithParams(12, 13, 14, 15, 16);
}