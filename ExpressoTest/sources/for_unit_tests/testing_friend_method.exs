module main;


class SomeClass
{
    public def a() -> void
    {
        println("a called");
        self.b();
    }

    def b() -> void
    friends with a
    {
        println("b called");
    }
}

def main()
{
    let inst = SomeClass{};
    inst.a();
}