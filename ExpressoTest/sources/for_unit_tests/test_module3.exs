module test_module3;


def usePrivateFunctionImpl(n (- int)
{
    return n + 1;
}

export def usePrivateFunction(n (- int)
{
    return usePrivateFunctionImpl(n);
}
