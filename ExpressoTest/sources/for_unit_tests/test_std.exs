module main;


import std_expresso::{Option<>, Result<,>} from "std.exs" as {Option, Result};

def main()
{
    let option = Option::Some<int>{0: 10};
    println("${option.unwrap()}");

    let mapped = option.map(|value| value + 1);
    println("${mapped.unwrap()}");

    let result = Result::Ok<int, string>{0: 20};
    println("${result.unwrapOr(30)}");
}