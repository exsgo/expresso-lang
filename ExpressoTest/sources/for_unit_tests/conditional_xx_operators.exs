module main;


def main()
{
    let x = 10, flag = true;
    if x == 10 && flag {
        println("conditional and");
    }

    if x == 10 || flag {
        println("conditional or");
    }

    if true && (false || true) {
        println("first true");
    }

    if false && (true || true) {
        println("second true");
    }else{
        println("second false");
    }
}