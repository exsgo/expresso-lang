module main;


import System.Exception as Exception;
import std_expresso::{Option<>, Result<,>} from "std.exs" as {Option, Result};

def printOption(option (- Option<int>) -> string
{
    match option {
        Option::Some{value} => return "${value}";,
        Option::None{} => return "Option::None()";
    }
}

def main()
{
    let option = Option::Some<int>{0: 10};
    let none = Option::None<int>{};
    let option_b = Option::Some<int>{0: 5};
    let option_c = Option::Some<int>{0: 20};
    let error_msg = "error";

    println("isSome: ${option.isSome()}");
    println("isNone: ${option.isNone()}");
    try{
        none.expect("thrown expected");
    }
    catch e (- Exception {
        println("expect: ${e.Message}");
    }
    println("unwrapOr: ${none.unwrapOr(20)}");
    println("unwrapOrElse: ${none.unwrapOrElse(|| 20)}");
    println("mapOr(Some): ${option.mapOr(20, |value| value + 1)}");
    println("mapOr(None): ${none.mapOr(20, |value| value + 1)}");
    println("mapOrElse: ${none.mapOrElse<int>(|| 20, |value| value + 1)}");
    println("okOr: ${option.okOr(error_msg)}");
    println("okOrElse: ${none.okOrElse(|| error_msg)}");
    println("and: ${option.and(option_b)}");
    println("andThen: ${option.andThen<int>(|value| value + 1)}");
    println("filter: ${option.filter(|value| value == 10).unwrap()}");
    println("or: ${none.or(option_c).unwrap()}");
    println("orElse: ${none.orElse(|| 20).unwrap()}");
    println("xor: ${printOption(option.xor(option_c))}");
    // These 2 methods won't work properly in Expresso
    //println("replace: ${option.replace()}");
    //println("take: ${option.take()}");
}