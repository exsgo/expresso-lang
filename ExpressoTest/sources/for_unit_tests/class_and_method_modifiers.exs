module main;


class SomeClass
{
    public let X (- int, Y (- int;

    public override def ToString()
    {
        return "(x: ${self.X}, y: ${self.Y})";
    }
}

abstract class AbstractClass
{
    public abstract def getX() -> int;
}

sealed class ChildClass : AbstractClass
{
    private let x (- int;
    public def getX() -> int
    {
        return self.x;
    }
}

def main()
{
    let seq = [SomeClass{X: 1, Y: 2}, SomeClass{X: 3, Y: 4}, SomeClass{X: 5, Y: 6}, ...];
    let child_inst = ChildClass{x: 10};
    let x = child_inst.getX();

    println("${seq}, ${x}");
}
