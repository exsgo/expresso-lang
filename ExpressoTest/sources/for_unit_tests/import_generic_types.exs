module main;


import System.Collections.Generic.{IEnumerable<>, SortedDictionary<,>} as {IEnumerable, SortedDictionary};

def main()
{
    let dict = SortedDictionary<string, int>{};
    println("${dict}");
}