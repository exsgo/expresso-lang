module main;


def main()
{
    let c (- |int| -> int = |i (- int| -> int i + 1;
    
    let a = c(1);

    println("${a}");
}
