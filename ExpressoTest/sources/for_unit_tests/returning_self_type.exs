/**
 * Test program for reutrning the self class type.
 */

module main;

import my_option::MyOption<> from "../my_option.exs" as MyOption;

class TestClass
{
	public let X (- int;

	public def returnSelf() -> TestClass
	{
	    return self;
	}

	public def returnSelfWithOption() -> MyOption<TestClass>
	{
		return MyOption::Some<TestClass>{0: self};
	}

	public def getX()
	{
		return self.X;
	}
}

def main()
{
	let inst = TestClass{X: 10};
	let inst2 = inst.returnSelf();
	let a = inst2.X;
	let b = inst2.getX();
	let option = inst.returnSelfWithOption();
	let c = option.unwrap().X;
	let d = option.unwrap().getX();

	println("${inst}, ${inst2}, field: ${a}, method: ${b}, option(field): ${c}, option(method): ${d}");
}
