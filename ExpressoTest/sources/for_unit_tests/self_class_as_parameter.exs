module main;


class SomeClass
{
    private let x (- int;

    private def getX()
    {
        return "x: ${self.x}";
    }

    public def someMethod(this (- SomeClass) -> void
    {
        println("${this.x}, ${this.getX()}");
    }
}

def main()
{
    let inst = SomeClass{x: 10};
    let inst2 = SomeClass{x: 20};
    inst.someMethod(inst2);
}