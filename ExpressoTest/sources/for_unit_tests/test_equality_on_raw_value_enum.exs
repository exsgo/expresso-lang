module main;


enum SomeEnum
{
    A = 1
}

def main()
{
    let some_enum = SomeEnum::A;
    if some_enum == SomeEnum::A {
        println("SomeEnum::A");
    }

    let is_a = some_enum == SomeEnum::A;
    println("${is_a}");
}