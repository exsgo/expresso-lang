module main;


interface SomeInterface
{
    def someMethod() -> int;
}

class SomeClass : SomeInterface
{
    private let x (- int;

    public def someMethod() -> int
    {
        return self.x;
    }
}

class GenericClass<T>
where T: SomeInterface
{
    private let inst (- T;

    public def doSomething() -> int
    {
        return self.inst.someMethod();
    }
}

def main()
{
    let inst = GenericClass{inst: SomeClass{x: 10}};
    println("${inst.doSomething()}");
}