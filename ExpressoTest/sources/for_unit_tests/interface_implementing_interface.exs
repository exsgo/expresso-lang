module main;


interface SomeInterface
{
    def getX() -> int;
}

interface SomeInterface2 : SomeInterface
{
    def getXPlus(n (- int) -> int;
}

class SomeClass : SomeInterface2
{
    private let x (- int;

    public def getX() -> int
    {
        return self.x;
    }

    public def getXPlus(n (- int) -> int
    {
        return self.getX() + n;
    }
}

def main()
{
    let inst = SomeClass{x: 10};
    println("${inst.getX()}, ${inst.getXPlus(1)}");
}