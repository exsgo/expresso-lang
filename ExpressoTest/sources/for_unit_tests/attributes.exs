/**
 * Test program for attributes
 */
#[asm: AssemblyDescription{description: "test assembly for attributes"}]

#[CLSCompliant{isCompliant: true}]
module main;

import System.{SerializableAttribute, CLSCompliantAttribute, ObsoleteAttribute, Attribute, AttributeUsageAttribute, AttributeTargets} as 
{SerializableAttribute, CLSCompliantAttribute, ObsoleteAttribute, Attribute, AttributeUsageAttribute, AttributeTargets};
import System.Diagnostics.ConditionalAttribute as ConditionalAttribute;
import System.Reflection.AssemblyDescriptionAttribute as AssemblyDescriptionAttribute;



#[AttributeUsage{validOn: AttributeTargets.All}]
class AuthorAttribute : Attribute
{
    let _name (- string;
}

#[Author{_name: "train12"}]
let _y = 100;

#[Obsolete]
def doSomethingInModule()
{
    ;
}

#[Serializable]
class AttributeTest
{
	#[Obsolete]
	let _x (- int;

	#[Conditional{conditionString: "DEBUG"}]
	public def doSomething(#[Author{_name: "train12"}] _dummy (- string)
	{
		println("Do something");
	}

	#[return: Author{_name: "train12"}]
	public def doSomething2(n = 100) -> int
	{
        println("${n}");
		return 10;
	}
}

def main()
{
    let x = AttributeTest{_x: 10};
    x.doSomething("some string");
    x.doSomething2();
}
