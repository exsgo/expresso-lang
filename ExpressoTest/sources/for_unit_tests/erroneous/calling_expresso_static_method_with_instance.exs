module main;


import test_module::SomeClass from "../test_module5.exs" as SomeClass;

def main()
{
    let inst = SomeClass{};
    inst.staticMethod();
}