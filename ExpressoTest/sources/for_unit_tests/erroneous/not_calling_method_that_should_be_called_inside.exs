module main;


interface SomeInterface
{
    def a() -> void requires b in;
    def b() -> void;
}

class SomeClass : SomeInterface
{
    public def a() -> void
    {
        ;
    }

    public def b() -> void
    {
        ;
    }
}

def main()
{
    ;
}