module main;


class SomeClass
{
    let x (- int;
}

def main()
{
    let a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7, h = 8, i = 9;
    println("${a}, ${b}, ${c}, ${d}, ${e}, ${f}, ${g}, ${h}, ${i}, ${SomeClass{x: 10}}");
}