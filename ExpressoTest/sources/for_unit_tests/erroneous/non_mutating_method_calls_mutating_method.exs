module main;


class SomeClass
{
    var x (- int;

    private mutating def mutatingMethod()
    {
        self.x = 10;
    }

    public def callMutatingMethod()
    {
        self.mutatingMethod();
    }
}

def main()
{
    ;
}