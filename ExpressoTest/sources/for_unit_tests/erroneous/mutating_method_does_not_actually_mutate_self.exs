module main;


class SomeClass
{
    private def doNothing()
    {
        ;
    }

    public mutating def doSomething()
    {
        self.doNothing();
    }
}

def main()
{
    ;
}