module main;


import InteroperabilityTest.SomeGenericClass<,> from "../InteroperabilityTest.dll" as SomeGenericClass;

def main()
{
    let x = SomeGenericClass<int, string>{t: "abc", u: "def"};
}