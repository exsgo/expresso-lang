module main;


class SomeClass
{
    let x (- int;
}

def main()
{
    let a = SomeClass{x: 10};
    match a {
        SomeClass{} => println("matched");,
        _ => println("");
    }
}