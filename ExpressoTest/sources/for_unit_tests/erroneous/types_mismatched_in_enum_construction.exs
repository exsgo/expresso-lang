module main;


enum SomeEnum
{
    A(int)
}

def main()
{
    let x = SomeEnum::A{0: "abc"};
}