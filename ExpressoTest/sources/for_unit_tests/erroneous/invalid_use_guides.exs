module main;

interface SomeInterface
{
    def a() -> void requires b afterwards;
    def b() -> void;
}

class SomeClass
{
    def a() -> void
    {
        ;
    }

    def b() -> void
    {
        ;
    }

    def c() -> void
    requires a in
    {
        ;
    }
}

def main()
{
    ;
}