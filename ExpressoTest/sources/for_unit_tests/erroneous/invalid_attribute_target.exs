module main;


import System.Diagnostics.ConditionalAttribute as ConditionalAttribute;

class SomeClass
{
    #[Conditional{conditionString: "DEBUG"}]
    let _x (- int;
}

def main()
{
    ;
}