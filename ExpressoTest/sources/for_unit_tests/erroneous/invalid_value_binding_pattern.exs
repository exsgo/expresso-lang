module main;


def main()
{
    let a = 10;
    match a {
        let b = a => println("b = ${b}");
    }
}