module main;


class GenericClass<T>
{
    private var value (- T;

    public mutating def setValue(value (- T)
    {
        self.value = value;
    }
}

def main()
{
    var a = GenericClass<int>{value: 1};
    a.setValue(2.0);
}