module main;


class A
{
    let a (- int;
}

def main()
{
    let ary = [A{a: 10}, A{a: 20}];
    for let A{a: a} in ary {
        ;
    }
}