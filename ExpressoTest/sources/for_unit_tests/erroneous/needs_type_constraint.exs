module main;



class MyClass
{
    public let X (- int;
}

class GenericClass<T>
{
    private let x (- T;

    public def getXX() -> int
    {
        return self.x.X;
    }
}

def main()
{
    let a = GenericClass<MyClass>{x: MyClass{X: 10}};

    println("${a.getXX()}");
}