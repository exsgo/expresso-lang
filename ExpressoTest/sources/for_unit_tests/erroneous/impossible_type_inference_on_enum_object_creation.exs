module main;


enum MyOption<T>
{
    Some(T),
    None()
}

def main()
{
    let option = MyOption::Some{0: 10};
    println("${option.unwrap()}");
}