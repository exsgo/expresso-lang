module main;


enum SomeEnum
{
    A(int)
}

def main()
{
    let a = SomeEnum::A{0: 10};
    match a {
        SomeEnum::A{"10"} => println("SomeEnum::A{10}");
    }
}