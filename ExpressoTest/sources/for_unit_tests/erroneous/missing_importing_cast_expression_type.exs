module main;


import test_erroneous_module2::{ErroneousClass, createDerivedErroneousClass} from "./test_erroneous_module2.exs" as {ErroneousClass, createDerivedErroneousClass};

def main()
{
    let _ = createDerivedErroneousClass(10, 20) as ErroneousClass;
}