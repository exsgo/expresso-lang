module main;


def someMethod()
{
    println("called");
}

def main()
{
    let str = "${someMethod()}";
}