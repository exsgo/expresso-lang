module main;


enum SomeEnum
{
    A(int)
}

def main()
{
    let some_enum = SomeEnum::A{0: 10, 1: 20};
}