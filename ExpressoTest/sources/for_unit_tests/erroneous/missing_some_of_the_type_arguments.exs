module main;


class SomeClass<T>
{
    let x (- T;
}

def main()
{
    let inst = SomeClass<>{x: 10};
}