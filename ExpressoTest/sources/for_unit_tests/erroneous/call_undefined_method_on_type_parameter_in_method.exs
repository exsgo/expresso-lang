module main;


class SomeClass
{
    private let x (- int;

    public def print()
    {
        println("${self.x}");
    }
}

class SomeClass2<T>
{
    private let x (- T;
    
    public def someMethod<T>(value (- T)
    where T: SomeClass
    {
        value.undefinedMethod();
    }
}

def main()
{
    ;
}