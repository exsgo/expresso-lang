module main;


class SomeClass
{
    private let x (- int;

    public def print()
    {
        println("${self.x}");
    }
}

class SomeClass2<T>
where T: SomeClass
{
    let x (- T;

    public def someMethod()
    {
        self.x.undefinedMethod();
    }
}

def main()
{
    ;
}