module main;


def recursive(n (- int)
{
    if n == 100 {
        return n;
    }else{
        return recursive(n + 10);
    }
}

def main()
{
    recursive(80);
}