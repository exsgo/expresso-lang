module main;


def someFunction(n (- int)
{
    println("${n}");
}

def main()
{
    someFunction(1, 2);
}