module main;


import InteroperabilityTest.{EnumTest, TestEnum} from "../InteroperabilityTest.dll" as {EnumTest, TestEnum};

def main()
{
    let inst = EnumTest{};
    println("${inst.TestEnumeration(TestEnum.SomeField)}");
}