module main;

class SomeClass
{
    public def a() -> void
    {
        println("a called");
        self.c();
    }

    public def b() -> void
    {
        println("b called");
    }

    public def c() -> void
    requires b beforehand
    {
        println("c called");
    }

    public def d() -> void
    {
        println("d called");
        self.b();
    }
}

def main()
{
    let inst = SomeClass{};
    inst.d();
    inst.a();
}