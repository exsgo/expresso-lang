module main;


enum SomeEnum
{
    A(int)
}

def main()
{
    let a = SomeEnum::A{0: 1};
    match a {
        SomeEnum::A{value} => println("A");,
        SomeEnum::B{} => println("B");
    }
}