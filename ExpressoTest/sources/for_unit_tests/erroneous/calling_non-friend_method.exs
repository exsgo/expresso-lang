module main;


class SomeClass
{
    def a() -> void
    friends with c
    {
        ;
    }

    public def b() -> void
    {
        self.a();
    }

    public def c() -> void
    {
        self.a();
    }
}

def main()
{
    ;
}