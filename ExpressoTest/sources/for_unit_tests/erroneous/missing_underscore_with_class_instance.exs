module main;


class SomeClass
{
    let x (- int;
    let y (- int;
    let z (- int;
}

def main()
{
    let x = SomeClass{x: 10, y: 20, z: 30};
    match x {
        SomeClass{x: 10, y: 20, z: 25} => println("SomeClass");
    }
}