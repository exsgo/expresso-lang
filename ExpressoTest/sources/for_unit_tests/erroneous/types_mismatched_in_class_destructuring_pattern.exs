module main;


class SomeClass
{
    let x (- int;
}

def main()
{
    let a = SomeClass{x: 10};
    match a {
        SomeClass{x: "10"} => println("SomeClass{10}");,
        SomeClass{x: x} => println("otherwise");
    }
}