module main;


class TestClass
{
	public let X (- int;
}

def main()
{
	var t = TestClass{X: 10};
	t.X = 20;
}