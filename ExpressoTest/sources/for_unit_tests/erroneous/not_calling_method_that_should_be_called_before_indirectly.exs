module main;


class SomeClass
{
    public def a() -> void
    {
        ;
    }

    public def b() -> void
    {
        self.a();
    }

    public def c() -> void
    {
        self.b();
    }

    public def d() -> void
    requires a beforehand
    {
        ;
    }
}

def main()
{
    let inst = SomeClass{};
    inst.d();
    inst.c();
}