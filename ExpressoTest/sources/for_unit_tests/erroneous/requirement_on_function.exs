module main;


def a() -> void
{
    println("a called");
}

def b() -> void
requires a beforehand
{
    println("b called");
}

def main()
{
    ;
}