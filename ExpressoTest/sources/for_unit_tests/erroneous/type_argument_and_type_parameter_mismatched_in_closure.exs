module main;


import std_expresso::Option<> from "std.exs" as Option;

def main()
{
    let none = Option::None<int>{};
    let _mappped = none.mapOrElse<string>(|| "none", |value| value + 1);
}