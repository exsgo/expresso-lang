module main;


enum SomeEnum
{
    A(int),
    B(string)
}

def main()
{
    let x = SomeEnum{0: 10};
}