module test_erroneous_module2;


export class ErroneousClass
{
}

export class DerivedErroneousClass : ErroneousClass
{
    let _x (- int, _y (- int;
}

export def createDerivedErroneousClass(x (- int, y (- int) -> DerivedErroneousClass
{
    return DerivedErroneousClass{_x: x, _y: y};
}