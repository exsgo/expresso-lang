module main;


sealed class SealedClass
{
}

class DerivedClass : SealedClass
{
}

def main()
{
    ;
}