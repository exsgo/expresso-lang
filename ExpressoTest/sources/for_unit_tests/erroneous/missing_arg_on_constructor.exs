module main;


class SomeClass
{
    private let x (- int;
}

def main()
{
    let x = SomeClass{};
}