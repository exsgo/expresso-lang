module main;


interface IInterface
{
    def someMethod() -> void;
}

class TestClass : IInterface
{
    private def someMethod() -> void
    {
        ;
    }
}

def main()
{
    ;
}