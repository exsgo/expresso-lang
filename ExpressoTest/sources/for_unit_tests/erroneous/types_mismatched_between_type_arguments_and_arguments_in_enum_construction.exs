module main;


enum SomeEnum<T>
{
    A(T)
}

def main()
{
    let x = SomeEnum::A<int>{0: "abc"};
    println("${x}");
}