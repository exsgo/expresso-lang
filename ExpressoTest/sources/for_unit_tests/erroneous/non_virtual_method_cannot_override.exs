module main;


class ParentClass
{
    public def nonVirtualMethod()
    {
        return 1;
    }
}

class ChildClass : ParentClass
{
    public override def nonVirtualMethod()
    {
        return 2;
    }
}

def main()
{
    ;
}