module main;


import System.Exception as Exception;

interface HasCause
{
    def getCause() -> string;
}

class MyException : Exception, HasCause
{
}

def main()
{
    ;
}