module main;


def someFunction(x (- int, y = 1)
{
    println("${x}, ${y}");
}

def main()
{
    someFunction();
}