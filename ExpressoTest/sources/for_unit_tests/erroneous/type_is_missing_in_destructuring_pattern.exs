module main;


class SomeClass
{
    let x (- int;
}

def main()
{
    let inst = SomeClass{x: 10};
    match inst {
        UndefinedClass{} => println("matched");,
        _ => println("not matched");
    }
}