module main;


class SomeClass
{
    public def a() requires b beforehand
    {
        ;
    }

    public def b()
    {
        ;
    }
}

def main()
{
    let inst = SomeClass{};
    inst.a();
}