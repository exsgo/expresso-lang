module main;



class MyClass
{
    public let X (- int;
}

class GenericClass<T>
{
    public let X (- T;
}

def main()
{
    let a = GenericClass<MyClass>{X: MyClass{X: 10}};

    println("${a.X.X}");
}