module main;


class GenericClass
{
    public static def someMethod<T>(items (- vector<T>)
    {
        println("${items}");
        return items[0];
    }
}

def main()
{
    let first = GenericClass.someMethod([1, 2, 3]);
}