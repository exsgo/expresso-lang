module test_erroneous_module;


let _private_field (- int = 100;

export def add1(n (- int)
{
	return n + 1;
}

export interface SimpleInterface
{
	def someMethod() -> int;
}