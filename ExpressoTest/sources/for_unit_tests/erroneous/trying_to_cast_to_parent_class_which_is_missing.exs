module main;


class DerivedClass : ParentClass
{
}

def main()
{
    let inst = DerivedClass{};
    let inst2 = inst as ParentClass;
}