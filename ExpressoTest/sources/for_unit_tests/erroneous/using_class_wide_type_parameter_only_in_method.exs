module main;


class GenericClass<T>
{
    public static def someMethod(value (- T)
    {
        println("${value}");
    }
}

def main()
{
    GenericClass.someMethod(10);
}