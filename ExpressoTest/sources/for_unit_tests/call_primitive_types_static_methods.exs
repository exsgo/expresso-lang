module main;


def main()
{
    let str = "abc";
    let modified = string.Concat(str, "def");
    println("${modified}");

    let parsed = int.Parse("10");
    println("${parsed}");
}