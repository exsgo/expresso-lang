module main;


def main()
{
    var chars = char[10];
    chars[0] = 'a';
    chars[1] = 'b';
    chars[2] = 'c';

    let str = string{value: chars};
    println("${str}");
}