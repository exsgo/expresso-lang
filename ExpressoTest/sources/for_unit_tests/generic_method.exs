module main;


class SomeClass
{
    public static def printT<T>(value (- T)
    {
        println("${value}");
    }
}

def main()
{
    let a = 10;
    let b = 15.0;

    SomeClass.printT<int>(a);
    SomeClass.printT<double>(b);
}