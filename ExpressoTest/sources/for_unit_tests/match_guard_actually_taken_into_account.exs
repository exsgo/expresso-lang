module main;


def main()
{
    let ch = 'a';
    let flag = false;

    match ch {
        'a' if flag => println("flag is true");,
        'a' => println("flag is false");,
        _ => println("otherwise");
    }
}