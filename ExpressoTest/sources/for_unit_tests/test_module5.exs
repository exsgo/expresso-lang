module test_module;


export class SomeClass
{
    public static def staticMethod()
    {
        println("called staticMethod");
    }

    public def instanceMethod()
    {
        println("called instanceMethod");
    }
}