module main;


import test_module3::usePrivateFunction from "./test_module3.exs" as usePrivateFunction;

def main()
{
    let a = usePrivateFunction(10);
    println("${a}");
}