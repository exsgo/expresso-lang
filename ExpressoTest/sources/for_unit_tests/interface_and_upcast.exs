module main;


import System.Math as Math;


interface HasArea
{
    def calculateArea() -> double;
}

class Circle : HasArea
{
    let _x (- int, _y (- int, radius (- int;
    
    public def calculateArea() -> double
    {
        return 2.0 * Math.PI * ((self.radius * self.radius) as double);
    }
}

class Rectangle : HasArea
{
    let _x (- int, _y (- int, width (- int, height (- int;

    public def calculateArea() -> double
    {
        return (self.width * self.height) as double;
    }
}

def main()
{
    var has_area = Circle{_x: 0, _y: 0, radius: 10} as HasArea;
    println("Circle: ${has_area.calculateArea()}");
    has_area = Rectangle{_x: 0, _y: 0, width: 5, height: 5} as HasArea;
    println("Rectangle: ${has_area.calculateArea()}");
}