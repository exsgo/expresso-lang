module main;


import InteroperabilityTest.StaticTest from "./InteroperabilityTest.dll" as StaticTest;

def main()
{
    let str = StaticTest.GetString(null);
    println("${str}");
}