/**
 * This situation can be a problem because context.TargetType would be null when inspecting the call of a module function.
 */
module main;


def externalFunction()
{
    println("externalFunction called");
}

class SomeClass
{
    private def a()
    {
        println("a called");
    }

    public def publicMethod()
    {
        self.a();

        externalFunction();
    }
}

def main()
{
    let inst = SomeClass{};
    inst.publicMethod();
}