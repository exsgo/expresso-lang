module main;


class SomeClass<T>
{
    private let _x (- T;

    public static def applyClosures<U>(c1 (- || -> T, c2 (- |T| -> U) -> U
    {
        return c2(c1());
    }
}

def main()
{
    let result = SomeClass<int>.applyClosures<int>(|| 10, |value| value + 1);
    println("${result}");
}