module main;


class MyClass
{
    public let X (- int;
}

class GenericClass2<T>
where T: MyClass
{
    public let X (- T;

    public def getX() -> T
    {
        return self.X;
    }
}

def main()
{
    let a = GenericClass2<MyClass>{X: MyClass{X: 10}};
    let b = GenericClass2{X: MyClass{X: 20}};

    println("${a.getX().X}, ${b.getX().X}");
    println("${a.X.X}, ${b.X.X}");
}