module main;


import InteroperabilityTest.* from "./InteroperabilityTest.dll" as *;

def main()
{
    ArgumentTest.MethodWithOptional(10);
    
    if EnumTest.TestEnumeration(TestEnum.SomeField) {
		println("enum matched!");
	}

    let a = "a";
    let connected = a.Concatenate("bc");
    println("${connected}");

    let inst = InteroperabilityTest{};
    inst.DoSomething();

    let seq = MyIntseq{start: 0, end: 10, step: 1};

    for let i in seq {
        println("${i}");
    }

    PropertyTest.SomeProperty = 100;
	println("after: ${PropertyTest.SomeProperty}");

    let inst2 = SomeGenericClass{t: 10, u: 10u};
    println("${inst2}");

    StaticTest.DoSomething();
}