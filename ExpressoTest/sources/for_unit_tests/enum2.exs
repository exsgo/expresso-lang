module main;


enum SomeEnum
{
    A = 1,
    B,
    C

    public def print() -> void
    {
        match self {
            SomeEnum::A => println("A");,
            SomeEnum::B => println("B");,
            SomeEnum::C => println("C");
        }
    }
}

class SomeClass
{
    private let x (- SomeEnum;

    public def matchEnum() -> void
    {
        match self.x {
            SomeEnum::A => println("A in matchEnum");,
            SomeEnum::B => println("B in matchEnum");,
            SomeEnum::C => println("C in matchEnum");
        }
    }
}

def main()
{
    let enum_a = SomeEnum::A;

    println("${enum_a}");

    match enum_a {
        SomeEnum::A => println("A");,
        SomeEnum::B => println("B");,
        SomeEnum::C => println("C");
    }

    enum_a.print();

    let some_class = SomeClass{x: enum_a};
    some_class.matchEnum();
}