module main;

class GenericClass<TParam>
{
    private let x (- TParam;

    public def map<TResult>(f (- |TParam| -> TResult) -> TResult
    {
        return f(self.x);
    }
}

def main()
{
    let inst = GenericClass{x: 10};
    let mapped = inst.map(|value| value + 1);
    println("${mapped}");
}