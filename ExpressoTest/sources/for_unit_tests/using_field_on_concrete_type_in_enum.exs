module main;


import my_option::MyOption<> from "../my_option.exs" as MyOption;

class MyClass2
{
    public let X (- int;
}

def isMatch(value (- MyOption<MyClass2>)
{
    match value {
        MyOption::Some{value} => println("${value.X}");,
        MyOption::None{} => println("None");
    }
}

def main()
{
    let some = MyOption::Some<MyClass2>{0: MyClass2{X: 1}};
    let none = MyOption::None<MyClass2>{};

    isMatch(some);
    isMatch(none);
}