module main;


import InteroperabilityTest.MyIntseq from "./for_unit_tests/InteroperabilityTest.dll" as MyIntseq;

def main()
{
    let seq = MyIntseq{start: 0, end: 10, step: 1};

    for let i in seq {
        println("${i}");
    }
}
