﻿using System;
using System.IO;
using System.Reflection;

using Expresso.CodeGen;

using NUnit.Framework;

namespace Expresso.Test
{
    [TestFixture]
    public class TutorialTests
    {
        static string GenerateAssembly(string filePath, string targetFilePath = "../../test_executables")
        {
            var parser = new Parser(new Scanner(filePath)){
                DoPostParseProcessing = true
            };
            parser.Parse();

            var ast = parser.TopmostAst;
            var start_index = filePath.LastIndexOf("/", StringComparison.CurrentCulture) + 1;
            var dot_index = filePath.LastIndexOf(".", StringComparison.CurrentCulture);
            var executable_name = filePath.Substring(start_index, dot_index - start_index);
            var options = new ExpressoCompilerOptions{
                OutputPath = targetFilePath,
                BuildType = BuildType.Debug | BuildType.Executable,
                ExecutableName = executable_name
            };

            var generator = new CodeGenerator(parser, options);
            generator.VisitAst(ast, null);

            return executable_name;
        }

        static void TestFile(string executableName, string executableDirectoryPath = "../../test_executables", object[] parameters = null)
        {
            // In order to load external assemblies we need to use `Assembly.LoadFrom`
            var asm = Assembly.LoadFrom(Path.GetFullPath($@"{executableDirectoryPath}/{executableName}.exe"));
            var main_method = asm.EntryPoint;

            Assert.AreEqual(main_method.Name, "main");
            Assert.IsTrue(main_method.IsStatic);
            Assert.AreEqual(typeof(void), main_method.ReturnType);
            Assert.AreEqual((parameters == null) ? 0 : 1, main_method.GetParameters().Length);

            //try{
                main_method.Invoke(null, parameters);
            // Do not catch exceptions here because that would make us confused
            /*}
            catch(TargetInvocationException e){
                var inner = e.InnerException;
                Console.WriteLine(e);
                Console.WriteLine(inner);
            }*/
        }

        // This should be SetUp because TutorialTests can be run after EmitterTests
        [SetUp]
        public void Cleanup()
        {
            CodeGenerator.Symbols.Clear();
            Parser.CachedSymbolTables.Clear();
            ExpressoCompilerHelpers.SymbolTableCache.Clear();
        }

        [Test]
        public void Exsdoc()
        {
            Console.WriteLine("Test exsdoc");

            var executable_name = GenerateAssembly("../../../ExpressoPrograms/exsdoc/main.exs", "../../../ExpressoPrograms/exsdoc/bin");

            TestFile(executable_name, "../../../ExpressoPrograms/exsdoc/bin", new object[]{new []{"../../../ExpressoPrograms/exsdoc/doc_sample.exs", "../../../ExpressoPrograms/exsdoc/bin/test.html"}});
        }

        [Test]
        public void Listing6()
        {
            Console.WriteLine("Test Listing6");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing6.exs");

            TestFile(executable_name);
        }

        [Test]
        public void PalindromeTests()
        {
            Console.WriteLine("Test PalindromeTests");

            var executable_name = GenerateAssembly("../../sources/palindrome_tests.exs");

            TestFile(executable_name);
        }

        [Test]
        public void MyIntseq()
        {
            Console.WriteLine("Test MyIntseq");

            var executable_name = GenerateAssembly("../../sources/my_intseq.exs");

            TestFile(executable_name);
        }

        [Test]
        public void Listing13_5()
        {
            Console.WriteLine("Test Listing 13-5");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing13-5.exs");

            TestFile(executable_name);
        }

        [Test]
        public void UsesOfMyOption()
        {
            Console.WriteLine("Test UsesOfMyOption");

            var executable_name = GenerateAssembly("../../sources/uses_of_myoption.exs");

            TestFile(executable_name);
        }

        [Test]
        public void ExsdocLexer()
        {
            Console.WriteLine("Test exsdoc lexer");

            var executable_name = GenerateAssembly("../../../ExpressoPrograms/exsdoc/main_lexer.exs", "../../../ExpressoPrograms/exsdoc/bin");

            TestFile(executable_name, "../../../ExpressoPrograms/exsdoc/bin", new object[]{new []{"../../../ExpressoPrograms/exsdoc/doc_sample.exs"}});
        }

        [Test]
        public void Listing13_3()
        {
            Console.WriteLine("Test Listing13-3");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing13-3.exs");

            TestFile(executable_name);
        }

        [Test]
        public void UsesOfRawValueEnum()
        {
            Console.WriteLine("Test UsesOfRawValueEnum");

            var executable_name = GenerateAssembly("../../sources/uses_of_raw_value_enum.exs");

            TestFile(executable_name);
        }

        [Test]
        public void ExsdocParser()
        {
            Console.WriteLine("Test exsdoc parser");

            var executable_name = GenerateAssembly("../../../ExpressoPrograms/exsdoc/main_parser.exs", "../../../ExpressoPrograms/exsdoc/bin");

            TestFile(executable_name, "../../../ExpressoPrograms/exsdoc/bin", new object[]{new []{"../../../ExpressoPrograms/exsdoc/doc_sample.exs"}});
        }

        [Test]
        public void Listing1_2()
        {
            Console.WriteLine("Test Listing1-2");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing1-2.exs");

            TestFile(executable_name);
        }

        [Test]
        public void Listing2_2()
        {
            Console.WriteLine("Test Listing2-2");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing2-2.exs");

            TestFile(executable_name);
        }

        [Test]
        public void Listing2_7()
        {
            Console.WriteLine("Test Listing2-7");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing2-7.exs");

            TestFile(executable_name);
        }

        [Test]
        public void Listing8_1()
        {
            Console.WriteLine("Test Listing8-1");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing8-1.exs");

            TestFile(executable_name);
        }

        [Test]
        public void Listing8_2()
        {
            Console.WriteLine("Test Listing8-2");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing8-2.exs");
            
            TestFile(executable_name);
        }

        [Test]
        public void Listing8_3()
        {
            Console.WriteLine("Test Listing8-3");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing8-3.exs");

            TestFile(executable_name);
        }

        [Test]
        public void Listing2_5()
        {
            Console.WriteLine("Test Listing2-5");

            var executable_name = GenerateAssembly("../../sources/TutorialExamples/Listing2-5.exs");

            TestFile(executable_name);
        }

        [Test]
        public void Test2()
        {
            Console.WriteLine("Test Test2");

            var executable_name = GenerateAssembly("../../sources/test2.exs");

            TestFile(executable_name);
        }

        [Test]
        public void ModifyModuleVariable()
        {
            Console.WriteLine("Test ModifyModuleVariable");

            var executable_name = GenerateAssembly("../../sources/modify_module_variable.exs");

            TestFile(executable_name);
        }
    }
}

