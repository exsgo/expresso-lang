﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

using Expresso.CodeGen;

using NUnit.Framework;

namespace Expresso.Test
{
    [TestFixture]
    public class EmitterTests
    {
        static int index = 0;
        static void GenerateAssembly(string filePath)
        {
            Console.WriteLine("Test{0:D3}", index);
            var parser = new Parser(new Scanner(filePath)){
                DoPostParseProcessing = true
            };
            parser.Parse();

            var ast = parser.TopmostAst;
            var options = new ExpressoCompilerOptions{
                OutputPath = "../../test_executables",
                BuildType = BuildType.Debug | BuildType.Executable,
                ExecutableName = "main" + index.ToString("D3")
            };

            var generator = new CodeGenerator(parser, options);
            generator.VisitAst(ast, null);
        }

        static void TestFile(object[][] parameters = null)
        {
            var asm = Assembly.LoadFrom(Path.GetFullPath(string.Format(@"../../test_executables/main{0:D3}.exe", index)));
            var main_method = asm.EntryPoint;
            Assert.AreEqual(main_method.Name, "main");
            Assert.IsTrue(main_method.IsStatic);
            Assert.AreEqual(typeof(void), main_method.ReturnType);
            Assert.AreEqual((parameters == null) ? 0 : 1, main_method.GetParameters().Length);

            main_method.Invoke(null, parameters);
        }

        [TearDown]
        public void Cleanup()
        {
            CodeGenerator.Symbols.Clear();
            Parser.CachedSymbolTables.Clear();
            ExpressoCompilerHelpers.SymbolTableCache.Clear();
            ++index;
        }

        [Test]
        public void SimpleLiterals()
        {
            Console.WriteLine("Test SimpleLiterals");

            GenerateAssembly("../../sources/for_unit_tests/simple_literals.exs");

            TestFile();
        }

        [Test]
        public void SimpleArithmetic()
        {
            Console.WriteLine("Test SimpleArithmetic");

            GenerateAssembly("../../sources/for_unit_tests/simple_arithmetic.exs");

            TestFile();
        }

        [Test]
        public void GeneralExpressions()
        {
            Console.WriteLine("Test GeneralExpressions");

            GenerateAssembly("../../sources/for_unit_tests/general_expressions.exs");

            TestFile();
        }

        [Test]
        public void BasicStatements()
        {
            Console.WriteLine("Test BasicStatements");

            GenerateAssembly("../../sources/for_unit_tests/basic_statements.exs");

            TestFile();
        }

        /*[Test]
        public void ComplexExpressions()
        {
            Console.WriteLine("Test ComplextExpressions");

            GenerateAssembly("../../sources/for_unit_tests/complex_expressions.exs");

            TestFile();
        }*/

        [Test]
        public void MatchStatements()
        {
            Console.WriteLine("Test MatchStatements");

            GenerateAssembly("../../sources/for_unit_tests/match_statements.exs");

            TestFile();
        }

        [Test]
        public void FunctionsAndRecursiveCall()
        {
            Console.WriteLine("Test FunctionsAndRecursiveCall");

            GenerateAssembly("../../sources/for_unit_tests/functions_and_recursive_call.exs");

            TestFile();
        }

        [Test]
        public void Class()
        {
            Console.WriteLine("Test Class");

            GenerateAssembly("../../sources/for_unit_tests/class.exs");

            TestFile();
        }

        [Test]
        public void Module()
        {
            Console.WriteLine("Test Module");

            GenerateAssembly("../../sources/for_unit_tests/module.exs");

            TestFile();
        }

        [Test]
        public void Closures()
        {
            Console.WriteLine("Test Closures");

            GenerateAssembly("../../sources/for_unit_tests/closures.exs");

            TestFile();
        }

        [Test]
        public void ClosuresWithCompoundStatements()
        {
            Console.WriteLine("Test ClosuresWithCompoundStatements");

            GenerateAssembly("../../sources/for_unit_tests/closures_with_compound_statements.exs");

            TestFile();
        }

        [Test]
        public void TryStatements()
        {
            Console.WriteLine("Test TryStatements");

            GenerateAssembly("../../sources/for_unit_tests/try_statements.exs");

            TestFile();
        }

        [Test]
        public void BuiltinObjects()
        {
            Console.WriteLine("Test BuiltinObjects");

            GenerateAssembly("../../sources/for_unit_tests/builtin_objects.exs");

            TestFile();
        }

        [Test]
        public void Interface()
        {
            Console.WriteLine("Test Interface");

            GenerateAssembly("../../sources/for_unit_tests/interface.exs");

            TestFile();
        }

        [Test]
        public void Slices()
        {
            Console.WriteLine("Test Slices");

            GenerateAssembly("../../sources/for_unit_tests/slices.exs");

            TestFile();
        }

        [Test]
        public void TypeCast()
        {
            Console.WriteLine("Test TypeCast");

            GenerateAssembly("../../sources/for_unit_tests/type_cast.exs");

            TestFile();
        }

        [Test]
        public void UseOfTheStandardLibrary()
        {
            Console.WriteLine("Test UseOfTheStandardLibrary");

            GenerateAssembly("../../sources/for_unit_tests/use_of_the_standard_library.exs");

            TestFile();
        }

        [Test]
        public void VariousStrings()
        {
            Console.WriteLine("Test VariousStrings");

            GenerateAssembly("../../sources/for_unit_tests/various_strings.exs");

            TestFile();
        }

        [Test]
        public void Module2()
        {
            Console.WriteLine("Test Module2");

            GenerateAssembly("../../sources/for_unit_tests/module2.exs");

            TestFile();
        }

        [Test]
        public void InteroperabilityTestWithCSharp()
        {
            Console.WriteLine("Test InteroperabilityTestWithCSharp");

            GenerateAssembly("../../sources/for_unit_tests/interoperability_test_with_csharp.exs");

            TestFile();
        }

        [Test]
        public void AdvancedForLoops()
        {
            Console.WriteLine("Test AdvancedForLoops");

            GenerateAssembly("../../sources/for_unit_tests/advanced_for_loops.exs");

            TestFile();
        }

        [Test]
        public void PropertyTests()
        {
            Console.WriteLine("Test PropertyTests");

            GenerateAssembly("../../sources/for_unit_tests/property_tests.exs");

            TestFile();
        }

        [Test]
        public void TestEnumInCSharp()
        {
            Console.WriteLine("Test TestEnumInCSharp");

            GenerateAssembly("../../sources/for_unit_tests/test_enum_in_csharp.exs");

            TestFile();
        }

        [Test]
        public void TestReference()
        {
            Console.WriteLine("Test TestReference");

            GenerateAssembly("../../sources/for_unit_tests/test_reference.exs");

            TestFile();
        }

        [Test]
        public void Attributes()
        {
            Console.WriteLine("Test Attributes");

            GenerateAssembly("../../sources/for_unit_tests/attributes.exs");

            var asm = Assembly.LoadFrom(Path.GetFullPath(string.Format("../../test_executables/main{0:D3}.exe", index)));
            var main_method = asm.EntryPoint;
            Assert.AreEqual(main_method.Name, "main");
            Assert.IsTrue(main_method.IsStatic);
            Assert.AreEqual(typeof(void), main_method.ReturnType);
            Assert.AreEqual(0, main_method.GetParameters().Length);

            var attribute1 = asm.GetCustomAttribute<AssemblyDescriptionAttribute>();
            var type1 = attribute1.GetType();
            Assert.IsNotNull(attribute1);
            Assert.AreEqual(type1.Name, "AssemblyDescriptionAttribute");

#if WINDOWS
            var module = asm.GetModule(string.Format("main{0:D3}.exe", index));
#else
            var module = asm.GetModule(string.Format("main{0:D3}", index));
#endif
            var author_attribute_type = module.GetType("AuthorAttribute");
            var attribute2 = module.GetCustomAttributes(true).First();
            var type2 = attribute2.GetType();
            Assert.IsNotNull(attribute2);
            Assert.AreEqual(type2.Name, "CLSCompliantAttribute");

            var test_type = module.GetType("AttributeTest");
            var attribute3 = test_type.GetCustomAttribute<SerializableAttribute>();
            var type3 = attribute3.GetType();
            Assert.IsNotNull(attribute3);
            Assert.AreEqual(type3.Name, "SerializableAttribute");

            var do_something_method = test_type.GetMethod("doSomething");
            var attribute4 = do_something_method.GetCustomAttribute<ConditionalAttribute>();
            var type4 = attribute4.GetType();
            Assert.IsNotNull(attribute4);
            Assert.AreEqual(type4.Name, "ConditionalAttribute");

            var x_field = test_type.GetField("_x", BindingFlags.NonPublic | BindingFlags.Instance);
            var attribute5 = x_field.GetCustomAttribute<ObsoleteAttribute>();
            var type5 = attribute5.GetType();
            Assert.IsNotNull(attribute5);
            Assert.AreEqual(type5.Name, "ObsoleteAttribute");

            var module_type = module.GetType("Main");
            var do_something_in_module_method = module_type.GetMethod("doSomethingInModule", BindingFlags.NonPublic | BindingFlags.Static);
            var attribute6 = do_something_in_module_method.GetCustomAttribute<ObsoleteAttribute>();
            var type6 = attribute6.GetType();
            Assert.IsNotNull(attribute6);
            Assert.AreEqual(type6.Name, "ObsoleteAttribute");

            var y_field = module_type.GetField("_y", BindingFlags.NonPublic | BindingFlags.Static);
            var attribute7 = y_field.GetCustomAttribute(author_attribute_type);
            var type7 = attribute7.GetType();
            Assert.IsNotNull(attribute7);
            Assert.AreEqual(type7.Name, "AuthorAttribute");

            var params_of_do_something_method = do_something_method.GetParameters();
            var attribute8 = params_of_do_something_method[0].GetCustomAttribute(author_attribute_type);
            var type8 = attribute8.GetType();
            Assert.IsNotNull(attribute8);
            Assert.AreEqual(type8.Name, "AuthorAttribute");

            var do_something2_method = test_type.GetMethod("doSomething2");
            var attribute9 = do_something2_method.ReturnParameter.GetCustomAttribute(author_attribute_type);
            var type9 = attribute9.GetType();
            Assert.IsNotNull(attribute9);
            Assert.AreEqual(type9.Name, "AuthorAttribute");

            main_method.Invoke(null, new object[]{});
        }

        [Test]
        public void Enum1()
        {
            Console.WriteLine("Test Enum1");

            GenerateAssembly("../../sources/for_unit_tests/enum1.exs");

            TestFile();
        }

        [Test]
        public void Enum2()
        {
            Console.WriteLine("Test Enum2");

            GenerateAssembly("../../sources/for_unit_tests/enum2.exs");

            TestFile();
        }

        [Test]
        public void GenericClass()
        {
            Console.WriteLine("Test GenericClass");

            GenerateAssembly("../../sources/for_unit_tests/generic_class.exs");

            TestFile();
        }

        [Test]
        public void ConditionalXXOperators()
        {
            Console.WriteLine("Test ConditionalXXOperators");

            GenerateAssembly("../../sources/for_unit_tests/conditional_xx_operators.exs");

            TestFile();
        }

        [Test]
        public void HelloWorld()
        {
            Console.WriteLine("Test HelloWorld");

            GenerateAssembly("../../sources/for_unit_tests/hello_world.exs");

            TestFile();
        }

        [Test]
        public void ImportGenericTypes()
        {
            Console.WriteLine("Test ImportGenericTypes");

            GenerateAssembly("../../sources/for_unit_tests/import_generic_types.exs");

            TestFile();
        }

        [Test]
        public void PrimitiveCasts()
        {
            Console.WriteLine("Test Casts");

            GenerateAssembly("../../sources/for_unit_tests/primitive_casts.exs");

            TestFile();
        }

        [Test]
        public void InterfaceAndUpcast()
        {
            Console.WriteLine("Test InterfaceAndUpcast");

            GenerateAssembly("../../sources/for_unit_tests/interface_and_upcast.exs");

            TestFile();
        }

        [Test]
        public void ArgsOnMain()
        {
            Console.WriteLine("Test ArgsOnMain");

            GenerateAssembly("../../sources/for_unit_tests/args_on_main.exs");

            TestFile(new []{new []{"parameter", "second"}});
        }

        [Test]
        public void GenericMethod()
        {
            Console.WriteLine("Test GenericMethod");

            GenerateAssembly("../../sources/for_unit_tests/generic_method.exs");

            TestFile();
        }

        [Test]
        public void ClassAndMethodModifiers()
        {
            Console.WriteLine("Test ClassAndMethodModifiers");

            GenerateAssembly("../../sources/for_unit_tests/class_and_method_modifiers.exs");

            TestFile();
        }

        [Test]
        public void Downcast()
        {
            Console.WriteLine("Test Downcast");

            GenerateAssembly("../../sources/for_unit_tests/downcast.exs");

            TestFile();
        }

        [Test]
        public void GenericClassWithWhere()
        {
            Console.WriteLine("Test GenericClassWithWhere");

            GenerateAssembly("../../sources/for_unit_tests/generic_class_with_where.exs");

            TestFile();
        }

        [Test]
        public void FunctionType()
        {
            Console.WriteLine("Test FunctionType");

            GenerateAssembly("../../sources/for_unit_tests/function_type.exs");

            TestFile();
        }

        [Test]
        public void ParametersInferredFromInitialValues()
        {
            Console.WriteLine("Test ParametersInferredFromInitialValues");

            GenerateAssembly("../../sources/for_unit_tests/parameters_inferred_from_initial_values.exs");

            TestFile();
        }

        /*[Test]
        public void FunctionsAsValues()
        {
            Console.WriteLine("Test FunctionsAsValues");

            GenerateAssembly("../../sources/for_unit_tests/functions_as_values.exs");

            TestFile();
        }*/

        [Test]
        public void GenericMethodWithWhere()
        {
            Console.WriteLine("Test GenericMethodWithWhere");

            GenerateAssembly("../../sources/for_unit_tests/generic_method_with_where.exs");

            TestFile();
        }

        [Test]
        public void EnumGenerics()
        {
            Console.WriteLine("Test EnumGenerics");

            GenerateAssembly("../../sources/for_unit_tests/enum_generics.exs");

            TestFile();
        }

        [Test]
        public void AssignmentUsingIndexer()
        {
            Console.WriteLine("Test AssignmentUsingIndexer");

            GenerateAssembly("../../sources/for_unit_tests/assignment_using_indexer.exs");

            TestFile();
        }

        [Test]
        public void NotebehaviorParenthesizedPatterns()
        {
            Console.WriteLine("Test ParenthesizedPatterns");

            GenerateAssembly("../../sources/for_unit_tests/parenthesized_patterns.exs");

            TestFile();
        }

        [Test]
        public void UsingFieldOnConcreteTypeInEnum()
        {
            Console.WriteLine("Test UsingFieldOnConcreteTypeInEnum");

            GenerateAssembly("../../sources/for_unit_tests/using_field_on_concrete_type_in_enum.exs");

            TestFile();
        }

        [Test]
        public void UsingFieldOnConcreteTypeInClassWithWhere()
        {
            Console.WriteLine("Test UsingFieldOnConcreteTypeInClassWithWhere");

            GenerateAssembly("../../sources/for_unit_tests/using_field_on_concrete_type_in_class_with_where.exs");

            TestFile();
        }

        [Test]
        public void ReturningSelfType()
        {
            Console.WriteLine("Test ReturningSelfType");

            GenerateAssembly("../../sources/for_unit_tests/returning_self_type.exs");

            TestFile();
        }

        [Test]
        public void SelfClassAsParameter()
        {
            Console.WriteLine("Test SelfClassAsParameter");

            GenerateAssembly("../../sources/for_unit_tests/self_class_as_parameter.exs");

            TestFile();
        }

        [Test]
        public void UsingFieldOnConcreteTypeInEnumOutside()
        {
            Console.WriteLine("Test UsingFieldOnConcreteTypeInEnumOutside");

            GenerateAssembly("../../sources/for_unit_tests/using_field_on_concrete_type_in_enum_outside.exs");

            TestFile();
        }

        [Test]
        public void UsingFieldOnConcreteTypeInClass()
        {
            Console.WriteLine("Test UsingFieldOnConcreteTypeInClass");

            GenerateAssembly("../../sources/for_unit_tests/using_field_on_concrete_type_in_class.exs");

            TestFile();
        }

        [Test]
        public void SelfClassAsField()
        {
            Console.WriteLine("Test SelfClassAsField");

            GenerateAssembly("../../sources/for_unit_tests/self_class_as_field.exs");

            TestFile();
        }

        [Test]
        public void TypesPointAtEachOther()
        {
            Console.WriteLine("Test TypesPointAtEachOther");

            GenerateAssembly("../../sources/for_unit_tests/types_point_at_each_other.exs");

            TestFile();
        }

        [Test]
        public void EnumAndTypePointAtEachOther()
        {
            Console.WriteLine("Test EnumAndTypePointAtEachOther");

            GenerateAssembly("../../sources/for_unit_tests/enum_and_type_point_at_each_other.exs");

            TestFile();
        }

        [Test]
        public void CallModuleFunctionAfterMethod()
        {
            Console.WriteLine("Test CallModuleFunctionAfterMethod");

            GenerateAssembly("../../sources/for_unit_tests/call_module_function_after_method.exs");

            TestFile();
        }

        [Test]
        public void AdvancedFunctionCalls()
        {
            Console.WriteLine("Test AdvancedFunctionCalls");

            GenerateAssembly("../../sources/for_unit_tests/advanced_function_calls.exs");

            TestFile();
        }

        [Test]
        public void IndirectlyUsingUnexportedItem()
        {
            Console.WriteLine("Test IndirectlyUsingUnexportedItem");

            GenerateAssembly("../../sources/for_unit_tests/indirectly_using_unexported_item.exs");

            TestFile();
        }

        [Test]
        public void LazyInitedArray()
        {
            Console.WriteLine("Test Lazy-InitedArray");

            GenerateAssembly("../../sources/for_unit_tests/lazy-inited_array.exs");

            TestFile();
        }

        [Test]
        public void ImplementingTypeDefinedInExternalModule()
        {
            Console.WriteLine("Test ImplementingTypeDefinedInExternalModule");

            GenerateAssembly("../../sources/for_unit_tests/implementing_type_defined_in_external_module.exs");

            TestFile();
        }

        [Test]
        public void CallMethodDefinedAfterCurrentMethod()
        {
            Console.WriteLine("Test CallMethodDefinedAfterCurrentMethod");

            GenerateAssembly("../../sources/for_unit_tests/call_method_defined_after_current_method.exs");

            TestFile();
        }

        [Test]
        public void TestArrangementOfCtorParameters()
        {
            Console.WriteLine("Test TestArrangementOfCtorParameters");

            GenerateAssembly("../../sources/for_unit_tests/test_arrangement_of_ctor_parameters.exs");

            TestFile();
        }

        [Test]
        public void LazyInitVariable()
        {
            Console.WriteLine("Test Lazy-InitVariable");

            GenerateAssembly("../../sources/for_unit_tests/lazy-init_variable.exs");

            TestFile();
        }

        [Test]
        public void UsingUseGuide()
        {
            Console.WriteLine("Test UsingUseGuide");

            GenerateAssembly("../../sources/for_unit_tests/using_use_guide.exs");

            TestFile();
        }

        [Test]
        public void EnumsPointAtEachOther()
        {
            Console.WriteLine("Test EnumsPointAtEachOther");

            GenerateAssembly("../../sources/for_unit_tests/enums_point_at_each_other.exs");

            TestFile();
        }

        [Test]
        public void TestStd()
        {
            Console.WriteLine("Test TestStd");

            GenerateAssembly("../../sources/for_unit_tests/test_std.exs");

            TestFile();
        }

        [Test]
        public void CallPrimitiveTypesStaticMethods()
        {
            Console.WriteLine("Test CallPrimitiveTypes'StaticMethods");

            GenerateAssembly("../../sources/for_unit_tests/call_primitive_types_static_methods.exs");

            TestFile();
        }

        [Test]
        public void MatchGuardActuallyTakenIntoAccount()
        {
            Console.WriteLine("Test MatchGuardActuallyTakenIntoAccount");

            GenerateAssembly("../../sources/for_unit_tests/match_guard_actually_taken_into_account.exs");

            TestFile();
        }

        [Test]
        public void ExtensionMethods()
        {
            Console.WriteLine("Test ExtensionMethods");

            GenerateAssembly("../../sources/for_unit_tests/extension_methods.exs");

            TestFile();
        }

        [Test]
        public void InterfaceImplementingInterface()
        {
            Console.WriteLine("Test InterfaceImplementingInterface");

            GenerateAssembly("../../sources/for_unit_tests/interface_implementing_interface.exs");

            TestFile();
        }

        [Test]
        public void InterfaceConstraint()
        {
            Console.WriteLine("Test InterfaceConstraint");

            GenerateAssembly("../../sources/for_unit_tests/interface_constraint.exs");

            TestFile();
        }

        [Test]
        public void UsingGenericsOnClosure()
        {
            Console.WriteLine("Test UsingGenericsOnClosure");

            GenerateAssembly("../../sources/for_unit_tests/using_generics_on_closure.exs");

            TestFile();
        }

        [Test]
        public void TestEqualityOnRawValueEnum()
        {
            Console.WriteLine("Test TestEqualityOnRawValueEnum");

            GenerateAssembly("../../sources/for_unit_tests/test_equality_on_raw_value_enum.exs");

            TestFile();
        }

        [Test]
        public void OptionFunctionalities()
        {
            Console.WriteLine("Test OptionFunctionalities");

            GenerateAssembly("../../sources/for_unit_tests/option_functionalities.exs");

            TestFile();
        }

        [Test]
        public void StaticMethodWithClosuresThatTakeTypeParameters()
        {
            Console.WriteLine("Test StaticMethodWithClosuresThatTakeTypeParameters");

            GenerateAssembly("../../sources/for_unit_tests/static_method_with_closures_that_take_type_parameters.exs");

            TestFile();
        }

        [Test]
        public void ResultFunctionalities()
        {
            Console.WriteLine("Test ResultFunctionalities");

            GenerateAssembly("../../sources/for_unit_tests/result_functionalities.exs");

            TestFile();
        }

        [Test]
        public void TestingIfLet()
        {
            Console.WriteLine("Test TestingIfLet");

            GenerateAssembly("../../sources/for_unit_tests/testing_if_let.exs");

            TestFile();
        }

        [Test]
        public void TestingIfVar()
        {
            Console.WriteLine("Test TestingIfVar");

            GenerateAssembly("../../sources/for_unit_tests/testing_if_var.exs");

            TestFile();
        }

        [Test]
        public void UsingUseGuideInAComplicatedWay()
        {
            Console.WriteLine("Test UsingUseGuideInAComplicatedWay");

            GenerateAssembly("../../sources/for_unit_tests/using_use_guide_in_a_complicated_way.exs");

            TestFile();
        }

        [Test]
        public void TestingFriendMethod()
        {
            Console.WriteLine("Test TestingFriendMethod");

            GenerateAssembly("../../sources/for_unit_tests/testing_friend_method.exs");

            TestFile();
        }

        [Test]
        public void TestingIfLetWithMismatchingValue()
        {
            Console.WriteLine("Test TestingIfLetWithMismatchingValue");

            GenerateAssembly("../../sources/for_unit_tests/testing_if_let_with_mismatching_value.exs");

            TestFile();
        }

        [Test]
        public void Globs()
        {
            Console.WriteLine("Test Globs");

            GenerateAssembly("../../sources/for_unit_tests/globs.exs");

            TestFile();
        }

        [Test]
        public void ImportClassesThatReferenceOtherDllOnCsharp()
        {
            Console.WriteLine("Test ImportClassesThatReferenceOtherDllOnCsharp");

            GenerateAssembly("../../sources/for_unit_tests/import_classes_that_reference_other_dll_on_csharp.exs");

            TestFile();
        }

        [Test]
        public void NullInCallExpression()
        {
            Console.WriteLine("Test NullInCallExpression");

            GenerateAssembly("../../sources/for_unit_tests/null_in_call_expression.exs");

            TestFile();
        }
    }
}

