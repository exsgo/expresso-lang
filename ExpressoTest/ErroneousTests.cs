﻿using Expresso.Ast.Analysis;
using Expresso.CodeGen;
using NUnit.Framework;

namespace Expresso.Test
{
    [TestFixture]
    public class ErroneousTests
    {
        [Test]
        public void Literals1()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/literals1.exs")){
                DoPostParseProcessing = true
            };

#if DEBUG
            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0003"));
#else
            Assert.That(parser.Parse, Throws.TypeOf<FatalError>().With.Message.Contains("Invalid syntax found"));
#endif
            Assert.AreEqual(2, parser.errors.count);
        }

        [Test]
        public void Literals2()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/literals2.exs")) {
                DoPostParseProcessing = true
            };

#if DEBUG
            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0003"));
#else
            Assert.That(parser.Parse, Throws.TypeOf<FatalError>().With.Message.Contains("Invalid syntax found"));
#endif
            Assert.AreEqual(3, parser.errors.count);
        }

        [Test]
        public void Reassignment()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/reassignment.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1900"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InterfaceNotImplemented()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/interface_not_implemented.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1910"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UndefinedFunction()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/undefined_function.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0102"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void HasNoReturnTypeInInterface()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/has_no_return_type_in_interface.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1602"));
            Assert.AreEqual(2, parser.errors.count);
        }

        // abandoned
        /*[Test]
        public void CannotCallFunctionBeforeDeclared()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_call_function_before_declared.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0120"///*"ES1111"));
            Assert.AreEqual(1, parser.errors.count);
        }*/

        [Test]
        public void InvalidIterable()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_iterable.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1301"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidConditionalExpressionWithVariable()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_conditional_expression_with_variable.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4000"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ReassigningDifferentType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/reassigning_different_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1100"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidIntseqExpression()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_intseq_expression.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4001"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidOptionalValue()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_optional_value.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1110"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidUseOfNotOperator()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_use_of_not_operator.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1200"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidUseOfPlusOperator()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_use_of_plus_operator.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3200"/*"ES1201"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedOnValueOfObjectCreation()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_on_value_of_object_creation.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2003"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingField()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_field.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1502"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingTypeDeclaration()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_type_declaration.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidSliceOnDictionary()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_slice_on_dictionary.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3012"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidSliceOnCustomType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_slice_on_custom_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3011"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidCasting()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_casting.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1003"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallAVariable()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_a_variable.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1802"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidBinaryOperator()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_binary_operator.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1002"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InconsistentTypes()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/inconsistent_types.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NotCompatibleWithRightHandSide()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/not_compatible_with_right_hand_side.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1100"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NotCompatibleOnConditionalExpression()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/not_compatible_on_conditional_expression.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1005"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedOnMatchStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_on_match_statement.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1022"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidConditionalOnIfStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_conditional_on_if_statement.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4000"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidBreakStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_break_statement.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4010"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidContinueStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_continue_statement.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4011"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingElementType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_element_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1310"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UnknownField()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/unknown_field.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2001"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void PrivateAccessibility()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/private_accessibility.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ProtectedAccessibility()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/protected_accessibility.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3301"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MemberImmutability()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/member_immutability.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1902"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void DifferentAccessModifierOnMethods()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/different_access_modifier_on_methods.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1030"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedOnArgument()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_on_argument.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1303"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void PrimitiveTypesCannotBeDerived()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/primitive_types_cannot_be_derived.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1913"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CannotBeDerivedFromVectorType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_be_derived_from_vector_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1913"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidUseOfNull()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_use_of_null.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1020"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallMutatingMethodOnImmutableVarible()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_mutating_method_on_immutable_variable.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2100"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImportUnexportedItem()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/import_unexported_item.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3302"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImportUndefiedSymbol()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/import_undefined_symbol.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0103"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void OutOfIntRangeIntSeq()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/out_of_int_range_intseq.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4002"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void IntseqWarning1()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/intseq_warning1.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4003"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void IntseqWarning2()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/intseq_warning2.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4004"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void IntseqWarning3()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/intseq_warning3.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4005"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void IntseqWarning4()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/intseq_warning4.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4006"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidUseOfIndexerOperator()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_use_of_indexer_operator.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3013"));
            Assert.AreEqual(1, parser.errors.count);
        }

        // not being used
        /*[Test]
        public void MemberOfNotAType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/member_of_not_a_type.exs")){
                DoPostParseProcessing = true
            };
            parser.Parse();

            Assert.AreEqual(2, parser.errors.count);
        }*/

        [Test]
        public void KeywordIdentifier()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/keyword_identifier.exs")){
                DoPostParseProcessing = true
            };

            parser.Parse();
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImportFromUnknownFile()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/import_from_unknown_file.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0006"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NonOptionalParameterAfterOptionalParameter()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/non_optional_parameter_after_optional_parameter.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0002"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidVariadicParameter()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_variadic_parameter.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0010"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NonArrayVariadicParameter()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/non_array_variadic_parameter.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0001"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypeAnnotationAndOptionalValueOmitted()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/type_annotation_and_optional_value_omitted.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0004"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void FieldOfTuple()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/field_of_tuple.exs")){
                DoPostParseProcessing = true
            };

            #if !DEBUG
            Assert.That(parser.Parse, Throws.TypeOf<FatalError>().With.Message.Contains("Invalid syntax found"));
            #else
            Assert.That(parser.Parse, Throws.TypeOf<System.NullReferenceException>());
            #endif
            Assert.AreEqual(2, parser.errors.count);
        }

        [Test]
        public void NotBalancedAugmentedAssignment()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/not_balanced_augmented_assignment.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0008"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UntypedCatchIdentifier()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/untyped_catch_identifier.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0011"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NoContextLet()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/no_context_let.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0003"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UnbalancedRawString()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/unbalanced_raw_string.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0005"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingMainFunction()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_main_function.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4020"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UsingBeforeDeclared()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/using_before_declared.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0120"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImportUndefinedTypeFromSystem()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/import_undefined_type_from_system.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES5000"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ShadowModuleVariableWithLocalBinding()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/shadow_module_variable_with_local_binding.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3100"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ShadowModuleVariableWithForBinding()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/shadow_module_variable_with_for_binding.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3100"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void StringInterpolationError()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/string_interpolation_error.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0102"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingClosureParameterTypes()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_closure_parameter_types.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3010"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ObjectCreationOnAttribute()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/object_creation_on_attribute.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4022"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void DuplicateRawValueEnumMember()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/duplicate_raw_value_enum_member.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4030"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MultipleStyleEnum()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/multiple_style_enum.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4031"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void AssignmentOnParameter()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/assignment_on_parameter.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1905"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MultipleItemsOnAugmentedAssignment()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/multiple_items_on_augmented_assignment.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2101"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedAtForStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_at_for_statement.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidPatternInForStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_pattern_in_for_statement.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4040"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void AddingBigintToInt()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/adding_bigint_to_int.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1002"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void BaseClassNotInLastPosition()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/base_class_not_in_last_position.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1011"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InterfaceDerivedFromClass()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/interface_derived_from_class.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1010"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ParentClassMissing()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/parent_class_missing.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"/*"ES1912"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooFewTypeArguments()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_few_type_arguments.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2020"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooManyTypeArguments()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_many_type_arguments.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2021"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedInTypeArgumentsAndArguments()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_in_type_arguments_and_arguments.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2022"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CannotTakeReferenceOnLetDeclarations()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_take_reference_on_let_declarations.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1904"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NoOverridableMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/no_overridable_method.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1031"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NonVirtualMethodCannotOverride()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/non_virtual_method_cannot_override.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1032"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void StringEnumInitializer()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/string_enum_initializer.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0140"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MoreThan1ParentClass()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/more_than_1_parent_class.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0130"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void SealedClassesCannotBeDerived()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/sealed_classes_cannot_be_derived.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1914"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void AbstractMethodInOrdinalClass()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/abstract_method_in_ordinal_class.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0150"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void AbstractClassCannotBeInstantiated()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/abstract_class_cannot_be_instantiated.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1401"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InterfaceCannotBeInstantiated()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/interface_cannot_be_instantiated.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1400"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CastTargetTypeIsMissing()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cast_target_type_is_missing.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypeAnnotationConflictedWithExpressionType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/type_annotation_conflicted_with_expression_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1304"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ConstraintUndefinedType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/constraint_undefined_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void FunctionInStringInterpolationReturnsNothing()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/function_in_string_interpolation_returns_nothing.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1801"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallUndefinedMethodOnTypeParameterInClass()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_undefined_method_on_type_parameter_in_class.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2004"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallUndefinedMethodOnTypeParameterInMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_undefined_method_on_type_parameter_in_method.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2200"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UsingUndeclaredTypeParameter()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/using_undeclared_type_parameter.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"/*"ES2000"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallUndefinedOverload()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_undefined_overload.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1601"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingSelf()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_self.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ReturnTypeMissingOnCalledMethodDefinedAfterCurrentMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/return_type_missing_on_called_method_defined_after_current_method.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4101"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NeedsTypeConstraint()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/needs_type_constraint.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2201"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ReturnTypeIsDifferent()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/return_type_is_different.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1610"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CannotUseFieldWithGenericType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_use_field_with_generic_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2201"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingArgOnConstructor()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_arg_on_constructor.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2010"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MethodDefinedInTheNextTypeWhichIsNotDefined()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/method_defined_in_the_next_type_which_is_not_defined.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"/*"ES0123"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedBetweenTypeArgumentsAndArgumentsInEnumConstruction()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_between_type_arguments_and_arguments_in_enum_construction.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2022"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void VariantNameMissingInEnumConstruction()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/variant_name_missing_in_enum_construction.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2400"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooFewArgumentsInEnumConstrcution()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_few_arguments_in_enum_construction.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2011"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooManyArgumentsInEnumConstrcution()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_many_arguments_in_enum_construction.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2012"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedInEnumConstruction()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_in_enum_construction.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2013"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypelessParameterWithoutOption()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/typeless_parameter_without_option.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0004"/*"ES1310"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void RecursiveFunctionSignatureNotComplete()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/recursive_function_signature_not_complete.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4100"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NonAbstractMethodWithoutBody()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/non_abstract_method_without_body.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0030"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooManyArgumentCallExpresso()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_many_argument_call_expresso.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2031"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooFewArgumentCallExpressoOptional()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_few_argument_call_expresso_optional.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2030"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooFewArgumentCallExpressoWithoutOptional()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_few_argument_call_expresso_without_optional.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2030"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooManyArgumentCallCsharp()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_many_argument_call_csharp.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2031"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooFewArgumentCallCsharpOptional()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_few_argument_call_csharp_optional.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2030"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooFewArgumentCallCsharpWithoutOptional()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_few_argument_call_csharp_without_optional.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2030"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedInMethodWithConcreteType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_in_method_with_concrete_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1303"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UnrecommendedTypeName()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/unrecommended_type_name.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4200"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UnrecommendedFunctionName()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/unrecommended_function_name.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4201"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UnrecommendedPublicFieldName()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/unrecommended_public_field_name.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4202"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UnrecommendedPrivateFieldName()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/unrecommended_private_field_name.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4203"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallCamelCaseMethodOnSystemNamespace()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_camel_case_method_on_system_namespace.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1600"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TryStatementWithoutCatchOrFinally()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/try_statement_without_catch_or_finally.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0007"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ReturningVariableInClosureBeforeDeclared()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/returning_variable_in_closure_before_declared.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0102"/*"ES0100"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void FieldsCannotHaveDefaultValues()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/fields_cannot_have_default_values.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0110"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void AccidentallyUsingSelfInFunction()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/accidentally_using_self_in_function.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0201"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CreateInstanceOfUnknownClass()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/create_instance_of_unknown_class.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallUndefinedMethodWhichShouldBeDefinedInTheNextType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_undefined_method_which_should_be_defined_in_the_next_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1600"/*"ES0122"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TryingToCastToParentClassWhichIsMissing()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/trying_to_cast_to_parent_class_which_is_missing.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"/*"ES1004"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void AddingStringToInt()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/adding_string_to_int.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1002"/*"ES1202"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedInMatchPatterns()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_in_match_patterns.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1101"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void DictionaryLiteralWhereKeyTypesDiffer()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/dictionary_literal_where_key_types_differ.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1102"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void DictionaryLiteralWhereValueTypesDiffer()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/dictionary_literal_where_value_types_differ.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1103"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ArrayLiteralWhereItemTypesDiffer()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/array_literal_where_item_types_differ.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1104"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedInModuleVariableDefnition()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_in_module_variable_defnition.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1300"/*"ES0111"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidIterableOnForStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_iterable_on_for_statement.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1301"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ModuleVariableWithoutTypeAndInitialValue()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/module_variable_without_type_and_initial_value.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0003"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ReferringToUndefinedModuleVariableInExternalModule()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/referring_to_undefined_module_variable_in_external_module.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0102"/*"ES1700"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CreatingUndefinedClassInExternalModule()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/creating_undefined_class_in_external_module.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0102"/*"ES1501"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ConstructingUndefinedVariant()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/constructing_undefined_variant.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0102"/*"ES1503"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidArraySizeExpression()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_array_size_expression.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1710"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ReturnTypesAreDifferentInEachIfBlock()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/return_types_are_different_in_each_if_block.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1800"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void EmptyBodyStatement()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/empty_body_statement.exs")){
                DoPostParseProcessing = true
            };

            #if DEBUG
            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1901"));
            Assert.AreEqual(2, parser.errors.count);
            #else
            Assert.That(parser.Parse, Throws.TypeOf<FatalError>().With.Message.Contains("Invalid syntax found!"));
            Assert.AreEqual(1, parser.errors.count);
            #endif
        }

        [Test]
        public void CannotDeriveFromTypesDefinedInExternalModuleWithoutAlias()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_derive_from_types_defined_in_external_module_without_alias.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0100"/*"ES1911"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallUndefinedMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/call_undefined_method.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1600"/*cannot see: "ES2002"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingSomeOfTheTypeArguments()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_some_of_the_type_arguments.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2023"/*Invalid syntax error in Release build*/));
            Assert.AreEqual(2, parser.errors.count);
        }

        [Test]
        public void SyntaxErrorInStringInterpolation()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/syntax_error_in_string_interpolation.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3000"));
            Assert.AreEqual(1, parser.errors.count);    // one of the errors go to the inner_parser
        }

        [Test]
        public void DeclareTypesOfTheSameName()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/declare_types_of_the_same_name.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES3014"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void DeclareFieldsOfTheSameName()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/declare_fields_of_the_same_name.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0160"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void DeclareVariablesOfTheSameName()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/declare_variables_of_the_same_name.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0160"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidAttributeTarget()
        {
            CodeGenerator.Symbols.Clear();

            Assert.That(() => {
                var options = new ExpressoCompilerOptions{
                    ExecutableName = "erroneous",
                    BuildType = BuildType.Debug | BuildType.Executable,
                    OutputPath = "../../test_executables"
                };

                ExpressoCompilerService.CompileToAssembly("../../sources/for_unit_tests/erroneous/invalid_attribute_target.exs", options);
            }, Throws.TypeOf<ParserException>().With.Message.Contains("ES4023"));
        }

        [Test]
        public void NotAllowedAttributeTarget()
        {
            CodeGenerator.Symbols.Clear();

            Assert.That(() => {
                var options = new ExpressoCompilerOptions{
                    ExecutableName = "erroneous",
                    BuildType = BuildType.Debug | BuildType.Executable,
                    OutputPath = "../../test_executables"
                };

                ExpressoCompilerService.CompileToAssembly("../../sources/for_unit_tests/erroneous/not_allowed_attribute_target.exs", options);
            }, Throws.TypeOf<ParserException>().With.Message.Contains("ES4021"));
        }

        [Test]
        public void TypesDifferInArgumentTypeAndParameterType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_differ_in_argument_type_and_parameter_type_with_generics.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1210"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UsingClassWideTypeParameterOnlyInMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/using_class_wide_type_parameter_only_in_method.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2202"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypeIsMissingInDestructuringPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/type_is_missing_in_destructuring_pattern.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0101"/*"ES1812"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void VariantIsMissingInDestructuringPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/variant_is_missing_in_destructuring_pattern.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0102"/*"ES2401"*/));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedInEnumDestructuringPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_in_enum_destructuring_pattern.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1811"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void FieldCountMismatchedInEnumDestructuringPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/field_count_mismatched_in_enum_destructuring_pattern.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1810"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TypesMismatchedInClassDestructuringPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/types_mismatched_in_class_destructuring_pattern.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1820"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void FieldNameIsMissingInDestructuringPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/field_name_is_missing_in_destructuring_pattern.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2410"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void FieldCountMismatchedInClassDestructuringPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/field_count_mismatched_in_class_destructuring_pattern.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1821"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingUnderscoreWithString()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_underscore_with_string.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingUnderscoreWithClassInstance()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_underscore_with_class_instance.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingUnderscoreWithTuple()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_underscore_with_tuple.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UsingUninitializedVariableInPrintln()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/using_uninitialized_variable_in_println.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0200"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NotUsingVariable()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/not_using_variable.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0400"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MutatingMethodDoesNotActuallyMutateSelf()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/mutating_method_does_not_actually_mutate_self.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4110"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NonMutatingMethodChangesItsValue()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/non_mutating_method_changes_its_value.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4120"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NonMutatingMethodCallsMutatingMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/non_mutating_method_calls_mutating_method.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4120"));
            Assert.AreEqual(1, parser.errors.count);
        }

        // unable to test
        /*[Test]
        public void NonMutatingMethodAccessToMutableField()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/non_mutating_method_access_to_mutable_field.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4121"));
            Assert.AreEqual(1, parser.errors.count);
        }*/

        [Test]
        public void NotCallingMethodThatShouldBeCalledBeforehand()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/not_calling_method_that_should_be_called_beforehand.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void NotCallingMethodThatShouldBeCalledInside()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/not_calling_method_that_should_be_called_inside.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4301"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CannotApplyOperatorOnGenericType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_apply_operator_on_generic_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1007"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImpossibleTypeInferenceOnEnumObjectCreation()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/impossible_type_inference_on_enum_object_creation.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1410"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidPathWithoutGenerics()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_path_without_generics.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2500"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CannotDefineAnotherVariableOfTheSameNameInInnerScope()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_define_another_variable_of_the_same_name_in_inner_scope.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0160"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CannotDefineParameterOfTheSameNameInInnerClosure()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_define_parameter_of_the_same_name_in_inner_closure.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0160"));
            Assert.AreEqual(1, parser.errors.count);
        }

        // I can't come up with a case in which this matters
        /*[Test]
        public void CannotInferSomeOfTheTypeParameters()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/cannot_infer_some_of_the_type_parameters.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2005"));
            Assert.AreEqual(1, parser.errors.count);
        }*/

        [Test]
        public void TypeArgumentAndTypeParameterMismatchedInClosure()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/type_argument_and_type_parameter_mismatched_in_closure.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1303"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void TooManySubstringsThatContainCurlybraceAndNeedInterpolation()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/too_many_substrings_that_contain_curlybrace_and_need_interpolation.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4400"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void InvalidValueBindingPattern()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_value_binding_pattern.exs")){
                DoPostParseProcessing = true
            };

            #if DEBUG
            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0003"/*"ES0301"*/));
            #else
            Assert.That(parser.Parse, Throws.TypeOf<FatalError>().With.Message.Contains("Invalid syntax found"));
            #endif
            Assert.AreEqual(4, parser.errors.count);
        }

        [Test]
        public void InvalidUseGuides()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/invalid_use_guides.exs")){
                DoPostParseProcessing = true
            };

            #if DEBUG
            parser.Parse();
            #else
            Assert.That(parser.Parse, Throws.TypeOf<FatalError>().With.Message.Contains("Invalid syntax found"));
            #endif
            Assert.AreEqual(2, parser.errors.count);
        }

        [Test]
        public void NotCallingMethodThatShouldBeCalledBeforeIndirectly()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/not_calling_method_that_should_be_called_before_indirectly.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void RequirementOnFunction()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/requirement_on_function.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4302"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void IndirectCallsToRequiredMethodAndC()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/indirect_calls_to_required_method_and_c.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4300"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallingNonFriendMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/calling_non-friend_method.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4310"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallingInstanceMethodWithTypeReference()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/calling_instance_method_with_type_reference.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0210"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallingStaticMethodWithInstance()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/calling_static_method_with_instance.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0211"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallingExpressoInstanceMethodWithTypeReference()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/calling_expresso_instance_method_with_type_reference.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0210"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void CallingExpressoStaticMethodWithInstance()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/calling_expresso_static_method_with_instance.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES0211"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UsingDefaultParameterOnObjectType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/using_default_parameter_on_object_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES4500"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void UsingNullOnDefaultParameterOfTypeString()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/using_null_on_default_parameter_of_type_string.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1021"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void MissingImportingCastExpressionType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/missing_importing_cast_expression_type.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES2000"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImportingTypeWithoutPostfix()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/importing_type_without_postfix.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1510"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImportingTypeWithUnnecessaryPostfix()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/importing_type_with_unnecessary_postfix.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1511"));
            Assert.AreEqual(1, parser.errors.count);
        }

        [Test]
        public void ImportingTypeWithTooManyTypeParameters()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/erroneous/importing_type_with_too_many_type_parameters.exs")){
                DoPostParseProcessing = true
            };

            Assert.That(parser.Parse, Throws.TypeOf<ParserException>().With.Message.Contains("ES1512"));
            Assert.AreEqual(1, parser.errors.count);
        }
    }
}

