using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;

using Expresso.Ast;

using ICSharpCode.NRefactory;

using NUnit.Framework;

namespace Expresso.Test
{
    [SetUpFixture]
    public class SetupClass
    {
        [OneTimeSetUp]
        public void RunBeforeAnyTest()
        {
            var dir = Path.GetDirectoryName(typeof(SetupClass).Assembly.Location);
            Directory.SetCurrentDirectory(dir);
        }
    }

    [TestFixture]
    public class ParserTests
    {
        [Test]
        public void SimpleLiterals()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/simple_literals.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("a")),
                            Helpers.MakeSeq(Expression.MakeConstant("int", 255)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("h_a")),
                            Helpers.MakeSeq(Expression.MakeConstant("int", 0xff)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "h_a_",
                                    Helpers.MakePrimitiveType("int")
                                )
                            ),
                            Helpers.MakeSeq(Expression.MakeConstant("int", 0xff)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("b")),
                            Helpers.MakeSeq(Expression.MakeConstant("double", 1000.0)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f_b")),
                            Helpers.MakeSeq(Expression.MakeConstant("double", 1.0e4)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "f_b_",
                                    Helpers.MakePrimitiveType("double")
                                )
                            ),
                            Helpers.MakeSeq(Expression.MakeConstant("double", 1.0e4)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("c")),
                            Helpers.MakeSeq(Expression.MakeConstant("double", 0.001)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f_c")),
                            Helpers.MakeSeq(Expression.MakeConstant("double", .1e-2)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f_c2")),
                            Helpers.MakeSeq(Expression.MakeConstant("double", .0001)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("d")),
                            Helpers.MakeSeq(Expression.MakeConstant("bigint", BigInteger.Parse("10000000"))),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "d_",
                                    Helpers.MakePrimitiveType("bigint")
                                )
                            ),
                            Helpers.MakeSeq(Expression.MakeConstant("bigint", BigInteger.Parse("10000000"))),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("d2")),
                            Helpers.MakeSeq(Expression.MakeConstant("bigint", BigInteger.Parse("10000000"))),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("e")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "This is a test")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("u")),
                            Helpers.MakeSeq(Expression.MakeConstant("uint", 1000u)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("u_")),
                            Helpers.MakeSeq(Expression.MakeConstant("uint", 1000U)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f_a")),
                            Helpers.MakeSeq(Expression.MakeConstant("float", 1.0e4f)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "f_a_",
                                    Helpers.MakePrimitiveType("float")
                                )
                            ),
                            Helpers.MakeSeq(Expression.MakeConstant("float", 1.0e4f)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "f",
                                    Helpers.MakeGenericType("array", Helpers.MakePrimitiveType("int"))
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("array", AstType.MakePlaceholderType()),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f_")),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("array", AstType.MakePlaceholderType()),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 3)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "f2",
                                    Helpers.MakeGenericType("vector", Helpers.MakePrimitiveType("int"))
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType()),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f2_")),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType()),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 3)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        /*Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomeIdentifierPattern("f3")),
                            Helpers.MakeSeq(Expression.MakeParen(Expression.MakeSequenceExpression(null))),
                            Modifiers.Immutable
                        ),*/
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f3_")),
                            Helpers.MakeSeq(Expression.MakeParen(
                                Expression.MakeSequenceExpression(
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("string", "abc"),
                                    Expression.MakeConstant("bool", true)
                                )
                            )),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomeTuplePatternWithType(
                                    "f3_a",
                                    "f3_b",
                                    "f3_c"
                                )
                            ),
                            Helpers.MakeSeq(Helpers.MakeIdentifierPath("f3_")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "g",
                                    Helpers.MakeGenericType("dictionary", Helpers.MakePrimitiveType("string"), Helpers.MakePrimitiveType("int"))
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("dictionary", AstType.MakePlaceholderType(), AstType.MakePlaceholderType()),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("g_")),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("dictionary", AstType.MakePlaceholderType(), AstType.MakePlaceholderType()),
                                    Helpers.MakeSeq(
                                        Expression.MakeKeyValuePair(Expression.MakeConstant("string", "akari"), Expression.MakeConstant("int", 13)),
                                        Expression.MakeKeyValuePair(Expression.MakeConstant("string", "chinatsu"), Expression.MakeConstant("int", 13)),
                                        Expression.MakeKeyValuePair(Expression.MakeConstant("string", "京子"), Expression.MakeConstant("int", 14)),
                                        Expression.MakeKeyValuePair(Expression.MakeConstant("string", "結衣"), Expression.MakeConstant("int", 14))
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("h")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "私変わっちゃうの・・？")),
                            Modifiers.None
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("h2")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "Yes, you can!")),
                            Modifiers.None
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("h3")),
                            Helpers.MakeSeq(Expression.MakeConstant("char", 'a')),
                            Modifiers.None
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("i")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "よかった。私変わらないんだね！・・え、変われないの？・・・なんだかフクザツ")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("i2")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "Oh, you just can't...")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("i3")),
                            Helpers.MakeSeq(Expression.MakeConstant("char", 'a')),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("i3_"),
                            Expression.MakeConstant("char", '\''),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("i4")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "\u0041\u005a\u0061\u007A\u3042\u30A2")), //AZazあア
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("i5")),
                            Helpers.MakeSeq(Expression.MakeConstant("char", '\u0041')), //A
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("i6")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "This is a normal string.\n Seems 2 lines? Yes, you're right!")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("i6_")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", @"This is a raw string.\n Seems 2 lines? Nah, indeed.")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("i7"),
                            Expression.MakeConstant("string", @"This is a raw string.
It's 2 lines."),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("i7_"),
                            Expression.MakeConstant("string", @"This is a ""truly raw"" string.
It's 2 lines and double quotes are allowed."),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("i7__"),
                            Expression.MakeConstant("string", "\""),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("j")),
                            Helpers.MakeSeq(
                                Expression.MakeIntSeq(
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 10),
                                    Expression.MakeConstant("int", 1),
                                    false
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("j2")),
                            Helpers.MakeSeq(
                                Expression.MakeIntSeq(
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 10),
                                    Expression.MakeConstant("int", 1),
                                    false
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "j3",
                                    Helpers.MakePrimitiveType("intseq")
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeIntSeq(
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 10),
                                    Expression.MakeConstant("int", 1),
                                    false
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("j_2")),
                            Helpers.MakeSeq(
                                Expression.MakeIntSeq(
                                    Expression.MakeUnaryExpr(OperatorType.Minus, Expression.MakeConstant("int", 5)),
                                    Expression.MakeUnaryExpr(OperatorType.Minus, Expression.MakeConstant("int", 10)),
                                    Expression.MakeUnaryExpr(OperatorType.Minus, Expression.MakeConstant("int", 1)),
                                    false
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("j_2_")),
                            Helpers.MakeSeq(
                                Expression.MakeIntSeq(
                                    Expression.MakeConstant("int", 0),
                                    Expression.MakeUnaryExpr(OperatorType.Minus, Expression.MakeConstant("int", 10)),
                                    Expression.MakeUnaryExpr(OperatorType.Minus, Expression.MakeConstant("int", 1)),
                                    true
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${h_a}, ${h_a_}, ${b}, ${f_b}, ${f_b_}, ${c}, ${f_c}, ${f_c2}, ${d}, ${d_}, ${d2}, ${e}, ${u}, ${u_}, ${f_a}, ${f_a_}, ${f}, ${f_}, ${f2}, ${f2_}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${f3_}, ${f3_a}, ${f3_b}, ${f3_c}, ${g}, ${g_}, ${h}, ${h2}, ${h3}, ${i}, ${i2}, ${i3}, ${i3_}, ${i4}, ${i5}, ${i6}, ${i6_}, ${i7}, ${i7_}, ${i7__}, ${j}, ${j2}, ${j3}, ${j_2}, ${j_2_}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            var debug_output = new DebugOutputWalker(Console.Out);
            ast.AcceptWalker(debug_output);
            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void SimpleArithmetic()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/simple_arithmetic.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("x"),
                                Helpers.MakeSomePatternWithType("xp"),
                                Helpers.MakeSomePatternWithType("xm"),
                                Helpers.MakeSomePatternWithType("xt"),
                                Helpers.MakeSomePatternWithType("xd"),
                                Helpers.MakeSomePatternWithType("xmod"),
                                Helpers.MakeSomePatternWithType("xpower")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 3),
                                Expression.MakeConstant("int", 3),
                                Expression.MakeConstant("int", 3),
                                Expression.MakeConstant("int", 3),
                                Expression.MakeConstant("int", 4),
                                Expression.MakeConstant("int", 4),
                                Expression.MakeConstant("int", 3)
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("a")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Plus,
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeConstant("int", 4)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("b")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Minus,
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeConstant("int", 4)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("c")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Times,
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeConstant("int", 4)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("d")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Divide,
                                    Expression.MakeConstant("int", 4),
                                    Expression.MakeConstant("int", 2)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("e")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Modulus,
                                    Expression.MakeConstant("int", 4),
                                    Expression.MakeConstant("int", 2)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Power,
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeConstant("int", 2)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Plus,
                                Helpers.MakeIdentifierPath("xp"),
                                Expression.MakeConstant("int", 4)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Minus,
                                Helpers.MakeIdentifierPath("xm"),
                                Expression.MakeConstant("int", 4)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Times,
                                Helpers.MakeIdentifierPath("xt"),
                                Expression.MakeConstant("int", 4)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Divide,
                                Helpers.MakeIdentifierPath("xd"),
                                Expression.MakeConstant("int", 2)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Modulus,
                                Helpers.MakeIdentifierPath("xmod"),
                                Expression.MakeConstant("int", 2)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Power,
                                Helpers.MakeIdentifierPath("xpower"),
                                Expression.MakeConstant("int", 2)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${x}, ${a}, ${b}, ${c}, ${d}, ${e}, ${f}, ${xp}, ${xm}, ${xt}, ${xd}, ${xmod}, ${xpower}")
                            )
                        )
                    ),
                    Helpers.MakeVoidType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void GeneralExpressions()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/general_expressions.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("ary")),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("array", AstType.MakePlaceholderType()),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 3)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("d")),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("dictionary", AstType.MakePlaceholderType(), AstType.MakePlaceholderType()),
                                    Helpers.MakeSeq(
                                        Expression.MakeKeyValuePair(Expression.MakeConstant("string", "a"), Expression.MakeConstant("int", 14)),
                                        Expression.MakeKeyValuePair(Expression.MakeConstant("string", "b"), Expression.MakeConstant("int", 13)),
                                        Expression.MakeKeyValuePair(Expression.MakeConstant("string", "何か"), Expression.MakeConstant("int", 100))
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("m")),
                            Helpers.MakeSeq(
                                Helpers.MakeIndexerExpression(
                                    Helpers.MakeIdentifierPath("ary"),
                                    Expression.MakeConstant("int", 0)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("m2")),
                            Helpers.MakeSeq(
                                Helpers.MakeIndexerExpression(
                                    Helpers.MakeIdentifierPath("d"),
                                    Expression.MakeConstant("string", "a")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("x")),
                            Helpers.MakeSeq(Expression.MakeConstant("int", 100)),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("p")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Plus,
                                    Helpers.MakeIndexerExpression(
                                        Helpers.MakeIdentifierPath("ary"),
                                        Expression.MakeConstant("int", 0)
                                    ),
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Plus,
                                        Helpers.MakeIndexerExpression(
                                            Helpers.MakeIdentifierPath("ary"),
                                            Expression.MakeConstant("int", 1)
                                        ),
                                        Helpers.MakeIndexerExpression(
                                            Helpers.MakeIdentifierPath("ary"),
                                            Expression.MakeConstant("int", 2)
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("q")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Plus,
                                    Helpers.MakeIndexerExpression(
                                        Helpers.MakeIdentifierPath("d"),
                                        Expression.MakeConstant("string", "a")
                                    ),
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Plus,
                                        Helpers.MakeIndexerExpression(
                                            Helpers.MakeIdentifierPath("d"),
                                            Expression.MakeConstant("string", "b")
                                        ),
                                        Helpers.MakeIndexerExpression(
                                            Helpers.MakeIdentifierPath("d"),
                                            Expression.MakeConstant("string", "何か")
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("r")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.BitwiseShiftRight,
                                    Helpers.MakeIdentifierPath("x"),
                                    Helpers.MakeIdentifierPath("p")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("s")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.BitwiseShiftLeft,
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeConstant("int", 2)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("t")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.BitwiseAnd,
                                    Helpers.MakeIdentifierPath("r"),
                                    Helpers.MakeIdentifierPath("s")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("v")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.BitwiseOr,
                                    Helpers.MakeIdentifierPath("x"),
                                    Helpers.MakeIdentifierPath("t")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("w")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Plus,
                                    Helpers.MakeIdentifierPath("r"),
                                    Helpers.MakeIdentifierPath("s")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("y")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Plus,
                                    Helpers.MakeIndexerExpression(
                                        Helpers.MakeIdentifierPath("ary"),
                                        Expression.MakeConstant("int", 0)
                                    ),
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Plus,
                                        Helpers.MakeIndexerExpression(
                                            Helpers.MakeIdentifierPath("ary"),
                                            Expression.MakeConstant("int", 1)
                                        ),
                                        Helpers.MakeIndexerExpression(
                                            Helpers.MakeIdentifierPath("d"),
                                            Expression.MakeConstant("string", "a")
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("z")),
                            Helpers.MakeSeq(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Times,
                                    Helpers.MakeIdentifierPath("v"),
                                    Helpers.MakeIdentifierPath("w")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("z2"),
                            Expression.MakeCondExpr(
                                Expression.MakeParen(
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Equality,
                                        Helpers.MakeIdentifierPath("x"),
                                        Expression.MakeConstant("int", 100)
                                    )
                                ),
                                Expression.MakeConstant("int", 0),
                                Expression.MakeConstant("int", 1)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${ary}, ${d}, ${m}, ${m2}, ${x}, ${p}, ${q}, ${r}, ${s}, ${t}, ${v}, ${w}, ${y}, ${z}, ${z2}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void BasicStatements()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/basic_statements.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("x"),
                                Helpers.MakeSomePatternWithType("y"),
                                Helpers.MakeSomePatternWithType("z"),
                                Helpers.MakeSomePatternWithType("w")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 100),
                                Expression.MakeConstant("int", 20),
                                Expression.MakeConstant("int", 300),
                                Expression.MakeConstant("int", 400)
                            ),
                            Modifiers.Immutable
                        ),
                        Helpers.MakeVariableDeclaration(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("flag", Helpers.MakePrimitiveType("bool"))),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSequenceExpression(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "${x}, ${y}, ${z}, ${w}")
                                )
                            )
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Equality,
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeConstant("int", 100)
                                )
                            ),
                            Statement.MakeBlock(
                                Helpers.MakeSeq(
                                    Statement.MakeExprStmt(
                                        Expression.MakeSingleAssignment(
                                            Helpers.MakeIdentifierPath("flag"),
                                            Expression.MakeConstant("bool", true)
                                        )
                                    )
                                )
                            ),
                            Statement.MakeIfStmt(
                                PatternConstruct.MakeExpressionPattern(
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Equality,
                                        Helpers.MakeIdentifierPath("x"),
                                        Expression.MakeConstant("int", 0)
                                    )
                                ),
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Expression.MakeSingleAssignment(
                                            Helpers.MakeIdentifierPath("flag"),
                                            Expression.MakeConstant("bool", false)
                                        )
                                    )
                                ),
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Expression.MakeSingleAssignment(
                                            Helpers.MakeIdentifierPath("flag"),
                                            Expression.MakeConstant("bool", false)
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("sum")),
                            Helpers.MakeSeq(Expression.MakeConstant("int", 0)),
                            Modifiers.None
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomePatternWithType("p"),
                            Expression.MakeIntSeq(
                                Expression.MakeConstant("int", 0),
                                Helpers.MakeIdentifierPath("y"),
                                Expression.MakeConstant("int", 1),
                                false
                            ),
                            Statement.MakeBlock(
                                Helpers.MakeSeq(
                                    Helpers.MakeAugmentedAssignment(
                                        OperatorType.Plus,
                                        Helpers.MakeSeq(Helpers.MakeIdentifierPath("sum")),
                                        Helpers.MakeSeq(Helpers.MakeIdentifierPath("p"))
                                    ),
                                    Statement.MakeExprStmt(
                                        Expression.MakeSequenceExpression(
                                            Helpers.MakeCallExpression(
                                                Helpers.MakeIdentifierPath("println"),
                                                Expression.MakeConstant("string", "${p}, ${sum}")
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "fibs",
                                    Helpers.MakeGenericType(
                                        "vector",
                                        AstType.MakePrimitiveType("int")
                                    )
                                ),
                                Helpers.MakeSomePatternWithType("a"),
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq<Expression>(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType()),
                                    Enumerable.Empty<Expression>()
                                ),
                                Expression.MakeConstant("int", 0),
                                Expression.MakeConstant("int", 1)
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeWhileStmt(
                            Expression.MakeBinaryExpr(
                                OperatorType.LessThan,
                                Helpers.MakeIdentifierPath("b"),
                                Expression.MakeConstant("int", 1000)
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Expression.MakeMemRef(
                                                Helpers.MakeIdentifierPath("fibs"),
                                                Helpers.MakeSomeIdent("Add")
                                            ),
                                            Helpers.MakeIdentifierPath("b")
                                        )
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeAssignment(
                                        Helpers.MakeSeq(
                                            Helpers.MakeIdentifierPath("a"),
                                            Helpers.MakeIdentifierPath("b")
                                        ),
                                        Helpers.MakeSeq<Expression>(
                                            Helpers.MakeIdentifierPath("b"),
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Plus,
                                                Helpers.MakeIdentifierPath("a"),
                                                Helpers.MakeIdentifierPath("b")
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("n")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 100)
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeDoWhileStmt(
                            Expression.MakeBinaryExpr(
                                OperatorType.GreaterThan,
                                Helpers.MakeIdentifierPath("n"),
                                Expression.MakeConstant("int", 0)
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Expression.MakeSingleAugmentedAssignment(
                                        OperatorType.Minus,
                                        Helpers.MakeIdentifierPath("n"),
                                        Expression.MakeConstant("int", 40)
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${n}")
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "vec",
                                    Helpers.MakeGenericType(
                                        "vector",
                                        AstType.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType()),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomePatternWithType("i"),
                            Helpers.MakeSequenceInitializer(
                                Helpers.MakeGenericType("vector", AstType.MakePlaceholderType()),
                                Expression.MakeConstant("int", 0),
                                Expression.MakeConstant("int", 1),
                                Expression.MakeConstant("int", 2),
                                Expression.MakeConstant("int", 3),
                                Expression.MakeConstant("int", 4),
                                Expression.MakeConstant("int", 5),
                                Expression.MakeConstant("int", 6),
                                Expression.MakeConstant("int", 7),
                                Expression.MakeConstant("int", 8),
                                Expression.MakeConstant("int", 9)
                            ),
                            Statement.MakeBlock(
                                Statement.MakeVarDecl(
                                    Helpers.MakeSeq(
                                        Helpers.MakeSomePatternWithType("MAX_J")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 8)
                                    ),
                                    Modifiers.Immutable
                                ),
                                Statement.MakeValueBindingForStmt(
                                    Modifiers.Immutable,
                                    Helpers.MakeSomePatternWithType("j"),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 0),
                                        Expression.MakeConstant("int", 10),
                                        Expression.MakeConstant("int", 1),
                                        false
                                    ),
                                    Statement.MakeBlock(
                                        Statement.MakeIfStmt(
                                            PatternConstruct.MakeExpressionPattern(
                                                Expression.MakeBinaryExpr(
                                                    OperatorType.Equality,
                                                    Helpers.MakeIdentifierPath("i"),
                                                    Expression.MakeConstant("int", 3)
                                                )
                                            ),
                                            Statement.MakeBlock(
                                                Statement.MakeBreakStmt(Expression.MakeConstant("int", 1))
                                            ),
                                            null
                                        ),
                                        Statement.MakeIfStmt(
                                            PatternConstruct.MakeExpressionPattern(
                                                Expression.MakeBinaryExpr(
                                                    OperatorType.Equality,
                                                    Helpers.MakeIdentifierPath("i"),
                                                    Expression.MakeConstant("int", 6)
                                                )
                                            ),
                                            Statement.MakeBlock(
                                                Statement.MakeBreakStmt(Expression.MakeConstant("int", 1))
                                            )
                                        ),
                                        Statement.MakeIfStmt(
                                            PatternConstruct.MakeExpressionPattern(
                                                Expression.MakeBinaryExpr(
                                                    OperatorType.Equality,
                                                    Helpers.MakeIdentifierPath("j"),
                                                    Helpers.MakeIdentifierPath("MAX_J")
                                                )
                                            ),
                                            Statement.MakeBlock(
                                                Statement.MakeContinueStmt(Expression.MakeConstant("int", 2))
                                            ),
                                            null
                                        ),
                                        Statement.MakeExprStmt(
                                            Helpers.MakeCallExpression(
                                                Expression.MakeMemRef(
                                                    Helpers.MakeIdentifierPath("vec"),
                                                    Helpers.MakeSomeIdent("Add")
                                                ),
                                                Helpers.MakeIdentifierPath("i")
                                            )
                                        ),
                                        Statement.MakeExprStmt(
                                            Helpers.MakeCallExpression(
                                                Expression.MakeMemRef(
                                                    Helpers.MakeIdentifierPath("vec"),
                                                    Helpers.MakeSomeIdent("Add")
                                                ),
                                                Helpers.MakeIdentifierPath("j")
                                            )
                                        ),
                                        Statement.MakeExprStmt(
                                            Helpers.MakeCallExpression(
                                                Helpers.MakeIdentifierPath("println"),
                                                Expression.MakeConstant("string", "${i}, ${j}")
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${flag}, ${sum}, ${a}, ${b}, ${fibs}, ${vec}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void MatchStatements()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/match_statements.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                EntityDeclaration.MakeClassDecl(
                    "TestClass6",
                    Enumerable.Empty<AstType>(),
                    Helpers.MakeSeq<EntityDeclaration>(
                        EntityDeclaration.MakeField(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("X", Helpers.MakePrimitiveType("int")),
                                Helpers.MakeSomePatternWithType("Y", Helpers.MakePrimitiveType("int")),
                                Helpers.MakeSomePatternWithType("_Z", Helpers.MakePrimitiveType("int"))
                            ),
                            Helpers.MakeSeq(
                                Expression.Null,
                                Expression.Null,
                                Expression.Null
                            ),
                            Modifiers.Public | Modifiers.Immutable
                        )
                    ),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("tmp")),
                            Helpers.MakeSeq(Expression.MakeConstant("string", "akarichan")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("tmp"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "kawakawa")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(Expression.MakeConstant("string", "akarichan"))
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "ankokuthunder!")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(Expression.MakeConstant("string", "chinatsu"))
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "gaichiban!")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(Expression.MakeConstant("string", "kyoko"))
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "doyaxtu!")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(Expression.MakeConstant("string", "yui"))
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "otherwise")
                                    )
                                ),
                                PatternConstruct.MakeWildcardPattern()
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("tmp2")),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 1)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("flag"),
                            Expression.MakeConstant("bool", true),
                            Modifiers.Immutable
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("tmp2"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "0")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(Expression.MakeConstant("int", 0))
                            ),
                            Statement.MakeMatchClause(
                                Helpers.MakeIdentifierPath("flag"),
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "1 or 2")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(Expression.MakeConstant("int", 1)),
                                PatternConstruct.MakeExpressionPattern(Expression.MakeConstant("int", 2))
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "${i} is in the range of 3 to 10")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeIdentifierPattern(
                                    "i",
                                    AstType.MakePlaceholderType(),
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 3),
                                            Expression.MakeConstant("int", 10),
                                            Expression.MakeConstant("int", 1),
                                            true
                                        )
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "otherwise")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeWildcardPattern()
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("tmp3")),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("TestClass6"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("X"),
                                        AstNode.MakeIdentifier("Y"),
                                        AstNode.MakeIdentifier("_Z")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 3)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("tmp3"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "X is ${X}")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    Helpers.MakeGenericType("TestClass6"),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "X",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIgnoringRestPattern()
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "X is ${X} and Y is ${Y}")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    Helpers.MakeGenericType("TestClass6"),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "X",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "Y",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeWildcardPattern()
                                )
                            )
                        ),
                        /*Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomeIdent("tmp4")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType()),
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 2),
                                    Expression.MakeConstant("int", 3),
                                    Expression.MakeConstant("int", 4)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("tmp4"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "x and y are both vector's elements and the values are ${x} and ${y} respectively")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeCollectionPattern(
                                    true,
                                    PatternConstruct.MakeIdentifierPattern(
                                        "x",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "y",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIgnoringRestPattern()
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("print"),
                                            Expression.MakeConstant("string", "tmp4 is a vector")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeCollectionPattern(
                                    true,
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeConstant("int", 1)
                                    ),
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeConstant("int", 2)
                                    ),
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeConstant("int", 3)
                                    ),
                                    PatternConstruct.MakeWildcardPattern()
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomeIdent("tmp5")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("array", AstType.MakePlaceholderType()),
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 2),
                                    Expression.MakeConstant("int", 3),
                                    Expression.MakeConstant("int", 4)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("tmp5"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "x and y are both array's elements and the values are ${x} and ${y} respectively")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeCollectionPattern(
                                    false,
                                    PatternConstruct.MakeIdentifierPattern(
                                        "x",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "y",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIgnoringRestPattern()
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("print"),
                                            Expression.MakeConstant("string", "tmp5 is an array")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeCollectionPattern(
                                    false,
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeConstant("int", 1)
                                    ),
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeConstant("int", 2)
                                    ),
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeConstant("int", 3)
                                    ),
                                    PatternConstruct.MakeWildcardPattern()
                                )
                            )
                        ),*/
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("tmp6")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeParen(
                                    Expression.MakeSequenceExpression(
                                        Expression.MakeConstant("int", 4),
                                        Expression.MakeConstant("int", 5)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("tmp6"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "x is ${x}")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeTuplePattern(
                                    PatternConstruct.MakeIdentifierPattern(
                                        "x",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIgnoringRestPattern()
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "x is ${x} and y is ${y}")
                                        )
                                    )
                                ),
                                PatternConstruct.MakeTuplePattern(
                                    PatternConstruct.MakeIdentifierPattern(
                                        "x",
                                        AstType.MakePlaceholderType(),
                                        null
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "y",
                                        AstType.MakePlaceholderType(),
                                        null
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void FunctionsAndRecursiveCall()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/functions_and_recursive_call.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "test",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("a")),
                            Helpers.MakeSeq(Expression.MakeConstant("int", 10)),
                            Modifiers.Immutable
                        ),
                        Helpers.MakeSingleItemReturnStatement(
                            Expression.MakeBinaryExpr(
                                OperatorType.Plus,
                                Helpers.MakeIdentifierPath("a"),
                                Expression.MakeConstant("int", 10)
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "test2",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter("n", Helpers.MakePrimitiveType("int"))
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Expression.MakeBinaryExpr(
                                OperatorType.Plus,
                                Helpers.MakeIdentifierPath("n"),
                                Expression.MakeConstant("int", 10)
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "test3",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter("n", Helpers.MakePrimitiveType("int"))
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Expression.MakeBinaryExpr(
                                OperatorType.Plus,
                                Helpers.MakeIdentifierPath("n"),
                                Expression.MakeConstant("int", 20)
                            )
                        )
                    ),
                    AstType.MakePrimitiveType("int"),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "test4",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter("n", Helpers.MakePrimitiveType("int"))
                    ),
                    Statement.MakeBlock(
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.GreaterThanOrEqual,
                                    Helpers.MakeIdentifierPath("n"),
                                    Expression.MakeConstant("int", 100)
                                )
                            ),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Helpers.MakeIdentifierPath("n")
                                )
                            ),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("test4"),
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Plus,
                                            Helpers.MakeIdentifierPath("n"),
                                            Expression.MakeConstant("int", 10)
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    Helpers.MakePrimitiveType("int"),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "test5",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            Helpers.MakeSomeIdent("n"),
                            Expression.MakeConstant("int", 100)
                        )
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Helpers.MakeIdentifierPath("n")
                        )
                    ),
                    Helpers.MakePrimitiveType("int"),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("a")),
                            Helpers.MakeSeq(
                                Expression.MakeCallExpr(
                                    Helpers.MakeIdentifierPath("test"),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("b")),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("test2"),
                                    Expression.MakeConstant("int", 20)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("c")),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("test3"),
                                    Expression.MakeConstant("int", 20)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("d")),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("test4"),
                                    Expression.MakeConstant("int", 80)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("e")),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("test5")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("f")),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("test5"),
                                    Expression.MakeConstant("int", 90)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}, ${c}, ${d}, ${e}, ${f}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);

            Helpers.AstStructuralEqual(ast, expected);
        }

        /*[Test]
        public void ComplexExpressions()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/complex_expressions.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("x")),
                            Helpers.MakeSeq(
                                Expression.MakeComp(
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeCompFor(
                                        Helpers.MakeSomePatternWithType("x"),
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 100),
                                            Expression.MakeConstant("int", 1),
                                            false
                                        ),
                                        null
                                    ),
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType())
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("y")),
                            Helpers.MakeSeq(
                                Expression.MakeComp(
                                    Helpers.MakeIdentifierPath("x"),
                                    Expression.MakeCompFor(
                                        Helpers.MakeSomePatternWithType("x"),
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 100),
                                            Expression.MakeConstant("int", 1),
                                            false
                                        ),
                                        Expression.MakeCompIf(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Equality,
                                                Expression.MakeBinaryExpr(
                                                    OperatorType.Modulus, 
                                                    Helpers.MakeIdentifierPath("x"),
                                                    Expression.MakeConstant("int", 2)
                                                ),
                                                Expression.MakeConstant("int", 0)
                                            ),
                                            null
                                        )
                                    ),
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType())
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("z")),
                            Helpers.MakeSeq(
                                Expression.MakeComp(
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(
                                            Helpers.MakeIdentifierPath("x"),
                                            Helpers.MakeIdentifierPath("y")
                                        )
                                    ),
                                    Expression.MakeCompFor(
                                        Helpers.MakeSomePatternWithType("x"),
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 100),
                                            Expression.MakeConstant("int", 1),
                                            false
                                        ),
                                        Expression.MakeCompIf(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Equality,
                                                Expression.MakeBinaryExpr(
                                                    OperatorType.Modulus,
                                                    Helpers.MakeIdentifierPath("x"),
                                                    Expression.MakeConstant("int", 2)
                                                ),
                                                Expression.MakeConstant("int", 0)
                                            ),
                                            Expression.MakeCompFor(
                                                Helpers.MakeSomePatternWithType("y"),
                                                Expression.MakeIntSeq(
                                                    Expression.MakeConstant("int", 0),
                                                    Expression.MakeConstant("int", 100),
                                                    Expression.MakeConstant("int", 1),
                                                    false
                                                ),
                                                null
                                            )
                                        )
                                    ),
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType())
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("triangles")),
                            Helpers.MakeSeq(
                                Expression.MakeComp(
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(
                                            Helpers.MakeIdentifierPath("a"),
                                            Helpers.MakeIdentifierPath("b"),
                                            Helpers.MakeIdentifierPath("c")
                                        )
                                    ),
                                    Expression.MakeCompFor(
                                        Helpers.MakeSomePatternWithType("c"),
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 1),
                                            Expression.MakeConstant("int", 10),
                                            Expression.MakeConstant("int", 1),
                                            true
                                        ),
                                        Expression.MakeCompFor(
                                            Helpers.MakeSomePatternWithType("b"),
                                            Expression.MakeIntSeq(
                                                Expression.MakeConstant("int", 1),
                                                Helpers.MakeIdentifierPath("c"),
                                                Expression.MakeConstant("int", 1),
                                                true
                                            ),
                                            Expression.MakeCompFor(
                                                Helpers.MakeSomePatternWithType("a"),
                                                Expression.MakeIntSeq(
                                                    Expression.MakeConstant("int", 1),
                                                    Helpers.MakeIdentifierPath("b"),
                                                    Expression.MakeConstant("int", 1),
                                                    true
                                                ),
                                                Expression.MakeCompIf(
                                                    Expression.MakeBinaryExpr(
                                                        OperatorType.Equality,
                                                        Expression.MakeBinaryExpr(
                                                            OperatorType.Plus,
                                                            Expression.MakeBinaryExpr(
                                                                OperatorType.Power,
                                                                Helpers.MakeIdentifierPath("a"),
                                                                Expression.MakeConstant("int", 2)
                                                            ),
                                                            Expression.MakeBinaryExpr(
                                                                OperatorType.Power,
                                                                Helpers.MakeIdentifierPath("b"),
                                                                Expression.MakeConstant("int", 2)
                                                            )
                                                        ),
                                                        Expression.MakeBinaryExpr(
                                                            OperatorType.Power,
                                                            Helpers.MakeIdentifierPath("c"),
                                                            Expression.MakeConstant("int", 2)
                                                        )
                                                    ),
                                                    null
                                                )
                                            )
                                        )
                                    ),
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType())
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("specific_triangles")),
                            Helpers.MakeSeq(
                                Expression.MakeComp(
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(
                                            Helpers.MakeIdentifierPath("a"),
                                            Helpers.MakeIdentifierPath("b"),
                                            Helpers.MakeIdentifierPath("c")
                                        )
                                    ),
                                    Expression.MakeCompFor(
                                        Helpers.MakeSomePatternWithType("c"),
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 1),
                                            Expression.MakeConstant("int", 10),
                                            Expression.MakeConstant("int", 1),
                                            true
                                        ),
                                        Expression.MakeCompFor(
                                            Helpers.MakeSomePatternWithType("b"),
                                            Expression.MakeIntSeq(
                                                Expression.MakeConstant("int", 1),
                                                Helpers.MakeIdentifierPath("c"),
                                                Expression.MakeConstant("int", 1),
                                                true
                                            ),
                                            Expression.MakeCompFor(
                                                Helpers.MakeSomePatternWithType("a"),
                                                Expression.MakeIntSeq(
                                                    Expression.MakeConstant("int", 1),
                                                    Helpers.MakeIdentifierPath("b"),
                                                    Expression.MakeConstant("int", 1),
                                                    true
                                                ),
                                                Expression.MakeCompIf(
                                                    Expression.MakeBinaryExpr(
                                                        OperatorType.ConditionalAnd,
                                                        Expression.MakeBinaryExpr(
                                                            OperatorType.Equality,
                                                            Expression.MakeBinaryExpr(
                                                                OperatorType.Plus,
                                                                Expression.MakeBinaryExpr(
                                                                    OperatorType.Power,
                                                                    Helpers.MakeIdentifierPath("a"),
                                                                    Expression.MakeConstant("int", 2)
                                                                ),
                                                                Expression.MakeBinaryExpr(
                                                                    OperatorType.Power,
                                                                    Helpers.MakeIdentifierPath("b"),
                                                                    Expression.MakeConstant("int", 2)
                                                                )
                                                            ),
                                                            Expression.MakeBinaryExpr(
                                                                OperatorType.Power,
                                                                Helpers.MakeIdentifierPath("c"),
                                                                Expression.MakeConstant("int", 2)
                                                            )
                                                        ),
                                                        Expression.MakeBinaryExpr(
                                                            OperatorType.Equality,
                                                            Expression.MakeBinaryExpr(
                                                                OperatorType.Plus,
                                                                Helpers.MakeIdentifierPath("a"),
                                                                Expression.MakeBinaryExpr(
                                                                    OperatorType.Plus,
                                                                    Helpers.MakeIdentifierPath("b"),
                                                                    Helpers.MakeIdentifierPath("c")
                                                                )
                                                            ),
                                                            Expression.MakeConstant("int", 24)
                                                        )
                                                    ),
                                                    null
                                                )
                                            )
                                        )
                                    ),
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType())
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Helpers.MakeVariableDeclaration(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a", AstType.MakePrimitiveType("int")),
                                Helpers.MakeSomePatternWithType("b", AstType.MakePrimitiveType("int")),
                                Helpers.MakeSomePatternWithType("c", AstType.MakePrimitiveType("int"))
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeMultipleAssignment(
                                Expression.MakeMultipleAssignment(
                                    Expression.MakeSingleAssignment(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeIdentifierPath("b")
                                    ),
                                    Expression.MakeSequenceExpression(Helpers.MakeIdentifierPath("c"))
                                ),
                                Expression.MakeSequenceExpression(Expression.MakeConstant("int", 0))
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeAssignment(
                                Expression.MakeSequenceExpression(
                                    Helpers.MakeIdentifierPath("a"),
                                    Helpers.MakeIdentifierPath("b"),
                                    Helpers.MakeIdentifierPath("c")
                                ),
                                Expression.MakeSequenceExpression(
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 2),
                                    Expression.MakeConstant("int", 3)
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "vec",
                                    Helpers.MakeGenericType(
                                        "vector", 
                                        Helpers.MakeGenericType(
                                            "tuple", 
                                            Helpers.MakePrimitiveType("int"),
                                            Helpers.MakePrimitiveType("int")
                                        )
                                    )
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType("vector", AstType.MakePlaceholderType()),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(Helpers.MakeSomePatternWithType("t")),
                            Helpers.MakeSeq(
                                Expression.MakeParen(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeIdentifierPath("b"),
                                        Helpers.MakeIdentifierPath("c")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("vec"),
                                    Helpers.MakeSomeIdent("add")
                                ),
                                Expression.MakeParen(
                                    Expression.MakeSequenceExpression(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeIdentifierPath("b")
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${x}, ${y}, ${z}, ${triangles}, ${specific_triangles}, ${a}, ${b}, ${c}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }*/

        [Test]
        public void TestModule()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/test_module.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("test_module", new List<EntityDeclaration>{
                EntityDeclaration.MakeClassDecl(
                    "TestClass3",
                    Enumerable.Empty<AstType>(),
                    Helpers.MakeSeq<EntityDeclaration>(
                        EntityDeclaration.MakeField(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("x", Helpers.MakePrimitiveType("int")),
                                Helpers.MakeSomePatternWithType("y", Helpers.MakePrimitiveType("int"))
                            ),
                            null,
                            Modifiers.Private | Modifiers.Immutable
                        ),
                        Helpers.MakeFunc(
                            "getX",
                            Enumerable.Empty<ParameterDeclaration>(),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        Helpers.MakeSomeIdent("x")
                                    )
                                )
                            ),
                            AstType.MakePlaceholderType(),
                            Modifiers.Public
                        ),
                        Helpers.MakeFunc(
                            "getY",
                            Enumerable.Empty<ParameterDeclaration>(),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        Helpers.MakeSomeIdent("y")
                                    )
                                )
                            ),
                            Helpers.MakePrimitiveType("int"),
                            Modifiers.Public
                        )
                    ),
                    Modifiers.Export
                ),
                EntityDeclaration.MakeField(
                    Helpers.MakeSeq(
                        Helpers.MakeSomePatternWithType("pair")
                    ),
                    Helpers.MakeSeq(
                        Expression.MakeParen(
                            Expression.MakeSequenceExpression(
                                Expression.MakeConstant("int", 100),
                                Expression.MakeConstant("int", 200)
                            )
                        )
                    ),
                    Modifiers.Export | Modifiers.Immutable
                ),
                Helpers.MakeFunc(
                    "createTest",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "x",
                            Helpers.MakePrimitiveType("int")
                        ),
                        EntityDeclaration.MakeParameter(
                            "y",
                            Helpers.MakePrimitiveType("int")
                        )
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("TestClass3"),
                                Helpers.MakeSeq(
                                    AstNode.MakeIdentifier("x"),
                                    AstNode.MakeIdentifier("y")
                                ),
                                Helpers.MakeSeq(
                                    Helpers.MakeIdentifierPath("x"),
                                    Helpers.MakeIdentifierPath("y")
                                )
                            )
                        )
                    ),
                    Helpers.MakeGenericType("TestClass3"),
                    Modifiers.Export
                ),
                Helpers.MakeFunc(
                    "mySin",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "x",
                            Helpers.MakePrimitiveType("double")
                        )
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("Math"),
                                    Helpers.MakeSomeIdent("Sin")
                                ),
                                Helpers.MakeIdentifierPath("x")
                            )
                        )
                    ),
                    Helpers.MakePrimitiveType("double"),
                    Modifiers.Export
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Math"),
                    AstNode.MakeIdentifier("Math")
                )
            ));

            Assert.IsNotNull(ast);

            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void Module()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/module.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("TestModule"),
                                        Helpers.MakeGenericType("TestClass3")
                                    ),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("x"),
                                        AstNode.MakeIdentifier("y")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 100),
                                        Expression.MakeConstant("int", 300)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakePath(
                                        Helpers.MakeSomeIdent("TestModule"),
                                        Helpers.MakeSomeIdent("createTest")
                                    ),
                                    Expression.MakeConstant("int", 50),
                                    Expression.MakeConstant("int", 100)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakePath(
                                    Helpers.MakeSomeIdent("TestModule"),
                                    Helpers.MakeSomeIdent("pair")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("d")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakePath(
                                        Helpers.MakeSomeIdent("TestModule"),
                                        Helpers.MakeSomeIdent("mySin")
                                    ),
                                    Expression.MakeConstant("double", 0.0)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("e")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeSomeIdent("getX")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}, ${c}, ${d}, ${e}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("test_module"),
                    AstNode.MakeIdentifier("TestModule"),
                    AstNode.MakeIdentifier("./test_module.exs")
                )
            ));

            Assert.IsNotNull(ast);

            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void Class()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/class.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                EntityDeclaration.MakeClassDecl(
                    "TestClass",
                    Enumerable.Empty<AstType>(),
                    Helpers.MakeSeq<EntityDeclaration>(
                        EntityDeclaration.MakeField(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("X", Helpers.MakePrimitiveType("int"))
                            ),
                            Helpers.MakeSeq(
                                Expression.Null
                            ),
                            Modifiers.Public | Modifiers.Immutable
                        ),
                        EntityDeclaration.MakeField(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("y", Helpers.MakePrimitiveType("int")),
                                Helpers.MakeSomePatternWithType("z", Helpers.MakePrimitiveType("int"))
                            ),
                            Helpers.MakeSeq(
                                Expression.Null,
                                Expression.Null
                            ),
                            Modifiers.Private | Modifiers.Immutable
                        ),
                        Helpers.MakeFunc(
                            "getX",
                            Enumerable.Empty<ParameterDeclaration>(),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        Helpers.MakeSomeIdent("X")
                                    )
                                )
                            ),
                            AstType.MakePlaceholderType(),
                            Modifiers.Public
                        ),
                        Helpers.MakeFunc(
                            "getY",
                            Enumerable.Empty<ParameterDeclaration>(),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        Helpers.MakeSomeIdent("y")
                                    )
                                )
                            ),
                            Helpers.MakePrimitiveType("int"),
                            Modifiers.Public
                        ),
                        Helpers.MakeFunc(
                            "getXPlus",
                            Helpers.MakeSeq(
                                EntityDeclaration.MakeParameter("n", Helpers.MakePrimitiveType("int"))
                            ),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Plus,
                                        Helpers.MakeCallExpression(
                                            Expression.MakeMemRef(
                                                Expression.MakeSelfRef(),
                                                Helpers.MakeSomeIdent("getX")
                                            )
                                        ),
                                        Helpers.MakeIdentifierPath("n")
                                    )
                                )
                            ), 
                            Helpers.MakePrimitiveType("int"),
                            Modifiers.Public
                        ),
                        Helpers.MakeFunc(
                            "getZ",
                            Enumerable.Empty<ParameterDeclaration>(),
                            Statement.MakeBlock(
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        Helpers.MakeSomeIdent("z")
                                    )
                                )
                            ),
                            Helpers.MakePrimitiveType("int"),
                            Modifiers.Public
                        )
                    ),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("TestClass"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("X"),
                                        AstNode.MakeIdentifier("y"),
                                        AstNode.MakeIdentifier("z")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 3)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        /*Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomeIdent("b")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeNewExpr(
                                    Expression.MakeObjectCreation(
                                        Helpers.MakeGenericType("Test"),
                                        Helpers.MakeSeq(
                                            AstNode.MakeIdentifier("x"),
                                            AstNode.MakeIdentifier("y")
                                        ),
                                        Helpers.MakeSeq(
                                            Expression.MakeConstant("int", 1),
                                            Expression.MakeConstant("int", 3)
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),*/
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeSomeIdent("getX")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("d")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeSomeIdent("getY")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("e")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeSomeIdent("getXPlus")
                                    ),
                                    Expression.MakeConstant("int", 100)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("f")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("a"),
                                        Helpers.MakeSomeIdent("getZ")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("g")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("a"),
                                    Helpers.MakeSomeIdent("X")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "(a.X, a.y, a.z, X, X + 100) = (${c}, ${d}, ${f}, ${g}, ${e})")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void Closures()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/closures.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "addOneToOne",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "addOne",
                            Helpers.MakeFunctionType(
                                "closure",
                                Helpers.MakePrimitiveType("int"),
                                Helpers.MakePrimitiveType("int")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("addOne"),
                                Expression.MakeConstant("int", 1)
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq<Expression>(
                                Expression.MakeClosureExpression(
                                    AstType.MakePrimitiveType("int"),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Plus,
                                                Helpers.MakeIdentifierPath("x"),
                                                Expression.MakeConstant("int", 1)
                                            )
                                        )
                                    ),
                                    liftedIdentifiers: null,
                                    parameters: EntityDeclaration.MakeParameter(
                                        "x",
                                        Helpers.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c2")
                            ),
                            Helpers.MakeSeq<Expression>(
                                Expression.MakeClosureExpression(
                                    AstType.MakePrimitiveType("int"),
                                    Statement.MakeBlock(
                                        Statement.MakeVarDecl(
                                            Helpers.MakeSeq(
                                                Helpers.MakeSomePatternWithType("y")
                                            ),
                                            Helpers.MakeSeq(
                                                Expression.MakeConstant("int", 1)
                                            ),
                                            Modifiers.Immutable
                                        ),
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Plus,
                                                Helpers.MakeIdentifierPath("x"),
                                                Helpers.MakeIdentifierPath("y")
                                            )
                                        )
                                    ),
                                    liftedIdentifiers: null,
                                    parameters: EntityDeclaration.MakeParameter(
                                        "x",
                                        Helpers.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("c"),
                                    Expression.MakeConstant("int", 1)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("c2"),
                                    Expression.MakeConstant("int", 1)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("d")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("addOneToOne"),
                                    Expression.MakeClosureExpression(
                                        AstType.MakePlaceholderType(),
                                        Statement.MakeBlock(
                                            Helpers.MakeSingleItemReturnStatement(
                                                Expression.MakeBinaryExpr(
                                                    OperatorType.Plus,
                                                    Helpers.MakeIdentifierPath("x"),
                                                    Expression.MakeConstant("int", 1)
                                                )
                                            )
                                        ),
                                        liftedIdentifiers: null,
                                        parameters: EntityDeclaration.MakeParameter(
                                            "x",
                                            AstType.MakePlaceholderType()
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("e")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 2)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c3")
                            ),
                            Helpers.MakeSeq<Expression>(
                                Expression.MakeClosureExpression(
                                    AstType.MakePrimitiveType("int"),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Plus,
                                                Helpers.MakeIdentifierPath("x"),
                                                Helpers.MakeIdentifierPath("e")
                                            )
                                        )
                                    ),
                                    liftedIdentifiers: null,
                                    parameters: EntityDeclaration.MakeParameter(
                                        "x",
                                        Helpers.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("f")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("c3"),
                                    Expression.MakeConstant("int", 1)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}, ${d}, ${f}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ClosuresWithCompoundStatements()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/closures_with_compound_statements.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq<Expression>(
                                Expression.MakeClosureExpression(
                                    AstType.MakePrimitiveType("int"),
                                    Statement.MakeBlock(
                                        Statement.MakeIfStmt(
                                            PatternConstruct.MakeExpressionPattern(Helpers.MakeIdentifierPath("f")),
                                            Statement.MakeBlock(
                                                Helpers.MakeSingleItemReturnStatement(
                                                    Expression.MakeConstant("int", 1)
                                                )
                                            ),
                                            Statement.MakeBlock(
                                                Helpers.MakeSingleItemReturnStatement(
                                                    Expression.MakeConstant("int", 0)
                                                )
                                            )
                                        )
                                    ),
                                    liftedIdentifiers: null,
                                    parameters: EntityDeclaration.MakeParameter(
                                        "f",
                                        Helpers.MakePrimitiveType("bool")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("c"),
                                    Expression.MakeConstant("bool", true)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c2")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeClosureExpression(
                                    AstType.MakePrimitiveType("int"),
                                    Statement.MakeBlock(
                                        Statement.MakeVarDecl(
                                            Helpers.MakeSeq(
                                                Helpers.MakeSomePatternWithType("result")
                                            ),
                                            Helpers.MakeSeq(
                                                Expression.MakeConstant("int", 0)
                                            ),
                                            Modifiers.None
                                        ),
                                        Statement.MakeValueBindingForStmt(
                                            Modifiers.Immutable,
                                            Helpers.MakeSomePatternWithType("j"),
                                            Expression.MakeIntSeq(
                                                Expression.MakeConstant("int", 0),
                                                Helpers.MakeIdentifierPath("i"),
                                                Expression.MakeConstant("int", 1),
                                                false
                                            ),
                                            Statement.MakeBlock(
                                                Statement.MakeExprStmt(
                                                    Expression.MakeAugumentedAssignment(
                                                        OperatorType.Plus,
                                                        Expression.MakeSequenceExpression(Helpers.MakeIdentifierPath("result")),
                                                        Expression.MakeSequenceExpression(Helpers.MakeIdentifierPath("j"))
                                                    )
                                                )
                                            )
                                        ),
                                        Helpers.MakeSingleItemReturnStatement(
                                            Helpers.MakeIdentifierPath("result")
                                        )
                                    ),
                                    liftedIdentifiers: null,
                                    parameters: EntityDeclaration.MakeParameter(
                                        "i",
                                        Helpers.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("c2"),
                                    Expression.MakeConstant("int", 10)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c3")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeClosureExpression(
                                    Helpers.MakeVoidType(),
                                    Statement.MakeBlock(
                                        Statement.MakeVarDecl(
                                            Helpers.MakeSeq(
                                                Helpers.MakeSomePatternWithType("j")
                                            ),
                                            Helpers.MakeSeq(
                                                Helpers.MakeIdentifierPath("i")
                                            ),
                                            Modifiers.None
                                        ),
                                        Statement.MakeWhileStmt(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.GreaterThan,
                                                Helpers.MakeIdentifierPath("j"),
                                                Expression.MakeConstant("int", 0)
                                            ),
                                            Statement.MakeBlock(
                                                Statement.MakeExprStmt(
                                                    Helpers.MakeCallExpression(
                                                        Helpers.MakeIdentifierPath("println"),
                                                        Expression.MakeConstant("string", "${j}")
                                                    )
                                                ),
                                                Statement.MakeExprStmt(
                                                    Expression.MakeSingleAugmentedAssignment(
                                                        OperatorType.Minus,
                                                        Helpers.MakeIdentifierPath("j"),
                                                        Expression.MakeConstant("int", 1)
                                                    )
                                                )
                                            )
                                        ),
                                        Statement.MakeExprStmt(
                                            Helpers.MakeCallExpression(
                                                Helpers.MakeIdentifierPath("println"),
                                                Expression.MakeConstant("string", "BOOM!")
                                            )
                                        )
                                    ),
                                    liftedIdentifiers: null,
                                    parameters: EntityDeclaration.MakeParameter(
                                        "i",
                                        Helpers.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("c3"),
                                Expression.MakeConstant("int", 3)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TryStatements()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/try_statements.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "throwException",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeThrowStmt(
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType(
                                    "Exception"
                                ),
                                Helpers.MakeSeq(
                                    AstNode.MakeIdentifier("message")
                                ),
                                Helpers.MakeSeq(
                                    Expression.MakeConstant("string", "An unknown error has occurred")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "testTryFinally",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("tmp"),
                            Expression.MakeConstant("int", 1),
                            Modifiers.None
                        ),
                        Statement.MakeTryStmt(
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "tmp is ${tmp} at first")
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("throwException")
                                    )
                                )
                            ),
                            null,
                            Statement.MakeFinallyClause(
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "First finally block")
                                        )
                                    ),
                                    Statement.MakeExprStmt(
                                        Expression.MakeSingleAssignment(
                                            Helpers.MakeIdentifierPath("tmp"), 
                                            Expression.MakeConstant("int", 2)
                                        )
                                    ),
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "tmp is ${tmp} at last")
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Helpers.MakeTryStmt(
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "First try block")
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("throwException")
                                    )
                                )
                            ),
                            null,
                            Statement.MakeCatchClause(
                                AstNode.MakeIdentifier(
                                    "e",
                                    Helpers.MakeGenericType("Exception")
                                ),
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "First catch block")
                                        )
                                    ),
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "${e.Message}")
                                        )
                                    )
                                )
                            )
                        ),
                        Helpers.MakeTryStmt(
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("testTryFinally")
                                    )
                                )
                            ),
                            null,
                            Statement.MakeCatchClause(
                                AstNode.MakeIdentifier(
                                    "e",
                                    Helpers.MakeGenericType("Exception")
                                ),
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "${e.Message}")
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("tmp2")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 1)
                            ),
                            Modifiers.None
                        ),
                        Helpers.MakeTryStmt(
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "tmp2 is ${tmp2} at first")
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("throwException")
                                    )
                                )
                            ),
                            Statement.MakeFinallyClause(
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "Second finally block")
                                        )
                                    ),
                                    Statement.MakeExprStmt(
                                        Expression.MakeSingleAssignment(
                                            Helpers.MakeIdentifierPath("tmp2"), 
                                            Expression.MakeConstant("int", 3)
                                        )
                                    )
                                )
                            ),
                            Statement.MakeCatchClause(
                                AstNode.MakeIdentifier(
                                    "e",
                                    Helpers.MakeGenericType("Exception")
                                ),
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "Second catch block")
                                        )
                                    ),
                                    Statement.MakeExprStmt(
                                        Expression.MakeSingleAssignment(
                                            Helpers.MakeIdentifierPath("tmp2"),
                                            Expression.MakeConstant("int", 2)
                                        )
                                    ),
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "${e.Message}")
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "tmp2 is ${tmp2} at last")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Exception"),
                    AstNode.MakeIdentifier("Exception")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void BuiltinObjects()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/builtin_objects.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        1
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        2
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        3
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        4
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        5
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        6
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        7
                                    ),
                                    Expression.MakeConstant(
                                        "int",
                                        8
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "dictionary",
                                        AstType.MakePlaceholderType(),
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeKeyValuePair(
                                        Expression.MakeConstant(
                                            "string",
                                            "akari"
                                        ),
                                        Expression.MakeConstant(
                                            "int",
                                            13
                                        )
                                    ),
                                    Expression.MakeKeyValuePair(
                                        Expression.MakeConstant(
                                            "string",
                                            "chinatsu"
                                        ),
                                        Expression.MakeConstant(
                                            "int",
                                            13
                                        )
                                    ),
                                    Expression.MakeKeyValuePair(
                                        Expression.MakeConstant(
                                            "string",
                                            "kyoko"
                                        ),
                                        Expression.MakeConstant(
                                            "int",
                                            14
                                        )
                                    ),
                                    Expression.MakeKeyValuePair(
                                        Expression.MakeConstant(
                                            "string",
                                            "yui"
                                        ),
                                        Expression.MakeConstant(
                                            "int",
                                            14
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeParen(
                                    Expression.MakeSequenceExpression(
                                        Expression.MakeConstant(
                                            "string",
                                            "akarichan"
                                        ),
                                        Expression.MakeConstant(
                                            "string",
                                            "kawakawa"
                                        ),
                                        Expression.MakeConstant(
                                            "string",
                                            "chinatsuchan"
                                        ),
                                        Expression.MakeConstant(
                                            "int",
                                            2424
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("d")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a"),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant(
                                            "int",
                                            0
                                        ),
                                        Expression.MakeConstant(
                                            "int",
                                            3
                                        ),
                                        Expression.MakeConstant(
                                            "int",
                                            1
                                        ),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("e")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 0),
                                        Expression.MakeConstant("int", 10),
                                        Expression.MakeConstant("int", 1),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("f")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "vector",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 0),
                                        Expression.MakeConstant("int", 10),
                                        Expression.MakeConstant("int", 1),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("g")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 0),
                                        Expression.MakeUnaryExpr(
                                            OperatorType.Minus,
                                            Expression.MakeConstant("int", 10)
                                        ),
                                        Expression.MakeUnaryExpr(
                                            OperatorType.Minus,
                                            Expression.MakeConstant("int", 1)
                                        ),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("h")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 0),
                                        Expression.MakeConstant("int", 100),
                                        Expression.MakeConstant("int", 2),
                                        true
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("i")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 5),
                                        Expression.MakeConstant("int", 15),
                                        Expression.MakeConstant("int", 2),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("j"),
                            Expression.MakeArrayInitializer(
                                AstType.MakePrimitiveType("int"),
                                Expression.MakeConstant("int", 5)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "y",
                                    Helpers.MakeGenericType(
                                        "vector",
                                        Helpers.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "vector",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomePatternWithType("x"),
                            Helpers.MakeIdentifierPath("d"),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("print"),
                                        Expression.MakeConstant("string", "${x}")
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Expression.MakeMemRef(
                                            Helpers.MakeIdentifierPath("y"),
                                            Helpers.MakeSomeIdent("Add")
                                        ),
                                        Helpers.MakeIdentifierPath("x")
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}, ${c}, ${d}, ${e}, ${f}, ${g}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${h}, ${i}, ${j}, ${y}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Interface()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/interface.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeInterfaceDecl(
                    "IInterface",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "doSomeBehavior",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        Helpers.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "TestClass2", 
                    Helpers.MakeSeq<AstType>(
                        Helpers.MakeGenericType("IInterface")
                    ),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSeq(
                            Helpers.MakeSomePatternWithType(
                                "x",
                                Helpers.MakePrimitiveType("int")
                            )
                        ),
                        Helpers.MakeSeq(
                            Expression.Null
                        ),
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "doSomeBehavior",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    Helpers.MakeSomeIdent("x")
                                )
                            )
                        ),
                        Helpers.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("t")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("TestClass2"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("x")
                                    ),
                                    Helpers.MakeSeq<Expression>(
                                        Expression.MakeConstant("int", 1)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("t"),
                                        Helpers.MakeSomeIdent("doSomeBehavior")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "t.x = ${a}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Slices()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/slices.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 10),
                                            Expression.MakeConstant("int", 1),
                                            false
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a"),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 4),
                                        Expression.MakeConstant("int", 1),
                                        true
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "vector",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 10),
                                            Expression.MakeConstant("int", 1),
                                            false
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("d")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("c"),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 4),
                                        Expression.MakeConstant("int", 1),
                                        true
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("negative_seq")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeIntSeq(
                                            Expression.MakeUnaryExpr(
                                                OperatorType.Minus,
                                                Expression.MakeConstant("int", 10)
                                            ),
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 1),
                                            false
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("to_negative")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeIntSeq(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeUnaryExpr(
                                                OperatorType.Minus,
                                                Expression.MakeConstant("int", 10)
                                            ),
                                            Expression.MakeUnaryExpr(
                                                OperatorType.Minus,
                                                Expression.MakeConstant("int", 1)
                                            ),
                                            false
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "Legend: (f(x) = x - 10, g(x) = -x)")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${negative_seq}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${to_negative}")
                            )
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomeTuplePatternWithType("x", "to_n"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("Enumerable"),
                                    Helpers.MakeSomeIdent("Zip")
                                ),
                                Expression.MakeCastExpr(
                                    Helpers.MakeIdentifierPath("negative_seq"),
                                    AstType.MakeSimpleType(
                                        "IEnumerable",
                                        TextLocation.Empty,
                                        TextLocation.Empty,
                                        AstType.MakePrimitiveType("int")
                                    )
                                ),
                                Expression.MakeCastExpr(
                                    Helpers.MakeIdentifierPath("to_negative"),
                                    AstType.MakeSimpleType(
                                        "IEnumerable",
                                        TextLocation.Empty,
                                        TextLocation.Empty,
                                        AstType.MakePrimitiveType("int")
                                    )
                                ),
                                Expression.MakeClosureExpression(
                                    AstType.MakePlaceholderType(),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeParen(
                                                Expression.MakeSequenceExpression(
                                                    Helpers.MakeIdentifierPath("l"),
                                                    Helpers.MakeIdentifierPath("r")
                                                )
                                            )
                                        )
                                    ),
                                    TextLocation.Empty,
                                    null,
                                    EntityDeclaration.MakeParameter(
                                        "l",
                                        AstType.MakePlaceholderType()
                                    ),
                                    EntityDeclaration.MakeParameter(
                                        "r",
                                        AstType.MakePlaceholderType()
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("print"),
                                        Expression.MakeConstant("string", "(f(x), g(x)) = (${x}, ${to_n}),")
                                    )
                                ),
                                Statement.MakeIfStmt(
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Equality,
                                            Helpers.MakeIdentifierPath("x"),
                                            Helpers.MakeIdentifierPath("to_n")
                                        )
                                    ),
                                    Statement.MakeBlock(
                                        Statement.MakeExprStmt(
                                            Helpers.MakeCallExpression(
                                                Helpers.MakeIdentifierPath("println"),
                                                Expression.MakeConstant("string", "Crossed over")
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("first_half")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a"),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 0),
                                        Expression.MakeConstant("int", 5),
                                        Expression.MakeConstant("int", 1),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("second_half")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a"),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 5),
                                        Expression.MakeConstant("int", 10),
                                        Expression.MakeConstant("int", 1),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomeTuplePatternWithType("a", "b"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("Enumerable"),
                                    Helpers.MakeSomeIdent("Zip")
                                ),
                                Expression.MakeCastExpr(
                                    Helpers.MakeIdentifierPath("first_half"),
                                    AstType.MakeSimpleType(
                                        "IEnumerable",
                                        TextLocation.Empty,
                                        TextLocation.Empty,
                                        AstType.MakePrimitiveType("int")
                                    )
                                ),
                                Expression.MakeCastExpr(
                                    Helpers.MakeIdentifierPath("second_half"),
                                    AstType.MakeSimpleType(
                                        "IEnumerable",
                                        TextLocation.Empty,
                                        TextLocation.Empty,
                                        AstType.MakePrimitiveType("int")
                                    )
                                ),
                                Expression.MakeClosureExpression(
                                    AstType.MakePlaceholderType(),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeParen(
                                                Expression.MakeSequenceExpression(
                                                    Helpers.MakeIdentifierPath("l"),
                                                    Helpers.MakeIdentifierPath("r")
                                                )
                                            )
                                        )
                                    ),
                                    TextLocation.Empty,
                                    null,
                                    EntityDeclaration.MakeParameter(
                                        "l",
                                        AstType.MakePlaceholderType()
                                    ),
                                    EntityDeclaration.MakeParameter(
                                        "r",
                                        AstType.MakePlaceholderType()
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("print"),
                                        Expression.MakeConstant("string", "${a}, ${b}, ")
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${b}, ${d}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Linq.Enumerable"),
                    AstNode.MakeIdentifier("Enumerable")
                ),
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType(
                        "System.Collections.Generic.IEnumerable",
                        AstType.MakePlaceholderType()
                    ),
                    AstNode.MakeIdentifier("IEnumerable")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TypeCast()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/type_cast.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeCastExpr(
                                    Helpers.MakeIdentifierPath("a"),
                                    AstType.MakePrimitiveType("byte")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void UseOfTheStandardLibrary()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/use_of_the_standard_library.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType(
                                    "writer",
                                    Helpers.MakeGenericType("FileStream")
                                )
                            ),
                            Helpers.MakeSeq(
                                Expression.Null
                            ),
                            Modifiers.None
                        ),
                        Helpers.MakeTryStmt(
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Expression.MakeSingleAssignment(
                                        Helpers.MakeIdentifierPath("writer"),
                                        Helpers.MakeCallExpression(
                                            Expression.MakeMemRef(
                                                Helpers.MakeIdentifierPath("File"),
                                                Helpers.MakeSomeIdent("OpenWrite")
                                            ),
                                            Expression.MakeConstant("string", "./some_text.txt")
                                        )
                                    )
                                ),
                                Statement.MakeVarDecl(
                                    Helpers.MakeSeq(
                                        Helpers.MakeSomePatternWithType("bytes") 
                                    ),
                                    Helpers.MakeSeq(
                                        Helpers.MakeCallExpression(
                                            Expression.MakeMemRef(
                                                Expression.MakeObjectCreation(
                                                    Helpers.MakeGenericType("UTF8Encoding"),
                                                    Helpers.MakeSeq(
                                                        AstNode.MakeIdentifier("encoderShouldEmitUTF8Identifier")
                                                    ),
                                                    Helpers.MakeSeq(
                                                        Expression.MakeConstant("bool", true)
                                                    )
                                                ),
                                                Helpers.MakeSomeIdent("GetBytes")
                                            ),
                                            Expression.MakeConstant("string", "This is to test writing a file")
                                        )
                                    ),
                                    Modifiers.Immutable
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Expression.MakeMemRef(
                                            Helpers.MakeIdentifierPath("writer"),
                                            Helpers.MakeSomeIdent("Write")
                                        ),
                                        Helpers.MakeIdentifierPath("bytes"),
                                        Expression.MakeConstant("int", 0),
                                        Expression.MakeMemRef(
                                            Helpers.MakeIdentifierPath("bytes"),
                                            Helpers.MakeSomeIdent("Length")
                                        )
                                    )
                                )
                            ),
                            Statement.MakeFinallyClause(
                                Statement.MakeBlock(
                                    Statement.MakeIfStmt(
                                        PatternConstruct.MakeExpressionPattern(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.InEquality,
                                                Helpers.MakeIdentifierPath("writer"),
                                                Expression.MakeNullRef()
                                            )
                                        ),
                                        Statement.MakeBlock(
                                            Statement.MakeExprStmt(
                                                Helpers.MakeCallExpression(
                                                    Expression.MakeMemRef(
                                                        Helpers.MakeIdentifierPath("writer"),
                                                        Helpers.MakeSomeIdent("Dispose")
                                                    )
                                                )
                                            )
                                        ),
                                        BlockStatement.Null
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("System.IO.File"),
                        Helpers.MakeGenericType("System.IO.FileStream")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("File"),
                        AstNode.MakeIdentifier("FileStream")
                    )
                ),
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Text.UTF8Encoding"),
                    AstNode.MakeIdentifier("UTF8Encoding")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void VariousStrings()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/various_strings.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeClassDecl(
                    "TestClass5",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSeq(
                            Helpers.MakeSomePatternWithType(
                                "x",
                                Helpers.MakePrimitiveType("int")
                            )
                        ),
                        Helpers.MakeSeq(
                            Expression.Null
                        ),
                        Modifiers.Immutable | Modifiers.Private
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeSeq(
                            Helpers.MakeSomePatternWithType(
                                "Y",
                                Helpers.MakePrimitiveType("int")
                            )
                        ),
                        Helpers.MakeSeq(
                            Expression.Null
                        ),
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    Helpers.MakeSomeIdent("x")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("x")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 5)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("t")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("TestClass5"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("x"),
                                        AstNode.MakeIdentifier("Y")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("ary")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 1),
                                    Expression.MakeConstant("int", 2),
                                    Expression.MakeConstant("int", 3),
                                    Expression.MakeConstant("int", 5),
                                    Expression.MakeConstant("int", 8)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("string", "some string")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("string", "some string containing templates: ${x + 1}")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("string", "another string containing templates: ${t.getX()}, ${t.Y}")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("d")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("string", "the 6th fibonacci number is ${ary[5]}")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("e")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("string", "a string containing dollar symbol: $$x = ${x}")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("f")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant(
                                    "string",
                                    @"This is a string.
This line is also a part of the string."
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}, ${c}, ${d}, ${e}, ${f}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Module2()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/module2.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected_ast = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("t")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("TestClass4"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("x"),
                                        AstNode.MakeIdentifier("y")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("t2")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("createTest"),
                                    Expression.MakeConstant("int", 3),
                                    Expression.MakeConstant("int", 4)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("x")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeIdentifierPath("pair")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${t}, ${t2}, ${x}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("test_module2::TestClass4"),
                        Helpers.MakeGenericType("test_module2::createTest"),
                        Helpers.MakeGenericType("test_module2::pair")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("TestClass4"),
                        AstNode.MakeIdentifier("createTest"),
                        AstNode.MakeIdentifier("pair")
                    ),
                    "./test_module2.exs"
                )
            ));

            Assert.IsNotNull(ast);

            Helpers.AstStructuralEqual(ast, expected_ast);
        }

        [Test]
        public void InteroperabilityTestWithCSharp()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/interoperability_test_with_csharp.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("t")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("InteroperabilityTest"),
                                    Enumerable.Empty<Identifier>(),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("t"),
                                    Helpers.MakeSomeIdent("DoSomething")
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("i")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("t"),
                                        Helpers.MakeSomeIdent("GetSomeInt")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("list")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("t"),
                                        Helpers.MakeSomeIdent("GetIntList")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("StaticTest"),
                                    Helpers.MakeSomeIdent("DoSomething")
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("flag")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("StaticTest"),
                                        Helpers.MakeSomeIdent("GetSomeBool")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("seq")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("StaticTest"),
                                        Helpers.MakeSomeIdent("GetSomeIntSeq")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${i}, ${list}, ${flag}, ${seq}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        AstType.MakeSimpleType("InteroperabilityTest.InteroperabilityTest"),
                        AstType.MakeSimpleType("InteroperabilityTest.StaticTest")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("InteroperabilityTest"),
                        AstNode.MakeIdentifier("StaticTest")
                    ),
                    "./InteroperabilityTest.dll"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void AdvancedForLoops()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/advanced_for_loops.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("dict")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "dictionary",
                                        AstType.MakePlaceholderType(),
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeKeyValuePair(Expression.MakeConstant("string", "akari"), Expression.MakeConstant("int", 13)),
                                    Expression.MakeKeyValuePair(Expression.MakeConstant("string", "chinatsu"), Expression.MakeConstant("int", 13)),
                                    Expression.MakeKeyValuePair(Expression.MakeConstant("string", "kyoko"), Expression.MakeConstant("int", 14)),
                                    Expression.MakeKeyValuePair(Expression.MakeConstant("string", "yui"), Expression.MakeConstant("int", 14))
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "array",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(Expression.MakeConstant("int", 1), Expression.MakeConstant("int", 2))
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(Expression.MakeConstant("int", 3), Expression.MakeConstant("int", 4))
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(Expression.MakeConstant("int", 5), Expression.MakeConstant("int", 6))
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("v")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeSequenceInitializer(
                                    Helpers.MakeGenericType(
                                        "vector",
                                        AstType.MakePlaceholderType()
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(Expression.MakeConstant("int", 7), Expression.MakeConstant("int", 8))
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(Expression.MakeConstant("int", 9), Expression.MakeConstant("int", 10))
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeSequenceExpression(Expression.MakeConstant("int", 11), Expression.MakeConstant("int", 12))
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomeTuplePatternWithType("key", "value"),
                            Helpers.MakeIdentifierPath("dict"),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${key}: ${value}, ")
                                    )
                                )
                            )
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomeTuplePatternWithType("first", "second"),
                            Helpers.MakeIdentifierPath("a"),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "(${first}, ${second}), ")
                                    )
                                )
                            )
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomeTuplePatternWithType("first2", "second2"),
                            Helpers.MakeIdentifierPath("v"),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "(${first2}, ${second2}), ")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            });

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void PropertyTests()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/property_tests.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "before: ${PropertyTest.SomeProperty}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("PropertyTest"),
                                    Helpers.MakeSomeIdent("SomeProperty")
                                ),
                                Expression.MakeConstant("int", 100)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "after: ${PropertyTest.SomeProperty}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("inst")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("PropertyTest"),
                                    Enumerable.Empty<Identifier>(),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "before: ${inst.SomeInstanceProperty}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    Helpers.MakeSomeIdent("SomeInstanceProperty")
                                ),
                                Expression.MakeConstant("int", 1000)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "after: ${inst.SomeInstanceProperty}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("InteroperabilityTest.PropertyTest"),
                    AstNode.MakeIdentifier("PropertyTest"),
                    AstNode.MakeIdentifier("./InteroperabilityTest.dll")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestEnumInCSharp()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/test_enum_in_csharp.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("EnumTest"),
                                        Helpers.MakeSomeIdent("TestEnumeration")
                                    ),
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("TestEnum"),
                                        Helpers.MakeSomeIdent("SomeField")
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "matched!")
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("tester")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("EnumTest"),
                                    Enumerable.Empty<Identifier>(),
                                    Enumerable.Empty<Expression>()
                                )
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("tester"),
                                    Helpers.MakeSomeIdent("TestProperty")
                                ),
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("TestEnum"),
                                    Helpers.MakeSomeIdent("YetAnotherField")
                                )
                            )
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("tester"),
                                        Helpers.MakeSomeIdent("TestEnumerationOnInstance")
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "matched again!")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("InteroperabilityTest.EnumTest"),
                        Helpers.MakeGenericType("InteroperabilityTest.TestEnum")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("EnumTest"),
                        AstNode.MakeIdentifier("TestEnum")
                    ),
                    "./InteroperabilityTest.dll"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Attributes()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/attributes.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeClassDecl(
                    "AuthorAttribute",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("Attribute")
                    ),
                    Modifiers.None,
                    AstNode.MakeAttributeSection(
                        null,
                        Expression.MakeObjectCreation(
                            Helpers.MakeGenericType("AttributeUsage"),
                            Helpers.MakeSeq(
                                AstNode.MakeIdentifier("validOn")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("AttributeTargets"),
                                    Helpers.MakeSomeIdent("All")
                                )
                            )
                        )
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType("_name", AstType.MakePrimitiveType("string")),
                        Expression.Null,
                        Modifiers.Immutable | Modifiers.Private
                    )
                ),
                EntityDeclaration.MakeField(
                    Helpers.MakeSomePatternWithType("_y"),
                    Expression.MakeConstant("int", 100),
                    Modifiers.Immutable,
                    AstNode.MakeAttributeSection(
                        null,
                        Expression.MakeObjectCreation(
                            Helpers.MakeGenericType("AuthorAttribute"),
                            AstNode.MakeIdentifier("_name"),
                            Expression.MakeConstant("string", "train12")
                        )
                    )
                ),
                Helpers.MakeFunc(
                    "doSomethingInModule",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeEmptyStmt()
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None,
                    null,
                    AstNode.MakeAttributeSection(
                        null,
                        Expression.MakeObjectCreation(
                            Helpers.MakeGenericType("ObsoleteAttribute")
                        )
                    )
                ),
                Helpers.MakeClassDecl(
                    "AttributeTest",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    AstNode.MakeAttributeSection(
                        null,
                        Expression.MakeObjectCreation(
                            Helpers.MakeGenericType("SerializableAttribute")
                        )
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType("_x", AstType.MakePrimitiveType("int")),
                        Expression.Null,
                        Modifiers.Immutable | Modifiers.Private,
                        AstNode.MakeAttributeSection(
                            null,
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("ObsoleteAttribute")
                            )
                        )
                    ),
                    Helpers.MakeFunc(
                        "doSomething",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "_dummy",
                                AstType.MakePrimitiveType("string"),
                                Expression.Null,
                                AstNode.MakeAttributeSection(
                                    null,
                                    Expression.MakeObjectCreation(
                                        Helpers.MakeGenericType("AuthorAttribute"),
                                        AstNode.MakeIdentifier("_name"),
                                        Expression.MakeConstant("string", "train12")
                                    )
                                )
                            )
                        ),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "Do something")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public,
                        null,
                        AstNode.MakeAttributeSection(
                            null,
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("ConditionalAttribute"),
                                AstNode.MakeIdentifier("conditionString"),
                                Expression.MakeConstant("string", "DEBUG")
                            )
                        )
                    ),
                    Helpers.MakeFunc(
                        "doSomething2",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "n",
                                AstType.MakePlaceholderType(),
                                Expression.MakeConstant("int", 100)
                            )
                        ),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "${n}")
                                )
                            ),
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeConstant("int", 10)
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("x")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("AttributeTest"),
                                    AstNode.MakeIdentifier("_x"),
                                    Expression.MakeConstant("int", 10)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("x"),
                                    Helpers.MakeSomeIdent("doSomething")
                                ),
                                Expression.MakeConstant("string", "some string")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("x"),
                                    Helpers.MakeSomeIdent("doSomething2")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        AstType.MakeSimpleType("System.SerializableAttribute"),
                        AstType.MakeSimpleType("System.CLSCompliantAttribute"),
                        AstType.MakeSimpleType("System.ObsoleteAttribute"),
                        AstType.MakeSimpleType("System.Attribute"),
                        AstType.MakeSimpleType("System.AttributeUsageAttribute"),
                        AstType.MakeSimpleType("System.AttributeTargets")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("SerializableAttribute"),
                        AstNode.MakeIdentifier("CLSCompliantAttribute"),
                        AstNode.MakeIdentifier("ObsoleteAttribute"),
                        AstNode.MakeIdentifier("Attribute"),
                        AstNode.MakeIdentifier("AttributeUsageAttribute"),
                        AstNode.MakeIdentifier("AttributeTargets")
                    )
                ),
                AstNode.MakeImportDecl(
                    AstType.MakeSimpleType("System.Diagnostics.ConditionalAttribute"),
                    AstNode.MakeIdentifier("ConditionalAttribute")
                ),
                AstNode.MakeImportDecl(
                    AstType.MakeSimpleType("System.Reflection.AssemblyDescriptionAttribute"),
                    AstNode.MakeIdentifier("AssemblyDescriptionAttribute")
                )
            ), Helpers.MakeSeq(
                AstNode.MakeAttributeSection(
                    "asm",
                    Expression.MakeObjectCreation(
                        Helpers.MakeGenericType("AssemblyDescriptionAttribute"),
                        AstNode.MakeIdentifier("description"),
                        Expression.MakeConstant("string", "test assembly for attributes")
                    )
                ),
                AstNode.MakeAttributeSection(
                    null,
                    Expression.MakeObjectCreation(
                        Helpers.MakeGenericType("CLSCompliantAttribute"),
                        AstNode.MakeIdentifier("isCompliant"),
                        Expression.MakeConstant("bool", true)
                    )
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Enum1()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/enum1.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeEnumDecl(
                    "Union",
                    Modifiers.None,
                    Helpers.MakeTupleStyleMember(
                        "A",
                        "Union",
                        null,
                        Helpers.MakePrimitiveType("intseq")
                    ),
                    Helpers.MakeTupleStyleMember(
                        "B",
                        "Union",
                        null,
                        Helpers.MakePrimitiveType("int"),
                        Helpers.MakePrimitiveType("uint")
                    ),
                    Helpers.MakeTupleStyleMember(
                        "C",
                        "Union",
                        null,
                        Helpers.MakePrimitiveType("string"),
                        Helpers.MakePrimitiveType("char")
                    ),
                    Helpers.MakeTupleStyleMember(
                        "D",
                        "Union"
                    ),
                    Helpers.MakeFunc(
                        "print",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "${some_intseq}")
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Union"),
                                            Helpers.MakeGenericType("A")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("some_intseq")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "${some_int}, ${some_uint}")
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Union"),
                                            Helpers.MakeGenericType("B")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("some_int")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("some_uint")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "${some_str}, ${some_char}")
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Union"),
                                            Helpers.MakeGenericType("C")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("some_str")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("some_char")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "D")
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Union"),
                                            Helpers.MakeGenericType("D")
                                        )
                                    )
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("some_enum")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Union"),
                                        Helpers.MakeGenericType("A")
                                    ),
                                    AstNode.MakeIdentifier("0"),
                                    Expression.MakeIntSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 10),
                                        Expression.MakeConstant("int", 1),
                                        false
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("some_enum2")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Union"),
                                        Helpers.MakeGenericType("B")
                                    ),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("0"),
                                        AstNode.MakeIdentifier("1")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("uint", 2u)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${some_enum}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${some_enum2}")
                            )
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("some_enum"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${some_intseq}")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Union"),
                                        Helpers.MakeGenericType("A")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        Helpers.MakeSomeIdent("some_intseq")
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${some_int}, ${some_uint}")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Union"),
                                        Helpers.MakeGenericType("B")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        Helpers.MakeSomeIdent("some_int")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        Helpers.MakeSomeIdent("some_uint")
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${some_str}, ${some_char}")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Union"),
                                        Helpers.MakeGenericType("C")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        Helpers.MakeSomeIdent("some_str")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        Helpers.MakeSomeIdent("some_char")
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "D")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Union"),
                                        Helpers.MakeGenericType("D")
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("some_enum2"),
                                    Helpers.MakeSomeIdent("print")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Enum2()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/enum2.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeEnumDecl(
                    "SomeEnum",
                    Modifiers.None,
                    Helpers.MakeRawValueEnumField("A", "SomeEnum", 1),
                    Helpers.MakeRawValueEnumField("B", "SomeEnum", 2),
                    Helpers.MakeRawValueEnumField("C", "SomeEnum", 3),
                    Helpers.MakeFunc(
                        "print",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "A")
                                        )
                                    ),
                                    PatternConstruct.MakeTypePathPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SomeEnum"),
                                            Helpers.MakeGenericType("A")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "B")
                                        )
                                    ),
                                    PatternConstruct.MakeTypePathPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SomeEnum"),
                                            Helpers.MakeGenericType("B")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "C")
                                        )
                                    ),
                                    PatternConstruct.MakeTypePathPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SomeEnum"),
                                            Helpers.MakeGenericType("C")
                                        )
                                    )
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            Helpers.MakeGenericType("SomeEnum")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "matchEnum",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    Helpers.MakeSomeIdent("x")
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "A in matchEnum")
                                        )
                                    ),
                                    PatternConstruct.MakeTypePathPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SomeEnum"),
                                            Helpers.MakeGenericType("A")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "B in matchEnum")
                                        )
                                    ),
                                    PatternConstruct.MakeTypePathPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SomeEnum"),
                                            Helpers.MakeGenericType("B")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "C in matchEnum")
                                        )
                                    ),
                                    PatternConstruct.MakeTypePathPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SomeEnum"),
                                            Helpers.MakeGenericType("C")
                                        )
                                    )
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("enum_a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakePath(
                                    Helpers.MakeSomeIdent("SomeEnum"),
                                    Helpers.MakeSomeIdent("A")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${enum_a}")
                            )
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("enum_a"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "A")
                                    )
                                ),
                                PatternConstruct.MakeTypePathPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("SomeEnum"),
                                        Helpers.MakeGenericType("A")
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "B")
                                    )
                                ),
                                PatternConstruct.MakeTypePathPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("SomeEnum"),
                                        Helpers.MakeGenericType("B")
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "C")
                                    )
                                ),
                                PatternConstruct.MakeTypePathPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("SomeEnum"),
                                        Helpers.MakeGenericType("C")
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("enum_a"),
                                    Helpers.MakeSomeIdent("print")
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("some_class")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("SomeClass"),
                                    AstNode.MakeIdentifier("x"),
                                    Helpers.MakeIdentifierPath("enum_a")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("some_class"),
                                    Helpers.MakeSomeIdent("matchEnum")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void GenericClass()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/generic_class.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeInterfaceDecl(
                    "SomeInterface",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(
                            AstType.MakeParameterType("T"),
                            null
                        )
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        AstType.MakeParameterType("T"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "GenericClass",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType(
                            "SomeInterface",
                            AstType.MakeParameterType("T")
                        )
                    ),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(
                            AstType.MakeParameterType("T"),
                            null
                        )
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            AstType.MakeParameterType("T")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    Helpers.MakeSomeIdent("x")
                                )
                            )
                        ),
                        AstType.MakeParameterType("T"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeEnumDecl(
                    "MyResult",
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(
                            AstType.MakeParameterType("T"),
                            null
                        ),
                        AstNode.MakeTypeConstraint(
                            AstType.MakeParameterType("E"),
                            null
                        )
                    ),
                    Helpers.MakeTupleStyleMember(
                        "Ok",
                        "MyResult",
                        null,
                        AstType.MakeParameterType("T")
                    ),
                    Helpers.MakeTupleStyleMember(
                        "Error",
                        "MyResult",
                        null,
                        AstType.MakeParameterType("E")
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("GenericClass"),
                                    AstNode.MakeIdentifier("x"),
                                    Expression.MakeConstant("int", 10),
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        AstType.MakePrimitiveType("int")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("b")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("GenericClass"),
                                    AstNode.MakeIdentifier("x"),
                                    Expression.MakeConstant("int", 20)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("c")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeObjectCreation(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("MyResult"),
                                        Helpers.MakeGenericType("Ok")
                                    ),
                                    AstNode.MakeIdentifier("0"),
                                    Expression.MakeConstant("int", 10),
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        AstType.MakePrimitiveType("int")
                                    ),
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        AstType.MakePrimitiveType("string")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("d")
                            ),
                            Helpers.MakeSeq(
                                Helpers.MakeObjectCreation(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("MyResult"),
                                        Helpers.MakeGenericType("Error")
                                    ),
                                    AstNode.MakeIdentifier("0"),
                                    Expression.MakeConstant("string", "Error"),
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        AstType.MakePrimitiveType("int")
                                    ),
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        AstType.MakePrimitiveType("string")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}, ${c}, ${d}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void InterfaceAndUpcast()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/interface_and_upcast.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeInterfaceDecl(
                    "HasArea",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "calculateArea",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        Helpers.MakePrimitiveType("double"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "Circle",
                    Helpers.MakeSeq<AstType>(
                        Helpers.MakeGenericType("HasArea")
                    ),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSeq(
                            Helpers.MakeSomePatternWithType(
                                "_x",
                                Helpers.MakePrimitiveType("int")
                            ),
                            Helpers.MakeSomePatternWithType(
                                "_y",
                                Helpers.MakePrimitiveType("int")
                            ),
                            Helpers.MakeSomePatternWithType(
                                "radius",
                                Helpers.MakePrimitiveType("int")
                            )
                        ),
                        Helpers.MakeSeq(
                            Expression.Null,
                            Expression.Null,
                            Expression.Null
                        ),
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "calculateArea",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Times,
                                    Expression.MakeConstant("double", 2.0),
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Times,
                                        Expression.MakeMemRef(
                                            Helpers.MakeIdentifierPath("Math"),
                                            Helpers.MakeSomeIdent("PI")
                                        ),
                                        Expression.MakeParen(
                                            Expression.MakeCastExpr(
                                                Expression.MakeParen(
                                                    Expression.MakeBinaryExpr(
                                                        OperatorType.Times,
                                                        Expression.MakeMemRef(
                                                            Expression.MakeSelfRef(),
                                                            Helpers.MakeSomeIdent("radius")
                                                        ),
                                                        Expression.MakeMemRef(
                                                            Expression.MakeSelfRef(),
                                                            Helpers.MakeSomeIdent("radius")
                                                        )
                                                    )
                                                ),
                                                AstType.MakePrimitiveType("double")
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        Helpers.MakePrimitiveType("double"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "Rectangle",
                    Helpers.MakeSeq<AstType>(
                        Helpers.MakeGenericType("HasArea")
                    ),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSeq(
                            Helpers.MakeSomePatternWithType(
                                "_x",
                                Helpers.MakePrimitiveType("int")
                            ),
                            Helpers.MakeSomePatternWithType(
                                "_y",
                                Helpers.MakePrimitiveType("int")
                            ),
                            Helpers.MakeSomePatternWithType(
                                "width",
                                Helpers.MakePrimitiveType("int")
                            ),
                            Helpers.MakeSomePatternWithType(
                                "height",
                                Helpers.MakePrimitiveType("int")
                            )
                        ),
                        Helpers.MakeSeq(
                            Expression.Null,
                            Expression.Null,
                            Expression.Null,
                            Expression.Null
                        ),
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "calculateArea",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeCastExpr(
                                    Expression.MakeParen(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Times,
                                            Expression.MakeMemRef(
                                                Expression.MakeSelfRef(),
                                                Helpers.MakeSomeIdent("width")
                                            ),
                                            Expression.MakeMemRef(
                                                Expression.MakeSelfRef(),
                                                Helpers.MakeSomeIdent("height")
                                            )
                                        )
                                    ),
                                    Helpers.MakePrimitiveType("double")
                                )
                            )
                        ),
                        Helpers.MakePrimitiveType("double"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("has_area")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeCastExpr(
                                    Expression.MakeObjectCreation(
                                        Helpers.MakeGenericType("Circle"),
                                        Helpers.MakeSeq(
                                            AstNode.MakeIdentifier("_x"),
                                            AstNode.MakeIdentifier("_y"),
                                            AstNode.MakeIdentifier("radius")
                                        ),
                                        Helpers.MakeSeq<Expression>(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 10)
                                        )
                                    ),
                                    Helpers.MakeGenericType("HasArea")
                                )
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "Circle: ${has_area.calculateArea()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIdentifierPath("has_area"),
                                Expression.MakeCastExpr(
                                    Expression.MakeObjectCreation(
                                        Helpers.MakeGenericType("Rectangle"),
                                        Helpers.MakeSeq(
                                            AstNode.MakeIdentifier("_x"),
                                            AstNode.MakeIdentifier("_y"),
                                            AstNode.MakeIdentifier("width"),
                                            AstNode.MakeIdentifier("height")
                                        ),
                                        Helpers.MakeSeq<Expression>(
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 0),
                                            Expression.MakeConstant("int", 5),
                                            Expression.MakeConstant("int", 5)
                                        )
                                    ),
                                    Helpers.MakeGenericType("HasArea")
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "Rectangle: ${has_area.calculateArea()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Math"),
                    AstNode.MakeIdentifier("Math")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ImportGenericTypes()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/import_generic_types.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", new List<EntityDeclaration>{
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("dict")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("SortedDictionary"),
                                    Enumerable.Empty<Identifier>(),
                                    Enumerable.Empty<Expression>(),
                                    Helpers.MakeSeq(
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            AstType.MakePrimitiveType("string")
                                        ),
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            AstType.MakePrimitiveType("int")
                                        )
                                    )
                                )
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${dict}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            }, Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("System.Collections.Generic.IEnumerable", AstType.MakePlaceholderType()),
                        Helpers.MakeGenericType(
                            "System.Collections.Generic.SortedDictionary",
                            AstType.MakePlaceholderType(),
                            AstType.MakePlaceholderType()
                        )
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("IEnumerable"),
                        AstNode.MakeIdentifier("SortedDictionary")
                    )
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ArgsOnMain()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/args_on_main.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq(
                Helpers.MakeFunc(
                    "main",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "args",
                            Helpers.MakeGenericType(
                                "array",
                                Helpers.MakePrimitiveType("string")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.LessThan,
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("args"),
                                        Helpers.MakeSomeIdent("Length")
                                    ),
                                    Expression.MakeConstant("int", 2)
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "The length of args is less than what we expected: ${args.Length}")
                                    )
                                )
                            ),
                            Statement.MakeIfStmt(
                                PatternConstruct.MakeExpressionPattern(
                                    Expression.MakeUnaryExpr(
                                        OperatorType.Not,
                                        Helpers.MakeCallExpression(
                                            Expression.MakeMemRef(
                                                Helpers.MakeIndexer(
                                                    Helpers.MakeIdentifierPath("args"),
                                                    Expression.MakeConstant("int", 0)
                                                ),
                                                Helpers.MakeSomeIdent("Equals")
                                            ),
                                            Expression.MakeConstant("string", "parameter")
                                        )
                                    )
                                ),
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "Expected the first parameter is 'parameter'")
                                        )
                                    )
                                ),
                                Statement.MakeIfStmt(
                                    PatternConstruct.MakeExpressionPattern(
                                        Expression.MakeUnaryExpr(
                                            OperatorType.Not,
                                            Helpers.MakeCallExpression(
                                                Expression.MakeMemRef(
                                                    Helpers.MakeIndexer(
                                                        Helpers.MakeIdentifierPath("args"),
                                                        Expression.MakeConstant("int", 1)
                                                    ),
                                                    Helpers.MakeSomeIdent("Equals")
                                                ),
                                                Expression.MakeConstant("string", "second")
                                            )
                                        )
                                    ),
                                    Statement.MakeBlock(
                                        Statement.MakeExprStmt(
                                            Helpers.MakeCallExpression(
                                                Helpers.MakeIdentifierPath("println"),
                                                Expression.MakeConstant("string", "Unexpected second parameter")
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestReference()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/test_reference.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq(
                Helpers.MakeFunc(
                    "changeValue",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "val",
                            AstType.MakeReferenceType(
                                Helpers.MakePrimitiveType("int")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIdentifierPath("val"),
                                Expression.MakeConstant("int", 1000)
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("a")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 100)
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "Before: ${a}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("changeValue"),
                                Expression.MakeUnaryExpr(
                                    OperatorType.Reference,
                                    Helpers.MakeIdentifierPath("a")
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "After: ${a}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ClassAndMethodModifiers()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/class_and_method_modifiers.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSeq(
                            Helpers.MakePaticularPatternWithType(
                                "X",
                                Helpers.MakePrimitiveType("int")
                            ),
                            Helpers.MakePaticularPatternWithType(
                                "Y",
                                Helpers.MakePrimitiveType("int")
                            )
                        ),
                        Helpers.MakeSeq(
                            Expression.Null,
                            Expression.Null
                        ),
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "ToString",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeConstant("string", "(x: ${self.X}, y: ${self.Y})")
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public | Modifiers.Override
                    )
                ),
                Helpers.MakeClassDecl(
                    "AbstractClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.Abstract,
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        Helpers.MakePrimitiveType("int"),
                        Modifiers.Abstract | Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "ChildClass",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("AbstractClass")
                    ),
                    Modifiers.Sealed,
                    EntityDeclaration.MakeField(
                        Helpers.MakePaticularPatternWithType(
                            "x",
                            Helpers.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    Helpers.MakeSomeIdent("x")
                                )
                            )
                        ),
                        Helpers.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("seq"),
                            Helpers.MakeSequenceInitializer(
                                Helpers.MakeGenericType(
                                    "vector",
                                    AstType.MakePlaceholderType()
                                ),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("SomeClass"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("X"),
                                        AstNode.MakeIdentifier("Y")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 1),
                                        Expression.MakeConstant("int", 2)
                                    )
                                ),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("SomeClass"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("X"),
                                        AstNode.MakeIdentifier("Y")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 3),
                                        Expression.MakeConstant("int", 4)
                                    )
                                ),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("SomeClass"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("X"),
                                        AstNode.MakeIdentifier("Y")
                                    ),
                                    Helpers.MakeSeq(
                                        Expression.MakeConstant("int", 5),
                                        Expression.MakeConstant("int", 6)
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("child_inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("ChildClass"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("x"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("child_inst"),
                                    Helpers.MakeSomeIdent("getX")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${seq}, ${x}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ConditionalXXOperators()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/conditional_xx_operators.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSeq(
                                Helpers.MakeSomePatternWithType("x"),
                                Helpers.MakeSomePatternWithType("flag")
                            ),
                            Helpers.MakeSeq(
                                Expression.MakeConstant("int", 10),
                                Expression.MakeConstant("bool", true)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalAnd,
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Equality,
                                        Helpers.MakeIdentifierPath("x"),
                                        Expression.MakeConstant("int", 10)
                                    ),
                                    Helpers.MakeIdentifierPath("flag")
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "conditional and")
                                    )
                                )
                            )
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalOr,
                                    Expression.MakeBinaryExpr(
                                        OperatorType.Equality,
                                        Helpers.MakeIdentifierPath("x"),
                                        Expression.MakeConstant("int", 10)
                                    ),
                                    Helpers.MakeIdentifierPath("flag")
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "conditional or")
                                    )
                                )
                            )
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalAnd,
                                    Expression.MakeConstant("bool", true),
                                    Expression.MakeParen(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.ConditionalOr,
                                            Expression.MakeConstant("bool", false),
                                            Expression.MakeConstant("bool", true)
                                        )
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "first true")
                                    )
                                )
                            )
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalAnd,
                                    Expression.MakeConstant("bool", false),
                                    Expression.MakeParen(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.ConditionalOr,
                                            Expression.MakeConstant("bool", true),
                                            Expression.MakeConstant("bool", true)
                                        )
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "second true")
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "second false")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Downcast()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/downcast.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "doSomething",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "called with SomeClass")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Virtual | Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "DerivedClass",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("SomeClass")
                    ),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "doSomething",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "called with DerivedClass")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Override | Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeCastExpr(
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("DerivedClass")
                                ),
                                Helpers.MakeGenericType("SomeClass")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    Helpers.MakeSomeIdent("doSomething")
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst_type"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    Helpers.MakeSomeIdent("GetType")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "inst: ${inst_type.Name}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("derived"),
                            Expression.MakeCastExpr(
                                Helpers.MakeIdentifierPath("inst"),
                                Helpers.MakeGenericType("DerivedClass")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("derived"),
                                    Helpers.MakeSomeIdent("doSomething")
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("derived_type"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("derived"),
                                    Helpers.MakeSomeIdent("GetType")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "derived: ${derived_type.Name}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Type"),
                    AstNode.MakeIdentifier("Type")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void HelloWorld()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/hello_world.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "Hello, world!")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void GenericMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/generic_method.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "printT",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "value",
                                AstType.MakeParameterType("T")
                            )
                        ),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "${value}")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public | Modifiers.Static,
                        Helpers.MakeSeq(
                            AstNode.MakeTypeConstraint(
                                AstType.MakeParameterType("T"),
                                null
                            )
                        )
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeConstant("int", 10),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b"),
                            Expression.MakeConstant("double", 15.0),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("SomeClass"),
                                    Helpers.MakeSomeIdent("printT")
                                ),
                                Helpers.MakeSeq(
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        Helpers.MakePrimitiveType("int")
                                    )
                                ),
                                Helpers.MakeIdentifierPath("a")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("SomeClass"),
                                    Helpers.MakeSomeIdent("printT")
                                ),
                                Helpers.MakeSeq(
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        Helpers.MakePrimitiveType("double")
                                    )
                                ),
                                Helpers.MakeIdentifierPath("b")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void FunctionType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/function_type.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeExactPatternWithType(
                                "c",
                                Helpers.MakeFunctionType(
                                    "closure",
                                    Helpers.MakePrimitiveType("int"),
                                    Helpers.MakePrimitiveType("int")
                                )
                            ),
                            Expression.MakeClosureExpression(
                                AstType.MakePrimitiveType("int"),
                                Statement.MakeBlock(
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Plus,
                                            Helpers.MakeIdentifierPath("i"),
                                            Expression.MakeConstant("int", 1)
                                        )
                                    )
                                ),
                                TextLocation.Empty,
                                null,
                                EntityDeclaration.MakeParameter(
                                    "i",
                                    Helpers.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("c"),
                                Expression.MakeConstant("int", 1)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ParametersInferredFromInitialValues()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/parameters_inferred_from_initial_values.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq(
                Helpers.MakeFunc(
                    "someFunc",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "a",
                            AstType.MakePlaceholderType(),
                            Expression.MakeConstant("int", 1)
                        ),
                        EntityDeclaration.MakeParameter(
                            "b",
                            AstType.MakePlaceholderType(),
                            Expression.MakeConstant("bool", true)
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}, ${b}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("someFunc")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("someFunc"),
                                Expression.MakeConstant("int", 2),
                                Expression.MakeConstant("bool", false)
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void GenericClassWithWhere()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/generic_class_with_where.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass2",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "X",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("X")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "ToString",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeConstant("string", "${self.X}")
                            )
                        ),
                        AstType.MakePrimitiveType("string"),
                        Modifiers.Public | Modifiers.Override
                    )
                ),
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(
                            AstType.MakeParameterType("T"),
                            Helpers.MakeSeq(
                                Helpers.MakeGenericType("SomeClass2")
                            )
                        )
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            AstType.MakeParameterType("T")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "print",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeConstant("string", "field: ${self.x.X}, method: ${self.x.getX()}")
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass2"),
                                AstNode.MakeIdentifier("X"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass"),
                                AstNode.MakeIdentifier("x"),
                                Helpers.MakeIdentifierPath("a"),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    Helpers.MakeGenericType("SomeClass2")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst.print()}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst2"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass"),
                                AstNode.MakeIdentifier("x"),
                                Helpers.MakeIdentifierPath("a")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst2.print()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void GenericMethodWithWhere()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/generic_method_with_where.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass2",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            Helpers.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Immutable | Modifiers.Private
                    ),
                    Helpers.MakeFunc(
                        "ToString",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeConstant("string", "${self.x}")
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Override | Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "printT",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "value",
                                AstType.MakeParameterType("T")
                            )
                        ),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "${value}")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public | Modifiers.Static,
                        Helpers.MakeSeq(
                            Helpers.MakeTypeConstraint(
                                AstType.MakeParameterType("T"),
                                Helpers.MakeGenericType("SomeClass2")
                            )
                        )
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Helpers.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass2"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("SomeClass"),
                                    Helpers.MakeSomeIdent("printT")
                                ),
                                Helpers.MakeSeq(
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        Helpers.MakeGenericType("SomeClass2")
                                    )
                                ),
                                Helpers.MakeIdentifierPath("a")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void AssignmentUsingIndexer()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/assignment_using_indexer.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Helpers.MakeSequenceInitializer(
                                Helpers.MakeGenericType("array"),
                                Expression.MakeConstant("int", 1),
                                Expression.MakeConstant("int", 2),
                                Expression.MakeConstant("int", 3)
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a"),
                                    Expression.MakeConstant("int", 0)
                                ),
                                Expression.MakeConstant("int", 4)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b"),
                            Expression.MakeConstant("int", 2),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a"),
                                    Helpers.MakeIdentifierPath("b")
                                ),
                                Expression.MakeConstant("int", 8)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Plus,
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a"),
                                    Helpers.MakeIdentifierPath("b")
                                ),
                                Expression.MakeConstant("int", 2)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a2"),
                            Helpers.MakeSequenceInitializer(
                                Helpers.MakeGenericType("vector"),
                                Expression.MakeConstant("int", 1),
                                Expression.MakeConstant("int", 2),
                                Expression.MakeConstant("int", 3)
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a2"),
                                    Expression.MakeConstant("int", 0)
                                ),
                                Expression.MakeConstant("int", 4)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a2}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a2"),
                                    Helpers.MakeIdentifierPath("b")
                                ),
                                Expression.MakeConstant("int", 8)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a2}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAugmentedAssignment(
                                OperatorType.Plus,
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("a2"),
                                    Helpers.MakeIdentifierPath("b")
                                ),
                                Expression.MakeConstant("int", 2)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a2}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ParenthesizedPatterns()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/parenthesized_patterns.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("flag"),
                            Expression.MakeConstant("bool", false),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("tmp"),
                            Expression.MakeConstant("int", 1),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalAnd,
                                    Expression.MakeUnaryExpr(
                                        OperatorType.Not,
                                        Helpers.MakeIdentifierPath("flag")
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.ConditionalOr,
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Equality,
                                                Helpers.MakeIdentifierPath("tmp"),
                                                Expression.MakeConstant("int", 1)
                                            ),
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Equality,
                                                Helpers.MakeIdentifierPath("tmp"),
                                                Expression.MakeConstant("int", 2)
                                            )
                                        )
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "1 or 2")
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("flag2"),
                            Expression.MakeConstant("bool", false),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalOr,
                                    Expression.MakeParen(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.ConditionalAnd,
                                            Helpers.MakeIdentifierPath("flag"),
                                            Helpers.MakeIdentifierPath("flag2")
                                        )
                                    ),
                                    Expression.MakeBinaryExpr(
                                        OperatorType.ConditionalOr,
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Equality,
                                            Helpers.MakeIdentifierPath("tmp"),
                                            Expression.MakeConstant("int", 1)
                                        ),
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Equality,
                                            Helpers.MakeIdentifierPath("tmp"),
                                            Expression.MakeConstant("int", 2)
                                        )
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "1 or 2 with flag2")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void UsingFieldOnConcreteTypeInEnum()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/using_field_on_concrete_type_in_enum.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "MyClass2",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "X",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    )
                ),
                Helpers.MakeFunc(
                    "isMatch",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "value",
                            Helpers.MakeGenericType(
                                "MyOption",
                                Helpers.MakeGenericType("MyClass2")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("value"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${value.X}")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("MyOption"),
                                        Helpers.MakeGenericType("Some")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(AstNode.MakeIdentifier("value"))
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "None")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("MyOption"),
                                        Helpers.MakeGenericType("None")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("some"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("MyOption"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("MyClass2"),
                                    AstNode.MakeIdentifier("X"),
                                    Expression.MakeConstant("int", 1)
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    Helpers.MakeGenericType("MyClass2")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("none"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("MyOption"),
                                    Helpers.MakeGenericType("None")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    Helpers.MakeGenericType("MyClass2")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("isMatch"),
                                Helpers.MakeIdentifierPath("some")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("isMatch"),
                                Helpers.MakeIdentifierPath("none")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("my_option::MyOption"),
                    AstNode.MakeIdentifier("MyOption"),
                    AstNode.MakeIdentifier("../my_option.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void EnumGenerics()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/enum_generics.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "isMatch",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "value",
                            Helpers.MakeGenericType(
                                "MyOption",
                                AstType.MakePrimitiveType("int")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("value"),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${value}")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("MyOption"),
                                        Helpers.MakeGenericType("Some")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(AstNode.MakeIdentifier("value"))
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "None")
                                    )
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("MyOption"),
                                        Helpers.MakeGenericType("None")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("some"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("MyOption"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 1),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("none"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("MyOption"),
                                    Helpers.MakeGenericType("None")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("isMatch"),
                                Helpers.MakeIdentifierPath("some")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("isMatch"),
                                Helpers.MakeIdentifierPath("none")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("my_option::MyOption"),
                    AstNode.MakeIdentifier("MyOption"),
                    AstNode.MakeIdentifier("../my_option.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void NotebehaviorParenthesizedPatterns()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/parenthesized_patterns.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("flag"),
                            Expression.MakeConstant("bool", false),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("tmp"),
                            Expression.MakeConstant("int", 1),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalAnd,
                                    Expression.MakeUnaryExpr(
                                        OperatorType.Not,
                                        Helpers.MakeIdentifierPath("flag")
                                    ),
                                    Expression.MakeParen(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.ConditionalOr,
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Equality,
                                                Helpers.MakeIdentifierPath("tmp"),
                                                Expression.MakeConstant("int", 1)
                                            ),
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Equality,
                                                Helpers.MakeIdentifierPath("tmp"),
                                                Expression.MakeConstant("int", 2)
                                            )
                                        )
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "1 or 2")
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("flag2"),
                            Expression.MakeConstant("bool", false),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.ConditionalOr,
                                    Expression.MakeParen(
                                        Expression.MakeBinaryExpr(
                                            OperatorType.ConditionalAnd,
                                            Helpers.MakeIdentifierPath("flag"),
                                            Helpers.MakeIdentifierPath("flag2")
                                        )
                                    ),
                                    Expression.MakeBinaryExpr(
                                        OperatorType.ConditionalOr,
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Equality,
                                            Helpers.MakeIdentifierPath("tmp"),
                                            Expression.MakeConstant("int", 1)
                                        ),
                                        Expression.MakeBinaryExpr(
                                            OperatorType.Equality,
                                            Helpers.MakeIdentifierPath("tmp"),
                                            Expression.MakeConstant("int", 2)
                                        )
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "1 or 2 with flag2")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));
        }

        [Test]
        public void UsingFieldOnConcreteTypeInClassWithWhere()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/using_field_on_concrete_type_in_class_with_where.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "MyClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "X",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    )
                ),
                Helpers.MakeClassDecl(
                    "GenericClass2",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        Helpers.MakeTypeConstraint(
                            AstType.MakeParameterType("T"),
                            Helpers.MakeGenericType("MyClass")
                        )
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType("X", AstType.MakeParameterType("T")),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("X")
                                )
                            )
                        ),
                        AstType.MakeParameterType("T"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("GenericClass2"),
                                AstNode.MakeIdentifier("X"),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("MyClass"),
                                    AstNode.MakeIdentifier("X"),
                                    Expression.MakeConstant("int", 10)
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    Helpers.MakeGenericType("MyClass")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("GenericClass2"),
                                AstNode.MakeIdentifier("X"),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("MyClass"),
                                    AstNode.MakeIdentifier("X"),
                                    Expression.MakeConstant("int", 20)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a.getX().X}, ${b.getX().X}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a.X.X}, ${b.X.X}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ReturningSelfType()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/returning_self_type.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "TestClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "X",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "returnSelf",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeSelfRef()
                            )
                        ),
                        Helpers.MakeGenericType("TestClass"),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "returnSelfWithOption",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeObjectCreation(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("MyOption"),
                                        Helpers.MakeGenericType("Some")
                                    ),
                                    AstNode.MakeIdentifier("0"),
                                    Expression.MakeSelfRef(),
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        Helpers.MakeGenericType("TestClass")
                                    )
                                )
                            )
                        ),
                        Helpers.MakeGenericType(
                            "MyOption",
                            Helpers.MakeGenericType("TestClass")
                        ),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("X")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("TestClass"),
                                AstNode.MakeIdentifier("X"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst2"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("returnSelf")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeMemRef(
                                Helpers.MakeIdentifierPath("inst2"),
                                AstNode.MakeIdentifier("X")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst2"),
                                    AstNode.MakeIdentifier("getX")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("returnSelfWithOption")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("c"),
                            Expression.MakeMemRef(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("option"),
                                        AstNode.MakeIdentifier("unwrap")
                                    )
                                ),
                                AstNode.MakeIdentifier("X")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("d"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeCallExpression(
                                        Expression.MakeMemRef(
                                            Helpers.MakeIdentifierPath("option"),
                                            AstNode.MakeIdentifier("unwrap")
                                        )
                                    ),
                                    AstNode.MakeIdentifier("getX")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst}, ${inst2}, field: ${a}, method: ${b}, option(field): ${c}, option(method): ${d}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("my_option::MyOption"),
                    AstNode.MakeIdentifier("MyOption"),
                    AstNode.MakeIdentifier("../my_option.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void SelfClassAsParameter()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/self_class_as_parameter.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "x",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeConstant("string", "x: ${self.x}")
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Private
                    ),
                    Helpers.MakeFunc(
                        "someMethod",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "this",
                                Helpers.MakeGenericType("SomeClass")
                            )
                        ),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "${this.x}, ${this.getX()}")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst2"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 20)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("someMethod")
                                ),
                                Helpers.MakeIdentifierPath("inst2")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void UsingFieldOnConcreteTypeInEnumOutside()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/using_field_on_concrete_type_in_enum_outside.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "MyClass3",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "X",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("X")
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("some"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("MyOption"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("MyClass3"),
                                    AstNode.MakeIdentifier("X"),
                                    Expression.MakeConstant("int", 1)
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    Helpers.MakeGenericType("MyClass3")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "field: ${some.unwrap().X}, method: ${some.unwrap().getX()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("my_option::MyOption"),
                    AstNode.MakeIdentifier("MyOption"),
                    AstNode.MakeIdentifier("../my_option.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void UsingFieldOnConcreteTypeInClass()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/using_field_on_concrete_type_in_class.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "MyClass4",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "X",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("X")
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "GenericClass2",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(AstType.MakeParameterType("T"))
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            AstType.MakeParameterType("T")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("x")
                                )
                            )
                        ),
                        AstType.MakeParameterType("T"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("GenericClass2"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("MyClass4"),
                                    AstNode.MakeIdentifier("X"),
                                    Expression.MakeConstant("int", 2)
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    Helpers.MakeGenericType("MyClass4")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "field: ${inst.getX().X}, method: ${inst.getX().getX()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TypesPointAtEachOther()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/types_point_at_each_other.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "Husband",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "W",
                            Helpers.MakeGenericType(
                                "MyOption",
                                Helpers.MakeGenericType("Wife")
                            )
                        ),
                        Expression.Null,
                        Modifiers.Public
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeSomePatternWithType(
                            "age",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "setWife",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "newWife",
                                Helpers.MakeGenericType("Wife")
                            )
                        ),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Expression.MakeSingleAssignment(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("W")
                                    ),
                                    Expression.MakeObjectCreation(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        AstNode.MakeIdentifier("0"),
                                        Helpers.MakeIdentifierPath("newWife"),
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            Helpers.MakeGenericType("Wife")
                                        )
                                    )
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public | Modifiers.Mutating
                    ),
                    Helpers.MakeFunc(
                        "print",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "Husband: ${self.age}, Wife(field): ${self.W.unwrap().Age}, Wife(method): ${self.W.unwrap().getAge()}")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "Wife",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "_H",
                            Helpers.MakeGenericType(
                                "MyOption",
                                Helpers.MakeGenericType("Husband")
                            )
                        ),
                        Expression.Null,
                        Modifiers.Public
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "Age",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Public | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getAge",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("Age")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "setHusband",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "h",
                                Helpers.MakeGenericType("Husband")
                            )
                        ),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Expression.MakeSingleAssignment(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("_H")
                                    ),
                                    Expression.MakeObjectCreation(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        AstNode.MakeIdentifier("0"),
                                        Helpers.MakeIdentifierPath("h"),
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            Helpers.MakeGenericType("Husband")
                                        )
                                    )
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public | Modifiers.Mutating
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("h"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("Husband"),
                                Helpers.MakeSeq(
                                    AstNode.MakeIdentifier("W"),
                                    AstNode.MakeIdentifier("age")
                                ),
                                Helpers.MakeSeq<Expression>(
                                    Expression.MakeObjectCreation(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        AstNode.MakeIdentifier("0"),
                                        Expression.MakeObjectCreation(
                                            Helpers.MakeGenericType("Wife"),
                                            Helpers.MakeSeq(
                                                AstNode.MakeIdentifier("_H"),
                                                AstNode.MakeIdentifier("Age")
                                            ),
                                            Helpers.MakeSeq<Expression>(
                                                Expression.MakeObjectCreation(
                                                    AstType.MakeMemberType(
                                                        Helpers.MakeGenericType("MyOption"),
                                                        Helpers.MakeGenericType("None")
                                                    ),
                                                    AstType.MakeKeyValueType(
                                                        AstType.MakeParameterType(""),
                                                        Helpers.MakeGenericType("Husband")
                                                    )
                                                ),
                                                Expression.MakeConstant("int", 23)
                                            )
                                        ),
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            Helpers.MakeGenericType("Wife")
                                        )
                                    ),
                                    Expression.MakeConstant("int", 24)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeCallExpression(
                                        Expression.MakeMemRef(
                                            Expression.MakeMemRef(
                                                Helpers.MakeIdentifierPath("h"),
                                                AstNode.MakeIdentifier("W")
                                            ),
                                            AstNode.MakeIdentifier("unwrap")
                                        )
                                    ),
                                    AstNode.MakeIdentifier("setHusband")
                                ),
                                Helpers.MakeIdentifierPath("h")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("h"),
                                    AstNode.MakeIdentifier("print")
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("h"),
                                    AstNode.MakeIdentifier("setWife")
                                ),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("Wife"),
                                    Helpers.MakeSeq(
                                        AstNode.MakeIdentifier("_H"),
                                        AstNode.MakeIdentifier("Age")
                                    ),
                                    Helpers.MakeSeq<Expression>(
                                        Expression.MakeObjectCreation(
                                            AstType.MakeMemberType(
                                                Helpers.MakeGenericType("MyOption"),
                                                Helpers.MakeGenericType("Some")
                                            ),
                                            AstNode.MakeIdentifier("0"),
                                            Helpers.MakeIdentifierPath("h"),
                                            AstType.MakeKeyValueType(
                                                AstType.MakeParameterType(""),
                                                Helpers.MakeGenericType("Husband")
                                            )
                                        ),
                                        Expression.MakeConstant("int", 20)
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("h"),
                                    AstNode.MakeIdentifier("print")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("my_option::MyOption"),
                    AstNode.MakeIdentifier("MyOption"),
                    AstNode.MakeIdentifier("../my_option.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void EnumAndTypePointAtEachOther()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/enum_and_type_point_at_each_other.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeEnumDecl(
                    "SimpleAST",
                    Modifiers.None,
                    Helpers.MakeTupleStyleMember(
                        "Ident",
                        "SimpleAST",
                        null,
                        Helpers.MakeGenericType("SimpleAstNode")
                    ),
                    Helpers.MakeTupleStyleMember(
                        "Expr",
                        "SimpleAST",
                        null,
                        Helpers.MakeGenericType("SimpleAstNode")
                    ),
                    Helpers.MakeFunc(
                        "getChild",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeObjectCreation(
                                            AstType.MakeMemberType(
                                                Helpers.MakeGenericType("MyOption"),
                                                Helpers.MakeGenericType("None")
                                            ),
                                            AstType.MakeKeyValueType(
                                                AstType.MakeParameterType(""),
                                                Helpers.MakeGenericType("SimpleAST")
                                            )
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SimpleAST"),
                                            Helpers.MakeGenericType("Ident")
                                        ),
                                        PatternConstruct.MakeWildcardPattern()
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeMemRef(
                                            Helpers.MakeIdentifierPath("node"),
                                            AstNode.MakeIdentifier("Child")
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SimpleAST"),
                                            Helpers.MakeGenericType("Expr")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("node")
                                        )
                                    )
                                )
                            )
                        ),
                        Helpers.MakeGenericType(
                            "MyOption",
                            Helpers.MakeGenericType("SimpleAST")
                        ),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "print",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeConstant("string", "SimpleAST::Ident()")
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SimpleAST"),
                                            Helpers.MakeGenericType("Ident")
                                        ),
                                        PatternConstruct.MakeWildcardPattern()
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeConstant("string", "SimpleAST::Expr(${self.getChild().unwrap().print()})")
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("SimpleAST"),
                                            Helpers.MakeGenericType("Expr")
                                        ),
                                        PatternConstruct.MakeWildcardPattern()
                                    )
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("string"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "SimpleAstNode",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "Child",
                            Helpers.MakeGenericType(
                                "MyOption",
                                Helpers.MakeGenericType("SimpleAST")
                            )
                        ),
                        Expression.Null,
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "createAstNode",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "option",
                            Helpers.MakeGenericType(
                                "MyOption",
                                Helpers.MakeGenericType("SimpleAST")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SimpleAstNode"),
                                AstNode.MakeIdentifier("Child"),
                                Helpers.MakeIdentifierPath("option")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("child"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("SimpleAST"),
                                    Helpers.MakeGenericType("Ident")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("createAstNode"),
                                    Expression.MakeObjectCreation(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("None")
                                        ),
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            Helpers.MakeGenericType("SimpleAST")
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("parent"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("SimpleAST"),
                                    Helpers.MakeGenericType("Expr")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("createAstNode"),
                                    Expression.MakeObjectCreation(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        AstNode.MakeIdentifier("0"),
                                        Helpers.MakeIdentifierPath("child"),
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            Helpers.MakeGenericType("SimpleAST")
                                        )
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${parent.print()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("my_option::MyOption"),
                    AstNode.MakeIdentifier("MyOption"),
                    AstNode.MakeIdentifier("../my_option.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void CallModuleFunctionAfterMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/call_module_function_after_method.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "externalFunction",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "externalFunction called")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "a",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "a called")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Private
                    ),
                    Helpers.MakeFunc(
                        "publicMethod",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("a")
                                    )
                                )
                            ),
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("externalFunction")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("publicMethod")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void AdvancedFunctionCalls()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/advanced_function_calls.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "someFunction",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "x",
                            AstType.MakePrimitiveType("int")
                        ),
                        EntityDeclaration.MakeParameter(
                            "y",
                            AstType.MakePlaceholderType(),
                            Expression.MakeConstant("int", 1)
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${x}, ${y}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "someFunction2",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "x",
                            AstType.MakePrimitiveType("int")
                        ),
                        EntityDeclaration.MakeParameter(
                            "y",
                            Helpers.MakeGenericType(
                                "array",
                                AstType.MakePrimitiveType("int")
                            ),
                            null,
                            null,
                            true
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${x}, ${y}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("someFunction"),
                                Expression.MakeConstant("int", 1)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("someFunction"),
                                Expression.MakeConstant("int", 1),
                                Expression.MakeConstant("int", 2)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("someFunction2"),
                                Expression.MakeConstant("int", 3)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("someFunction2"),
                                Expression.MakeConstant("int", 3),
                                Expression.MakeConstant("int", 4),
                                Expression.MakeConstant("int", 5),
                                Expression.MakeConstant("int", 6),
                                Expression.MakeConstant("int", 7)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("ArgumentTest"),
                                    AstNode.MakeIdentifier("MethodWithOptional")
                                ),
                                Expression.MakeConstant("int", 10)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("ArgumentTest"),
                                    AstNode.MakeIdentifier("MethodWithOptional")
                                ),
                                Expression.MakeConstant("int", 10),
                                Expression.MakeConstant("int", 11)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("ArgumentTest"),
                                    AstNode.MakeIdentifier("MethodWithParams")
                                ),
                                Expression.MakeConstant("int", 12)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("ArgumentTest"),
                                    AstNode.MakeIdentifier("MethodWithParams")
                                ),
                                Expression.MakeConstant("int", 12),
                                Expression.MakeConstant("int", 13),
                                Expression.MakeConstant("int", 14),
                                Expression.MakeConstant("int", 15),
                                Expression.MakeConstant("int", 16)
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("InteroperabilityTest.ArgumentTest"),
                    AstNode.MakeIdentifier("ArgumentTest"),
                    AstNode.MakeIdentifier("./InteroperabilityTest.dll")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestModule3()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/test_module3.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("test_module3", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "usePrivateFunctionImpl",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "n",
                            AstType.MakePrimitiveType("int")
                        )
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Expression.MakeBinaryExpr(
                                OperatorType.Plus,
                                Helpers.MakeIdentifierPath("n"),
                                Expression.MakeConstant("int", 1)
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "usePrivateFunction",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "n",
                            AstType.MakePrimitiveType("int")
                        )
                    ),
                    Statement.MakeBlock(
                        Helpers.MakeSingleItemReturnStatement(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("usePrivateFunctionImpl"),
                                Helpers.MakeIdentifierPath("n")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.Export
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void IndirectlyUsingUnexportedItem()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/indirectly_using_unexported_item.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("usePrivateFunction"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${a}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("test_module3::usePrivateFunction"),
                    AstNode.MakeIdentifier("usePrivateFunction"),
                    AstNode.MakeIdentifier("./test_module3.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void LazyInitedArray()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/lazy-inited_array.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("chars"),
                            Expression.MakeArrayInitializer(
                                AstType.MakePrimitiveType("char"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("chars"),
                                    Expression.MakeConstant("int", 0)
                                ),
                                Expression.MakeConstant("char", 'a')
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("chars"),
                                    Expression.MakeConstant("int", 1)
                                ),
                                Expression.MakeConstant("char", 'b')
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIndexer(
                                    Helpers.MakeIdentifierPath("chars"),
                                    Expression.MakeConstant("int", 2)
                                ),
                                Expression.MakeConstant("char", 'c')
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("str"),
                            Expression.MakeObjectCreation(
                                AstType.MakePrimitiveType("string"),
                                AstNode.MakeIdentifier("value"),
                                Helpers.MakeIdentifierPath("chars")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${str}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ImplementingTypeDefinedInExternalModule()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/implementing_type_defined_in_external_module.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SimpleClass",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("SimpleInterface")
                    ),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "someMethod",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeConstant("int", 10)
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SimpleClass")
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst.someMethod()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("test_module4::SimpleInterface"),
                    AstNode.MakeIdentifier("SimpleInterface"),
                    AstNode.MakeIdentifier("./test_module4.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void CallMethodDefinedAfterCurrentMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/call_method_defined_after_current_method.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "callee",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "callee called")
                                )
                            ),
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("calledMethod")
                                    )
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "calledMethod",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "calledMethod called")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Private
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass")
                            ),
                            Modifiers.None
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("callee")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void SelfClassAsField()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/self_class_as_field.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeEnumDecl(
                    "MyOption",
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(
                            AstType.MakeParameterType("T")
                        )
                    ),
                    Helpers.MakeTupleStyleMember(
                        "Some",
                        "MyOption",
                        null,
                        AstType.MakeParameterType("T")
                    ),
                    Helpers.MakeTupleStyleMember(
                        "None",
                        "MyOption"
                    ),
                    Helpers.MakeFunc(
                        "unwrap",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Helpers.MakeIdentifierPath("value")
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("value")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Statement.MakeThrowStmt(
                                        Expression.MakeObjectCreation(
                                            Helpers.MakeGenericType("Exception"),
                                            AstNode.MakeIdentifier("message"),
                                            Expression.MakeConstant("string", "option is empty")
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("None")
                                        )
                                    )
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "Node",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "Next",
                            Helpers.MakeGenericType(
                                "MyOption",
                                Helpers.MakeGenericType("Node")
                            )
                        ),
                        Expression.Null,
                        Modifiers.Public
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "value",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getValue",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("value")
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "print",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "${self.value}, Next: ${self.Next.unwrap().value}, OneAfterNext: ${self.Next.unwrap().Next.unwrap().getValue()}")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("node"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("Node"),
                                Helpers.MakeSeq(
                                    AstNode.MakeIdentifier("Next"),
                                    AstNode.MakeIdentifier("value")
                                ),
                                Helpers.MakeSeq<Expression>(
                                    Expression.MakeObjectCreation(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("MyOption"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        AstNode.MakeIdentifier("0"),
                                        Expression.MakeObjectCreation(
                                            Helpers.MakeGenericType("Node"),
                                            Helpers.MakeSeq(
                                                AstNode.MakeIdentifier("Next"),
                                                AstNode.MakeIdentifier("value")
                                            ),
                                            Helpers.MakeSeq<Expression>(
                                                Expression.MakeObjectCreation(
                                                    AstType.MakeMemberType(
                                                        Helpers.MakeGenericType("MyOption"),
                                                        Helpers.MakeGenericType("Some")
                                                    ),
                                                    AstNode.MakeIdentifier("0"),
                                                    Expression.MakeObjectCreation(
                                                        Helpers.MakeGenericType("Node"),
                                                        Helpers.MakeSeq(
                                                            AstNode.MakeIdentifier("Next"),
                                                            AstNode.MakeIdentifier("value")
                                                        ),
                                                        Helpers.MakeSeq<Expression>(
                                                            Expression.MakeObjectCreation(
                                                                AstType.MakeMemberType(
                                                                    Helpers.MakeGenericType("MyOption"),
                                                                    Helpers.MakeGenericType("None")
                                                                ),
                                                                AstType.MakeKeyValueType(
                                                                    AstType.MakeParameterType(""),
                                                                    Helpers.MakeGenericType("Node")
                                                                )
                                                            ),
                                                            Expression.MakeConstant("int", 30)
                                                        )
                                                    ),
                                                    AstType.MakeKeyValueType(
                                                        AstType.MakeParameterType(""),
                                                        Helpers.MakeGenericType("Node")
                                                    )
                                                ),
                                                Expression.MakeConstant("int", 20)
                                            )
                                        ),
                                        AstType.MakeKeyValueType(
                                            AstType.MakeParameterType(""),
                                            Helpers.MakeGenericType("Node")
                                        )
                                    ),
                                    Expression.MakeConstant("int", 10)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("node"),
                                    AstNode.MakeIdentifier("print")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Exception"),
                    AstNode.MakeIdentifier("Exception")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void LazyInitVariable()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/lazy-init_variable.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeExactPatternWithType(
                                "n",
                                AstType.MakePrimitiveType("int")
                            ),
                            Expression.Null,
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Helpers.MakeIdentifierPath("n"),
                                Expression.MakeConstant("int", 10)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${n}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestArrangementOfCtorParameters()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/test_arrangement_of_ctor_parameters.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeSeq(
                            Helpers.MakePaticularPatternWithType(
                                "a",
                                AstType.MakePrimitiveType("int")
                            ),
                            Helpers.MakePaticularPatternWithType(
                                "b",
                                AstType.MakePrimitiveType("int")
                            )
                        ),
                        Helpers.MakeSeq(
                            Expression.Null,
                            Expression.Null
                        ),
                        Modifiers.Immutable | Modifiers.Private
                    ),
                    Helpers.MakeFunc(
                        "print",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "SomeClass{{${self.a}, ${self.b}}}")
                                )
                            )
                        ),
                        AstType.MakePlaceholderType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass"),
                                Helpers.MakeSeq(
                                    AstNode.MakeIdentifier("b"),
                                    AstNode.MakeIdentifier("a")
                                ),
                                Helpers.MakeSeq(
                                    Expression.MakeConstant("int", 20),
                                    Expression.MakeConstant("int", 10)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("print")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void UsingUseGuide()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/using_use_guide.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeInterfaceDecl(
                    "PostModifierInterface",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "c",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        Helpers.MakeVoidType(),
                        Modifiers.Public,
                        Helpers.MakePostModifiers(
                            null, AstNode.MakeIdentifier("d"), null
                        )
                    ),
                    Helpers.MakeFunc(
                        "d",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "PostModifierTest",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("PostModifierInterface")
                    ),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "a",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "a called")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "b",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "b called")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public,
                        Helpers.MakePostModifiers(Helpers.MakeSomeIdent("a"), null, null)
                    ),
                    // FIXME: Can be deleted: 2019/8/8
                    /*Helpers.MakeFunc(
                        "c",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "c called")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public,
                        Helpers.MakePostModifiers(null, Helpers.MakeSomeIdent("a"), null)
                    ),*/
                    Helpers.MakeFunc(
                        "c",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "c called")
                                )
                            ),
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("d")
                                    )
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "d",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "d called")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("PostModifierTest")
                            ),
                            Modifiers.Immutable
                        ),
                        // FIXME: Can be deleted: 2019/8/8
                        /*Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("c")
                                )
                            )
                        ),*/
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("a")
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("b")
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("c")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void PrimitiveCasts()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/primitive_casts.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeConstant("int", 1),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b"),
                            Expression.MakeConstant("uint", 1u),
                            Modifiers.Immutable
                        ),
                        /*Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeConstant("bigint", 1),
                            Modifiers.Immutable
                        )*/
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("d"),
                            Expression.MakeConstant("float", 1.0f),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("e"),
                            Expression.MakeConstant("double", 1.0),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b2"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("a"), AstType.MakePrimitiveType("uint")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("c2"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("a"), AstType.MakePrimitiveType("float")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("d2"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("a"), AstType.MakePrimitiveType("double")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("e2"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("a"), AstType.MakePrimitiveType("byte")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a3"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("b"), AstType.MakePrimitiveType("int")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("c3"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("b"), AstType.MakePrimitiveType("float")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("d3"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("b"), AstType.MakePrimitiveType("double")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("e3"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("b"), AstType.MakePrimitiveType("byte")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a5"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("d"), AstType.MakePrimitiveType("int")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b5"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("d"), AstType.MakePrimitiveType("uint")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("c5"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("d"), AstType.MakePrimitiveType("double")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("e5"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("d"), AstType.MakePrimitiveType("byte")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a6"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("e"), AstType.MakePrimitiveType("int")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("b6"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("e"), AstType.MakePrimitiveType("uint")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("c6"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("e"), AstType.MakePrimitiveType("float")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("d6"),
                            Expression.MakeCastExpr(Helpers.MakeIdentifierPath("e"), AstType.MakePrimitiveType("byte")),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "from int: ${b2}, ${c2}, ${d2}, ${e2}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "from uint: ${a3}, ${c3}, ${d3}, ${e3}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "from float: ${a5}, ${b5}, ${c5}, ${e5}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "from double: ${a6}, ${b6}, ${c6}, ${d6}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void EnumsPointAtEachOther()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/enums_point_at_each_other.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeEnumDecl(
                    "EnumA",
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(AstType.MakeParameterType("T"))
                    ),
                    Helpers.MakeTupleStyleMember(
                        "A",
                        "EnumA"
                    ),
                    Helpers.MakeTupleStyleMember(
                        "B",
                        "EnumA",
                        null,
                        AstType.MakePrimitiveType("int")
                    ),
                    Helpers.MakeFunc(
                        "convert",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeObjectCreation(
                                            AstType.MakeMemberType(
                                                Helpers.MakeGenericType("EnumB"),
                                                Helpers.MakeGenericType("C")
                                            ),
                                            AstType.MakeKeyValueType(
                                                AstType.MakeParameterType(""),
                                                AstType.MakeParameterType("T")
                                            )
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("EnumA"),
                                            Helpers.MakeGenericType("A")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeObjectCreation(
                                            AstType.MakeMemberType(
                                                Helpers.MakeGenericType("EnumB"),
                                                Helpers.MakeGenericType("D")
                                            ),
                                            AstNode.MakeIdentifier("0"),
                                            Helpers.MakeIdentifierPath("value"),
                                            AstType.MakeKeyValueType(
                                                AstType.MakeParameterType(""),
                                                AstType.MakeParameterType("T")
                                            )
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("EnumA"),
                                            Helpers.MakeGenericType("B")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(AstNode.MakeIdentifier("value"))
                                    )
                                )
                            )
                        ),
                        Helpers.MakeGenericType(
                            "EnumB",
                            AstType.MakeParameterType("T")
                        ),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeEnumDecl(
                    "EnumB",
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(AstType.MakeParameterType("T"))
                    ),
                    Helpers.MakeTupleStyleMember(
                        "C",
                        "EnumB"
                    ),
                    Helpers.MakeTupleStyleMember(
                        "D",
                        "EnumB",
                        null,
                        AstType.MakePrimitiveType("int")
                    ),
                    Helpers.MakeFunc(
                        "convert",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeObjectCreation(
                                            AstType.MakeMemberType(
                                                Helpers.MakeGenericType("EnumA"),
                                                Helpers.MakeGenericType("A")
                                            ),
                                            AstType.MakeKeyValueType(
                                                AstType.MakeParameterType(""),
                                                AstType.MakeParameterType("T")
                                            )
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("EnumB"),
                                            Helpers.MakeGenericType("C")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeObjectCreation(
                                            AstType.MakeMemberType(
                                                Helpers.MakeGenericType("EnumA"),
                                                Helpers.MakeGenericType("B")
                                            ),
                                            AstNode.MakeIdentifier("0"),
                                            Helpers.MakeIdentifierPath("value"),
                                            AstType.MakeKeyValueType(
                                                AstType.MakeParameterType(""),
                                                AstType.MakeParameterType("T")
                                            )
                                        )
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("EnumB"),
                                            Helpers.MakeGenericType("D")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(AstNode.MakeIdentifier("value"))
                                    )
                                )
                            )
                        ),
                        Helpers.MakeGenericType(
                            "EnumA",
                            AstType.MakeParameterType("T")
                        ),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "toString",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeMatchStmt(
                                Expression.MakeSelfRef(),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeConstant("string", "EnumB::C{}")
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("EnumB"),
                                            Helpers.MakeGenericType("C")
                                        )
                                    )
                                ),
                                Statement.MakeMatchClause(
                                    null,
                                    Helpers.MakeSingleItemReturnStatement(
                                        Expression.MakeConstant("string", "EnumB::D{{${value}}}")
                                    ),
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("EnumB"),
                                            Helpers.MakeGenericType("D")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(AstNode.MakeIdentifier("value"))
                                    )
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("string"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("enum_a"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("EnumA"),
                                    Helpers.MakeGenericType("A")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("converted_a"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("enum_a"),
                                    AstNode.MakeIdentifier("convert")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${converted_a.toString()}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("enum_b"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("EnumA"),
                                    Helpers.MakeGenericType("B")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 10),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("converted_b"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("enum_b"),
                                    AstNode.MakeIdentifier("convert")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${converted_b.toString()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestStd()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/test_std.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 10),
                                AstType.MakeKeyValueType(AstType.MakeParameterType(""), AstType.MakePrimitiveType("int"))
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${option.unwrap()}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("mapped"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("option"),
                                    AstNode.MakeIdentifier("map")
                                ),
                                Helpers.MakeClosureExpressionInParser(
                                    AstType.MakePlaceholderType(),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Plus,
                                                Helpers.MakeIdentifierPath("value"),
                                                Expression.MakeConstant("int", 1)
                                            )
                                        )
                                    ),
                                    EntityDeclaration.MakeParameter(
                                        Helpers.MakeSomeIdent("value")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${mapped.unwrap()}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("result"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Result"),
                                    Helpers.MakeGenericType("Ok")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 20),
                                AstType.MakeKeyValueType(AstType.MakeParameterType(""), AstType.MakePrimitiveType("int")),
                                AstType.MakeKeyValueType(AstType.MakeParameterType(""), AstType.MakePrimitiveType("string"))
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${result.unwrapOr(30)}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType(
                            "std_expresso::Option",
                            AstType.MakePlaceholderType()
                        ),
                        Helpers.MakeGenericType(
                            "std_expresso::Result",
                            AstType.MakePlaceholderType(),
                            AstType.MakePlaceholderType()
                        )
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("Option"),
                        AstNode.MakeIdentifier("Result")
                    ),
                    AstNode.MakeIdentifier("std.exs")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void CallPrimitiveTypesStaticMethods()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/call_primitive_types_static_methods.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("str"),
                            Expression.MakeConstant("string", "abc"),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("modified"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("string"),
                                    AstNode.MakeIdentifier("Concat")
                                ),
                                Helpers.MakeIdentifierPath("str"),
                                Expression.MakeConstant("string", "def")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${modified}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("parsed"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("int"),
                                    AstNode.MakeIdentifier("Parse")
                                ),
                                Expression.MakeConstant("string", "10")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${parsed}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void MatchGuardActuallyTakenIntoAccount()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/match_guard_actually_taken_into_account.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("ch"),
                            Expression.MakeConstant("char", 'a'),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("flag"),
                            Expression.MakeConstant("bool", false),
                            Modifiers.Immutable
                        ),
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("ch"),
                            Statement.MakeMatchClause(
                                Helpers.MakeIdentifierPath("flag"),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "flag is true")
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(
                                    Expression.MakeConstant("char", 'a')
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "flag is false")
                                    )
                                ),
                                PatternConstruct.MakeExpressionPattern(
                                    Expression.MakeConstant("char", 'a')
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "otherwise")
                                    )
                                ),
                                PatternConstruct.MakeWildcardPattern()
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ExtensionMethods()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/extension_methods.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeConstant("string", "a"),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("connected"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("a"),
                                    AstNode.MakeIdentifier("Concatenate")
                                ),
                                Expression.MakeConstant("string", "bc")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${connected}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("ary"),
                            Helpers.MakeSequenceInitializer(
                                Helpers.MakeGenericType("array"),
                                Expression.MakeConstant("int", 1),
                                Expression.MakeConstant("int", 3),
                                Expression.MakeConstant("int", 5)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomePatternWithType("pair"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Expression.MakeParen(
                                        Expression.MakeCastExpr(
                                            Helpers.MakeIdentifierPath("ary"),
                                            Helpers.MakeGenericType(
                                                "IEnumerable",
                                                AstType.MakePrimitiveType("int")
                                            )
                                        )
                                    ),
                                    AstNode.MakeIdentifier("Zip")
                                ),
                                Expression.MakeCastExpr(
                                    Helpers.MakeSequenceInitializer(
                                        Helpers.MakeGenericType("array"),
                                        Expression.MakeConstant("int", 2),
                                        Expression.MakeConstant("int", 4),
                                        Expression.MakeConstant("int", 6)
                                    ),
                                    Helpers.MakeGenericType(
                                        "IEnumerable",
                                        AstType.MakePrimitiveType("int")
                                    )
                                ),
                                Expression.MakeClosureExpression(
                                    AstType.MakePlaceholderType(),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeParen(
                                                Expression.MakeSequenceExpression(
                                                    Helpers.MakeIdentifierPath("l"),
                                                    Helpers.MakeIdentifierPath("r")
                                                )
                                            )
                                        )
                                    ),
                                    default,
                                    null,
                                    EntityDeclaration.MakeParameter(
                                        Helpers.MakeSomeIdent("l")
                                    ),
                                    EntityDeclaration.MakeParameter(
                                        Helpers.MakeSomeIdent("r")
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${pair}")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Linq.Enumerable"),
                    AstNode.MakeIdentifier("Enumerable")
                ),
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType(
                        "System.Collections.Generic.IEnumerable",
                        AstType.MakePlaceholderType()
                    ),
                    AstNode.MakeIdentifier("IEnumerable")
                ),
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("InteroperabilityTest.ExtensionClass"),
                    AstNode.MakeIdentifier("ExtensionClass"),
                    AstNode.MakeIdentifier("./InteroperabilityTest.dll")
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void InterfaceImplementingInterface()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/interface_implementing_interface.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeInterfaceDecl(
                    "SomeInterface",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeInterfaceDecl(
                    "SomeInterface2",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("SomeInterface")
                    ),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "getXPlus",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "n",
                                AstType.MakePrimitiveType("int")
                            )
                        ),
                        null,
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("SomeInterface2")
                    ),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "getX",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("x")
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "getXPlus",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "n",
                                AstType.MakePrimitiveType("int")
                            )
                        ),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Plus,
                                    Helpers.MakeCallExpression(
                                        Expression.MakeMemRef(
                                            Expression.MakeSelfRef(),
                                            AstNode.MakeIdentifier("getX")
                                        )
                                    ),
                                    Helpers.MakeIdentifierPath("n")
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst.getX()}, ${inst.getXPlus(1)}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void InterfaceConstraint()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/interface_constraint.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeInterfaceDecl(
                    "SomeInterface",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "someMethod",
                        Enumerable.Empty<ParameterDeclaration>(),
                        null,
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("SomeInterface")
                    ),
                    Modifiers.None,
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            AstType.MakePrimitiveType("int")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "someMethod",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Expression.MakeMemRef(
                                    Expression.MakeSelfRef(),
                                    AstNode.MakeIdentifier("x")
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeClassDecl(
                    "GenericClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        Helpers.MakeTypeConstraint(
                            AstType.MakeParameterType("T"),
                            Helpers.MakeGenericType("SomeInterface")
                        )
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "inst",
                            AstType.MakeParameterType("T")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "doSomething",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Expression.MakeMemRef(
                                            Expression.MakeSelfRef(),
                                            AstNode.MakeIdentifier("inst")
                                        ),
                                        AstNode.MakeIdentifier("someMethod")
                                    )
                                )
                            )
                        ),
                        AstType.MakePrimitiveType("int"),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("GenericClass"),
                                AstNode.MakeIdentifier("inst"),
                                Expression.MakeObjectCreation(
                                    Helpers.MakeGenericType("SomeClass"),
                                    AstNode.MakeIdentifier("x"),
                                    Expression.MakeConstant("int", 10)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst.doSomething()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void UsingGenericsOnClosure()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/using_generics_on_closure.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "GenericClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(AstType.MakeParameterType("TParam"))
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "x",
                            AstType.MakeParameterType("TParam")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "map",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "f",
                                Helpers.MakeFunctionType(
                                    "closure",
                                    AstType.MakeParameterType("TResult"),
                                    AstType.MakeParameterType("TParam")
                                )
                            )
                        ),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("f"),
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("x")
                                    )
                                )
                            )
                        ),
                        AstType.MakeParameterType("TResult"),
                        Modifiers.Public,
                        Helpers.MakeSeq(
                            Helpers.MakeTypeConstraint(
                                AstType.MakeParameterType("TResult")
                            )
                        )
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("GenericClass"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("mapped"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("map")
                                ),
                                Helpers.MakeClosureExpressionInParser(
                                    AstType.MakePlaceholderType(),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Plus,
                                                Helpers.MakeIdentifierPath("value"),
                                                Expression.MakeConstant("int", 1)
                                            )
                                        )
                                    ),
                                    EntityDeclaration.MakeParameter(
                                        Helpers.MakeSomeIdent("value")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${mapped}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestEqualityOnRawValueEnum()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/test_equality_on_raw_value_enum.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeEnumDecl(
                    "SomeEnum",
                    Modifiers.None,
                    Helpers.MakeRawValueEnumField("A", "SomeEnum", 1)
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("some_enum"),
                            Helpers.MakePathExpressionInParser("SomeEnum", "A"),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Expression.MakeBinaryExpr(
                                    OperatorType.Equality,
                                    Helpers.MakeIdentifierPath("some_enum"),
                                    Helpers.MakePathExpressionInParser("SomeEnum", "A")
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "SomeEnum::A")
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("is_a"),
                            Expression.MakeBinaryExpr(
                                OperatorType.Equality,
                                Helpers.MakeIdentifierPath("some_enum"),
                                Helpers.MakePathExpressionInParser("SomeEnum", "A")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${is_a}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void StaticMethodWithClosuresThatTakeTypeParameters()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/static_method_with_closures_that_take_type_parameters.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeSeq(
                        AstNode.MakeTypeConstraint(AstType.MakeParameterType("T"))
                    ),
                    EntityDeclaration.MakeField(
                        Helpers.MakeExactPatternWithType(
                            "_x",
                            AstType.MakeParameterType("T")
                        ),
                        Expression.Null,
                        Modifiers.Private | Modifiers.Immutable
                    ),
                    Helpers.MakeFunc(
                        "applyClosures",
                        Helpers.MakeSeq(
                            EntityDeclaration.MakeParameter(
                                "c1",
                                Helpers.MakeFunctionType(
                                    "closure",
                                    AstType.MakeParameterType("T")
                                )
                            ),
                            EntityDeclaration.MakeParameter(
                                "c2",
                                Helpers.MakeFunctionType(
                                    "closure",
                                    AstType.MakeParameterType("U"),
                                    AstType.MakeParameterType("T")
                                )
                            )
                        ),
                        Statement.MakeBlock(
                            Helpers.MakeSingleItemReturnStatement(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("c2"),
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("c1")
                                    )
                                )
                            )
                        ),
                        AstType.MakeParameterType("U"),
                        Modifiers.Public | Modifiers.Static,
                        Helpers.MakeSeq(
                            Helpers.MakeTypeConstraint(
                                AstType.MakeParameterType("U")
                            )
                        )
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("result"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPathWithTypeArgumentsInParser(
                                        "SomeClass",
                                        AstType.MakePrimitiveType("int")
                                    ),
                                    Helpers.MakeFunctionIdentifier(
                                        "applyClosures",
                                        AstType.MakeParameterType("U"),
                                        Helpers.MakeFunctionType(
                                            "closure",
                                            AstType.MakeParameterType("T")
                                        ),
                                        Helpers.MakeFunctionType(
                                            "closure",
                                            AstType.MakeParameterType("U"),
                                            AstType.MakeParameterType("T")
                                        )
                                    )
                                ),
                                Helpers.MakeSeq(
                                    AstType.MakeKeyValueType(
                                        AstType.MakeParameterType(""),
                                        AstType.MakePrimitiveType("int")
                                    )
                                ),
                                Helpers.MakeClosureExpressionInParser(
                                    AstType.MakePlaceholderType(),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeConstant("int", 10)
                                        )
                                    )
                                ),
                                Helpers.MakeClosureExpressionInParser(
                                    AstType.MakePlaceholderType(),
                                    Statement.MakeBlock(
                                        Helpers.MakeSingleItemReturnStatement(
                                            Expression.MakeBinaryExpr(
                                                OperatorType.Plus,
                                                Helpers.MakeIdentifierPath("value"),
                                                Expression.MakeConstant("int", 1)
                                            )
                                        )
                                    ),
                                    EntityDeclaration.MakeParameter(
                                        Helpers.MakeSomeIdent("value")
                                    )
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${result}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ResultFunctionalities()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/result_functionalities.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "printResult",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "result",
                            Helpers.MakeGenericType(
                                "Result",
                                AstType.MakePrimitiveType("int"),
                                AstType.MakePrimitiveType("string")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("result"),
                            Statement.MakeMatchClause(
                                null,
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeConstant("string", "${value}")
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Result"),
                                        Helpers.MakeGenericType("Ok")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "value",
                                        AstType.MakePlaceholderType()
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Helpers.MakeSingleItemReturnStatement(
                                    Helpers.MakeIdentifierPath("error")
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Result"),
                                        Helpers.MakeGenericType("Error")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "error",
                                        AstType.MakePlaceholderType()
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePrimitiveType("string"),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("result"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Result"),
                                    Helpers.MakeGenericType("Ok")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 10),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("string")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("result_error"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Result"),
                                    Helpers.MakeGenericType("Error")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("string", "error"),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("string")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("result_b"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Result"),
                                    Helpers.MakeGenericType("Ok")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 5),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("string")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "isOk: ${result.isOk()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "isError: ${result.isError()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "ok: ${result.ok().unwrap()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "error: ${result_error.error().unwrap()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "map: ${result.map(|value| value + 1).unwrapOr(20)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "mapOrElse: ${result.mapOrElse<string>(|error| error, |value| \"${value}\")}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "mapError: ${result_error.mapError(|error| error).unwrapOr(20)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "and: ${result.and(result_b).unwrapOr(20)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "andThen: ${result.andThen(|value| value + 1).unwrapOr(20)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "or: ${result_error.or(result_b).unwrapOr(20)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "orElse: ${printResult(result_error.orElse(|error| error))}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "unwrapOrElse: ${result_error.unwrapOrElse(|_error| 20)}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType(
                            "std_expresso::Option",
                            AstType.MakePlaceholderType()
                        ),
                        Helpers.MakeGenericType(
                            "std_expresso::Result",
                            AstType.MakePlaceholderType(),
                            AstType.MakePlaceholderType()
                        )
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("Option"),
                        AstNode.MakeIdentifier("Result")
                    ),
                    "std.exs"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void OptionFunctionalities()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/option_functionalities.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "printOption",
                    Helpers.MakeSeq(
                        EntityDeclaration.MakeParameter(
                            "option",
                            Helpers.MakeGenericType(
                                "Option",
                                AstType.MakePrimitiveType("int")
                            )
                        )
                    ),
                    Statement.MakeBlock(
                        Statement.MakeMatchStmt(
                            Helpers.MakeIdentifierPath("option"),
                            Statement.MakeMatchClause(
                                null,
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeConstant("string", "${value}")
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Option"),
                                        Helpers.MakeGenericType("Some")
                                    ),
                                    PatternConstruct.MakeIdentifierPattern(
                                        "value",
                                        AstType.MakePlaceholderType()
                                    )
                                )
                            ),
                            Statement.MakeMatchClause(
                                null,
                                Helpers.MakeSingleItemReturnStatement(
                                    Expression.MakeConstant("string", "Option::None()")
                                ),
                                PatternConstruct.MakeDestructuringPattern(
                                    AstType.MakeMemberType(
                                        Helpers.MakeGenericType("Option"),
                                        Helpers.MakeGenericType("None")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePrimitiveType("string"),
                    Modifiers.None
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 10),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("none"),
                            Expression.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("None")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option_b"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 5),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option_c"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 20),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("error_msg"),
                            Expression.MakeConstant("string", "error"),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "isSome: ${option.isSome()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "isNone: ${option.isNone()}")
                            )
                        ),
                        Helpers.MakeTryStmt(
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Expression.MakeMemRef(
                                            Helpers.MakeIdentifierPath("none"),
                                            AstNode.MakeIdentifier("expect")
                                        ),
                                        Expression.MakeConstant("string", "thrown expected")
                                    )
                                )
                            ),
                            null,
                            Statement.MakeCatchClause(
                                AstNode.MakeIdentifier(
                                    "e",
                                    Helpers.MakeGenericType("Exception")
                                ),
                                Statement.MakeBlock(
                                    Statement.MakeExprStmt(
                                        Helpers.MakeCallExpression(
                                            Helpers.MakeIdentifierPath("println"),
                                            Expression.MakeConstant("string", "expect: ${e.Message}")
                                        )
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "unwrapOr: ${none.unwrapOr(20)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "unwrapOrElse: ${none.unwrapOrElse(|| 20)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "mapOr(Some): ${option.mapOr(20, |value| value + 1)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "mapOr(None): ${none.mapOr(20, |value| value + 1)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "mapOrElse: ${none.mapOrElse<int>(|| 20, |value| value + 1)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "okOr: ${option.okOr(error_msg)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "okOrElse: ${none.okOrElse(|| error_msg)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "and: ${option.and(option_b)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "andThen: ${option.andThen<int>(|value| value + 1)}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "filter: ${option.filter(|value| value == 10).unwrap()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "or: ${none.or(option_c).unwrap()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "orElse: ${none.orElse(|| 20).unwrap()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "xor: ${printOption(option.xor(option_c))}")
                            )
                        )/*,
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "replace: ${option.replace()}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "take: ${option.take()}")
                            )
                        )*/
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeGenericType("System.Exception"),
                    AstNode.MakeIdentifier("Exception")
                ),
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType(
                            "std_expresso::Option",
                            AstType.MakePlaceholderType()
                        ),
                        Helpers.MakeGenericType(
                            "std_expresso::Result",
                            AstType.MakePlaceholderType(),
                            AstType.MakePlaceholderType()
                        )
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("Option"),
                        AstNode.MakeIdentifier("Result")
                    ),
                    "std.exs"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestingIfLet()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/testing_if_let.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 10),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeValueBindingPattern(
                                Helpers.MakeSomePatternWithType(
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Option"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("value")
                                        )
                                    )
                                ),
                                Helpers.MakeIdentifierPath("option"),
                                Modifiers.Immutable
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "Option::Some{{${value}}}")
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("error"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Result"),
                                    Helpers.MakeGenericType("Error")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("string", "error"),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("string")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeValueBindingPattern(
                                Helpers.MakeSomePatternWithType(
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Result"),
                                            Helpers.MakeGenericType("Error")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("msg")
                                        )
                                    )
                                ),
                                Helpers.MakeIdentifierPath("error"),
                                Modifiers.Immutable
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "Result::Error{{${msg}}}")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType(
                            "std_expresso::Option",
                            AstType.MakePlaceholderType()
                        ),
                        Helpers.MakeGenericType(
                            "std_expresso::Result",
                            AstType.MakePlaceholderType(),
                            AstType.MakePlaceholderType()
                        )
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("Option"),
                        AstNode.MakeIdentifier("Result")
                    ),
                    "std.exs"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestingIfVar()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/testing_if_var.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("Some")
                                ),
                                AstNode.MakeIdentifier("0"),
                                Expression.MakeConstant("int", 10),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeValueBindingPattern(
                                Helpers.MakeSomePatternWithType(
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Option"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("value")
                                        )
                                    )
                                ),
                                Helpers.MakeIdentifierPath("option"),
                                Modifiers.None
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${value}")
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Expression.MakeSingleAssignment(
                                        Helpers.MakeIdentifierPath("value"),
                                        Expression.MakeConstant("int", 20)
                                    )
                                ),
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${value}")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType(
                            "std_expresso::Option",
                            AstType.MakePlaceholderType()
                        )
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("Option")
                    ),
                    "std.exs"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void UsingUseGuideInAComplicatedWay()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/using_use_guide_in_a_complicated_way.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "PostModifierTest2",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "a",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "called a")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "b",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "called b")
                                )
                            ),
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("a")
                                    )
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "c",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "called c")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public,
                        AstNode.MakePostModifiers(null, Helpers.MakeSeq(AstNode.MakeIdentifier("a")), null)
                    ),
                    Helpers.MakeFunc(
                        "d",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "called d")
                                )
                            ),
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("b")
                                    )
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Helpers.MakeObjectCreation(
                                Helpers.MakeGenericType("PostModifierTest2")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("d")
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("c")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestingFriendMethod()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/testing_friend_method.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeClassDecl(
                    "SomeClass",
                    Enumerable.Empty<AstType>(),
                    Modifiers.None,
                    Helpers.MakeFunc(
                        "a",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "a called")
                                )
                            ),
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Expression.MakeSelfRef(),
                                        AstNode.MakeIdentifier("b")
                                    )
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Public
                    ),
                    Helpers.MakeFunc(
                        "b",
                        Enumerable.Empty<ParameterDeclaration>(),
                        Statement.MakeBlock(
                            Statement.MakeExprStmt(
                                Helpers.MakeCallExpression(
                                    Helpers.MakeIdentifierPath("println"),
                                    Expression.MakeConstant("string", "b called")
                                )
                            )
                        ),
                        Helpers.MakeVoidType(),
                        Modifiers.Private,
                        AstNode.MakePostModifiers(Helpers.MakeSeq(AstNode.MakeIdentifier("a")), null, null)
                    )
                ),
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Helpers.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeClass")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("a")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void TestingIfLetWithMismatchingValue()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/testing_if_let_with_mismatching_value.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("option"),
                            Helpers.MakeObjectCreation(
                                AstType.MakeMemberType(
                                    Helpers.MakeGenericType("Option"),
                                    Helpers.MakeGenericType("None")
                                ),
                                AstType.MakeKeyValueType(
                                    AstType.MakeParameterType(""),
                                    AstType.MakePrimitiveType("int")
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeValueBindingPattern(
                                Helpers.MakeSomePatternWithType(
                                    PatternConstruct.MakeDestructuringPattern(
                                        AstType.MakeMemberType(
                                            Helpers.MakeGenericType("Option"),
                                            Helpers.MakeGenericType("Some")
                                        ),
                                        PatternConstruct.MakeIdentifierPattern(
                                            Helpers.MakeSomeIdent("value")
                                        )
                                    )
                                ),
                                Helpers.MakeIdentifierPath("option"),
                                Modifiers.Immutable
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "Option is some and the value is ${value}.")
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "Option::None{}")
                                    )
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType(
                            "std_expresso::Option",
                            AstType.MakePlaceholderType()
                        )
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("Option")
                    ),
                    "std.exs"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void Globs()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/globs.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("ArgumentTest"),
                                    AstNode.MakeIdentifier("MethodWithOptional")
                                ),
                                Expression.MakeConstant("int", 10)
                            )
                        ),
                        Statement.MakeIfStmt(
                            PatternConstruct.MakeExpressionPattern(
                                Helpers.MakeCallExpression(
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("EnumTest"),
                                        AstNode.MakeIdentifier("TestEnumeration")
                                    ),
                                    Expression.MakeMemRef(
                                        Helpers.MakeIdentifierPath("TestEnum"),
                                        AstNode.MakeIdentifier("SomeField")
                                    )
                                )
                            ),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "enum matched!")
                                    )
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("a"),
                            Expression.MakeConstant("string", "a"),
                            Modifiers.Immutable
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("connected"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("a"),
                                    AstNode.MakeIdentifier("Concatenate")
                                ),
                                Expression.MakeConstant("string", "bc")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${connected}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("InteroperabilityTest")
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("inst"),
                                    AstNode.MakeIdentifier("DoSomething")
                                )
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("seq"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("MyIntseq"),
                                Helpers.MakeSeq(
                                    AstNode.MakeIdentifier("start"),
                                    AstNode.MakeIdentifier("end"),
                                    AstNode.MakeIdentifier("step")
                                ),
                                Helpers.MakeSeq(
                                    Expression.MakeConstant("int", 0),
                                    Expression.MakeConstant("int", 10),
                                    Expression.MakeConstant("int", 1)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeValueBindingForStmt(
                            Modifiers.Immutable,
                            Helpers.MakeSomePatternWithType("i"),
                            Helpers.MakeIdentifierPath("seq"),
                            Statement.MakeBlock(
                                Statement.MakeExprStmt(
                                    Helpers.MakeCallExpression(
                                        Helpers.MakeIdentifierPath("println"),
                                        Expression.MakeConstant("string", "${i}")
                                    )
                                )
                            )
                        ),
                        Statement.MakeExprStmt(
                            Expression.MakeSingleAssignment(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("PropertyTest"),
                                    AstNode.MakeIdentifier("SomeProperty")
                                ),
                                Expression.MakeConstant("int", 100)
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "after: ${PropertyTest.SomeProperty}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst2"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("SomeGenericClass"),
                                Helpers.MakeSeq(
                                    AstNode.MakeIdentifier("t"),
                                    AstNode.MakeIdentifier("u")
                                ),
                                Helpers.MakeSeq(
                                    Expression.MakeConstant("int", 10),
                                    Expression.MakeConstant("uint", 10u)
                                )
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst2}")
                            )
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("StaticTest"),
                                    AstNode.MakeIdentifier("DoSomething")
                                )
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("InteroperabilityTest.*")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("*")
                    ),
                    "./InteroperabilityTest.dll"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void ImportClassesThatReferenceOtherDllOnCsharp()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/import_classes_that_reference_other_dll_on_csharp.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("ReferenceOtherDll"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 10)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst.Get()}")
                            )
                        ),
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("inst2"),
                            Expression.MakeObjectCreation(
                                Helpers.MakeGenericType("ConcreteReferenceOtherDll"),
                                AstNode.MakeIdentifier("x"),
                                Expression.MakeConstant("int", 20)
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${inst2.Get()}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("InteroperabilityTest.Additionals.ReferenceOtherDll"),
                        Helpers.MakeGenericType("InteroperabilityTest.Additionals.ConcreteReferenceOtherDll")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("ReferenceOtherDll"),
                        AstNode.MakeIdentifier("ConcreteReferenceOtherDll")
                    ),
                    "./InteroperabilityTest.dll"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }

        [Test]
        public void NullInCallExpression()
        {
            var parser = new Parser(new Scanner("../../sources/for_unit_tests/null_in_call_expression.exs"));
            parser.Parse();

            var ast = parser.TopmostAst;

            var expected = AstNode.MakeModuleDef("main", Helpers.MakeSeq<EntityDeclaration>(
                Helpers.MakeFunc(
                    "main",
                    Enumerable.Empty<ParameterDeclaration>(),
                    Statement.MakeBlock(
                        Statement.MakeVarDecl(
                            Helpers.MakeSomePatternWithType("str"),
                            Helpers.MakeCallExpression(
                                Expression.MakeMemRef(
                                    Helpers.MakeIdentifierPath("StaticTest"),
                                    AstNode.MakeIdentifier("GetString")
                                ),
                                Expression.MakeNullRef()
                            ),
                            Modifiers.Immutable
                        ),
                        Statement.MakeExprStmt(
                            Helpers.MakeCallExpression(
                                Helpers.MakeIdentifierPath("println"),
                                Expression.MakeConstant("string", "${str}")
                            )
                        )
                    ),
                    AstType.MakePlaceholderType(),
                    Modifiers.None
                )
            ), Helpers.MakeSeq(
                AstNode.MakeImportDecl(
                    Helpers.MakeSeq(
                        Helpers.MakeGenericType("InteroperabilityTest.StaticTest")
                    ),
                    Helpers.MakeSeq(
                        AstNode.MakeIdentifier("StaticTest")
                    ),
                    "./InteroperabilityTest.dll"
                )
            ));

            Assert.IsNotNull(ast);
            Helpers.AstStructuralEqual(ast, expected);
        }
    }
}
