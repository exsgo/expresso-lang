﻿using System;
namespace InteroperabilityTest.OptionalParameters
{
    public static class OptionalParameters
    {
        public static void UseDefaultParameterOnObject(object param = null)
        {
            Console.WriteLine($"UseDefaultParameterOnObject Called: {param}!");
        }

        public static void UseDefaultParameterOnString(string str = null)
        {
            Console.WriteLine($"UseDefaultParameterOnString Called: {str}!");
        }
    }
}
