﻿using System;
using System.Linq;

namespace InteroperabilityTest
{
    public static class ArgumentTest
    {
        public static void MethodWithOptional(int x, int y = 1)
        {
            Console.WriteLine("{0}, {1}", x, y);
        }

        public static void MethodWithoutOptional(int x)
        {
            Console.WriteLine("{0}", x);
        }

        public static void MethodWithParams(int x, params int[] ys)
        {
            Console.Write("{0}, ", x);

            if(ys.Any()){
                Console.Write("{0}", ys.First());
                foreach(var y in ys.Skip(1))
                    Console.Write(", {0}", y);
            }

            Console.WriteLine();
        }
    }
}
