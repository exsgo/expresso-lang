﻿using System;
namespace InteroperabilityTest
{
    public static class ExtensionClass
    {
        public static string Concatenate(this string a, string b)
        {
            return $"{a}{b}";
        }
    }
}
