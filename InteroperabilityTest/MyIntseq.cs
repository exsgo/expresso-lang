﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace InteroperabilityTest
{
    public class MyIntseq : IEnumerable<int>
    {
        int start, end, step;

        public MyIntseq(int start, int end, int step)
        {
            this.start = start;
            this.end = end;
            this.step = step;
        }

        class Enumerator : IEnumerator<int>
        {
            MyIntseq seq;
            int current, next;

            public Enumerator(MyIntseq seq)
            {
                this.seq = seq;
                next = seq.start - seq.step;
                current = -127;
            }

            public int Current{
                get{
                    if(next == int.MinValue)
                        throw new InvalidOperationException();

                    current = next;
                    return current;
                }
            }

            object IEnumerator.Current => Current;

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if(next == int.MinValue)
                    return false;

                if(seq.end == int.MinValue || seq.step > 0 && next + seq.step < seq.end || seq.step < 0 && next + seq.step > seq.end){
                    next += seq.step;
                    return true;
                }

                next = int.MinValue;
                return false;
            }

            public void Reset()
            {
                next = seq.start - seq.step;
                current = -127;
            }
        }

        public IEnumerator<int> GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
