﻿using System;
namespace InteroperabilityTest
{
    public class SomeGenericClass<T, U>
    {
        T t;
        U u;

        public SomeGenericClass(T t, U u)
        {
            this.t = t;
            this.u = u;
        }
    }
}
