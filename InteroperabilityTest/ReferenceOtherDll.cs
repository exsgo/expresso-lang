﻿using AnotherCSharpDll;

namespace InteroperabilityTest.Additionals
{
    public class ReferenceOtherDll : SomeInterface
    {
        int x;

        public ReferenceOtherDll(int x)
        {
            this.x = x;
        }

        public int Get()
        {
            return x;
        }
    }

    public class ConcreteReferenceOtherDll : AbstractClass
    {
        int x;

        public ConcreteReferenceOtherDll(int x)
        {
            this.x = x;
        }

        public override int Get()
        {
            return x;
        }
    }
}
