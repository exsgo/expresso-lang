#! /bin/bash

if [ "$1" = "ast" ]; then
	mono ../../../../bin/Release/exsc.exe ./exscAst.exs -o bin/Debug -e exscAst
elif [ "$1" = "test" ]; then
	mono ../../../../bin/Release/exsc.exe ./test.exs -o bin/Debug -e test
else
	mono ../../../../bin/Release/exsc.exe ./exsc.exs -o bin/Debug -e exsc
fi
