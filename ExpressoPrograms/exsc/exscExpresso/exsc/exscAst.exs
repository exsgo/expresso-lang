module main;


import ICSharpCode.NRefactory.TextLocation from "bin/Debug/ExpressoAst.dll" as TextLocation;
import Expresso.* from "bin/Debug/ExpressoAst.dll" as *;
import Expresso.Ast.* from "bin/Debug/ExpressoAst.dll" as *;

def main(args (- string[])
{
    var parser = Parser{scanner: Scanner{fileName: args[0]}, parentLocation: TextLocation.Empty};
    parser.Parse();

    let ast = parser.TopmostAst;
    println("${ast}");
}