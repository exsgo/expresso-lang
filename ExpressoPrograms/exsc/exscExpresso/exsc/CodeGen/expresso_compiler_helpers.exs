module expresso_compiler_helpers;


import System.Runtime.Serialization.ISerializable as ISerializable;
import ICSharpCode.NRefactory.TextLocation from "../bin/Debug/ExpressoAst.dll" as TextLocation;
import ICSharpCode.NRefactory.PatternMatching.* from "../bin/Debug/ExpressoAst.dll" as *;
import ICSharpCode.NRefactory.TypeSystem.* from "../bin/Debug/ExpressoAst.dll" as *;
import Expresso.IGenericCloneable<> from "../bin/Debug/ExpressoAst.dll" as IGenericCloneable;
import Expresso.Ast.{PrimitiveType, SimpleType, AstType, Modifiers, Identifier} from "../bin/Debug/ExpressoAst.dll" as {PrimitiveType, SimpleType, AstType, Modifiers, Identifier};
import Expresso.Ast.Analysis.{SymbolTable} from "../bin/Debug/ExpressoAst.dll" as {SymbolTable};
import expresso_std::Option<> from "std.exs" as Option;

let StartOfIdentifierId = 1u;

let StartOfPrimitiveIdentifierId = StartOfIdentifierId + 2u;

let EndOfPrimitiveIdentifierIds = StartOfPrimitiveIdentifierId + 15u;

export var EndOfImportedIdentifierIds = EndOfPrimitiveIdentifierIds;

let _TYPE_PREFIX = "type_";

let SpecialNamesMapInverse = {
    "Expresso.Runtime.Builtins.ExpressoIntegerSequence" : ("intseq", StartOfPrimitiveIdentifierId + 0u),
    "Expresso.Runtime.Builtins.Slice" : ("slice", StartOfPrimitiveIdentifierId + 1u),
    "System.Boolean" : ("bool", StartOfPrimitiveIdentifierId + 2u),
    "System.Int32" : ("int", StartOfPrimitiveIdentifierId + 3u),
    "System.UInt32" : ("uint", StartOfPrimitiveIdentifierId + 4u),
    "System.Single" : ("float", StartOfPrimitiveIdentifierId + 5u),
    "System.Double" : ("double", StartOfPrimitiveIdentifierId + 6u),
    "System.Char" : ("char", StartOfPrimitiveIdentifierId + 7u),
    "System.Byte" : ("byte", StartOfPrimitiveIdentifierId + 8u),
    "System.String" : ("string", StartOfPrimitiveIdentifierId + 9u),
    "System.Array" : ("array", StartOfPrimitiveIdentifierId + 10u),
    "System.Collections.Generic.List" : ("vector", StartOfPrimitiveIdentifierId + 11u),
    "System.Tuple" : ("tuple", StartOfPrimitiveIdentifierId + 12u),
    "System.Collections.Generic.Dictionary" : ("dictionary", StartOfPrimitiveIdentifierId + 13u),
    "System.Numeric.BigInteger" : ("bigint", StartOfPrimitiveIdentifierId + 14u)
};

export def getExpressoTypeName(csharpFullName (- string) -> string
{
    let backqoute_index = csharpFullName.IndexOf('`');
    // csharp_name contains names without `'s, which we need
    let csharp_name = csharpFullName.Substring(0, (backqoute_index == -1) ? csharpFullName.Length : backqoute_index);
    if SpecialNamesMapInverse.ContainsKey(csharp_name) {
        let primitive_type = SpecialNamesMapInverse[csharp_name];
        let (type_name, _) = primitive_type;
        return type_name;
    }else{
        return csharp_name;
    }
}

export def getPrimitiveAstType(type (- string) -> Option<PrimitiveType>
{
    let type_code = PrimitiveType.GetActualKnownTypeCodeForPrimitiveType(type);
    if type_code == KnownTypeCode.None {
        return Option::None<PrimitiveType>{};
    }

    return Option::Some<PrimitiveType>{0: AstType.MakePrimitiveType(type, TextLocation.Empty)};
}

export def registerPrimitiveTypesAndTheirBaseTypeTables(target (- SymbolTable)
{
    for let (_key, value) in SpecialNamesMapInverse {
        let (type_name, identifier_id) = value;
        let type = (type_name == "array" || type_name == "vector" || type_name == "dictionary") ? AstType.MakeSimpleType(type_name, TextLocation.Empty) as AstType
                                                                                                : AstType.MakePrimitiveType(type_name, TextLocation.Empty) as AstType;
        target.AddTypeSymbol(type_name, type, Modifiers.None);
        target.GetTypeSymbol(type_name).IdentifierId = identifier_id;
    }

    for let child_table in SymbolTable.PrimitiveTypeSymbolTables.Children {
        target.AddTypeTable(child_table.Clone());
    }
}