module expresso_name_binder;


import Expresso.Parser from "bin/Debug/ExpressoAst.dll" as Parser;
import Expresso.Ast.* from "bin/Debug/ExpressoAst.dll" as *;

/// # Summary
/// The name binding and name resolving:
/// During name binding, we first define names on variables, fields, methods and types.
/// And then bind names on those items except for types.
/// For types we'll bind them during type validity check.
/// By name binding, I mean that we give the same id defined on the name definition phase.
/// That means that it is not guaranteed that the type of the target object is resolved.
export class ExpressoNameBinder
{
    private var scope_counter (- int;
    private var symbol_table (- SymbolTable;
    private let parser (- Parser;

    public static bindAst(ast (- ExpressoAst, parser (- Parser)
    {
        let binder = ExpressoNameBinder{scope_counter: 0, symbol_table: parser.Symbols, parser: parser};
        binder.bind(ast);

        println("We have given ids on total of ${unique_id_generator.getCurrentId() - 2u} identifiers.");
    }
}