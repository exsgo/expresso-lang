module unique_id_generator;


import Expresso.Ast.Identifier from "../bin/Debug/ExpressoAst.dll" as Identifier;
import expresso_compiler_helpers::EndOfPrimitiveIdentifierId from "../CodeGen/expresso_compiler_helpers.exs" as EndOfPrimitiveIdentifierId;

var id = EndOfPrimitiveIdentifierId;

export def getCurrentId() -> uint
{
    return id;
}

export def defineNewId(ident (- Identifier) -> void
{
    if ident.IdentifierId != 0u {
        return;
    }

    ident.IdentifierId = id;
    id += 1;
}